-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: openspace_gc
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_category`
--

DROP TABLE IF EXISTS `account_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_category` (
  `category_id` bigint NOT NULL,
  `category_default_id` int DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_note` varchar(255) DEFAULT NULL,
  `category_serial` int DEFAULT NULL,
  `category_type` varchar(255) DEFAULT NULL,
  `nature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_category`
--

LOCK TABLES `account_category` WRITE;
/*!40000 ALTER TABLE `account_category` DISABLE KEYS */;
INSERT INTO `account_category` VALUES (1,101,'Fixed Asset','101',0,'Asset','Debit'),(2,102,'Cash & Cash Equivalance','102',0,'Asset','Debit'),(3,201,'Long Term Liability','201',0,'Liability','Credit'),(4,202,'Short Term Liability','202',0,'Liability','Credit'),(5,203,'Owner\'s Equity','203',0,'Liability','Credit'),(6,301,'Regular Income','301',0,'Income','Credit'),(7,302,'Others Income','302',0,'Income','Credit'),(8,401,'Regular Expense','401',0,'Expense','Debit'),(9,402,'Others Expense','402',0,'Expense','Debit'),(10,103,'Account Receivable','103',0,'Asset','Debit'),(11,204,'Account Payable','204',0,'Liability','Credit'),(12,103,'OFPS Account','N/A',1,'Asset','Debit');
/*!40000 ALTER TABLE `account_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_ledger`
--

DROP TABLE IF EXISTS `account_ledger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_ledger` (
  `ledger_id` bigint NOT NULL AUTO_INCREMENT,
  `ledger_name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `category_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`ledger_id`),
  UNIQUE KEY `UKrp4pm8e3bnud69kd9ndqj95x9` (`ledger_name`,`institute_id`),
  KEY `FK8pqjgnsu5vkiw599we85ien3h` (`category_id`),
  KEY `FK6cxcj7fw0fne1c7hh98r5pofj` (`institute_id`),
  CONSTRAINT `FK6cxcj7fw0fne1c7hh98r5pofj` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FK8pqjgnsu5vkiw599we85ien3h` FOREIGN KEY (`category_id`) REFERENCES `account_category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_ledger`
--

LOCK TABLES `account_ledger` WRITE;
/*!40000 ALTER TABLE `account_ledger` DISABLE KEYS */;
INSERT INTO `account_ledger` VALUES (1,'Cash','eee',2,1),(2,'IBBL','N/A',2,1),(3,'Tuition fee','All kind of Tuition Fees',6,1),(4,'Admission Fees','All kind of Admission Fees',6,1),(5,'All Fee Fine ','asdf',6,1),(6,'Sports Fee','N/A',6,1),(7,'Exam Fees','N/A',6,1),(8,'SBL-0002634313655','N/A',12,1),(9,'BKash','N/A',12,1),(11,'Scouts fee','',6,1),(13,'Office Snacs','N/A',8,1),(14,'Bkashs','',2,1),(56,'Student Income','Test',6,1),(201,'Entertainment','',8,1),(202,'Utility Bill','',8,1),(203,'Miscellaneous Fund','',2,1),(230,'Opening Balance','',5,1),(267,'test','this is demo',2,1),(268,'test44','test note',2,1);
/*!40000 ALTER TABLE `account_ledger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_transaction`
--

DROP TABLE IF EXISTS `account_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_transaction` (
  `trn_id` bigint NOT NULL AUTO_INCREMENT,
  `module_id` bigint DEFAULT NULL,
  `trn_amount` double DEFAULT NULL,
  `trn_date` date DEFAULT NULL,
  `trn_time` datetime DEFAULT NULL,
  `trn_type` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `voucher_id` bigint NOT NULL,
  `voucher_no` varchar(255) DEFAULT NULL,
  `voucher_note` varchar(255) DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  PRIMARY KEY (`trn_id`),
  UNIQUE KEY `UK2b98s4o7ng8p6xu1sys17ijh2` (`voucher_id`,`institute_id`),
  KEY `FKknwhsadul4ikg1n8ddcau06m8` (`institute_id`),
  CONSTRAINT `FKknwhsadul4ikg1n8ddcau06m8` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25663 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_transaction`
--

LOCK TABLES `account_transaction` WRITE;
/*!40000 ALTER TABLE `account_transaction` DISABLE KEYS */;
INSERT INTO `account_transaction` VALUES (1,2,1,'2021-04-22','2021-04-22 09:33:22','Receipt','babai',1,'','Manual Fees Collection',1),(2,2,50,'2021-04-22','2021-04-22 09:34:25','Receipt','babai',2,'','Manual Fees Collection',1),(3,2,780,'2021-04-25','2021-04-25 04:55:31','Receipt','babai',3,'','Manual Fees Collection',1),(4,2,1320,'2021-04-25','2021-04-25 05:05:09','Receipt','babai',4,'','Manual Fees Collection',1),(5,2,900,'2021-04-25','2021-04-25 05:28:33','Receipt','babai',5,'','Manual Fees Collection',1),(6,2,110,'2021-04-25','2021-04-25 05:41:00','Receipt','babai',6,'','Manual Fees Collection',1),(7,2,1000,'2021-04-25','2021-04-25 05:42:39','Receipt','babai',7,'','Manual Fees Collection',1),(8,2,1,'2021-04-25','2021-04-25 05:44:00','Receipt','babai',8,'','Manual Fees Collection',1),(9,2,1140,'2021-04-25','2021-04-25 05:45:19','Receipt','babai',9,'','Manual Fees Collection',1),(10,2,500,'2021-04-25','2021-04-25 05:45:41','Receipt','babai',10,'','Manual Fees Collection',1),(11,2,50,'2021-04-25','2021-04-25 07:04:29','Receipt','online',11,'','Online Fees Payment',1),(12,2,1120,'2021-04-25','2021-04-25 07:11:25','Receipt','babai',12,'','Manual Fees Collection',1),(13,2,100,'2021-04-25','2021-04-25 07:12:34','Receipt','babai',13,'','Manual Fees Collection',1),(14,1,3000,'2021-04-25','2021-04-25 08:20:53','Receipt','babai',14,'1000012','Note',1),(15,1,66,'2021-04-25','2021-04-25 08:23:13','Receipt','babai',15,'66666','',1),(16,1,500,'2021-04-25','2021-04-25 08:34:07','Receipt','babai',16,'','Note',1),(17,1,67,'2021-04-27','2021-04-25 08:34:55','Receipt','babai',17,'','',1),(18,1,345,'2021-04-25','2021-04-25 08:36:29','Payment','babai',18,'','',1),(19,1,500,'2021-04-25','2021-04-25 08:36:58','Contra','babai',19,'12','7657 567',1),(20,1,120,'2021-04-25','2021-04-25 08:39:40','Journal','babai',20,'we4','234',1),(21,2,500,'2021-05-19','2021-05-19 08:41:22','Receipt','jhonarther',21,'','Manual Fees Collection',1),(22,2,800,'2021-05-19','2021-05-19 08:42:58','Receipt','jhonarther',22,'','Manual Fees Collection',1),(23,2,900,'2021-05-19','2021-05-19 08:43:15','Receipt','jhonarther',23,'','Manual Fees Collection',1),(24,2,500,'2021-05-24','2021-05-24 04:12:41','Receipt','jhonarther',24,'','Manual Fees Collection',1),(25,2,1100,'2021-05-24','2021-05-24 04:30:12','Receipt','babai',25,'','Manual Fees Collection',1),(26,2,1300,'2021-05-24','2021-05-24 04:30:49','Receipt','babai',26,'','Manual Fees Collection',1),(27,2,1300,'2021-05-24','2021-05-24 04:41:12','Receipt','babai',27,'','Manual Fees Collection',1),(28,2,1200,'2021-05-24','2021-05-24 04:42:21','Receipt','babai',28,'','Manual Fees Collection',1),(29,2,1190,'2021-05-24','2021-05-24 05:32:56','Receipt','babai',29,'','Manual Fees Collection',1),(30,2,1190,'2021-05-24','2021-05-24 05:35:28','Receipt','babai',30,'','Manual Fees Collection',1),(31,2,1470,'2021-05-24','2021-05-24 05:36:44','Receipt','babai',31,'','Manual Fees Collection',1),(32,2,1300,'2021-05-24','2021-05-24 05:37:42','Receipt','babai',32,'','Manual Fees Collection',1),(33,2,1100,'2021-05-24','2021-05-24 05:39:03','Receipt','babai',33,'','Manual Fees Collection',1),(34,2,1900,'2021-05-25','2021-05-25 04:37:37','Receipt','jhonarther',34,'','Manual Fees Collection',1),(35,1,100,'2021-05-25','2021-05-25 09:14:39','Receipt','babai',35,'','',1),(36,1,100,NULL,'2021-05-25 09:15:23','Receipt','babai',36,'','',1),(37,1,300,'2021-05-25','2021-05-25 09:52:35','Contra','babai',37,'','',1),(38,2,350,'2021-05-27','2021-05-27 03:34:55','Receipt','online',38,'','Online Fees Payment',1),(39,2,350,'2021-05-27','2021-05-27 04:24:26','Receipt','jhonarther',39,'','Manual Fees Collection',1),(40,2,1300,'2021-05-27','2021-05-27 05:01:38','Receipt','jhonarther',40,'','Manual Fees Collection',1),(41,2,350,'2021-05-27','2021-05-27 06:21:40','Receipt','online',41,'','Online Fees Payment',1),(42,2,350,'2021-05-31','2021-05-31 10:23:37','Receipt','Atiqur Rahman',42,'','Manual Fees Collection',1),(44,2,500,'2021-06-01','2021-06-01 03:52:56','Receipt','online',43,'','Online Fees Payment',1),(46,2,200,'2021-06-06','2021-06-06 06:04:13','Receipt','babai',44,'','Manual Fees Collection',1),(47,1,500,'2021-06-06','2021-06-06 06:07:50','Receipt','babai',45,'','',1),(48,2,400,'2021-06-08','2021-06-08 08:28:40','Receipt','online',46,'','Online Fees Payment',1),(49,1,100,'2021-06-13','2021-06-13 09:52:24','Receipt','Atiqur Rahman',47,'3','selff',1),(50,1,50,'2021-06-13','2021-06-13 09:52:58','Receipt','Atiqur Rahman',48,'33','self',1),(52,1,50,'2021-06-13','2021-06-13 09:53:40','Contra','Atiqur Rahman',50,'ff','',1),(53,1,333,'2021-06-13','2021-06-13 10:25:03','Journal','Atiqur Rahman',51,'f','12',1),(54,2,500,'2021-06-15','2021-06-15 04:39:06','Receipt','online',52,'','Online Fees Payment',1),(55,2,400,'2021-06-22','2021-06-22 04:18:51','Receipt','Atiqur Rahman',53,'','Manual Fees Collection',1),(56,2,2800,'2021-06-24','2021-06-24 04:22:22','Receipt','online',54,'','Online Fees Payment',1),(57,2,7500,'2021-06-24','2021-06-24 04:23:54','Receipt','online',55,'','Online Fees Payment',1),(58,2,400,'2021-06-24','2021-06-24 05:26:47','Receipt','online',56,'','Online Fees Payment',1),(59,2,900,'2021-06-24','2021-06-24 05:43:40','Receipt','online',57,'','Online Fees Payment',1),(60,2,500,'2021-06-24','2021-06-24 05:44:19','Receipt','online',58,'','Online Fees Payment',1),(61,2,500,'2021-06-24','2021-06-24 10:46:39','Receipt','online',59,'','Online Fees Payment',1),(62,2,400,'2021-06-27','2021-06-27 04:07:56','Receipt','online',60,'','Online Fees Payment',1),(63,2,500,'2021-06-27','2021-06-27 07:14:00','Receipt','online',61,'','Online Fees Payment',1),(64,2,500,'2021-06-27','2021-06-27 07:28:31','Receipt','online',62,'','Online Fees Payment',1),(68,2,400,'2021-06-28','2021-06-28 05:33:59','Receipt','online',63,'','Online Fees Payment',1),(72,2,500,'2021-06-28','2021-06-28 07:20:56','Receipt','online',64,'','Online Fees Payment',1),(91,2,800,'2021-06-29','2021-06-29 08:22:06','Receipt','online',65,'','Online Fees Payment',1),(97,2,3000,'2021-07-04','2021-07-04 03:26:00','Receipt','jhonarther',66,'','Manual Fees Collection',1),(98,2,500,'2021-07-05','2021-07-05 06:10:07','Receipt','online',67,'','Online Fees Payment',1),(99,2,500,'2021-07-05','2021-07-05 08:45:49','Receipt','online',68,'','Online Fees Payment',1),(100,2,450,'2021-07-05','2021-07-05 09:38:05','Receipt','online',69,'','Online Fees Payment',1),(101,2,300,'2021-07-05','2021-07-05 11:01:55','Receipt','online',70,'','Online Fees Payment',1),(102,1,10,'2021-07-06','2021-07-06 06:21:36','Receipt','jhonarther',71,'','',1),(103,2,15,'2021-07-09','2021-07-09 09:28:06','Receipt','online',72,'','Online Fees Payment',1),(104,2,15,'2021-07-09','2021-07-09 09:33:08','Receipt','online',73,'','Online Fees Payment',1),(426,2,1300,'2021-08-08','2021-08-08 06:56:25','Receipt','01750316386',74,'','Manual Fees Collection',1),(833,2,1800,'2021-08-19','2021-08-19 09:06:07','Receipt','01951901919',75,'','Manual Fees Collection',1),(1008,2,15,'2021-09-02','2021-09-02 04:36:44','Receipt','online',76,'','Online Fees Payment',1),(1009,2,15,'2021-09-02','2021-09-02 04:47:06','Receipt','online',77,'','Online Fees Payment',1),(1203,2,400,'2021-09-05','2021-09-05 12:00:50','Receipt','babai',78,'','Manual Fees Collection',1),(1204,2,400,'2021-09-05','2021-09-05 12:05:06','Receipt','babai',79,'','Manual Fees Collection',1),(1400,2,15,'2021-09-06','2021-09-06 17:08:20','Receipt','online',80,'','Online Fees Payment',1),(1634,2,600,'2021-09-08','2021-09-08 03:54:31','Receipt','01917217779',81,'','Manual Fees Collection',1),(2769,2,15,'2021-09-14','2021-09-14 07:05:20','Receipt','online',82,'','Online Fees Payment',1),(4021,2,100,'2021-09-28','2021-09-28 07:09:58','Receipt','online',83,'','Online Fees Payment',1),(4173,2,15,'2021-09-28','2021-09-28 17:59:15','Receipt','online',84,'','Online Fees Payment',1),(5821,2,15,'2021-10-04','2021-10-04 08:17:06','Receipt','online',85,'','Online Fees Payment',1),(5922,1,200,'2021-10-05','2021-10-04 18:24:38','Contra','01706355722',86,'1','',1),(6940,2,15,'2021-10-05','2021-10-05 08:41:27','Receipt','online',87,'','Online Fees Payment',1),(11004,2,15,'2021-10-24','2021-10-24 11:09:23','Receipt','arif',88,'','Manual Fees Collection',1),(11243,2,5260,'2021-10-25','2021-10-25 11:09:36','Receipt','arif',89,'','Manual Fees Collection',1),(11244,2,200,'2021-10-25','2021-10-25 11:10:05','Receipt','arif',90,'','Manual Fees Collection',1),(11315,1,1350,'2021-10-25','2021-10-25 18:09:01','Payment','jhonarther',91,'1234','Guest Appayon,Tea, Biscuits, Cold Driks and Biriyani ',1),(11425,2,1125,'2021-10-26','2021-10-26 10:10:59','Receipt','01750316386',92,'','Manual Fees Collection',1),(11466,1,500,'2021-10-26','2021-10-26 11:28:34','Payment','01778708888',93,'452','Gas Bill of June 2021',1),(13161,2,15,'2021-11-02','2021-11-02 23:14:25','Receipt','online',94,'','Online Fees Payment',1),(13162,2,30,'2021-11-02','2021-11-02 23:18:32','Receipt','online',95,'','Online Fees Payment',1),(18908,2,15,'2021-11-09','2021-11-09 12:12:34','Receipt','online',96,'','Online Fees Payment',1),(18929,1,50000,'2021-11-09','2021-11-09 12:26:17','Journal','babai',97,'','',1),(20822,1,1000,'2021-11-23','2021-11-23 18:10:24','Contra','babai',98,'','',1),(25645,2,2000,'2022-01-06','2022-01-06 17:05:15','Receipt','jhonarther',99,'','Manual Fees Collection',1),(25646,2,2000,'2022-01-06','2022-01-06 17:05:15','Receipt','jhonarther',100,'','Manual Fees Collection',1),(25647,2,1000,'2022-01-12','2022-01-12 14:27:10','Receipt','jhonarther',101,'','Manual Fees Collection',1),(25648,2,150,'2022-01-13','2022-01-13 10:37:06','Receipt','jhonarther',102,'','Manual Fees Collection',1),(25649,2,344.96,'2022-01-16','2022-01-16 11:19:52','Receipt','jhonarther',103,'','Manual Fees Collection',1),(25651,2,15,'2022-01-25','2022-01-25 17:03:19','Receipt','jhonarther',104,'','Manual Fees Collection',1),(25652,2,210,'2022-01-30','2022-01-30 12:56:30','Receipt','jhonarther',105,'','Manual Fees Collection',1),(25653,2,30,'2022-02-02','2022-02-02 12:27:27','Receipt','01917217779',106,'','Manual Fees Collection',1),(25657,2,44,'2022-02-02','2022-02-02 16:42:10','Receipt','01917217779',107,'','Manual Fees Collection',1),(25658,2,33,'2022-02-13','2022-02-13 10:35:59','Receipt','arif',108,'','Manual Fees Collection',1),(25659,2,6000,'2022-02-13','2022-02-13 10:37:29','Receipt','arif',109,'','Manual Fees Collection',1),(25660,2,2010,'2022-02-13','2022-02-13 10:38:16','Receipt','arif',110,'','Manual Fees Collection',1),(25661,2,2880,'2022-02-13','2022-02-13 10:39:05','Receipt','arif',111,'','Manual Fees Collection',1),(25662,2,1240,'2022-02-13','2022-02-13 10:40:58','Receipt','arif',112,'','Manual Fees Collection',1);
/*!40000 ALTER TABLE `account_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_transaction_details`
--

DROP TABLE IF EXISTS `account_transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_transaction_details` (
  `trn_details_id` bigint NOT NULL AUTO_INCREMENT,
  `credit_amount` double DEFAULT NULL,
  `debit_amount` double DEFAULT NULL,
  `ledger_id` bigint DEFAULT NULL,
  `trn_id` bigint DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`trn_details_id`),
  KEY `FK67ht8icbdym7p3fpodevorm5i` (`ledger_id`),
  KEY `FKfgxiin6gn0xa8i4q7ifv42v97` (`trn_id`),
  KEY `FKm7pn9kfh6mc585nkf02jsvefk` (`institute_id`),
  CONSTRAINT `FK67ht8icbdym7p3fpodevorm5i` FOREIGN KEY (`ledger_id`) REFERENCES `account_ledger` (`ledger_id`),
  CONSTRAINT `FKfgxiin6gn0xa8i4q7ifv42v97` FOREIGN KEY (`trn_id`) REFERENCES `account_transaction` (`trn_id`),
  CONSTRAINT `FKm7pn9kfh6mc585nkf02jsvefk` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_transaction_details`
--

LOCK TABLES `account_transaction_details` WRITE;
/*!40000 ALTER TABLE `account_transaction_details` DISABLE KEYS */;
INSERT INTO `account_transaction_details` VALUES (1,0,1,1,1,1),(2,1,0,3,1,1),(3,0,50,1,2,1),(4,50,0,3,2,1),(5,0,780,1,3,1),(6,150,0,3,3,1),(7,30,0,5,3,1),(8,600,0,4,3,1),(9,0,1320,2,4,1),(10,100,0,3,4,1),(11,20,0,5,4,1),(12,1200,0,4,4,1),(13,0,900,2,5,1),(14,170,0,3,5,1),(15,40,0,5,5,1),(16,690,0,4,5,1),(17,0,110,1,6,1),(18,100,0,3,6,1),(19,10,0,5,6,1),(20,0,1000,1,7,1),(21,1000,0,5,7,1),(22,0,1,1,8,1),(23,1,0,4,8,1),(24,0,1140,1,9,1),(25,130,0,3,9,1),(26,1010,0,5,9,1),(27,0,500,1,10,1),(28,500,0,4,10,1),(29,0,50,8,11,1),(30,50,0,3,11,1),(31,0,1120,2,12,1),(32,100,0,3,12,1),(33,30,0,5,12,1),(34,990,0,4,12,1),(35,0,100,1,13,1),(36,90,0,3,13,1),(37,10,0,5,13,1),(38,0,3000,2,14,1),(39,3000,0,4,14,1),(40,0,66,2,15,1),(41,66,0,6,15,1),(42,0,500,2,16,1),(43,500,0,4,16,1),(44,0,67,2,17,1),(45,67,0,3,17,1),(46,345,0,1,18,1),(47,0,345,13,18,1),(48,500,0,1,19,1),(49,0,500,2,19,1),(50,0,120,9,20,1),(51,120,0,2,20,1),(52,0,500,1,21,1),(53,500,0,3,21,1),(54,0,800,1,22,1),(55,500,0,3,22,1),(56,300,0,7,22,1),(57,0,900,1,23,1),(58,500,0,3,23,1),(59,400,0,7,23,1),(60,0,500,2,24,1),(61,500,0,3,24,1),(62,0,1100,2,25,1),(63,800,0,3,25,1),(64,300,0,7,25,1),(65,0,1300,2,26,1),(66,1000,0,3,26,1),(67,300,0,7,26,1),(68,0,1300,2,27,1),(69,1000,0,3,27,1),(70,300,0,7,27,1),(71,0,1200,2,28,1),(72,1000,0,3,28,1),(73,200,0,7,28,1),(74,0,1190,2,29,1),(75,200,0,3,29,1),(76,40,0,5,29,1),(77,700,0,4,29,1),(78,250,0,7,29,1),(79,0,1190,2,30,1),(80,200,0,3,30,1),(81,40,0,5,30,1),(82,700,0,4,30,1),(83,250,0,7,30,1),(84,0,1470,2,31,1),(85,100,0,3,31,1),(86,1020,0,5,31,1),(87,100,0,4,31,1),(88,250,0,7,31,1),(89,0,1300,2,32,1),(90,1000,0,3,32,1),(91,300,0,7,32,1),(92,0,1100,2,33,1),(93,800,0,3,33,1),(94,300,0,7,33,1),(95,0,1900,1,34,1),(96,1000,0,3,34,1),(97,900,0,7,34,1),(98,0,100,2,35,1),(99,100,0,4,35,1),(100,0,100,2,36,1),(101,100,0,11,36,1),(102,300,0,1,37,1),(103,0,300,2,37,1),(104,0,350,8,38,1),(105,350,0,3,38,1),(106,0,350,1,39,1),(107,80,0,3,39,1),(108,20,0,5,39,1),(109,250,0,7,39,1),(110,0,1300,1,40,1),(111,1000,0,3,40,1),(112,300,0,7,40,1),(113,0,350,8,41,1),(114,350,0,3,41,1),(115,0,350,1,42,1),(116,350,0,3,42,1),(119,0,500,8,44,1),(120,500,0,3,44,1),(123,0,200,1,46,1),(124,200,0,3,46,1),(125,0,500,1,47,1),(126,500,0,6,47,1),(127,0,400,9,48,1),(128,400,0,3,48,1),(129,0,100,1,49,1),(130,100,0,3,49,1),(131,0,50,2,50,1),(132,50,0,7,50,1),(135,50,0,1,52,1),(136,0,50,2,52,1),(137,333,333,2,53,1),(138,0,500,8,54,1),(139,500,0,5,54,1),(140,0,400,1,55,1),(141,400,0,5,55,1),(142,0,2800,8,56,1),(143,2800,0,5,56,1),(144,0,7500,8,57,1),(145,7000,0,3,57,1),(146,500,0,5,57,1),(147,0,400,8,58,1),(148,400,0,5,58,1),(149,0,900,8,59,1),(150,500,0,5,59,1),(151,400,0,3,59,1),(152,0,500,8,60,1),(153,500,0,5,60,1),(154,0,500,8,61,1),(155,500,0,5,61,1),(156,0,400,8,62,1),(157,400,0,5,62,1),(158,0,500,8,63,1),(159,500,0,5,63,1),(160,0,500,8,64,1),(161,500,0,5,64,1),(168,0,400,8,68,1),(169,400,0,5,68,1),(176,0,500,8,72,1),(177,500,0,5,72,1),(215,0,800,8,91,1),(216,400,0,5,91,1),(217,400,0,3,91,1),(228,0,3000,1,97,1),(229,3000,0,5,97,1),(230,0,500,8,98,1),(231,500,0,5,98,1),(232,0,500,8,99,1),(233,500,0,5,99,1),(234,0,450,8,100,1),(235,450,0,5,100,1),(236,0,300,8,101,1),(237,300,0,3,101,1),(238,0,10,1,102,1),(239,10,0,4,102,1),(240,0,15,8,103,1),(241,15,0,5,103,1),(242,0,15,8,104,1),(243,15,0,5,104,1),(869,0,1300,2,426,1),(870,1000,0,5,426,1),(871,300,0,3,426,1),(1687,0,1800,1,833,1),(1688,1300,0,5,833,1),(1689,500,0,3,833,1),(2040,0,15,8,1008,1),(2041,15,0,5,1008,1),(2042,0,15,8,1009,1),(2043,15,0,5,1009,1),(2428,0,400,1,1203,1),(2429,400,0,5,1203,1),(2430,0,400,1,1204,1),(2431,400,0,5,1204,1),(2814,0,15,8,1400,1),(2815,15,0,5,1400,1),(3276,0,600,1,1634,1),(3277,600,0,3,1634,1),(5526,0,15,8,2769,1),(5527,15,0,3,2769,1),(8012,0,100,8,4021,1),(8013,100,0,3,4021,1),(8316,0,15,8,4173,1),(8317,15,0,3,4173,1),(11547,0,15,8,5821,1),(11548,15,0,3,5821,1),(11749,200,0,1,5922,1),(11750,0,200,2,5922,1),(13717,0,15,8,6940,1),(13718,15,0,3,6940,1),(22194,0,15,2,11004,1),(22195,15,0,3,11004,1),(22800,0,5260,1,11243,1),(22801,5000,0,56,11243,1),(22802,250,0,3,11243,1),(22803,10,0,5,11243,1),(22804,0,200,1,11244,1),(22805,190,0,3,11244,1),(22806,10,0,5,11244,1),(22979,1350,0,203,11315,1),(22980,0,1350,13,11315,1),(23199,0,1125,1,11425,1),(23200,15,0,3,11425,1),(23201,1110,0,5,11425,1),(23330,500,0,203,11466,1),(23331,0,500,202,11466,1),(27304,0,15,8,13161,1),(27305,15,0,3,13161,1),(27306,0,30,8,13162,1),(27307,30,0,3,13162,1),(38834,0,15,8,18908,1),(38835,15,0,3,18908,1),(38876,0,50000,1,18929,1),(38877,50000,0,230,18929,1),(42852,1000,0,203,20822,1),(42853,0,1000,14,20822,1),(52487,0,2000,1,25645,1),(52488,1900,0,3,25645,1),(52489,100,0,5,25645,1),(52490,0,2000,1,25646,1),(52491,1900,0,3,25646,1),(52492,100,0,5,25646,1),(52493,0,1000,1,25647,1),(52494,1000,0,3,25647,1),(52495,0,150,1,25648,1),(52496,99.99,0,3,25648,1),(52497,50.010000000000005,0,5,25648,1),(52498,0,344.96,1,25649,1),(52499,249.92,0,3,25649,1),(52500,95.04,0,5,25649,1),(52503,0,15,1,25651,1),(52504,15,0,3,25651,1),(52505,0,210,1,25652,1),(52506,160,0,3,25652,1),(52507,50,0,5,25652,1),(52508,0,30,14,25653,1),(52509,30,0,3,25653,1),(52516,0,44,1,25657,1),(52517,44,0,3,25657,1),(52518,0,33,2,25658,1),(52519,33,0,3,25658,1),(52520,0,6000,2,25659,1),(52521,1000,0,5,25659,1),(52522,5000,0,56,25659,1),(52523,0,2010,1,25660,1),(52524,1500,0,3,25660,1),(52525,510,0,5,25660,1),(52526,0,2880,1,25661,1),(52527,1550,0,3,25661,1),(52528,1330,0,5,25661,1),(52529,0,1240,1,25662,1),(52530,1000,0,3,25662,1),(52531,200,0,5,25662,1),(52532,40,0,6,25662,1);
/*!40000 ALTER TABLE `account_transaction_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_teacher_assign`
--

DROP TABLE IF EXISTS `class_teacher_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `class_teacher_assign` (
  `class_teacher_assign_id` bigint NOT NULL AUTO_INCREMENT,
  `class_config_id` bigint DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  `staff_id` bigint DEFAULT NULL,
  PRIMARY KEY (`class_teacher_assign_id`),
  UNIQUE KEY `UKeha66qacjcrxc7grfhxt83fci` (`staff_id`,`class_config_id`,`institute_id`),
  KEY `FK644uqheswkm6aashi2m201ttm` (`class_config_id`),
  KEY `FK3jw94u3nnooc00ckjbu44gnbc` (`institute_id`),
  CONSTRAINT `FK1u812lrjpnrlker5qdynonepu` FOREIGN KEY (`staff_id`) REFERENCES `staff_basic_info` (`staff_id`),
  CONSTRAINT `FK3jw94u3nnooc00ckjbu44gnbc` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FK644uqheswkm6aashi2m201ttm` FOREIGN KEY (`class_config_id`) REFERENCES `core_setting_class_configuration` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=288 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_teacher_assign`
--

LOCK TABLES `class_teacher_assign` WRITE;
/*!40000 ALTER TABLE `class_teacher_assign` DISABLE KEYS */;
INSERT INTO `class_teacher_assign` VALUES (2,21,1,1),(133,325,1,1),(131,326,1,1),(49,325,1,2),(31,2,1,12),(5,3,1,137),(287,3,1,599);
/*!40000 ALTER TABLE `class_teacher_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_academic_year`
--

DROP TABLE IF EXISTS `core_setting_academic_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_academic_year` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKem2mkhdf55yckabu2j7pd1awn` (`name`,`institute_id`),
  KEY `FK4e805992qdy34bhl4vld3plmr` (`institute_id`),
  CONSTRAINT `FK4e805992qdy34bhl4vld3plmr` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_academic_year`
--

LOCK TABLES `core_setting_academic_year` WRITE;
/*!40000 ALTER TABLE `core_setting_academic_year` DISABLE KEYS */;
INSERT INTO `core_setting_academic_year` VALUES (2,'2021',2,1,1),(3,'2022',3,1,1),(4,'2028',1,1,1),(7,'2023',4,1,1),(28,'2026',1,1,1),(29,'2019',6,1,1),(69,'2024',5,0,1);
/*!40000 ALTER TABLE `core_setting_academic_year` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_class`
--

DROP TABLE IF EXISTS `core_setting_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_class` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  `online_admission` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKsx8brjtayqkl6893dxh4h5syy` (`name`,`institute_id`),
  KEY `FK28ojppy3og7b5n7331b658a2g` (`institute_id`),
  CONSTRAINT `FK28ojppy3og7b5n7331b658a2g` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_class`
--

LOCK TABLES `core_setting_class` WRITE;
/*!40000 ALTER TABLE `core_setting_class` DISABLE KEYS */;
INSERT INTO `core_setting_class` VALUES (2,'One',1,1,1,NULL),(3,'Two',2,1,1,NULL),(4,'Six (private\'s)',6,1,1,NULL),(5,'Nine',9,1,1,NULL),(92,'Three ',3,1,1,NULL),(129,'Five',5,0,1,NULL),(146,'Four',4,1,1,NULL),(148,'Alim',32,1,1,NULL),(149,'Eleven',11,1,1,NULL),(151,'Seven',7,1,1,NULL),(152,'Eight',8,1,1,NULL),(153,'Ten',10,1,1,NULL),(154,'Twelve',12,1,1,NULL),(299,'ASDF (1st)',1,1,1,0);
/*!40000 ALTER TABLE `core_setting_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_class_configuration`
--

DROP TABLE IF EXISTS `core_setting_class_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_class_configuration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `class_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  `section_id` bigint NOT NULL,
  `shift_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKd765ev0l72rduchw9frx5ie4j` (`class_id`,`shift_id`,`section_id`,`institute_id`),
  KEY `FK1r0jj5y4y0eo1eunt4uqraskh` (`institute_id`),
  KEY `FKpovjf4y82vo0tyji2uw3xdiju` (`section_id`),
  KEY `FKjkpomf0cqnnkh16u4nh0genne` (`shift_id`),
  CONSTRAINT `FK1r0jj5y4y0eo1eunt4uqraskh` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKjkpomf0cqnnkh16u4nh0genne` FOREIGN KEY (`shift_id`) REFERENCES `core_setting_shift` (`id`),
  CONSTRAINT `FKpovjf4y82vo0tyji2uw3xdiju` FOREIGN KEY (`section_id`) REFERENCES `core_setting_section` (`id`),
  CONSTRAINT `FKr78pem42uomlhegag23x8at6j` FOREIGN KEY (`class_id`) REFERENCES `core_setting_class` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=735 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_class_configuration`
--

LOCK TABLES `core_setting_class_configuration` WRITE;
/*!40000 ALTER TABLE `core_setting_class_configuration` DISABLE KEYS */;
INSERT INTO `core_setting_class_configuration` VALUES (2,3,1,2,1,2,2),(3,2,1,3,1,2,2),(4,3,1,4,1,2,2),(5,4,1,5,1,2,2),(21,0,1,4,1,3,2),(249,0,1,129,1,2,2),(250,0,1,129,1,3,2),(322,0,1,146,1,2,2),(323,0,1,146,1,3,3),(324,0,1,148,1,185,21),(325,0,1,148,1,185,22),(326,0,1,149,1,186,23),(734,0,1,299,1,2,2);
/*!40000 ALTER TABLE `core_setting_class_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_designation`
--

DROP TABLE IF EXISTS `core_setting_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_designation` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKsxfduy92rn6fd0o97dkq7tfe2` (`name`,`institute_id`),
  KEY `FK1upn95qimx3oq0wgy6w2yvx4g` (`institute_id`),
  CONSTRAINT `FK1upn95qimx3oq0wgy6w2yvx4g` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_designation`
--

LOCK TABLES `core_setting_designation` WRITE;
/*!40000 ALTER TABLE `core_setting_designation` DISABLE KEYS */;
INSERT INTO `core_setting_designation` VALUES (2,'Principal',1,1,1),(4,'Asst. Principal ',2,1,1),(87,'Senior Teacher',3,1,1),(100,'Developer',4,1,1);
/*!40000 ALTER TABLE `core_setting_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_exam`
--

DROP TABLE IF EXISTS `core_setting_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_exam` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `default_id` int DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKemv0my03oimi1ph63ijk1ejg8` (`name`,`institute_id`),
  KEY `FKppmbxi52nxjsuqgl4sim584oj` (`institute_id`),
  CONSTRAINT `FKppmbxi52nxjsuqgl4sim584oj` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_exam`
--

LOCK TABLES `core_setting_exam` WRITE;
/*!40000 ALTER TABLE `core_setting_exam` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_setting_exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_fee_subhead`
--

DROP TABLE IF EXISTS `core_setting_fee_subhead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_fee_subhead` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK2ohgtcemkrq05gh5thr6dbmpn` (`name`,`institute_id`),
  KEY `FKb80e1l8pw5mj1dbit2ow2qv5v` (`institute_id`),
  CONSTRAINT `FKb80e1l8pw5mj1dbit2ow2qv5v` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_fee_subhead`
--

LOCK TABLES `core_setting_fee_subhead` WRITE;
/*!40000 ALTER TABLE `core_setting_fee_subhead` DISABLE KEYS */;
INSERT INTO `core_setting_fee_subhead` VALUES (1,'January',1,1,1),(2,'February',2,1,1),(3,'March',3,1,1),(4,'April',4,1,1),(5,'May',5,1,1),(6,'June',6,1,1),(7,'July',7,1,1),(8,'August',8,1,1),(9,'Septembar',9,1,1),(10,'October',10,1,1),(11,'November',11,1,1),(12,'December',12,1,1),(13,'New Admission',13,1,1),(14,'Old Admission',14,1,1),(15,'Session Charge',15,1,1),(16,'Sports Fee',16,1,1),(17,'Scouts Fee',18,1,1),(18,'Hostel Fee',19,1,1),(20,'1st Term Exam',18,1,1),(21,'2nd Term Exam',22,1,1),(22,'Final Exam',23,1,1);
/*!40000 ALTER TABLE `core_setting_fee_subhead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_fee_waiver`
--

DROP TABLE IF EXISTS `core_setting_fee_waiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_fee_waiver` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKhc3dlghkfwhrum5e01kftapho` (`name`,`institute_id`),
  KEY `FKbvs8abnwgevgthgo4rxqq5nhb` (`institute_id`),
  CONSTRAINT `FKbvs8abnwgevgthgo4rxqq5nhb` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_fee_waiver`
--

LOCK TABLES `core_setting_fee_waiver` WRITE;
/*!40000 ALTER TABLE `core_setting_fee_waiver` DISABLE KEYS */;
INSERT INTO `core_setting_fee_waiver` VALUES (1,'Poor Waiver',1,1,1),(2,'Merit Waiver',2,0,1),(4,'Special Waiver',3,1,1);
/*!40000 ALTER TABLE `core_setting_fee_waiver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_feehead`
--

DROP TABLE IF EXISTS `core_setting_feehead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_feehead` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `default_id` int DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKadn7jrt9hw6hk2cllx9w55uep` (`name`,`institute_id`),
  KEY `FK58co1gwofae6i2p8sj6xeo3x` (`institute_id`),
  CONSTRAINT `FK58co1gwofae6i2p8sj6xeo3x` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_feehead`
--

LOCK TABLES `core_setting_feehead` WRITE;
/*!40000 ALTER TABLE `core_setting_feehead` DISABLE KEYS */;
INSERT INTO `core_setting_feehead` VALUES (1,100,'Absent Fine',50,1,1),(2,NULL,'Tuition Fee',1,1,1),(3,NULL,'Admission Fee',2,1,1),(4,NULL,'Session Fee',3,1,1),(5,NULL,'Exam Fee',4,1,1),(6,NULL,'Sports Fee',5,1,1),(7,NULL,'Labratory Fee',6,1,1),(8,NULL,'Misselinious Fee',7,1,1);
/*!40000 ALTER TABLE `core_setting_feehead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_group`
--

DROP TABLE IF EXISTS `core_setting_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_group` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKpaswcvgk864ksx6ojw9ek4moi` (`name`,`institute_id`),
  KEY `FKj2gxu2uin2hi6u60lu8fbmj0g` (`institute_id`),
  CONSTRAINT `FKj2gxu2uin2hi6u60lu8fbmj0g` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_group`
--

LOCK TABLES `core_setting_group` WRITE;
/*!40000 ALTER TABLE `core_setting_group` DISABLE KEYS */;
INSERT INTO `core_setting_group` VALUES (2,'General',2,1,1),(3,'N/A',1,1,1),(4,'Science',3,1,1),(6,'Commerce',7,1,1),(7,'Humanities',5,1,1),(8,'Business Studies',4,1,1),(91,'Madrasah',6,1,1),(92,'shapla',8,1,1);
/*!40000 ALTER TABLE `core_setting_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_group_configuration`
--

DROP TABLE IF EXISTS `core_setting_group_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_group_configuration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `application_fee` int DEFAULT NULL,
  `serial` int DEFAULT NULL,
  `service_charge` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `total_fee` int DEFAULT NULL,
  `class_id` bigint NOT NULL,
  `group_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKs822pjck1qn7vjgplgkjhw9je` (`class_id`,`group_id`,`institute_id`),
  KEY `FKmiv5vch7fhel8lv2pscnpq1m1` (`group_id`),
  KEY `FK8qjqw38995hqvug21hf6vdrly` (`institute_id`),
  CONSTRAINT `FK4t15dua2y672upl2u7un2ieqe` FOREIGN KEY (`class_id`) REFERENCES `core_setting_class` (`id`),
  CONSTRAINT `FK8qjqw38995hqvug21hf6vdrly` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKmiv5vch7fhel8lv2pscnpq1m1` FOREIGN KEY (`group_id`) REFERENCES `core_setting_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=591 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_group_configuration`
--

LOCK TABLES `core_setting_group_configuration` WRITE;
/*!40000 ALTER TABLE `core_setting_group_configuration` DISABLE KEYS */;
INSERT INTO `core_setting_group_configuration` VALUES (2,200,1,20,1,220,2,3,1),(3,200,2,20,1,220,3,3,1),(4,200,3,20,1,220,4,2,1),(5,200,4,20,1,220,5,4,1),(8,200,5,20,1,220,5,8,1),(165,NULL,0,20,1,20,129,3,1),(219,NULL,0,20,1,20,5,7,1),(220,NULL,0,20,1,20,5,6,1),(221,NULL,0,20,1,20,148,3,1),(222,NULL,0,20,1,20,149,92,1);
/*!40000 ALTER TABLE `core_setting_group_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_period`
--

DROP TABLE IF EXISTS `core_setting_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_period` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `default_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKk4yr4nf48e0yew9bjyx757x0` (`name`,`institute_id`),
  UNIQUE KEY `UKat5csg22my27d47ucxmspcmi0` (`default_id`,`institute_id`),
  KEY `FKjj9ifxfu6efkd4ij3k3th5efi` (`institute_id`),
  CONSTRAINT `FKjj9ifxfu6efkd4ij3k3th5efi` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_period`
--

LOCK TABLES `core_setting_period` WRITE;
/*!40000 ALTER TABLE `core_setting_period` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_setting_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_section`
--

DROP TABLE IF EXISTS `core_setting_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_section` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKj28j4fjat0bk3was9gbaquivs` (`name`,`institute_id`),
  KEY `FK67qvdnuvsm5amtx8mkd01502u` (`institute_id`),
  CONSTRAINT `FK67qvdnuvsm5amtx8mkd01502u` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=382 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_section`
--

LOCK TABLES `core_setting_section` WRITE;
/*!40000 ALTER TABLE `core_setting_section` DISABLE KEYS */;
INSERT INTO `core_setting_section` VALUES (2,'A',1,1,1),(3,'B',2,1,1),(185,'C',3,1,1),(186,'human',4,1,1),(381,'Demo',5,1,1);
/*!40000 ALTER TABLE `core_setting_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_shift`
--

DROP TABLE IF EXISTS `core_setting_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_shift` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKa3egup0pekd0eun2no876gy5c` (`name`,`institute_id`),
  KEY `FK1svtbpgc1k69e1fl2vexa72mq` (`institute_id`),
  CONSTRAINT `FK1svtbpgc1k69e1fl2vexa72mq` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_shift`
--

LOCK TABLES `core_setting_shift` WRITE;
/*!40000 ALTER TABLE `core_setting_shift` DISABLE KEYS */;
INSERT INTO `core_setting_shift` VALUES (2,'Morning',1,1,1),(3,'Day',2,1,1),(21,'Evening ',3,1,1),(22,'N/A',4,1,1),(23,'night',3,1,1),(61,'mid-',5,1,1);
/*!40000 ALTER TABLE `core_setting_shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_student_category`
--

DROP TABLE IF EXISTS `core_setting_student_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_student_category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKsbt13pf1yjpxh2c20nkjcxw2s` (`name`,`institute_id`),
  KEY `FKcahjcwc59y6vxmegjoe9y4v4w` (`institute_id`),
  CONSTRAINT `FKcahjcwc59y6vxmegjoe9y4v4w` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_student_category`
--

LOCK TABLES `core_setting_student_category` WRITE;
/*!40000 ALTER TABLE `core_setting_student_category` DISABLE KEYS */;
INSERT INTO `core_setting_student_category` VALUES (2,'General',1,1,1),(4,'N/A',2,1,1),(15,'Sunnah',3,1,1),(16,'bangla',4,1,1),(51,'english',5,1,1);
/*!40000 ALTER TABLE `core_setting_student_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_subject`
--

DROP TABLE IF EXISTS `core_setting_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_subject` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKsfg613n3xawj2alpxa0ekokn2` (`name`,`institute_id`),
  KEY `FKdumjdwpxi1ie81t193di6eoaq` (`institute_id`),
  CONSTRAINT `FKdumjdwpxi1ie81t193di6eoaq` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_subject`
--

LOCK TABLES `core_setting_subject` WRITE;
/*!40000 ALTER TABLE `core_setting_subject` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_setting_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_setting_subject_configuration`
--

DROP TABLE IF EXISTS `core_setting_subject_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `core_setting_subject_configuration` (
  `subjectconfig_id` bigint NOT NULL AUTO_INCREMENT,
  `subject_merge_id` int DEFAULT NULL,
  `subject_serial` int DEFAULT NULL,
  `subject_status` int DEFAULT NULL,
  `class_id` bigint DEFAULT NULL,
  `group_id` bigint DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  `subject_id` bigint DEFAULT NULL,
  PRIMARY KEY (`subjectconfig_id`),
  UNIQUE KEY `UK2st3fut8uhr1l0vxiabr6n123` (`subject_id`,`class_id`,`group_id`),
  KEY `FKs89k17yi1t7tmeidkfvvpgp2o` (`class_id`),
  KEY `FK15jd0su9s54i7osketcffr59s` (`group_id`),
  KEY `FK28ri65qudk2kkd914q2ld3emk` (`institute_id`),
  CONSTRAINT `FK15jd0su9s54i7osketcffr59s` FOREIGN KEY (`group_id`) REFERENCES `core_setting_group` (`id`),
  CONSTRAINT `FK28ri65qudk2kkd914q2ld3emk` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FK77smie94bka37jnuk41wntywu` FOREIGN KEY (`subject_id`) REFERENCES `core_setting_subject` (`id`),
  CONSTRAINT `FKs89k17yi1t7tmeidkfvvpgp2o` FOREIGN KEY (`class_id`) REFERENCES `core_setting_class` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_setting_subject_configuration`
--

LOCK TABLES `core_setting_subject_configuration` WRITE;
/*!40000 ALTER TABLE `core_setting_subject_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_setting_subject_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `default_signature`
--

DROP TABLE IF EXISTS `default_signature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `default_signature` (
  `signature_id` bigint NOT NULL AUTO_INCREMENT,
  `sign_img_name` varchar(255) DEFAULT NULL,
  `sign_status` int NOT NULL,
  `signature_title` varchar(255) NOT NULL,
  `used_id` int NOT NULL,
  `used_name` varchar(255) DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`signature_id`),
  UNIQUE KEY `UK66bbk2tft413nrto9bgese4l3` (`institute_id`,`used_id`),
  CONSTRAINT `FKk32rabb3w5rijbg6sc5yir5qs` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_signature`
--

LOCK TABLES `default_signature` WRITE;
/*!40000 ALTER TABLE `default_signature` DISABLE KEYS */;
INSERT INTO `default_signature` VALUES (15,'1_11_signature.png',1,'HeadMaster',11,'Student ID Card',1),(17,'1_13_signature.png',1,'HeadMaster',13,'Mark Sheet Right',1),(18,'1_15_signature.png',1,'Class Teacher',15,'Mark Sheet Left',1);
/*!40000 ALTER TABLE `default_signature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_amount_configuration`
--

DROP TABLE IF EXISTS `fee_amount_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_amount_configuration` (
  `config_id` bigint NOT NULL AUTO_INCREMENT,
  `fee_amount` double DEFAULT NULL,
  `fine_amount` double DEFAULT NULL,
  `class_id` bigint NOT NULL,
  `fee_head_id` bigint NOT NULL,
  `fee_subhead_id` bigint NOT NULL,
  `group_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  `student_category_id` bigint NOT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `UK2wnmu9vqj4stu3re0m0g73cf0` (`class_id`,`group_id`,`student_category_id`,`fee_head_id`,`fee_subhead_id`,`institute_id`),
  KEY `FKgn2kcsvxdwi71o851vuf7hv4o` (`fee_head_id`),
  KEY `FK9gu9smjh0tiybtao65t3w74en` (`fee_subhead_id`),
  KEY `FKomqx2y5aj3pwyruxjuxur9s0i` (`group_id`),
  KEY `FKa5rjg1fccuigad7s5isdmkjrd` (`institute_id`),
  KEY `FKpfs7m2bsgvv2bf85gkjipy1oe` (`student_category_id`),
  CONSTRAINT `FK881mt0lqh0pfrcvpe14hpr8h3` FOREIGN KEY (`class_id`) REFERENCES `core_setting_class` (`id`),
  CONSTRAINT `FK9gu9smjh0tiybtao65t3w74en` FOREIGN KEY (`fee_subhead_id`) REFERENCES `core_setting_fee_subhead` (`id`),
  CONSTRAINT `FKa5rjg1fccuigad7s5isdmkjrd` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKgn2kcsvxdwi71o851vuf7hv4o` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKomqx2y5aj3pwyruxjuxur9s0i` FOREIGN KEY (`group_id`) REFERENCES `core_setting_group` (`id`),
  CONSTRAINT `FKpfs7m2bsgvv2bf85gkjipy1oe` FOREIGN KEY (`student_category_id`) REFERENCES `core_setting_student_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3564 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_amount_configuration`
--

LOCK TABLES `fee_amount_configuration` WRITE;
/*!40000 ALTER TABLE `fee_amount_configuration` DISABLE KEYS */;
INSERT INTO `fee_amount_configuration` VALUES (21,1200,10,3,3,13,3,1,2),(22,1100,10,3,3,14,3,1,2),(24,250,10,3,5,20,3,1,2),(25,300,10,3,5,21,3,1,2),(39,500,0,2,2,1,3,1,4),(40,500,0,2,2,2,3,1,4),(41,500,0,2,2,3,3,1,4),(42,500,0,2,2,4,3,1,4),(43,500,50,2,2,5,3,1,4),(44,500,50,2,2,6,3,1,4),(45,500,50,2,2,7,3,1,4),(46,500,50,2,2,8,3,1,4),(47,500,50,2,2,9,3,1,4),(48,500,50,2,2,10,3,1,4),(49,500,50,2,2,11,3,1,4),(50,500,50,2,2,12,3,1,4),(51,300,0,2,5,20,3,1,4),(52,400,0,2,5,21,3,1,4),(53,500,0,2,5,22,3,1,4),(54,300,0,2,5,20,3,1,2),(55,400,0,2,5,21,3,1,2),(56,500,0,2,5,22,3,1,2),(1117,5000,0,3,4,15,3,1,2),(1440,500,0,2,3,13,3,1,4),(1441,500,0,2,3,14,3,1,4),(1454,600,0,129,2,1,3,1,2),(1455,600,0,129,2,2,3,1,2),(1456,600,0,129,2,3,3,1,2),(1457,600,0,129,2,4,3,1,2),(1458,600,0,129,2,5,3,1,2),(1459,600,0,129,2,6,3,1,2),(1460,600,0,129,2,7,3,1,2),(1461,600,0,129,2,8,3,1,2),(1462,600,0,129,2,9,3,1,2),(1463,600,0,129,2,10,3,1,2),(1464,600,0,129,2,11,3,1,2),(1465,600,0,129,2,12,3,1,2),(1594,0,0,5,3,1,4,1,2),(1595,0,0,5,3,13,4,1,2),(1596,0,0,5,3,14,4,1,2),(1598,1000,125,2,4,15,3,1,2),(1611,11,0,5,2,2,4,1,2),(1612,11,0,5,2,3,4,1,2),(1613,11,0,5,2,4,4,1,2),(1614,11,0,5,2,5,4,1,2),(1615,11,0,5,2,6,4,1,2),(1616,11,0,5,2,7,4,1,2),(1617,11,0,5,2,8,4,1,2),(1618,11,0,5,2,10,4,1,2),(1619,11,0,5,2,11,4,1,2),(1620,11,0,5,2,12,4,1,2),(3419,100,0,5,2,1,6,1,2),(3420,100,0,5,2,2,6,1,2),(3421,0,0,5,2,3,6,1,2),(3422,0,0,5,2,4,6,1,2),(3423,0,0,5,2,5,6,1,2),(3424,0,0,5,2,6,6,1,2),(3425,0,0,5,2,7,6,1,2),(3426,0,0,5,2,8,6,1,2),(3427,0,0,5,2,9,6,1,2),(3428,0,0,5,2,10,6,1,2),(3429,0,0,5,2,11,6,1,2),(3430,0,0,5,2,12,6,1,2),(3528,0,0,2,2,2,3,1,2),(3529,0,0,2,2,6,3,1,2),(3530,0,0,2,2,7,3,1,2),(3531,0,0,2,2,8,3,1,2),(3532,0,0,2,2,9,3,1,2),(3533,0,0,2,2,10,3,1,2),(3534,0,0,2,2,11,3,1,2),(3535,0,0,2,2,12,3,1,2),(3536,500,0,3,2,1,3,1,4),(3537,500,0,3,2,2,3,1,4),(3538,500,0,3,2,3,3,1,4),(3539,500,0,3,2,4,3,1,4),(3540,500,0,3,2,5,3,1,4),(3541,500,0,3,2,6,3,1,4),(3542,500,0,3,2,7,3,1,4),(3543,500,0,3,2,8,3,1,4),(3544,500,0,3,2,9,3,1,4),(3545,500,0,3,2,10,3,1,4),(3546,500,0,3,2,11,3,1,4),(3547,500,0,3,2,12,3,1,4),(3553,500,100,3,2,4,3,1,2),(3554,500,100,3,2,5,3,1,2),(3555,500,100,3,2,6,3,1,2),(3556,500,100,3,2,7,3,1,2),(3557,500,100,3,2,8,3,1,2),(3558,500,100,3,2,9,3,1,2),(3559,500,100,3,2,10,3,1,2),(3560,500,100,3,2,11,3,1,2),(3561,500,50,3,2,12,3,1,2),(3562,40,0,3,6,17,3,1,2);
/*!40000 ALTER TABLE `fee_amount_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_fine_ledger_configuration`
--

DROP TABLE IF EXISTS `fee_fine_ledger_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_fine_ledger_configuration` (
  `config_id` bigint NOT NULL AUTO_INCREMENT,
  `ledger_id` bigint NOT NULL,
  `fee_head_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `UKat456amvev3v0npdsts0mo7o6` (`fee_head_id`,`institute_id`),
  KEY `FKsnnpfrjtc0ujtsjsju6pp11gr` (`ledger_id`),
  KEY `FK3ma2yt6o5cfn7l2hgetlmcgb7` (`institute_id`),
  CONSTRAINT `FK3ma2yt6o5cfn7l2hgetlmcgb7` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FK4eq2sert7yi1u5p5jfqee2vw4` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKsnnpfrjtc0ujtsjsju6pp11gr` FOREIGN KEY (`ledger_id`) REFERENCES `account_ledger` (`ledger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_fine_ledger_configuration`
--

LOCK TABLES `fee_fine_ledger_configuration` WRITE;
/*!40000 ALTER TABLE `fee_fine_ledger_configuration` DISABLE KEYS */;
INSERT INTO `fee_fine_ledger_configuration` VALUES (1,5,2,1),(2,5,4,1),(3,5,3,1),(4,5,5,1);
/*!40000 ALTER TABLE `fee_fine_ledger_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_head_ledger_configuration`
--

DROP TABLE IF EXISTS `fee_head_ledger_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_head_ledger_configuration` (
  `config_id` bigint NOT NULL AUTO_INCREMENT,
  `ledger_id` bigint NOT NULL,
  `fee_head_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `UK5juejpvy9bdf3b3bt3oldm7ru` (`fee_head_id`,`institute_id`),
  KEY `FKer4qkgpg50t1pqv02mvcqb48a` (`ledger_id`),
  KEY `FKjgd406rfwa4owmccbul1x8a9k` (`institute_id`),
  CONSTRAINT `FK36nggt4gnji2aifkifv7qfsu0` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKer4qkgpg50t1pqv02mvcqb48a` FOREIGN KEY (`ledger_id`) REFERENCES `account_ledger` (`ledger_id`),
  CONSTRAINT `FKjgd406rfwa4owmccbul1x8a9k` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_head_ledger_configuration`
--

LOCK TABLES `fee_head_ledger_configuration` WRITE;
/*!40000 ALTER TABLE `fee_head_ledger_configuration` DISABLE KEYS */;
INSERT INTO `fee_head_ledger_configuration` VALUES (1,3,2,1),(2,5,3,1),(3,56,4,1),(4,3,8,1),(5,3,5,1),(52,6,7,1),(153,6,6,1);
/*!40000 ALTER TABLE `fee_head_ledger_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_invoice`
--

DROP TABLE IF EXISTS `fee_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_invoice` (
  `master_id` bigint NOT NULL AUTO_INCREMENT,
  `create_date` date DEFAULT NULL,
  `dps_status` int DEFAULT NULL,
  `due_amount` double DEFAULT NULL,
  `execution_date` datetime DEFAULT NULL,
  `generate_type` varchar(255) DEFAULT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `online_created` int DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `paid_status` int DEFAULT NULL,
  `payable_amount` double DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `ledger_id` bigint DEFAULT NULL,
  `trn_id` bigint DEFAULT NULL,
  `identification_id` bigint DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  PRIMARY KEY (`master_id`),
  UNIQUE KEY `UK_7r6w5h1ihkdkqdma4yeibtfl2` (`invoice_id`),
  UNIQUE KEY `UK_fevnl84cnmyaluht4960ffoun` (`trn_id`),
  KEY `FKva789b80pfsxeuai0wckb9n2` (`ledger_id`),
  KEY `FKgkkmwtyxe621k6crgisvu4ram` (`identification_id`),
  KEY `FKal1wal3gl9pod4t4rfnuxa7gh` (`institute_id`),
  CONSTRAINT `FKal1wal3gl9pod4t4rfnuxa7gh` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKgkkmwtyxe621k6crgisvu4ram` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`),
  CONSTRAINT `FKr92p54r19wssc06a30wpd0303` FOREIGN KEY (`trn_id`) REFERENCES `account_transaction` (`trn_id`),
  CONSTRAINT `FKva789b80pfsxeuai0wckb9n2` FOREIGN KEY (`ledger_id`) REFERENCES `account_ledger` (`ledger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34392 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_invoice`
--

LOCK TABLES `fee_invoice` WRITE;
/*!40000 ALTER TABLE `fee_invoice` DISABLE KEYS */;
INSERT INTO `fee_invoice` VALUES (1,'2021-04-22',0,0,'2021-04-22 09:33:22','Manual','2110000001','',0,1,1,1,'2021-04-22','babai',1,1,203,1),(2,'2021-04-22',0,50,'2021-04-22 09:34:25','Manual','2110000002','',0,50,1,100,'2021-04-22','babai',1,2,203,1),(3,'2021-04-25',0,0,'2021-04-25 04:55:31','Manual','2110000003','Poor Student',0,780,1,780,'2021-04-25','babai',1,3,199,1),(7,'2021-04-25',0,0,'2021-04-25 05:05:09','Manual','2110000005','',0,1320,1,1320,'2021-04-25','babai',2,4,200,1),(8,'2021-04-25',0,0,'2021-04-25 05:28:33','Manual','2110000006','guilulli',0,900,1,900,'2021-04-25','babai',2,5,205,1),(9,'2021-04-25',0,0,'2021-04-25 05:41:00','Manual','2110000007','',0,110,1,110,'2021-04-25','babai',1,6,202,1),(10,'2021-04-25',0,1,'2021-04-25 05:42:39','Manual','2110000008','',0,1000,1,1001,'2021-04-25','babai',1,7,202,1),(11,'2021-04-25',0,0,'2021-04-25 05:44:00','Manual','2110000009','',0,1,1,1,'2021-04-25','babai',1,8,202,1),(12,'2021-04-25',0,1120,'2021-04-25 05:45:19','Manual','2110000010','',0,1140,1,2260,'2021-04-25','babai',1,9,203,1),(13,'2021-04-25',0,500,'2021-04-25 05:45:41','Manual','2110000011','',0,500,1,1000,'2021-04-25','babai',1,10,203,1),(15,'2021-04-25',1,0,'2021-04-25 07:00:10','Public Online','2110000013','Online Payment',1,50,1,50,'2021-04-25','UMME TABASSUM SUBHA',8,11,202,1),(16,'2021-04-25',0,0,'2021-04-25 07:11:25','Manual','2110000014','',0,1120,1,1120,'2021-04-25','babai',2,12,210,1),(17,'2021-04-25',0,110,'2021-04-25 07:12:34','Manual','2110000015','',0,100,1,210,'2021-04-25','babai',1,13,210,1),(18,'2021-05-19',0,0,'2021-05-19 08:41:22','Manual','2110000016','',0,500,1,500,'2021-05-19','jhonarther',1,21,2,1),(19,'2021-05-19',0,0,'2021-05-19 08:42:58','Manual','2110000017','',0,800,1,800,'2021-05-19','jhonarther',1,22,2,1),(20,'2021-05-19',0,0,'2021-05-19 08:43:15','Manual','2110000018','',0,900,1,900,'2021-05-19','jhonarther',1,23,2,1),(28,'2021-05-24',0,0,'2021-05-24 04:12:41','Manual','2110000020','',0,500,1,500,'2021-05-24','jhonarther',2,24,3,1),(29,'2021-05-24',0,200,'2021-05-24 04:30:12','Manual','2110000021','',0,1100,1,1300,'2021-05-24','babai',2,25,3,1),(30,'2021-05-24',0,500,'2021-05-24 04:30:49','Manual','2110000022','',0,1300,1,1800,'2021-05-24','babai',2,26,5,1),(31,'2021-05-24',0,0,'2021-05-24 04:41:12','Manual','2110000023','',0,1300,1,1300,'2021-05-24','babai',2,27,6,1),(32,'2021-05-24',0,0,'2021-05-24 04:42:21','Manual','2110000024','',0,1200,1,1200,'2021-05-24','babai',2,28,7,1),(33,'2021-05-24',0,0,'2021-05-24 05:32:56','Manual','2110000025','',0,1190,1,1190,'2021-05-24','babai',2,29,219,1),(34,'2021-05-24',0,0,'2021-05-24 05:35:28','Manual','2110000026','',0,1190,1,1190,'2021-05-24','babai',2,30,209,1),(35,'2021-05-24',0,0,'2021-05-24 05:36:44','Manual','2110000027','',0,1470,1,1470,'2021-05-24','babai',2,31,199,1),(36,'2021-05-24',0,0,'2021-05-24 05:37:42','Manual','2110000028','',0,1300,1,1300,'2021-05-24','babai',2,32,12,1),(37,'2021-05-24',0,0,'2021-05-24 05:39:03','Manual','2110000029','',0,1100,1,1100,'2021-05-24','babai',2,33,13,1),(38,'2021-05-25',0,0,'2021-05-25 04:37:37','Manual','2110000030','',0,1900,1,1900,'2021-05-25','jhonarther',1,34,3,1),(39,'2021-05-27',1,0,'2021-05-27 03:33:10','Public Online','2110000031','Online Payment',1,350,1,350,'2021-05-27','MARZIA JAHANGIR',8,38,3,1),(40,'2021-05-27',0,0,'2021-05-27 04:24:26','Manual','2110000032','',0,350,1,350,'2021-05-27','jhonarther',1,39,202,1),(41,'2021-05-27',0,0,'2021-05-27 05:01:38','Manual','2110000033','',0,1300,1,1300,'2021-05-27','jhonarther',1,40,574,1),(42,'2021-05-27',1,0,'2021-05-27 06:19:56','Public Online','2110000034','Online Payment',1,350,1,350,'2021-05-27','MARZIA JAHANGIR',8,41,3,1),(60,'2021-05-31',0,0,'2021-05-31 10:23:37','Manual','2110000038','',0,350,1,350,'2021-05-31','Atiqur Rahman',1,42,3,1),(64,'2021-06-01',1,0,'2021-06-01 03:52:04','Public Online','2110000039','Online Payment',1,500,1,500,'2021-06-01','RUBAIYA AKHTER',8,44,10,1),(70,'2021-06-06',0,0,'2021-06-06 06:04:13','Manual','2110000041','',0,200,1,200,'2021-06-06','babai',1,46,3,1),(71,'2021-06-08',1,0,'2021-06-08 08:26:32','Public Online','2110000042','Online Payment',1,400,1,400,'2021-06-08','Mst. Eta Moni',9,48,11,1),(78,'2021-06-15',1,0,'2021-06-15 04:37:39','Public Online','2110000046','Online Payment',1,500,1,500,'2021-06-15','Sumi Akter',8,54,579,1),(84,'2021-06-22',0,0,'2021-06-22 04:18:51','Manual','2110000050','',0,400,1,400,'2021-06-22','Atiqur Rahman',1,55,11,1),(96,'2021-06-24',1,0,'2021-06-24 04:20:47','Public Online','2110000055','Online Payment',1,2800,1,2800,'2021-06-24','TAWHIDA TASNIM',8,56,216,1),(97,'2021-06-24',1,0,'2021-06-24 04:23:21','Public Online','2110000056','Online Payment',1,7500,1,7500,'2021-06-24','ZANNATUN NESA PREMA',8,57,218,1),(111,'2021-06-24',1,0,'2021-06-24 05:25:21','Public Online','2110000060','Online Payment',1,400,1,400,'2021-06-24','MEHENAZ AFRIN MEDHA',8,58,209,1),(123,'2021-06-24',1,0,'2021-06-24 05:43:09','Public Online','2110000063','Online Payment',1,900,1,900,'2021-06-24','MRITTIKA HAQUE SREYA',8,59,8,1),(125,'2021-06-24',1,0,'2021-06-24 05:43:22','Public Online','2110000065','Online Payment',1,500,1,500,'2021-06-24','TRISHA MONDAL',8,60,217,1),(150,'2021-06-24',1,0,'2021-06-24 10:46:03','Public Online','2110000069','Online Payment',1,500,1,500,'2021-06-24','FARHANA MAHI',8,61,224,1),(152,'2021-06-27',1,0,'2021-06-27 04:05:54','Public Online','2110000070','Online Payment',1,400,1,400,'2021-06-27','Porshi Rani Saha',8,62,6,1),(154,'2021-06-27',1,0,'2021-06-27 07:12:34','Public Online','2110000071','Online Payment',1,500,1,500,'2021-06-27','MRITTIKA HAQUE SREYA',8,63,8,1),(155,'2021-06-27',1,0,'2021-06-27 07:27:52','Public Online','2110000072','Online Payment',1,500,1,500,'2021-06-27','MRITTIKA HAQUE SREYA',8,64,8,1),(160,'2021-06-28',1,0,'2021-06-28 05:21:00','Public Online','2110000074','Online Payment',1,400,1,400,'2021-06-28','Porshi Rani Saha',8,68,6,1),(180,'2021-06-28',1,0,'2021-06-28 07:20:19','Public Online','2110000076','Online Payment',1,500,1,500,'2021-06-28','MRITTIKA HAQUE SREYA',8,72,8,1),(208,'2021-07-04',0,0,'2021-07-04 03:26:00','Manual','2110000079','',0,3000,1,3000,'2021-07-04','jhonarther',1,97,199,1),(209,'2021-07-05',1,0,'2021-07-05 06:09:09','Public Online','2110000080','Online Payment',1,500,1,500,'2021-07-05','ZANNATUN NESA PREMA',8,98,218,1),(218,'2021-07-05',1,0,'2021-07-05 10:49:42','Public Online','2110000085','Online Payment',1,300,1,300,'2021-07-05','MEHENAZ AFRIN MEDHA',8,101,209,1),(222,'2021-07-09',1,0,'2021-07-09 09:27:18','Public Online','2110000087','Online Payment',1,15,1,15,'2021-07-09','MISS. LABONNO AKTER',8,103,227,1),(226,'2021-07-09',1,0,'2021-07-09 09:31:35','Public Online','2110000088','Online Payment',1,15,1,15,'2021-07-09','MST. IFAT ARA (ETHINA)',8,104,219,1),(1746,'2021-08-08',0,0,'2021-08-08 06:56:25','Manual','2110000095','',0,1300,1,1300,'2021-08-08','01750316386',2,426,575,1),(2449,'2021-08-19',0,200,'2021-08-19 09:06:07','Manual','2110000101','',0,1800,1,2000,'2021-08-19','01951901919',1,833,575,1),(2876,'2021-09-02',1,0,'2021-09-02 04:35:09','Public Online','2110000103','Online Payment',1,15,1,15,'2021-09-02','Online',8,1008,219,1),(2877,'2021-09-02',1,0,'2021-09-02 04:46:08','Public Online','2110000104','Online Payment',1,15,1,15,'2021-09-02','Online',8,1009,271,1),(3547,'2021-09-05',0,0,'2021-09-05 12:00:50','Manual','2110000107','',0,400,1,400,'2021-09-05','babai',1,1203,3,1),(3548,'2021-09-05',0,0,'2021-09-05 12:05:06','Manual','2110000108','',0,400,1,400,'2021-09-05','babai',1,1204,6,1),(4152,'2021-09-06',1,0,'2021-09-06 17:07:18','Public Online','2110000109','Online Payment',1,15,1,15,'2021-09-06','Online',8,1400,235,1),(4752,'2021-09-08',0,0,'2021-09-08 03:54:31','Manual','2110000111','',0,600,1,600,'2021-09-08','01917217779',1,1634,13818,1),(7436,'2021-09-14',1,0,'2021-09-14 07:04:33','Public Online','2110000118','Online Payment',1,15,1,15,'2021-09-14','Online',8,2769,217,1),(9582,'2021-09-28',1,0,'2021-09-28 07:09:01','Public Online','2110000131','Online Payment',1,100,1,100,'2021-09-28','Online',8,4021,258,1),(10395,'2021-09-28',1,0,'2021-09-28 17:58:15','Public Online','2110000133','Online Payment',1,15,1,15,'2021-09-28','Online',8,4173,253,1),(13436,'2021-10-04',1,0,'2021-10-04 08:16:07','Public Online','2110000139','Online Payment',1,15,1,15,'2021-10-04','Online',8,5821,216,1),(15134,'2021-10-05',1,0,'2021-10-05 08:40:16','Public Online','2110000141','Online Payment',1,15,1,15,'2021-10-05','Online',8,6940,211,1),(17069,'2021-10-12',1,0,'2021-10-12 04:52:25','Public Online','2110000152','Online Payment',1,15,0,15,NULL,'Online',8,NULL,2,1),(20167,'2021-10-24',0,0,'2021-10-24 11:09:23','Manual','2110000155','',0,15,1,15,'2021-10-24','arif',2,11004,210,1),(20245,'2021-10-25',0,0,'2021-10-25 11:09:36','Manual','2110000156','',0,5260,1,5260,'2021-10-25','arif',1,11243,200,1),(20246,'2021-10-25',0,245,'2021-10-25 11:10:05','Manual','2110000157','',0,200,1,445,'2021-10-25','arif',1,11244,200,1),(20970,'2021-10-26',0,0,'2021-10-26 10:10:59','Manual','2110000161','',0,1125,1,1125,'2021-10-26','01750316386',1,11425,210,1),(23176,'2021-11-02',1,0,'2021-11-02 23:14:25','OFPS','2110000164','Online Fees Payment',1,15,1,15,'2021-11-02','online',8,13161,253,1),(23177,'2021-11-02',1,0,'2021-11-02 23:18:32','OFPS','2110000165','Online Fees Payment',1,30,1,30,'2021-11-02','online',8,13162,253,1),(28627,'2021-11-09',1,0,'2021-11-09 12:12:33','OFPS','2110000166','Online Fees Payment',1,15,1,15,'2021-11-09','online',8,18908,211,1),(34375,'2022-01-06',0,0,'2022-01-06 17:05:15','Manual','2210000001','Fees Collection From Payslip',0,2000,1,2000,'2022-01-06','jhonarther',1,25645,566,1),(34376,'2022-01-06',0,0,'2022-01-06 17:05:15','Manual','2210000002','Fees Collection From Payslip',0,2000,1,2000,'2022-01-06','jhonarther',1,25646,567,1),(34377,'2022-01-12',0,0,'2022-01-12 14:27:10','Manual','2210000003','Fees Collection From Payslip',0,1000,1,1000,'2022-01-12','jhonarther',1,25647,49834,1),(34378,'2022-01-13',0,30,'2022-01-13 10:37:06','Manual','2210000004','Fees Collection From Payslip',0,150,1,180,'2022-01-13','jhonarther',1,25648,242,1),(34379,'2022-01-16',0,0,'2022-01-16 11:19:52','Manual','2210000005','Fees Collection From Payslip',0,344.96,1,344.96,'2022-01-16','jhonarther',1,25649,198,1),(34381,'2022-01-25',0,0,'2022-01-25 17:03:19','Manual','2210000006','123654',0,15,1,15,'2022-01-25','jhonarther',1,25651,199,1),(34382,'2022-01-30',0,0,'2022-01-30 12:56:30','Manual','2210000007','',0,210,1,210,'2022-01-30','jhonarther',1,25652,201,1),(34383,'2022-02-02',0,3,'2022-02-02 12:27:27','Manual','2210000008','',0,30,1,33,'2022-02-02','01917217779',14,25653,380,1),(34386,'2022-02-02',0,0,'2022-02-02 16:42:10','Manual','2210000009','Fees Collection From Payslip',0,44,1,44,'2022-02-02','01917217779',1,25657,380,1),(34387,'2022-02-13',0,0,'2022-02-13 10:35:59','Manual','2210000010','',0,33,1,33,'2022-02-13','arif',2,25658,380,1),(34388,'2022-02-13',0,210,'2022-02-13 10:37:29','Manual','2210000011','',0,6000,1,6210,'2022-02-13','arif',2,25659,220,1),(34389,'2022-02-13',0,0,'2022-02-13 10:38:16','Manual','2210000012','',0,2010,1,2010,'2022-02-13','arif',1,25660,220,1),(34390,'2022-02-13',0,0,'2022-02-13 10:39:05','Manual','2210000013','',0,2880,1,2880,'2022-02-13','arif',1,25661,220,1),(34391,'2022-02-13',0,0,'2022-02-13 10:40:58','Manual','2210000014','',0,1240,1,1240,'2022-02-13','arif',1,25662,220,1);
/*!40000 ALTER TABLE `fee_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_invoice_backup`
--

DROP TABLE IF EXISTS `fee_invoice_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_invoice_backup` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `create_date` date DEFAULT NULL,
  `dps_status` int DEFAULT NULL,
  `due_amount` double DEFAULT NULL,
  `execution_date` datetime DEFAULT NULL,
  `generate_type` varchar(255) DEFAULT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `master_id` bigint DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `paid_status` int DEFAULT NULL,
  `payable_amount` double DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `ledger_id` bigint DEFAULT NULL,
  `trn_id` bigint DEFAULT NULL,
  `identification_id` bigint DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_oa9870fk5s1f358chbgapxe7s` (`master_id`),
  KEY `FK872sisepjbbbrjqxevjbj2tik` (`ledger_id`),
  KEY `FKtj54dtj07vcjorum04nv0ti8o` (`trn_id`),
  KEY `FKodfev9t10alhrt2wkj1vob7uw` (`identification_id`),
  KEY `FKoh0xh2aoff8f1qmgk4vx7bku3` (`institute_id`),
  CONSTRAINT `FK872sisepjbbbrjqxevjbj2tik` FOREIGN KEY (`ledger_id`) REFERENCES `account_ledger` (`ledger_id`),
  CONSTRAINT `FKodfev9t10alhrt2wkj1vob7uw` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`),
  CONSTRAINT `FKoh0xh2aoff8f1qmgk4vx7bku3` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKtj54dtj07vcjorum04nv0ti8o` FOREIGN KEY (`trn_id`) REFERENCES `account_transaction` (`trn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_invoice_backup`
--

LOCK TABLES `fee_invoice_backup` WRITE;
/*!40000 ALTER TABLE `fee_invoice_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_invoice_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_invoice_details`
--

DROP TABLE IF EXISTS `fee_invoice_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_invoice_details` (
  `details_id` bigint NOT NULL AUTO_INCREMENT,
  `due_amount` double DEFAULT NULL,
  `due_paid` double DEFAULT NULL,
  `due_waiver_amount` double DEFAULT NULL,
  `fee_paid` double DEFAULT NULL,
  `fee_waiver_amount` double DEFAULT NULL,
  `fine_paid` double DEFAULT NULL,
  `invoice_id` varchar(255) DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `payable_amount` double DEFAULT NULL,
  `required_amount` double DEFAULT NULL,
  `waiver_note` varchar(255) DEFAULT NULL,
  `fee_head_id` bigint NOT NULL,
  `master_id` bigint DEFAULT NULL,
  `fee_sub_head_id` bigint DEFAULT NULL,
  `identification_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  `waiver_id` bigint DEFAULT NULL,
  PRIMARY KEY (`details_id`),
  UNIQUE KEY `UKe9npu50muuh7xg912jtlcbiwu` (`institute_id`,`fee_head_id`,`fee_sub_head_id`,`identification_id`),
  KEY `FKj5abns65jio2k3mcodnjp1dbn` (`fee_head_id`),
  KEY `FKqlj0oekofw80jao6qfgi907st` (`master_id`),
  KEY `FK796d5gfklecdj67a2xmoeie7r` (`fee_sub_head_id`),
  KEY `FK3cbgi76v7lbycwxuu27r92jjj` (`identification_id`),
  KEY `FK5jgyt0vto2yijwfv0cqfw8mre` (`waiver_id`),
  CONSTRAINT `FK3cbgi76v7lbycwxuu27r92jjj` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`),
  CONSTRAINT `FK5jgyt0vto2yijwfv0cqfw8mre` FOREIGN KEY (`waiver_id`) REFERENCES `core_setting_fee_waiver` (`id`),
  CONSTRAINT `FK796d5gfklecdj67a2xmoeie7r` FOREIGN KEY (`fee_sub_head_id`) REFERENCES `core_setting_fee_subhead` (`id`),
  CONSTRAINT `FKj5abns65jio2k3mcodnjp1dbn` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKqlj0oekofw80jao6qfgi907st` FOREIGN KEY (`master_id`) REFERENCES `fee_invoice` (`master_id`),
  CONSTRAINT `FKrwwwb17i7ei9kuix5b8xnby3c` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42898 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_invoice_details`
--

LOCK TABLES `fee_invoice_details` WRITE;
/*!40000 ALTER TABLE `fee_invoice_details` DISABLE KEYS */;
INSERT INTO `fee_invoice_details` VALUES (1,0,0,0,1,99,0,'2110000001',1,1,1,'',2,1,1,203,1,1),(2,50,0,0,50,0,0,'2110000002',50,100,100,'',2,2,2,203,1,1),(3,0,0,0,75,25,10,'2110000003',85,85,85,'',2,3,1,199,1,1),(4,0,0,0,75,25,10,'2110000003',85,85,85,'',2,3,2,199,1,1),(5,0,0,0,600,500,10,'2110000003',610,610,610,'',3,3,14,199,1,1),(10,0,0,0,100,0,10,'2110000005',110,110,110,'',2,7,1,200,1,1),(11,0,0,0,1200,0,10,'2110000005',1210,1210,1210,'',3,7,13,200,1,1),(12,0,0,0,56.66666666666667,43.333333333333336,10,'2110000006',66.66666666666667,66.66666666666667,66.66666666666667,'',2,8,1,205,1,1),(13,0,0,0,56.66666666666667,43.333333333333336,10,'2110000006',66.66666666666667,66.66666666666667,66.66666666666667,'',2,8,2,205,1,1),(14,0,0,0,56.66666666666667,43.333333333333336,10,'2110000006',66.66666666666667,66.66666666666667,66.66666666666667,'',2,8,3,205,1,1),(15,0,0,0,690,510,10,'2110000006',700,700,700,'',3,8,13,205,1,1),(16,0,0,0,100,0,10,'2110000007',110,110,110,'',2,9,1,202,1,1),(17,1,0,0,0,999,1000,'2110000008',1000,1001,1001,'',4,10,15,202,1,1),(18,0,1,0,0,0,0,'2110000009',1,0,1,'',4,11,NULL,202,1,1),(19,55,0,0,45,0,5,'2110000010',50,105,105,'',2,12,3,203,1,1),(20,55,0,0,45,0,5,'2110000010',50,105,105,'',2,12,4,203,1,1),(21,1000,0,0,0,0,1000,'2110000010',1000,2000,2000,'',4,12,15,203,1,1),(22,10,40,0,0,0,0,'2110000010',40,0,50,'',2,12,NULL,203,1,1),(23,500,500,0,0,0,0,'2110000011',500,0,1000,'',4,13,NULL,203,1,1),(26,0,0,0,50,50,0,'2110000013',50,50,50,NULL,2,15,2,202,1,1),(27,0,0,0,50,50,10,'2110000014',60,60,60,'',2,16,1,210,1,1),(28,0,0,0,50,50,10,'2110000014',60,60,60,'',2,16,2,210,1,1),(29,0,0,0,990,210,10,'2110000014',1000,1000,1000,'',3,16,13,210,1,1),(30,55,0,0,45,0,5,'2110000015',50,105,105,'',2,17,3,210,1,1),(31,55,0,0,45,0,5,'2110000015',50,105,105,'',2,17,4,210,1,1),(32,0,0,0,500,0,0,'2110000016',500,500,500,'',2,18,1,2,1,1),(33,0,0,0,500,0,0,'2110000017',500,500,500,'',2,19,2,2,1,1),(34,0,0,0,300,0,0,'2110000017',300,300,300,'',5,19,20,2,1,1),(35,0,0,0,500,0,0,'2110000018',500,500,500,'',2,20,3,2,1,1),(36,0,0,0,400,0,0,'2110000018',400,400,400,'',5,20,21,2,1,1),(44,0,0,0,500,0,0,'2110000020',500,500,500,'',2,28,1,3,1,1),(45,100,0,0,400,0,0,'2110000021',400,500,500,'',2,29,3,3,1,1),(46,100,0,0,400,0,0,'2110000021',400,500,500,'',2,29,4,3,1,1),(47,0,0,0,300,0,0,'2110000021',300,300,300,'',5,29,20,3,1,1),(48,166.66666666666669,0,0,333.3333333333333,0,0,'2110000022',333.3333333333333,500,500,'',2,30,1,5,1,1),(49,166.66666666666669,0,0,333.3333333333333,0,0,'2110000022',333.3333333333333,500,500,'',2,30,2,5,1,1),(50,166.66666666666669,0,0,333.3333333333333,0,0,'2110000022',333.3333333333333,500,500,'',2,30,3,5,1,1),(51,0,0,0,300,0,0,'2110000022',300,300,300,'',5,30,20,5,1,1),(52,0,0,0,500,0,0,'2110000023',500,500,500,'',2,31,1,6,1,1),(53,0,0,0,500,0,0,'2110000023',500,500,500,'',2,31,2,6,1,1),(54,0,0,0,300,0,0,'2110000023',300,300,300,'',5,31,20,6,1,1),(55,0,0,0,333.3333333333333,166.66666666666666,0,'2110000024',333.3333333333333,333.3333333333333,333.3333333333333,'',2,32,1,7,1,4),(56,0,0,0,333.3333333333333,166.66666666666666,0,'2110000024',333.3333333333333,333.3333333333333,333.3333333333333,'',2,32,2,7,1,4),(57,0,0,0,333.3333333333333,166.66666666666666,0,'2110000024',333.3333333333333,333.3333333333333,333.3333333333333,'',2,32,3,7,1,4),(58,0,0,0,200,100,0,'2110000024',200,200,200,'',5,32,20,7,1,1),(59,0,0,0,100,0,10,'2110000025',110,110,110,'',2,33,1,219,1,1),(60,0,0,0,100,0,10,'2110000025',110,110,110,'',2,33,2,219,1,1),(61,0,0,0,700,500,10,'2110000025',710,710,710,'',3,33,13,219,1,1),(62,0,0,0,250,0,10,'2110000025',260,260,260,'',5,33,20,219,1,1),(63,0,0,0,100,0,10,'2110000026',110,110,110,'',2,34,1,209,1,1),(64,0,0,0,100,0,10,'2110000026',110,110,110,'',2,34,2,209,1,1),(65,0,0,0,700,400,10,'2110000026',710,710,710,'',3,34,14,209,1,1),(66,0,0,0,250,0,10,'2110000026',260,260,260,'',5,34,20,209,1,1),(67,0,0,0,100,0,10,'2110000027',110,110,110,'',2,35,3,199,1,1),(68,0,0,0,100,900,1000,'2110000027',1100,1100,1100,'',4,35,15,199,1,1),(69,0,0,0,250,0,10,'2110000027',260,260,260,'',5,35,20,199,1,1),(70,0,0,0,500,0,0,'2110000028',500,500,500,'',2,36,1,12,1,1),(71,0,0,0,500,0,0,'2110000028',500,500,500,'',2,36,2,12,1,1),(72,0,0,0,300,0,0,'2110000028',300,300,300,'',5,36,20,12,1,1),(73,0,0,0,400,100,0,'2110000029',400,400,400,'',2,37,1,13,1,2),(74,0,0,0,400,100,0,'2110000029',400,400,400,'',2,37,2,13,1,2),(75,0,0,0,300,0,0,'2110000029',300,300,300,'',5,37,20,13,1,1),(76,0,0,0,500,0,0,'2110000030',500,500,500,'',2,38,6,3,1,1),(77,0,0,0,500,0,0,'2110000030',500,500,500,'',2,38,7,3,1,1),(78,0,0,0,450,0,0,'2110000030',450,450,450,'',5,38,21,3,1,1),(79,0,0,0,450,0,0,'2110000030',450,450,450,'',5,38,22,3,1,1),(80,0,0,0,350,150,0,'2110000031',350,350,350,NULL,2,39,10,3,1,1),(81,0,0,0,80,20,10,'2110000032',90,90,90,'',2,40,3,202,1,1),(82,0,0,0,250,0,10,'2110000032',260,260,260,'',5,40,20,202,1,1),(83,0,0,0,500,0,0,'2110000033',500,500,500,'',2,41,1,574,1,1),(84,0,0,0,500,0,0,'2110000033',500,500,500,'',2,41,2,574,1,1),(85,0,0,0,300,0,0,'2110000033',300,300,300,'',5,41,20,574,1,1),(86,0,0,0,350,150,0,'2110000034',350,350,350,NULL,2,42,8,3,1,1),(107,0,0,0,350,150,0,'2110000038',350,350,350,'',2,60,5,3,1,1),(115,0,0,0,500,0,0,'2110000039',500,500,500,NULL,2,64,6,10,1,NULL),(121,0,200,0,0,0,0,'2110000041',200,0,200,'',2,70,NULL,3,1,1),(122,0,0,0,400,100,0,'2110000042',400,400,400,NULL,2,71,1,11,1,1),(130,0,0,0,500,0,0,'2110000046',500,500,500,NULL,2,78,3,579,1,NULL),(136,0,0,0,400,100,0,'2110000050',400,400,400,'',2,84,2,11,1,1),(149,0,0,0,500,0,0,'2110000055',500,500,500,NULL,2,96,1,216,1,NULL),(150,0,0,0,1100,0,0,'2110000055',1100,1100,1100,NULL,3,96,14,216,1,NULL),(151,0,0,0,1200,0,0,'2110000055',1200,1200,1200,NULL,3,96,13,216,1,NULL),(152,0,0,0,7000,0,0,'2110000056',7000,7000,7000,NULL,4,97,15,218,1,NULL),(153,0,0,0,500,0,0,'2110000056',500,500,500,NULL,2,97,2,218,1,NULL),(168,0,0,0,400,100,0,'2110000060',400,400,400,NULL,2,111,4,209,1,1),(179,0,0,0,400,0,0,'2110000063',400,400,400,NULL,5,123,21,8,1,NULL),(180,0,0,0,500,0,0,'2110000063',500,500,500,NULL,2,123,8,8,1,NULL),(182,0,0,0,500,0,0,'2110000065',500,500,500,NULL,2,125,3,217,1,NULL),(216,0,0,0,500,0,0,'2110000069',500,500,500,NULL,2,150,5,224,1,NULL),(218,0,0,0,400,100,0,'2110000070',400,400,400,NULL,2,152,5,6,1,1),(220,0,0,0,500,0,0,'2110000071',500,500,500,NULL,2,154,1,8,1,NULL),(221,0,0,0,500,0,0,'2110000072',500,500,500,NULL,2,155,3,8,1,NULL),(226,0,0,0,400,100,0,'2110000074',400,400,400,NULL,2,160,6,6,1,1),(246,0,0,0,500,0,0,'2110000076',500,500,500,NULL,2,180,5,8,1,NULL),(287,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,4,199,1,1),(288,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,5,199,1,1),(289,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,6,199,1,1),(290,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,7,199,1,1),(291,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,8,199,1,1),(292,0,0,0,500,0,0,'2110000079',500,500,500,'',2,208,9,199,1,1),(293,0,0,0,500,0,0,'2110000080',500,500,500,NULL,2,209,5,218,1,NULL),(303,0,0,0,300,0,0,'2110000085',300,300,300,NULL,5,218,21,209,1,NULL),(307,0,0,0,15,0,0,'2110000087',15,15,15,NULL,2,222,3,227,1,NULL),(312,0,0,0,15,0,0,'2110000088',15,15,15,NULL,2,226,3,219,1,NULL),(2900,0,0,0,500,0,0,'2110000095',500,500,500,'',2,1746,2,575,1,1),(2901,0,0,0,500,0,0,'2110000095',500,500,500,'',2,1746,3,575,1,1),(2902,0,0,0,300,0,0,'2110000095',300,300,300,'',5,1746,20,575,1,1),(3993,66.66666666666669,0,0,433.3333333333333,0,0,'2110000101',433.3333333333333,500,500,'',2,2449,7,575,1,4),(3994,66.66666666666669,0,0,433.3333333333333,0,0,'2110000101',433.3333333333333,500,500,'',2,2449,8,575,1,4),(3995,66.66666666666669,0,0,433.3333333333333,0,0,'2110000101',433.3333333333333,500,500,'',2,2449,9,575,1,4),(3996,0,0,0,500,0,0,'2110000101',500,500,500,'',5,2449,22,575,1,1),(4651,0,0,0,15,0,0,'2110000103',15,15,15,NULL,2,2876,6,219,1,NULL),(4652,0,0,0,15,0,0,'2110000104',15,15,15,NULL,2,2877,2,271,1,NULL),(5311,0,0,0,400,100,0,'2110000107',400,400,400,'',2,3547,9,3,1,1),(5312,0,0,0,400,100,0,'2110000108',400,400,400,'',2,3548,3,6,1,1),(5911,0,0,0,15,0,0,'2110000109',15,15,15,NULL,2,4152,2,235,1,NULL),(6504,0,0,0,600,0,0,'2110000111',600,600,600,'',2,4752,2,13818,1,1),(9105,0,0,0,15,0,0,'2110000118',15,15,15,NULL,2,7436,7,217,1,NULL),(11418,0,0,0,100,0,0,'2110000131',100,100,100,NULL,2,9582,1,258,1,NULL),(12227,0,0,0,15,0,0,'2110000133',15,15,15,NULL,2,10395,6,253,1,NULL),(14888,0,0,0,15,0,0,'2110000139',15,15,15,NULL,2,13436,3,216,1,NULL),(16547,0,0,0,15,0,0,'2110000141',15,15,15,NULL,2,15134,3,211,1,NULL),(18920,0,0,0,15,0,0,'2110000152',15,15,15,NULL,2,17069,10,2,1,NULL),(24830,0,0,0,15,0,0,'2110000155',15,15,15,'',2,20167,5,210,1,1),(25108,0,0,0,5000,0,0,'2110000156',5000,5000,5000,'',4,20245,15,200,1,1),(25109,0,0,0,250,0,10,'2110000156',260,260,260,'',5,20245,20,200,1,1),(25110,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,2,200,1,1),(25111,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,3,200,1,1),(25112,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,4,200,1,1),(25113,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,5,200,1,1),(25114,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,6,200,1,1),(25115,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,7,200,1,1),(25116,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,8,200,1,1),(25117,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,9,200,1,1),(25118,3.8888888888888893,0,0,11.11111111111111,0,0,'2110000157',11.11111111111111,15,15,'',2,20246,11,200,1,1),(25119,210,0,0,90,0,10,'2110000157',100,310,310,'',5,20246,21,200,1,1),(26063,0,0,0,15,0,0,'2110000161',15,15,15,'',2,20970,6,210,1,1),(26064,0,0,0,1100,0,10,'2110000161',1110,1110,1110,'',3,20970,14,210,1,1),(29458,0,0,0,15,0,0,'2110000164',15,15,15,'N/A',2,23176,5,253,1,NULL),(29459,0,0,0,15,0,0,'2110000165',15,15,15,'N/A',2,23177,8,253,1,NULL),(29460,0,0,0,15,0,0,'2110000165',15,15,15,'N/A',2,23177,7,253,1,NULL),(35320,0,0,0,15,0,0,'2110000166',15,15,15,'N/A',2,28627,5,211,1,NULL),(42840,0,0,0,500,0,50,'2210000001',550,550,550,'',2,34375,10,566,1,NULL),(42841,0,0,0,500,0,50,'2210000001',550,550,550,'',2,34375,9,566,1,NULL),(42842,0,0,0,500,0,0,'2210000001',500,500,500,'',5,34375,22,566,1,NULL),(42843,0,0,0,400,0,0,'2210000001',400,400,400,'',5,34375,21,566,1,NULL),(42844,0,0,0,500,0,50,'2210000002',550,550,550,'',2,34376,10,567,1,NULL),(42845,0,0,0,400,0,0,'2210000002',400,400,400,'',5,34376,21,567,1,NULL),(42846,0,0,0,500,0,50,'2210000002',550,550,550,'',2,34376,9,567,1,NULL),(42847,0,0,0,500,0,0,'2210000002',500,500,500,'',5,34376,22,567,1,NULL),(42848,0,0,0,500,0,0,'2210000003',500,500,500,'',2,34377,1,49834,1,NULL),(42849,0,0,0,500,0,0,'2210000003',500,500,500,'',2,34377,2,49834,1,NULL),(42850,10,0,0,33.33,0,16.67,'2210000004',50,60,60,'',2,34378,1,242,1,1),(42851,10,0,0,33.33,0,16.67,'2210000004',50,60,60,'',2,34378,2,242,1,1),(42852,10,0,0,33.33,0,16.67,'2210000004',50,60,60,'',2,34378,3,242,1,1),(42853,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,3,198,1,1),(42854,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,1,198,1,1),(42855,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,9,198,1,1),(42856,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,6,198,1,1),(42857,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,11,198,1,1),(42858,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,4,198,1,1),(42859,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,7,198,1,1),(42860,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,2,198,1,1),(42861,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,8,198,1,1),(42862,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,10,198,1,1),(42863,0,0,0,22.72,0,8.64,'2210000005',31.36,31.36,31.36,'',2,34379,5,198,1,1),(42865,0,0,0,15,0,0,'2210000006',15,15,15,'',2,34381,10,199,1,1),(42866,0,0,0,32,0,10,'2210000007',42,42,42,'',2,34382,1,201,1,1),(42867,0,0,0,32,0,10,'2210000007',42,42,42,'',2,34382,2,201,1,1),(42868,0,0,0,32,0,10,'2210000007',42,42,42,'',2,34382,7,201,1,1),(42869,0,0,0,32,0,10,'2210000007',42,42,42,'',2,34382,8,201,1,1),(42870,0,0,0,32,0,10,'2210000007',42,42,42,'',2,34382,4,201,1,1),(42871,1,0,0,10,0,0,'2210000008',10,11,11,'',2,34383,3,380,1,1),(42872,1,0,0,10,0,0,'2210000008',10,11,11,'',2,34383,4,380,1,1),(42873,1,0,0,10,0,0,'2210000008',10,11,11,'',2,34383,6,380,1,1),(42877,0,0,0,11,0,0,'2210000009',11,11,11,'',2,34386,5,380,1,1),(42878,0,0,0,11,0,0,'2210000009',11,11,11,'',2,34386,8,380,1,1),(42879,0,0,0,11,0,0,'2210000009',11,11,11,'',2,34386,12,380,1,1),(42880,0,0,0,11,0,0,'2210000009',11,11,11,'',2,34386,11,380,1,1),(42881,0,0,0,11,0,0,'2210000010',11,11,11,'',2,34387,2,380,1,1),(42882,0,0,0,11,0,0,'2210000010',11,11,11,'',2,34387,7,380,1,1),(42883,0,0,0,11,0,0,'2210000010',11,11,11,'',2,34387,10,380,1,1),(42884,210,0,0,990,0,10,'2210000011',1000,1210,1210,'',3,34388,13,220,1,1),(42885,0,0,0,5000,0,0,'2210000011',5000,5000,5000,'',4,34388,15,220,1,1),(42886,0,0,0,500,0,100,'2210000012',600,600,600,'',2,34389,5,220,1,1),(42887,0,0,0,500,0,100,'2210000012',600,600,600,'',2,34389,6,220,1,1),(42888,0,0,0,500,0,100,'2210000012',600,600,600,'',2,34389,7,220,1,1),(42889,0,210,0,0,0,0,'2210000012',210,0,210,'',3,34389,NULL,220,1,1),(42890,0,0,0,500,0,100,'2210000013',600,600,600,'',2,34390,4,220,1,1),(42891,0,0,0,500,0,100,'2210000013',600,600,600,'',2,34390,8,220,1,1),(42892,0,0,0,1100,0,10,'2210000013',1110,1110,1110,'',3,34390,14,220,1,1),(42893,0,0,0,275,0,10,'2210000013',285,285,285,'',5,34390,20,220,1,1),(42894,0,0,0,275,0,10,'2210000013',285,285,285,'',5,34390,21,220,1,1),(42895,0,0,0,500,0,100,'2210000014',600,600,600,'',2,34391,9,220,1,1),(42896,0,0,0,500,0,100,'2210000014',600,600,600,'',2,34391,10,220,1,1),(42897,0,0,0,40,0,0,'2210000014',40,40,40,'',6,34391,17,220,1,1);
/*!40000 ALTER TABLE `fee_invoice_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_invoice_details_backup`
--

DROP TABLE IF EXISTS `fee_invoice_details_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_invoice_details_backup` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `details_id` bigint DEFAULT NULL,
  `due_amount` double DEFAULT NULL,
  `due_paid` double DEFAULT NULL,
  `due_waiver_amount` double DEFAULT NULL,
  `fee_paid` double DEFAULT NULL,
  `fee_waiver_amount` double DEFAULT NULL,
  `fine_paid` double DEFAULT NULL,
  `invoice_id` varchar(255) DEFAULT NULL,
  `master_id` bigint DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `payable_amount` double DEFAULT NULL,
  `required_amount` double DEFAULT NULL,
  `waiver_note` varchar(255) DEFAULT NULL,
  `fee_head_id` bigint NOT NULL,
  `backup_id` bigint DEFAULT NULL,
  `fee_sub_head_id` bigint DEFAULT NULL,
  `identification_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  `waiver_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpb4k15a79ba68sdw9ove993s5` (`fee_head_id`),
  KEY `FK5rtf0auvusd3wtru337cv6tm7` (`backup_id`),
  KEY `FKooranpm9rgyf26iydo7b3prkg` (`fee_sub_head_id`),
  KEY `FKq99mr0r3mknbsiwr3lfj7rdk9` (`identification_id`),
  KEY `FKq0ni1x5h83cuk46t8issfl1v4` (`institute_id`),
  KEY `FK1yr6ye8phtaxphjsv38ero5tq` (`waiver_id`),
  CONSTRAINT `FK1yr6ye8phtaxphjsv38ero5tq` FOREIGN KEY (`waiver_id`) REFERENCES `core_setting_fee_waiver` (`id`),
  CONSTRAINT `FK5rtf0auvusd3wtru337cv6tm7` FOREIGN KEY (`backup_id`) REFERENCES `fee_invoice_backup` (`id`),
  CONSTRAINT `FKooranpm9rgyf26iydo7b3prkg` FOREIGN KEY (`fee_sub_head_id`) REFERENCES `core_setting_fee_subhead` (`id`),
  CONSTRAINT `FKpb4k15a79ba68sdw9ove993s5` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKq0ni1x5h83cuk46t8issfl1v4` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKq99mr0r3mknbsiwr3lfj7rdk9` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_invoice_details_backup`
--

LOCK TABLES `fee_invoice_details_backup` WRITE;
/*!40000 ALTER TABLE `fee_invoice_details_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_invoice_details_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_subhead_configuration`
--

DROP TABLE IF EXISTS `fee_subhead_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_subhead_configuration` (
  `config_id` bigint NOT NULL AUTO_INCREMENT,
  `fee_head_id` bigint NOT NULL,
  `fee_subhead_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `UKcc9v6pjmsf9txoam0qu86w3iq` (`fee_head_id`,`fee_subhead_id`,`institute_id`),
  KEY `FKf411lx30jvhnk4q5r62nmx6u5` (`fee_subhead_id`),
  KEY `FKni4goiqo2aj5jhsitiodf3xu7` (`institute_id`),
  CONSTRAINT `FK2upwg3w3pq2y37pe5m85fbam7` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKf411lx30jvhnk4q5r62nmx6u5` FOREIGN KEY (`fee_subhead_id`) REFERENCES `core_setting_fee_subhead` (`id`),
  CONSTRAINT `FKni4goiqo2aj5jhsitiodf3xu7` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=487 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_subhead_configuration`
--

LOCK TABLES `fee_subhead_configuration` WRITE;
/*!40000 ALTER TABLE `fee_subhead_configuration` DISABLE KEYS */;
INSERT INTO `fee_subhead_configuration` VALUES (416,2,1,1),(417,2,2,1),(418,2,3,1),(419,2,4,1),(134,2,5,1),(135,2,6,1),(136,2,7,1),(137,2,8,1),(138,2,9,1),(139,2,10,1),(140,2,11,1),(141,2,12,1),(246,3,1,1),(23,3,13,1),(24,3,14,1),(420,4,15,1),(25,5,20,1),(26,5,21,1),(27,5,22,1),(16,6,16,1),(17,6,17,1),(35,8,16,1);
/*!40000 ALTER TABLE `fee_subhead_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_subhead_time_configuration`
--

DROP TABLE IF EXISTS `fee_subhead_time_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_subhead_time_configuration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `executed_date` datetime DEFAULT NULL,
  `fee_active_date` date DEFAULT NULL,
  `fine_active_date` date DEFAULT NULL,
  `payable_month` varchar(255) NOT NULL,
  `payable_year` int NOT NULL,
  `fee_head_id` bigint NOT NULL,
  `fee_sub_head_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKidqrslw7leh1yl76cvwidie2i` (`institute_id`,`fee_head_id`,`fee_sub_head_id`,`payable_year`),
  KEY `FKqu05pf44egy1bufo2rdcr5ihw` (`fee_head_id`),
  KEY `FKha1co5pyvsk6x33lxhlvm18ba` (`fee_sub_head_id`),
  CONSTRAINT `FKb9gs07aualg74f11m35k1u2os` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKha1co5pyvsk6x33lxhlvm18ba` FOREIGN KEY (`fee_sub_head_id`) REFERENCES `core_setting_fee_subhead` (`id`),
  CONSTRAINT `FKqu05pf44egy1bufo2rdcr5ihw` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_subhead_time_configuration`
--

LOCK TABLES `fee_subhead_time_configuration` WRITE;
/*!40000 ALTER TABLE `fee_subhead_time_configuration` DISABLE KEYS */;
INSERT INTO `fee_subhead_time_configuration` VALUES (15,'2021-04-25 03:55:17','2021-03-01','2021-03-31','March',2021,2,3,1),(16,'2021-04-25 03:55:17','2021-04-01','2021-04-30','April',2021,2,4,1),(17,'2021-04-25 03:55:17','2021-05-01','2021-05-31','May',2021,2,5,1),(18,'2021-04-25 03:55:17','2021-06-01','2021-06-30','June',2021,2,6,1),(19,'2021-04-25 03:55:17','2021-07-01','2021-07-31','July',2021,2,7,1),(20,'2021-04-25 03:55:17','2021-08-01','2021-08-31','August',2021,2,8,1),(21,'2021-04-25 03:55:17','2021-09-01','2021-09-30','September',2021,2,9,1),(22,'2021-04-25 03:55:17','2021-10-01','2021-10-31','October',2021,2,10,1),(23,'2021-04-25 03:55:17','2021-11-01','2021-11-30','November',2021,2,11,1),(28,'2021-04-25 04:05:28','2021-04-01','2021-04-30','April',2021,5,20,1),(29,'2021-04-25 04:05:28','2021-08-01','2021-08-31','August',2021,5,21,1),(30,'2021-04-25 04:05:28','2021-11-01','2021-11-30','November',2021,5,22,1),(38,'2021-06-13 10:43:54','2020-02-01','2020-02-29','February',2020,2,1,1),(109,'2021-09-05 12:02:19','2021-09-01','2021-09-30','September',2021,3,13,1),(110,'2021-09-05 12:03:43','2021-09-01','2021-09-30','September',2021,3,14,1),(125,'2021-10-04 18:59:59','2021-04-01','2021-04-30','April',2021,4,15,1),(350,'2021-11-30 12:18:50','2021-12-01','2021-12-31','December',2021,2,1,1),(351,'2021-11-30 12:18:50','2021-12-01','2021-12-31','December',2021,2,2,1);
/*!40000 ALTER TABLE `fee_subhead_time_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_waiver_configuration`
--

DROP TABLE IF EXISTS `fee_waiver_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_waiver_configuration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `config_date` date DEFAULT NULL,
  `waiver_amount` double DEFAULT NULL,
  `fee_head_id` bigint DEFAULT NULL,
  `fee_waiver_id` bigint DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  `identification_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKajua1lbj3p3myjmaekoio49dg` (`institute_id`,`fee_head_id`,`identification_id`),
  KEY `FKeemnfww2dlui6iyn7gem6crkj` (`fee_head_id`),
  KEY `FKpmqv0ws9m40d43uuy1sjubcp1` (`fee_waiver_id`),
  KEY `FKfara70r2h8rqv58k0gs18wc9c` (`identification_id`),
  CONSTRAINT `FKabtcg821yxni24o1a2thfv7am` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKeemnfww2dlui6iyn7gem6crkj` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FKfara70r2h8rqv58k0gs18wc9c` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`),
  CONSTRAINT `FKpmqv0ws9m40d43uuy1sjubcp1` FOREIGN KEY (`fee_waiver_id`) REFERENCES `core_setting_fee_waiver` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=487 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_waiver_configuration`
--

LOCK TABLES `fee_waiver_configuration` WRITE;
/*!40000 ALTER TABLE `fee_waiver_configuration` DISABLE KEYS */;
INSERT INTO `fee_waiver_configuration` VALUES (27,'2021-05-27',150,2,1,1,488),(33,'2021-06-05',100,2,1,1,6),(34,'2021-06-05',100,2,1,1,9),(35,'2021-06-05',100,2,1,1,11),(36,'2021-07-15',50,2,1,1,319),(37,'2021-07-15',50,2,1,1,321),(38,'2021-07-15',50,2,1,1,322),(39,'2021-07-15',50,2,1,1,324),(40,'2021-08-29',200,3,2,1,4),(41,'2021-08-29',200,3,2,1,5),(45,'2021-09-07',100,2,2,1,5),(46,'2021-09-07',100,2,2,1,7),(284,'2021-10-04',32,2,1,1,3),(285,'2021-10-04',32,2,1,1,484),(483,'2021-11-24',1000,4,1,1,581),(484,'2021-11-24',1000,4,1,1,199),(485,'2021-11-24',50,3,2,1,199),(486,'2022-02-10',100,2,4,1,224);
/*!40000 ALTER TABLE `fee_waiver_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institute_info`
--

DROP TABLE IF EXISTS `institute_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `institute_info` (
  `institute_id` bigint NOT NULL,
  `academic_year` int DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `institute_email` varchar(255) DEFAULT NULL,
  `institute_name` varchar(255) DEFAULT NULL,
  `institute_status` int DEFAULT NULL,
  `institute_type` int DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `online_admission_service_charge` int DEFAULT NULL,
  `online_admission_status` int DEFAULT NULL,
  `bill_cycle` varchar(255) DEFAULT NULL,
  `package_id` int DEFAULT NULL,
  `eiin_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`institute_id`),
  UNIQUE KEY `UK_h5444r9bs7e6yj6tqljrp7o93` (`eiin_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institute_info`
--

LOCK TABLES `institute_info` WRITE;
/*!40000 ALTER TABLE `institute_info` DISABLE KEYS */;
INSERT INTO `institute_info` VALUES (1,2021,'Mirpur','Jhon Arther','0191397807','2021-01-01 04:04:04','1.png','she@amgaisl.coam','Basabo High School',1,6,'','2021-01-01 00:00:00',20,1,'Monthly',2,'2543');
/*!40000 ALTER TABLE `institute_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_district`
--

DROP TABLE IF EXISTS `location_district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location_district` (
  `district_id` int NOT NULL,
  `district_name` varchar(255) DEFAULT NULL,
  `division_id` int DEFAULT NULL,
  PRIMARY KEY (`district_id`),
  UNIQUE KEY `UK_g407khfibnie2u9xrfhs5832g` (`district_name`),
  KEY `FK4v8ybmfardskguo08652flinb` (`division_id`),
  CONSTRAINT `FK4v8ybmfardskguo08652flinb` FOREIGN KEY (`division_id`) REFERENCES `location_division` (`division_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_district`
--

LOCK TABLES `location_district` WRITE;
/*!40000 ALTER TABLE `location_district` DISABLE KEYS */;
INSERT INTO `location_district` VALUES (11,'Barguna',1),(12,'Barishal',1),(13,'Bhola',1),(14,'Jhalokati',1),(15,'Patuakhali',1),(16,'Pirojpur',1),(17,'Bandarban',2),(18,'Brahmanbaria',2),(19,'Chandpur',2),(20,'Chattogram',2),(21,'Cumilla',2),(22,'Cox\'S Bazar',2),(23,'Feni',2),(24,'Khagrachhari',2),(25,'Lakshmipur',2),(26,'Noakhali',2),(27,'Rangamati',2),(28,'Dhaka',3),(29,'Faridpur',3),(30,'Gazipur',3),(31,'Gopalganj',3),(32,'Kishoregonj',3),(33,'Madaripur',3),(34,'Manikganj',3),(35,'Munshiganj',3),(36,'Narayanganj',3),(37,'Narsingdi',3),(38,'Rajbari',3),(39,'Shariatpur',3),(40,'Tangail',3),(41,'Bagerhat',4),(42,'Chuadanga',4),(43,'Jeshore',4),(44,'Jhenaidah',4),(45,'Khulna',4),(46,'Kushtia',4),(47,'Magura',4),(48,'Meherpur',4),(49,'Narail',4),(50,'Satkhira',4),(51,'Jamalpur',5),(52,'Mymensingh',5),(53,'Netrakona',5),(54,'Sherpur',5),(55,'Bogura',6),(56,'Joypurhat',6),(57,'Naogaon',6),(58,'Natore',6),(59,'Chapainawabganj',6),(60,'Pabna',6),(61,'Rajshahi',6),(62,'Sirajganj',6),(63,'Dinajpur',7),(64,'Gaibandha',7),(65,'Kurigram',7),(66,'Lalmonirhat',7),(67,'Nilphamari',7),(68,'Panchagarh',7),(69,'Rangpur',7),(70,'Thakurgaon',7),(71,'Habiganj',8),(72,'Maulvibazar',8),(73,'Sunamganj',8),(74,'Sylhet',8);
/*!40000 ALTER TABLE `location_district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_division`
--

DROP TABLE IF EXISTS `location_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location_division` (
  `division_id` int NOT NULL,
  `division_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`division_id`),
  UNIQUE KEY `UK_13kxiv845r6af5wbl121o2fli` (`division_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_division`
--

LOCK TABLES `location_division` WRITE;
/*!40000 ALTER TABLE `location_division` DISABLE KEYS */;
INSERT INTO `location_division` VALUES (1,'Barishal'),(2,'Chattogram'),(3,'Dhaka'),(4,'Khulna'),(5,'Mymensingh'),(6,'Rajshahi'),(7,'Rangpur'),(8,'Sylhet');
/*!40000 ALTER TABLE `location_division` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_post_office`
--

DROP TABLE IF EXISTS `location_post_office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location_post_office` (
  `post_office_id` int NOT NULL,
  `post_office_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `thana_id` int DEFAULT NULL,
  PRIMARY KEY (`post_office_id`),
  KEY `FKptif7gr258s71cwtjumehk9a6` (`thana_id`),
  CONSTRAINT `FKptif7gr258s71cwtjumehk9a6` FOREIGN KEY (`thana_id`) REFERENCES `location_thana` (`thana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_post_office`
--

LOCK TABLES `location_post_office` WRITE;
/*!40000 ALTER TABLE `location_post_office` DISABLE KEYS */;
/*!40000 ALTER TABLE `location_post_office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_thana`
--

DROP TABLE IF EXISTS `location_thana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location_thana` (
  `thana_id` int NOT NULL,
  `thana_name` varchar(255) DEFAULT NULL,
  `district_id` int DEFAULT NULL,
  PRIMARY KEY (`thana_id`),
  KEY `FKcqpxlbk5l8qddtfrjbwbyq0ad` (`district_id`),
  CONSTRAINT `FKcqpxlbk5l8qddtfrjbwbyq0ad` FOREIGN KEY (`district_id`) REFERENCES `location_district` (`district_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_thana`
--

LOCK TABLES `location_thana` WRITE;
/*!40000 ALTER TABLE `location_thana` DISABLE KEYS */;
INSERT INTO `location_thana` VALUES (81,'Amtali',11),(82,'Bamna',11),(83,'Barguna Sadar',11),(84,'Betagi',11),(85,'Patharghata',11),(86,'Taltali',11),(87,'Agailjhara',12),(88,'Babuganj',12),(89,'Bakerganj',12),(90,'Banari Para',12),(91,'Gaurnadi',12),(92,'Hizla',12),(93,'Barishal Sadar (Kotwali)',12),(94,'Mhendiganj',12),(95,'Muladi',12),(96,'Wazirpur',12),(97,'Bhola Sadar',13),(98,'Burhanuddin',13),(99,'Char Fasson',13),(100,'Daulat Khan',13),(101,'Lalmohan',13),(102,'Manpura',13),(103,'Tazumuddin',13),(104,'Jhalokati Sadar',14),(105,'Kanthalia',14),(106,'Nalchity',14),(107,'Rajapur',14),(108,'Bauphal',15),(109,'Dashmina',15),(110,'Dumki',15),(111,'Galachipa',15),(112,'Kalapara',15),(113,'Mirzaganj',15),(114,'Patuakhali Sadar',15),(115,'Rangabali',15),(116,'Bhandaria',16),(117,'Kawkhali',16),(118,'Mathbaria',16),(119,'Nazirpur',16),(120,'Pirojpur Sadar',16),(121,'Nesarabad (Swarupkati)',16),(122,'Zianagar',16),(123,'Alikadam',17),(124,'Bandarban Sadar',17),(125,'Lama',17),(126,'Naikhongchhari',17),(127,'Rowangchhari',17),(128,'Ruma',17),(129,'Thanchi',17),(130,'Akhaura',18),(131,'Ashuganj',18),(132,'Banchharampur',18),(133,'Bijoynagar',18),(134,'Brahmanbaria Sadar',18),(135,'Kasba',18),(136,'Nabinagar',18),(137,'Nasirnagar',18),(138,'Sarail',18),(139,'Chandpur Sadar',19),(140,'Faridganj',19),(141,'Haim Char',19),(142,'Hajiganj',19),(143,'Kachua',19),(144,'Matlab Dakshin',19),(145,'Matlab Uttar',19),(146,'Shahrasti',19),(147,'Anowara',20),(148,'Bayejid Bostami',20),(149,'Banshkhali',20),(150,'Bakalia',20),(151,'Boalkhali',20),(152,'Chandanaish',20),(153,'Chandgaon',20),(154,'Chattogram Port',20),(155,'Double Mooring',20),(156,'Fatikchhari',20),(157,'Halishahar',20),(158,'Hathazari',20),(159,'Kotwali',20),(160,'Khulshi',20),(161,'Lohagara',20),(162,'Mirsharai',20),(163,'Pahartali',20),(164,'Panchlaish',20),(165,'Patiya',20),(166,'Patenga',20),(167,'Rangunia',20),(168,'Raozan',20),(169,'Sandwip',20),(170,'Satkania',20),(171,'Sitakunda',20),(172,'Barura',21),(173,'Brahman Para',21),(174,'Burichang',21),(175,'Chandina',21),(176,'Chauddagram',21),(177,'Cumilla Sadar Dakshin',21),(178,'Daudkandi',21),(179,'Debidwar',21),(180,'Homna',21),(181,'Cumilla Adarsha Sadar',21),(182,'Laksam',21),(183,'Monohorgonj',21),(184,'Meghna',21),(185,'Muradnagar',21),(186,'Nangalkot',21),(187,'Titas',21),(188,'Chakaria',22),(189,'Cox\'S Bazar Sadar',22),(190,'Kutubdia',22),(191,'Maheshkhali',22),(192,'Pekua',22),(193,'Ramu',22),(194,'Teknaf',22),(195,'Ukhia',22),(196,'Chhagalnaiya',23),(197,'Daganbhuiyan',23),(198,'Feni Sadar',23),(199,'Fulgazi',23),(200,'Parshuram',23),(201,'Sonagazi',23),(202,'Dighinala',24),(203,'Khagrachhari Sadar',24),(204,'Lakshmichhari',24),(205,'Mahalchhari',24),(206,'Manikchhari',24),(207,'Matiranga',24),(208,'Panchhari',24),(209,'Ramgarh',24),(210,'Kamalnagar',25),(211,'Lakshmipur Sadar',25),(212,'Raipur',25),(213,'Ramganj',25),(214,'Ramgati',25),(215,'Begumganj',26),(216,'Chatkhil',26),(217,'Companiganj',26),(218,'Hatiya',26),(219,'Kabirhat',26),(220,'Senbagh',26),(221,'Sonaimuri',26),(222,'Subarnachar',26),(223,'Noakhali Sadar',26),(224,'Baghaichhari',27),(225,'Barkal Upazila',27),(226,'Kawkhali (Betbunia)',27),(227,'Belai Chhari',27),(228,'Kaptai',27),(229,'Jurai Chhari',27),(230,'Langadu',27),(231,'Naniarchar',27),(232,'Rajasthali',27),(233,'Rangamati Sadar',27),(234,'Adabor',28),(235,'Badda',28),(236,'Bangshal',28),(237,'Biman Bandar',28),(238,'Banani',28),(239,'Bhasan Tek',28),(240,'Bhatara',28),(241,'Cantonment',28),(242,'Chak Bazar',28),(243,'Dakshinkhan',28),(244,'Darus Salam',28),(245,'Demra',28),(246,'Dhamrai',28),(247,'Dhanmondi',28),(248,'Dohar',28),(249,'Gendaria',28),(250,'Gulshan',28),(251,'Hazaribagh',28),(252,'Jatrabari',28),(253,'Kafrul',28),(254,'Kadamtali',28),(255,'Kalabagan',28),(256,'Keraniganj',28),(257,'Kamrangir Char',28),(258,'Khilgaon',28),(259,'Khilkhet',28),(260,'Kotwali',28),(261,'Lalbagh',28),(262,'Mirpur',28),(263,'Mohammadpur',28),(264,'Motijheel',28),(265,'Mugda Para',28),(266,'Nawabganj',28),(267,'New Market',28),(268,'Pallabi',28),(269,'Paltan',28),(270,'Ramna',28),(271,'Rampura',28),(272,'Rupnagar',28),(273,'Sabujbagh',28),(274,'Savar',28),(275,'Shahjahanpur',28),(276,'Shah Ali',28),(277,'Shahbagh',28),(278,'Sher-E-Bangla Nagar',28),(279,'Shyampur',28),(280,'Sutrapur',28),(281,'Tejgaon',28),(282,'Tejgaon Ind. Area',28),(283,'Turag',28),(284,'Uttara Paschim',28),(285,'Uttara Purba',28),(286,'Uttar Khan',28),(287,'Wari',28),(288,'Alfadanga',29),(289,'Bhanga',29),(290,'Boalmari',29),(291,'Char Bhadrasan',29),(292,'Faridpur Sadar',29),(293,'Madhukhali',29),(294,'Nagarkanda',29),(295,'Sadarpur',29),(296,'Saltha',29),(297,'Gazipur Sadar',30),(298,'Kaliakair',30),(299,'Kaliganj',30),(300,'Kapasia',30),(301,'Sreepur',30),(302,'Gopalganj Sadar',31),(303,'Kashiani',31),(304,'Kotalipara',31),(305,'Muksudpur',31),(306,'Tungipara',31),(307,'Bakshiganj',51),(308,'Dewanganj',51),(309,'Islampur',51),(310,'Jamalpur Sadar',51),(311,'Madarganj',51),(312,'Melandaha',51),(313,'Sarishabari Upazila',51),(314,'Austagram',32),(315,'Bajitpur',32),(316,'Bhairab',32),(317,'Hossainpur',32),(318,'Itna',32),(319,'Karimganj',32),(320,'Katiadi',32),(321,'Kishoreganj Sadar',32),(322,'Kuliar Char',32),(323,'Mithamain',32),(324,'Nikli',32),(325,'Pakundia',32),(326,'Tarail',32),(327,'Kalkini',33),(328,'Madaripur Sadar',33),(329,'Rajoir',33),(330,'Shibchar',33),(331,'Daulatpur',34),(332,'Ghior',34),(333,'Harirampur',34),(334,'Manikganj Sadar',34),(335,'Saturia',34),(336,'Shibalaya',34),(337,'Singair',34),(338,'Gazaria',35),(339,'Lohajang',35),(340,'Munshiganj Sadar',35),(341,'Serajdikhan',35),(342,'Sreenagar',35),(343,'Tongibari',35),(344,'Bhaluka',52),(345,'Dhobaura',52),(346,'Fulbaria',52),(347,'Gaffargaon',52),(348,'Gauripur',52),(349,'Haluaghat',52),(350,'Ishwarganj',52),(351,'Mymensingh Sadar',52),(352,'Muktagachha',52),(353,'Nandail',52),(354,'Phulpur',52),(355,'Trishal',52),(356,'Araihazar',36),(357,'Sonargaon',36),(358,'Bandar',36),(359,'Narayanganj Sadar',36),(360,'Rupganj',36),(361,'Belabo',37),(362,'Manohardi',37),(363,'Narsingdi Sadar',37),(364,'Palash',37),(365,'Roypura',37),(366,'Shibpur',37),(367,'Atpara',53),(368,'Barhatta',53),(369,'Durgapur',53),(370,'Khaliajuri',53),(371,'Kalmakanda',53),(372,'Kendua',53),(373,'Madan',53),(374,'Mohanganj',53),(375,'Netrakona Sadar',53),(376,'Purbadhala',53),(377,'Baliakandi',38),(378,'Goalanda',38),(379,'Kalukhali',38),(380,'Pangsha',38),(381,'Rajbari Sadar',38),(382,'Bhedarganj',39),(383,'Damudya',39),(384,'Gosairhat',39),(385,'Naria',39),(386,'Shariatpur Sadar',39),(387,'Zanjira',39),(388,'Jhenaigati',54),(389,'Nakla',54),(390,'Nalitabari',54),(391,'Sherpur Sadar',54),(392,'Sreebardi',54),(393,'Basail',40),(394,'Bhuapur',40),(395,'Delduar',40),(396,'Dhanbari',40),(397,'Ghatail',40),(398,'Gopalpur',40),(399,'Kalihati',40),(400,'Madhupur',40),(401,'Mirzapur',40),(402,'Nagarpur',40),(403,'Sakhipur',40),(404,'Tangail Sadar',40),(405,'Bagerhat Sadar',41),(406,'Chitalmari',41),(407,'Fakirhat',41),(408,'Kachua',41),(409,'Mollahat',41),(410,'Mongla',41),(411,'Morrelganj',41),(412,'Rampal',41),(413,'Sarankhola',41),(414,'Alamdanga',42),(415,'Chuadanga Sadar',42),(416,'Damurhuda',42),(417,'Jiban Nagar',42),(418,'Abhaynagar',43),(419,'BagherPara',43),(420,'Chaugachha',43),(421,'Jhikargachha',43),(422,'Jeshore Sadar',43),(423,'Keshabpur',43),(424,'Manirampur',43),(425,'Sharsha',43),(426,'Harinakunda',44),(427,'Jhenaidah Sadar',44),(428,'Kaliganj',44),(429,'Kotchandpur',44),(430,'Maheshpur',44),(431,'Shailkupa',44),(432,'Batiaghata',45),(433,'Dacope',45),(434,'Daulatpur',45),(435,'Dumuria',45),(436,'Dighalia',45),(437,'Khalishpur',45),(438,'Khan Jahan Ali',45),(439,'Khulna Sadar',45),(440,'Koyra',45),(441,'Paikgachha',45),(442,'Phultala',45),(443,'Rupsa',45),(444,'Sonadanga',45),(445,'Terokhada',45),(446,'Bheramara',46),(447,'Daulatpur',46),(448,'Khoksa',46),(449,'Kumarkhali',46),(450,'Kushtia Sadar',46),(451,'Mirpur',46),(452,'Magura Sadar',47),(453,'Mohammadpur',47),(454,'Shalikha',47),(455,'Sreepur',47),(456,'Gangni',48),(457,'Mujib Nagar',48),(458,'Meherpur Sadar',48),(459,'Kalia',49),(460,'Lohagara',49),(461,'NarailSadar',49),(462,'Assasuni',50),(463,'Debhata',50),(464,'Kalaroa',50),(465,'Kaliganj',50),(466,'Satkhira Sadar',50),(467,'Shyamnagar',50),(468,'Tala',50),(469,'Adamdighi',55),(470,'Bogura Sadar',55),(471,'Dhunat',55),(472,'Dhupchanchia',55),(473,'Gabtali',55),(474,'Kahaloo',55),(475,'Nandigram',55),(476,'Sariakandi',55),(477,'Shajahanpur',55),(478,'Sherpur',55),(479,'Shibganj',55),(480,'Sonatola',55),(481,'Akkelpur',56),(482,'Joypurhat Sadar',56),(483,'Kalai',56),(484,'Khetlal',56),(485,'Panchbibi',56),(486,'Atrai',57),(487,'Badalgachhi',57),(488,'Dhamoirhat',57),(489,'Manda',57),(490,'Mahadebpur',57),(491,'NaogaonSadar',57),(492,'Niamatpur',57),(493,'Patnitala',57),(494,'Porsha',57),(495,'Raninagar',57),(496,'Sapahar',57),(497,'Bagatipara',58),(498,'Baraigram',58),(499,'Gurudaspur',58),(500,'Lalpur',58),(501,'Natore Sadar',58),(502,'Singra',58),(503,'Bholahat',59),(504,'Chapainawabganj Sadar',59),(505,'Gomastapur',59),(506,'Nachole',59),(507,'Shibganj',59),(508,'Atgharia',60),(509,'Bera',60),(510,'Bhangura',60),(511,'Chatmohar',60),(512,'Faridpur',60),(513,'Ishwardi',60),(514,'PabnaSadar',60),(515,'Santhia',60),(516,'Sujanagar',60),(517,'Bagha',61),(518,'Baghmara',61),(519,'Boalia',61),(520,'Charghat',61),(521,'Durgapur',61),(522,'Godagari',61),(523,'Matihar',61),(524,'Mohanpur',61),(525,'Paba',61),(526,'Puthia',61),(527,'Rajpara',61),(528,'Shah Makhdum',61),(529,'Tanore',61),(530,'Belkuchi',62),(531,'Chauhali',62),(532,'Kamarkhanda',62),(533,'Kazipur',62),(534,'Royganj',62),(535,'Shahjadpur',62),(536,'Sirajganj Sadar',62),(537,'Tarash',62),(538,'Ullah Para',62),(539,'Birampur',63),(540,'Birganj',63),(541,'Biral',63),(542,'Bochaganj',63),(543,'Chirirbandar',63),(544,'Dinajpur Sadar',63),(545,'Fulbari',63),(546,'Ghoraghat',63),(547,'Hakimpur',63),(548,'Kaharole',63),(549,'Khansama',63),(550,'Nawabganj',63),(551,'Parbatipur',63),(552,'Fulchhari',64),(553,'Gaibandha Sadar',64),(554,'Gobindaganj',64),(555,'Palashbari',64),(556,'Sadullapur',64),(557,'Saghata',64),(558,'Sundarganj',64),(559,'Bhurungamari',65),(560,'CharRajibpur',65),(561,'Kurigram Sadar',65),(562,'Nageshwari',65),(563,'Phulbari',65),(564,'Rajarhat',65),(565,'Raumari',65),(566,'Ulipur',65),(567,'Aditmari',66),(568,'Hatibandha',66),(569,'Kaliganj',66),(570,'Lalmonirhat Sadar',66),(571,'Patgram',66),(572,'Dimla',67),(573,'Domar',67),(574,'Jaldhaka',67),(575,'Kishoreganj',67),(576,'Nilphamari Sadar',67),(577,'Saidpur',67),(578,'Atwari',68),(579,'Boda',68),(580,'Debiganj',68),(581,'Panchagarh Sadar',68),(582,'Tentulia',68),(583,'Badarganj',69),(584,'Gangachara',69),(585,'Kaunia',69),(586,'MithaPukur',69),(587,'RangpurSadar',69),(588,'Pirgachha',69),(589,'Pirganj',69),(590,'Taraganj',69),(591,'Baliadangi',70),(592,'Haripur',70),(593,'Pirganj',70),(594,'Ranisankail',70),(595,'ThakurgaonSadar',70),(596,'Ajmiriganj',71),(597,'Bahubal',71),(598,'Baniachong',71),(599,'Chunarughat',71),(600,'HabiganjSadar',71),(601,'Lakhai',71),(602,'Madhabpur',71),(603,'Nabiganj',71),(604,'Barlekha',72),(605,'Juri',72),(606,'Kamalganj',72),(607,'Kulaura',72),(608,'MaulvibazarSadar',72),(609,'Rajnagar',72),(610,'Sreemangal',72),(611,'Bishwambarpur',73),(612,'Chhatak',73),(613,'DakshinSunamganj',73),(614,'Derai',73),(615,'Dharampasha',73),(616,'Dowarabazar',73),(617,'Jagannathpur',73),(618,'Jamalganj',73),(619,'Sulla',73),(620,'SunamganjSadar',73),(621,'Tahirpur',73),(622,'Balaganj',74),(623,'BeaniBazar',74),(624,'Bishwanath',74),(625,'Companiganj',74),(626,'DakshinSurma',74),(627,'Fenchuganj',74),(628,'Golapganj',74),(629,'Gowainghat',74),(630,'Jaintapur',74),(631,'Kanaighat',74),(632,'Sylhet Sadar',74),(633,'Zakiganj',74),(634,'Shaistaganj',71);
/*!40000 ALTER TABLE `location_thana` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `money_receipt_template`
--

DROP TABLE IF EXISTS `money_receipt_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `money_receipt_template` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `default_id` int NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_55k46b5you6ohi6sff1bddgh9` (`institute_id`),
  CONSTRAINT `FKf0ksf5iqqoql93xicvk9e89qq` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `money_receipt_template`
--

LOCK TABLES `money_receipt_template` WRITE;
/*!40000 ALTER TABLE `money_receipt_template` DISABLE KEYS */;
INSERT INTO `money_receipt_template` VALUES (1,103,1);
/*!40000 ALTER TABLE `money_receipt_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_recovery_info`
--

DROP TABLE IF EXISTS `password_recovery_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_recovery_info` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `date_executed` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_a8awleq3vjy3i7myqp8eonsae` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_recovery_info`
--

LOCK TABLES `password_recovery_info` WRITE;
/*!40000 ALTER TABLE `password_recovery_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_recovery_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_basic_info`
--

DROP TABLE IF EXISTS `staff_basic_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_basic_info` (
  `staff_id` bigint NOT NULL AUTO_INCREMENT,
  `birth_date` date DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `custom_staff_id` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `resign_date` date DEFAULT NULL,
  `staff_address` varchar(255) DEFAULT NULL,
  `staff_name` varchar(255) DEFAULT NULL,
  `staff_serial` int DEFAULT NULL,
  `staff_status` int DEFAULT NULL,
  `designation_id` bigint DEFAULT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`staff_id`),
  UNIQUE KEY `UKlt6ifl537nd4fpb80nnblachw` (`custom_staff_id`,`institute_id`),
  KEY `FKs426pt5q2xuw1ljoly7etu44x` (`designation_id`),
  KEY `FK20i2r1cop9cfgmo1x3hyweiie` (`institute_id`),
  CONSTRAINT `FK20i2r1cop9cfgmo1x3hyweiie` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKs426pt5q2xuw1ljoly7etu44x` FOREIGN KEY (`designation_id`) REFERENCES `core_setting_designation` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=600 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_basic_info`
--

LOCK TABLES `staff_basic_info` WRITE;
/*!40000 ALTER TABLE `staff_basic_info` DISABLE KEYS */;
INSERT INTO `staff_basic_info` VALUES (1,'2022-01-26','B+','900002','mehedimomin5515@gmail.com',NULL,'Murjali','Male','1_900002.jpg','01773182802','Mallori','Islam',NULL,NULL,'Murtoja Alam',1,1,4,1),(2,NULL,'A+','900003','hello',NULL,'Sekender','Male',NULL,'01672408287','Sokina begum','Islam',NULL,'werwfc','Sikder patwoary',2,1,2,1),(4,NULL,'B+','900004','email@gmail.com',NULL,'Bin Kasim','Male',NULL,'01672408287','Monowara','Islam',NULL,' sfjaskldf','Mohammad Ali ZInnah',3,1,2,1),(5,NULL,'A+','900005','www',NULL,'wwww','Male',NULL,'01672408287','eeee','Islam',NULL,' ','qqqqq',5,1,2,1),(8,NULL,'B+','900006',NULL,NULL,'Motaher Hossain','Male',NULL,'01672408287','Moriom Begum','Islam',NULL,'Dhaka Bangladesh','Motiur Rahman',6,1,NULL,1),(9,NULL,'A+','900007',NULL,NULL,'Abdul High','Male',NULL,'01672408287','Rojina begum','Islam',NULL,'Mirpur Dhaka','Md. Raju Ahmed',7,1,NULL,1),(10,NULL,'O+','900008',NULL,NULL,'Rejwan','Male',NULL,'01672408287','Marjana','Islam',NULL,'Dhanmondi','Abdur Rahim',8,1,NULL,1),(11,NULL,'AB+','900009','email@gmail.com',NULL,'Roton Miya','Female',NULL,'01672408287','Rabeya','Islam',NULL,'Dhanmondi kolabagan','Md. Jamal Nasser',9,1,2,1),(12,NULL,'AB+','900010',NULL,NULL,'Tomal ','Male',NULL,'01672408287','Promila','Islam',NULL,'sdfas','Md. Khalilur Rahman',10,1,NULL,1),(13,NULL,'O+','900011','rmial@gamil.com',NULL,'Mahtab uddin','Female',NULL,'01672408287','Rimi khatun','Islam',NULL,'Green road','Marjina khatun',11,1,4,1),(14,NULL,'AB+','900012',NULL,NULL,'SHahidul islam','Female',NULL,'01672408287','Maria Khatun','Islam',NULL,'sdv','Shaila Begum',12,1,NULL,1),(21,NULL,'AB+','900013',NULL,NULL,'Father','Male','1_900013.jpg','01672408287','Mother','Islam',NULL,'Uttora Dia bari dhaka,','Mohammad Habibur Rahman',13,1,NULL,1),(32,NULL,'AB+','900014','email@gmail.com',NULL,'Father','Male','1_900014.jpg','01672408287','Mother','Islam',NULL,'dhaka','Atiqur Rahman',14,1,4,1),(137,NULL,'B+','900015','asdf@asdfas.com',NULL,'Abu taher','Male',NULL,'01672408287','Ma','Islam',NULL,'dfg','Shoibal Hossain',15,1,4,1),(141,NULL,'B+','900016',NULL,NULL,'Md Hashem Ali','Male',NULL,'01672408287','Ferdosi Begum','Islam',NULL,'Dhaka','Md Nasir Uddin',16,1,NULL,1),(270,'1969-03-13','O+','900017','tawfiquebabu@gmail.com',NULL,'Md Azizul Haque','Male',NULL,'01672408287','Samsun Nahar','Islam',NULL,'Dhaka','Tawfiqur Rahaman',17,1,2,1),(513,NULL,'O+','900018','admin@isp.com',NULL,'F','Female','1_900018.jpg','01672408287','M','Islam',NULL,'Dhanmondi-Dhaka','Sultana Parvin',18,1,87,1),(598,NULL,'A+','900019',NULL,NULL,'ss','Male',NULL,'01672408287','dd','Islam',NULL,'jhgyuk','mr.x',19,1,NULL,1),(599,'2022-02-03','A+','900020',NULL,NULL,'SS','Male',NULL,'12345678912','SS','Islam',NULL,NULL,'Mr. T',20,1,100,1);
/*!40000 ALTER TABLE `staff_basic_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_id_template`
--

DROP TABLE IF EXISTS `staff_id_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `staff_id_template` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `barcode_value` varchar(255) DEFAULT NULL,
  `template_id` int NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKslggutrh0t27iruqekign0eag` (`institute_id`),
  CONSTRAINT `FKhmlhpcqdgm0qusc97b1euf9x2` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_id_template`
--

LOCK TABLES `staff_id_template` WRITE;
/*!40000 ALTER TABLE `staff_id_template` DISABLE KEYS */;
INSERT INTO `staff_id_template` VALUES (2,'Barcode value not found',201,1);
/*!40000 ALTER TABLE `staff_id_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_basic`
--

DROP TABLE IF EXISTS `student_basic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_basic` (
  `student_id` bigint NOT NULL AUTO_INCREMENT,
  `std_session` varchar(255) DEFAULT NULL,
  `blood_group` varchar(255) DEFAULT NULL,
  `custom_student_id` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) NOT NULL,
  `guardian_email` varchar(255) DEFAULT NULL,
  `guardian_mobile` varchar(255) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `mailimg_address` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) NOT NULL,
  `reg_date` date DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_gender` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_religion` varchar(255) NOT NULL,
  `institute_id` bigint NOT NULL,
  `permanent_address_id` int DEFAULT NULL,
  `present_address_id` int DEFAULT NULL,
  `mailing_address` varchar(255) DEFAULT NULL,
  `post_office` varchar(255) DEFAULT NULL,
  `villege` varchar(255) DEFAULT NULL,
  `thana_id` int DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `birth_certificate_no` varchar(255) DEFAULT NULL,
  `father_nid` varchar(255) DEFAULT NULL,
  `father_occupation` varchar(255) DEFAULT NULL,
  `father_photo` varchar(255) DEFAULT NULL,
  `mother_nid` varchar(255) DEFAULT NULL,
  `mother_occupation` varchar(255) DEFAULT NULL,
  `mother_photo` varchar(255) DEFAULT NULL,
  `permanent_post_office` varchar(255) DEFAULT NULL,
  `permanent_villege` varchar(255) DEFAULT NULL,
  `present_post_office` varchar(255) DEFAULT NULL,
  `present_villege` varchar(255) DEFAULT NULL,
  `permanent_thana_id` int DEFAULT NULL,
  `present_thana_id` int DEFAULT NULL,
  `father_designation` varchar(255) DEFAULT NULL,
  `father_working_place` varchar(255) DEFAULT NULL,
  `mother_designation` varchar(255) DEFAULT NULL,
  `mother_working_place` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  UNIQUE KEY `UK9q15y9y4hueax67vghylfkivr` (`custom_student_id`,`institute_id`),
  KEY `FK5624cyuuatdwsi851qs7d68ey` (`institute_id`),
  KEY `FKkic1t5meb32yjmvxj4thai7lr` (`permanent_address_id`),
  KEY `FKmaq9fbadmil0bi2n6j4w665k6` (`present_address_id`),
  KEY `FKopupo5oggieu6jqd8af7cyr8w` (`thana_id`),
  KEY `FKbbsvci8efsx381mn8wbtkik6k` (`permanent_thana_id`),
  KEY `FKclwx47vk4tuvkcqvmvilc1c87` (`present_thana_id`),
  CONSTRAINT `FK5624cyuuatdwsi851qs7d68ey` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FKbbsvci8efsx381mn8wbtkik6k` FOREIGN KEY (`permanent_thana_id`) REFERENCES `location_thana` (`thana_id`),
  CONSTRAINT `FKclwx47vk4tuvkcqvmvilc1c87` FOREIGN KEY (`present_thana_id`) REFERENCES `location_thana` (`thana_id`),
  CONSTRAINT `FKkic1t5meb32yjmvxj4thai7lr` FOREIGN KEY (`permanent_address_id`) REFERENCES `location_post_office` (`post_office_id`),
  CONSTRAINT `FKmaq9fbadmil0bi2n6j4w665k6` FOREIGN KEY (`present_address_id`) REFERENCES `location_post_office` (`post_office_id`),
  CONSTRAINT `FKopupo5oggieu6jqd8af7cyr8w` FOREIGN KEY (`thana_id`) REFERENCES `location_thana` (`thana_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50078 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_basic`
--

LOCK TABLES `student_basic` WRITE;
/*!40000 ALTER TABLE `student_basic` DISABLE KEYS */;
INSERT INTO `student_basic` VALUES (2,'2020-2021','A+','1','Swapon Kumer Poddar',NULL,'01773182802','1_1.jpg',NULL,'Sraboni  Rani Mondal','2021-04-20','2021-07-02','Female','Shajuti poddar sruti','Hinduism',1,NULL,NULL,NULL,'Bamna',NULL,87,'2352546',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Dhka Post','Dhk','Bamna',NULL,NULL,87,'Star','Dhaka',NULL,NULL),(3,NULL,'AB-','2357821','MD. JAHANGIR ALAM',NULL,'01672408287','1_2357821.jpg',NULL,'MST. SARMIN SULTANA','2021-04-20','2021-01-01','Female','MARZIA JAHANGIR','Islam',1,NULL,NULL,NULL,'Fnaulo',NULL,81,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Fnaulo',NULL,NULL,81,NULL,NULL,NULL,NULL),(4,NULL,NULL,'2357921','Md. Jasim Mondol',NULL,'01672408287','1_2357921.jpg',NULL,'Mst. Rahima khatun','2021-04-20',NULL,'Female',' Zeba Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,'2358021','Md. Fazlar Rahman',NULL,'01672408287',NULL,NULL,'Mst. Nazma Akter','2021-04-20',NULL,'Female','Farha Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,'2358121','Nitai Chandra Saha',NULL,'01672408287','1_2358121.jpg',NULL,'Shiuli Rani Saha','2021-04-20',NULL,'Female','Porshi Rani Saha','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,NULL,NULL,'2358221','Md. Rezaul Kaim',NULL,'01672408287',NULL,NULL,'Mst. Fayejun Nesa','2021-04-20',NULL,'Female','Raha Karim','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,NULL,NULL,'2358321','MD. EKRAMUL HAQUE',NULL,'01672408287','1_2358321.jpg',NULL,'MST. JEBUN NESA JELY','2021-04-20',NULL,'Female','MRITTIKA HAQUE SREYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,NULL,'2358421','Md.Asaduzzaman',NULL,'01672408287',NULL,NULL,'Mst.Rehena Khatun','2021-04-20',NULL,'Female','Mst. Al Sadia ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,NULL,NULL,'2358521','MD. HAFIJUR RAHMAN',NULL,'01672408287',NULL,NULL,'SABINA YASMIN','2021-04-20',NULL,'Female','RUBAIYA AKHTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,NULL,'2358621','Md. Abdul Aziz',NULL,'01672408287',NULL,NULL,'Mst. Bilkis Begum','2021-04-20',NULL,'Female','Mst. Eta Moni','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,NULL,'2358721','A.K.M. Atiqur Rahman',NULL,'01672408287',NULL,NULL,'Mst. Rajia Sultana','2021-04-20',NULL,'Female','Adiba jaman','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,NULL,NULL,'2358821','MD. SHAHADAT HOSSAIN',NULL,'01672408287',NULL,NULL,'MST MAUSHUMI ARA','2021-04-20',NULL,'Female','MST. MEHZABIN YESMIN (MEDHA)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,NULL,NULL,'2358921','Shamim al Manun',NULL,'01672408287',NULL,NULL,'Mst. Kona khatun','2021-04-20',NULL,'Female','Suraiya Khatun','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,NULL,NULL,'2359021','MD.SADIQUR RA HMAN',NULL,'01672408287',NULL,NULL,'Mrs. Asmaul Husna Sonia','2021-04-20',NULL,'Female','Jannatun Safa','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,NULL,NULL,'2359121','MD. JAFAR IQBAL',NULL,'01672408287',NULL,NULL,'MST. ROJINA PERVEN','2021-04-20',NULL,'Female','MST. JERIN TASNIYA JEBA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,NULL,'2359221','Md. Golam Mostofa',NULL,'01672408287',NULL,NULL,'Mst. Mahmuda Khatun','2021-04-20',NULL,'Female','Monjil Tanjila Borno','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,NULL,'2359321','Md. Azizul Haque ',NULL,'01672408287',NULL,NULL,'Mst. Samima','2021-04-20',NULL,'Female','Asfia','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,NULL,NULL,'2359421','MD. FIROZ HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. JOLYFA BEGUM','2021-04-20',NULL,'Female','FARIHA HOSSAIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,NULL,NULL,'2359521','MD. SAMIM HOSSAIN',NULL,'01672408287',NULL,NULL,'SARMIN AKTER LIZA','2021-04-20',NULL,'Female','SANJIDA ARABI SEJUTI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,NULL,NULL,'2359621','ANUP KUMAR ROY',NULL,'01672408287',NULL,NULL,'NAMITA ROY','2021-04-20',NULL,'Female','SHASHWATI ROY','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,NULL,NULL,'2359721','Md.Hasanuzzaman',NULL,'01672408287',NULL,NULL,'Mst.Surya Akter ','2021-04-20',NULL,'Female','Mst.Humaira Hasan','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,NULL,NULL,'2359821','Md. Monjur Saklayen',NULL,'01672408287',NULL,NULL,'Mst. Selina Akter','2021-04-20',NULL,'Female','Mst. Samiha Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,NULL,NULL,'2359921','MD. RUHUL AMIN',NULL,'01672408287',NULL,NULL,'MST. ROZE PARVIN','2021-04-20',NULL,'Female','MAHAZABIN SAUDA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,NULL,NULL,'2360021','Md. Rasheduzzaman',NULL,'01672408287',NULL,NULL,'Mst. Soma Akter','2021-04-20',NULL,'Female','Raisa Zaman','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,NULL,NULL,'2360121','MD. ANOWER HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. HALEMA KHATUN','2021-04-20',NULL,'Female','FAUJEA AFIFA REFA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,NULL,NULL,'2360221','Md. Sultan Nasir Uddin',NULL,'01672408287',NULL,NULL,'Mst. Lipi Akter Banu','2021-04-20',NULL,'Female','Mst. Nawshin Tabassum Toa','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,NULL,NULL,'2360321','MD. SAIFUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. MAHFUZA KHATUN','2021-04-20',NULL,'Female','MST. SADIYA AFRIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,NULL,NULL,'2360421','Md. Samsul  Alam  Talukder',NULL,'01672408287',NULL,NULL,'Most. Nipa Yasmin','2021-04-20',NULL,'Female','Nawshin  Sarar','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,NULL,NULL,'2360521','MD. HUMAYUN KABIR',NULL,'01672408287',NULL,NULL,'ZANNATUL FERDUS','2021-04-20',NULL,'Female','UMME HABIBA ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,NULL,NULL,'2360621','MD. RAFIQUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. LABONI ISLAM','2021-04-20',NULL,'Female','FARLINA TAHSIN RAISA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,NULL,NULL,'2360721','MOKLESUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. ZAKIA SULTANA','2021-04-20',NULL,'Female','MST. MAHNAZ TASFIA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,NULL,NULL,'2360821','MD. MUKUL',NULL,'01672408287',NULL,NULL,'MST. LIPI','2021-04-20',NULL,'Female','MEHEJABIN SULTANA CHAND','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,NULL,NULL,'2360921','MD. AMIR HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. UMMAY MALIA SULTANA (RESHMI)','2021-04-20',NULL,'Female','UMME AMIRA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,NULL,NULL,'2361021','MD. SELIM REZA',NULL,'01672408287',NULL,NULL,'SHULI KHATUN','2021-04-20',NULL,'Female','SAMIYA AKTHER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,NULL,NULL,'2361121','Md. Abdur Razzak',NULL,'01672408287',NULL,NULL,'Mukti Begum','2021-04-20',NULL,'Female','Nodi Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,NULL,NULL,'2361221','MD. HARUNOR RASHID',NULL,'01672408287',NULL,NULL,'MST. ISRUAT JAHAN','2021-04-20',NULL,'Female','TASFIA SABA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,NULL,NULL,'2361321','MD.SAZZAT HOSSAIN',NULL,'01672408287',NULL,NULL,'MST.ZOSNA','2021-04-20',NULL,'Female','NISHAT TASNIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,NULL,NULL,'2361421','MD. ANWAR HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. HALIMA BEGUM','2021-04-20',NULL,'Female','ALTAFUN NISA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,NULL,NULL,'2361521','Md. Raihan Hossain',NULL,'01672408287',NULL,NULL,'Mst. Samsun Nahar','2021-04-20',NULL,'Female','Mst. Rima Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,NULL,NULL,'2361621','Sanjay  Bhowmick ',NULL,'01672408287',NULL,NULL,'Rony  Rani Bhowmick','2021-04-20',NULL,'Female','Arpita Bhowmick Joya','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,NULL,NULL,'2361721','MD. ASLAM HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. NASIMA AKTER','2021-04-20',NULL,'Female','ARAFIN JAHAN PROVA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,NULL,NULL,'2361821','SHAFIQUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. TASMIAH ISLAM','2021-04-20',NULL,'Female','SHAMIHA ISLAM SHARA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(44,NULL,NULL,'2361921','Md. Zakariya Habib ( Rupok)',NULL,'01672408287',NULL,NULL,'Mst. Ismot Ara Jahan','2021-04-20',NULL,'Female','Nanjiba Taskin','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(45,NULL,NULL,'2362021','abu hayet',NULL,'01672408287',NULL,NULL,'fahomida kamal','2021-04-20',NULL,'Female','Shasshoti kamal','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,NULL,NULL,'2362121','Md. Jahangir Alom',NULL,'01672408287',NULL,NULL,'Mst. Moni Begum','2021-04-20',NULL,'Female','Mitali Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(47,NULL,NULL,'2362221','Md. Jewal Sardar',NULL,'01672408287',NULL,NULL,'Mst. Asma Akter','2021-04-20',NULL,'Female','Mst. Lamia Akter Jui','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,NULL,NULL,'2362321','S.M. Eiddris Ali',NULL,'01672408287',NULL,NULL,'Mst. Flora Nasrin','2021-04-20',NULL,'Female','Ertiza Fariha Esha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,NULL,NULL,'2362421','LATE. MAMUNUR RASHID',NULL,'01672408287',NULL,NULL,'MEHEDI YESMIN','2021-04-20',NULL,'Female','NABILA YESMIN (JHUMU)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,NULL,NULL,'2362521','MD.ABU SHAKIB',NULL,'01672408287',NULL,NULL,'NIRA','2021-04-20',NULL,'Female','ASFIA NUR ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,NULL,NULL,'2362621','Tarun Chowdhori',NULL,'01672408287',NULL,NULL,'Anita Gour','2021-04-20',NULL,'Female','Sreemoti Nishi Rani Chowdhori','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,NULL,NULL,'2362721','MD. SHABUJ',NULL,'01672408287',NULL,NULL,'MST. ROMANA AKTER','2021-04-20',NULL,'Female','MALIHA TABASSUM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,NULL,NULL,'2362821','Md. Shriful Islam',NULL,'01672408287',NULL,NULL,'Mst.Shibli','2021-04-20',NULL,'Female','Mst. Soyaiba Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,NULL,NULL,'2362921','Mridul Sarkar',NULL,'01672408287',NULL,NULL,'Krishna Sarkar','2021-04-20',NULL,'Female','Mohona Sarkar','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(55,NULL,NULL,'2363021','Md. Abul Basar ',NULL,'01672408287',NULL,NULL,'Mst. Bethe Banu','2021-04-20',NULL,'Female','Nushrat Jahan','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,NULL,NULL,'2363121','MD. MAMUNUR RASID',NULL,'01672408287',NULL,NULL,'MST. REKHA PARVIN','2021-04-20',NULL,'Female','MISS. MARJIA AKHTER JIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,NULL,NULL,'2363221','MD. Tajlur Rashid',NULL,'01672408287',NULL,NULL,'Mst. Shakila','2021-04-20',NULL,'Female','Tasfia Tahrim','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,NULL,NULL,'2363321','Md. Pinto Islam',NULL,'01672408287',NULL,NULL,'Mst. Laboni','2021-04-20',NULL,'Female','Jiniya Muni','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,NULL,NULL,'2363421','Md. Komor Uddin',NULL,'01672408287',NULL,NULL,'Mst. Angor Begum','2021-04-20',NULL,'Female','Mst. Kamrun Nahar Toli','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(60,NULL,NULL,'2363521','Md.Abu Saeed',NULL,'01672408287',NULL,NULL,'Mst.Monihar','2021-04-20',NULL,'Female','Sagupta Tasnim Saoti','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,NULL,NULL,'2363621','MD. ASHRAFUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. TASLIMA BEGUM','2021-04-20',NULL,'Female','AFRIN AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(62,NULL,NULL,'2363721','LATE. ALI REZA',NULL,'01672408287',NULL,NULL,'PAPIA AKHTER','2021-04-20',NULL,'Female','NUR-E JAMELI RUHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(63,NULL,NULL,'2363821','Md. Rafiqul Islam',NULL,'01672408287',NULL,NULL,'Mst. Rabeya Basri','2021-04-20',NULL,'Female','Mst. Nusrat Jahan Nila','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(64,NULL,NULL,'2363921','MD. RAFIQUL',NULL,'01672408287',NULL,NULL,'MST. PYARA BIBI','2021-04-20',NULL,'Female','MIMMA AKTHER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(65,NULL,NULL,'2364021','Md. Rejaul Sornoker',NULL,'01672408287',NULL,NULL,'Mst. Roni Akter','2021-04-20',NULL,'Female','NOWSHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(66,NULL,NULL,'2364121','SHAKE FARID UDDIN',NULL,'01672408287',NULL,NULL,'SAMIMA NASRIN','2021-04-20',NULL,'Female','SHAKE FAOZIA FARIHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,NULL,NULL,'2364221','MD. RAIHAN ALI DEWAN',NULL,'01672408287',NULL,NULL,'MST. RINA AKTER PARVIN','2021-04-20',NULL,'Female','FARHANA AFRIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(68,NULL,NULL,'2364321','Md.Rajab Ali Khan',NULL,'01672408287',NULL,NULL,'Mst.Nadira Parvin ','2021-04-20',NULL,'Female','Miss. Roshnara Ali Khan ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(69,NULL,NULL,'2364421','MD. KUDDUS',NULL,'01672408287',NULL,NULL,'MST.SHARMIN','2021-04-20',NULL,'Female','SUMAYA AKTHER KAKOLI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(70,NULL,NULL,'2364521','Md. Mumin Hosen',NULL,'01672408287',NULL,NULL,'Mst. Habiba Begum','2021-04-20',NULL,'Female','Mahila Busra Megla','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(71,NULL,NULL,'2364621','MD.MOSTAFIZUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST.ROTNA BEGUM','2021-04-20',NULL,'Female','MOUMI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(72,NULL,NULL,'2374921','Md. Feroj Zaman',NULL,'01672408287',NULL,NULL,'Mst. Bina Akter','2021-04-20',NULL,'Female','Afrin Jannati Raisa','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(73,NULL,NULL,'2375021','Md.Elieas Ali',NULL,'01672408287',NULL,NULL,'Mst. Taslima Khatun Sopna','2021-04-20',NULL,'Female','Mst.Umme Habiba','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(74,NULL,NULL,'2375121','Md. Habibur Rahman',NULL,'01672408287',NULL,NULL,'Mst. Lovely Akter Bithi','2021-04-20',NULL,'Female','Umme Habiba Himu','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(75,NULL,NULL,'2375221','Md. Fazlur Rahman',NULL,'01672408287',NULL,NULL,'Mst. Rohima Bibi','2021-04-20',NULL,'Female','Mst. Ifrat Ara Jui','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(76,NULL,NULL,'2375321','Md. Anowar Hosen',NULL,'01672408287',NULL,NULL,'Mst. Shewly Akter','2021-04-20',NULL,'Female','Miss Afrin','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(77,NULL,NULL,'2376621','Saidful Islam',NULL,'01672408287',NULL,NULL,'Momtaj Begum','2021-04-20',NULL,'Female','Suronggona Islam','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(200,'2020-2021','AB-','2207318','D.M. Golam Rosul Saklayn',NULL,'01672408287','1_2207318.jpg',NULL,'Sultana Mohosina Haque','2021-04-20','2021-07-01','Female','Sharika Saimun','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'2352542',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(201,'2020-2021','B+','2207618','MD. YOUNUS MONDOL',NULL,'01672408287','1_2207618.jpg',NULL,'MSS. AFROZA BEGUM','2021-04-20','2021-07-02','Female','MS. UMMA KULSUM ASHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'2352541',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(202,'2020-2021',NULL,'2207718','LET. ASHEQUE KARIM TAREFDER',NULL,'01672408287',NULL,NULL,'MST. NAZMA BEGUM','2021-04-20',NULL,'Female','RODOSHI AVA RUPTI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'2352546',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(203,'2020-2021',NULL,'2212118','HEMANTA KUMAR PAUL',NULL,'01672408287',NULL,NULL,'DOLI','2021-04-20',NULL,'Female','NISHITA PAUL','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(204,'',NULL,'2211318','MD. SAHAJAHAN ALI',NULL,'01672408287',NULL,NULL,'ROTHNA KHATUN','2021-04-20',NULL,'Female','UMME TABASSUM SUBHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(205,'',NULL,'2211518','MD. ZAHURUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SOBY PARVIN','2021-04-20',NULL,'Female','MOSA. ZARIN MOSTARIN JUI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(206,'',NULL,'2222018','MD. ABDUL KALAM AZAD',NULL,'01672408287',NULL,NULL,'MST. SALMA PARVIN','2021-04-20',NULL,'Female','NAJIA NAOSHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(207,'',NULL,'2207918','MD. MOTAHAR HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. SAMMI AKHTER','2021-04-20',NULL,'Female','MAYEESHA AKTHAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(208,'',NULL,'2221218','Md. Hannan Rashid',NULL,'01672408287',NULL,NULL,'Selina Rashid','2021-04-20',NULL,'Male','Humaira Tasnia Faija','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(209,'',NULL,'2211918','MD. KAMRUL HASAN',NULL,'01672408287',NULL,NULL,'MST. SHARMIN AKTAR','2021-04-20',NULL,'Female','SAMIHA HASAN AISHE','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(210,'',NULL,'2210918','MD. ABDUSH SHUBHEN',NULL,'01672408287',NULL,NULL,'MAOSUMI BANU','2021-04-20',NULL,'Female','MST. FATEMATUJ JUHURA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(211,'',NULL,'2207418','MD. ZANNATUL FERDUS',NULL,'01672408287',NULL,NULL,'MRS.KHADIZA AKTER','2021-04-20',NULL,'Female','MEHENAZ AFRIN MEDHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(212,'',NULL,'2207518','MD. ABDUL HALIM',NULL,'01672408287',NULL,NULL,'MST. KHADIZA BEGUM','2021-04-20',NULL,'Female','MST. FATEMA TUZ JOHURA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(213,'',NULL,'2212518','MD. TARIKUL ALAM',NULL,'01672408287',NULL,NULL,'MS. NAZAMA BEGUM','2021-04-20',NULL,'Female','MS. TANIKA NISTHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(214,'',NULL,'2208218','MD.KAFIL UDDIN',NULL,'01672408287',NULL,NULL,'MST. ALPONA KHTUN (LUCKY)','2021-04-20',NULL,'Female','AFIEA BINTA KAKON','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(215,'',NULL,'2211118','MD. JAKARIA HABIB',NULL,'01672408287',NULL,NULL,'MST. SHAMIMA  ARA (ETI)','2021-04-20',NULL,'Female','MST. JINIA AFRIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(216,'',NULL,'2211718','MD. SHAED ANSARI TIPU',NULL,'01672408287',NULL,NULL,' MASUDA BEGUM','2021-04-20',NULL,'Female','MUSFICA ANSARI SUMAYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(217,'',NULL,'2208018','SUBAL SAHA',NULL,'01672408287',NULL,NULL,'JOYA RANI SAHA','2021-04-20',NULL,'Female','VUMIKA SAHA','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(218,'',NULL,'2208318','MD. TOUHIDUL ISLAM SHAJU',NULL,'01672408287',NULL,NULL,'MST. JAKIA SULTANA','2021-04-20',NULL,'Female','TAWHIDA TASNIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(219,'',NULL,'2215218','PARITOSH KUMAR MONDAL',NULL,'01672408287',NULL,NULL,'SHIKHA RANI MONDAL','2021-04-20',NULL,'Female','TRISHA MONDAL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(220,'',NULL,'2208418','ZAHADUR RAHAMAN',NULL,'01672408287',NULL,NULL,'BINA AKTHER PARY','2021-04-20',NULL,'Female','ZANNATUN NESA PREMA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(221,'',NULL,'2207118','S.M. IFTAKER RAHMAN',NULL,'01672408287','1_2207118.jpg',NULL,'MAKSUDA AKTER','2021-04-20',NULL,'Female','MST. IFAT ARA (ETHINA)','Islam',1,NULL,NULL,NULL,'Amtali Sadar',NULL,81,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Amtali Sadar',NULL,NULL,81,NULL,NULL,NULL,NULL),(222,'',NULL,'2215018','MD. ATIQUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. KHURSIDA ZERIN','2021-04-20',NULL,'Female','MOST. ARIFA NAWSHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(223,'',NULL,'2212418','MD. ABDUL MANNAN',NULL,'01672408287',NULL,NULL,'RABEYA AKTHER BITHI','2021-04-20',NULL,'Female','MUSRAT TABASSUM MADHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(224,'',NULL,'2314819','MD. JOYIN UDDIN AHMED',NULL,'01672408287',NULL,NULL,'SHAHANAZ PERVIN MALA','2021-04-20',NULL,'Female','MONOWARA AHMED PINKI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(225,'',NULL,'2211818','NEPAL SARDAR',NULL,'01672408287',NULL,NULL,'SUKUMARI SARDAR','2021-04-20',NULL,'Female','THITI SORDAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(226,'',NULL,'2222118','MD. ABDUL KHALEK',NULL,'01672408287',NULL,NULL,'MST. MONI BEGUM','2021-04-20',NULL,'Female','FARHANA MAHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(227,'',NULL,'2220718','MD. ABDUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. SOMPA BANU','2021-04-20',NULL,'Female','JANNATUN FARDAUS','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(228,'',NULL,'2211018','MD. SHAHIDUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. HASINA BEGUM','2021-04-20',NULL,'Female','SADIA ISLAM SAMMO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(229,'',NULL,'2217518','ABDUL LATIF ',NULL,'01672408287',NULL,NULL,'SHAPLA BEGUM','2021-04-20',NULL,'Female','MISS. LABONNO AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(230,'',NULL,'2211218','MD. REAZUL HASAN',NULL,'01672408287',NULL,NULL,'SALMA KHATUN','2021-04-20',NULL,'Female','AYUBI HASAN MEDHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(231,'',NULL,'2221918','MD. MASUD RANA',NULL,'01672408287',NULL,NULL,'MST. NAHAR-UL-JANNAT','2021-04-20',NULL,'Female','MST. TAYAFUN ARSHIB(DIBA)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(232,'',NULL,'2217718','MD. SURUG ALI',NULL,'01672408287',NULL,NULL,'MST. PRIYA BIBI','2021-04-20',NULL,'Female','MIST. TAMANNA ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(233,'',NULL,'2221718','MD. NURUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHAMIMA AKTER','2021-04-20',NULL,'Female',' LIMA AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(234,'',NULL,'2215518','MD. MIZANUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. RESMAUL HUSNIA','2021-04-20',NULL,'Female','MISS. SHERAJAM MONIRA MUNI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(235,'',NULL,'2215618','MD. ANWAR SHEKH (MITHU)',NULL,'01672408287',NULL,NULL,'MST. MAFUZA','2021-04-20',NULL,'Female','AFROZA MOSTARI CHITROLAKHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(236,'',NULL,'2211418','LATE. ATAUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. REKHA KHANOM','2021-04-20',NULL,'Female','RISHITA TASNUVA JERIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(237,'',NULL,'2214918','ATAUL HAQUE',NULL,'01672408287',NULL,NULL,'MOST. SULITANA AKTAR SILPI','2021-04-20',NULL,'Female','MRS. ARIYA AKTAR SADIYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(238,'',NULL,'2219618','MD. HASAN ALI',NULL,'01672408287',NULL,NULL,'MST. MUNIRA BEGUM','2021-04-20',NULL,'Female','UMME HABIBA ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(239,'',NULL,'2222218','MD. NAZRUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHARIFA BEGUM','2021-04-20',NULL,'Female','SUMAIYA ISLAM (NAORIN)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(240,'',NULL,'2314619','MD.MIZANUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. HABIBA BANU','2021-04-20',NULL,'Female','MAHBUBA YEASMIN MITHILA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(241,'',NULL,'2314719','MD. FARUQUE HOSSAIN',NULL,'01672408287',NULL,NULL,'MRS NAZMA PARVIN','2021-04-20',NULL,'Female','FARIEA AKTER RASMI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(242,'',NULL,'2220818','MD. ABU RASEL KHAN',NULL,'01672408287',NULL,NULL,'MST. RUNA','2021-04-20',NULL,'Female','SUMOONA KHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(243,'',NULL,'2218618','MD. ABU BAKKER SIDDEK',NULL,'01672408287',NULL,NULL,'MST. DULALY','2021-04-20',NULL,'Female','SHABIHA SIDDIKEE MIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(244,'',NULL,'2215418','MD. RAFIQUL ALAM',NULL,'01672408287',NULL,NULL,'MST. RITU','2021-04-20',NULL,'Female','SANIA ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(245,'',NULL,'2319219','MD. NURZZAMAN HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. MAYA BANU','2021-04-20',NULL,'Female','MST. LOVA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(246,'',NULL,'2212218','SHEIKH TUHHIN',NULL,'01672408287',NULL,NULL,'MST. TUNIKA NASRIN','2021-04-20',NULL,'Female','TANISHA TANJUM ROJA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(247,'',NULL,'2217618','MD. SURUZ ALI MONDAL',NULL,'01672408287',NULL,NULL,'MST. BEUTI RANI','2021-04-20',NULL,'Female','SHADIYA ISLAM SUSHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(248,'',NULL,'2218518','MD. MOSTOFA HOSSEN',NULL,'01672408287',NULL,NULL,'MST. RUZINA BEGUM','2021-04-20',NULL,'Female','FARHANA YASMIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(249,'',NULL,'2219018','MD. HAFIZUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. FULJAN BEGUM','2021-04-20',NULL,'Female','HASNA HENA HAPPY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(250,'',NULL,'2220918','MD. JAHIDUL PRAMANIK',NULL,'01672408287',NULL,NULL,'MST. RINA BANU','2021-04-20',NULL,'Female','JANNATUN FERDOUS JUTHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(251,'',NULL,'2220418','MD. GULKER NINE ',NULL,'01672408287',NULL,NULL,'MST. TUHINA AKTER','2021-04-20',NULL,'Female','ASMAUL HOSNA NATASHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(252,'','A+','2219518','SUJON RAHMAN',NULL,'01672408287',NULL,NULL,'MST. RABEA BEGUM','2021-04-20','2001-09-08','Female','MIM AKHTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(253,'',NULL,'2217318','MD.ABDUL MAZID',NULL,'01672408287',NULL,NULL,'MST.RIYA','2021-04-20',NULL,'Female','MST.MIM AKTHER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(254,'',NULL,'2219218','MD. MOMINUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHULI AKTER','2021-04-20',NULL,'Female','MST. MORIYOM KHANUM MIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(255,'',NULL,'2215118','MD. RUHUL AMIN KHAN',NULL,'01672408287',NULL,NULL,'MST. SHARMEN SULTANA','2021-04-20',NULL,'Female','RAHATIN NAZAT KHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(256,'',NULL,'2219418','PROSANTO SAHA',NULL,'01672408287',NULL,NULL,'DIPA SAHA','2021-04-20',NULL,'Female','DISHA SAHA','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(257,'',NULL,'2217418','MOHAMMED REZAUL ISLAM ROTON',NULL,'01672408287',NULL,NULL,'MST. LILI ','2021-04-20',NULL,'Female','NUSRAT JAHAN RIYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(258,'',NULL,'2220618','MD. SADEKUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SUMI BEGUM','2021-04-20',NULL,'Female','MIFTAHUL JANNAT MAISHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(259,'',NULL,'2218018','MD. HIMEL',NULL,'01672408287',NULL,NULL,'MOSLEMA ','2021-04-20',NULL,'Female','HOMAIRA AKTER HIMU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(260,'',NULL,'2217918','MD. DULAL HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. MASUDA BANU','2021-04-20',NULL,'Female','RIYA MONI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(261,'',NULL,'2218818','MOJAMMEL HAQUE',NULL,'01672408287',NULL,NULL,'MST. NADIRA BEGUM','2021-04-20',NULL,'Female','RIYA AKTER SETU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(262,'',NULL,'2218118','MD. SHAMSUL HAQUE',NULL,'01672408287',NULL,NULL,'MOST. FERDOUSY BEGUM','2021-04-20',NULL,'Female','MYMONA FARZANA FARIA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(263,'',NULL,'2218318','MD. JUEL ALI',NULL,'01672408287',NULL,NULL,'MST. SHAHANAZ PARVIN','2021-04-20',NULL,'Female','FARJANA YESNIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(264,'',NULL,'2218218','KALAM',NULL,'01672408287',NULL,NULL,'SALINA','2021-04-20',NULL,'Female','RIYA MUNI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(265,'',NULL,'2319719','F',NULL,'01672408287',NULL,NULL,'M','2021-04-20',NULL,'Female','Gayotry Chokroborty','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(266,'',NULL,'2220318','MD. SAMSUL ALAM CHOWDHURY',NULL,'01672408287',NULL,NULL,'MST. ROUSHON ARA BEGUM','2021-04-20',NULL,'Female','TASLIMA JAHAN CHOWDHURY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(267,'',NULL,'2221618','SREE AMIT KUMAR RAY',NULL,'01672408287',NULL,NULL,'SUMA RANI ROY','2021-04-20',NULL,'Female','SREE UISE RAY SHENHA ','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(268,'',NULL,'2220218','MD. ABDUL KHALEK',NULL,'01672408287',NULL,NULL,'MST. MINA BEGUM','2021-04-20',NULL,'Female','MST. ARABI AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(269,'',NULL,'2221818','AMIMUL HUQ CHOWDHORY AKBAR',NULL,'01672408287',NULL,NULL,'TANGENA CHOWDHORY LONA','2021-04-20',NULL,'Female','ADRITA HAQUE CHOWDHORY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(270,'',NULL,'2376921','Md. Sarwar Jahan',NULL,'01672408287',NULL,NULL,'Mst. Nelufa Akther','2021-04-20',NULL,'Female','Sabila Jahan Sneha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(271,'',NULL,'2379721','Md. Abdul Hamid',NULL,'01672408287',NULL,NULL,'Mrs. Rebeka Sultana','2021-04-20',NULL,'Female','Mis. Farhana Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(272,'',NULL,'2379921','Rejaul Islam',NULL,'01672408287',NULL,NULL,'Fizun Nahar','2021-04-20',NULL,'Female','Fabia Binta Reza','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(273,'',NULL,'2380321','Abdur Rouf',NULL,'01672408287',NULL,NULL,'Sultana Razia','2021-04-20',NULL,'Female','Samiya Afrin Simu','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(318,NULL,NULL,'1610272','Emtiyaz Faruk ',NULL,'01672408287',NULL,NULL,'Nur Jahan','2021-04-20',NULL,'Female','Mst.Yesmin Jahan','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(319,NULL,NULL,'2279218','MD.ENSAB ALI SARKAR',NULL,'01672408287',NULL,NULL,'MST.NAHIDA KHATUN','2021-04-20',NULL,'Female','MOST.NASEHA JAHAN TAZIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(320,NULL,NULL,'2259418','AMAL KUMAR GHOSH',NULL,'01672408287',NULL,NULL,'BABY RANI GHOSH','2021-04-20',NULL,'Female','ARPITA GHOSH','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(321,NULL,NULL,'2283218','SYED MD.TAZUL ISLAM ',NULL,'01672408287',NULL,NULL,'SELINA PERVIN ','2021-04-20',NULL,'Female','SYEDA TASNIA TARANNUM ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(322,NULL,NULL,'1610229','Md. Sohel Rana',NULL,'01672408287',NULL,NULL,'Mst. Tanjila Khatun Nayon','2021-04-20',NULL,'Female','Aononna Nusrat Sorna','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(323,NULL,NULL,'1610202','Hakim Mondol',NULL,'01672408287',NULL,NULL,'Helena Begum','2021-04-20',NULL,'Female','Mst. Khairun Nesa','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(324,NULL,NULL,'2283318','ASHIM KUMAR TARAFDER',NULL,'01672408287',NULL,NULL,'SIMA SARKAR','2021-04-20',NULL,'Female','PAYEL TARAFDER','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(325,NULL,NULL,'1610294','Sultan Ahmed Anamul Haque',NULL,'01672408287',NULL,NULL,'Mst. Momota Jahan','2021-04-20',NULL,'Female','Farah Liza','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(326,NULL,NULL,'1610305','Md. Sakowat Hossain',NULL,'01672408287',NULL,NULL,'Mst. Rajiya Sulatana','2021-04-20',NULL,'Female','Sumaiya Basri Neha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(327,NULL,NULL,'2284218','MD. ABDUL HAKIM',NULL,'01672408287',NULL,NULL,'SALMA BANU','2021-04-20',NULL,'Female','RUTBAH MAHDIAT RAMIZAH','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(328,NULL,NULL,'2283118','MD.MOHASIN REZA',NULL,'01672408287',NULL,NULL,'MISS RONAKE AKTER','2021-04-20',NULL,'Female','  MIMSTOBA REZA  MOU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(329,NULL,NULL,'1610307','Md. Tawhidul Islam',NULL,'01672408287',NULL,NULL,'Shamima Nasrin','2021-04-20',NULL,'Female','Mst. Tahsin A Jannat','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(330,NULL,NULL,'2301619','S.M. Golam Rabbani',NULL,'01672408287',NULL,NULL,'Mst. Owajifa Sultana','2021-04-20',NULL,'Female','Mst. Sadia Sultana (Richi)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(331,NULL,NULL,'2284418','MD. HUMAUN  KABIR',NULL,'01672408287',NULL,NULL,'NASRIN SULTANA','2021-04-20',NULL,'Female','HUMYRA JANNAT','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(332,NULL,NULL,'1610274','Md. Jaman Sardar',NULL,'01672408287',NULL,NULL,'Mst. Adori Banu','2021-04-20',NULL,'Female','Jesmin Ara Jothi','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(333,NULL,NULL,'2283618','MD. SAMSUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. RIKTA BEGUM','2021-04-20',NULL,'Female','MST. NUSRAT JAHAN SADIYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(334,NULL,NULL,'1610299','Kazi Sayed Taymor Kabir',NULL,'01672408287',NULL,NULL,'Sayeda Mahbuba Sultana','2021-04-20',NULL,'Female','Sajiya Tasnim','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(335,NULL,NULL,'2283818','MD. AZAD HOSSAIN',NULL,'01672408287',NULL,NULL,'MOST. NURUN NAHAR PARVIN','2021-04-20',NULL,'Female','JOYNOB RABEYA JANNAT','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(336,NULL,NULL,'2283718','MD. FORHAD HUSSAIN',NULL,'01672408287',NULL,NULL,'ZULEKHA BEGUM','2021-04-20',NULL,'Female','FAWZIA TASNIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(337,NULL,NULL,'1610313','Prodip Chndro Mohanto',NULL,'01672408287',NULL,NULL,'Rikta Rani','2021-04-20',NULL,'Female','Retu Rani ','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(338,NULL,NULL,'2283518','MD. NAZMUL HUDA',NULL,'01672408287',NULL,NULL,'MST. JULIA AKTER','2021-04-20',NULL,'Female','RAMISA HUDA RIPTY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(339,NULL,NULL,'1610318','Md. Sajedul Islam',NULL,'01672408287',NULL,NULL,'Mst. Hosne Ara','2021-04-20',NULL,'Female','Farabi Islam','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(340,NULL,NULL,'2259118','ASHIM DAS',NULL,'01672408287',NULL,NULL,'SUMA DAS','2021-04-20',NULL,'Female','AHONA DAS','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(341,NULL,NULL,'2283418','MD. ABDUL LOTIF ',NULL,'01672408287',NULL,NULL,'MST. MOMITUZZEHA','2021-04-20',NULL,'Female','MST. LABIBA AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(342,NULL,NULL,'2258718','MD. MAMUNUR RASHID',NULL,'01672408287',NULL,NULL,'MST. SANJIDA YESMIN','2021-04-20',NULL,'Female','MITHILA FARJANA MAHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(343,NULL,NULL,'2259018','MD. ABUL KASHEM',NULL,'01672408287',NULL,NULL,'MST. AJMIRA BEGUM','2021-04-20',NULL,'Female','MISS TANVIN AKTER ETY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(344,NULL,NULL,'2259718','NIMAI CHANDRA KARMOKER',NULL,'01672408287',NULL,NULL,'BINDU KARMOKER','2021-04-20',NULL,'Female','BONY KARMOKAR','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(345,NULL,NULL,'2284118','MD. BEDARUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. REBEKA SULTANA ','2021-04-20',NULL,'Female','MST. JANNATUN MAOA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(346,NULL,NULL,'2284318','MANIK CHANDRA DASH ',NULL,'01672408287',NULL,NULL,'RANI DASH ','2021-04-20',NULL,'Female','ISHITA RANI DASH ','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(347,NULL,NULL,'2203417','Manik Shaha',NULL,'01672408287',NULL,NULL,'Suborna Shaha','2021-04-20',NULL,'Female','Disha Shaha','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(348,NULL,NULL,'2313819','MD. NAZRUL ISLAM',NULL,'01672408287',NULL,NULL,'MARIM PARVEEN','2021-04-20',NULL,'Female','SHAHAD ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(349,NULL,NULL,'2295919','MD. SHARIFUL ISLAM',NULL,'01672408287',NULL,NULL,'CHAMPA BEGUM','2021-04-20',NULL,'Female','SURAIYA ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(350,NULL,NULL,'2258518','MD. SHAFIQUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SULTANA SHAFIQ','2021-04-20',NULL,'Female','SUAD ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(351,NULL,NULL,'1610203','Md. Aslam Hossain',NULL,'01672408287',NULL,NULL,'Farjana Begum','2021-04-20',NULL,'Female','Rojoni Somapti','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(352,NULL,NULL,'1610295','Md. Irtajul Islam',NULL,'01672408287',NULL,NULL,'Mst. Asia Khatun','2021-04-20',NULL,'Female','Mariya Sultana','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(353,NULL,NULL,'2181817','Mofajjol Hossain',NULL,'01672408287',NULL,NULL,'Khursheda Jahan','2021-04-20',NULL,'Female','Farhia Yesmin','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(354,NULL,NULL,'1610208','Md. Obaydur Rahman Kazol',NULL,'01672408287',NULL,NULL,'Dipti','2021-04-20',NULL,'Female','Kaniz Farhana Krepa','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(355,NULL,NULL,'2316119','MD. JAMIL UDDIN SARKER',NULL,'01672408287',NULL,NULL,'MST. SHAMMY SARKER','2021-04-20',NULL,'Female','MST. SILVIA SARKAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(356,NULL,NULL,'1610278','Wahed Box Mondol',NULL,'01672408287',NULL,NULL,'Mst. Razifa Akter','2021-04-20',NULL,'Female','Marziya ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(357,NULL,NULL,'1610279','Md. Joynul Abedin ',NULL,'01672408287',NULL,NULL,'Mst. Dilruba Akter Somi','2021-04-20',NULL,'Female','Mst. Zobaiya Arobi Jui','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(358,NULL,NULL,'2274418','MD. ABDUL WADUD AKAND',NULL,'01672408287',NULL,NULL,'MST. BIZLY BEGUM','2021-04-20',NULL,'Female','Mst. Srabon Akter Chaity','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(359,NULL,NULL,'2181917','Mofajjol Hossain',NULL,'01672408287',NULL,NULL,'Khursheda Jahan','2021-04-20',NULL,'Female','Fairose Yesmin','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(360,NULL,NULL,'2295719','MD. HAFIZAR RAHMAN',NULL,'01672408287',NULL,NULL,'MST RONY BEGOM','2021-04-20',NULL,'Female','MOST.  HADIYA ASHRAFI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(361,NULL,NULL,'1610228','Md. Shamsuzzaman',NULL,'01672408287',NULL,NULL,'Asma Zaman','2021-04-20',NULL,'Female','Samiha Jaman Suhi','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(362,NULL,NULL,'1610315','Md. Masud Rana',NULL,'01672408287',NULL,NULL,'Shamima Akter','2021-04-20',NULL,'Female','Ridwana Afrin Rafiya','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(363,NULL,NULL,'1610221','A.K.M. Zahidul Haque',NULL,'01672408287',NULL,NULL,'Mst. Sumi Haque','2021-04-20',NULL,'Female','Mst. Zabiya Haque Simi','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(364,NULL,NULL,'2259218','MD. ZEKRUL HASAN MITHU',NULL,'01672408287',NULL,NULL,'SHARMIN AKTER','2021-04-20',NULL,'Female','ZUAIRIA TASNIM RITI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(365,NULL,NULL,'1610275','Rafiqul Islam',NULL,'01672408287',NULL,NULL,'Mst. Sharmin Akter','2021-04-20',NULL,'Female','Mst. Rabeya Bashri Rukomoni','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(366,NULL,NULL,'2203717','Md.Rejaul',NULL,'01672408287',NULL,NULL,'Moushumi Sharmin','2021-04-20',NULL,'Female','Sufiya Binte Awal','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(367,NULL,NULL,'1610285','Md. Mamonur Rashid',NULL,'01672408287',NULL,NULL,'Sharmin Sultana','2021-04-20',NULL,'Female','Marjiya Rashid Maisha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(368,NULL,NULL,'2315919','MD. JAKIR HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. ETI BANU','2021-04-20',NULL,'Female','MS. NIRMA BANU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(369,NULL,NULL,'1610282','Md.Matiur Rahman',NULL,'01672408287',NULL,NULL,'Mst. Nargis Parvin','2021-04-20',NULL,'Female','Mst. Keya Banu','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(370,NULL,NULL,'2258918','MD. AINUL SARDAR',NULL,'01672408287',NULL,NULL,'MST. LABONY AKHTER','2021-04-20',NULL,'Female','ZANNATUN ANU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(371,NULL,NULL,'2258818','MD. AINUL SARDAR',NULL,'01672408287',NULL,NULL,'MST. LABONY AKTHER','2021-04-20',NULL,'Female','HUMAIRA APU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(372,NULL,NULL,'1610212','Biplob Kumar Saha',NULL,'01672408287',NULL,NULL,'Shikha Rani Saha','2021-04-20',NULL,'Female','Sathi Rani','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(373,NULL,NULL,'1610207','Golam Kibriya',NULL,'01672408287',NULL,NULL,'Mst. Runa Layla','2021-04-20',NULL,'Female','Tasniya Tahsin Sineha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(374,NULL,NULL,'2203617','Md.Mitho',NULL,'01672408287',NULL,NULL,'Julakkha','2021-04-20',NULL,'Female','Nusrat','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(375,NULL,NULL,'2303019','MD. SAFIQUL ISLAM',NULL,'01672408287',NULL,NULL,'MORSHEDA AKTER','2021-04-20',NULL,'Female','MASKURA MARIAM SADIKA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(376,NULL,NULL,'2321819','M HASANUR RAHAMAN',NULL,'01672408287',NULL,NULL,'MST. MODINATUN NESA','2021-04-20',NULL,'Female','MISS ANIKA TAHSIN PAPRI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(377,NULL,NULL,'2259618','MD. AMRUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. REHENA AKTER','2021-04-20',NULL,'Female','RAKHI AKTER RANI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(378,NULL,NULL,'2215318','MD. RAFIQUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. RABEYA BEGUM','2021-04-20',NULL,'Female','FARZANA ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(379,NULL,NULL,'2355120','Md. Anowar Hosen',NULL,'01672408287',NULL,NULL,'Mst. Halima Begum','2021-04-20',NULL,'Female','Alia Khatun','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(380,NULL,NULL,'2357120','Md. Siddiqur Rahman  ',NULL,'01672408287',NULL,NULL,'Mst. Rimi Rahman','2021-04-20',NULL,'Female','Sumaiya Rahman Badhon','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(381,NULL,NULL,'2377721','Zahidul Islam',NULL,'01672408287',NULL,NULL,'Gulsan Ara Banu','2021-04-20',NULL,'Female','Nadia Afrin','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(383,NULL,NULL,'2380322','D.M. Golam Rosul Saklayn',NULL,'01672408287','1_2380322.jpg',NULL,'Sultana Mohosina Haque','2021-04-20',NULL,'Female','Sharika Saimun','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(384,NULL,NULL,'2380323','MD. YOUNUS MONDOL',NULL,'01672408287',NULL,NULL,'MSS. AFROZA BEGUM','2021-04-20',NULL,'Female','MS. UMMA KULSUM ASHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(385,NULL,NULL,'2380324','LET. ASHEQUE KARIM TAREFDER',NULL,'01672408287',NULL,NULL,'MST. NAZMA BEGUM','2021-04-20',NULL,'Female','RODOSHI AVA RUPTI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(386,NULL,NULL,'2380325','HEMANTA KUMAR PAUL',NULL,'01672408287',NULL,NULL,'DOLI','2021-04-20',NULL,'Female','NISHITA PAUL','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(387,NULL,'B+','2380326','MD. SAHAJAHAN ALI',NULL,'01672408287',NULL,NULL,'ROTHNA KHATUN','2021-04-20','2021-01-01','Female','UMME TABASSUM SUBHA','Islam',1,NULL,NULL,NULL,'Fnaulo','Mawna',84,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Fnaulo','Mawna',NULL,84,NULL,NULL,NULL,NULL),(388,NULL,NULL,'2380327','MD. ZAHURUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SOBY PARVIN','2021-04-20',NULL,'Female','MOSA. ZARIN MOSTARIN JUI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(389,NULL,NULL,'2380328','MD. ABDUL KALAM AZAD',NULL,'01672408287',NULL,NULL,'MST. SALMA PARVIN','2021-04-20',NULL,'Female','NAJIA NAOSHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(390,NULL,NULL,'2380329','MD. MOTAHAR HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. SAMMI AKHTER','2021-04-20',NULL,'Female','MAYEESHA AKTHAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(391,NULL,NULL,'2380330','Md. Hannan Rashid',NULL,'01672408287',NULL,NULL,'Selina Rashid','2021-04-20',NULL,'Male','Humaira Tasnia Faija','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(392,NULL,NULL,'2380331','MD. KAMRUL HASAN',NULL,'01672408287',NULL,NULL,'MST. SHARMIN AKTAR','2021-04-20',NULL,'Female','SAMIHA HASAN AISHE','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(393,NULL,NULL,'2380332','MD. ABDUSH SHUBHEN',NULL,'01672408287',NULL,NULL,'MAOSUMI BANU','2021-04-20',NULL,'Female','MST. FATEMATUJ JUHURA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(394,NULL,NULL,'2380333','MD. ZANNATUL FERDUS',NULL,'01672408287',NULL,NULL,'MRS.KHADIZA AKTER','2021-04-20',NULL,'Female','MEHENAZ AFRIN MEDHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(395,NULL,NULL,'2380334','MD. ABDUL HALIM',NULL,'01672408287',NULL,NULL,'MST. KHADIZA BEGUM','2021-04-20',NULL,'Female','MST. FATEMA TUZ JOHURA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(396,NULL,NULL,'2380335','MD. TARIKUL ALAM',NULL,'01672408287',NULL,NULL,'MS. NAZAMA BEGUM','2021-04-20',NULL,'Female','MS. TANIKA NISTHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(397,NULL,NULL,'2380336','MD.KAFIL UDDIN',NULL,'01672408287',NULL,NULL,'MST. ALPONA KHTUN (LUCKY)','2021-04-20',NULL,'Female','AFIEA BINTA KAKON','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(398,NULL,NULL,'2380337','MD. JAKARIA HABIB',NULL,'01672408287',NULL,NULL,'MST. SHAMIMA  ARA (ETI)','2021-04-20',NULL,'Female','MST. JINIA AFRIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(399,NULL,NULL,'2380338','MD. SHAED ANSARI TIPU',NULL,'01672408287',NULL,NULL,' MASUDA BEGUM','2021-04-20',NULL,'Female','MUSFICA ANSARI SUMAYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(400,NULL,NULL,'2380339','SUBAL SAHA',NULL,'01672408287',NULL,NULL,'JOYA RANI SAHA','2021-04-20',NULL,'Female','VUMIKA SAHA','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(401,NULL,NULL,'2380340','MD. TOUHIDUL ISLAM SHAJU',NULL,'01672408287',NULL,NULL,'MST. JAKIA SULTANA','2021-04-20',NULL,'Female','TAWHIDA TASNIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(402,NULL,NULL,'2380341','PARITOSH KUMAR MONDAL',NULL,'01672408287',NULL,NULL,'SHIKHA RANI MONDAL','2021-04-20',NULL,'Female','TRISHA MONDAL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(403,NULL,NULL,'2380342','ZAHADUR RAHAMAN',NULL,'01672408287',NULL,NULL,'BINA AKTHER PARY','2021-04-20',NULL,'Female','ZANNATUN NESA PREMA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(404,NULL,NULL,'2380343','S.M. IFTAKER RAHMAN',NULL,'01672408287',NULL,NULL,'MAKSUDA AKTER','2021-04-20',NULL,'Female','MST. IFAT ARA (ETHINA)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(405,NULL,NULL,'2380344','MD. ATIQUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. KHURSIDA ZERIN','2021-04-20',NULL,'Female','MOST. ARIFA NAWSHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(406,NULL,NULL,'2380345','MD. ABDUL MANNAN',NULL,'01672408287',NULL,NULL,'RABEYA AKTHER BITHI','2021-04-20',NULL,'Female','MUSRAT TABASSUM MADHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(407,NULL,NULL,'2380346','MD. JOYIN UDDIN AHMED',NULL,'01672408287',NULL,NULL,'SHAHANAZ PERVIN MALA','2021-04-20',NULL,'Female','MONOWARA AHMED PINKI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(408,NULL,NULL,'2380347','NEPAL SARDAR',NULL,'01672408287',NULL,NULL,'SUKUMARI SARDAR','2021-04-20',NULL,'Female','THITI SORDAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(409,NULL,NULL,'2380348','MD. ABDUL KHALEK',NULL,'01672408287',NULL,NULL,'MST. MONI BEGUM','2021-04-20',NULL,'Female','FARHANA MAHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(410,NULL,NULL,'2380349','MD. ABDUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. SOMPA BANU','2021-04-20',NULL,'Female','JANNATUN FARDAUS','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(411,NULL,NULL,'2380350','MD. SHAHIDUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. HASINA BEGUM','2021-04-20',NULL,'Female','SADIA ISLAM SAMMO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(412,NULL,NULL,'2380351','ABDUL LATIF ',NULL,'01672408287',NULL,NULL,'SHAPLA BEGUM','2021-04-20',NULL,'Female','MISS. LABONNO AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(413,NULL,NULL,'2380352','MD. REAZUL HASAN',NULL,'01672408287',NULL,NULL,'SALMA KHATUN','2021-04-20',NULL,'Female','AYUBI HASAN MEDHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(414,NULL,NULL,'2380353','MD. MASUD RANA',NULL,'01672408287',NULL,NULL,'MST. NAHAR-UL-JANNAT','2021-04-20',NULL,'Female','MST. TAYAFUN ARSHIB(DIBA)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(415,NULL,NULL,'2380354','MD. SURUG ALI',NULL,'01672408287',NULL,NULL,'MST. PRIYA BIBI','2021-04-20',NULL,'Female','MIST. TAMANNA ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(416,NULL,NULL,'2380355','MD. NURUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHAMIMA AKTER','2021-04-20',NULL,'Female',' LIMA AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(417,NULL,NULL,'2380356','MD. MIZANUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. RESMAUL HUSNIA','2021-04-20',NULL,'Female','MISS. SHERAJAM MONIRA MUNI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(418,NULL,NULL,'2380357','MD. ANWAR SHEKH (MITHU)',NULL,'01672408287',NULL,NULL,'MST. MAFUZA','2021-04-20',NULL,'Female','AFROZA MOSTARI CHITROLAKHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(419,NULL,NULL,'2380358','LATE. ATAUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. REKHA KHANOM','2021-04-20',NULL,'Female','RISHITA TASNUVA JERIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(420,NULL,NULL,'2380359','ATAUL HAQUE',NULL,'01672408287',NULL,NULL,'MOST. SULITANA AKTAR SILPI','2021-04-20',NULL,'Female','MRS. ARIYA AKTAR SADIYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(421,NULL,NULL,'2380360','MD. HASAN ALI',NULL,'01672408287',NULL,NULL,'MST. MUNIRA BEGUM','2021-04-20',NULL,'Female','UMME HABIBA ','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(422,NULL,NULL,'2380361','MD. NAZRUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHARIFA BEGUM','2021-04-20',NULL,'Female','SUMAIYA ISLAM (NAORIN)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(423,NULL,NULL,'2380362','MD.MIZANUR RAHMAN',NULL,'01672408287',NULL,NULL,'MST. HABIBA BANU','2021-04-20',NULL,'Female','MAHBUBA YEASMIN MITHILA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(424,NULL,NULL,'2380363','MD. FARUQUE HOSSAIN',NULL,'01672408287',NULL,NULL,'MRS NAZMA PARVIN','2021-04-20',NULL,'Female','FARIEA AKTER RASMI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(425,NULL,NULL,'2380364','MD. ABU RASEL KHAN',NULL,'01672408287',NULL,NULL,'MST. RUNA','2021-04-20',NULL,'Female','SUMOONA KHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(426,NULL,NULL,'2380365','MD. ABU BAKKER SIDDEK',NULL,'01672408287',NULL,NULL,'MST. DULALY','2021-04-20',NULL,'Female','SHABIHA SIDDIKEE MIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(427,NULL,NULL,'2380366','MD. RAFIQUL ALAM',NULL,'01672408287',NULL,NULL,'MST. RITU','2021-04-20',NULL,'Female','SANIA ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(428,NULL,NULL,'2380367','MD. NURZZAMAN HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. MAYA BANU','2021-04-20',NULL,'Female','MST. LOVA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(429,NULL,NULL,'2380368','SHEIKH TUHHIN',NULL,'01672408287',NULL,NULL,'MST. TUNIKA NASRIN','2021-04-20',NULL,'Female','TANISHA TANJUM ROJA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(430,NULL,NULL,'2380369','MD. SURUZ ALI MONDAL',NULL,'01672408287',NULL,NULL,'MST. BEUTI RANI','2021-04-20',NULL,'Female','SHADIYA ISLAM SUSHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(431,NULL,NULL,'2380370','MD. MOSTOFA HOSSEN',NULL,'01672408287',NULL,NULL,'MST. RUZINA BEGUM','2021-04-20',NULL,'Female','FARHANA YASMIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(432,NULL,NULL,'2380371','MD. HAFIZUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. FULJAN BEGUM','2021-04-20',NULL,'Female','HASNA HENA HAPPY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(433,NULL,NULL,'2380372','MD. JAHIDUL PRAMANIK',NULL,'01672408287',NULL,NULL,'MST. RINA BANU','2021-04-20',NULL,'Female','JANNATUN FERDOUS JUTHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(434,NULL,NULL,'2380373','MD. GULKER NINE ',NULL,'01672408287',NULL,NULL,'MST. TUHINA AKTER','2021-04-20',NULL,'Female','ASMAUL HOSNA NATASHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(435,NULL,NULL,'2380374','SUJON RAHMAN',NULL,'01672408287',NULL,NULL,'MST. RABEA BEGUM','2021-04-20',NULL,'Female','MIM AKHTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(436,NULL,NULL,'2380375','MD.ABDUL MAZID',NULL,'01672408287',NULL,NULL,'MST.RIYA','2021-04-20',NULL,'Female','MST.MIM AKTHER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(437,NULL,NULL,'2380376','MD. MOMINUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SHULI AKTER','2021-04-20',NULL,'Female','MST. MORIYOM KHANUM MIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(438,NULL,NULL,'2380377','MD. RUHUL AMIN KHAN',NULL,'01672408287',NULL,NULL,'MST. SHARMEN SULTANA','2021-04-20',NULL,'Female','RAHATIN NAZAT KHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(439,NULL,NULL,'2380378','PROSANTO SAHA',NULL,'01672408287',NULL,NULL,'DIPA SAHA','2021-04-20',NULL,'Female','DISHA SAHA','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(440,NULL,NULL,'2380379','MOHAMMED REZAUL ISLAM ROTON',NULL,'01672408287',NULL,NULL,'MST. LILI ','2021-04-20',NULL,'Female','NUSRAT JAHAN RIYA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(441,NULL,NULL,'2380380','MD. SADEKUL ISLAM',NULL,'01672408287',NULL,NULL,'MST. SUMI BEGUM','2021-04-20',NULL,'Female','MIFTAHUL JANNAT MAISHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(442,NULL,NULL,'2380381','MD. HIMEL',NULL,'01672408287',NULL,NULL,'MOSLEMA ','2021-04-20',NULL,'Female','HOMAIRA AKTER HIMU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(443,NULL,NULL,'2380382','MD. DULAL HOSSAIN',NULL,'01672408287',NULL,NULL,'MST. MASUDA BANU','2021-04-20',NULL,'Female','RIYA MONI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(444,NULL,NULL,'2380383','MOJAMMEL HAQUE',NULL,'01672408287',NULL,NULL,'MST. NADIRA BEGUM','2021-04-20',NULL,'Female','RIYA AKTER SETU','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(445,NULL,NULL,'2380384','MD. SHAMSUL HAQUE',NULL,'01672408287',NULL,NULL,'MOST. FERDOUSY BEGUM','2021-04-20',NULL,'Female','MYMONA FARZANA FARIA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(446,NULL,NULL,'2380385','MD. JUEL ALI',NULL,'01672408287',NULL,NULL,'MST. SHAHANAZ PARVIN','2021-04-20',NULL,'Female','FARJANA YESNIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(447,NULL,NULL,'2380386','KALAM',NULL,'01672408287',NULL,NULL,'SALINA','2021-04-20',NULL,'Female','RIYA MUNI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(448,NULL,NULL,'2380387','F',NULL,'01672408287',NULL,NULL,'M','2021-04-20',NULL,'Female','Gayotry Chokroborty','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(449,NULL,NULL,'2380388','MD. SAMSUL ALAM CHOWDHURY',NULL,'01672408287',NULL,NULL,'MST. ROUSHON ARA BEGUM','2021-04-20',NULL,'Female','TASLIMA JAHAN CHOWDHURY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(450,NULL,NULL,'2380389','SREE AMIT KUMAR RAY',NULL,'01672408287',NULL,NULL,'SUMA RANI ROY','2021-04-20',NULL,'Female','SREE UISE RAY SHENHA ','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(451,NULL,NULL,'2380390','MD. ABDUL KHALEK',NULL,'01672408287',NULL,NULL,'MST. MINA BEGUM','2021-04-20',NULL,'Female','MST. ARABI AKTER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(452,NULL,NULL,'2380391','AMIMUL HUQ CHOWDHORY AKBAR',NULL,'01672408287',NULL,NULL,'TANGENA CHOWDHORY LONA','2021-04-20',NULL,'Female','ADRITA HAQUE CHOWDHORY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(453,NULL,NULL,'2380392','Md. Sarwar Jahan',NULL,'01672408287',NULL,NULL,'Mst. Nelufa Akther','2021-04-20',NULL,'Female','Sabila Jahan Sneha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(454,NULL,NULL,'2380393','Md. Abdul Hamid',NULL,'01672408287',NULL,NULL,'Mrs. Rebeka Sultana','2021-04-20',NULL,'Female','Mis. Farhana Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(455,NULL,NULL,'2380394','Rejaul Islam',NULL,'01672408287',NULL,NULL,'Fizun Nahar','2021-04-20',NULL,'Female','Fabia Binta Reza','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(456,NULL,NULL,'2380395','Abdur Rouf',NULL,'01672408287',NULL,NULL,'Sultana Razia','2021-04-20',NULL,'Female','Samiya Afrin Simu','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(469,NULL,NULL,'2380396','x',NULL,'01672408287',NULL,NULL,'y','2021-04-27',NULL,'Female','Maliha','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(485,NULL,NULL,'2380397','f name',NULL,'01672408287','1_2380397.jpg',NULL,'m Namw test','2021-05-20','2015-01-12','Male','Kabila Mohonta','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(488,'2021-2022',NULL,'2380398','MD DELWAR HOSSAIN',NULL,'01672408287',NULL,NULL,'SARMIN AKTER','2021-05-20',NULL,'Male','MD MINHAZUL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(489,NULL,NULL,'2380399','ABUL KASEM',NULL,'01672408287',NULL,NULL,'TAHMINA AKTER','2021-05-20',NULL,'Male','ABU SAYED MOLLAH','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(490,NULL,NULL,'2380400','MD ALAMGIR HOSSIN',NULL,'01672408287',NULL,NULL,'NARGIS AKTER','2021-05-20',NULL,'Male','MAHIM HASSAN NAHID','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(491,NULL,NULL,'2380401','MD ABUL HASAN',NULL,'01672408287',NULL,NULL,'RABEYA HASAN REKHA','2021-05-20',NULL,'Male','MD SALMIN HASAN SIAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(492,NULL,NULL,'2380402','MD NANNU BEPARY',NULL,'01672408287',NULL,NULL,'PARVIN','2021-05-20',NULL,'Male','MD OSMAN GONI FOYSAL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(493,NULL,NULL,'2380403','MD. MOSLEM HOSSAIN',NULL,'01672408287',NULL,NULL,'REHANA AKTER','2021-05-20',NULL,'Male','MD. YEAMIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(494,NULL,NULL,'2380404','NASIR UDDIN',NULL,'01672408287',NULL,NULL,'SHAYELA HOSSAIN','2021-05-20',NULL,'Male','NAFIS AHMED SIYAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(495,NULL,NULL,'2380405','MD. NOBU ISLAM',NULL,'01672408287',NULL,NULL,'SHARMIN AKTER','2021-05-20',NULL,'Male','MD. SIYAM ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(496,NULL,NULL,'2380406','MD MONIRUL ISLAM',NULL,'01672408287',NULL,NULL,'KHONDOKAR MANSURA','2021-05-20',NULL,'Male','MAHIBUL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(497,NULL,NULL,'2380407','MD HASIB MAHMUD',NULL,'01672408287',NULL,NULL,'MOFAZA AKTHER','2021-05-20',NULL,'Male','MD SHAKIB MAHMUD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(498,NULL,NULL,'2380408','MD ABDOR ROB',NULL,'01672408287',NULL,NULL,'MONI','2021-05-20',NULL,'Male','RAIHAN SAMI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(499,NULL,NULL,'2380409','MD. IBRAHIM HOSSAIN',NULL,'01672408287',NULL,NULL,'SAYEDA MAHFUZA','2021-05-20',NULL,'Male','MD. TAHSIN AHMAD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(500,NULL,NULL,'2380410','SAEFUR RAHMAN',NULL,'01672408287',NULL,NULL,'MAHARUN NASA','2021-05-20',NULL,'Male','FATIN ISRAKANI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(501,NULL,NULL,'2380411','MOTIUR RAHMAN',NULL,'01672408287',NULL,NULL,'TANZINA AKTER','2021-05-20',NULL,'Male','SAIFUR RAFI TAHSIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(502,NULL,NULL,'2380412','MD BADSHA KHONDOKAR',NULL,'01672408287',NULL,NULL,'MITA KHANDAKAR','2021-05-20',NULL,'Male','MD ABRAR KHONDAKAR SAHIL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(503,NULL,NULL,'2380413','MAMUN',NULL,'01672408287',NULL,NULL,'SHAMIMA BEGUM','2021-05-20',NULL,'Male','MISHNAT AL MUHI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(504,NULL,NULL,'2380414','MD ALI HOSSAN',NULL,'01672408287',NULL,NULL,'AKSIRUN NESA','2021-05-20',NULL,'Male','IBTIHAJ ABRAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(505,NULL,NULL,'2380415','MD SUJON',NULL,'01672408287',NULL,NULL,'SANJIDA AKTER','2021-05-20',NULL,'Male','MD ARAFAT ISLAM SOHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(506,NULL,NULL,'2380416','MAMUN MORSHED',NULL,'01672408287',NULL,NULL,'JASMIN AKTHER','2021-05-20',NULL,'Male','NIAZ MORSHED SHUVO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(507,NULL,NULL,'2380417','MD SALIM',NULL,'01672408287',NULL,NULL,'SELINA AKTHER','2021-05-20',NULL,'Male','MD SOHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(508,NULL,NULL,'2380418','MD AZIZUR RAHMAN',NULL,'01672408287',NULL,NULL,'MARIA AKTER RIMA','2021-05-20',NULL,'Male','MD RIFAT AHMED','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(509,NULL,NULL,'2380419','ABUL',NULL,'01672408287',NULL,NULL,'ROKEYA','2021-05-20',NULL,'Male','AZIZUL ISLAM RAFI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(510,NULL,NULL,'2380420','ABU OBAIDULLAH',NULL,'01672408287',NULL,NULL,'NURJAHAN','2021-05-20',NULL,'Male','MD FUAD HASIF BIN OBAED','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(511,NULL,NULL,'2380421','ABU SALAM',NULL,'01672408287',NULL,NULL,'REHANA BEGUM','2021-05-20',NULL,'Male','MD AIN UDDIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(512,NULL,NULL,'2380422','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','N','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(513,NULL,NULL,'2380423','JUWEL HASAN',NULL,'01672408287',NULL,NULL,'SHAKILA ISLAM','2021-05-20',NULL,'Male','MIRAZ HASAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(514,NULL,NULL,'2380424','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','N','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(515,NULL,NULL,'2380425','KAMRUL HASAN',NULL,'01672408287',NULL,NULL,'AISHA BEGUM','2021-05-20',NULL,'Male','ARAFAT HOSSAIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(516,NULL,NULL,'2380426','MD AL AMIN',NULL,'01672408287',NULL,NULL,'MRS DILARAA MIN','2021-05-20',NULL,'Male','TAMJIDA MINRAYAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(517,NULL,NULL,'2380427','MD FARADU ISLAM',NULL,'01672408287',NULL,NULL,'MIS RENUKA BEGUM','2021-05-20',NULL,'Male','MD TUHIN ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(518,NULL,NULL,'2380428','MONIR HOSSION',NULL,'01672408287',NULL,NULL,'LOVELY YESMIN MUNA','2021-05-20',NULL,'Male','ABDULLAH AL MASUD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(519,NULL,NULL,'2380429','MD SOHEL',NULL,'01672408287',NULL,NULL,'DALIA AKTAR','2021-05-20',NULL,'Male','MD ROHOMAT ISLAM PRINCE','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(520,NULL,NULL,'2380430','SHARIF HOSSAIN',NULL,'01672408287',NULL,NULL,'AKHI AKTER','2021-05-20',NULL,'Male','ALIF HASAN ARKO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(521,NULL,NULL,'2380431','MD YOUNUS',NULL,'01672408287',NULL,NULL,'NASRIN','2021-05-20',NULL,'Male','MD EALID','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(522,NULL,NULL,'2380432','MD KAJOL CHOWDHURY',NULL,'01672408287',NULL,NULL,'NAJMA CHOWDHURY','2021-05-20',NULL,'Male','ALL AKSAR CHOWDHURY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(523,NULL,NULL,'2380433','ABUL BASHAR',NULL,'01672408287',NULL,NULL,'MEEM','2021-05-20',NULL,'Male','ABDUR RAHMAN RAIYAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(524,NULL,NULL,'2380434','JOSHIM UDDIN',NULL,'01672408287',NULL,NULL,'ROZI AKTER','2021-05-20',NULL,'Male','AMIR HAMZA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(525,NULL,NULL,'2380435','MD SAROWER KARIM',NULL,'01672408287',NULL,NULL,'AKLIMA AKTER','2021-05-20',NULL,'Male','MD JUNAYED KARIM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(526,NULL,NULL,'2380436','AL AMIN',NULL,'01672408287',NULL,NULL,'ANOWARA BEGUM','2021-05-20',NULL,'Male','NIHAL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(527,NULL,NULL,'2380437','ABDUL BATEN',NULL,'01672408287',NULL,NULL,'HABIBA','2021-05-20',NULL,'Male','HABIBUL ISLAM MAHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(528,NULL,NULL,'2380438','MD SAMIM AHMAD',NULL,'01672408287',NULL,NULL,'RIPA','2021-05-20',NULL,'Male','MD SANIAT AHMAD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(529,NULL,NULL,'2380439','MANSUR AHMAD',NULL,'01672408287',NULL,NULL,'RAYHANA AKER','2021-05-20',NULL,'Male','MANJIL AHMAD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(530,NULL,NULL,'2380440','MD. HAZI AMIR',NULL,'01672408287',NULL,NULL,'TASLIMA','2021-05-20',NULL,'Male','MD. MAHIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(531,NULL,NULL,'2380441','MD ABUL BASHAR',NULL,'01672408287',NULL,NULL,'PUSHPO BASHAR','2021-05-20',NULL,'Male','MD JAHIDUL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(532,NULL,NULL,'2380442','MOTIUR RAHMAN',NULL,'01672408287',NULL,NULL,'HOSNEARA BEGUM','2021-05-20',NULL,'Male','MD. ASIF','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(533,NULL,NULL,'2380443','GIASH UDDIN',NULL,'01672408287',NULL,NULL,'RASHEDA AKTER','2021-05-20',NULL,'Male','MD. JALAL UDDIN SAMIO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(534,NULL,NULL,'2380444','MD AKTER HOSSAIN',NULL,'01672408287',NULL,NULL,'ASMA AKTER','2021-05-20',NULL,'Male','MD AFZAL HOSSAIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(535,NULL,NULL,'2380445','MD RUHUL AMIN',NULL,'01672408287',NULL,NULL,'SAMSUNNAHAR MONI','2021-05-20',NULL,'Male','MD NOOR E AMIN MASHRAFI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(536,NULL,NULL,'2380446','MD JAHANGIR ALOM',NULL,'01672408287',NULL,NULL,'KOHINUR BEGUM','2021-05-20',NULL,'Male','MD ALI HOSSAIN RODOY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(537,NULL,'AB+','2380447','MD NAZRUL ISLAM',NULL,'01672408287','1_2380447.png',NULL,'TANJILA','2021-05-20','2021-08-24','Male','MD TAHAMID ISLAM','Islam',1,NULL,NULL,NULL,'123',NULL,205,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'123',NULL,NULL,205,NULL,NULL,NULL,NULL),(538,NULL,NULL,'2380448','MD BABAR SIKDER',NULL,'01672408287',NULL,NULL,'SHANTA ISLAM','2021-05-20',NULL,'Male','MD SRABON SIKDER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(539,NULL,NULL,'2380449','MD ABDUR JABBAR KHOKON',NULL,'01672408287',NULL,NULL,'MST. HASINA AKTER RIA','2021-05-20',NULL,'Male','MD ZIANUR RAHMAN (HASIB)','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(540,NULL,NULL,'2380450','UJJAL MOLLA',NULL,'01672408287',NULL,NULL,'PARVIN BEGUM','2021-05-20',NULL,'Male','MEHEDI HASAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(541,NULL,NULL,'2380451','MD MANZURUL ISLAM',NULL,'01672408287',NULL,NULL,'RUMA AKTER','2021-05-20',NULL,'Male','MD RASIDUL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(542,NULL,NULL,'2380452','KAMRUL HASSAN',NULL,'01672408287',NULL,NULL,'NIGER SULTANA AKHI','2021-05-20',NULL,'Male','AKIL AFTAB FARHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(543,NULL,NULL,'2380453','NURUL ISLAM',NULL,'01672408287',NULL,NULL,'RAMIJA BAGOM','2021-05-20',NULL,'Male','ANAYET AHAMED','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(544,NULL,NULL,'2380454','MASUD PARVEN',NULL,'01672408287',NULL,NULL,'TESLIMA','2021-05-20',NULL,'Male','ROBIUL AWAL RAKIB','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(545,NULL,NULL,'2380455','MD ZAKIR',NULL,'01672408287',NULL,NULL,'AKLIMA','2021-05-20',NULL,'Male','MD ADNAN HOSSAIN JUNAYET','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(546,NULL,NULL,'2380456','NOOR-NOBI LUCKY',NULL,'01672408287',NULL,NULL,'HAMIDA AKTHER','2021-05-20',NULL,'Male','SOHANOOR ALVY','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(547,NULL,NULL,'2380457','HAFIJUL ISLAM',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','REDWAN ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(548,NULL,NULL,'2380458','MD NUR ISLAM',NULL,'01672408287',NULL,NULL,'ARIR KOLY','2021-05-20',NULL,'Male','MD AMIR HAMZA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(549,NULL,NULL,'2380459','MD MOKTER HOSSAIN',NULL,'01672408287',NULL,NULL,'YEASMIN','2021-05-20',NULL,'Male','MD ANANTO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(550,NULL,NULL,'2380460','AL MAMUN',NULL,'01672408287',NULL,NULL,'TAPOSHI RABIYA','2021-05-20',NULL,'Male','TANJIL AHAMED JISAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(551,NULL,NULL,'2380461','MD. MASUD',NULL,'01672408287',NULL,NULL,'PARVIN','2021-05-20',NULL,'Male','MAHIN HASAN ANIK','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(552,NULL,NULL,'2380462','ABDUL HAKIM',NULL,'01672408287',NULL,NULL,'RUBINA HAKIM','2021-05-20',NULL,'Male','MD ABDUR RAZZAK','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(553,NULL,NULL,'2380463','MD. NIZAM SIKDER',NULL,'01672408287',NULL,NULL,'FATEMA BEGUM','2021-05-20',NULL,'Male','ASHAD SIKDER','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(554,NULL,NULL,'2380464','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','MD. JIHAD ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(555,NULL,NULL,'2380465','MD MOTIUR RAHMAN',NULL,'01672408287',NULL,NULL,'MIS TANJINA BEGUM','2021-05-20',NULL,'Male','MD TANJIL RAHMAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(556,NULL,NULL,'2380466','JUSEM',NULL,'01672408287',NULL,NULL,'LUTHFUS','2021-05-20',NULL,'Male','SAIMON','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(557,NULL,NULL,'2380467','MD ANOWAR HOSSAIN',NULL,'01672408287',NULL,NULL,'NAJMA BEGUM','2021-05-20',NULL,'Male','MD ANAMUL HOSSAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(558,NULL,NULL,'2380468','MD. SHOHIDUL ISLAM MASUM',NULL,'01672408287',NULL,NULL,'MARGIA AKTER','2021-05-20',NULL,'Male','MD TAHJIB ISLAM TAHJIB','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(559,NULL,NULL,'2380469','MD. AKKAS ALI',NULL,'01672408287',NULL,NULL,'LAYLA AKTER','2021-05-20',NULL,'Male','JAMIL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(560,NULL,NULL,'2380470','ASLAM PRODAN',NULL,'01672408287',NULL,NULL,'SABINA AKTER','2021-05-20',NULL,'Male','MD JUBAYER PRODAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(561,NULL,NULL,'2380471','MOFAJJAL HOSSAIN',NULL,'01672408287',NULL,NULL,'RAHANA HOSSAIN','2021-05-20',NULL,'Male','RAHEM HOSSAIN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(562,NULL,NULL,'2380472','SHOHID',NULL,'01672408287',NULL,NULL,'BUTY AKTER','2021-05-20',NULL,'Male','JUBAYAR','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(563,NULL,NULL,'2380473','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','ARIAN PRODHAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(564,NULL,NULL,'2380474','MD RASAL',NULL,'01672408287',NULL,NULL,'BORNA','2021-05-20',NULL,'Male','MD AMINYL ISLAM SHUVO','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(565,NULL,NULL,'2380475','MD PARVEZ',NULL,'01672408287',NULL,NULL,'MRS SURAIYA AKTER','2021-05-20',NULL,'Male','MD SAJJADUL KARIM SINHA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(566,NULL,NULL,'2380476','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','N','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(567,NULL,NULL,'2380477','F',NULL,'01672408287',NULL,NULL,'M','2021-05-20',NULL,'Male','N','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(568,NULL,NULL,'2380478','MD. SHAMIM',NULL,'01672408287',NULL,NULL,'SONEY AKTER','2021-05-20',NULL,'Male','MD. YOUSUF','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(569,NULL,NULL,'2380479','MD SIPU',NULL,'01672408287',NULL,NULL,'NIBO AKTER','2021-05-20',NULL,'Male','MD SHAKIL AHMED','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(570,NULL,NULL,'2380480','ALI KABIR',NULL,'01672408287',NULL,NULL,'BORNALI','2021-05-20',NULL,'Male','SALMAN ALI RABBI','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(571,NULL,NULL,'2380481','MD. SHELIM AHMED',NULL,'01672408287',NULL,NULL,'MRS. MUKTI BEGUM','2021-05-20',NULL,'Male','MAHMUDUR RAHMAN','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(572,NULL,NULL,'2380482','MD MONIR KHAN',NULL,'01672408287',NULL,NULL,'SAGIDA AKTAR','2021-05-20',NULL,'Male','SALMAN HOSSAIN SHAD','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(573,'2020-2021','A+','2380483','MD DELWAR HOSSAIN',NULL,'01672408287',NULL,NULL,'SARMIN AKTER','2021-05-20','2021-07-14','Male','MD MINHAZUL ISLAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'2352547',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(574,NULL,NULL,'2380484','ABUL KASEM',NULL,'01672408287',NULL,NULL,'TAHMINA AKTER','2021-05-20',NULL,'Male','ABU SAYED MOLLAH','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(575,NULL,NULL,'2380485','MD ALAMGIR HOSSIN',NULL,'01672408287',NULL,NULL,'NARGIS AKTER','2021-05-20',NULL,'Male','MAHIM HASSAN NAHID','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(576,NULL,NULL,'2380486','MD ABUL HASAN',NULL,'01672408287',NULL,NULL,'RABEYA HASAN REKHA','2021-05-20',NULL,'Male','MD SALMIN HASAN SIAM','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(577,NULL,NULL,'2380487','MD NANNU BEPARY',NULL,'01672408287',NULL,NULL,'PARVIN','2021-05-20',NULL,'Male','MD OSMAN GONI FOYSAL','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(578,NULL,NULL,'2380488','Montu',NULL,'01672408287',NULL,NULL,'Sila','2021-05-27',NULL,'Male','Jhontu','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(579,NULL,'AB+','147852','Askar Ali',NULL,'01672408287','1_147852.png',NULL,'Sufia Begum ','2021-05-27','1996-11-15','Female','Afrin Rahman','Islam',1,NULL,NULL,NULL,'Dhaka',NULL,490,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Dhaka',NULL,NULL,490,NULL,NULL,NULL,NULL),(580,NULL,NULL,'12345674','x',NULL,'01672408287',NULL,NULL,'y','2021-05-29',NULL,'Male','xx','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(583,NULL,NULL,'2380489','Abdus Sattar',NULL,'01672408287',NULL,NULL,'Feroza Akter','2021-06-13',NULL,'Male','Atiqur Rahman','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(584,NULL,NULL,'2380490','Abdus Sattar',NULL,'01672408287',NULL,NULL,'Feroza Akter','2021-06-14',NULL,'Male','Atiqur Rahman','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(585,'',NULL,'2380491','F',NULL,'01672408287',NULL,NULL,'M','2021-06-15',NULL,'Male','Sumi Akter','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(586,NULL,NULL,'1478563','F',NULL,'01672408287',NULL,NULL,'M','2021-06-16',NULL,'Male','Awal Thakur','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'14785236',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(587,NULL,NULL,'12345678','F',NULL,'01672408287',NULL,NULL,'M','2021-06-16',NULL,'Male','Kawsar','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'1782369',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(588,NULL,NULL,'123456p','F',NULL,'01672408287',NULL,NULL,'M','2021-06-16',NULL,'Male','Kawsar Hosen','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'1782369',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(589,'',NULL,'123456789123459','F',NULL,'01672408287',NULL,NULL,'M','2021-06-16',NULL,'Male','Kawsar Mozumder','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'17827856969',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13873,NULL,NULL,'123456789123460','Abdus Sobahan',NULL,'01672408287','1_123456789123460.jpg',NULL,'Sultana 1','2021-09-07','2021-08-31','Female','','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'12898901',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13874,NULL,NULL,'','Shemul Hasan',NULL,'01672408287','1_.jpg',NULL,'Sultana 2','2021-09-07','2021-09-06','Male','Abdus Sobahan','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'12909090',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18403,NULL,NULL,'123456789123462','qwewe',NULL,'01672408287','1_123456789123462.jpg',NULL,'wewe','2021-09-21',NULL,'Male','test','Christian',1,NULL,NULL,NULL,NULL,NULL,NULL,'10000',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18410,'2004-2005',NULL,'123456789123463','F',NULL,'01672408287','1_123456789123463.jpg',NULL,'M','2021-09-23',NULL,'Male','Riad','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18411,NULL,NULL,'123456789123464','N',NULL,'01672408287',NULL,NULL,'M','2021-09-23',NULL,'Female','NAZIA','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18469,NULL,NULL,'1234567142','F',NULL,'01672408287',NULL,NULL,'M','2021-09-30',NULL,'Male','Riad Hossain','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'1023',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20356,NULL,'A+','123456789123466','sdsddd',NULL,'01672408287',NULL,NULL,'sdsdsd','2021-10-04','2004-02-03','Male','ms dhoni','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'100200300',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50066,NULL,NULL,'123456789123467','F',NULL,'01672408287',NULL,NULL,'M','2022-01-12',NULL,'Male','Sujon','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50067,NULL,NULL,'123456789123468','',NULL,'',NULL,NULL,'','2022-01-26',NULL,'','','',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50068,NULL,NULL,'123456789123469','',NULL,'',NULL,NULL,'','2022-01-26',NULL,'','','',1,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50069,NULL,NULL,'238039','hh',NULL,'777',NULL,NULL,'hhh','2022-01-26',NULL,'Male','hhh','Hinduism',1,NULL,NULL,NULL,NULL,NULL,NULL,'777',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50071,NULL,NULL,'123456789123470','ss',NULL,'0168525822',NULL,NULL,'ss','2022-02-03',NULL,'Male','Riad','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'258',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50072,'2022',NULL,'852','ss',NULL,'015685555246',NULL,NULL,'dd','2022-02-03',NULL,'Male','arif','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'741',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50074,'2022',NULL,'123456789123471','Mr.Y',NULL,'12365498778',NULL,NULL,'Mrs.Z','2022-02-03',NULL,'Male','Mr.X','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'852',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50076,'2024',NULL,'123456','Mr.Y',NULL,'12365478998','1_123456.jpg',NULL,'Mr.Z','2022-02-03',NULL,'Male','Mr.X','Islam',1,NULL,NULL,NULL,NULL,NULL,NULL,'852',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `student_basic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_fee_head_discard_info`
--

DROP TABLE IF EXISTS `student_fee_head_discard_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_fee_head_discard_info` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `date_executed` datetime DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `fee_head_id` bigint DEFAULT NULL,
  `identification_id` bigint NOT NULL,
  `institute_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK44s1u9xh7w3oi81a9wsodxn7v` (`fee_head_id`,`identification_id`),
  KEY `FK4l6blnub9labgtly8qksri4rn` (`identification_id`),
  KEY `FK5u5wqlg93pjumix5vnfwh77js` (`institute_id`),
  CONSTRAINT `FK45s19fgbrlo2dcdxk9mora277` FOREIGN KEY (`fee_head_id`) REFERENCES `core_setting_feehead` (`id`),
  CONSTRAINT `FK4l6blnub9labgtly8qksri4rn` FOREIGN KEY (`identification_id`) REFERENCES `student_identification` (`identification_id`),
  CONSTRAINT `FK5u5wqlg93pjumix5vnfwh77js` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_fee_head_discard_info`
--

LOCK TABLES `student_fee_head_discard_info` WRITE;
/*!40000 ALTER TABLE `student_fee_head_discard_info` DISABLE KEYS */;
INSERT INTO `student_fee_head_discard_info` VALUES (3,NULL,NULL,5,231,1),(4,'2022-01-31 15:02:41','arif',3,382,1),(6,'2022-01-31 15:19:27','arif',5,380,1);
/*!40000 ALTER TABLE `student_fee_head_discard_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_id_template`
--

DROP TABLE IF EXISTS `student_id_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_id_template` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `barcode_value` varchar(255) DEFAULT NULL,
  `template_id` int NOT NULL,
  `institute_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK8tsiqe03yxod3xp3muc31hsxi` (`institute_id`),
  CONSTRAINT `FKgqclhmye0t7h6era75d4jl3dc` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_id_template`
--

LOCK TABLES `student_id_template` WRITE;
/*!40000 ALTER TABLE `student_id_template` DISABLE KEYS */;
INSERT INTO `student_id_template` VALUES (2,'Barcode value not found',101,1);
/*!40000 ALTER TABLE `student_id_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_identification`
--

DROP TABLE IF EXISTS `student_identification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_identification` (
  `identification_id` bigint NOT NULL AUTO_INCREMENT,
  `academic_year` int NOT NULL,
  `disable_date` datetime DEFAULT NULL,
  `disable_reason` varchar(255) DEFAULT NULL,
  `migrated_status` bit(1) DEFAULT NULL,
  `migration_status` bit(1) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `student_roll` int DEFAULT NULL,
  `student_status` bit(1) DEFAULT NULL,
  `class_config_id` bigint NOT NULL,
  `group_id` bigint NOT NULL,
  `institute_id` bigint NOT NULL,
  `student_id` bigint NOT NULL,
  `student_category_id` bigint NOT NULL,
  PRIMARY KEY (`identification_id`),
  UNIQUE KEY `UK34vg3nhk53ag0k4x6b189j1d0` (`academic_year`,`institute_id`,`student_id`),
  KEY `FKdincpl5tpiysksm9ueo9hs0yr` (`class_config_id`),
  KEY `FKl8stjk9xv2gmxqwjawid8isi8` (`group_id`),
  KEY `FK16ikuq7xnfww3d8wek4onua9p` (`institute_id`),
  KEY `FK5bg5xlfjv9mghgf41rlxwmh9i` (`student_id`),
  KEY `FKqp49r3dp1k8num6wkp70nwqwv` (`student_category_id`),
  CONSTRAINT `FK16ikuq7xnfww3d8wek4onua9p` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`),
  CONSTRAINT `FK5bg5xlfjv9mghgf41rlxwmh9i` FOREIGN KEY (`student_id`) REFERENCES `student_basic` (`student_id`),
  CONSTRAINT `FKdincpl5tpiysksm9ueo9hs0yr` FOREIGN KEY (`class_config_id`) REFERENCES `core_setting_class_configuration` (`id`),
  CONSTRAINT `FKl8stjk9xv2gmxqwjawid8isi8` FOREIGN KEY (`group_id`) REFERENCES `core_setting_group` (`id`),
  CONSTRAINT `FKqp49r3dp1k8num6wkp70nwqwv` FOREIGN KEY (`student_category_id`) REFERENCES `core_setting_student_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49858 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_identification`
--

LOCK TABLES `student_identification` WRITE;
/*!40000 ALTER TABLE `student_identification` DISABLE KEYS */;
INSERT INTO `student_identification` VALUES (2,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',3,3,1,2,16),(3,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,2,_binary '',2,3,1,3,2),(4,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,3,_binary '',2,3,1,4,2),(5,2021,NULL,NULL,_binary '\0',_binary '',NULL,4,_binary '',2,3,1,5,2),(6,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',2,3,1,6,2),(7,2021,NULL,NULL,_binary '\0',_binary '',NULL,6,_binary '',2,3,1,7,2),(8,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,7,_binary '',2,3,1,8,2),(9,2021,NULL,NULL,_binary '\0',_binary '',NULL,8,_binary '',2,3,1,9,2),(10,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',2,3,1,10,2),(11,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,10,_binary '',2,3,1,11,2),(12,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,11,_binary '',2,3,1,12,2),(13,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,12,_binary '',2,3,1,13,2),(14,2021,NULL,NULL,_binary '\0',_binary '',NULL,13,_binary '',2,3,1,14,2),(15,2021,NULL,NULL,_binary '\0',_binary '',NULL,14,_binary '',2,3,1,15,2),(16,2021,NULL,NULL,_binary '\0',_binary '',NULL,15,_binary '',2,3,1,16,2),(17,2021,NULL,NULL,_binary '\0',_binary '',NULL,16,_binary '',2,3,1,17,2),(18,2021,NULL,NULL,_binary '\0',_binary '',NULL,17,_binary '',2,3,1,18,2),(19,2021,NULL,NULL,_binary '\0',_binary '',NULL,18,_binary '',2,3,1,19,2),(20,2021,NULL,NULL,_binary '\0',_binary '',NULL,19,_binary '',2,3,1,20,2),(21,2021,NULL,NULL,_binary '\0',_binary '',NULL,20,_binary '',2,3,1,21,2),(22,2021,NULL,NULL,_binary '\0',_binary '',NULL,21,_binary '',2,3,1,22,2),(23,2021,NULL,NULL,_binary '\0',_binary '',NULL,22,_binary '',2,3,1,23,2),(24,2021,NULL,NULL,_binary '\0',_binary '',NULL,23,_binary '',2,3,1,24,2),(25,2021,NULL,NULL,_binary '\0',_binary '',NULL,24,_binary '',2,3,1,25,2),(26,2021,NULL,NULL,_binary '\0',_binary '',NULL,25,_binary '',2,3,1,26,2),(27,2021,NULL,NULL,_binary '\0',_binary '',NULL,26,_binary '',2,3,1,27,2),(28,2021,NULL,NULL,_binary '\0',_binary '',NULL,27,_binary '',2,3,1,28,2),(29,2021,NULL,NULL,_binary '\0',_binary '',NULL,28,_binary '',2,3,1,29,2),(30,2021,NULL,NULL,_binary '\0',_binary '',NULL,29,_binary '',2,3,1,30,2),(31,2021,NULL,NULL,_binary '\0',_binary '',NULL,30,_binary '',2,3,1,31,2),(32,2021,NULL,NULL,_binary '\0',_binary '',NULL,31,_binary '',2,3,1,32,2),(33,2021,NULL,NULL,_binary '\0',_binary '',NULL,32,_binary '',2,3,1,33,2),(34,2021,NULL,NULL,_binary '\0',_binary '',NULL,33,_binary '',2,3,1,34,2),(35,2021,NULL,NULL,_binary '\0',_binary '',NULL,34,_binary '',2,3,1,35,2),(36,2021,NULL,NULL,_binary '\0',_binary '',NULL,35,_binary '',2,3,1,36,2),(37,2021,NULL,NULL,_binary '\0',_binary '',NULL,36,_binary '',2,3,1,37,2),(38,2021,NULL,NULL,_binary '\0',_binary '',NULL,37,_binary '',2,3,1,38,2),(39,2021,NULL,NULL,_binary '\0',_binary '',NULL,38,_binary '',2,3,1,39,2),(40,2021,NULL,NULL,_binary '\0',_binary '',NULL,39,_binary '',2,3,1,40,2),(41,2021,NULL,NULL,_binary '\0',_binary '',NULL,40,_binary '',2,3,1,41,2),(42,2021,NULL,NULL,_binary '\0',_binary '',NULL,41,_binary '',2,3,1,42,2),(43,2021,NULL,NULL,_binary '\0',_binary '',NULL,42,_binary '',2,3,1,43,2),(44,2021,NULL,NULL,_binary '\0',_binary '',NULL,43,_binary '',2,3,1,44,2),(45,2021,NULL,NULL,_binary '\0',_binary '',NULL,44,_binary '',2,3,1,45,2),(46,2021,NULL,NULL,_binary '\0',_binary '',NULL,45,_binary '',2,3,1,46,2),(47,2021,NULL,NULL,_binary '\0',_binary '',NULL,46,_binary '',2,3,1,47,2),(48,2021,NULL,NULL,_binary '\0',_binary '',NULL,47,_binary '',2,3,1,48,2),(49,2021,NULL,NULL,_binary '\0',_binary '',NULL,48,_binary '',2,3,1,49,2),(50,2021,NULL,NULL,_binary '\0',_binary '',NULL,49,_binary '',2,3,1,50,2),(51,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',2,3,1,51,2),(52,2021,NULL,NULL,_binary '\0',_binary '',NULL,51,_binary '',2,3,1,52,2),(53,2021,NULL,NULL,_binary '\0',_binary '',NULL,52,_binary '',2,3,1,53,2),(54,2021,NULL,NULL,_binary '\0',_binary '',NULL,53,_binary '',2,3,1,54,2),(55,2021,NULL,NULL,_binary '\0',_binary '',NULL,54,_binary '',2,3,1,55,2),(56,2021,NULL,NULL,_binary '\0',_binary '',NULL,55,_binary '',2,3,1,56,2),(57,2021,NULL,NULL,_binary '\0',_binary '',NULL,56,_binary '',2,3,1,57,2),(58,2021,NULL,NULL,_binary '\0',_binary '',NULL,57,_binary '',2,3,1,58,2),(59,2021,NULL,NULL,_binary '\0',_binary '',NULL,58,_binary '',2,3,1,59,2),(60,2021,NULL,NULL,_binary '\0',_binary '',NULL,59,_binary '',2,3,1,60,2),(61,2021,NULL,NULL,_binary '\0',_binary '',NULL,60,_binary '',2,3,1,61,2),(62,2021,NULL,NULL,_binary '\0',_binary '',NULL,61,_binary '',2,3,1,62,2),(63,2021,NULL,NULL,_binary '\0',_binary '',NULL,62,_binary '',2,3,1,63,2),(64,2021,NULL,NULL,_binary '\0',_binary '',NULL,63,_binary '',2,3,1,64,2),(65,2021,NULL,NULL,_binary '\0',_binary '',NULL,64,_binary '',2,3,1,65,2),(66,2021,NULL,NULL,_binary '\0',_binary '',NULL,65,_binary '',2,3,1,66,2),(67,2021,NULL,NULL,_binary '\0',_binary '',NULL,66,_binary '',2,3,1,67,2),(68,2021,NULL,NULL,_binary '\0',_binary '',NULL,67,_binary '',2,3,1,68,2),(69,2021,NULL,NULL,_binary '\0',_binary '',NULL,68,_binary '',2,3,1,69,2),(70,2021,NULL,NULL,_binary '\0',_binary '',NULL,70,_binary '',2,3,1,70,2),(71,2021,NULL,NULL,_binary '\0',_binary '',NULL,71,_binary '',2,3,1,71,2),(72,2021,NULL,NULL,_binary '\0',_binary '',NULL,72,_binary '',2,3,1,72,2),(73,2021,NULL,NULL,_binary '\0',_binary '',NULL,73,_binary '',2,3,1,73,2),(74,2021,NULL,NULL,_binary '\0',_binary '',NULL,74,_binary '',2,3,1,74,2),(75,2021,NULL,NULL,_binary '\0',_binary '',NULL,75,_binary '',2,3,1,75,2),(76,2021,NULL,NULL,_binary '\0',_binary '',NULL,76,_binary '',2,3,1,76,2),(77,2021,NULL,NULL,_binary '\0',_binary '',NULL,77,_binary '',2,3,1,77,2),(198,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',3,3,1,200,2),(199,2021,NULL,NULL,_binary '\0',_binary '',NULL,2,_binary '',3,3,1,201,2),(200,2021,NULL,NULL,_binary '\0',_binary '',NULL,3,_binary '',3,3,1,202,2),(201,2021,NULL,NULL,_binary '\0',_binary '',NULL,4,_binary '',3,3,1,203,2),(202,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',3,3,1,204,2),(203,2021,NULL,NULL,_binary '\0',_binary '',NULL,6,_binary '',3,3,1,205,2),(204,2021,NULL,NULL,_binary '\0',_binary '',NULL,7,_binary '',3,3,1,206,2),(205,2021,NULL,NULL,_binary '\0',_binary '',NULL,8,_binary '',3,3,1,207,2),(206,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',3,3,1,208,2),(207,2021,NULL,NULL,_binary '\0',_binary '',NULL,10,_binary '',3,3,1,209,2),(208,2021,NULL,NULL,_binary '\0',_binary '',NULL,11,_binary '',3,3,1,210,2),(209,2021,NULL,NULL,_binary '\0',_binary '',NULL,12,_binary '',3,3,1,211,2),(210,2021,NULL,NULL,_binary '\0',_binary '',NULL,13,_binary '',3,3,1,212,2),(211,2021,NULL,NULL,_binary '\0',_binary '',NULL,14,_binary '',3,3,1,213,2),(212,2021,NULL,NULL,_binary '\0',_binary '',NULL,15,_binary '',3,3,1,214,2),(213,2021,NULL,NULL,_binary '\0',_binary '',NULL,16,_binary '',3,3,1,215,2),(214,2021,NULL,NULL,_binary '\0',_binary '',NULL,17,_binary '',3,3,1,216,2),(215,2021,NULL,NULL,_binary '\0',_binary '',NULL,18,_binary '',3,3,1,217,2),(216,2021,NULL,NULL,_binary '\0',_binary '',NULL,19,_binary '',3,3,1,218,2),(217,2021,NULL,NULL,_binary '\0',_binary '',NULL,20,_binary '',3,3,1,219,2),(218,2021,NULL,NULL,_binary '\0',_binary '',NULL,21,_binary '',3,3,1,220,2),(219,2021,NULL,NULL,_binary '\0',_binary '',NULL,22,_binary '',3,3,1,221,2),(220,2021,NULL,NULL,_binary '\0',_binary '',NULL,23,_binary '',3,3,1,222,2),(221,2021,NULL,NULL,_binary '\0',_binary '',NULL,26,_binary '',3,3,1,223,2),(222,2021,NULL,NULL,_binary '\0',_binary '',NULL,27,_binary '',3,3,1,224,2),(223,2021,NULL,NULL,_binary '\0',_binary '',NULL,28,_binary '',3,3,1,225,2),(224,2021,NULL,NULL,_binary '\0',_binary '',NULL,29,_binary '',3,3,1,226,2),(225,2021,NULL,NULL,_binary '\0',_binary '',NULL,30,_binary '',3,3,1,227,2),(226,2021,NULL,NULL,_binary '\0',_binary '',NULL,31,_binary '',3,3,1,228,2),(227,2021,NULL,NULL,_binary '\0',_binary '',NULL,32,_binary '',3,3,1,229,2),(228,2021,NULL,NULL,_binary '\0',_binary '',NULL,33,_binary '',3,3,1,230,2),(229,2021,NULL,NULL,_binary '\0',_binary '',NULL,34,_binary '',3,3,1,231,2),(230,2021,NULL,NULL,_binary '\0',_binary '',NULL,36,_binary '',3,3,1,232,2),(231,2021,NULL,NULL,_binary '\0',_binary '',NULL,37,_binary '',3,3,1,233,2),(232,2021,NULL,NULL,_binary '\0',_binary '',NULL,38,_binary '',3,3,1,234,2),(233,2021,NULL,NULL,_binary '\0',_binary '',NULL,39,_binary '',3,3,1,235,2),(234,2021,NULL,NULL,_binary '\0',_binary '',NULL,40,_binary '',3,3,1,236,2),(235,2021,NULL,NULL,_binary '\0',_binary '',NULL,41,_binary '',3,3,1,237,2),(236,2021,NULL,NULL,_binary '\0',_binary '',NULL,42,_binary '',3,3,1,238,2),(237,2021,NULL,NULL,_binary '\0',_binary '',NULL,43,_binary '',3,3,1,239,2),(238,2021,NULL,NULL,_binary '\0',_binary '',NULL,45,_binary '',3,3,1,240,2),(239,2021,NULL,NULL,_binary '\0',_binary '',NULL,46,_binary '',3,3,1,241,2),(240,2021,NULL,NULL,_binary '\0',_binary '',NULL,47,_binary '',3,3,1,242,2),(241,2021,NULL,NULL,_binary '\0',_binary '',NULL,48,_binary '',3,3,1,243,2),(242,2021,NULL,NULL,_binary '\0',_binary '',NULL,49,_binary '',3,3,1,244,2),(243,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,245,2),(244,2021,NULL,NULL,_binary '\0',_binary '',NULL,51,_binary '',3,3,1,246,2),(245,2021,NULL,NULL,_binary '\0',_binary '',NULL,52,_binary '',3,3,1,247,2),(246,2021,NULL,NULL,_binary '\0',_binary '',NULL,53,_binary '',3,3,1,248,2),(247,2021,NULL,NULL,_binary '\0',_binary '',NULL,54,_binary '',3,3,1,249,2),(248,2021,NULL,NULL,_binary '\0',_binary '',NULL,55,_binary '',3,3,1,250,2),(249,2021,NULL,NULL,_binary '\0',_binary '',NULL,56,_binary '',3,3,1,251,2),(250,2021,NULL,NULL,_binary '\0',_binary '',NULL,57,_binary '',3,3,1,252,2),(251,2021,NULL,NULL,_binary '\0',_binary '',NULL,58,_binary '',3,3,1,253,2),(252,2021,NULL,NULL,_binary '\0',_binary '',NULL,60,_binary '',3,3,1,254,2),(253,2021,NULL,NULL,_binary '\0',_binary '',NULL,61,_binary '',3,3,1,255,2),(254,2021,NULL,NULL,_binary '\0',_binary '',NULL,62,_binary '',3,3,1,256,2),(255,2021,NULL,NULL,_binary '\0',_binary '',NULL,63,_binary '',3,3,1,257,2),(256,2021,NULL,NULL,_binary '\0',_binary '',NULL,64,_binary '',3,3,1,258,2),(257,2021,NULL,NULL,_binary '\0',_binary '',NULL,65,_binary '',3,3,1,259,2),(258,2021,NULL,NULL,_binary '\0',_binary '',NULL,68,_binary '',3,3,1,260,2),(259,2021,NULL,NULL,_binary '\0',_binary '',NULL,69,_binary '',3,3,1,261,2),(260,2021,NULL,NULL,_binary '\0',_binary '',NULL,70,_binary '',3,3,1,262,2),(261,2021,NULL,NULL,_binary '\0',_binary '',NULL,71,_binary '',3,3,1,263,2),(262,2021,NULL,NULL,_binary '\0',_binary '',NULL,72,_binary '',3,3,1,264,2),(263,2021,NULL,NULL,_binary '\0',_binary '',NULL,73,_binary '',3,3,1,265,2),(264,2021,NULL,NULL,_binary '\0',_binary '',NULL,74,_binary '',3,3,1,266,2),(265,2021,NULL,NULL,_binary '\0',_binary '',NULL,75,_binary '',3,3,1,267,2),(266,2021,NULL,NULL,_binary '\0',_binary '',NULL,76,_binary '',3,3,1,268,2),(267,2021,NULL,NULL,_binary '\0',_binary '',NULL,78,_binary '',3,3,1,269,2),(268,2021,NULL,NULL,_binary '\0',_binary '',NULL,79,_binary '',3,3,1,270,2),(269,2021,NULL,NULL,_binary '\0',_binary '',NULL,80,_binary '',3,3,1,271,2),(270,2021,NULL,NULL,_binary '\0',_binary '',NULL,81,_binary '',3,3,1,272,2),(271,2021,NULL,NULL,_binary '\0',_binary '',NULL,82,_binary '',3,3,1,273,2),(316,2021,NULL,NULL,_binary '\0',_binary '',NULL,2,_binary '',4,2,1,318,2),(317,2021,NULL,NULL,_binary '\0',_binary '',NULL,3,_binary '',4,2,1,319,2),(318,2021,NULL,NULL,_binary '\0',_binary '',NULL,4,_binary '',4,2,1,320,2),(319,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',4,2,1,321,2),(320,2021,NULL,NULL,_binary '\0',_binary '',NULL,6,_binary '',4,2,1,322,2),(321,2021,NULL,NULL,_binary '\0',_binary '',NULL,7,_binary '',4,2,1,323,2),(322,2021,NULL,NULL,_binary '\0',_binary '',NULL,8,_binary '',4,2,1,324,2),(323,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',4,2,1,325,2),(324,2021,NULL,NULL,_binary '\0',_binary '',NULL,10,_binary '',4,2,1,326,2),(325,2021,NULL,NULL,_binary '\0',_binary '',NULL,11,_binary '',4,2,1,327,2),(326,2021,NULL,NULL,_binary '\0',_binary '',NULL,12,_binary '',4,2,1,328,2),(327,2021,NULL,NULL,_binary '\0',_binary '',NULL,13,_binary '',4,2,1,329,2),(328,2021,NULL,NULL,_binary '\0',_binary '',NULL,14,_binary '',4,2,1,330,2),(329,2021,NULL,NULL,_binary '\0',_binary '',NULL,15,_binary '',4,2,1,331,2),(330,2021,NULL,NULL,_binary '\0',_binary '',NULL,16,_binary '',4,2,1,332,2),(331,2021,NULL,NULL,_binary '\0',_binary '',NULL,17,_binary '',4,2,1,333,2),(332,2021,NULL,NULL,_binary '\0',_binary '',NULL,18,_binary '',4,2,1,334,2),(333,2021,NULL,NULL,_binary '\0',_binary '',NULL,19,_binary '',4,2,1,335,2),(334,2021,NULL,NULL,_binary '\0',_binary '',NULL,20,_binary '',4,2,1,336,2),(335,2021,NULL,NULL,_binary '\0',_binary '',NULL,22,_binary '',4,2,1,337,2),(336,2021,NULL,NULL,_binary '\0',_binary '',NULL,23,_binary '',4,2,1,338,2),(337,2021,NULL,NULL,_binary '\0',_binary '',NULL,24,_binary '',4,2,1,339,2),(338,2021,NULL,NULL,_binary '\0',_binary '',NULL,25,_binary '',4,2,1,340,2),(339,2021,NULL,NULL,_binary '\0',_binary '',NULL,26,_binary '',4,2,1,341,2),(340,2021,NULL,NULL,_binary '\0',_binary '',NULL,27,_binary '',4,2,1,342,2),(341,2021,NULL,NULL,_binary '\0',_binary '',NULL,28,_binary '',4,2,1,343,2),(342,2021,NULL,NULL,_binary '\0',_binary '',NULL,29,_binary '',4,2,1,344,2),(343,2021,NULL,NULL,_binary '\0',_binary '',NULL,30,_binary '',4,2,1,345,2),(344,2021,NULL,NULL,_binary '\0',_binary '',NULL,31,_binary '',4,2,1,346,2),(345,2021,NULL,NULL,_binary '\0',_binary '',NULL,32,_binary '',4,2,1,347,2),(346,2021,NULL,NULL,_binary '\0',_binary '',NULL,33,_binary '',4,2,1,348,2),(347,2021,NULL,NULL,_binary '\0',_binary '',NULL,34,_binary '',4,2,1,349,2),(348,2021,NULL,NULL,_binary '\0',_binary '',NULL,35,_binary '',4,2,1,350,2),(349,2021,NULL,NULL,_binary '\0',_binary '',NULL,36,_binary '',4,2,1,351,2),(350,2021,NULL,NULL,_binary '\0',_binary '',NULL,37,_binary '',4,2,1,352,2),(351,2021,NULL,NULL,_binary '\0',_binary '',NULL,38,_binary '',4,2,1,353,2),(352,2021,NULL,NULL,_binary '\0',_binary '',NULL,39,_binary '',4,2,1,354,2),(353,2021,NULL,NULL,_binary '\0',_binary '',NULL,40,_binary '',4,2,1,355,2),(354,2021,NULL,NULL,_binary '\0',_binary '',NULL,41,_binary '',4,2,1,356,2),(355,2021,NULL,NULL,_binary '\0',_binary '',NULL,42,_binary '',4,2,1,357,2),(356,2021,NULL,NULL,_binary '\0',_binary '',NULL,43,_binary '',4,2,1,358,2),(357,2021,NULL,NULL,_binary '\0',_binary '',NULL,44,_binary '',4,2,1,359,2),(358,2021,NULL,NULL,_binary '\0',_binary '',NULL,45,_binary '',4,2,1,360,2),(359,2021,NULL,NULL,_binary '\0',_binary '',NULL,46,_binary '',4,2,1,361,2),(360,2021,NULL,NULL,_binary '\0',_binary '',NULL,47,_binary '',4,2,1,362,2),(361,2021,NULL,NULL,_binary '\0',_binary '',NULL,48,_binary '',4,2,1,363,2),(362,2021,NULL,NULL,_binary '\0',_binary '',NULL,48,_binary '',4,2,1,364,2),(363,2021,NULL,NULL,_binary '\0',_binary '',NULL,49,_binary '',4,2,1,365,2),(364,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',4,2,1,366,2),(365,2021,NULL,NULL,_binary '\0',_binary '',NULL,51,_binary '',4,2,1,367,2),(366,2021,NULL,NULL,_binary '\0',_binary '',NULL,52,_binary '',4,2,1,368,2),(367,2021,NULL,NULL,_binary '\0',_binary '',NULL,53,_binary '',4,2,1,369,2),(368,2021,NULL,NULL,_binary '\0',_binary '',NULL,54,_binary '',4,2,1,370,2),(369,2021,NULL,NULL,_binary '\0',_binary '',NULL,56,_binary '',4,2,1,371,2),(370,2021,NULL,NULL,_binary '\0',_binary '',NULL,57,_binary '',4,2,1,372,2),(371,2021,NULL,NULL,_binary '\0',_binary '',NULL,58,_binary '',4,2,1,373,2),(372,2021,NULL,NULL,_binary '\0',_binary '',NULL,59,_binary '',4,2,1,374,2),(373,2021,NULL,NULL,_binary '\0',_binary '',NULL,60,_binary '',4,2,1,375,2),(374,2021,NULL,NULL,_binary '\0',_binary '',NULL,61,_binary '',4,2,1,376,2),(375,2021,NULL,NULL,_binary '\0',_binary '',NULL,63,_binary '',4,2,1,377,2),(376,2021,NULL,NULL,_binary '\0',_binary '',NULL,64,_binary '',4,2,1,378,2),(377,2021,NULL,NULL,_binary '\0',_binary '',NULL,65,_binary '',4,2,1,379,2),(378,2021,NULL,NULL,_binary '\0',_binary '',NULL,66,_binary '',4,2,1,380,2),(379,2021,NULL,NULL,_binary '\0',_binary '',NULL,67,_binary '',4,2,1,381,2),(380,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',5,4,1,383,2),(381,2021,NULL,NULL,_binary '\0',_binary '',NULL,2,_binary '',5,4,1,384,2),(382,2021,NULL,NULL,_binary '\0',_binary '',NULL,3,_binary '',5,4,1,385,2),(383,2021,NULL,NULL,_binary '\0',_binary '',NULL,4,_binary '',5,4,1,386,2),(384,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',5,4,1,387,2),(385,2021,NULL,NULL,_binary '\0',_binary '',NULL,6,_binary '',5,4,1,388,2),(386,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,7,_binary '',5,4,1,389,2),(387,2021,NULL,NULL,_binary '\0',_binary '',NULL,8,_binary '',5,4,1,390,2),(388,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',5,4,1,391,2),(389,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,10,_binary '',5,4,1,392,2),(390,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,11,_binary '',5,4,1,393,2),(391,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,12,_binary '',5,4,1,394,2),(392,2021,NULL,NULL,_binary '\0',_binary '',NULL,13,_binary '',5,4,1,395,2),(393,2021,NULL,NULL,_binary '\0',_binary '',NULL,14,_binary '',5,4,1,396,2),(394,2021,NULL,NULL,_binary '\0',_binary '',NULL,15,_binary '',5,4,1,397,2),(395,2021,NULL,NULL,_binary '\0',_binary '',NULL,16,_binary '',5,4,1,398,2),(396,2021,NULL,NULL,_binary '\0',_binary '',NULL,17,_binary '',5,4,1,399,2),(397,2021,NULL,NULL,_binary '\0',_binary '',NULL,18,_binary '',5,4,1,400,2),(398,2021,NULL,NULL,_binary '\0',_binary '',NULL,19,_binary '',5,4,1,401,2),(399,2021,NULL,NULL,_binary '\0',_binary '',NULL,20,_binary '',5,4,1,402,2),(400,2021,NULL,NULL,_binary '\0',_binary '',NULL,21,_binary '',5,4,1,403,2),(401,2021,NULL,NULL,_binary '\0',_binary '',NULL,22,_binary '',5,4,1,404,2),(402,2021,NULL,NULL,_binary '\0',_binary '',NULL,23,_binary '',5,4,1,405,2),(403,2021,NULL,NULL,_binary '\0',_binary '',NULL,26,_binary '',5,4,1,406,2),(404,2021,NULL,NULL,_binary '\0',_binary '',NULL,27,_binary '',5,4,1,407,2),(405,2021,NULL,NULL,_binary '\0',_binary '',NULL,28,_binary '',5,4,1,408,2),(406,2021,NULL,NULL,_binary '\0',_binary '',NULL,29,_binary '',5,4,1,409,2),(407,2021,NULL,NULL,_binary '\0',_binary '',NULL,30,_binary '',5,4,1,410,2),(408,2021,NULL,NULL,_binary '\0',_binary '',NULL,31,_binary '',5,4,1,411,2),(409,2021,NULL,NULL,_binary '\0',_binary '',NULL,32,_binary '',5,4,1,412,2),(410,2021,NULL,NULL,_binary '\0',_binary '',NULL,33,_binary '',5,7,1,413,2),(411,2021,NULL,NULL,_binary '\0',_binary '',NULL,34,_binary '',5,7,1,414,2),(412,2021,NULL,NULL,_binary '\0',_binary '',NULL,36,_binary '',5,7,1,415,2),(413,2021,NULL,NULL,_binary '\0',_binary '',NULL,37,_binary '',5,7,1,416,2),(414,2021,NULL,NULL,_binary '\0',_binary '',NULL,38,_binary '',5,7,1,417,2),(415,2021,NULL,NULL,_binary '\0',_binary '',NULL,39,_binary '',5,7,1,418,2),(416,2021,NULL,NULL,_binary '\0',_binary '',NULL,40,_binary '',5,7,1,419,2),(417,2021,NULL,NULL,_binary '\0',_binary '',NULL,41,_binary '',5,4,1,420,2),(418,2021,NULL,NULL,_binary '\0',_binary '',NULL,42,_binary '',5,4,1,421,2),(419,2021,NULL,NULL,_binary '\0',_binary '',NULL,43,_binary '',5,4,1,422,2),(420,2021,NULL,NULL,_binary '\0',_binary '',NULL,45,_binary '',5,4,1,423,2),(421,2021,NULL,NULL,_binary '\0',_binary '',NULL,46,_binary '',5,4,1,424,2),(422,2021,NULL,NULL,_binary '\0',_binary '',NULL,47,_binary '',5,4,1,425,2),(423,2021,NULL,NULL,_binary '\0',_binary '',NULL,48,_binary '',5,4,1,426,2),(424,2021,NULL,NULL,_binary '\0',_binary '',NULL,49,_binary '',5,4,1,427,2),(425,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',5,4,1,428,2),(426,2021,NULL,NULL,_binary '\0',_binary '',NULL,51,_binary '',5,4,1,429,2),(427,2021,NULL,NULL,_binary '\0',_binary '',NULL,52,_binary '',5,4,1,430,2),(428,2021,NULL,NULL,_binary '\0',_binary '',NULL,53,_binary '',5,4,1,431,2),(429,2021,NULL,NULL,_binary '\0',_binary '',NULL,54,_binary '',5,4,1,432,2),(430,2021,NULL,NULL,_binary '\0',_binary '',NULL,55,_binary '',5,4,1,433,2),(431,2021,NULL,NULL,_binary '\0',_binary '',NULL,56,_binary '',5,4,1,434,2),(432,2021,NULL,NULL,_binary '\0',_binary '',NULL,57,_binary '',5,4,1,435,2),(433,2021,NULL,NULL,_binary '\0',_binary '',NULL,58,_binary '',5,4,1,436,2),(434,2021,NULL,NULL,_binary '\0',_binary '',NULL,60,_binary '',5,4,1,437,2),(435,2021,NULL,NULL,_binary '\0',_binary '',NULL,61,_binary '',5,4,1,438,2),(436,2021,NULL,NULL,_binary '\0',_binary '',NULL,62,_binary '',5,4,1,439,2),(437,2021,NULL,NULL,_binary '\0',_binary '',NULL,63,_binary '',5,4,1,440,2),(438,2021,NULL,NULL,_binary '\0',_binary '',NULL,64,_binary '',5,4,1,441,2),(439,2021,NULL,NULL,_binary '\0',_binary '',NULL,65,_binary '',5,4,1,442,2),(440,2021,NULL,NULL,_binary '\0',_binary '',NULL,68,_binary '',5,8,1,443,2),(441,2021,NULL,NULL,_binary '\0',_binary '',NULL,69,_binary '',5,8,1,444,2),(442,2021,NULL,NULL,_binary '\0',_binary '',NULL,70,_binary '',5,8,1,445,2),(443,2021,NULL,NULL,_binary '\0',_binary '',NULL,71,_binary '',5,8,1,446,2),(444,2021,NULL,NULL,_binary '\0',_binary '',NULL,72,_binary '',5,8,1,447,2),(445,2021,NULL,NULL,_binary '\0',_binary '',NULL,73,_binary '',5,8,1,448,2),(446,2021,NULL,NULL,_binary '\0',_binary '',NULL,74,_binary '',5,8,1,449,2),(447,2021,NULL,NULL,_binary '\0',_binary '',NULL,75,_binary '',5,8,1,450,2),(448,2021,NULL,NULL,_binary '\0',_binary '',NULL,76,_binary '',5,8,1,451,2),(449,2021,NULL,NULL,_binary '\0',_binary '',NULL,78,_binary '',5,8,1,452,2),(450,2021,NULL,NULL,_binary '\0',_binary '',NULL,79,_binary '',5,4,1,453,2),(451,2021,NULL,NULL,_binary '\0',_binary '',NULL,80,_binary '',5,4,1,454,2),(452,2021,NULL,NULL,_binary '\0',_binary '',NULL,81,_binary '',5,4,1,455,2),(453,2021,NULL,NULL,_binary '\0',_binary '',NULL,82,_binary '',5,4,1,456,2),(466,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',4,2,1,469,2),(482,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',21,3,1,485,4),(484,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,1,_binary '',2,3,1,488,2),(485,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,3,_binary '',2,3,1,489,4),(486,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',2,3,1,490,4),(487,2021,NULL,NULL,_binary '\0',_binary '',NULL,7,_binary '',2,3,1,491,4),(488,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',2,3,1,492,4),(489,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,11,_binary '',2,3,1,493,4),(490,2021,NULL,NULL,_binary '\0',_binary '',NULL,13,_binary '',2,3,1,494,4),(491,2021,NULL,NULL,_binary '\0',_binary '',NULL,15,_binary '',2,3,1,495,4),(492,2021,NULL,NULL,_binary '\0',_binary '',NULL,17,_binary '',2,3,1,496,4),(493,2021,NULL,NULL,_binary '\0',_binary '',NULL,19,_binary '',2,3,1,497,4),(494,2021,NULL,NULL,_binary '\0',_binary '',NULL,21,_binary '',2,3,1,498,4),(495,2021,NULL,NULL,_binary '\0',_binary '',NULL,23,_binary '',2,3,1,499,4),(496,2021,NULL,NULL,_binary '\0',_binary '',NULL,25,_binary '',2,3,1,500,4),(497,2021,NULL,NULL,_binary '\0',_binary '',NULL,27,_binary '',2,3,1,501,4),(498,2021,NULL,NULL,_binary '\0',_binary '',NULL,29,_binary '',2,3,1,502,4),(499,2021,NULL,NULL,_binary '\0',_binary '',NULL,31,_binary '',2,3,1,503,4),(500,2021,NULL,NULL,_binary '\0',_binary '',NULL,33,_binary '',2,3,1,504,4),(501,2021,NULL,NULL,_binary '\0',_binary '',NULL,35,_binary '',2,3,1,505,4),(502,2021,NULL,NULL,_binary '\0',_binary '',NULL,37,_binary '',2,3,1,506,4),(503,2021,NULL,NULL,_binary '\0',_binary '',NULL,39,_binary '',2,3,1,507,4),(504,2021,NULL,NULL,_binary '\0',_binary '',NULL,41,_binary '',2,3,1,508,4),(505,2021,NULL,NULL,_binary '\0',_binary '',NULL,43,_binary '',2,3,1,509,4),(506,2021,NULL,NULL,_binary '\0',_binary '',NULL,45,_binary '',2,3,1,510,4),(507,2021,NULL,NULL,_binary '\0',_binary '',NULL,47,_binary '',2,3,1,511,4),(508,2021,NULL,NULL,_binary '\0',_binary '',NULL,49,_binary '',2,3,1,512,4),(509,2021,NULL,NULL,_binary '\0',_binary '',NULL,51,_binary '',2,3,1,513,4),(510,2021,NULL,NULL,_binary '\0',_binary '',NULL,53,_binary '',2,3,1,514,4),(511,2021,NULL,NULL,_binary '\0',_binary '',NULL,55,_binary '',2,3,1,515,4),(512,2021,NULL,NULL,_binary '\0',_binary '',NULL,57,_binary '',2,3,1,516,4),(513,2021,NULL,NULL,_binary '\0',_binary '',NULL,59,_binary '',2,3,1,517,4),(514,2021,NULL,NULL,_binary '\0',_binary '',NULL,61,_binary '',2,3,1,518,4),(515,2021,NULL,NULL,_binary '\0',_binary '',NULL,63,_binary '',2,3,1,519,4),(516,2021,NULL,NULL,_binary '\0',_binary '',NULL,65,_binary '',2,3,1,520,4),(517,2021,NULL,NULL,_binary '\0',_binary '',NULL,67,_binary '',2,3,1,521,4),(518,2021,NULL,NULL,_binary '\0',_binary '',NULL,69,_binary '',2,3,1,522,4),(519,2021,NULL,NULL,_binary '\0',_binary '',NULL,71,_binary '',2,3,1,523,4),(520,2021,NULL,NULL,_binary '\0',_binary '',NULL,73,_binary '',2,3,1,524,4),(521,2021,NULL,NULL,_binary '\0',_binary '',NULL,75,_binary '',2,3,1,525,4),(522,2021,NULL,NULL,_binary '\0',_binary '',NULL,77,_binary '',2,3,1,526,4),(523,2021,NULL,NULL,_binary '\0',_binary '',NULL,79,_binary '',2,3,1,527,4),(524,2021,NULL,NULL,_binary '\0',_binary '',NULL,81,_binary '',2,3,1,528,4),(525,2021,NULL,NULL,_binary '\0',_binary '',NULL,83,_binary '',2,3,1,529,4),(526,2021,NULL,NULL,_binary '\0',_binary '',NULL,85,_binary '',2,3,1,530,4),(527,2021,NULL,NULL,_binary '\0',_binary '',NULL,87,_binary '',2,3,1,531,4),(528,2021,NULL,NULL,_binary '\0',_binary '',NULL,89,_binary '',2,3,1,532,4),(529,2021,NULL,NULL,_binary '\0',_binary '',NULL,91,_binary '',2,3,1,533,4),(530,2021,NULL,NULL,_binary '\0',_binary '',NULL,93,_binary '',2,3,1,534,4),(531,2021,NULL,NULL,_binary '\0',_binary '',NULL,95,_binary '',2,3,1,535,4),(532,2021,NULL,NULL,_binary '\0',_binary '',NULL,97,_binary '',2,3,1,536,4),(533,2021,NULL,NULL,_binary '\0',_binary '',NULL,99,_binary '',2,3,1,537,4),(534,2021,NULL,NULL,_binary '\0',_binary '',NULL,101,_binary '',2,3,1,538,4),(535,2021,NULL,NULL,_binary '\0',_binary '',NULL,103,_binary '',2,3,1,539,4),(536,2021,NULL,NULL,_binary '\0',_binary '',NULL,105,_binary '',2,3,1,540,4),(537,2021,NULL,NULL,_binary '\0',_binary '',NULL,107,_binary '',2,3,1,541,4),(538,2021,NULL,NULL,_binary '\0',_binary '',NULL,109,_binary '',2,3,1,542,4),(539,2021,NULL,NULL,_binary '\0',_binary '',NULL,111,_binary '',2,3,1,543,4),(540,2021,NULL,NULL,_binary '\0',_binary '',NULL,113,_binary '',2,3,1,544,4),(541,2021,NULL,NULL,_binary '\0',_binary '',NULL,115,_binary '',2,3,1,545,4),(542,2021,NULL,NULL,_binary '\0',_binary '',NULL,117,_binary '',2,3,1,546,4),(543,2021,NULL,NULL,_binary '\0',_binary '',NULL,119,_binary '',2,3,1,547,4),(544,2021,NULL,NULL,_binary '\0',_binary '',NULL,121,_binary '',2,3,1,548,4),(545,2021,NULL,NULL,_binary '\0',_binary '',NULL,123,_binary '',2,3,1,549,4),(546,2021,NULL,NULL,_binary '\0',_binary '',NULL,125,_binary '',2,3,1,550,4),(547,2021,NULL,NULL,_binary '\0',_binary '',NULL,127,_binary '',2,3,1,551,4),(548,2021,NULL,NULL,_binary '\0',_binary '',NULL,129,_binary '',2,3,1,552,4),(549,2021,NULL,NULL,_binary '\0',_binary '',NULL,131,_binary '',2,3,1,553,4),(550,2021,NULL,NULL,_binary '\0',_binary '',NULL,133,_binary '',2,3,1,554,4),(551,2021,NULL,NULL,_binary '\0',_binary '',NULL,135,_binary '',2,3,1,555,4),(552,2021,NULL,NULL,_binary '\0',_binary '',NULL,137,_binary '',2,3,1,556,4),(553,2021,NULL,NULL,_binary '\0',_binary '',NULL,139,_binary '',2,3,1,557,4),(554,2021,NULL,NULL,_binary '\0',_binary '',NULL,141,_binary '',2,3,1,558,4),(555,2021,NULL,NULL,_binary '\0',_binary '',NULL,143,_binary '',2,3,1,559,4),(556,2021,NULL,NULL,_binary '\0',_binary '',NULL,145,_binary '',2,3,1,560,4),(557,2021,NULL,NULL,_binary '\0',_binary '',NULL,147,_binary '',2,3,1,561,4),(558,2021,NULL,NULL,_binary '\0',_binary '',NULL,149,_binary '',2,3,1,562,4),(559,2021,NULL,NULL,_binary '\0',_binary '',NULL,151,_binary '',2,3,1,563,4),(560,2021,NULL,NULL,_binary '\0',_binary '',NULL,153,_binary '',2,3,1,564,4),(561,2021,NULL,NULL,_binary '\0',_binary '',NULL,155,_binary '',2,3,1,565,4),(562,2021,NULL,NULL,_binary '\0',_binary '',NULL,157,_binary '',2,3,1,566,4),(563,2021,NULL,NULL,_binary '\0',_binary '',NULL,159,_binary '',2,3,1,567,4),(564,2021,NULL,NULL,_binary '\0',_binary '',NULL,161,_binary '',2,3,1,568,4),(565,2021,NULL,NULL,_binary '\0',_binary '',NULL,163,_binary '',2,3,1,569,4),(566,2021,NULL,NULL,_binary '\0',_binary '',NULL,165,_binary '',2,3,1,570,4),(567,2021,NULL,NULL,_binary '\0',_binary '',NULL,167,_binary '',2,3,1,571,4),(568,2021,NULL,NULL,_binary '\0',_binary '',NULL,169,_binary '',2,3,1,572,4),(569,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',3,3,1,573,4),(570,2021,NULL,NULL,_binary '\0',_binary '\0',NULL,3,_binary '',2,3,1,574,4),(571,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',2,3,1,575,4),(572,2021,NULL,NULL,_binary '\0',_binary '',NULL,7,_binary '',2,3,1,576,4),(573,2021,NULL,NULL,_binary '\0',_binary '',NULL,9,_binary '',2,3,1,577,4),(574,2021,NULL,NULL,_binary '\0',_binary '',NULL,70,_binary '',2,3,1,578,2),(575,2021,NULL,NULL,_binary '\0',_binary '',NULL,200,_binary '',2,3,1,579,2),(576,2021,NULL,NULL,_binary '\0',_binary '',NULL,55,_binary '',3,3,1,580,2),(577,2021,NULL,NULL,_binary '\0',_binary '',NULL,5,_binary '',4,2,1,583,2),(578,2021,NULL,NULL,_binary '\0',_binary '',NULL,110,_binary '',5,8,1,584,2),(579,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,585,2),(580,2022,NULL,NULL,_binary '',_binary '',NULL,1,_binary '',3,3,1,488,2),(581,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,586,4),(582,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,587,4),(583,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,588,4),(584,2021,NULL,NULL,_binary '\0',_binary '',NULL,50,_binary '',3,3,1,589,4),(6614,2022,NULL,NULL,_binary '',_binary '',NULL,101,_binary '',3,3,1,11,2),(6615,2022,NULL,NULL,_binary '',_binary '',NULL,111,_binary '',3,3,1,12,2),(6616,2022,NULL,NULL,_binary '',_binary '',NULL,121,_binary '',3,3,1,13,2),(6617,2022,NULL,NULL,_binary '',_binary '',NULL,112,_binary '',3,3,1,493,4),(6618,2022,NULL,NULL,_binary '',_binary '',NULL,2,_binary '',3,3,1,3,2),(6619,2022,NULL,NULL,_binary '',_binary '',NULL,3,_binary '',3,3,1,4,2),(6620,2022,NULL,NULL,_binary '',_binary '',NULL,4,_binary '',3,3,1,489,4),(6621,2022,NULL,NULL,_binary '',_binary '',NULL,5,_binary '',3,3,1,574,4),(13818,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',249,3,1,13873,2),(13819,2021,NULL,NULL,_binary '\0',_binary '',NULL,2,_binary '',249,3,1,13874,2),(18320,2021,NULL,NULL,_binary '\0',_binary '',NULL,6,_binary '',249,3,1,18403,4),(18327,2021,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',325,4,1,18410,15),(18328,2019,NULL,NULL,_binary '\0',_binary '',NULL,1,_binary '',326,92,1,18411,16),(18386,2021,NULL,NULL,_binary '\0',_binary '',NULL,200,_binary '',3,3,1,18469,2),(20272,2021,NULL,NULL,_binary '\0',_binary '',NULL,66,_binary '',2,3,1,20356,2),(36334,2022,NULL,NULL,_binary '',_binary '',NULL,7,_binary '',325,3,1,389,2),(49831,2022,NULL,NULL,_binary '',_binary '',NULL,7,_binary '',3,3,1,8,2),(49834,2021,NULL,NULL,_binary '\0',_binary '',NULL,122,_binary '',3,3,1,50066,4),(49835,2025,NULL,NULL,_binary '',_binary '',NULL,11,_binary '',325,3,1,393,2),(49847,2025,NULL,NULL,_binary '',_binary '',NULL,10,_binary '',5,4,1,392,2),(49848,2025,NULL,NULL,_binary '',_binary '',NULL,12,_binary '',5,4,1,394,2),(49849,2021,NULL,NULL,_binary '\0',_binary '',NULL,NULL,_binary '',323,2,1,50067,2),(49850,2021,NULL,NULL,_binary '\0',_binary '',NULL,NULL,_binary '',323,2,1,50068,2),(49851,2021,'2022-01-30 13:12:52',NULL,_binary '\0',_binary '',NULL,777,_binary '',21,3,1,50069,4),(49853,2021,NULL,NULL,_binary '\0',_binary '',NULL,852,_binary '',21,2,1,50071,2),(49854,2022,NULL,NULL,_binary '\0',_binary '',NULL,258,_binary '',5,2,1,50072,15),(49855,2022,NULL,NULL,_binary '\0',_binary '',NULL,258,_binary '',322,2,1,50074,2),(49857,2022,'2022-02-03 12:19:05','ggg',_binary '\0',_binary '',NULL,258,_binary '\0',21,2,1,50076,2);
/*!40000 ALTER TABLE `student_identification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_profile_update_otp`
--

DROP TABLE IF EXISTS `student_profile_update_otp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `student_profile_update_otp` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `custom_student_id` varchar(255) DEFAULT NULL,
  `date_executed` datetime DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  `otp_generate_date` date DEFAULT NULL,
  `otp_status` int DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_profile_update_otp`
--

LOCK TABLES `student_profile_update_otp` WRITE;
/*!40000 ALTER TABLE `student_profile_update_otp` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_profile_update_otp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK50sgrid0o40hyncerx1si7l0j` (`username`,`rolename`),
  KEY `FKbowd6pwstq9pij13am04nxf3k` (`institute_id`),
  CONSTRAINT `FKbowd6pwstq9pij13am04nxf3k` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=541 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'ROLE_ADMIN','jhonarther',1),(2,'ROLE_CUSTOMER_SUPPORT','jhonarther',1),(98,'ROLE_SUPER_ADMIN','jhonarther',1),(536,'ROLE_ADMIN','riad',1),(537,'ROLE_OPERATOR','riad',1),(538,'ROLE_ACCOUNTANT','riad',1),(539,'ROLE_TEACHER','riad',1),(540,'ROLE_EXAM_CONTROLLER','riad',1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `enabled` bit(1) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `institute_id` bigint DEFAULT NULL,
  `staff_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r43af9ap4edm43mmtq01oddj6` (`username`),
  KEY `FKl7cbsds8r2wlyqaescyedph2d` (`institute_id`),
  KEY `FKf6e9vg9sdre3xy1hbqla5fxrx` (`staff_id`),
  CONSTRAINT `FKf6e9vg9sdre3xy1hbqla5fxrx` FOREIGN KEY (`staff_id`) REFERENCES `staff_basic_info` (`staff_id`),
  CONSTRAINT `FKl7cbsds8r2wlyqaescyedph2d` FOREIGN KEY (`institute_id`) REFERENCES `institute_info` (`institute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,_binary '','01913978807','Jhon Arther','$2a$04$rOb5iUsZ1yR2gmi80uX/eeQ4hKItckxqh2ZGgFiljdLSftX7DWHR6','jhonarther',1,NULL),(231,_binary '','01750316386','Riad Yousuf','$2a$10$mIHw.5ytO1pVA1nuJhWG3.lZPYcIYaWe0vkJH3aDV3DcRhzpAPKg.','riad',1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-31 15:40:21
