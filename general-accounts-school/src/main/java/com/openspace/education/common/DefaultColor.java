package com.openspace.education.common;
public enum DefaultColor {
	
	STUDENT_ID_CARD_TEMP_9_PRIMARY_COLOR("#2E3192"), 
	STUDENT_ID_CARD_TEMP_10_PRIMARY_COLOR("#2E3191"), 
	STUDENT_ID_CARD_SECONDARY_COLOR("#00AEEF"),
	STAFF_ID_CARD_TEMP_7_PRIMARY_COLOR("#2d3092"),
	STAFF_ID_CARD_TEMP_7_SECONDARY_COLOR("#00adee"),
	ADMIT_CARD_PRIMARY_COLOR("#2d3092"),
	ADMIT_CARD_SECONDARY_COLOR("#E8E8E8");
	
	private final String color;
	
	private DefaultColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

}
