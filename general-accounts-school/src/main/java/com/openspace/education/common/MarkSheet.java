package com.openspace.education.common;

public enum MarkSheet {

	SINGLE(100), GRANDFINAL(200);

	private final int marksheetTypeCode;

	private MarkSheet(int marksheetTypeCode) {
		this.marksheetTypeCode = marksheetTypeCode;
	}

	public int getMarksheetTypeCode() {
		return marksheetTypeCode;
	}

}
