package com.openspace.education.common;

import org.springframework.stereotype.Component;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HexColorCombination {

	private Pattern pattern;
	private Matcher matcher;

	private static final String HEX_PATTERN = "^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$";

	public HexColorCombination() {
	    pattern = Pattern.compile(HEX_PATTERN);
	}

	public boolean validate(final String hexColorCode) {

	    matcher = pattern.matcher(hexColorCode);
	    return matcher.matches();

	}
}
