package com.openspace.education.common;

public enum FileFolder {

	STAFF, INSTITUTE, STUDENT, DEFAULT, CLASSROUTINE, EXAMROUTINE, ONLINESTUDENT
	
}
