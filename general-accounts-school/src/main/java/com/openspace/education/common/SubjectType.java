package com.openspace.education.common;

public enum SubjectType {

	 	COMPULSARY(0),
	    CHOOSABLE(1), //Optional. When it is from FourthSubjectAssign Table then it will be optional
	    GROUP_BASED(2),
	    ELECTIVE(3), // For Both group e.g Science and Commerce i.e Computer 
	    UNCOUNTABLE(4);
	    
	    private final int code;
	    
	    private SubjectType(int code){
	        this.code=code;
	    }

	    public int getCode() {
	        return code;
	    }
}
