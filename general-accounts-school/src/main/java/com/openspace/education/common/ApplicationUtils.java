package com.openspace.education.common;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;



public class ApplicationUtils {
	
	
	public static String getFilePath(String imageFolder) {
		Properties prop = new Properties();
		InputStream inputStream = ApplicationUtils.class.getClassLoader()
				.getResourceAsStream("imagepath/image-path.properties");
		try {
			prop.load(inputStream);
			return prop.getProperty(imageFolder);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}
	
	
	public static double round(double number, int position) {
        double pow = 10;
        pow = Math.pow(pow, position);
        number = number * pow;
        number = Math.round(number);
        number = number / 100;
        return number;
    }
	
	public static double providePercentWithTwoDecimalDigits(int partNumber, int fullNumber) {
		if (fullNumber == 0) {
			return 0;
		}
		return Double.parseDouble(new BigDecimal(
				(Double.parseDouble(String.valueOf(partNumber)) / Double.parseDouble(String.valueOf(fullNumber))) * 100)
						.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
	}
	
	
	
	public static List<Date> getDateListByMonthAndYear(String monthName, String year) {

		List<Date> dateList = new ArrayList<>();

		SimpleDateFormat format = new SimpleDateFormat("yyyy/MMMM", Locale.US);
		Date utilDate = null;
		try {
			utilDate = format.parse(year + "/" + monthName);
		} catch (ParseException e) {
			System.out.println(e);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(utilDate);
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		for (int i = 0; i < maxDay; i++) {

			cal.set(Calendar.DAY_OF_MONTH, i + 1);

			Date date = cal.getTime();

			dateList.add(date);
		}

		return dateList;

	}
	
	
	public static int getDayOfMonth(Date aDate) {
		int dayNo = 0;

		if (aDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aDate);
			return cal.get(Calendar.DAY_OF_MONTH);
		} else {
			return 0;
		}

	}
	
	
	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}
}
