package com.openspace.education.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestBaseResponse<T> {

	
	private String message;
    private Integer messageType;
    private T item;
    
    
    public TestBaseResponse() {
    }

    public TestBaseResponse(T item) {
        super();
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getMessageType() {
		return messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}
    
    
    
    
}
