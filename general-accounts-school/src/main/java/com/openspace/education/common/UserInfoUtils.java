package com.openspace.education.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.user.model.entity.Users;


@Component
public class UserInfoUtils {
	
	 private static ApplicationContext context;

	    @Autowired
	    public UserInfoUtils(ApplicationContext ac) {
	        context = ac;
	    }

	    public static ApplicationContext getContext() {
	        return context;
	    }
	    
	    
	     public static String getHashPassword(String password) {
	        PasswordEncoder userPasswordEncoder = context.getBean("passwordEncoder", PasswordEncoder.class);
	        return userPasswordEncoder.encode(password);
	    }
	    
	    public static boolean isPreviousPasswordCorrect(String previousPlainPassword,String previousEncriptedPassword) {
	        PasswordEncoder userPasswordEncoder = context.getBean("passwordEncoder", PasswordEncoder.class);
	       return userPasswordEncoder.matches(previousPlainPassword, previousEncriptedPassword);
	    }
	    
	    
	    public static String getLoggedInUserName() {
	        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user.getUsername();

	    }
	    
	    public static Long getLoggedInUserId() {
	        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user.getId();

	    }
	    
	    
	   
	    
	    public static Long getLoggedInInstituteId() {
	    	
	       try { 
	    	   
	    	Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user.getInstitute().getInstituteId();
	        
	       }catch (Exception e) {
	    	   
		   }
	       return 0l;
	    }
	    
	    public static Institute getLoggedInInstitute() {
	        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user.getInstitute();
	    }
	    
	    public static Users getLoggedInUser() {
	        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user;
	    }
	    
	    
	    public static Integer getInstituteCurrentAcademicYear() {
	        Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	        return user.getInstitute().getAcademicYear();
	    }
	   

}
