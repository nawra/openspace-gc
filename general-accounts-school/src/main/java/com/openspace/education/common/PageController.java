package com.openspace.education.common;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties.Session;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import com.openspace.education.initialsetup.service.CoreSettingsService;
import com.openspace.education.initialsetup.service.GroupConfigurationService;
import com.openspace.education.institute.model.request.InstituteCreateRequest;
import com.openspace.education.location.service.LocationService;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.model.response.PostLoginViewInfo;
import com.openspace.education.user.service.UserService;

/**
 * @author Riad
 */
@Controller
public class PageController {

    @Autowired
    public LocationService locationService;

    @Autowired
    private UserService userService;

    @Autowired
    public CoreSettingsService coreSettingsService;
    @Autowired
    public GroupConfigurationService groupConfigurationService;

    // ------------------------ Student ------------------------//

    @GetMapping("/student-multiple-registration")
    public String showStudentMultipleRegistrationPage() {
        return "student-multiple-registration";
    }

    @GetMapping("/student-multiple-registration-custom")
    public String studentMultipleRegistrationCustom() {
        return "student-multiple-registration-custom";
    }

    @GetMapping("/student-excel-registration-with-custom-id")
    public String showStudentExcelRegistrationWithCustomIdPage() {
        return "student-excel-registration-with-custom-id";
    }

    @GetMapping("/student-excel-registration-without-custom-id")
    public String showStudentExcelRegistrationWithoutCustomIdPage() {
        return "student-excel-registration-without-custom-id";
    }

    @GetMapping("/student-basic-update")
    public String showStudentBasicUpdatePage() {
        return "student-basic-update";
    }
   
    @GetMapping("/student-id-update")
    public String showStudentIdUpdatePage() {
        return "student-id-update";
    }

    @GetMapping("/student-academic-info-update")
    public String showStudentAcademicInfoUpdatePage() {
        return "student-academic-info-update";
    }

    @GetMapping("/student-photo-update")
    public String showStudentPhotoUpdatePage() {
        return "student-photo-update";
    }

    @GetMapping("/student-address-update")
    public String studentAddressUpdate() {
        return "student-address-update";
    }

    @GetMapping("/student-migration")
    public String showStudentMigrationPage() {
        return "student-migration";
    }

    @GetMapping("/student-migration-merit")
    public String showStudentMigrationGeneralPage() {
        return "student-migration-merit";
    }
    
    @GetMapping("/student-enable-disable")
    public String studentEnableDisable() {
        return "student-enable-disable";
    }
    
    @GetMapping("/student-single-search")
    public String studentSingleSearch() {
        return "student-single-search";
    }

    @GetMapping("/student-registration-delete")
    public String studentRegistrationDelete() {
        return "student-registration-delete";
    }

    @GetMapping("/student-report-basic-info-list")
    public String showStudentReportBasicInfoListPage() {
        return "student-report-basic-info-list";
    }

    @GetMapping("/student-report-basic-info-list-class-wise")
    public String showStudentReportBasicInfoListClassWisePage() {
        return "student-report-basic-info-list-class-wise";
    }

    @GetMapping("/student-report-basic-info-list-group-wise")
    public String showStudentReportBasicInfoListGroupWisePage() {
        return "student-report-basic-info-list-group-wise";
    }

    @GetMapping("/student-report-section-wise-summary")
    public String showStudentReportSectionWiseSummaryPage() {
        return "student-report-section-wise-summary";
    }

    @GetMapping("/student-report-overview")
    public String studentReportOverviewPage() {
        return "student-report-overview";
    }

    @GetMapping("/student-id-card-template")
    public String showStudentIdCardTemplate() {
        return "student-id-card-template";
    }

    @GetMapping("/student-id-card-download")
    public String showStudentIdCardDownload() {
        return "student-id-card-download";
    }

    @GetMapping("/student-save-before-lottery")
    public String studentEntryBeforeLottery() {
        return "student-save-before-lottery";
    }



  



    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }


    @GetMapping("/")
    public String showLandingPage() {
        return "login";
    }

    @RequestMapping(value = "/postLogin", method = RequestMethod.POST)
    public String postLogin(Model model) {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String homePage(HttpSession session) {
        PostLoginViewInfo postLoginViewInfo = userService.postLoginViewInfo();
        session.setAttribute("username", postLoginViewInfo.getUsername());
        session.setAttribute("nickname", postLoginViewInfo.getNickname());
        session.setAttribute("instituteid", postLoginViewInfo.getInstituteid());
        session.setAttribute("eiinno", postLoginViewInfo.getEiinno());
        session.setAttribute("institutename", postLoginViewInfo.getInstitutename());
        session.setAttribute("address", postLoginViewInfo.getInstituteaddress());
        session.setAttribute("logo", postLoginViewInfo.getInstitutelogo());
        session.setAttribute("currentAcYear", postLoginViewInfo.getCurrentAcYear());

        session.setAttribute("staffId", postLoginViewInfo.getStaffId());
        session.setAttribute("customStaffId", postLoginViewInfo.getCustomStaffId());
        session.setAttribute("staffName", postLoginViewInfo.getStaffName());
        session.setAttribute("designation", postLoginViewInfo.getDesignation());
        session.setAttribute("packageId", postLoginViewInfo.getPackageId());
        session.setAttribute("userPhoto", postLoginViewInfo.getUserPhoto());
        return "home";
    }

    @GetMapping("/logout")
    public String logout(SessionStatus session, Session s) {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.setComplete();
        return "redirect:/login";
    }


    @GetMapping("/forgot-password")
    public String forgotPassword() {
        return "forgot-password";
    }

    // --------------- Initial Set-up About --------------------//

    @GetMapping("/initial-setup-designation")
    public String designationPage() {
        return "initial-setup-designation";
    }

    @GetMapping("/initial-setup-academic-year")
    public String academicYearPage() {
        return "initial-setup-academic-year";
    }

    @GetMapping("/initial-setup-class")
    public String classPage() {
        return "initial-setup-class";
    }

    @GetMapping("/initial-setup-shift")
    public String shiftPage() {
        return "initial-setup-shift";
    }

    @GetMapping("/initial-setup-section")
    public String sectionPage() {
        return "initial-setup-section";
    }

    @GetMapping("/initial-setup-group")
    public String groupPage() {
        return "initial-setup-group";
    }

    @GetMapping("/initial-setup-student-category")
    public String studentCategoryPage() {
        return "initial-setup-student-category";
    }

    @GetMapping("/initial-setup-exam")
    public String examPage() {
        return "initial-setup-exam";
    }

    @GetMapping("/initial-setup-subject")
    public String subjectPage() {
        return "initial-setup-subject";
    }

    @GetMapping("/initial-setup-period")
    public String periodPage() {
        return "initial-setup-period";
    }

    @GetMapping("/initial-setup-fee-head")
    public String FeeHeadPage() {
        return "initial-setup-fee-head";
    }

    @GetMapping("/initial-setup-fee-sub-head")
    public String FeeSubHeadPage() {
        return "initial-setup-fee-sub-head";
    }

    @GetMapping("/initial-setup-fee-waiver")
    public String FeeWaiverPage() {
        return "initial-setup-fee-waiver";
    }

    @GetMapping("/fee-money-receipt-template")
    public String moneyReceiptTemplate() {
        return "fee-money-receipt-template";
    }

    @GetMapping("/student-accounts-trn-delete")
    public String studentAccountsTrnDelete() {
        return "student-accounts-trn-delete";
    }

    // --------------- class configuration --------------------//

    @GetMapping("/initial-setup-class-configuration")
    public String classConfigPage() {
        return "initial-setup-class-configuration";
    }

    @GetMapping("/initial-setup-group-configuration")
    public String groupConfigPage() {
        return "initial-setup-group-configuration";
    }
    // --------------- Location About --------------------//

    @GetMapping("/location")
    public String locationPage(Model model) {
        ItemResponse itemResponse = locationService.divisionList();
        model.addAttribute("divisions", itemResponse.getItem());
        return "location-info";
    }

    // --------------- Institute --------------------//

    @GetMapping("/institute-registration")
    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    public String instituteRegistrationPage(Model model) {

        model.addAttribute("instituteReq", new InstituteCreateRequest());

        return "institute-registration";
    }

    @GetMapping("/institute-update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String instituteUpdatePage() {
        return "institute-update";
    }

    @GetMapping("/institute-list")
    public String instituteListPage() {
        return "institute-list";
    }   
    
    @GetMapping("/institute-student-count")
    public String instituteStudentCount() {
        return "institute-student-count";
    }

    @GetMapping("/institute-jump")
    public String instituteJumpPage() {
        return "institute-jump";
    }

    // --------------- User --------------------//

    @GetMapping("/user-list")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String userListPage() {

        return "user-list";
    }

    @GetMapping("/payable-bill")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String payableBill() {

        return "payable-bill";
    }

    @GetMapping("/paid-bill")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String paidBill() {

        return "paid-bill";
    }

    @GetMapping("/default-signature")
    public String goUserPage() {
        return "default-signature";
    }

    @GetMapping("/user-password-change")
    public String userPasswordChangePage() {
        return "user-password-change";
    }

    // ------------------------ Staff ------------------------//

    @GetMapping("/staff-single-registration")
    public String staffSingleRegistrationPage() {
        return "staff-single-registration";
    }
    
    @GetMapping("/staff-assign-class-teacher")
    public String staffAssignClassTeacher() {
        return "staff-assign-class-teacher";
    }

    @GetMapping("/staff-update-basic-info")
    public String staffUpdateBasicInfoPage() {
        return "staff-update-basic-info";
    }

    @GetMapping("/staff-report-basic-info-list")
    public String staffReportBasicInfoListPage() {
        return "staff-report-basic-info-list";
    }

    @GetMapping("/staff-id-card-template")
    public String staffIdCardTemplatePage() {
        return "staff-id-card-template";
    }

    @GetMapping("/staff-id-card-download")
    public String staffIdCardTemplateDownloadPage() {
        return "staff-id-card-download";
    }

    @GetMapping("/staff-excel-registration")
    public String staffMultipleRegistration() {
        return "staff-multiple-registration";
    }

    // --------------- General accounts ---------------------//

    @GetMapping("/ledger-list")
    public String ledgerList() {
        return "general-accounts-ledger-list";
    }

    @GetMapping("/receive-trn")
    public String showReceivePage() {
        return "general-accounts-receive";
    }

    @GetMapping("/payment-trn")
    public String showPaymentPage() {
        return "general-accounts-payment";
    }

    @GetMapping("/contra-trn")
    public String showContraPage() {
        return "general-accounts-contra";
    }

    @GetMapping("/journal-trn")
    public String showJournalPage() {
        return "general-accounts-journal";
    }

    @GetMapping("/trial-balance")
    public String showTrialBalancePage() {
        return "general-accounts-report-trial-balance";
    }

    @GetMapping("/balance-sheet")
    public String showBalanceSheetPage() {
        return "general-accounts-report-balance-sheet";
    }

    @GetMapping("/income-statement")
    public String showIncomeStatementPage() {
        return "general-accounts-report-income-statement";
    }

    @GetMapping("/journal-report")
    public String showJournalReportPage() {
        return "general-accounts-report-journal";
    }

    @GetMapping("/funds-flow")
    public String showFundsFlowPage() {
        return "general-accounts-report-funds-flow";
    }

    @GetMapping("/cash-summary")
    public String showCashSummaryPage() {
        return "general-accounts-report-cash-summary";
    }    
    
    @GetMapping("/general-accounts-report-user-wise")
    public String generalAccountsReportUserWise() {
        return "general-accounts-report-user-wise";
    }

    @GetMapping("/voucher-show")
    public String showVoucherTrnDeletePage() {
        return "general-accounts-trn-delete";
    }

    @GetMapping("/general-accounts-report-cash-book")
    public String generalAccountsReportCashBook() {
        return "general-accounts-report-cash-book";
    }

    @GetMapping("/general-accounts-report-ledger-book")
    public String generalAccountsReportLedgerBook() {
        return "general-accounts-report-ledger-book";
    }    
    
    @GetMapping("/general-accounts-report-voucher-template")
    public String generalAccountsReportVoucherTemplate() {
        return "general-accounts-report-voucher-template";
    }
    

    
    // --------------- Student Accounts ---------------------//

    @GetMapping("/fee-subhead-configuration")
    public String feeSubheadConfigurationPage() {
        return "fee-subhead-configuration";
    }

    @GetMapping("/fee-head-ledger-configuration")
    public String feeHeadLedgerConfigurationPage() {
        return "fee-head-ledger-configuration";
    }

    @GetMapping("/fee-fine-ledger-configuration")
    public String feeFineLedgerConfigurationPage() {
        return "fee-fine-ledger-configuration";
    }

    @GetMapping("/fee-amount-configuration")
    public String feeAmountConfigurationPage() {
        return "fee-amount-configuration";
    }

    @GetMapping("/fee-waiver-configuration")
    public String feeWaiverConfigurationPage() {
        return "fee-waiver-configuration";
    }

    @GetMapping("/fee-waiver-configuration-list")
    public String feeWaiverConfigurationListPage() {
        return "fee-waiver-configuration-list";
    }

    @GetMapping("/fee-subhead-time-configuration")
    public String feeSubheadTimeConfiguartionPage() {
        return "fee-subhead-time-configuration";
    }

    @GetMapping("/ofps-account-configuration")
    public String ofpsAccountConfigurationPage() {
        return "ofps-account-configuration";
    }

    // Manual Fee Collection //
    
    @GetMapping("/fee-collection-manual")
    public String feeCollectionManualPage() {
        return "fee-collection-manual";
    }

    // Student Fee Report //

    @GetMapping("/fee-report-section-due-details")
    public String feeReportSectionDueDetailsPage() {
        return "fee-report-section-due-details";
    }

    @GetMapping("/fee-report-section-paid-list")
    public String feeReportSectionPaidListPage() {
        return "fee-report-section-paid-list";
    }

    @GetMapping("/fee-report-all-paid-list")
    public String feeReportAllPaidListPage() {
        return "fee-report-all-paid-list";
    }

    @GetMapping("/fee-report-date-to-date-head-collection")
    public String feeReportDateToDateHeadCollection() {
        return "fee-report-date-to-date-head-collection";
    }



   


    // --------------- Erro Pages ---------------------//

    @GetMapping("/403")
    public String error403() {

        return "403";
    }

    // -------------------- Online Application View -------------------------//

    @GetMapping("/online-admission")
    public String onlineAdmission(HttpSession session) {
    	session.invalidate();
        return "online-admission";
    }
    

}
