package com.openspace.education.common;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//import com.lowagie.text.pdf.Barcode128;

@Component
public class ImageFactory {

	@Autowired
	HexColorCombination hexColorCombination;

	public Image makeStdIdCard1HeadingImage(String primaryColor, String secondaryColor) {

		int width = 2370;
		int height = 1950;
		int firstHeight = 1850;
		int secondHeight = 250;
		int firstThirdHeight = 150;

		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bufferedImage.createGraphics();

		int x[] = { 0, width, width, 0 };
		int y[] = { 0, 0, firstThirdHeight, firstHeight };
		g2d.setColor(Color.decode(primaryColor));
		g2d.setBackground(Color.decode(primaryColor));
		g2d.fillPolygon(x, y, 4);
		int a[] = { 0, width, width, 0 };
		int b[] = { height, secondHeight, firstThirdHeight, firstHeight };
		g2d.setColor(Color.decode(secondaryColor));
		g2d.setBackground(Color.decode(secondaryColor));
		g2d.fillPolygon(a, b, 4);

		g2d.dispose();

		return (Image) bufferedImage;

	}

	public void writeImage(String fullImagePath, String primaryColor, String secondaryColor) {
		File file = new File(fullImagePath);
		try {
			ImageIO.write((BufferedImage) makeStdIdCard1HeadingImage(primaryColor, secondaryColor), "png", file);
		} catch (IOException e) {
			System.out.print(e);
		}
	}

//	public void writeBarcodeImage(String imageFullPath, String barcodeValue) {
//		File file = new File(imageFullPath);
//
//		if (barcodeValue == null) {
//			barcodeValue = "No Value Found";
//		}
//
////		Barcode128 barcode = new Barcode128();
////		barcode.setGenerateChecksum(true);
////		barcode.setCode(barcodeValue);
////		Image image = barcode.createAwtImage(Color.BLACK, Color.WHITE);
//
//		BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null),
//				BufferedImage.TYPE_INT_ARGB);
//		Graphics g = bimage.createGraphics();
//		g.drawImage(image, 0, 0, null);
//		g.dispose();
//
//		try {
//			ImageIO.write(bimage, "png", file);
//		} catch (IOException e) {
//			System.out.print(e);
//		}
//	}

	public Image makeStdIdCard2HeadingImage(String primaryColor, String secondaryColor) {

		int width = 2370;
		int height = 1950;
		int firstHeight = 1850;
		int firstThirdHeight = 150;

		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = bufferedImage.createGraphics();

		int x[] = { 0, width, width, 0 };
		int y[] = { 0, 0, firstThirdHeight, firstHeight };
		g2d.setColor(Color.decode(primaryColor));
		g2d.setBackground(Color.decode(primaryColor));
		g2d.fillPolygon(x, y, 4);

		g2d.dispose();

		return (Image) bufferedImage;

	}

	public void changeImageColor(String currentPrimaryColor, String currentSecondaryColor, String toChangePrimaryColor,
			String toChangeSecondaryColor, String sourceImageFullPath, String targetImageFullPath) {

		try {
			BufferedImage originalImage = ImageIO.read(new File(sourceImageFullPath));

			BufferedImage newImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(),
					BufferedImage.TYPE_INT_ARGB);
			WritableRaster raster = newImage.getRaster();

			int[] newPrimaryColorPixel = provideColorPixel(toChangePrimaryColor);

			int[] newSecondaryColorPixel = provideColorPixel(toChangeSecondaryColor);

			int width = originalImage.getWidth();
			int height = originalImage.getHeight();

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					
					if (originalImage.getRGB(x, y) == Color.decode(currentPrimaryColor).getRGB()) {
						raster.setPixel(x, y, newPrimaryColorPixel);
					} else if (originalImage.getRGB(x, y) == Color.decode(currentSecondaryColor).getRGB()) {
						raster.setPixel(x, y, newSecondaryColorPixel);
					}
				}
			}

			ImageIO.write(newImage, "png", new File(targetImageFullPath));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public int[] provideColorPixel(String colorHexCode) {

		int[] pixel = new int[4];

		if (hexColorCombination.validate(colorHexCode)) {
			pixel[0] = Color.decode(colorHexCode).getRed();
			pixel[1] = Color.decode(colorHexCode).getGreen();
			pixel[2] = Color.decode(colorHexCode).getBlue();
			pixel[3] = Color.decode(colorHexCode).getAlpha();
		}

		return pixel;

	}

	public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
		int w = image.getWidth();
		int h = image.getWidth();
		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = output.createGraphics();
		g2.setComposite(AlphaComposite.Src);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(Color.WHITE);
		g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));
		g2.setComposite(AlphaComposite.SrcAtop);
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
		return output;
	}

	public /* static */ BufferedImage cropImage(int cornerRadius, String fileFullPath /* , String writePath */) {

		BufferedImage icon;
		try {

			if (Files.isReadable(Paths.get(fileFullPath).toAbsolutePath())) {
				icon = ImageIO.read(new File(fileFullPath));
				BufferedImage rounded = makeRoundedCorner(icon, cornerRadius);
				return rounded;

			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		return null;
	}

	public int provideCornerRadiusForStaff(int templateId) {

		switch (templateId) {

		case 203:
			return 440;

		}

		return 0;

	}
	
	
	public int provideCornerRadiusForStudent(int templateId) {

		switch (templateId) {

		case 102:
			return 400;

		}

		return 0;

	}
}
