package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingSection;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingSectionRepository extends JpaRepository<CoreSettingSection, Long>{
	
	public CoreSettingSection findByNameAndInstitute_InstituteId(String sectionName,Long instituteId);
	
	public List<CoreSettingSection> findByInstituteOrderBySerial(Institute institute);

}
