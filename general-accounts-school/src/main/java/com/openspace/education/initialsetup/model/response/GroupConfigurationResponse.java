package com.openspace.education.initialsetup.model.response;

public class GroupConfigurationResponse {

	private Long id;

	private int status;

	private int serial;

	private Long classId;

	private String className;

	private Long groupId;

	private String groupName;
	
	private Integer applicationFee;
	
	private String classGroupName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(Integer applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getClassGroupName() {
		return classGroupName;
	}

	public void setClassGroupName(String classGroupName) {
		this.classGroupName = classGroupName;
	}

	
}
