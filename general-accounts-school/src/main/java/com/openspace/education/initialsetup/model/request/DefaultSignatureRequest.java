package com.openspace.education.initialsetup.model.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


public class DefaultSignatureRequest {

	@NotEmpty
	private String signatureTitle;

	@NotNull
	private Integer usedId;

	@NotEmpty
	private String usedName;
	
	@NotNull
    private Integer signStatus;


	public String getSignatureTitle() {
		return signatureTitle;
	}

	public void setSignatureTitle(String signatureTitle) {
		this.signatureTitle = signatureTitle;
	}

	public Integer getUsedId() {
		return usedId;
	}

	public void setUsedId(Integer usedId) {
		this.usedId = usedId;
	}

	public String getUsedName() {
		return usedName;
	}

	public void setUsedName(String usedName) {
		this.usedName = usedName;
	}

	public Integer getSignStatus() {
		return signStatus;
	}

	public void setSignStatus(Integer signStatus) {
		this.signStatus = signStatus;
	}


}
