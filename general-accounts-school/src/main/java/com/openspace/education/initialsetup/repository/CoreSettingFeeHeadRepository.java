package com.openspace.education.initialsetup.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingFeeHeadRepository extends JpaRepository<CoreSettingFeeHead, Long>{
	
    public CoreSettingFeeHead findByNameAndInstitute(String name,Institute institute);
	
    public CoreSettingFeeHead findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingFeeHead> findByInstituteOrderBySerialAsc(Institute institute);
	
	public List<CoreSettingFeeHead> findByInstituteAndIdInOrderBySerialAsc(Institute institute,Set<Long> feeHeadIds);

}
