package com.openspace.education.initialsetup.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingSection;
import com.openspace.education.initialsetup.model.entity.CoreSettingShift;
import com.openspace.education.initialsetup.model.request.ClassConfigurationRequest;
import com.openspace.education.initialsetup.model.request.GroupConfigurationSerialUpdate;
import com.openspace.education.initialsetup.model.response.ClassConfigurationResponse;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.ClassTeacherAssign;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.repository.ClassTeacherAssignRepository;
import com.openspace.education.user.model.entity.Users;

@Service
public class ClassConfigurationService {

	public Logger logger = LoggerFactory.getLogger(ClassConfigurationService.class);

	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;
	
	@Autowired
	private ClassTeacherAssignRepository classTeacherAssignRepository;

	@Transactional
	public BaseResponse createClassConfiguration(ClassConfigurationRequest classConfigurationRequest) {

		BaseResponse baseResponse = new BaseResponse();
		Long instituteId = UserInfoUtils.getLoggedInInstituteId();
		Institute institute = UserInfoUtils.getLoggedInInstitute();

		if (classConfigurationRepository
				.findByClassInfo_IdAndShiftInfo_idAndSectionInfo_idAndInstitute_InstituteId(
						classConfigurationRequest.getClassId(), classConfigurationRequest.getShiftId(),
						classConfigurationRequest.getSectionId(), instituteId) != null) {

			baseResponse.setMessage("Duplicate Entry");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		ClassConfiguration classConfiguration = new ClassConfiguration();
		CoreSettingClass coreSettingClass = new CoreSettingClass();
		coreSettingClass.setId(classConfigurationRequest.getClassId());
		CoreSettingShift coreSettingShift = new CoreSettingShift();
		coreSettingShift.setId(classConfigurationRequest.getShiftId());
		CoreSettingSection coreSettingSection = new CoreSettingSection();
		coreSettingSection.setId(classConfigurationRequest.getSectionId());

		classConfiguration.setClassInfo(coreSettingClass);
		classConfiguration.setShiftInfo(coreSettingShift);
		classConfiguration.setSectionInfo(coreSettingSection);
		classConfiguration.setSerial(0);

		classConfiguration.setInstitute(institute);

		classConfigurationRepository.save(classConfiguration);

		baseResponse.setMessage("Class Configuration Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}

	
	
	public ItemResponse getAllClassConfigurationList() {

		ItemResponse itemResponse = new ItemResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Users user=UserInfoUtils.getLoggedInUser();
		List<ClassConfiguration> classConfigurationList=new ArrayList<>();
		
		if(isSheOrHeOnlyTeacher(user.getRoles())) {
			findTeacherClassConfigurationList(classConfigurationList, user.getStaff(), institute);
		}else {
		    classConfigurationList = classConfigurationRepository.findByInstituteOrderBySerial(institute);
		}

		List<ClassConfigurationResponse> classConfigurationResponseList = new ArrayList<>();



		classConfigurationList.forEach(csc -> {

			classConfigurationResponseList.add(copyClassConfigurationToClassConfigurationResponse(csc));
		});

		itemResponse.setItem(classConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	public ItemResponse getPublicAllClassConfigurationList(Institute institute) {

		ItemResponse itemResponse = new ItemResponse();

		List<ClassConfiguration> classConfigurationList=classConfigurationRepository.findByInstituteOrderBySerial(institute);
		
		List<ClassConfigurationResponse> classConfigurationResponseList = new ArrayList<>();

		classConfigurationList.forEach(csc -> {

			classConfigurationResponseList.add(copyClassConfigurationToClassConfigurationResponse(csc));
		});

		itemResponse.setItem(classConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	public void findTeacherClassConfigurationList(List<ClassConfiguration> classConfigurationList,Staff staff, Institute institute) {
		
		List<ClassTeacherAssign> classTeacherAssigns=classTeacherAssignRepository.findByInstituteAndStaff(institute, staff);
		
		for(ClassTeacherAssign cta : classTeacherAssigns) {
			
			classConfigurationList.add(cta.getClassConfiguration());
		}
		
	}
	
	public boolean isSheOrHeOnlyTeacher(List<String> roles) {
		boolean a=false;
		for(String str : roles) {
			if(str.equals("ROLE_TEACHER")) {
				a=true;
			}else {
				a=false;
				break;
			}
		}
		return a;
	}

	private ClassConfigurationResponse copyClassConfigurationToClassConfigurationResponse(ClassConfiguration csc) {

		ClassConfigurationResponse classConfigurationResponse = new ClassConfigurationResponse();
		String classConfigName = csc.getClassInfo().getName() + "-" + csc.getShiftInfo().getName() + "-"
				+ csc.getSectionInfo().getName();
		classConfigurationResponse.setId(csc.getId());
		classConfigurationResponse.setClassId(csc.getClassInfo().getId());
		classConfigurationResponse.setClassName(csc.getClassInfo().getName());
		classConfigurationResponse.setShiftId(csc.getShiftInfo().getId());
		classConfigurationResponse.setShiftName(csc.getShiftInfo().getName());
		classConfigurationResponse.setSectionId(csc.getSectionInfo().getId());
		classConfigurationResponse.setSectionName(csc.getSectionInfo().getName());
		classConfigurationResponse.setClassConfigName(classConfigName);
		classConfigurationResponse.setSerial(csc.getSerial());
		classConfigurationResponse.setStatus(csc.getStatus());

		return classConfigurationResponse;
	}

	@Transactional
	public BaseResponse updateClassConfigurationSerial(List<ClassConfigurationRequest> classConfigurationRequest) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
        List<Long> classConfigIds=new LinkedList<>();
        classConfigurationRequest.forEach(rq -> {
        	classConfigIds.add(rq.getId());
        });
		
		List<ClassConfiguration> classConfigurations= classConfigurationRepository
				.findByIdInAndInstitute(classConfigIds, institute);

			if(classConfigurations.size()<1) {
		    baseResponse.setMessage("No List Found");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
		


		
		for(ClassConfiguration config : classConfigurations) {
			
			for(ClassConfigurationRequest rq : classConfigurationRequest) {
				
				if(config.getId().equals(rq.getId())) {
					config.setSerial(rq.getSerial());
					break;
				}
			}
			
		
		}


		baseResponse.setMessage("Class Configuration Serial Successfully Updated");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	

	public BaseResponse deleteClassConfiguration(Long id) {

		BaseResponse baseResponse = new BaseResponse();

		ClassConfiguration classConfiguration = classConfigurationRepository.findOne(id);

		if (classConfiguration == null) {
			baseResponse.setMessage("Class Config not found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		try {

			classConfigurationRepository.delete(classConfiguration);

		} catch (DataIntegrityViolationException e) {
			logger.error(e.getMessage());
			baseResponse.setMessage("This Class Config is assigned in another configuration");
			baseResponse.setMessageType(0);
			return baseResponse;
		} catch (Exception e) {
			logger.error(e.getMessage());
			baseResponse.setMessage("Operation Failed");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		baseResponse.setMessage("Class Config successfully deleted");
		baseResponse.setMessageType(1);

		return baseResponse;

	}

}
