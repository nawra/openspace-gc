package com.openspace.education.initialsetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.initialsetup.model.request.DefaultSignatureRequest;
import com.openspace.education.initialsetup.service.DefaultSignatureService;

@Controller
@RequestMapping(value = "/signature")
public class DefaultSignatureController {

	@Autowired
	private DefaultSignatureService defaultSignatureService;

	@PostMapping(value = "/save")
	public ResponseEntity<ItemResponse> saveSignature(@RequestBody @Valid DefaultSignatureRequest request) {
		ItemResponse baseResponse = defaultSignatureService.createDefaultSign(request);
		return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
	}
	

	@GetMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteSignature(@RequestParam Long signatureId) {
		BaseResponse baseResponse = defaultSignatureService.deleteDefaultSignImage(signatureId);
		return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
	}
	
	
	
	@PostMapping(value = "/image/update")
	public ResponseEntity<ItemResponse> updateSignatureImage(@RequestParam Long signatureId,@RequestParam MultipartFile file) {
		ItemResponse baseResponse = defaultSignatureService.updateDefaultSignImage(file, signatureId);
		return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
	}
	
	
	@GetMapping(value = "/image/list")
	public ResponseEntity<ItemResponse> signatureImageList() {
		ItemResponse baseResponse = defaultSignatureService.defaultSignImageList();
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
   }
	
}
