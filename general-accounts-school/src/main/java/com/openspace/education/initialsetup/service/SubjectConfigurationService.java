package com.openspace.education.initialsetup.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingSubject;
import com.openspace.education.initialsetup.model.entity.SubjectConfiguration;
import com.openspace.education.initialsetup.model.request.SubjectConfigurationRequest;
import com.openspace.education.initialsetup.model.request.SubjectConfigurationUpdateRequest;
import com.openspace.education.initialsetup.model.response.SubjectConfigurationViewResponse;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingClassRepository;
import com.openspace.education.initialsetup.repository.CoreSettingGroupRepository;
import com.openspace.education.initialsetup.repository.CoreSettingSubjectRepository;
import com.openspace.education.initialsetup.repository.SubjectConfigurationRepository;
import com.openspace.education.institute.model.entity.Institute;

@Service
public class SubjectConfigurationService {
	
	@Autowired
	private CoreSettingClassRepository coreSettingClassRepository;
	
	@Autowired
	private CoreSettingGroupRepository coreSettingGroupRepository;
	
	@Autowired
	private CoreSettingSubjectRepository coreSettingSubjectRepository;
	
	@Autowired
	private SubjectConfigurationRepository subjectConfigurationRepository;
	
	
	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;
	
	
	@Transactional
	public BaseResponse saveSubjectConfiguration(SubjectConfigurationRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		CoreSettingClass classInfo=coreSettingClassRepository.findByIdAndInstitute(request.getClassId(), institute);
		if(classInfo==null) {
			baseResponse.setMessage("No Class Found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		CoreSettingGroup groupInfo=coreSettingGroupRepository.findByIdAndInstitute(request.getGroupId(), institute);
		if(groupInfo==null) {
			baseResponse.setMessage("No Group Found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		List<CoreSettingSubject> subjects=coreSettingSubjectRepository.findByIdInAndInstituteOrderBySerial(request.getSubjectIds(), institute);
		if(subjects.size()<1) {
			baseResponse.setMessage("No Subject Found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		List<SubjectConfiguration> checkSubjectConfigurations=subjectConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdAndAndSubjectInfo_IdInOrderBySubjectSerial(institute, classInfo.getId(), groupInfo.getId(), request.getSubjectIds());
		if(checkSubjectConfigurations.size()>0) {
			baseResponse.setMessage("Duplicate Entered");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		
		List<SubjectConfiguration> subjectConfigurations=new ArrayList<>();
		int serial=1;
		for(CoreSettingSubject subject:subjects) {
			
			SubjectConfiguration subjectConfiguration=new SubjectConfiguration();
			
			subjectConfiguration.setClassInfo(classInfo);
			subjectConfiguration.setGroupInfo(groupInfo);
			subjectConfiguration.setInstitute(institute);
			subjectConfiguration.setSubjectInfo(subject);
			subjectConfiguration.setSubjectMergeId(0);
			subjectConfiguration.setSubjectSerial(serial);
			subjectConfiguration.setSubjectStatus(0);
			
			subjectConfigurations.add(subjectConfiguration);
			
			serial++;
		}
		
		subjectConfigurationRepository.save(subjectConfigurations);
				
		baseResponse.setMessage("Subject Configuration Saved.");
		baseResponse.setMessageType(1);
		return baseResponse;
	}

	
	
	
	@Transactional
	public BaseResponse updateSubjectConfiguration(List<SubjectConfigurationUpdateRequest> requests) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Long> subjectConfigurationId=new ArrayList<>();
		for(SubjectConfigurationUpdateRequest scs:requests)	{
			subjectConfigurationId.add(scs.getSubjectConfigurationId());
		}
		
		List<SubjectConfiguration> subjectConfigurations=subjectConfigurationRepository.findBySubjectConfigurationIdInAndInstitute(subjectConfigurationId,institute );
		if(subjectConfigurations.size()<1) {
			baseResponse.setMessage("No Configuration Found ");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		
	
		for(SubjectConfiguration sbjcnf:subjectConfigurations) {
			

			for(SubjectConfigurationUpdateRequest req:requests) {
				
				if(sbjcnf.getSubjectConfigurationId().equals(req.getSubjectConfigurationId())) {
					sbjcnf.setSubjectMergeId(req.getSubjectMergeId());
					sbjcnf.setSubjectSerial(req.getSubjectSerial());
					sbjcnf.setSubjectStatus(req.getSubjectStatus());
					
					break;
				}
			}
			
		}
		
				
		baseResponse.setMessage("Subject Configuration Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
    
	public void copySubjectConfigurationToSubjectConfigurationView(SubjectConfiguration subjectConfiguration,SubjectConfigurationViewResponse response) {
		
		response.setClassId(subjectConfiguration.getClassInfo().getId());
		response.setClassName(subjectConfiguration.getClassInfo().getName());
		response.setGroupId(subjectConfiguration.getGroupInfo().getId());
		response.setGroupName(subjectConfiguration.getGroupInfo().getName());
		response.setSubjectConfigurationId(subjectConfiguration.getSubjectConfigurationId());
		response.setSubjectId(subjectConfiguration.getSubjectInfo().getId());
		response.setSubjectName(subjectConfiguration.getSubjectInfo().getName());
		response.setSubjectMergeId(subjectConfiguration.getSubjectMergeId());
		response.setSubjectSerial(subjectConfiguration.getSubjectSerial());
		response.setSubjectStatus(subjectConfiguration.getSubjectStatus());

		
	}
	
	
	public ItemResponse subjectConfigurationListByClassIdAndGroupId(Long classId,Long groupId) {
		
    	ItemResponse itemResponse=new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<SubjectConfiguration> subjectConfigurations=subjectConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdOrderBySubjectSerial(institute, classId, groupId);
		
	    List<SubjectConfigurationViewResponse> responses=new ArrayList<>();
		
	    for(SubjectConfiguration sbjcnf:subjectConfigurations) {
			
			SubjectConfigurationViewResponse response=new SubjectConfigurationViewResponse();
			copySubjectConfigurationToSubjectConfigurationView(sbjcnf, response);
			responses.add(response);
			
		}
	    System.out.println("responses "+responses.size());
		itemResponse.setItem(responses);		
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
	}
	
	
public ItemResponse subjectConfigurationListByClassConfigurationIdAndGroupId(Long classConfigurationId,Long groupId) {
		
    	ItemResponse itemResponse=new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		ClassConfiguration classConfiguration=classConfigurationRepository.findByIdAndInstitute(classConfigurationId, institute);
		
		List<SubjectConfiguration> subjectConfigurations=subjectConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdOrderBySubjectSerial(institute, classConfiguration.getClassInfo().getId(), groupId);
		
	    List<SubjectConfigurationViewResponse> responses=new ArrayList<>();
		
	    for(SubjectConfiguration sbjcnf:subjectConfigurations) {
			
			SubjectConfigurationViewResponse response=new SubjectConfigurationViewResponse();
			copySubjectConfigurationToSubjectConfigurationView(sbjcnf, response);
			responses.add(response);
			
		}
		itemResponse.setItem(responses);		
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
	}
	

public ItemResponse choosableSubjectConfigurationListByClassConfigurationIdAndGroupId(Long classConfigurationId,Long groupId) {
	
	ItemResponse itemResponse=new ItemResponse();
	
	Institute institute=UserInfoUtils.getLoggedInInstitute();
	
	ClassConfiguration classConfiguration=classConfigurationRepository.findByIdAndInstitute(classConfigurationId, institute);
	
	List<SubjectConfiguration> subjectConfigurations=subjectConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdAndSubjectStatusOrderBySubjectSerial(institute, classConfiguration.getClassInfo().getId(), groupId,1);
	
    List<SubjectConfigurationViewResponse> responses=new ArrayList<>();
	
    for(SubjectConfiguration sbjcnf:subjectConfigurations) {
		
		SubjectConfigurationViewResponse response=new SubjectConfigurationViewResponse();
		copySubjectConfigurationToSubjectConfigurationView(sbjcnf, response);
		responses.add(response);
		
	}
	itemResponse.setItem(responses);		
	itemResponse.setMessage("OK");
	itemResponse.setMessageType(1);
	return itemResponse;
}
	
	
	@Transactional
	public BaseResponse deleteSubjectConfiguration(Long subjectConfigId) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		SubjectConfiguration subjectConfiguration=subjectConfigurationRepository.findBySubjectConfigurationIdAndInstitute(subjectConfigId, institute);
		if(subjectConfiguration == null) {
			baseResponse.setMessage("No Configuration Found.");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		subjectConfigurationRepository.delete(subjectConfiguration);
				
		baseResponse.setMessage("Subject Configuration Successfully Deleted.");
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	

}
