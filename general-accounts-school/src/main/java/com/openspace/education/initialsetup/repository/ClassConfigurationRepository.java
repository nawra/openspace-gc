package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.institute.model.entity.Institute;

public interface ClassConfigurationRepository extends JpaRepository<ClassConfiguration, Long> {

	public ClassConfiguration findByClassInfo_IdAndShiftInfo_idAndSectionInfo_idAndInstitute_InstituteId(Long classId,Long shiftId, Long sectionId,Long instituteId);
	public List<ClassConfiguration>findByInstituteOrderBySerial(Institute institute);
	public ClassConfiguration findByIdAndInstitute(Long id,Institute institute);
	public List<ClassConfiguration> findByIdInAndInstitute(List<Long> classConfigIds,Institute institute);
}
