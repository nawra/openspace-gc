package com.openspace.education.initialsetup.model.response;



public class GlobalCoreSettingView {

	private String name;
	
	private Integer defaultId;
	
	

	public GlobalCoreSettingView(String name, Integer defaultId) {
		this.name = name;
		this.defaultId = defaultId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}
	
	
	
	


}
