package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.SubjectConfiguration;
import com.openspace.education.institute.model.entity.Institute;

public interface SubjectConfigurationRepository extends JpaRepository<SubjectConfiguration, Long>{
	
	

	public SubjectConfiguration findBySubjectConfigurationIdAndInstitute(Long subjectConfigId,Institute institute);
	
	public List<SubjectConfiguration> findByInstituteAndSubjectInfo_IdIn(Institute institute,List<Long> subjectIds);
	
	public List<SubjectConfiguration> findBySubjectConfigurationIdInAndInstitute(List<Long> subjectConfigIds,Institute institute);
	
	public List<SubjectConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdOrderBySubjectSerial(Institute institute,Long classId,Long groupId);
	
	public List<SubjectConfiguration> findByInstituteAndClassInfo_IdOrderBySubjectSerial(Institute institute,Long classId);

	public List<SubjectConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndSubjectStatusOrderBySubjectSerial(Institute institute,Long classId,Long groupId,Integer subjectStatus);
	
	public List<SubjectConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndAndSubjectInfo_IdInOrderBySubjectSerial(Institute institute,Long classId,Long groupId,List<Long> subjectIds);
	
	public SubjectConfiguration findByInstituteAndClassInfo_IdAndGroupInfo_IdAndSubjectInfo_Id(Institute institute,Long classId,Long groupId,Long subjectId);

	public List<SubjectConfiguration>  findByInstituteAndClassInfo_Id(Institute institute,Long classId);

	
}
