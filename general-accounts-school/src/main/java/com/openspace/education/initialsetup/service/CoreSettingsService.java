package com.openspace.education.initialsetup.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.generalaccount.service.GeneralAccountService;
import com.openspace.education.initialsetup.model.entity.CoreSettingAcademicYear;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingDesignation;
import com.openspace.education.initialsetup.model.entity.CoreSettingExam;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingPeriod;
import com.openspace.education.initialsetup.model.entity.CoreSettingSection;
import com.openspace.education.initialsetup.model.entity.CoreSettingShift;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.initialsetup.model.entity.CoreSettingSubject;
import com.openspace.education.initialsetup.model.request.CoreSettingsRequest;
import com.openspace.education.initialsetup.model.request.CoresettingUpdateRequest;
import com.openspace.education.initialsetup.model.request.PeriodCreateRequest;
import com.openspace.education.initialsetup.model.response.CoreSettingsResponse;
import com.openspace.education.initialsetup.model.response.GlobalCoreSettingView;
import com.openspace.education.initialsetup.model.response.PeriodViewResponse;
import com.openspace.education.initialsetup.repository.CoreSettingAcademicYearRepository;
import com.openspace.education.initialsetup.repository.CoreSettingClassRepository;
import com.openspace.education.initialsetup.repository.CoreSettingDesignationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingExamRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeSubHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeWaiverRepository;
import com.openspace.education.initialsetup.repository.CoreSettingGroupRepository;
import com.openspace.education.initialsetup.repository.CoreSettingPeriodRepository;
import com.openspace.education.initialsetup.repository.CoreSettingSectionRepository;
import com.openspace.education.initialsetup.repository.CoreSettingShiftRepository;
import com.openspace.education.initialsetup.repository.CoreSettingStudentCategoryRepository;
import com.openspace.education.initialsetup.repository.CoreSettingSubjectRepository;
import com.openspace.education.institute.model.entity.Institute;

@Service
public class CoreSettingsService {
	
	public Logger logger = LoggerFactory.getLogger(CoreSettingsService.class);
	
	@Autowired
	private CoreSettingClassRepository coreSettingClassRepository;
	
	@Autowired
	private CoreSettingShiftRepository coreSettingShiftRepository;
	
	@Autowired
	private CoreSettingSectionRepository coreSettingSectionRepository;
	
	@Autowired
	private CoreSettingDesignationRepository coreSettingDesignationRepository;
	
	@Autowired
	private CoreSettingGroupRepository coreSettingGroupRepository;
	
	@Autowired
	private CoreSettingStudentCategoryRepository coreSettingStudentCategoryRepository;
	
	
	@Autowired
	private CoreSettingAcademicYearRepository coreSettingAcademicYearRepository;
	
	@Autowired
	private CoreSettingExamRepository coreSettingExamRepository;
	
	@Autowired
	private CoreSettingSubjectRepository coreSettingSubjectRepository;
	
	@Autowired
	private CoreSettingPeriodRepository coreSettingPeriodRepository;
	
	
	@Autowired
	private CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	private CoreSettingFeeSubHeadRepository coreSettingFeeSubHeadRepository;
	
	@Autowired
	private CoreSettingFeeWaiverRepository coreSettingFeeWaiverRepository;
	
	

	
		///////////////////////////////// Academic Year ///////////////////////////////////////
			
			
			
		@Transactional
		public BaseResponse createCoreSettingAcademicYear(CoreSettingsRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		
		if (coreSettingAcademicYearRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		baseResponse.setMessage("" + request.getName() + " Already exists.");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		CoreSettingAcademicYear coreSettingAcademicYear = new CoreSettingAcademicYear();
		
		coreSettingAcademicYear.setName(request.getName());
		coreSettingAcademicYear.setStatus(1);
		coreSettingAcademicYear.setSerial(request.getSerial());
		coreSettingAcademicYear.setInstitute(institute);
		
		coreSettingAcademicYearRepository.save(coreSettingAcademicYear);
		
		baseResponse.setMessage("Academic Year Successfully Created");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		@Transactional
		public BaseResponse updateCoreSettingAcademicYear(CoresettingUpdateRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingAcademicYear coreSettingAcademicYear =coreSettingAcademicYearRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingAcademicYear.setName(request.getName());
		coreSettingAcademicYear.setStatus(request.getStatus());
		coreSettingAcademicYear.setSerial(request.getSerial());
		
		baseResponse.setMessage("Academic Year Successfully Updated");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public BaseResponse deleteCoreSettingAcademicYear(Long id){
		
		BaseResponse baseResponse=new BaseResponse(); 
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingAcademicYear coreSettingAcademicYear =coreSettingAcademicYearRepository.findByIdAndInstitute(id, institute);
		
		if(coreSettingAcademicYear==null){
		baseResponse.setMessage("Academic Year not found");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		try{
		
		coreSettingAcademicYearRepository.delete(coreSettingAcademicYear);
		
		}catch(DataIntegrityViolationException e){
		logger.error(e.getMessage());
		baseResponse.setMessage("This Academic Year is assigned in another configuration");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		catch(Exception e){
		logger.error(e.getMessage());
		baseResponse.setMessage("Operation Failed");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		
		baseResponse.setMessage("Academic Year successfully deleted");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		public ItemResponse academicYearList(Institute institute) {
		
		ItemResponse itemResponse = new ItemResponse();
		
		
		List<CoreSettingAcademicYear> list=coreSettingAcademicYearRepository.findByInstituteOrderBySerial(institute);
		
		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingAcademicYear acYear:list) {
		CoreSettingsResponse response=new CoreSettingsResponse();
		response.setId(acYear.getId());
		response.setName(acYear.getName());
		response.setSerial(acYear.getSerial());
		response.setStatus(acYear.getStatus());
		responses.add(response);
		
		}
		
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		}
	
	
	 //========================================= Class ======================================================
	
	@Transactional
	public BaseResponse createCoreSettingClass(CoreSettingsRequest coreSettingsRequest) {

		BaseResponse baseResponse = new BaseResponse();
		Long instituteId = UserInfoUtils.getLoggedInInstituteId();
		Institute institute = UserInfoUtils.getLoggedInInstitute();

		if (coreSettingClassRepository.findByNameAndInstitute_InstituteId(coreSettingsRequest.getName(), instituteId) != null) {
			baseResponse.setMessage("" + coreSettingsRequest.getName() + " Already exists.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		CoreSettingClass coreSettingClass = new CoreSettingClass();

		coreSettingClass.setName(coreSettingsRequest.getName());
		coreSettingClass.setStatus(1);
		coreSettingClass.setSerial(coreSettingsRequest.getSerial());
		coreSettingClass.setInstitute(institute);

		coreSettingClassRepository.save(coreSettingClass);

		baseResponse.setMessage("Class Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	public ItemResponse getAllClassList(Institute institute){
		
	      ItemResponse itemResponse=new ItemResponse();
	      
	      
	      
	      List<CoreSettingsResponse> coreSettingsResponseList = new ArrayList<>();
	      
	      List<CoreSettingClass> coreSettingClassList = coreSettingClassRepository.findByInstituteOrderBySerial(institute);
	      
			coreSettingClassList.forEach(csc -> {
				CoreSettingsResponse coreSettingsResponse = new CoreSettingsResponse();
				BeanUtils.copyProperties(csc, coreSettingsResponse);
				coreSettingsResponseList.add(coreSettingsResponse);
			});
	     
	      itemResponse.setItem(coreSettingsResponseList);
	      itemResponse.setMessage("OK");
	      itemResponse.setMessageType(1);
	      return itemResponse;
	        
	    }
	
	
	
	public ItemResponse getAllClassListByInstituteId(Long instituteId){
		
	      ItemResponse itemResponse=new ItemResponse();
	      	      
	      List<CoreSettingsResponse> coreSettingsResponseList = new ArrayList<>();
	      
	      List<CoreSettingClass> coreSettingClassList = coreSettingClassRepository.findByInstituteInstituteIdOrderBySerial(instituteId);
	      
			coreSettingClassList.forEach(csc -> {
				CoreSettingsResponse coreSettingsResponse = new CoreSettingsResponse();
				BeanUtils.copyProperties(csc, coreSettingsResponse);
				coreSettingsResponseList.add(coreSettingsResponse);
			});
	     
	      itemResponse.setItem(coreSettingsResponseList);
	      itemResponse.setMessage("OK");
	      itemResponse.setMessageType(1);
	      return itemResponse;
	        
	    }
	
	public ItemResponse getAllClassListByInstituteId2(Long instituteId){
		
	      ItemResponse itemResponse=new ItemResponse();
	      	      
	      List<CoreSettingsResponse> coreSettingsResponseList = new ArrayList<>();
	      
	      List<CoreSettingClass> coreSettingClassList = coreSettingClassRepository.findByInstituteInstituteIdAndOnlineAdmissionOrderBySerial(instituteId,1);
	      
			coreSettingClassList.forEach(csc -> {
				CoreSettingsResponse coreSettingsResponse = new CoreSettingsResponse();
				BeanUtils.copyProperties(csc, coreSettingsResponse);
				coreSettingsResponseList.add(coreSettingsResponse);
			});
	     
	      itemResponse.setItem(coreSettingsResponseList);
	      itemResponse.setMessage("OK");
	      itemResponse.setMessageType(1);
	      return itemResponse;
	        
	    }
	
	
	@Transactional
	public BaseResponse updateCoreSettingClass(CoresettingUpdateRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		
		CoreSettingClass coreSettingClass = coreSettingClassRepository.findOne(request.getId());
		
		if (coreSettingClass!= null) {
			coreSettingClass.setName(request.getName());
			coreSettingClass.setSerial(request.getSerial());
			coreSettingClass.setStatus(request.getStatus());
			
			baseResponse.setMessage("Class Successfully Updated");
			baseResponse.setMessageType(1);
			
		}else {
			baseResponse.setMessage("Something went wrong");
			baseResponse.setMessageType(0);
		}

		return baseResponse;

	}
	
	
	 public BaseResponse deleteCoreSettingClass(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       
	       CoreSettingClass coreSettingClass=coreSettingClassRepository.findOne(id);

	       
	       if(coreSettingClass==null){
	        baseResponse.setMessage("Class not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	   coreSettingClassRepository.delete(coreSettingClass);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Class is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Class successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	 
	 
	 //========================================= Shift ======================================================
	 
	 
	 @Transactional
		public BaseResponse createCoreSettingShift(CoreSettingsRequest coreSettingsRequest) {

			BaseResponse baseResponse = new BaseResponse();
			Long instituteId = UserInfoUtils.getLoggedInInstituteId();
			Institute institute = UserInfoUtils.getLoggedInInstitute();

			if (coreSettingShiftRepository.findByNameAndInstitute_InstituteId(coreSettingsRequest.getName(), instituteId) != null) {
				baseResponse.setMessage("" + coreSettingsRequest.getName() + " Already exists.");
				baseResponse.setMessageType(0);
				return baseResponse;
			}

			CoreSettingShift coreSettingShift = new CoreSettingShift();

			coreSettingShift.setName(coreSettingsRequest.getName());
			coreSettingShift.setSerial(coreSettingsRequest.getSerial());
			coreSettingShift.setStatus(1);
			coreSettingShift.setInstitute(institute);

			coreSettingShiftRepository.save(coreSettingShift);

			baseResponse.setMessage("Shift Successfully Created");
			baseResponse.setMessageType(1);

			return baseResponse;

		}
	 
	 
	 
	 public ItemResponse getAllShiftList(){
			
	      ItemResponse itemResponse=new ItemResponse();
	      
	      Institute institute = UserInfoUtils.getLoggedInInstitute();
	      
	      List<CoreSettingsResponse> coreSettingsResponseList = new ArrayList<>();
	      
	      List<CoreSettingShift> coreSettingShiftList = coreSettingShiftRepository.findByInstituteOrderBySerial(institute);
	      
	      coreSettingShiftList.forEach(css -> {
				CoreSettingsResponse coreSettingsResponse = new CoreSettingsResponse();
				BeanUtils.copyProperties(css, coreSettingsResponse);
				coreSettingsResponseList.add(coreSettingsResponse);
			});
	     
	      itemResponse.setItem(coreSettingsResponseList);
	      itemResponse.setMessage("OK");
	      itemResponse.setMessageType(1);
	      return itemResponse;
	        
	    }
	 
	 
	 @Transactional
		public BaseResponse updateCoreSettingShift(CoresettingUpdateRequest request) {

			BaseResponse baseResponse = new BaseResponse();
			
			CoreSettingShift coreSettingShift = coreSettingShiftRepository.findOne(request.getId());
			
			if (coreSettingShift!= null) {
				coreSettingShift.setName(request.getName());
				coreSettingShift.setSerial(request.getSerial());
				coreSettingShift.setStatus(request.getStatus());
				
				baseResponse.setMessage("Shift Successfully Updated");
				baseResponse.setMessageType(1);
				
			}else {
				baseResponse.setMessage("Something went wrong");
				baseResponse.setMessageType(0);
			}

			return baseResponse;

		}
	 
	 
	 
	 public BaseResponse deleteCoreSettingShift(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       
	       CoreSettingShift coreSettingShift=coreSettingShiftRepository.findOne(id);

	       
	       if(coreSettingShift==null){
	        baseResponse.setMessage("Shift not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	   coreSettingShiftRepository.delete(coreSettingShift);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Shift is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Shift successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	
	 
	 
	 
	 
	 
	//========================================= Section ======================================================
	 
	 
	 
	    @Transactional
		public BaseResponse createCoreSettingSection(CoreSettingsRequest coreSettingsRequest) {

			BaseResponse baseResponse = new BaseResponse();
			Long instituteId = UserInfoUtils.getLoggedInInstituteId();
			Institute institute = UserInfoUtils.getLoggedInInstitute();

			if (coreSettingSectionRepository.findByNameAndInstitute_InstituteId(coreSettingsRequest.getName(), instituteId) != null) {
				baseResponse.setMessage("" + coreSettingsRequest.getName() + " Already exists.");
				baseResponse.setMessageType(0);
				return baseResponse;
			}

			CoreSettingSection coreSettingSection = new CoreSettingSection();

			coreSettingSection.setName(coreSettingsRequest.getName());
			coreSettingSection.setSerial(coreSettingsRequest.getSerial());
			coreSettingSection.setStatus(1);
			coreSettingSection.setInstitute(institute);

			coreSettingSectionRepository.save(coreSettingSection);

			baseResponse.setMessage("Section Successfully Created");
			baseResponse.setMessageType(1);

			return baseResponse;

		}
	 
	 
	 
	 public ItemResponse getAllSectionList(){
			
	      ItemResponse itemResponse=new ItemResponse();
	      
	      Institute institute = UserInfoUtils.getLoggedInInstitute();
	      
	      List<CoreSettingsResponse> coreSettingsResponseList = new ArrayList<>();
	      
	      List<CoreSettingSection> coreSettingSectionList = coreSettingSectionRepository.findByInstituteOrderBySerial(institute);
	      
	      coreSettingSectionList.forEach(css -> {
				CoreSettingsResponse coreSettingsResponse = new CoreSettingsResponse();
				BeanUtils.copyProperties(css, coreSettingsResponse);
				coreSettingsResponseList.add(coreSettingsResponse);
			});
	     
	      itemResponse.setItem(coreSettingsResponseList);
	      itemResponse.setMessage("OK");
	      itemResponse.setMessageType(1);
	      return itemResponse;
	        
	    }
	 
	 
	   @Transactional
		public BaseResponse updateCoreSettingSection(CoresettingUpdateRequest request) {

			BaseResponse baseResponse = new BaseResponse();
			
			CoreSettingSection coreSettingSection = coreSettingSectionRepository.findOne(request.getId());
			
			if (coreSettingSection!= null) {
				coreSettingSection.setName(request.getName());
				coreSettingSection.setSerial(request.getSerial());
				coreSettingSection.setStatus(request.getStatus());
				
				baseResponse.setMessage("Section Successfully Updated");
				baseResponse.setMessageType(1);
				
			}else {
				baseResponse.setMessage("Something went wrong");
				baseResponse.setMessageType(0);
			}

			return baseResponse;

		}
	 
	 
	 
	 public BaseResponse deleteCoreSettingSection(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       
	       CoreSettingSection coreSettingSection = coreSettingSectionRepository.findOne(id);
	       
	       if(coreSettingSection==null){
	        baseResponse.setMessage("Section not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	   coreSettingSectionRepository.delete(coreSettingSection);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Section is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Section successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	 
	 
	 
	 
		

	
//////////////////////////////////////////////////// Group ///////////////////////////////////////////////////
	
	
	@Transactional
	public BaseResponse createCoreSettingGroup(CoreSettingsRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();


		if (coreSettingGroupRepository.findByNameAndInstitute(request.getName(), institute) != null) {
			baseResponse.setMessage("" + request.getName() + " Already exists.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		CoreSettingGroup coreSettingGroup = new CoreSettingGroup();

		coreSettingGroup.setName(request.getName());
		coreSettingGroup.setStatus(1);
		coreSettingGroup.setSerial(request.getSerial());
		coreSettingGroup.setInstitute(institute);

		coreSettingGroupRepository.save(coreSettingGroup);

		baseResponse.setMessage("Group Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	@Transactional
	public BaseResponse updateCoreSettingGroup(CoresettingUpdateRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingGroup coreSettingGroup =coreSettingGroupRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingGroup.setName(request.getName());
		coreSettingGroup.setStatus(request.getStatus());
		coreSettingGroup.setSerial(request.getSerial());

		baseResponse.setMessage("Group Successfully Updated");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	public BaseResponse deleteCoreSettingGroup(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       Institute institute = UserInfoUtils.getLoggedInInstitute();
	       CoreSettingGroup coreSettingGroup =coreSettingGroupRepository.findByIdAndInstitute(id, institute);
	       
	       if(coreSettingGroup==null){
	        baseResponse.setMessage("Group not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	   coreSettingGroupRepository.delete(coreSettingGroup);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Group is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Group successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	
	public ItemResponse groupList() {

		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingGroup> list=coreSettingGroupRepository.findByInstituteOrderBySerialAsc(institute);

		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingGroup group:list) {
			CoreSettingsResponse response=new CoreSettingsResponse();
			response.setId(group.getId());
			response.setName(group.getName());
			response.setSerial(group.getSerial());
			response.setStatus(group.getStatus());
			responses.add(response);
			
		}


		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	
	
	///////////////////////////////// Student Category ///////////////////////////////////////
	
	
	
	@Transactional
	public BaseResponse createCoreSettingStudentCategory(CoreSettingsRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();


		if (coreSettingStudentCategoryRepository.findByNameAndInstitute(request.getName(), institute) != null) {
			baseResponse.setMessage("" + request.getName() + " Already exists.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		CoreSettingStudentCategory coreSettingStudentCategory = new CoreSettingStudentCategory();

		coreSettingStudentCategory.setName(request.getName());
		coreSettingStudentCategory.setStatus(1);
		coreSettingStudentCategory.setSerial(request.getSerial());
		coreSettingStudentCategory.setInstitute(institute);

		coreSettingStudentCategoryRepository.save(coreSettingStudentCategory);

		baseResponse.setMessage("Student Category Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	@Transactional
	public BaseResponse updateCoreSettingStudentCategory(CoresettingUpdateRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingStudentCategory coreSettingDesignation =coreSettingStudentCategoryRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingDesignation.setName(request.getName());
		coreSettingDesignation.setStatus(request.getStatus());
		coreSettingDesignation.setSerial(request.getSerial());

		baseResponse.setMessage("Designation Successfully Updated");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	public BaseResponse deleteCoreSettingStudentCategory(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       Institute institute = UserInfoUtils.getLoggedInInstitute();
	       CoreSettingStudentCategory coreSettingStudentCategory =coreSettingStudentCategoryRepository.findByIdAndInstitute(id, institute);
	       
	       if(coreSettingStudentCategory==null){
	        baseResponse.setMessage("Designation not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	   coreSettingStudentCategoryRepository.delete(coreSettingStudentCategory);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Designation is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Designation successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	
	public ItemResponse studentCategoryList() {

		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingStudentCategory> list=coreSettingStudentCategoryRepository.findByInstituteOrderBySerialAsc(institute);

		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingStudentCategory studentCategory:list) {
			CoreSettingsResponse response=new CoreSettingsResponse();
			response.setId(studentCategory.getId());
			response.setName(studentCategory.getName());
			response.setSerial(studentCategory.getSerial());
			response.setStatus(studentCategory.getStatus());
			responses.add(response);
			
		}


		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	
	
	///////////////////////////////// Designation ///////////////////////////////////////
	
	
	
	@Transactional
	public BaseResponse createCoreSettingDesignation(CoreSettingsRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();


		if (coreSettingGroupRepository.findByNameAndInstitute(request.getName(), institute) != null) {
			baseResponse.setMessage("" + request.getName() + " Already exists.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		CoreSettingDesignation coreSettingDesignation = new CoreSettingDesignation();

		coreSettingDesignation.setName(request.getName());
		coreSettingDesignation.setStatus(1);
		coreSettingDesignation.setSerial(request.getSerial());
		coreSettingDesignation.setInstitute(institute);

		coreSettingDesignationRepository.save(coreSettingDesignation);

		baseResponse.setMessage("Designation Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	@Transactional
	public BaseResponse updateCoreSettingDesignation(CoresettingUpdateRequest request) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingDesignation coreSettingDesignation =coreSettingDesignationRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingDesignation.setName(request.getName());
		coreSettingDesignation.setStatus(request.getStatus());
		coreSettingDesignation.setSerial(request.getSerial());

		baseResponse.setMessage("Designation Successfully Updated");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	
	public BaseResponse deleteCoreSettingDesignation(Long id){
	       
	       BaseResponse baseResponse=new BaseResponse(); 
	       Institute institute = UserInfoUtils.getLoggedInInstitute();
		   CoreSettingDesignation coreSettingDesignation =coreSettingDesignationRepository.findByIdAndInstitute(id, institute);
	       
	       if(coreSettingDesignation==null){
	        baseResponse.setMessage("Designation not found");
	        baseResponse.setMessageType(0);
	        return baseResponse;
	       }
	       
	       try{
	           
	    	 coreSettingDesignationRepository.delete(coreSettingDesignation);
	       
	       }catch(DataIntegrityViolationException e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("This Designation is assigned in another configuration");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       catch(Exception e){
	          logger.error(e.getMessage());
	          baseResponse.setMessage("Operation Failed");
	          baseResponse.setMessageType(0);
	          return baseResponse;
	       }
	       
	       
	       baseResponse.setMessage("Designation successfully deleted");
	       baseResponse.setMessageType(1);
	       
	       return baseResponse;
	        
	    }
	
	public ItemResponse designationList() {

		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingDesignation> list=coreSettingDesignationRepository.findByInstituteOrderBySerialAsc(institute);

		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingDesignation designation:list) {
			CoreSettingsResponse response=new CoreSettingsResponse();
			response.setId(designation.getId());
			response.setName(designation.getName());
			response.setSerial(designation.getSerial());
			response.setStatus(designation.getStatus());
			responses.add(response);
			
		}


		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	
       ///////////////////////////////// Exam ///////////////////////////////////////
	
	
	
		@Transactional
		public BaseResponse createCoreSettingExam(CoreSettingsRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		
		if (coreSettingExamRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		baseResponse.setMessage("" + request.getName() + " Already exists.");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		CoreSettingExam coreSettingExam = new CoreSettingExam();
		
		coreSettingExam.setName(request.getName());
		coreSettingExam.setStatus(1);
		coreSettingExam.setSerial(request.getSerial());
		coreSettingExam.setInstitute(institute);
		
		if(isItGrandFinal(request.getName())) {
			coreSettingExam.setDefaultId(99);
		}
		
		coreSettingExamRepository.save(coreSettingExam);
		
		baseResponse.setMessage("Exam Successfully Created");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		public boolean isItGrandFinal(String name) {
			
			if(name.equalsIgnoreCase("Grand Final") || name.equalsIgnoreCase("GrandFinal") || name.equalsIgnoreCase("Grand-Final") || name.equalsIgnoreCase("grandfinal") ) {
				
				return true;
			}
			
			
			return false;
		}
		
		
		
		@Transactional
		public BaseResponse updateCoreSettingExam(CoresettingUpdateRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingExam coreSettingExam =coreSettingExamRepository.findByIdAndInstitute(request.getId(), institute);
		
		if(coreSettingExam.getDefaultId()!=null && coreSettingExam.getDefaultId().equals(99)) {
			coreSettingExam.setStatus(request.getStatus());
			coreSettingExam.setSerial(request.getSerial());	
		}else {
			coreSettingExam.setName(request.getName());
			coreSettingExam.setStatus(request.getStatus());
			coreSettingExam.setSerial(request.getSerial());
		}
		
		if(isItGrandFinal(request.getName())) {
			coreSettingExam.setDefaultId(99);
		}else {
			coreSettingExam.setDefaultId(0);
		}
		
		baseResponse.setMessage("Exam Successfully Updated");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public BaseResponse deleteCoreSettingExam(Long id){
		
		BaseResponse baseResponse=new BaseResponse(); 
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingExam coreSettingExam =coreSettingExamRepository.findByIdAndInstitute(id, institute);
		
		if(coreSettingExam==null){
		baseResponse.setMessage("Exam not found");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		try{
		
			coreSettingExamRepository.delete(coreSettingExam);
		
		}catch(DataIntegrityViolationException e){
		logger.error(e.getMessage());
		baseResponse.setMessage("This Exam is assigned in another configuration");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		catch(Exception e){
		logger.error(e.getMessage());
		baseResponse.setMessage("Operation Failed");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		
		baseResponse.setMessage("Exam successfully deleted");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		public ItemResponse examList() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingExam> list=coreSettingExamRepository.findByInstituteOrderBySerial(institute);
		
		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingExam exam:list) {
		CoreSettingsResponse response=new CoreSettingsResponse();
		response.setId(exam.getId());
		response.setName(exam.getName());
		response.setSerial(exam.getSerial());
		response.setStatus(exam.getStatus());
		responses.add(response);
		
		}
		
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		}
	
		
		///////////////////////////////// Subject ///////////////////////////////////////
				
				
				
		@Transactional
		public BaseResponse createCoreSettingSubject(CoreSettingsRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		
		if (coreSettingSubjectRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		baseResponse.setMessage("" + request.getName() + " Already exists.");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		CoreSettingSubject coreSettingSubject = new CoreSettingSubject();
		
		coreSettingSubject.setName(request.getName());
		coreSettingSubject.setStatus(1);
		coreSettingSubject.setSerial(request.getSerial());
		coreSettingSubject.setInstitute(institute);
		
		coreSettingSubjectRepository.save(coreSettingSubject);
		
		baseResponse.setMessage("Subject Successfully Created");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		@Transactional
		public BaseResponse updateCoreSettingSubject(CoresettingUpdateRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingSubject coreSettingSubject=coreSettingSubjectRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingSubject.setName(request.getName());
		coreSettingSubject.setStatus(request.getStatus());
		coreSettingSubject.setSerial(request.getSerial());
		
		baseResponse.setMessage("Subject Successfully Updated");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public BaseResponse deleteCoreSettingSubject(Long id){
		
		BaseResponse baseResponse=new BaseResponse(); 
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingSubject coreSettingSubject =coreSettingSubjectRepository.findByIdAndInstitute(id, institute);
		
		if(coreSettingSubject==null){
		baseResponse.setMessage("Exam not found");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		try{
		
			coreSettingSubjectRepository.delete(coreSettingSubject);
		
		}catch(DataIntegrityViolationException e){
		logger.error(e.getMessage());
		baseResponse.setMessage("This Subject is assigned in another configuration");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		catch(Exception e){
		logger.error(e.getMessage());
		baseResponse.setMessage("Operation Failed");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		
		baseResponse.setMessage("Subject successfully deleted");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		public ItemResponse subjectList() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingSubject> list=coreSettingSubjectRepository.findByInstituteOrderBySerial(institute);
		
		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingSubject subject:list) {
		CoreSettingsResponse response=new CoreSettingsResponse();
		response.setId(subject.getId());
		response.setName(subject.getName());
		response.setSerial(subject.getSerial());
		response.setStatus(subject.getStatus());
		responses.add(response);
		
		}
		
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		}
		
		
		
	   ///////////////////////////////// Period ///////////////////////////////////////
		

			@SuppressWarnings({ "rawtypes", "unchecked"})
			public ItemResponse globalPeriodList() {
				
				ItemResponse itemResponse = new ItemResponse();
								
				List<GlobalCoreSettingView> responses=new ArrayList<>();
				responses.add(new GlobalCoreSettingView("1st Period", 1));
				responses.add(new GlobalCoreSettingView("2nd Period", 2));
				responses.add(new GlobalCoreSettingView("3rd Period", 3));
				responses.add(new GlobalCoreSettingView("4th Period", 4));
				responses.add(new GlobalCoreSettingView("5th Period", 5));
				responses.add(new GlobalCoreSettingView("6th Period", 6));
				responses.add(new GlobalCoreSettingView("7th Period", 7));
				responses.add(new GlobalCoreSettingView("8th Period", 8));
				responses.add(new GlobalCoreSettingView("9th Period", 9));
				responses.add(new GlobalCoreSettingView("10th Period", 10));
				
				
				itemResponse.setItem(responses);
				itemResponse.setMessage("OK");
				itemResponse.setMessageType(1);
				return itemResponse;
				
				}
			
			
			@Transactional
			public BaseResponse createCoreSettingPeriod(PeriodCreateRequest request) {
			
			BaseResponse baseResponse = new BaseResponse();
			Institute institute = UserInfoUtils.getLoggedInInstitute();
			
			
			if (coreSettingSubjectRepository.findByNameAndInstitute(request.getName(), institute) != null) {
			baseResponse.setMessage("" + request.getName() + " Already exists.");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
			
			CoreSettingPeriod coreSettingPeriod = new CoreSettingPeriod();
			
			coreSettingPeriod.setName(request.getName());
			coreSettingPeriod.setDefaultId(request.getDefaultId());
			coreSettingPeriod.setStatus(1);
			coreSettingPeriod.setSerial(request.getSerial());
			coreSettingPeriod.setInstitute(institute);
			
			coreSettingPeriodRepository.save(coreSettingPeriod);
			
			baseResponse.setMessage("Period Successfully Created");
			baseResponse.setMessageType(1);
			
			return baseResponse;
			
			}
			
	
			
			
			
			public BaseResponse deleteCoreSettingPeriod(Long id){
			
			BaseResponse baseResponse=new BaseResponse(); 
			Institute institute = UserInfoUtils.getLoggedInInstitute();
			CoreSettingPeriod coreSettingPeriod =coreSettingPeriodRepository.findByIdAndInstitute(id, institute);
			
			if(coreSettingPeriod==null){
			baseResponse.setMessage("Exam not found");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
			
			try{
			
				coreSettingPeriodRepository.delete(coreSettingPeriod);
			
			}catch(DataIntegrityViolationException e){
			logger.error(e.getMessage());
			baseResponse.setMessage("This Period is assigned in another configuration");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
			catch(Exception e){
			logger.error(e.getMessage());
			baseResponse.setMessage("Operation Failed");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
			
			
			baseResponse.setMessage("Period successfully deleted");
			baseResponse.setMessageType(1);
			
			return baseResponse;
			
			}
			
			
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public ItemResponse periodList() {
			
			ItemResponse itemResponse = new ItemResponse();
			Institute institute = UserInfoUtils.getLoggedInInstitute();
			
			List<CoreSettingPeriod> list=coreSettingPeriodRepository.findByInstituteOrderBySerial(institute);
			
			List<PeriodViewResponse> responses=new ArrayList<>();
			
			for(CoreSettingPeriod period : list) {
			PeriodViewResponse response=new PeriodViewResponse();
			response.setId(period.getId());
			response.setName(period.getName());
			response.setSerial(period.getSerial());
			response.setStatus(period.getStatus());
			response.setDefaultId(period.getDefaultId());
			responses.add(response);
			
			}
			
			
			itemResponse.setItem(responses);
			itemResponse.setMessage("OK");
			itemResponse.setMessageType(1);
			return itemResponse;
			
			}
			
			
			
			
			
        
		///////////////////////////////// Fee Head ///////////////////////////////////////	
			

        @Transactional
		public BaseResponse createFeeHead(CoreSettingsRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		
		if (coreSettingFeeHeadRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		baseResponse.setMessage("" + request.getName() + " Already exists.");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		CoreSettingFeeHead coreSettingFeeHead = new CoreSettingFeeHead();
		
		if(request.getName().equals("Attendance Fine") || request.getName().equals("Absent Fine")) {
			coreSettingFeeHead.setDefaultId(100);	
		}
		
		coreSettingFeeHead.setName(request.getName());
		coreSettingFeeHead.setStatus(1);
		coreSettingFeeHead.setSerial(request.getSerial());
		coreSettingFeeHead.setInstitute(institute);
		
		coreSettingFeeHeadRepository.save(coreSettingFeeHead);
		
		baseResponse.setMessage("Fee Head Successfully Created");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		@Transactional
		public BaseResponse updateFeeHead(CoresettingUpdateRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingFeeHead coreSettingFeeHead=coreSettingFeeHeadRepository.findByIdAndInstitute(request.getId(), institute);
		
		if(coreSettingFeeHead.getDefaultId()!=null && coreSettingFeeHead.getDefaultId().equals(100)) {
			baseResponse.setMessage("You can not update this fee head.");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		coreSettingFeeHead.setName(request.getName());
		coreSettingFeeHead.setStatus(request.getStatus());
		coreSettingFeeHead.setSerial(request.getSerial());
		
		baseResponse.setMessage("Fee Head Successfully Updated");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public BaseResponse deleteFeeHead(Long id){
		
		BaseResponse baseResponse=new BaseResponse(); 
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingFeeHead coreSettingFeeHead =coreSettingFeeHeadRepository.findByIdAndInstitute(id, institute);
		
		if(coreSettingFeeHead==null){
		baseResponse.setMessage("Fee Head not found");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		if(coreSettingFeeHead.getDefaultId()!=null && coreSettingFeeHead.getDefaultId().equals(100)) {
			baseResponse.setMessage("You can not delete this fee head.");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		try{
		
			coreSettingFeeHeadRepository.delete(coreSettingFeeHead);
		
		}catch(DataIntegrityViolationException e){
		logger.error(e.getMessage());
		baseResponse.setMessage("This Fee Head is assigned in another configuration");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		catch(Exception e){
		logger.error(e.getMessage());
		baseResponse.setMessage("Operation Failed");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		
		baseResponse.setMessage("Fee Head successfully deleted");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public ItemResponse feeHeadList() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		boolean absentFine=true;
		
		List<CoreSettingFeeHead> list=coreSettingFeeHeadRepository.findByInstituteOrderBySerialAsc(institute);
		
		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingFeeHead feehead:list) {
		CoreSettingsResponse response=new CoreSettingsResponse();
		response.setId(feehead.getId());
		response.setName(feehead.getName());
		response.setSerial(feehead.getSerial());
		response.setStatus(feehead.getStatus());
		responses.add(response);
		
		if(feehead.getDefaultId()!=null && feehead.getDefaultId().equals(100)) {
			absentFine=false;	
		}
		
		}
		
		
		if(absentFine) {
			CoreSettingFeeHead coreSettingFeeHead=new CoreSettingFeeHead();
			coreSettingFeeHead.setName("Absent Fine");
			coreSettingFeeHead.setStatus(1);
			coreSettingFeeHead.setSerial(50);	
			coreSettingFeeHead.setDefaultId(100);
			coreSettingFeeHead.setInstitute(institute);
			coreSettingFeeHeadRepository.save(coreSettingFeeHead);
		}
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		}
		
		
		
		
		///////////////////////////////// Fee Sub Head ///////////////////////////////////////	
				
		
		@Transactional
		public BaseResponse createFeeSubHead(CoreSettingsRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		
		if (coreSettingFeeSubHeadRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		baseResponse.setMessage("" + request.getName() + " Already exists.");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		CoreSettingFeeSubHead coreSettingFeeSubHead = new CoreSettingFeeSubHead();
		
		coreSettingFeeSubHead.setName(request.getName());
		coreSettingFeeSubHead.setStatus(1);
		coreSettingFeeSubHead.setSerial(request.getSerial());
		coreSettingFeeSubHead.setInstitute(institute);
		
		coreSettingFeeSubHeadRepository.save(coreSettingFeeSubHead);
		
		baseResponse.setMessage("Fee Sub Head Successfully Created");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		@Transactional
		public BaseResponse updateFeeSubHead(CoresettingUpdateRequest request) {
		
		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingFeeSubHead coreSettingFeeSubHead=coreSettingFeeSubHeadRepository.findByIdAndInstitute(request.getId(), institute);
		coreSettingFeeSubHead.setName(request.getName());
		coreSettingFeeSubHead.setStatus(request.getStatus());
		coreSettingFeeSubHead.setSerial(request.getSerial());
		
		baseResponse.setMessage("Fee Sub Head Successfully Updated");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		
		
		public BaseResponse deleteFeeSubHead(Long id){
		
		BaseResponse baseResponse=new BaseResponse(); 
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		CoreSettingFeeSubHead coreSettingFeeSubHead =coreSettingFeeSubHeadRepository.findByIdAndInstitute(id, institute);
		
		if(coreSettingFeeSubHead==null){
		baseResponse.setMessage("Fee Sub Head not found");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		try{
		
			coreSettingFeeSubHeadRepository.delete(coreSettingFeeSubHead);
		
		}catch(DataIntegrityViolationException e){
		logger.error(e.getMessage());
		baseResponse.setMessage("This Fee Sub Head is assigned in another configuration");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		catch(Exception e){
		logger.error(e.getMessage());
		baseResponse.setMessage("Operation Failed");
		baseResponse.setMessageType(0);
		return baseResponse;
		}
		
		
		baseResponse.setMessage("Fee Sub Head successfully deleted");
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		}
		
		public ItemResponse feeSubHeadList() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingFeeSubHead> list=coreSettingFeeSubHeadRepository.findByInstituteOrderBySerialAsc(institute);
		
		List<CoreSettingsResponse> responses=new ArrayList<>();
		for(CoreSettingFeeSubHead feesubhead:list) {
		CoreSettingsResponse response=new CoreSettingsResponse();
		response.setId(feesubhead.getId());
		response.setName(feesubhead.getName());
		response.setSerial(feesubhead.getSerial());
		response.setStatus(feesubhead.getStatus());
		responses.add(response);
		
		}
		
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		}
		
		
		
		
        ///////////////////////////////// Fee Waiver ///////////////////////////////////////	
		
		
		 @Transactional
		 public BaseResponse createFeeWaiver(CoreSettingsRequest request) {
		 
		 BaseResponse baseResponse = new BaseResponse();
		 Institute institute = UserInfoUtils.getLoggedInInstitute();
		 
		 
		 if (coreSettingFeeWaiverRepository.findByNameAndInstitute(request.getName(), institute) != null) {
		 baseResponse.setMessage("" + request.getName() + " Already exists.");
		 baseResponse.setMessageType(0);
		 return baseResponse;
		 }
		 
		 CoreSettingFeeWaiver coreSettingFeeWaiver = new CoreSettingFeeWaiver();
		 
		 coreSettingFeeWaiver.setName(request.getName());
		 coreSettingFeeWaiver.setStatus(1);
		 coreSettingFeeWaiver.setSerial(request.getSerial());
		 coreSettingFeeWaiver.setInstitute(institute);
		 
		 coreSettingFeeWaiverRepository.save(coreSettingFeeWaiver);
		 
		 baseResponse.setMessage("Fee Waiver Successfully Created");
		 baseResponse.setMessageType(1);
		 
		 return baseResponse;
		 
		 }
		 
		 
		 
		 @Transactional
		 public BaseResponse updateFeeWaiver(CoresettingUpdateRequest request) {
		 
		 BaseResponse baseResponse = new BaseResponse();
		 Institute institute = UserInfoUtils.getLoggedInInstitute();
		 CoreSettingFeeWaiver coreSettingFeeWaiver=coreSettingFeeWaiverRepository.findByIdAndInstitute(request.getId(), institute);
		 coreSettingFeeWaiver.setName(request.getName());
		 coreSettingFeeWaiver.setStatus(request.getStatus());
		 coreSettingFeeWaiver.setSerial(request.getSerial());
		 
		 baseResponse.setMessage("Fee Waiver Successfully Updated");
		 baseResponse.setMessageType(1);
		 
		 return baseResponse;
		 
		 }
		 
		 
		 
		 public BaseResponse deleteFeeWaiver(Long id){
		 
		 BaseResponse baseResponse=new BaseResponse(); 
		 Institute institute = UserInfoUtils.getLoggedInInstitute();
		 CoreSettingFeeWaiver coreSettingFeeWaiver =coreSettingFeeWaiverRepository.findByIdAndInstitute(id, institute);
		 
		 if(coreSettingFeeWaiver==null){
		 baseResponse.setMessage("Fee Waiver not found");
		 baseResponse.setMessageType(0);
		 return baseResponse;
		 }
		 
		 try{
		 
		  coreSettingFeeWaiverRepository.delete(coreSettingFeeWaiver);
		 
		 }catch(DataIntegrityViolationException e){
		 logger.error(e.getMessage());
		 baseResponse.setMessage("This Fee Waiver is assigned in another configuration");
		 baseResponse.setMessageType(0);
		 return baseResponse;
		 }
		 catch(Exception e){
		 logger.error(e.getMessage());
		 baseResponse.setMessage("Operation Failed");
		 baseResponse.setMessageType(0);
		 return baseResponse;
		 }
		 
		 
		 baseResponse.setMessage("Fee Waiver successfully deleted");
		 baseResponse.setMessageType(1);
		 
		 return baseResponse;
		 
		 }
		 
		 
		 
		 public ItemResponse feeWaiverList() {
		 
		 ItemResponse itemResponse = new ItemResponse();
		 Institute institute = UserInfoUtils.getLoggedInInstitute();
		 
		 List<CoreSettingFeeWaiver> list=coreSettingFeeWaiverRepository.findByInstituteOrderBySerialAsc(institute);
		 
		 List<CoreSettingsResponse> responses=new ArrayList<>();
		 for(CoreSettingFeeWaiver feeWaiver:list) {
		 CoreSettingsResponse response=new CoreSettingsResponse();
		 response.setId(feeWaiver.getId());
		 response.setName(feeWaiver.getName());
		 response.setSerial(feeWaiver.getSerial());
		 response.setStatus(feeWaiver.getStatus());
		 responses.add(response);
		 
		 }
		 
		 
		 itemResponse.setItem(responses);
		 itemResponse.setMessage("OK");
		 itemResponse.setMessageType(1);
		 return itemResponse;
		 
		 }
	
		 
		 public List<CoreSettingsResponse> findFeeWaiverList() {
			 
			 Institute institute = UserInfoUtils.getLoggedInInstitute();
			 
			 List<CoreSettingFeeWaiver> list=coreSettingFeeWaiverRepository.findByInstituteOrderBySerialAsc(institute);
			 
			 List<CoreSettingsResponse> responses=new ArrayList<>();
			 for(CoreSettingFeeWaiver feeWaiver:list) {
			 CoreSettingsResponse response=new CoreSettingsResponse();
			 response.setId(feeWaiver.getId());
			 response.setName(feeWaiver.getName());
			 response.setSerial(feeWaiver.getSerial());
			 response.setStatus(feeWaiver.getStatus());
			 responses.add(response);
			 
			 }
			 
			 if(list.size() < 1) {
			
				 CoreSettingFeeWaiver coreSettingFeeWaiver = new CoreSettingFeeWaiver();
				 
				 coreSettingFeeWaiver.setName("Poor Waiver");
				 coreSettingFeeWaiver.setStatus(1);
				 coreSettingFeeWaiver.setSerial(10);
				 coreSettingFeeWaiver.setInstitute(institute);
				 
				 coreSettingFeeWaiver=coreSettingFeeWaiverRepository.save(coreSettingFeeWaiver);
				 
				 CoreSettingsResponse response=new CoreSettingsResponse();
				 response.setId(coreSettingFeeWaiver.getId());
				 response.setName("Poor Waiver");
				 response.setSerial(10);
				 response.setStatus(1);
				 responses.add(response);
				 
			 }
			 
			 return responses;
			 
			 }
		
}
