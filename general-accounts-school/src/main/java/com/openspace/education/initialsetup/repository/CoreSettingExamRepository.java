package com.openspace.education.initialsetup.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingExam;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingExamRepository extends JpaRepository<CoreSettingExam, Long>{
	
    public CoreSettingExam findByNameAndInstitute(String className,Institute institute);
	
	public List<CoreSettingExam> findByInstituteOrderBySerial(Institute institute);
	
	public CoreSettingExam findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingExam> findByIdInAndInstitute(List<Long> ids,Institute institute);

}
