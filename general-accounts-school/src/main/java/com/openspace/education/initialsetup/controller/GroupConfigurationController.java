package com.openspace.education.initialsetup.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.initialsetup.model.request.GroupConfigurationCreateRequest;
import com.openspace.education.initialsetup.model.request.GroupConfigurationSerialUpdate;
import com.openspace.education.initialsetup.service.GroupConfigurationService;

@RestController
@RequestMapping(value="/group/configuration")
public class GroupConfigurationController {
	
	
	@Autowired
	public GroupConfigurationService groupConfigurationService;
	
	
	//+++++++++++++++++++++++++++++++++++++Group Configuration++++++++++++++++++++++++++++++++++++++++++++		
	
	@PostMapping(value = "/create")
	public ResponseEntity<BaseResponse> groupConfigurationCreate(@RequestBody  @Valid  GroupConfigurationCreateRequest groupConfigurationRequest)  {
	return new ResponseEntity<>(this.groupConfigurationService.createGroupConfiguration(groupConfigurationRequest),HttpStatus.CREATED);
	}
	
	
	 @PostMapping(value = "/serial/update") 
	 public ResponseEntity<BaseResponse> groupConfigurationUpdate(@RequestBody @Valid List<GroupConfigurationSerialUpdate> requests) { 
	  BaseResponse baseResponse=groupConfigurationService.updateGroupConfigurationSerial(requests);
	  return new ResponseEntity<>(baseResponse,HttpStatus.CREATED); 
	  }	 
	 
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> groupConfigurationDelete(@RequestParam Long id)  {
	return new ResponseEntity<>(groupConfigurationService.deleteGroupConfiguration(id),HttpStatus.OK);
	}
	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> groupConfigurationList(){
	return new ResponseEntity<>(groupConfigurationService.getAllGroupConfigurationList(),HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/list/by/classconfig-id")
	public ResponseEntity<ItemResponse> groupConfigurationListByClassConfigId(@RequestParam Long id){
	return new ResponseEntity<>(groupConfigurationService.groupConfigurationListByClassConfigId(id),HttpStatus.OK);
	}
	
	@GetMapping(value = "/list/by/class-id")
	public ResponseEntity<ItemResponse> groupConfigurationListByClassId(@RequestParam Long classId){
	return new ResponseEntity<>(groupConfigurationService.groupConfigurationListByClassId(classId),HttpStatus.OK);
	}
	
	
	

}
