package com.openspace.education.initialsetup.model.response;

public class PeriodViewResponse extends CoreSettingsResponse{
	
	private Integer defaultId;

	public Integer getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}
	
	

}
