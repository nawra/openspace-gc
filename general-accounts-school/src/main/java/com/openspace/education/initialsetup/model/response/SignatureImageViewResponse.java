package com.openspace.education.initialsetup.model.response;

public class SignatureImageViewResponse {

	private Long signatureId;

	private String signatureTitle;

	private Integer usedId;

	private String usedName;

	private String signImage;

	private Integer signStatus;
	
	private String signStrStatus;
	
	

	public Long getSignatureId() {
		return signatureId;
	}

	public void setSignatureId(Long signatureId) {
		this.signatureId = signatureId;
	}

	public String getSignatureTitle() {
		return signatureTitle;
	}

	public void setSignatureTitle(String signatureTitle) {
		this.signatureTitle = signatureTitle;
	}

	public Integer getUsedId() {
		return usedId;
	}

	public void setUsedId(Integer usedId) {
		this.usedId = usedId;
	}

	public String getUsedName() {
		return usedName;
	}

	public void setUsedName(String usedName) {
		this.usedName = usedName;
	}

	public String getSignImage() {
		return signImage;
	}

	public void setSignImage(String signImage) {
		this.signImage = signImage;
	}

	public Integer getSignStatus() {
		return signStatus;
	}

	public void setSignStatus(Integer signStatus) {
		this.signStatus = signStatus;
	}

	public String getSignStrStatus() {
		return signStrStatus;
	}

	public void setSignStrStatus(String signStrStatus) {
		this.signStrStatus = signStrStatus;
	}
	
	


}
