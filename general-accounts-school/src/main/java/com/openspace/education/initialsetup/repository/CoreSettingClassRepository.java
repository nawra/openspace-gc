package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingClassRepository extends JpaRepository<CoreSettingClass, Long> {
	
	public CoreSettingClass findByNameAndInstitute_InstituteId(String className,Long instituteId);
	
	public CoreSettingClass findByIdAndInstitute(Long classId,Institute institute);
	
	public List<CoreSettingClass> findByInstituteOrderBySerial(Institute institute);
	
	public List<CoreSettingClass> findByInstituteInstituteIdOrderBySerial(Long instituteId);
	
	public List<CoreSettingClass> findByInstituteInstituteIdAndOnlineAdmissionOrderBySerial(Long instituteId,Integer onlineAdmission);
	
//	public int findTopByOrderByIdDesc();

}
