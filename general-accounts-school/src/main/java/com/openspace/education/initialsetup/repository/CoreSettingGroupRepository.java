package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingGroupRepository extends JpaRepository<CoreSettingGroup, Long>{
	
	public CoreSettingGroup findByNameAndInstitute(String name,Institute institute);
	
    public CoreSettingGroup findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingGroup> findByInstituteOrderBySerialAsc(Institute institute);
}
