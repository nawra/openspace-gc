package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingDesignation;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingDesignationRepository extends JpaRepository<CoreSettingDesignation, Long>{
	
	public CoreSettingDesignation findByNameAndInstitute(String name,Institute institute);
	
	public CoreSettingDesignation findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingDesignation> findByInstituteOrderBySerialAsc(Institute institute);

}
