package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.DefaultSignature;
import com.openspace.education.institute.model.entity.Institute;

public interface DefaultSignatureRepository extends JpaRepository<DefaultSignature, Long> {

	public List<DefaultSignature> findByInstitute(Institute institute);
	public DefaultSignature findByInstitute_InstituteIdAndUsedId(Long instituteId,Integer usedId);
	public DefaultSignature findBySignatureIdAndInstitute(Long signatureId,Institute institute);

}
