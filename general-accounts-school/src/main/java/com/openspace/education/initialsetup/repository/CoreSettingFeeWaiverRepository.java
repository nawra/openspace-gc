package com.openspace.education.initialsetup.repository;

import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingFeeWaiverRepository extends JpaRepository<CoreSettingFeeWaiver, Long>{
	
	public CoreSettingFeeWaiver findByNameAndInstitute(String name,Institute institute);
	
	public CoreSettingFeeWaiver findByIdAndInstitute(Long id,Institute institute);
		
    public List<CoreSettingFeeWaiver> findByInstituteOrderBySerialAsc(Institute institute);
    
    public List<CoreSettingFeeWaiver> findByInstituteAndIdInOrderBySerialAsc(Institute institute, Set<Long> waiverIds);

}
