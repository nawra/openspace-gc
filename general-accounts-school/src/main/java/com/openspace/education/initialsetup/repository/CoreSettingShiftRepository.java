package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingShift;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingShiftRepository extends JpaRepository<CoreSettingShift, Long>{
	
	public CoreSettingShift findByNameAndInstitute_InstituteId(String shiftName,Long instituteId);
	
	public List<CoreSettingShift> findByInstituteOrderBySerial(Institute institute);

}
