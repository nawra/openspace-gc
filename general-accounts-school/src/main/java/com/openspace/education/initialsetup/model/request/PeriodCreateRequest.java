package com.openspace.education.initialsetup.model.request;

public class PeriodCreateRequest extends CoreSettingsRequest{
	
	private Integer defaultId;

	public Integer getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}
	
	

}
