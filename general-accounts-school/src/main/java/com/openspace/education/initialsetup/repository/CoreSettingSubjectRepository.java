package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingSubject;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingSubjectRepository extends JpaRepository<CoreSettingSubject, Long>{
	
    public CoreSettingSubject findByNameAndInstitute(String className,Institute institute);
	
	public List<CoreSettingSubject> findByInstituteOrderBySerial(Institute institute);
	
	public CoreSettingSubject findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingSubject> findByIdInAndInstituteOrderBySerial(List<Long> ids,Institute institute);

}
