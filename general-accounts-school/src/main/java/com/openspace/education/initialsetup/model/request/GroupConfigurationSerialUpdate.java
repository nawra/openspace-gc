package com.openspace.education.initialsetup.model.request;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class GroupConfigurationSerialUpdate {
	
	@NotNull
	private Long id;
	private int serial;
	private Integer applicationFee;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getSerial() {
		return serial;
	}
	public void setSerial(int serial) {
		this.serial = serial;
	}
	public Integer getApplicationFee() {
		return applicationFee;
	}
	public void setApplicationFee(Integer applicationFee) {
		this.applicationFee = applicationFee;
	}

}
