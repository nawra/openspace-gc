package com.openspace.education.initialsetup.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingFeeSubHeadRepository extends JpaRepository<CoreSettingFeeSubHead, Long>{
	
	 public CoreSettingFeeSubHead findByNameAndInstitute(String name,Institute institute);
		
	 public CoreSettingFeeSubHead findByIdAndInstitute(Long id,Institute institute);
		
     public List<CoreSettingFeeSubHead> findByInstituteOrderBySerialAsc(Institute institute);
     
     public List<CoreSettingFeeSubHead> findByInstituteAndIdInOrderBySerialAsc(Institute institute,Set<Long> feeSubHeadIds);

}
