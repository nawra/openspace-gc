package com.openspace.education.initialsetup.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "default_signature", uniqueConstraints = @UniqueConstraint(columnNames = {"institute_id", "used_id"}))
public class DefaultSignature implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "signature_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long signatureId;

	
	@Column(name = "signature_title", nullable = false)
	private String signatureTitle;

	@Column(name = "used_id",nullable = false)
	private Integer usedId;

	@Column(name = "used_name")
	private String usedName;

	@Column(name = "sign_img_name")
	private String signImgName;

	@Column(name = "sign_status",nullable = false)
	private Integer signStatus;

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;

	public Long getSignatureId() {
		return signatureId;
	}

	public void setSignatureId(Long signatureId) {
		this.signatureId = signatureId;
	}

	public String getSignatureTitle() {
		return signatureTitle;
	}

	public void setSignatureTitle(String signatureTitle) {
		this.signatureTitle = signatureTitle;
	}

	public Integer getUsedId() {
		return usedId;
	}

	public void setUsedId(Integer usedId) {
		this.usedId = usedId;
	}

	public String getUsedName() {
		return usedName;
	}

	public void setUsedName(String usedName) {
		this.usedName = usedName;
	}

	public String getSignImgName() {
		return signImgName;
	}

	public void setSignImgName(String signImgName) {
		this.signImgName = signImgName;
	}

	public Integer getSignStatus() {
		return signStatus;
	}

	public void setSignStatus(Integer signStatus) {
		this.signStatus = signStatus;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

}
