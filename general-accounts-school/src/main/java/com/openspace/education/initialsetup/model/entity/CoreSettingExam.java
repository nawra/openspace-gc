package com.openspace.education.initialsetup.model.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "core_setting_exam", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "institute_id" }))
public class CoreSettingExam implements Serializable {

	
	
	private static final long serialVersionUID = 1891110674967061818L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "status")
	private int status = 1;

	@Column(name = "serial")
	private int serial = 0;
	
	@Column(name = "default_id")
	private Integer defaultId;    // 99 for grand final

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Integer getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}
	
	

}
