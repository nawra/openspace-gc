package com.openspace.education.initialsetup.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.request.CoreSettingsRequest;
import com.openspace.education.initialsetup.model.request.CoresettingUpdateRequest;
import com.openspace.education.initialsetup.model.request.PeriodCreateRequest;
import com.openspace.education.initialsetup.service.CoreSettingsService;
import com.openspace.education.institute.model.entity.Institute;


@Controller
@RequestMapping(value = "/initial-setup")
public class InitialSetupController {
	
	
		@Autowired
	    public CoreSettingsService coreSettingsService;
		
		
	
	
		//+++++++++++++++++++++++++++++++++++++++ Designation +++++++++++++++++++++++++++++++++++++++++++
	
	
		@PostMapping(value = "/designation/create")
	    public ResponseEntity<BaseResponse> createDesignation(@RequestBody CoreSettingsRequest request)  {
	        return new ResponseEntity<>(coreSettingsService.createCoreSettingDesignation(request), HttpStatus.CREATED);
	    }
		
		@PostMapping(value = "/designation/update")
	    public ResponseEntity<BaseResponse> createDesignation(@RequestBody @Valid CoresettingUpdateRequest request)  {
	        return new ResponseEntity<>(coreSettingsService.updateCoreSettingDesignation(request), HttpStatus.CREATED);
	    }
		
		@DeleteMapping(value = "/designation/delete")
	    public ResponseEntity<BaseResponse> deleteDesignation(@RequestParam Long id)  {
	        return new ResponseEntity<>(coreSettingsService.deleteCoreSettingDesignation(id), HttpStatus.CREATED);
	    }
		
	    @GetMapping(value = "/designation/list")
	    public ResponseEntity<ItemResponse> designationList()  {
	        return new ResponseEntity<>(coreSettingsService.designationList(), HttpStatus.OK);
	    }
    
    
        //++++++++++++++++++++++++++++++++++++ Academic Year +++++++++++++++++++++++++++++++++++++++
    
	    @PostMapping(value = "/academic-year/create")
	    public ResponseEntity<BaseResponse> createAcademicYear(@RequestBody CoreSettingsRequest request)  {
	        return new ResponseEntity<>(coreSettingsService.createCoreSettingAcademicYear(request), HttpStatus.CREATED);
	    }
		
		@PostMapping(value = "/academic-year/update")
	    public ResponseEntity<BaseResponse> updateAcademicYear(@RequestBody @Valid CoresettingUpdateRequest request)  {
	        return new ResponseEntity<>(coreSettingsService.updateCoreSettingAcademicYear(request), HttpStatus.CREATED);
	    }
		
		@DeleteMapping(value = "/academic-year/delete")
	    public ResponseEntity<BaseResponse> deleteAcademicYear(@RequestParam Long id)  {
	        return new ResponseEntity<>(coreSettingsService.deleteCoreSettingAcademicYear(id), HttpStatus.CREATED);
	    }
		
	    @GetMapping(value = "/academic-year/list")
	    public ResponseEntity<ItemResponse> academicYearList()  {
	    	Institute institute = UserInfoUtils.getLoggedInInstitute();
	        return new ResponseEntity<>(coreSettingsService.academicYearList(institute), HttpStatus.OK);
	      
	    }
    
    
        //++++++++++++++++++++++++++++++++++++ Class +++++++++++++++++++++++++++++++++++++++++++++
    
	    @PostMapping(value = "/class/create")
	    public ResponseEntity<BaseResponse> coreSettingClassCreate(@RequestBody @Valid  CoreSettingsRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.createCoreSettingClass(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value = "/class/update",method = RequestMethod.POST)
	    public ResponseEntity<BaseResponse> coreSettingClassUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingClass(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value = "/class/delete", method = RequestMethod.DELETE)
	    public ResponseEntity<BaseResponse> deleteLedger(@RequestParam Long id)  {
	        return new ResponseEntity<>(this.coreSettingsService.deleteCoreSettingClass(id), HttpStatus.OK);
	    }
	    
	    
	    @RequestMapping(value = "/class/list", method = RequestMethod.GET)
	    public ResponseEntity<ItemResponse> classList()  {
	    	Institute institute = UserInfoUtils.getLoggedInInstitute();
	        return new ResponseEntity<>(coreSettingsService.getAllClassList(institute), HttpStatus.OK);
	    }
	    
	    
	/*
	 * @RequestMapping(value = "/class/list/by/instituteId", method =
	 * RequestMethod.GET) public ResponseEntity<ItemResponse>
	 * classListByInstituteId(@RequestParam Long instituteId) { return new
	 * ResponseEntity<>(coreSettingsService.getAllClassListByInstituteId(instituteId
	 * ), HttpStatus.OK); }
	 */
    
	    
    
        //++++++++++++++++++++++++++++++++++++++++++ Shift +++++++++++++++++++++++++++++++++++++++++++++
    
    
	    @PostMapping(value = "/shift/create")
	    public ResponseEntity<BaseResponse> coreSettingShiftCreate(@RequestBody @Valid  CoreSettingsRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.createCoreSettingShift(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value = "/shift/update",method = RequestMethod.POST)
	    public ResponseEntity<BaseResponse> coreSettingShiftUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingShift(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @RequestMapping(value = "/shift/delete", method = RequestMethod.DELETE)
	    public ResponseEntity<BaseResponse> deleteShift(@RequestParam Long id)  {
	        return new ResponseEntity<>(this.coreSettingsService.deleteCoreSettingShift(id), HttpStatus.OK);
	    }
	    
	    
	    @RequestMapping(value = "/shift/list", method = RequestMethod.GET)
	    public ResponseEntity<ItemResponse> shiftList()  {
	        return new ResponseEntity<>(coreSettingsService.getAllShiftList(), HttpStatus.OK);
	    }
    
    
       //++++++++++++++++++++++++++++++++++++++++++++ Section ++++++++++++++++++++++++++++++++++++++++++++
    
    
	    @PostMapping(value = "/section/create")
	    public ResponseEntity<BaseResponse> coreSettingSectionCreate(@RequestBody @Valid  CoreSettingsRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.createCoreSettingSection(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @PostMapping(value = "/section/update")
	    public ResponseEntity<BaseResponse> coreSettingSectionUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingSection(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @DeleteMapping(value = "/section/delete")
	    public ResponseEntity<BaseResponse> deleteSection(@RequestParam Long id)  {
	        return new ResponseEntity<>(this.coreSettingsService.deleteCoreSettingSection(id), HttpStatus.OK);
	    }
	    
	    
	    @GetMapping(value = "/section/list")
	    public ResponseEntity<ItemResponse> sectionList()  {
	        return new ResponseEntity<>(coreSettingsService.getAllSectionList(), HttpStatus.OK);
	    }
    
        
    
        //++++++++++++++++++++++++++++++++++++++++++++++Group +++++++++++++++++++++++++++++++++++++++++++++
    
    
	    @PostMapping(value = "/group/create")
	    public ResponseEntity<BaseResponse> coreSettingGroupCreate(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.createCoreSettingGroup(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @PostMapping(value = "/group/update")
	    public ResponseEntity<BaseResponse> coreSettingGroupUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingGroup(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @DeleteMapping(value = "/group/delete")
	    public ResponseEntity<BaseResponse> deleteGroup(@RequestParam Long id)  {
	        return new ResponseEntity<>(this.coreSettingsService.deleteCoreSettingGroup(id), HttpStatus.OK);
	    }
	    
	    @GetMapping(value = "/group/list")
	    public ResponseEntity<ItemResponse> groupList(){
	        return new ResponseEntity<>(coreSettingsService.groupList(), HttpStatus.OK);
	    }
    
    
    
        //+++++++++++++++++++++++++++++++++++++++++Student Category++++++++++++++++++++++++++++++++++
    
	    @PostMapping(value = "/student-category/create")
	    public ResponseEntity<BaseResponse> coreSettingStudentCategoryCreate(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.createCoreSettingStudentCategory(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @PostMapping(value = "/student-category/update")
	    public ResponseEntity<BaseResponse> coreSettingStudentCategoryUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
	        return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingStudentCategory(coreSettingsRequest), HttpStatus.CREATED);
	    }
	    
	    @DeleteMapping(value = "/student-category/delete")
	    public ResponseEntity<BaseResponse> studentCategoryDelete(@RequestParam Long id)  {
	        return new ResponseEntity<>(coreSettingsService.deleteCoreSettingStudentCategory(id), HttpStatus.OK);
	    }
	    
	    @GetMapping(value = "/student-category/list")
	    public ResponseEntity<ItemResponse> studentCategoryList(){
	        return new ResponseEntity<>(coreSettingsService.studentCategoryList(), HttpStatus.OK);
	    }    
    
  
    
    
    	//+++++++++++++++++++++++++++++++++++++ Exam +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    
		@PostMapping(value = "/exam/create")
		public ResponseEntity<BaseResponse> coreSettingExamCreate(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
		return new ResponseEntity<>(this.coreSettingsService.createCoreSettingExam(coreSettingsRequest), HttpStatus.CREATED);
		}
		
		@PostMapping(value = "/exam/update")
		public ResponseEntity<BaseResponse> coreSettingExamUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
		return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingExam(coreSettingsRequest), HttpStatus.CREATED);
		}
		
		@DeleteMapping(value = "/exam/delete")
		public ResponseEntity<BaseResponse> examDelete(@RequestParam Long id)  {
		return new ResponseEntity<>(coreSettingsService.deleteCoreSettingExam(id), HttpStatus.OK);
		}
		
		@GetMapping(value = "/exam/list")
		public ResponseEntity<ItemResponse> examList(){
			ItemResponse itemResponse=coreSettingsService.examList();	
		return new ResponseEntity<>(itemResponse, HttpStatus.OK);
		}
		
		
		
		//+++++++++++++++++++++++++++++++++++ Subject +++++++++++++++++++++++++++++++++++++++++++
			    
			    
		@PostMapping(value = "/subject/create")
		public ResponseEntity<BaseResponse> coreSettingSubjectCreate(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
		return new ResponseEntity<>(this.coreSettingsService.createCoreSettingSubject(coreSettingsRequest), HttpStatus.CREATED);
		}
		
		@PostMapping(value = "/subject/update")
		public ResponseEntity<BaseResponse> coreSettingSubjectUpdate(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
		return new ResponseEntity<>(this.coreSettingsService.updateCoreSettingSubject(coreSettingsRequest), HttpStatus.CREATED);
		}
		
		@DeleteMapping(value = "/subject/delete")
		public ResponseEntity<BaseResponse> subjectDelete(@RequestParam Long id)  {
		return new ResponseEntity<>(coreSettingsService.deleteCoreSettingSubject(id), HttpStatus.OK);
		}
		
		@GetMapping(value = "/subject/list")
		public ResponseEntity<ItemResponse> subjectList(){
		return new ResponseEntity<>(coreSettingsService.subjectList(), HttpStatus.OK);
		}
		
	
		//+++++++++++++++++++++++++++++++++++ Period +++++++++++++++++++++++++++++++++++++++++++
		
	
		@SuppressWarnings("rawtypes")
		@GetMapping(value = "/global/period/list")
		public ResponseEntity<ItemResponse> globalPeriodList(){
		return new ResponseEntity<>(coreSettingsService.globalPeriodList(), HttpStatus.OK);
		}
		
		
		@PostMapping(value = "/period/create")
		public ResponseEntity<BaseResponse> coreSettingPeriodCreate(@RequestBody  @Valid  PeriodCreateRequest coreSettingsRequest)  {
		return new ResponseEntity<>(this.coreSettingsService.createCoreSettingPeriod(coreSettingsRequest), HttpStatus.CREATED);
		}
		
	
		@DeleteMapping(value = "/period/delete")
		public ResponseEntity<BaseResponse> periodDelete(@RequestParam Long id)  {
		return new ResponseEntity<>(coreSettingsService.deleteCoreSettingPeriod(id), HttpStatus.OK);
		}
		
		@SuppressWarnings("rawtypes")
		@GetMapping(value = "/period/list")
		public ResponseEntity<ItemResponse> periodList(){
		return new ResponseEntity<>(coreSettingsService.periodList(), HttpStatus.OK);
		}
		
		
		
		  //+++++++++++++++++++++++++++++++++++ Fee Head +++++++++++++++++++++++++++++++++++++++++++
		
		
			
			@PostMapping(value = "/fee-head/create")
			public ResponseEntity<BaseResponse> createFeeHead(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
			return new ResponseEntity<>(coreSettingsService.createFeeHead(coreSettingsRequest), HttpStatus.CREATED);
			}
			
			@PostMapping(value = "/fee-head/update")
			public ResponseEntity<BaseResponse> updatefeeHead(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
			return new ResponseEntity<>(coreSettingsService.updateFeeHead(coreSettingsRequest), HttpStatus.CREATED);
			}
			
		
			@DeleteMapping(value = "/fee-head/delete")
			public ResponseEntity<BaseResponse> deleteFeeHead(@RequestParam Long id)  {
			return new ResponseEntity<>(coreSettingsService.deleteFeeHead(id), HttpStatus.OK);
			}
			
			@SuppressWarnings("rawtypes")
			@GetMapping(value = "/fee-head/list")
			public ResponseEntity<ItemResponse> feeHeadList(){
			return new ResponseEntity<>(coreSettingsService.feeHeadList(), HttpStatus.OK);
			}
			
			
			
          //+++++++++++++++++++++++++++++++++++ Fee Sub Head +++++++++++++++++++++++++++++++++++++++++++
		
		
			
			@PostMapping(value = "/fee-sub-head/create")
			public ResponseEntity<BaseResponse> createFeeSubHead(@RequestBody  @Valid  CoreSettingsRequest coreSettingsRequest)  {
			return new ResponseEntity<>(coreSettingsService.createFeeSubHead(coreSettingsRequest), HttpStatus.CREATED);
			}
			
			@PostMapping(value = "/fee-sub-head/update")
			public ResponseEntity<BaseResponse> updateFeesubHead(@RequestBody @Valid  CoresettingUpdateRequest coresettingUpdateRequest)  {
			return new ResponseEntity<>(coreSettingsService.updateFeeSubHead(coresettingUpdateRequest), HttpStatus.CREATED);
			}
			
		
			@DeleteMapping(value = "/fee-sub-head/delete")
			public ResponseEntity<BaseResponse> deleteFeeSubHead(@RequestParam Long id)  {
			return new ResponseEntity<>(coreSettingsService.deleteFeeSubHead(id), HttpStatus.OK);
			}
			
			@SuppressWarnings("rawtypes")
			@GetMapping(value = "/fee-sub-head/list")
			public ResponseEntity<ItemResponse> feeSubHeadList(){
			return new ResponseEntity<>(coreSettingsService.feeSubHeadList(), HttpStatus.OK);
			}
			
			
			
			
           //+++++++++++++++++++++++++++++++++++ Fee Waiver +++++++++++++++++++++++++++++++++++++++++++
		
		
			
			@PostMapping(value = "/fee-waiver/create")
			public ResponseEntity<BaseResponse> createFeeWaiver(@RequestBody @Valid CoreSettingsRequest coreSettingsRequest)  {
			return new ResponseEntity<>(coreSettingsService.createFeeWaiver(coreSettingsRequest), HttpStatus.CREATED);
			}
			
			@PostMapping(value = "/fee-waiver/update")
			public ResponseEntity<BaseResponse> updateFeeWaiver(@RequestBody @Valid  CoresettingUpdateRequest coreSettingsRequest)  {
			return new ResponseEntity<>(coreSettingsService.updateFeeWaiver(coreSettingsRequest), HttpStatus.CREATED);
			}
			
		
			@DeleteMapping(value = "/fee-waiver/delete")
			public ResponseEntity<BaseResponse> deleteFeeWaiver(@RequestParam Long id)  {
			return new ResponseEntity<>(coreSettingsService.deleteFeeWaiver(id), HttpStatus.OK);
			}
			
			@SuppressWarnings("rawtypes")
			@GetMapping(value = "/fee-waiver/list")
			public ResponseEntity<ItemResponse> feeWaiverList(){
			return new ResponseEntity<>(coreSettingsService.feeWaiverList(), HttpStatus.OK);
			}


}
