package com.openspace.education.initialsetup.model.response;

public class DefaultSignatureResponse {

	private Long signatureId;

	private String signatureTitle;

	private Integer usedId;

	private String usedName;

	private String signImgName;

	private Integer signStatus;

	private Integer instituteId;

	public Long getSignatureId() {
		return signatureId;
	}

	public void setSignatureId(Long signatureId) {
		this.signatureId = signatureId;
	}

	public String getSignatureTitle() {
		return signatureTitle;
	}

	public void setSignatureTitle(String signatureTitle) {
		this.signatureTitle = signatureTitle;
	}

	public Integer getUsedId() {
		return usedId;
	}

	public void setUsedId(Integer usedId) {
		this.usedId = usedId;
	}

	public String getUsedName() {
		return usedName;
	}

	public void setUsedName(String usedName) {
		this.usedName = usedName;
	}

	public String getSignImgName() {
		return signImgName;
	}

	public void setSignImgName(String signImgName) {
		this.signImgName = signImgName;
	}

	public Integer getSignStatus() {
		return signStatus;
	}

	public void setSignStatus(Integer signStatus) {
		this.signStatus = signStatus;
	}

	public Integer getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Integer instituteId) {
		this.instituteId = instituteId;
	}



}
