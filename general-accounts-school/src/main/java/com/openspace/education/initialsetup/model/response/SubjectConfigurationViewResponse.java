package com.openspace.education.initialsetup.model.response;


public class SubjectConfigurationViewResponse {
	
    private Long subjectConfigurationId;
    
    private Integer subjectSerial;
    
    private Integer subjectStatus;

    private Long classId;
    
    private String className;
    
    private Long groupId;
    
    private String groupName;
    
    private Long subjectId;
    
    private String subjectName;
    
    private int subjectMergeId;
    

	public Long getSubjectConfigurationId() {
		return subjectConfigurationId;
	}

	public void setSubjectConfigurationId(Long subjectConfigurationId) {
		this.subjectConfigurationId = subjectConfigurationId;
	}

	public Integer getSubjectSerial() {
		return subjectSerial;
	}

	public void setSubjectSerial(Integer subjectSerial) {
		this.subjectSerial = subjectSerial;
	}

	public Integer getSubjectStatus() {
		return subjectStatus;
	}

	public void setSubjectStatus(Integer subjectStatus) {
		this.subjectStatus = subjectStatus;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getSubjectMergeId() {
		return subjectMergeId;
	}

	public void setSubjectMergeId(int subjectMergeId) {
		this.subjectMergeId = subjectMergeId;
	}
    
    
    

}
