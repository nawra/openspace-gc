package com.openspace.education.initialsetup.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GroupConfigurationCreateRequest {

	@NotNull(message = "classId can not be null")
	private Long classId;

	@NotNull
	@Size(min = 1)
	private List<Long> groupIds;



	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public List<Long> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<Long> groupIds) {
		this.groupIds = groupIds;
	}

	

	
}
