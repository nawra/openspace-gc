package com.openspace.education.initialsetup.model.dto;

public class DefaultSignatureDto {

	private String studentIdCardSignTitle;
	private String studentIdCardSignName;
	private int studentIdCardSignStatus;

	private String hrIdCardSignTitle;
	private String hrIdCardSignName;
	private int hrIdCardSignStatus;

	private String marksheetRightSignTitle;
	private String marksheetRightSignName;
	private int marksheetRightSignStatus;

	private String marksheetMiddleSignTitle;
	private String marksheetMiddleSignName;
	private int marksheetMiddleSignStatus;

	private String marksheetLeftSignTitle;
	private String marksheetLeftSignName;
	private int marksheetLeftSignStatus;

	private String admitCardLeftSignTitle;
	private String admitCardLeftSignImgName;
	private int admitCardLeftSignStatus;

	private String admitCardRightSignTitle;
	private String admitCardRightSignImgName;
	private int admitCardRightSignStatus;

	private String studentFeeReceiptBottomSignTitle;
	private String studentFeeReceiptBottomSignName;
	private int studentFeeReceiptBottomSignStatus;

	private String testimonialRightSignTitle;
	private String testimonialRightSignName;
	private int testimonialRightSignStatus;

	private String testimonialLeftSignTitle;
	private String testimonialLeftSignName;
	private int testimonialLeftSignStatus;

	private String tcRightSignTitle;
	private String tcRightSignName;
	private int tcRightSignStatus;

	private String tcLeftSignTitle;
	private String tcLeftSignName;
	private int tcLeftSignStatus;
	
	
	private String onlineAdmissioinSignName;
	private String onlineAdmissioinTitleName;
	private int onlineAdmissioinSignStatus;

	private String ctMarksheetMiddleSignTitle;
	private String ctMarksheetMiddleSignName;
	private int ctMarksheetMiddleSignStatus;

	private String ctMarksheetRightSignTitle;
	private String ctMarksheetRightSignName;
	private int ctMarksheetRightSignStatus;

	public String getStudentIdCardSignTitle() {
		return studentIdCardSignTitle;
	}

	public void setStudentIdCardSignTitle(String studentIdCardSignTitle) {
		this.studentIdCardSignTitle = studentIdCardSignTitle;
	}

	public String getStudentIdCardSignName() {
		return studentIdCardSignName;
	}

	public void setStudentIdCardSignName(String studentIdCardSignName) {
		this.studentIdCardSignName = studentIdCardSignName;
	}

	public int getStudentIdCardSignStatus() {
		return studentIdCardSignStatus;
	}

	public void setStudentIdCardSignStatus(int studentIdCardSignStatus) {
		this.studentIdCardSignStatus = studentIdCardSignStatus;
	}

	public String getHrIdCardSignTitle() {
		return hrIdCardSignTitle;
	}

	public void setHrIdCardSignTitle(String hrIdCardSignTitle) {
		this.hrIdCardSignTitle = hrIdCardSignTitle;
	}

	public String getHrIdCardSignName() {
		return hrIdCardSignName;
	}

	public void setHrIdCardSignName(String hrIdCardSignName) {
		this.hrIdCardSignName = hrIdCardSignName;
	}

	public int getHrIdCardSignStatus() {
		return hrIdCardSignStatus;
	}

	public void setHrIdCardSignStatus(int hrIdCardSignStatus) {
		this.hrIdCardSignStatus = hrIdCardSignStatus;
	}

	public String getMarksheetRightSignTitle() {
		return marksheetRightSignTitle;
	}

	public void setMarksheetRightSignTitle(String marksheetRightSignTitle) {
		this.marksheetRightSignTitle = marksheetRightSignTitle;
	}

	public String getMarksheetRightSignName() {
		return marksheetRightSignName;
	}

	public void setMarksheetRightSignName(String marksheetRightSignName) {
		this.marksheetRightSignName = marksheetRightSignName;
	}

	public int getMarksheetRightSignStatus() {
		return marksheetRightSignStatus;
	}

	public void setMarksheetRightSignStatus(int marksheetRightSignStatus) {
		this.marksheetRightSignStatus = marksheetRightSignStatus;
	}

	public String getMarksheetMiddleSignTitle() {
		return marksheetMiddleSignTitle;
	}

	public void setMarksheetMiddleSignTitle(String marksheetMiddleSignTitle) {
		this.marksheetMiddleSignTitle = marksheetMiddleSignTitle;
	}

	public String getMarksheetMiddleSignName() {
		return marksheetMiddleSignName;
	}

	public void setMarksheetMiddleSignName(String marksheetMiddleSignName) {
		this.marksheetMiddleSignName = marksheetMiddleSignName;
	}

	public int getMarksheetMiddleSignStatus() {
		return marksheetMiddleSignStatus;
	}

	public void setMarksheetMiddleSignStatus(int marksheetMiddleSignStatus) {
		this.marksheetMiddleSignStatus = marksheetMiddleSignStatus;
	}

	public String getMarksheetLeftSignTitle() {
		return marksheetLeftSignTitle;
	}

	public void setMarksheetLeftSignTitle(String marksheetLeftSignTitle) {
		this.marksheetLeftSignTitle = marksheetLeftSignTitle;
	}

	public String getMarksheetLeftSignName() {
		return marksheetLeftSignName;
	}

	public void setMarksheetLeftSignName(String marksheetLeftSignName) {
		this.marksheetLeftSignName = marksheetLeftSignName;
	}

	public int getMarksheetLeftSignStatus() {
		return marksheetLeftSignStatus;
	}

	public void setMarksheetLeftSignStatus(int marksheetLeftSignStatus) {
		this.marksheetLeftSignStatus = marksheetLeftSignStatus;
	}

	public String getAdmitCardLeftSignTitle() {
		return admitCardLeftSignTitle;
	}

	public void setAdmitCardLeftSignTitle(String admitCardLeftSignTitle) {
		this.admitCardLeftSignTitle = admitCardLeftSignTitle;
	}

	public String getAdmitCardLeftSignImgName() {
		return admitCardLeftSignImgName;
	}

	public void setAdmitCardLeftSignImgName(String admitCardLeftSignImgName) {
		this.admitCardLeftSignImgName = admitCardLeftSignImgName;
	}

	public int getAdmitCardLeftSignStatus() {
		return admitCardLeftSignStatus;
	}

	public void setAdmitCardLeftSignStatus(int admitCardLeftSignStatus) {
		this.admitCardLeftSignStatus = admitCardLeftSignStatus;
	}

	public String getAdmitCardRightSignTitle() {
		return admitCardRightSignTitle;
	}

	public void setAdmitCardRightSignTitle(String admitCardRightSignTitle) {
		this.admitCardRightSignTitle = admitCardRightSignTitle;
	}

	public String getAdmitCardRightSignImgName() {
		return admitCardRightSignImgName;
	}

	public void setAdmitCardRightSignImgName(String admitCardRightSignImgName) {
		this.admitCardRightSignImgName = admitCardRightSignImgName;
	}

	public int getAdmitCardRightSignStatus() {
		return admitCardRightSignStatus;
	}

	public void setAdmitCardRightSignStatus(int admitCardRightSignStatus) {
		this.admitCardRightSignStatus = admitCardRightSignStatus;
	}

	public String getStudentFeeReceiptBottomSignTitle() {
		return studentFeeReceiptBottomSignTitle;
	}

	public void setStudentFeeReceiptBottomSignTitle(String studentFeeReceiptBottomSignTitle) {
		this.studentFeeReceiptBottomSignTitle = studentFeeReceiptBottomSignTitle;
	}

	public String getStudentFeeReceiptBottomSignName() {
		return studentFeeReceiptBottomSignName;
	}

	public void setStudentFeeReceiptBottomSignName(String studentFeeReceiptBottomSignName) {
		this.studentFeeReceiptBottomSignName = studentFeeReceiptBottomSignName;
	}

	public int getStudentFeeReceiptBottomSignStatus() {
		return studentFeeReceiptBottomSignStatus;
	}

	public void setStudentFeeReceiptBottomSignStatus(int studentFeeReceiptBottomSignStatus) {
		this.studentFeeReceiptBottomSignStatus = studentFeeReceiptBottomSignStatus;
	}

	public String getTestimonialRightSignTitle() {
		return testimonialRightSignTitle;
	}

	public void setTestimonialRightSignTitle(String testimonialRightSignTitle) {
		this.testimonialRightSignTitle = testimonialRightSignTitle;
	}

	public String getTestimonialRightSignName() {
		return testimonialRightSignName;
	}

	public void setTestimonialRightSignName(String testimonialRightSignName) {
		this.testimonialRightSignName = testimonialRightSignName;
	}

	public int getTestimonialRightSignStatus() {
		return testimonialRightSignStatus;
	}

	public void setTestimonialRightSignStatus(int testimonialRightSignStatus) {
		this.testimonialRightSignStatus = testimonialRightSignStatus;
	}

	public String getTestimonialLeftSignTitle() {
		return testimonialLeftSignTitle;
	}

	public void setTestimonialLeftSignTitle(String testimonialLeftSignTitle) {
		this.testimonialLeftSignTitle = testimonialLeftSignTitle;
	}

	public String getTestimonialLeftSignName() {
		return testimonialLeftSignName;
	}

	public void setTestimonialLeftSignName(String testimonialLeftSignName) {
		this.testimonialLeftSignName = testimonialLeftSignName;
	}

	public int getTestimonialLeftSignStatus() {
		return testimonialLeftSignStatus;
	}

	public void setTestimonialLeftSignStatus(int testimonialLeftSignStatus) {
		this.testimonialLeftSignStatus = testimonialLeftSignStatus;
	}

	public String getTcRightSignTitle() {
		return tcRightSignTitle;
	}

	public void setTcRightSignTitle(String tcRightSignTitle) {
		this.tcRightSignTitle = tcRightSignTitle;
	}

	public String getTcRightSignName() {
		return tcRightSignName;
	}

	public void setTcRightSignName(String tcRightSignName) {
		this.tcRightSignName = tcRightSignName;
	}

	public int getTcRightSignStatus() {
		return tcRightSignStatus;
	}

	public void setTcRightSignStatus(int tcRightSignStatus) {
		this.tcRightSignStatus = tcRightSignStatus;
	}

	public String getTcLeftSignTitle() {
		return tcLeftSignTitle;
	}

	public void setTcLeftSignTitle(String tcLeftSignTitle) {
		this.tcLeftSignTitle = tcLeftSignTitle;
	}

	public String getTcLeftSignName() {
		return tcLeftSignName;
	}

	public void setTcLeftSignName(String tcLeftSignName) {
		this.tcLeftSignName = tcLeftSignName;
	}

	public int getTcLeftSignStatus() {
		return tcLeftSignStatus;
	}

	public void setTcLeftSignStatus(int tcLeftSignStatus) {
		this.tcLeftSignStatus = tcLeftSignStatus;
	}

	public String getCtMarksheetMiddleSignTitle() {
		return ctMarksheetMiddleSignTitle;
	}

	public void setCtMarksheetMiddleSignTitle(String ctMarksheetMiddleSignTitle) {
		this.ctMarksheetMiddleSignTitle = ctMarksheetMiddleSignTitle;
	}

	public String getCtMarksheetMiddleSignName() {
		return ctMarksheetMiddleSignName;
	}

	public void setCtMarksheetMiddleSignName(String ctMarksheetMiddleSignName) {
		this.ctMarksheetMiddleSignName = ctMarksheetMiddleSignName;
	}

	public int getCtMarksheetMiddleSignStatus() {
		return ctMarksheetMiddleSignStatus;
	}

	public void setCtMarksheetMiddleSignStatus(int ctMarksheetMiddleSignStatus) {
		this.ctMarksheetMiddleSignStatus = ctMarksheetMiddleSignStatus;
	}

	public String getCtMarksheetRightSignTitle() {
		return ctMarksheetRightSignTitle;
	}

	public void setCtMarksheetRightSignTitle(String ctMarksheetRightSignTitle) {
		this.ctMarksheetRightSignTitle = ctMarksheetRightSignTitle;
	}

	public String getCtMarksheetRightSignName() {
		return ctMarksheetRightSignName;
	}

	public void setCtMarksheetRightSignName(String ctMarksheetRightSignName) {
		this.ctMarksheetRightSignName = ctMarksheetRightSignName;
	}

	public int getCtMarksheetRightSignStatus() {
		return ctMarksheetRightSignStatus;
	}

	public void setCtMarksheetRightSignStatus(int ctMarksheetRightSignStatus) {
		this.ctMarksheetRightSignStatus = ctMarksheetRightSignStatus;
	}

	public String getOnlineAdmissioinSignName() {
		return onlineAdmissioinSignName;
	}

	public String getOnlineAdmissioinTitleName() {
		return onlineAdmissioinTitleName;
	}

	public int getOnlineAdmissioinSignStatus() {
		return onlineAdmissioinSignStatus;
	}

	public void setOnlineAdmissioinSignName(String onlineAdmissioinSignName) {
		this.onlineAdmissioinSignName = onlineAdmissioinSignName;
	}

	public void setOnlineAdmissioinTitleName(String onlineAdmissioinTitleName) {
		this.onlineAdmissioinTitleName = onlineAdmissioinTitleName;
	}

	public void setOnlineAdmissioinSignStatus(int onlineAdmissioinSignStatus) {
		this.onlineAdmissioinSignStatus = onlineAdmissioinSignStatus;
	}

}