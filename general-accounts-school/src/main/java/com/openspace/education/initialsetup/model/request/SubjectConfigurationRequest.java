package com.openspace.education.initialsetup.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubjectConfigurationRequest {

	
	@NotNull
	private Long classId;
	
	@NotNull
	private Long groupId;
	
	@NotNull
	@Size(min = 1)
	private List<Long> subjectIds;

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<Long> getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(List<Long> subjectIds) {
		this.subjectIds = subjectIds;
	}
	
	
	
}
