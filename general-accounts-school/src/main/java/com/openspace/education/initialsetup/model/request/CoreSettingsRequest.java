package com.openspace.education.initialsetup.model.request;

import org.hibernate.validator.constraints.NotBlank;

public class CoreSettingsRequest {


	@NotBlank
	private String name;

	private int status = 1;

	private int serial = 0;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

}
