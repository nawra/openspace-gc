package com.openspace.education.initialsetup.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingStudentCategoryRepository extends JpaRepository<CoreSettingStudentCategory, Long>{
	
    public CoreSettingStudentCategory findByNameAndInstitute(String name,Institute institute);
	
    public CoreSettingStudentCategory findByIdAndInstitute(Long id,Institute institute);
	
	public List<CoreSettingStudentCategory> findByInstituteOrderBySerialAsc(Institute institute);

}
