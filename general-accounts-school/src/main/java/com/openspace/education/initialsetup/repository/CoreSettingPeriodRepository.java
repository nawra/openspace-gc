package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingPeriod;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingPeriodRepository extends JpaRepository<CoreSettingPeriod, Long>{
	
    public CoreSettingPeriod findByNameAndInstitute_InstituteId(String periodName,Long instituteId);
	
	public List<CoreSettingPeriod> findByInstituteOrderBySerial(Institute institute);

	public CoreSettingPeriod findByIdAndInstitute(Long id, Institute institute);

}
