package com.openspace.education.initialsetup.model.response;

import java.awt.image.BufferedImage;
import java.util.Date;

public class StudentIdCardResponse {

	private String customStudentId;

	private String studentName;

	private String fatherName;

	private String motherName;

	private int studentRoll;

	private Integer academicYear;

	private String className;

	private String shiftName;

	private String sectionName;

	private String groupName;

	private String guardianMobile;

	private String bloodGroup;

	private String studentImageName;

	private String studentSession;
	
	private String dateofbirth;
	
	private String fatherImageName;
	
	private String motherImageName;

	private BufferedImage image;

	public String getCustomStudentId() {
		return customStudentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public int getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(int studentRoll) {
		this.studentRoll = studentRoll;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getStudentImageName() {
		return studentImageName;
	}

	public void setStudentImageName(String studentImageName) {
		this.studentImageName = studentImageName;
	}

	public String getStudentSession() {
		return studentSession;
	}

	public void setStudentSession(String studentSession) {
		this.studentSession = studentSession;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getDateofbirth() {
		return dateofbirth;
	}

	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}

	public String getFatherImageName() {
		return fatherImageName;
	}

	public String getMotherImageName() {
		return motherImageName;
	}

	public void setFatherImageName(String fatherImageName) {
		this.fatherImageName = fatherImageName;
	}

	public void setMotherImageName(String motherImageName) {
		this.motherImageName = motherImageName;
	}

	
}
