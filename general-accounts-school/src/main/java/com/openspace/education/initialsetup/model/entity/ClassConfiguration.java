package com.openspace.education.initialsetup.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "core_setting_class_configuration", uniqueConstraints = @UniqueConstraint(columnNames = { "class_id", "shift_id",
		"section_id", "institute_id" }))
public class ClassConfiguration implements Serializable{

	private static final long serialVersionUID = 3857016136942025553L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "status")
	private int status = 1;

	@Column(name = "serial")
	private int serial = 0;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private CoreSettingClass classInfo;

	@ManyToOne
	@JoinColumn(name = "shift_id", nullable = false)
	private CoreSettingShift shiftInfo;

	@ManyToOne
	@JoinColumn(name = "section_id", nullable = false)
	private CoreSettingSection sectionInfo;

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public CoreSettingClass getClassInfo() {
		return classInfo;
	}

	public void setClassInfo(CoreSettingClass classInfo) {
		this.classInfo = classInfo;
	}

	public CoreSettingShift getShiftInfo() {
		return shiftInfo;
	}

	public void setShiftInfo(CoreSettingShift shiftInfo) {
		this.shiftInfo = shiftInfo;
	}

	public CoreSettingSection getSectionInfo() {
		return sectionInfo;
	}

	public void setSectionInfo(CoreSettingSection sectionInfo) {
		this.sectionInfo = sectionInfo;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	
}
