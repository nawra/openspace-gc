package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.GroupConfiguration;
import com.openspace.education.institute.model.entity.Institute;

public interface GroupConfigurationRepository extends JpaRepository<GroupConfiguration, Long>{

	public List<GroupConfiguration> findByClassInfo_IdAndGroupInfo_IdInAndInstitute_InstituteId(Long classId,List<Long> groupIds, Long instituteId);
	public List<GroupConfiguration> findByClassInfo_IdAndInstitute(Long classId,Institute institute);
	
	public List<GroupConfiguration> findByClassInfo_IdAndInstituteInstituteId(Long classId,Long instituteId);

	public List<GroupConfiguration> findByIdInAndInstitute(List<Long> groupIds, Institute institute);
 
	public List<GroupConfiguration> findByInstituteOrderBySerial(Institute institute);
}
