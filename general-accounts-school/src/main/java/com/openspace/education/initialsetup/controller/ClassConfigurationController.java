package com.openspace.education.initialsetup.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.initialsetup.model.request.ClassConfigurationRequest;
import com.openspace.education.initialsetup.model.request.GroupConfigurationSerialUpdate;
import com.openspace.education.initialsetup.service.ClassConfigurationService;

@Controller
@RequestMapping(value = "/class/configuration")
public class ClassConfigurationController {
	
	
	@Autowired
	public ClassConfigurationService classConfigurationService;
	
	
	@PostMapping(value = "/create")
	public ResponseEntity<BaseResponse> classConfigurationCreate(@RequestBody @Valid ClassConfigurationRequest classConfigurationRequest)  {
	return new ResponseEntity<>(classConfigurationService.createClassConfiguration(classConfigurationRequest),HttpStatus.CREATED);
	}
	
	 @PostMapping(value = "/serial/update") 
	 public ResponseEntity<BaseResponse> groupConfigurationUpdate(@RequestBody @Valid List<ClassConfigurationRequest> requests) { 
	  BaseResponse baseResponse=classConfigurationService.updateClassConfigurationSerial(requests);
	  return new ResponseEntity<>(baseResponse,HttpStatus.CREATED); 
	  }	 
	 
	

	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> classConfigurationDelete(@RequestParam Long id)  {
	return new ResponseEntity<>(classConfigurationService.deleteClassConfiguration(id),HttpStatus.OK);
	}
	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> classConfigurationList(){
	return new ResponseEntity<>(classConfigurationService.getAllClassConfigurationList(),HttpStatus.OK);
	}
	

}
