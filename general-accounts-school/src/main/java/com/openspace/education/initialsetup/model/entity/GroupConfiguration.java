package com.openspace.education.initialsetup.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "core_setting_group_configuration", uniqueConstraints = @UniqueConstraint(columnNames = { "class_id",
		"group_id", "institute_id" }))
public class GroupConfiguration implements Serializable {

	private static final long serialVersionUID = 3857016136942025553L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "status")
	private int status = 1;

	@Column(name = "serial")
	private int serial = 0;

	@Column(name = "application_fee")
	private Integer applicationFee;

	@Column(name = "service_charge")
	private Integer serviceCharge;

	@Column(name = "total_fee")
	private Integer totalFee;

	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private CoreSettingClass classInfo;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private CoreSettingGroup groupInfo;

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}

	public CoreSettingClass getClassInfo() {
		return classInfo;
	}

	public void setClassInfo(CoreSettingClass classInfo) {
		this.classInfo = classInfo;
	}

	public CoreSettingGroup getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(CoreSettingGroup groupInfo) {
		this.groupInfo = groupInfo;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Integer getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(Integer applicationFee) {
		this.applicationFee = applicationFee;
	}

	public Integer getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(Integer serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public Integer getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Integer totalFee) {
		this.totalFee = totalFee;
	}

}
