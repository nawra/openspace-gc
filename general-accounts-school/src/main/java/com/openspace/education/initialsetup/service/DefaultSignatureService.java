package com.openspace.education.initialsetup.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.CommonInfoUtils;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.initialsetup.model.dto.DefaultSignatureDto;
import com.openspace.education.initialsetup.model.entity.DefaultSignature;
import com.openspace.education.initialsetup.model.request.DefaultSignatureRequest;
import com.openspace.education.initialsetup.model.response.DefaultSignatureResponse;
import com.openspace.education.initialsetup.model.response.SignatureImageViewResponse;
import com.openspace.education.initialsetup.repository.DefaultSignatureRepository;
import com.openspace.education.institute.model.entity.Institute;

@Service
public class DefaultSignatureService {

	public Logger logger = LoggerFactory.getLogger(DefaultSignatureService.class);

	@Autowired
	private DefaultSignatureRepository defaultSignatureRepository;
	@Autowired
	private ImageStorageService imageStorageService;
	@Autowired
	private CommonInfoUtils commonInfoUtils;
	
	 	@SuppressWarnings({ "unchecked", "rawtypes" })
		@Transactional
	    public ItemResponse createDefaultSign(DefaultSignatureRequest request) {

	 		ItemResponse baseResponse = new ItemResponse();

	        Institute institute = UserInfoUtils.getLoggedInInstitute();
	        
	        DefaultSignature defaultSing = defaultSignatureRepository.findByInstitute_InstituteIdAndUsedId(institute.getInstituteId(),request.getUsedId());
	        
	        if(defaultSing != null) {
	        	defaultSignatureRepository.delete(defaultSing);
	        	defaultSignatureRepository.flush();
	        	imageStorageService.deleteFile(FileFolder.DEFAULT.name(), defaultSing.getSignImgName());
	        }
	        
	        
	        DefaultSignature defaultSignature = new DefaultSignature();
	        
	        defaultSignature.setInstitute(institute);
	        defaultSignature.setSignatureTitle(request.getSignatureTitle());
	        defaultSignature.setSignStatus(1);
	        defaultSignature.setUsedId(request.getUsedId());
	        defaultSignature.setUsedName(request.getUsedName());
	        String imageName=""+institute.getInstituteId()+"_"+request.getUsedId()+"_signature.png";
	        defaultSignature.setSignImgName(imageName);
	        defaultSignature.setSignStatus(request.getSignStatus());


	        defaultSignature=defaultSignatureRepository.save(defaultSignature);
	        
	        baseResponse.setItem(defaultSignature.getSignatureId());
	        baseResponse.setMessageType(1);
	        baseResponse.setMessage("Save Successfull");
	        return baseResponse;
	    }
	 	
	 	
	    @SuppressWarnings({ "rawtypes", "unchecked" })
		public ItemResponse defaultSignImageList() {

	 		ItemResponse itemResponse = new ItemResponse();

	        Institute institute = UserInfoUtils.getLoggedInInstitute();
	        
	        List<DefaultSignature> defaultSings = defaultSignatureRepository.findByInstitute(institute);
	        
	        List<SignatureImageViewResponse> viewResponses=new ArrayList<>();
	        
	        for(DefaultSignature ds:defaultSings) {
	        	
	        	SignatureImageViewResponse view =new SignatureImageViewResponse();
	        	
	        	view.setSignatureId(ds.getSignatureId());
	        	view.setSignatureTitle(ds.getSignatureTitle());
	        	view.setSignStatus(ds.getSignStatus());
	        	view.setSignStrStatus(ds.getSignStatus()==1? "Yes" : "No");
	        	view.setUsedId(ds.getUsedId());
	        	view.setUsedName(ds.getUsedName());
	        	
	        	try {
					String imageBase64=new String(imageStorageService.fetchImageInBase64Encode(FileFolder.DEFAULT.name(), ds.getSignImgName()));
					view.setSignImage(imageBase64); 
	        	} catch (IOException e) {
	        		view.setSignImage("");
				}
	        	
	        	viewResponses.add(view);
	        }
	        
	        itemResponse.setItem(viewResponses);
	        itemResponse.setMessageType(1);
	        itemResponse.setMessage("OK");
	        return itemResponse;
	    }
	 	
	 	@Transactional
	    public ItemResponse updateDefaultSignImage(MultipartFile file,Long signatureId) {

	 		ItemResponse baseResponse = new ItemResponse();

	        Institute institute = UserInfoUtils.getLoggedInInstitute();
	        
	        String fileType = file.getContentType().substring(6, file.getContentType().length());
	        
	        DefaultSignature defaultSing = defaultSignatureRepository.findBySignatureIdAndInstitute(signatureId, institute);
	        imageStorageService.deleteFile(FileFolder.DEFAULT.name(), defaultSing.getSignImgName());
	        
	        String imageName=""+institute.getInstituteId()+"_"+defaultSing.getUsedId()+"_signature."+fileType;
	        defaultSing.setSignImgName(imageName);
	        
	        

	        imageStorageService.signatureImageUpload(FileFolder.DEFAULT.name(), imageName,fileType, file);
	        
	        baseResponse.setMessageType(1);
	        baseResponse.setMessage(defaultSing.getSignImgName()+" Successfully Saved.");
	        return baseResponse;
	    }
	 	
	 	
	 	@Transactional
	    public BaseResponse deleteDefaultSignImage(Long signatureId) {

	 		BaseResponse baseResponse = new BaseResponse();

	        Institute institute = UserInfoUtils.getLoggedInInstitute();
	        
	        DefaultSignature defaultSing = defaultSignatureRepository.findBySignatureIdAndInstitute(signatureId, institute);
	        
	        if(defaultSing!=null) {
	        
	        imageStorageService.deleteFile(FileFolder.DEFAULT.name(), defaultSing.getSignImgName());
	        
	        defaultSignatureRepository.delete(defaultSing);
	        
	        baseResponse.setMessageType(1);
	        baseResponse.setMessage("Signature Successfully Deleted.");
	        return baseResponse;
	      
	        }

	        baseResponse.setMessageType(0);
	        baseResponse.setMessage("No Signature Deleted.");
	        return baseResponse;
	    }
		
	 	public ItemResponse<List<DefaultSignatureResponse>> defaultSignatureList() {
			
			ItemResponse<List<DefaultSignatureResponse>> itemResponse = new ItemResponse<List<DefaultSignatureResponse>>();
			
			 Institute institute = UserInfoUtils.getLoggedInInstitute();
			 
			List<DefaultSignature> defaultSigns = defaultSignatureRepository.findByInstitute(institute);
			
			List<DefaultSignatureResponse> responses = new ArrayList<>();						

			for (DefaultSignature ds : defaultSigns) {
				
				DefaultSignatureResponse defaultSignatureResponse = new DefaultSignatureResponse();
				
				defaultSignatureResponse.setSignatureId(ds.getSignatureId());
				defaultSignatureResponse.setSignatureTitle(ds.getSignatureTitle());
				defaultSignatureResponse.setSignImgName(ds.getUsedName());
				defaultSignatureResponse.setSignStatus(ds.getSignStatus());
				defaultSignatureResponse.setUsedName(ds.getUsedName());
				defaultSignatureResponse.setUsedId(ds.getUsedId());
				
				responses.add(defaultSignatureResponse);
			}
						
			itemResponse.setItem(responses);
			itemResponse.setMessage("OK");
			itemResponse.setMessageType(1);
			return itemResponse;
			
			}
	 	
	 	
	 	public DefaultSignatureDto provideDefaultSignature(Institute institute) {

		List<DefaultSignature> defaultSigns = defaultSignatureRepository.findByInstitute(institute);

		if (defaultSigns.isEmpty()) {
			logger.info("Default Sign has not been found!");
			return new DefaultSignatureDto();
		}

		DefaultSignatureDto defaultSignatureDto = new DefaultSignatureDto();

		for (DefaultSignature defaultSignature : defaultSigns) {
			defaultSignatureDto = commonInfoUtils.mapDefaultSignDto(defaultSignature, defaultSignatureDto);
		}

		return defaultSignatureDto;
	}
	
}
