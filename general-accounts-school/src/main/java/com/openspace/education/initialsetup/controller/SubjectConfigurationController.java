package com.openspace.education.initialsetup.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.initialsetup.model.request.SubjectConfigurationRequest;
import com.openspace.education.initialsetup.model.request.SubjectConfigurationUpdateRequest;
import com.openspace.education.initialsetup.service.SubjectConfigurationService;

@Controller
@RequestMapping(value = "/subject/configuration")
public class SubjectConfigurationController {
	
	
	@Autowired
	public SubjectConfigurationService subjectConfigurationService;
	
	
	@PostMapping(value = "/create")
	public ResponseEntity<BaseResponse> subjectConfigurationCreate(@RequestBody @Valid SubjectConfigurationRequest subjectConfigurationRequest)  {
	return new ResponseEntity<>(subjectConfigurationService.saveSubjectConfiguration(subjectConfigurationRequest),HttpStatus.CREATED);
	}
	
	
	 @PostMapping(value = "/update") 
	 public ResponseEntity<BaseResponse> subjectConfigurationUpdate(@RequestBody @Valid List<SubjectConfigurationUpdateRequest> requests) { 
	 BaseResponse baseResponse=subjectConfigurationService.updateSubjectConfiguration(requests);
	 return new ResponseEntity<>(baseResponse,HttpStatus.CREATED); 
	 }	
	 
	 
	 @DeleteMapping(value = "/delete") 
	 public ResponseEntity<BaseResponse> subjectConfigurationDelete(@RequestParam Long subjectConfigurationId) { 
	 BaseResponse baseResponse=subjectConfigurationService.deleteSubjectConfiguration(subjectConfigurationId);
	 return new ResponseEntity<>(baseResponse,HttpStatus.CREATED); 
	 }
	  
	 
		
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/by/class-id/group-id")
	public ResponseEntity<ItemResponse> subjectConfigurationListByClasssIdAndGroupId(@RequestParam Long classId,@RequestParam Long groupId){
	return new ResponseEntity<>(subjectConfigurationService.subjectConfigurationListByClassIdAndGroupId(classId, groupId),HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/by/class-configuration-id/group-id")
	public ResponseEntity<ItemResponse> subjectConfigurationListByClassConfigurationIdAndGroupId(@RequestParam Long classConfigurationId,@RequestParam Long groupId){
	return new ResponseEntity<>(subjectConfigurationService.subjectConfigurationListByClassConfigurationIdAndGroupId(classConfigurationId, groupId),HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/choosable/list/by/class-configuration-id/group-id")
	public ResponseEntity<ItemResponse> choosableSubjectConfigurationListByClassConfigurationIdAndGroupId(@RequestParam Long classConfigurationId,@RequestParam Long groupId){
	return new ResponseEntity<>(subjectConfigurationService.choosableSubjectConfigurationListByClassConfigurationIdAndGroupId(classConfigurationId, groupId),HttpStatus.OK);
	}
	
	

}
