package com.openspace.education.initialsetup.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.GroupConfiguration;
import com.openspace.education.initialsetup.model.request.GroupConfigurationCreateRequest;
import com.openspace.education.initialsetup.model.request.GroupConfigurationSerialUpdate;
import com.openspace.education.initialsetup.model.response.GroupConfigurationResponse;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.GroupConfigurationRepository;
import com.openspace.education.institute.model.entity.Institute;

@Service
public class GroupConfigurationService {
	
	public Logger logger = LoggerFactory.getLogger(GroupConfigurationService.class);

	@Autowired
	private GroupConfigurationRepository groupConfigurationRepository;
	
	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;

	@Transactional
	public BaseResponse createGroupConfiguration(GroupConfigurationCreateRequest groupConfigurationRequest) {

		BaseResponse baseResponse = new BaseResponse();
		Long instituteId = UserInfoUtils.getLoggedInInstituteId();
		Institute institute = UserInfoUtils.getLoggedInInstitute();

		List<GroupConfiguration> groupConfigurations= groupConfigurationRepository
				.findByClassInfo_IdAndGroupInfo_IdInAndInstitute_InstituteId(groupConfigurationRequest.getClassId(),groupConfigurationRequest.getGroupIds(), instituteId);

			if(groupConfigurations.size()>0) {
		    baseResponse.setMessage("Duplicate Entry");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
		

		CoreSettingClass coreSettingClass = new CoreSettingClass();
		coreSettingClass.setId(groupConfigurationRequest.getClassId());
		
		List<GroupConfiguration> groupConfigurationList=new LinkedList<>();
		
		for(Long groupId:groupConfigurationRequest.getGroupIds()) {
			
		GroupConfiguration groupConfiguration = new GroupConfiguration();	
		
		CoreSettingGroup coreSettingGroup = new CoreSettingGroup();
		coreSettingGroup.setId(groupId);
		
		groupConfiguration.setClassInfo(coreSettingClass);
		groupConfiguration.setGroupInfo(coreSettingGroup);
		groupConfiguration.setSerial(0);
		groupConfiguration.setInstitute(institute);
		
		groupConfigurationList.add(groupConfiguration);
		
		}

		groupConfigurationRepository.save(groupConfigurationList);

		baseResponse.setMessage("Group Configuration Successfully Created");
		baseResponse.setMessageType(1);

		return baseResponse;

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse getAllGroupConfigurationList() {

		ItemResponse itemResponse = new ItemResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();

		List<GroupConfigurationResponse> groupConfigurationResponseList = new ArrayList<>();

		List<GroupConfiguration> groupConfigurationList = groupConfigurationRepository.findByInstituteOrderBySerial(institute);

		groupConfigurationList.forEach(gc -> {
		
			groupConfigurationResponseList.add(copyGroupConfigurationToGroupConfigurationResponse(gc));
		});

		itemResponse.setItem(groupConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse groupConfigurationListByClassConfigId(Long classConfigId) {

		ItemResponse itemResponse = new ItemResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		ClassConfiguration classConfiguration=classConfigurationRepository.findByIdAndInstitute(classConfigId, institute);
		
		if(classConfiguration==null) {
			itemResponse.setItem(null);
			itemResponse.setMessage("No Class Config Found");
			itemResponse.setMessageType(0);
			return itemResponse;	
		}

		List<GroupConfigurationResponse> groupConfigurationResponseList = new ArrayList<>();

		List<GroupConfiguration> groupConfigurationList = groupConfigurationRepository.findByClassInfo_IdAndInstitute(classConfiguration.getClassInfo().getId(),institute);

		groupConfigurationList.forEach(gc -> {
		
			groupConfigurationResponseList.add(copyGroupConfigurationToGroupConfigurationResponse(gc));
		});

		itemResponse.setItem(groupConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	//This public api for use online admission
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse groupConfigurationListByClassIdInstituteId(Long classId,Long instituteId) {

		
		ItemResponse itemResponse = new ItemResponse();
		
		List<GroupConfigurationResponse> groupConfigurationResponseList = new ArrayList<>();

		List<GroupConfiguration> groupConfigurationList = groupConfigurationRepository.findByClassInfo_IdAndInstituteInstituteId(classId,instituteId);

		groupConfigurationList.forEach(gc -> {
		
			groupConfigurationResponseList.add(copyGroupConfigurationToGroupConfigurationResponse(gc));
		});

		itemResponse.setItem(groupConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse groupConfigurationListByClassId(Long classId) {

		ItemResponse itemResponse = new ItemResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<GroupConfigurationResponse> groupConfigurationResponseList = new ArrayList<>();

		List<GroupConfiguration> groupConfigurationList = groupConfigurationRepository.findByClassInfo_IdAndInstitute(classId,institute);

		groupConfigurationList.forEach(gc -> {
		
			groupConfigurationResponseList.add(copyGroupConfigurationToGroupConfigurationResponse(gc));
		});

		itemResponse.setItem(groupConfigurationResponseList);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	private GroupConfigurationResponse copyGroupConfigurationToGroupConfigurationResponse(GroupConfiguration gc) {
		
		GroupConfigurationResponse groupConfigurationResponse = new GroupConfigurationResponse();
		
		groupConfigurationResponse.setClassId(gc.getClassInfo().getId());
		groupConfigurationResponse.setClassName(gc.getClassInfo().getName());
		groupConfigurationResponse.setGroupId(gc.getGroupInfo().getId());
		groupConfigurationResponse.setGroupName(gc.getGroupInfo().getName());
		groupConfigurationResponse.setSerial(gc.getSerial());
		groupConfigurationResponse.setApplicationFee(gc.getApplicationFee());
		groupConfigurationResponse.setStatus(gc.getStatus());
		groupConfigurationResponse.setId(gc.getId());
		groupConfigurationResponse.setClassGroupName(gc.getClassInfo().getName()+" ("+gc.getGroupInfo().getName()+")");
		
		return groupConfigurationResponse;
		
	}
	


	

	public BaseResponse deleteGroupConfiguration(Long id) {

		BaseResponse baseResponse = new BaseResponse();

		GroupConfiguration groupConfiguration = groupConfigurationRepository.findOne(id);

		if (groupConfiguration == null) {
			baseResponse.setMessage("Group Config not found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		try {

			groupConfigurationRepository.delete(groupConfiguration);

		} catch (DataIntegrityViolationException e) {
			logger.error(e.getMessage());
			baseResponse.setMessage("This Group Config is assigned in another configuration");
			baseResponse.setMessageType(0);
			return baseResponse;
		} catch (Exception e) {
			logger.error(e.getMessage());
			baseResponse.setMessage("Operation Failed");
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		baseResponse.setMessage("Group Config successfully deleted");
		baseResponse.setMessageType(1);

		return baseResponse;

	}
	
	
	@Transactional
	public BaseResponse updateGroupConfigurationSerial(List<GroupConfigurationSerialUpdate> requests) {

		BaseResponse baseResponse = new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Integer serviceCharge = institute.getOnLineAdmissionServiceCharge();
		
		if(serviceCharge ==null) {
			serviceCharge = 0;
		}
		
        List<Long> groupConfigIds=new LinkedList<>();
        requests.forEach(rq -> {
        	groupConfigIds.add(rq.getId());
        });
		
		List<GroupConfiguration> groupConfigurations= groupConfigurationRepository
				.findByIdInAndInstitute(groupConfigIds, institute);

			if(groupConfigurations.size()<1) {
		    baseResponse.setMessage("No List Found");
			baseResponse.setMessageType(0);
			return baseResponse;
			}
		


		
		for(GroupConfiguration config:groupConfigurations) {
			
			for(GroupConfigurationSerialUpdate rq: requests) {
								
				if(config.getId().equals(rq.getId())) {
					config.setSerial(rq.getSerial());
					config.setApplicationFee(rq.getApplicationFee());
					config.setServiceCharge(serviceCharge);
					if(rq.getApplicationFee() == null) {
						rq.setApplicationFee(0);
					}
					config.setTotalFee(rq.getApplicationFee()+serviceCharge);
					break;
				}
			}
			
		
		}


		baseResponse.setMessage("Group Configuration Serial Successfully Updated");
		baseResponse.setMessageType(1);

		return baseResponse;

	}


}
