package com.openspace.education.initialsetup.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.institute.model.entity.Institute;


@Entity
@Table(name="core_setting_subject_configuration", uniqueConstraints = @UniqueConstraint(columnNames = {"subject_id","class_id","group_id"}))
public class SubjectConfiguration {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="subjectconfig_id")
    private Long subjectConfigurationId;
    
    @Column(name="subject_serial")
    private Integer subjectSerial;
    
    @Column(name="subject_status") //compulsory=0, choosable=1, groupbased=2, uncountable=4
    private Integer subjectStatus;
    

    @ManyToOne 
    @JoinColumn(name="class_id")
    private CoreSettingClass classInfo;
    
    @ManyToOne
    @JoinColumn(name="group_id")
    private CoreSettingGroup groupInfo;
    
    @ManyToOne 
    @JoinColumn(name="subject_id")
    private CoreSettingSubject subjectInfo;
    
    @Column(name = "subject_merge_id")
    private int subjectMergeId;
    
    
    @ManyToOne
    @JoinColumn(name="institute_id")
    private Institute institute;
    
    


	public Long getSubjectConfigurationId() {
		return subjectConfigurationId;
	}


	public void setSubjectConfigurationId(Long subjectConfigurationId) {
		this.subjectConfigurationId = subjectConfigurationId;
	}


	public Integer getSubjectSerial() {
		return subjectSerial;
	}


	public void setSubjectSerial(Integer subjectSerial) {
		this.subjectSerial = subjectSerial;
	}


	public Integer getSubjectStatus() {
		return subjectStatus;
	}


	public void setSubjectStatus(Integer subjectStatus) {
		this.subjectStatus = subjectStatus;
	}


	public CoreSettingClass getClassInfo() {
		return classInfo;
	}


	public void setClassInfo(CoreSettingClass classInfo) {
		this.classInfo = classInfo;
	}


	public CoreSettingGroup getGroupInfo() {
		return groupInfo;
	}


	public void setGroupInfo(CoreSettingGroup groupInfo) {
		this.groupInfo = groupInfo;
	}


	public CoreSettingSubject getSubjectInfo() {
		return subjectInfo;
	}


	public void setSubjectInfo(CoreSettingSubject subjectInfo) {
		this.subjectInfo = subjectInfo;
	}


	public int getSubjectMergeId() {
		return subjectMergeId;
	}


	public void setSubjectMergeId(int subjectMergeId) {
		this.subjectMergeId = subjectMergeId;
	}


	public Institute getInstitute() {
		return institute;
	}


	public void setInstitute(Institute institute) {
		this.institute = institute;
	}
    
    
    

}
