package com.openspace.education.initialsetup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingAcademicYear;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.institute.model.entity.Institute;

public interface CoreSettingAcademicYearRepository extends JpaRepository<CoreSettingAcademicYear, Long>{
	
    public CoreSettingAcademicYear findByNameAndInstitute(String className,Institute institute);
	
	public List<CoreSettingAcademicYear> findByInstituteOrderBySerial(Institute institute);
	
	public List<CoreSettingAcademicYear> findByInstitute_InstituteIdInOrderBySerial(List<Long> instituteIds);
	
	public CoreSettingAcademicYear findByIdAndInstitute(Long id,Institute institute);

}
