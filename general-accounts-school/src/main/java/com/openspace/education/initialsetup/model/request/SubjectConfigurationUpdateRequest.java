package com.openspace.education.initialsetup.model.request;

import javax.validation.constraints.NotNull;

public class SubjectConfigurationUpdateRequest {
	

	@NotNull
    private Long subjectConfigurationId;

	@NotNull
    private Integer subjectSerial;
	
	@NotNull
    private Integer subjectStatus;
    
    private int subjectMergeId;
    
    

	public Long getSubjectConfigurationId() {
		return subjectConfigurationId;
	}

	public void setSubjectConfigurationId(Long subjectConfigurationId) {
		this.subjectConfigurationId = subjectConfigurationId;
	}

	public Integer getSubjectSerial() {
		return subjectSerial;
	}

	public void setSubjectSerial(Integer subjectSerial) {
		this.subjectSerial = subjectSerial;
	}

	public Integer getSubjectStatus() {
		return subjectStatus;
	}

	public void setSubjectStatus(Integer subjectStatus) {
		this.subjectStatus = subjectStatus;
	}

	public int getSubjectMergeId() {
		return subjectMergeId;
	}

	public void setSubjectMergeId(int subjectMergeId) {
		this.subjectMergeId = subjectMergeId;
	}
    
    

}
