package com.openspace.education.initialsetup.model.response;



public class ExamConfigurationViewResponse {
	
	private Long examConfigId;

    private int examSerial;

    private double percentage;

    private int meritProcessType ; 
    
    private String meritProcessTypeName;
    
    private Integer grandFinalProcessType;

    private Long classId;
    
    private String className;

    private Long examId;
    
    private String examName;
    
    private Integer examDefaultId;

	public Long getExamConfigId() {
		return examConfigId;
	}

	public void setExamConfigId(Long examConfigId) {
		this.examConfigId = examConfigId;
	}

	public int getExamSerial() {
		return examSerial;
	}

	public void setExamSerial(int examSerial) {
		this.examSerial = examSerial;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public int getMeritProcessType() {
		return meritProcessType;
	}

	public void setMeritProcessType(int meritProcessType) {
		this.meritProcessType = meritProcessType;
	}

	public Integer getGrandFinalProcessType() {
		return grandFinalProcessType;
	}

	public void setGrandFinalProcessType(Integer grandFinalProcessType) {
		this.grandFinalProcessType = grandFinalProcessType;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getMeritProcessTypeName() {
		return meritProcessTypeName;
	}

	public void setMeritProcessTypeName(String meritProcessTypeName) {
		this.meritProcessTypeName = meritProcessTypeName;
	}

	public Integer getExamDefaultId() {
		return examDefaultId;
	}

	public void setExamDefaultId(Integer examDefaultId) {
		this.examDefaultId = examDefaultId;
	}
    
    
    
    

}
