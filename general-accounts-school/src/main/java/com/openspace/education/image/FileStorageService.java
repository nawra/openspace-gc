package com.openspace.education.image;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.openspace.education.common.ApplicationUtils;


@Service
public class FileStorageService {
	
	
	public byte[] fetchFileInByte(String folderName, String fileName) throws Exception {

		String filePath = "";

		filePath = ApplicationUtils.getFilePath(folderName) + fileName;

		try {
			Path path = new File(filePath).toPath();

			byte[] file = Files.readAllBytes(path);

			return file;

		} catch (Exception e) {

			return new byte[0];
		}
	}

	
	public  String writeFileToPath(String folderName, byte[] content, String fileName) {

		String message = "";

		String fileFullPath = ApplicationUtils.getFilePath(folderName) + fileName;
		System.out.println("File Path :"+fileFullPath);

		File file = new File(fileFullPath);
		
		try {
			try (FileOutputStream fos = new FileOutputStream(file)) {
				fos.write(content);
				fos.flush();
			}

		} catch (FileNotFoundException e) {
			message = "No File found in path: " + fileFullPath;
			System.out.println(e);
		} catch (IOException e) {
			message = "No File found in path: " + fileFullPath;
			System.out.println(e);
		}
		return message;
	}
	
	

	
	public String deleteFile(String folderName, String fileName) {
        
    	String message = "";
 
        String filePath = "";
        
        filePath = ApplicationUtils.getFilePath(folderName) + fileName;
            	
        File fdelete = new File(filePath);
            
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                	message="File Successfully Deleted";
                } else {
                	message="File not Deleted";
                }
            }
                
      
        return message;
    }
	
	
	public Optional<String> provideFileExtension(String filename) {
		return Optional.ofNullable(filename).filter(f -> f.contains("."))
				.map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}

}
