/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.openspace.education.institute.model.entity.Institute;

/**
 *
 * @author riad
 */
public interface InstituteRepository extends JpaRepository<Institute, Long>{
    
    public Institute findByInstituteName(String instituteName);
    public Institute findByInstituteId(Long instituteId);
    
    public Institute findByEiinNo(String eiinNo);
    
    
    @Query(value = "select (case when max(institute_id) is not null then max(institute_id) else 0 end) from institute_info where institute_type=?1 ",nativeQuery=true)
    public Long findMaxInstituteId(Integer instituteType);
    
    public List<Institute> findByOnLineAdmissionStatusAndInstituteStatus(Integer admissionStatus,Integer instituteStatus);

    // native clause
    
    @Query(value = "SELECT i.institute_id,ifnull(count(si.identification_id),0) totalstudent FROM institute_info i, student_identification si " + 
    		" where i.institute_id=si.institute_id and i.academic_year=si.academic_year and si.student_status=1 and i.bill_cycle='Monthly' " + 
    		" group by i.institute_id",nativeQuery=true)
    public List<Object[]> searchMonthlyInstituteStudentCount();
    
    
    @Query(value = "SELECT i.institute_id,ifnull(count(si.identification_id),0) totalstudent FROM institute_info i, student_identification si " + 
    		" where i.institute_id=si.institute_id and i.academic_year=si.academic_year and si.student_status=1 and i.institute_id=?1",nativeQuery=true)
    public List<Object[]> searchMonthlyInstituteStudentCount(Long institueId);
    
    
    @Query(value = "select i.institute_id,i.institute_name,i.address,i.institute_status, ifnull(count(si.identification_id),0) totalstudent "
    		+ " from institute_info i,student_identification si "
    		+ " where i.institute_id=si.institute_id and si.academic_year=i.academic_year and si.student_status=true "
    		+ " group by i.institute_id order by i.institute_id",nativeQuery=true)
    public List<Object[]> searchInstituteWiseStudentCount();
    

}
