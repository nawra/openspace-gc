/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.service;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.ApplicationUtils;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.initialsetup.model.entity.CoreSettingDesignation;
import com.openspace.education.initialsetup.model.response.CoreSettingsResponse;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.institute.model.request.InstituteCreateRequest;
import com.openspace.education.institute.model.request.ShebaCommunityInstitusteCreate;
import com.openspace.education.institute.model.response.InstituteInfoResponse;
import com.openspace.education.institute.model.response.InstituteListView;
import com.openspace.education.institute.model.response.InstituteWiseMonthlyStudentCountView;
import com.openspace.education.institute.model.response.InstituteWiseStudentCount;
import com.openspace.education.institute.repository.InstituteRepository;

import com.openspace.education.user.model.entity.UserRoles;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.repository.UserRolesRepository;
import com.openspace.education.user.repository.UsersRepository;

/**
 *
 * @author riad
 */

@Service
public class InstituteService {
    
	Logger logger = LoggerFactory.getLogger(InstituteService.class);
    
   @Autowired 
   private InstituteRepository instituteRepository; 
   
   @Autowired 
   private UsersRepository usersRepository;
   
   @Autowired 
   private UserRolesRepository userRolesRepository;
   
   @Autowired
   private ImageStorageService imageStorageService;
   

	
	public static final String INSTITUTE_IMAGE_PATH = ApplicationUtils.getFilePath("INSTITUTE");
   

   
   public Long findMaxInstituteId(Integer instituteType) {
	   
	   Long maxinstituteId=instituteRepository.findMaxInstituteId(instituteType);
	   if(maxinstituteId==0 && instituteType.equals(1)) {
		   maxinstituteId=10000l; 
	   }else if(maxinstituteId==0 && instituteType.equals(2)) {
		   maxinstituteId=20000l;
	   }else if(maxinstituteId==0 && instituteType.equals(3)) {
		   maxinstituteId=30000l;
	   }
	   return maxinstituteId;
   }
   
   
   @Transactional
   public BaseResponse saveinstitute(InstituteCreateRequest createRequest){
       BaseResponse baseResponse=new BaseResponse();
       Integer currentAcademicYear=Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
       
       if(usersRepository.findByUsername(createRequest.getMobileNumber())!=null){
           baseResponse.setMessage("This Mobile No. Already exists as a User");
           baseResponse.setMessageType(0);
           return baseResponse;
           
       }
       
       Long instituteId=findMaxInstituteId(createRequest.getInstituteType())+1;
       
       Institute institute=new Institute();
       institute.setInstituteId(instituteId);
       institute.setInstituteName(createRequest.getInstituteName());
       institute.setAdminName(createRequest.getAdminName());
       institute.setAddress(createRequest.getAddress());
       institute.setContactNumber(createRequest.getInstituteContactNumber());
       institute.setCreateDate(new Date());
       institute.setInstituteEmail(createRequest.getInstituteEmail());
       institute.setInstituteStatus(1);
       institute.setAcademicYear(currentAcademicYear);
       institute.setInstituteType(createRequest.getInstituteType());
       institute.setOnLineAdmissionServiceCharge(1);
       institute.setOnLineAdmissionStatus(0);
       institute.setBillCycle("Monthly");
       institute.setEiinNo(createRequest.getEiinNo());
       institute=instituteRepository.save(institute);
       
      
        Users users=new Users();
        String encryptedPassword=UserInfoUtils.getHashPassword(createRequest.getMobileNumber());
        users.setEnabled(true);
        users.setPassword(encryptedPassword);
        users.setUsername(createRequest.getMobileNumber());
        users.setNickName(createRequest.getAdminName());
        users.setInstitute(institute);
        users=usersRepository.save(users);
        
        UserRoles roles=new UserRoles();
        roles.setUsername(createRequest.getMobileNumber());
        roles.setRoleName("ROLE_ADMIN");
        roles.setUsername(createRequest.getMobileNumber());
        roles.setInstitute(institute);

        userRolesRepository.save(roles);
       
        
        baseResponse.setMessage("Institute Created Successfully");
       
        baseResponse.setMessageType(1);
       
       return baseResponse;
       
   }
   
   
  
   
   public Integer instituteType(String type) {
	   
	   Integer instype=0;
	   
	   if(type.equals("College")) {
		   instype=1;  
	   }else if(type.equals("High School")) {
		   instype=2;  
	   }else if(type.equals("Primary School")) {
		   instype=3; 
	   }
	   
	   return instype;
   }
   
   
   
   @Transactional
   public ItemResponse saveShebaCommunityInstituteCreate(ShebaCommunityInstitusteCreate createRequest){
	   ItemResponse itemResponse=new ItemResponse();
       Integer currentAcademicYear=Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
       
        Integer instituteType=instituteType(createRequest.getInstituteType());
       
        
        if(instituteType==0){
     	   itemResponse.setMessage("Institute Type is not Correct");
     	   itemResponse.setMessageType(0);
           return itemResponse;
            
        }
        
        
       if(usersRepository.findByUsername(createRequest.getMobileNumber())!=null){
    	   itemResponse.setMessage("This Mobile No. Already exists as a User");
    	   itemResponse.setMessageType(0);
           return itemResponse;
           
       }
       
       
       
       Long instituteId=findMaxInstituteId(instituteType)+1;
       
       Institute institute=new Institute();
       institute.setInstituteId(instituteId);
       institute.setInstituteName(createRequest.getInstituteName());
       institute.setAdminName(createRequest.getAdminName());
       institute.setAddress(createRequest.getAddress());
       institute.setContactNumber(createRequest.getInstituteContactNumber());
       institute.setCreateDate(new Date());
       institute.setInstituteEmail(createRequest.getInstituteEmail());
       institute.setInstituteStatus(1);
       institute.setAcademicYear(currentAcademicYear);
       institute.setInstituteType(instituteType);
       institute.setOnLineAdmissionServiceCharge(1);
       institute.setOnLineAdmissionStatus(0);
       institute.setBillCycle(createRequest.getBillCycle());
       institute.setPackageId(createRequest.getPackageId());
       institute=instituteRepository.save(institute);
       
      
        Users users=new Users();
        String encryptedPassword=UserInfoUtils.getHashPassword(createRequest.getMobileNumber());
        users.setEnabled(true);
        users.setPassword(encryptedPassword);
        users.setUsername(createRequest.getMobileNumber());
        users.setNickName(createRequest.getAdminName());
        users.setInstitute(institute);
        users.setMobileNo(createRequest.getMobileNumber());
        users=usersRepository.save(users);
        
        UserRoles roles=new UserRoles();
        roles.setUsername(createRequest.getMobileNumber());
        roles.setRoleName("ROLE_ADMIN");
        roles.setUsername(createRequest.getMobileNumber());
        roles.setInstitute(institute);

        userRolesRepository.save(roles);
       
        
        itemResponse.setMessage("Institute Created Successfully");
        itemResponse.setMessageType(1);
        itemResponse.setItem(instituteId);
        
        
       return itemResponse;
       
   }
   
   
   

   
   

   @SuppressWarnings({ "unchecked", "rawtypes" })
   public ItemResponse instituteViewInfo(Institute institutecontext) {
	   
	   ItemResponse itemResponse=new ItemResponse();  
	   
	   Institute institute=instituteRepository.getOne(institutecontext.getInstituteId());
	   InstituteInfoResponse instituteInfoResponse=new InstituteInfoResponse();
	   instituteInfoResponse.setAddress(institute.getAddress());
	   instituteInfoResponse.setAdminName(institute.getAdminName());
	   instituteInfoResponse.setContactNumber(institute.getContactNumber());
	   instituteInfoResponse.setInstituteEmail(institute.getInstituteEmail());
	   instituteInfoResponse.setInstituteName(institute.getInstituteName());
	   instituteInfoResponse.setAcademicYear(institute.getAcademicYear());
	   instituteInfoResponse.setEiinNo(institute.getEiinNo());
	   instituteInfoResponse.setInstiltuteId(institute.getInstituteId());
	   
	   try {
		   instituteInfoResponse.setImageName(new String(imageStorageService.fetchImageInBase64Encode(FileFolder.INSTITUTE.name(), institute.getImageName())));   
	   }catch(Exception e) {
		   instituteInfoResponse.setImageName(""); 
	   }
	   
	   itemResponse.setItem(instituteInfoResponse);
	   itemResponse.setMessage("OK");
	   itemResponse.setMessageType(1);
	   
	   return itemResponse;
	   
   }
   
   
   @Transactional
   public BaseResponse updateInstitutePhoto(MultipartFile file) {
	   
	   BaseResponse baseResponse = new BaseResponse();
	   Institute institute=UserInfoUtils.getLoggedInInstitute();
	   Institute instituteSource = instituteRepository.getOne(institute.getInstituteId());
	   if(instituteSource==null) {
		   baseResponse.setMessage("No Instiute Found");
		   baseResponse.setMessageType(0);
		   return baseResponse;
	   }
	   
	   String imageName=institute.getInstituteId()+".png";
	   imageStorageService.uploadImgHeightWidth(FileFolder.INSTITUTE.name(), imageName, file, 300, 300);
	   instituteSource.setImageName(imageName);

	   
	   try {
		
		   if (imageStorageService.createWaterMark(INSTITUTE_IMAGE_PATH+imageName) == false) {
	            logger.info("Sorry! Watermark could not be created.");
	        }
		   
	} catch (Exception e) {
		logger.info(e.getMessage());
	}
	
	   baseResponse.setMessage("Instiute Image Successfully Updated.");
	   baseResponse.setMessageType(1);
	   return baseResponse;
	}
   
   
   @Transactional
   public BaseResponse updateInstitute(InstituteInfoResponse request) {
	   
	   BaseResponse baseResponse = new BaseResponse();
	   Institute institute=UserInfoUtils.getLoggedInInstitute();
	   Institute instituteSource = instituteRepository.getOne(institute.getInstituteId());
	   if(instituteSource==null) {
		   baseResponse.setMessage("No Instiute Found");
		   baseResponse.setMessageType(0);
		   return baseResponse;
	   }
	   
	   instituteSource.setAdminName(request.getAdminName());
	   instituteSource.setContactNumber(request.getContactNumber());
	   instituteSource.setInstituteEmail(request.getInstituteEmail());
	   instituteSource.setAddress(request.getAddress());
	   instituteSource.setInstituteName(request.getInstituteName());
	   instituteSource.setAcademicYear(request.getAcademicYear());
	   instituteSource.setEiinNo(request.getEiinNo());
	
	   baseResponse.setMessage("Instiute Successfully Updated.");
	   baseResponse.setMessageType(1);
	   return baseResponse;
	}
   
   
   public BaseResponse goToInstitute(Long instituteId) {
	   BaseResponse baseResponse = new BaseResponse();
	   Institute realInstitute=instituteRepository.findOne(instituteId);
	   
	   if(realInstitute==null) {
		   baseResponse.setMessage("No Instiute Found");
		   baseResponse.setMessageType(0);
		   return baseResponse;   
	   }
	   
	   Users user = (Users) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	   user.setInstitute(realInstitute);
	   
	   baseResponse.setMessage("OK");
	   baseResponse.setMessageType(1);
	   return baseResponse;
   }
   
   
   
   
   
	public ItemResponse instituteList() {

		ItemResponse itemResponse = new ItemResponse();
		
		List<Institute> list=instituteRepository.findByOnLineAdmissionStatusAndInstituteStatus(1,1);
		
		List<InstituteInfoResponse> responses=new ArrayList<>();
		
		
		for (Institute institute : list) {
			
			InstituteInfoResponse instituteInfoResponse=new InstituteInfoResponse();
			instituteInfoResponse.setInstiltuteId(institute.getInstituteId());
			instituteInfoResponse.setInstituteName(institute.getInstituteName());
			instituteInfoResponse.setAddress(institute.getAddress());
			instituteInfoResponse.setImageName(institute.getImageName());
			responses.add(instituteInfoResponse);
			
		}


		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	
	public ItemResponse instituteListForAdmin() {

		ItemResponse itemResponse = new ItemResponse();
		
		List<Institute> list=instituteRepository.findAll();
		
		List<InstituteListView> responses=new ArrayList<>();
		
		for (Institute institute : list) {
			
			InstituteListView instituteInfoResponse=new InstituteListView();
			instituteInfoResponse.setInstituteId(institute.getInstituteId());
			instituteInfoResponse.setAcademicYear(institute.getAcademicYear());
			instituteInfoResponse.setAddress(institute.getAddress());
			instituteInfoResponse.setAdminName(institute.getAdminName());
			instituteInfoResponse.setContactNumber(institute.getContactNumber());
			instituteInfoResponse.setCreateDate(institute.getCreateDate());
			instituteInfoResponse.setImageName(institute.getImageName());
			instituteInfoResponse.setInstituteEmail(institute.getInstituteEmail());
			instituteInfoResponse.setInstituteName(institute.getInstituteName());
			
			if(institute.getInstituteStatus()==1) {
				instituteInfoResponse.setInstituteStatus("Active");
				instituteInfoResponse.setStatus(true);
			}else {
				instituteInfoResponse.setInstituteStatus("Inactive");
				instituteInfoResponse.setStatus(false);
			}
			
			if(institute.getInstituteType()!=null) {
			instituteInfoResponse.setInstituteType(instituteType(institute.getInstituteType()));
			}
			instituteInfoResponse.setOnLineAdmissionServiceCharge(institute.getOnLineAdmissionServiceCharge());
			instituteInfoResponse.setOnLineAdmissionStatus(institute.getOnLineAdmissionStatus());
			
			responses.add(instituteInfoResponse);
			
		}


		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;

	}
	
	
	public String instituteType(Integer instituteType) {
		
		String type="";
		
		if(instituteType==1) {
			type="College";	
		}else if(instituteType==2) {
			type="High School";	
		}else if(instituteType==3) {
			type="Primary School";	
		}
		
		return type;
	}
	
	
	@Transactional
	public BaseResponse activeInstitute(Long instituteId) {
		   
		   BaseResponse baseResponse = new BaseResponse();
		   Institute realInstitute=instituteRepository.findOne(instituteId);
		   
		   if(realInstitute==null) {
			   
			   baseResponse.setMessage("No Instiute Found");
			   baseResponse.setMessageType(0);
			   return baseResponse;   
		   }else {
			   realInstitute.setInstituteStatus(1);  
		   }
		   		   
		   baseResponse.setMessage("OK");
		   baseResponse.setMessageType(1);
		   return baseResponse;
	   }
	
	
	
	@Transactional
	public BaseResponse inactiveInstitute(Long instituteId) {
		   
		   BaseResponse baseResponse = new BaseResponse();
		   Institute realInstitute=instituteRepository.findOne(instituteId);
		   
		   if(realInstitute == null) {
			   baseResponse.setMessage("No Instiute Found");
			   baseResponse.setMessageType(0);
			   return baseResponse;   
		   }else {
			   realInstitute.setInstituteStatus(0);  
		   }
		   		   
		   baseResponse.setMessage("OK");
		   baseResponse.setMessageType(1);
		   return baseResponse;
	   }
	
	
	public ItemResponse instituteWiseMonthlyStudentCount() {
		
		ItemResponse itemResponse = new ItemResponse();
		
		List<Object[]> objects = instituteRepository.searchMonthlyInstituteStudentCount();
		
		List<InstituteWiseMonthlyStudentCountView> views = new ArrayList<>();
		
		for(Object[] obj : objects) {
		
			InstituteWiseMonthlyStudentCountView view = new InstituteWiseMonthlyStudentCountView();
			view.setInstituteId(Long.parseLong(obj[0].toString()));
			view.setTotalStudent(Long.parseLong(obj[1].toString()));
			views.add(view);
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("OK");
		
		return itemResponse;
		
		
	}
	
	
	public ItemResponse instituteWiseStudentCount() {
		
		ItemResponse itemResponse = new ItemResponse();
		
		List<Object[]> objects = instituteRepository.searchInstituteWiseStudentCount();
		
		List<InstituteWiseStudentCount> views = new ArrayList<>();
		
		for(Object[] obj : objects) {
		
			InstituteWiseStudentCount view = new InstituteWiseStudentCount();
			view.setInstituteId(Long.parseLong(obj[0].toString()));
			view.setInstituteName(obj[1].toString());
			view.setAddress(obj[2].toString());
			Integer instituteStatus=Integer.parseInt(obj[3]+"");
			view.setInstituteStatus(instituteStatus);
			if(instituteStatus==1) {
				view.setInstituteStatusString("Active");	
			}else {
				view.setInstituteStatusString("Inactive");	
			}			
			view.setTotalStudent(Integer.parseInt(obj[4]+""));
			
			views.add(view);
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("OK");
		
		return itemResponse;
		
		
	}
	
	
  public ItemResponse instituteStudentCountByInstituteId(Long instituteId) {
		
		ItemResponse itemResponse = new ItemResponse();
		itemResponse.setItem(0);
		
		Institute institute = instituteRepository.findOne(instituteId);
		
		if(institute==null) {
			itemResponse.setMessageType(0);
			itemResponse.setMessage("No institute Found");
			return itemResponse;
		}
		
		
		itemResponse.setMessageType(0);
		
		List<Object[]> objects = instituteRepository.searchMonthlyInstituteStudentCount(instituteId);
		
		for(Object[] obj : objects) {
			
			itemResponse.setItem(Long.parseLong(obj[1].toString()));
			itemResponse.setMessageType(1);
		}
		
		itemResponse.setMessage("OK");
		
		return itemResponse;
		
		
	}
    
}
