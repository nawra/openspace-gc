/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author riad
 */

@Entity
@Table(name = "institute_info")
public class Institute implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "institute_name")
	private String instituteName;

	@Column(name = "institute_status")
	private Integer instituteStatus;

	@Column(name = "address")
	private String address;

	@Column(name = "admin_name")
	private String adminName;

	@Column(name = "contact_number")
	private String contactNumber;

	@Column(name = "institute_email")
	private String instituteEmail;

	@Column(name = "academic_year")
	private Integer academicYear;

	@Column(name = "image_name")
	private String imageName;
	
	
	@Column(name = "eiin_no", unique = true)
	private String eiinNo;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "institute_type") // 1 for College, 2 for High School, 3 for Primary School
	private Integer instituteType;

	@Column(name = "online_admission_status") //1 for online admission taken 0 for online admission not taken
	private Integer onLineAdmissionStatus;
	
	@Column(name = "online_admission_service_charge")
	private Integer onLineAdmissionServiceCharge;
	
	@Column(name = "bill_cycle")
	private String billCycle;
	
	@Column(name = "package_id")
	private Integer packageId;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public Integer getInstituteStatus() {
		return instituteStatus;
	}

	public void setInstituteStatus(Integer instituteStatus) {
		this.instituteStatus = instituteStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getInstituteEmail() {
		return instituteEmail;
	}

	public void setInstituteEmail(String instituteEmail) {
		this.instituteEmail = instituteEmail;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public Integer getInstituteType() {
		return instituteType;
	}

	public void setInstituteType(Integer instituteType) {
		this.instituteType = instituteType;
	}

	public Integer getOnLineAdmissionServiceCharge() {
		return onLineAdmissionServiceCharge;
	}

	public void setOnLineAdmissionServiceCharge(Integer onLineAdmissionServiceCharge) {
		this.onLineAdmissionServiceCharge = onLineAdmissionServiceCharge;
	}

	public Integer getOnLineAdmissionStatus() {
		return onLineAdmissionStatus;
	}

	public void setOnLineAdmissionStatus(Integer onLineAdmissionStatus) {
		this.onLineAdmissionStatus = onLineAdmissionStatus;
	}

	public String getBillCycle() {
		return billCycle;
	}

	public void setBillCycle(String billCycle) {
		this.billCycle = billCycle;
	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public String getEiinNo() {
		return eiinNo;
	}

	public void setEiinNo(String eiinNo) {
		this.eiinNo = eiinNo;
	}

	
	
	
}
