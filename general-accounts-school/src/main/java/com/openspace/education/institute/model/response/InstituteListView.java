package com.openspace.education.institute.model.response;

import java.util.Date;

import javax.persistence.Column;

public class InstituteListView {
	
	private Long instituteId;

	private String instituteName;

	private String instituteStatus;
	
	private boolean status;

	private String address;

	private String adminName;

	private String contactNumber;

	private String instituteEmail;

	private Integer academicYear;

	private String imageName;

	private Date createDate;


	// 1 for College, 2 for High School, 3 for Primary School
	private String instituteType="";

	private Integer onLineAdmissionStatus;
	
	private Integer onLineAdmissionServiceCharge;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getInstituteStatus() {
		return instituteStatus;
	}

	public void setInstituteStatus(String instituteStatus) {
		this.instituteStatus = instituteStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getInstituteEmail() {
		return instituteEmail;
	}

	public void setInstituteEmail(String instituteEmail) {
		this.instituteEmail = instituteEmail;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	

	public String getInstituteType() {
		return instituteType;
	}

	public void setInstituteType(String instituteType) {
		this.instituteType = instituteType;
	}

	public Integer getOnLineAdmissionStatus() {
		return onLineAdmissionStatus;
	}

	public void setOnLineAdmissionStatus(Integer onLineAdmissionStatus) {
		this.onLineAdmissionStatus = onLineAdmissionStatus;
	}

	public Integer getOnLineAdmissionServiceCharge() {
		return onLineAdmissionServiceCharge;
	}

	public void setOnLineAdmissionServiceCharge(Integer onLineAdmissionServiceCharge) {
		this.onLineAdmissionServiceCharge = onLineAdmissionServiceCharge;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	
}
