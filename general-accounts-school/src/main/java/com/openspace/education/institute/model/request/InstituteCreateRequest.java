/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.model.request;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author riad
 */
public class InstituteCreateRequest {
    

  
    @NotBlank(message = "Institute name can not be blank")
    private String instituteName;
    
    @NotBlank(message = "Address can not be blank")
    private String address;
    
    @NotBlank(message = "Email can not be blank")
    private String instituteEmail;
    
    @NotBlank(message = "Contact number can not be blank")
    private String instituteContactNumber;

    @NotBlank(message = "Admin can not be blank")
    private String adminName;
    
    @NotBlank(message = "Gender can not be blank")
    private String gender;
    
    @NotBlank(message = "Religion can not be blank")
    private String religion;
    
    @NotBlank(message = "Mobile Number can not be blank")
    private String mobileNumber;
    
    
    @NotNull(message = "Institute Type can not be null")
    private Integer instituteType;
    
    
    private String eiinNo;

   

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    

    public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getInstituteEmail() {
		return instituteEmail;
	}

	public void setInstituteEmail(String instituteEmail) {
		this.instituteEmail = instituteEmail;
	}

	public String getInstituteContactNumber() {
		return instituteContactNumber;
	}

	public void setInstituteContactNumber(String instituteContactNumber) {
		this.instituteContactNumber = instituteContactNumber;
	}

	public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

	public Integer getInstituteType() {
		return instituteType;
	}

	public void setInstituteType(Integer instituteType) {
		this.instituteType = instituteType;
	}

	public String getEiinNo() {
		return eiinNo;
	}

	public void setEiinNo(String eiinNo) {
		this.eiinNo = eiinNo;
	}
    


    
}
