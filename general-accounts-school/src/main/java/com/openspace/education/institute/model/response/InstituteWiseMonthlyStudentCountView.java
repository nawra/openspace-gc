package com.openspace.education.institute.model.response;

public class InstituteWiseMonthlyStudentCountView {
	
	private Long instituteId;
	private Long totalStudent;
	
	public Long getInstituteId() {
		return instituteId;
	}
	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}
	public Long getTotalStudent() {
		return totalStudent;
	}
	public void setTotalStudent(Long totalStudent) {
		this.totalStudent = totalStudent;
	}
	
	

}
