/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.model.response;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author riad
 */
public class InstituteInfoResponse {
    


	private Long instiltuteId;
	@NotBlank
    private String instituteName;

	@NotBlank
    private String address;

	@NotBlank
    private String adminName;
    
	@NotBlank
    private String contactNumber;
    
    private String instituteEmail;
    
    private String imageName;
    
    private Integer academicYear;
    
    private String eiinNo;
    


	public String getInstituteName() {
		return instituteName;
	}


	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}


	

	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getAdminName() {
		return adminName;
	}


	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	public String getContactNumber() {
		return contactNumber;
	}


	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}


	public String getInstituteEmail() {
		return instituteEmail;
	}


	public void setInstituteEmail(String instituteEmail) {
		this.instituteEmail = instituteEmail;
	}


	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	public Integer getAcademicYear() {
		return academicYear;
	}


	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}


	public Long getInstiltuteId() {
		return instiltuteId;
	}


	public void setInstiltuteId(Long instiltuteId) {
		this.instiltuteId = instiltuteId;
	}


	public String getEiinNo() {
		return eiinNo;
	}


	public void setEiinNo(String eiinNo) {
		this.eiinNo = eiinNo;
	}


	

    
}
