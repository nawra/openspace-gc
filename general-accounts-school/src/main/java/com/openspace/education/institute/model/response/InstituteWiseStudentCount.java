package com.openspace.education.institute.model.response;

public class InstituteWiseStudentCount {

	private Long instituteId;
	
	private String instituteName;
	
	private String address;
	
	private Integer instituteStatus;
	
	private String instituteStatusString;
	
	private Integer totalStudent;

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getInstituteStatus() {
		return instituteStatus;
	}

	public void setInstituteStatus(Integer instituteStatus) {
		this.instituteStatus = instituteStatus;
	}

	public String getInstituteStatusString() {
		return instituteStatusString;
	}

	public void setInstituteStatusString(String instituteStatusString) {
		this.instituteStatusString = instituteStatusString;
	}

	public Integer getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(Integer totalStudent) {
		this.totalStudent = totalStudent;
	}
	
	
}
