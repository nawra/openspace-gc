package com.openspace.education.institute.model.request;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class ShebaCommunityInstitusteCreate {
	
	@NotBlank(message = "Institute name can not be blank")
    private String instituteName;
    
    @NotBlank(message = "Address can not be blank")
    private String address;
    
    @NotBlank(message = "Email can not be blank")
    private String instituteEmail;
    
    @NotBlank(message = "Contact number can not be blank")
    private String instituteContactNumber;

    @NotBlank(message = "Admin can not be blank")
    private String adminName;
    
    @NotBlank(message = "Mobile Number can not be blank")
    private String mobileNumber;
    
    @NotBlank(message = "Institute Type can not be blank")
    private String instituteType;
    
    @NotBlank(message = "Bill Cycle can not be blank")
    private String billCycle;
    
    @NotNull(message = "packageId can not be null")
    private Integer packageId;

	public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInstituteEmail() {
		return instituteEmail;
	}

	public void setInstituteEmail(String instituteEmail) {
		this.instituteEmail = instituteEmail;
	}

	public String getInstituteContactNumber() {
		return instituteContactNumber;
	}

	public void setInstituteContactNumber(String instituteContactNumber) {
		this.instituteContactNumber = instituteContactNumber;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getInstituteType() {
		return instituteType;
	}

	public void setInstituteType(String instituteType) {
		this.instituteType = instituteType;
	}

	public String getBillCycle() {
		return billCycle;
	}

	public void setBillCycle(String billCycle) {
		this.billCycle = billCycle;
	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	
    
    
    
    

}
