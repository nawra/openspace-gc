/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.institute.controller;


import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.institute.model.request.InstituteCreateRequest;
import com.openspace.education.institute.model.request.ShebaCommunityInstitusteCreate;
import com.openspace.education.institute.model.response.InstituteInfoResponse;
import com.openspace.education.institute.service.InstituteService;


@Controller
@RequestMapping("/institute")
public class InstituteController {
    
    @Autowired
    private InstituteService instituteService;

    @PostMapping(value = "/registration")
    public String createinstitute(@Valid @ModelAttribute("instituteReq") InstituteCreateRequest instituteReq,BindingResult result,Model model)  {
    	
    	if (result.hasErrors()) {
    		model.addAttribute("instituteReq", instituteReq);
            return "institute-registration";
            
        }
    	BaseResponse baseResponse=instituteService.saveinstitute(instituteReq);
    	model.addAttribute("instituteReq", new InstituteCreateRequest());
    	model.addAttribute("msg", baseResponse.getMessage());
    	return "institute-registration";
    }
    
    
    
   
    
    
    @GetMapping(value = "/view")
    public ResponseEntity<ItemResponse> instituteView()  {
    	Institute institute=UserInfoUtils.getLoggedInInstitute();
    	ItemResponse itemResponse = instituteService.instituteViewInfo(institute);
    	return new ResponseEntity<>(itemResponse,HttpStatus.OK);
    }
    
    @GetMapping(value = "/wise/student/count")
    public ResponseEntity<ItemResponse> instituteWiseStudentCount()  {
    	ItemResponse itemResponse = instituteService.instituteWiseStudentCount();
    	return new ResponseEntity<>(itemResponse,HttpStatus.OK);
    }
    
    
    
    @PostMapping(value = "/update")
    public ResponseEntity<BaseResponse> instituteUpdate(@RequestBody @Valid InstituteInfoResponse infoResponse)  {
    	BaseResponse baseResponse=instituteService.updateInstitute(infoResponse);
    	return new ResponseEntity<>(baseResponse,HttpStatus.OK);
    }
    
    @PostMapping(value = "/photo/update")
    public ResponseEntity<BaseResponse> photoUpdate(@RequestParam MultipartFile file)  {
    	BaseResponse baseResponse=instituteService.updateInstitutePhoto(file);
    	return new ResponseEntity<>(baseResponse,HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/jump")
    public ResponseEntity<BaseResponse> instituteJump(@RequestParam Long instituteId)  {
    	BaseResponse baseResponse=instituteService.goToInstitute(instituteId);
    	return new ResponseEntity<>(baseResponse,HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/list")
    public ResponseEntity<ItemResponse> instituteList()  {
    	ItemResponse itemResponse=instituteService.instituteListForAdmin();
    	return new ResponseEntity<>(itemResponse,HttpStatus.OK);
    }
    

    @GetMapping(value = "/active")
    public ResponseEntity<BaseResponse> instituteActive(@RequestParam Long instituteId)  {
    	BaseResponse baseResponse=instituteService.activeInstitute(instituteId);
    	return new ResponseEntity<>(baseResponse,HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/inactive")
    public ResponseEntity<BaseResponse> instituteInactive(@RequestParam Long instituteId)  {
    	BaseResponse baseResponse=instituteService.inactiveInstitute(instituteId);
    	return new ResponseEntity<>(baseResponse,HttpStatus.OK);
    }
    
    
    
    @PostMapping(value = "/create/sheba-community")
    @PreAuthorize("hasRole('ROLE_SHEBA_COMMUNITY')")
    public ResponseEntity<ItemResponse> createShebaCommunityInstitute(@RequestBody @Valid ShebaCommunityInstitusteCreate instituteReq)  {
    	ItemResponse itemResponse=instituteService.saveShebaCommunityInstituteCreate(instituteReq);
    	return new ResponseEntity<>(itemResponse, HttpStatus.CREATED);
    }
    
    @GetMapping(value = "/wise/monthly-bill/total/student")
    @PreAuthorize("hasRole('ROLE_SHEBA_COMMUNITY')")
    public ResponseEntity<ItemResponse> instituteWiseMonthlyTotalStudent()  {
    	ItemResponse itemResponse = instituteService.instituteWiseMonthlyStudentCount();
    	return new ResponseEntity<>(itemResponse, HttpStatus.OK);
    }
    
    
    @GetMapping(value = "/total/student")
    @PreAuthorize("hasRole('ROLE_SHEBA_COMMUNITY')")
    public ResponseEntity<ItemResponse> instituteTotalStudent(@RequestParam Long instituteId)  {
    	ItemResponse itemResponse = instituteService.instituteStudentCountByInstituteId(instituteId);
    	return new ResponseEntity<>(itemResponse, HttpStatus.OK);
    }
    
}
