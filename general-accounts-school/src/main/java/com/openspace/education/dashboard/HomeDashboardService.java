package com.openspace.education.dashboard;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingClassRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.repository.StaffRepository;
import com.openspace.education.student.model.entity.StudentBasic;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdentificationRepository;

@Service
public class HomeDashboardService {
	
	private Logger logger = LoggerFactory.getLogger(HomeDashboardService.class);

	@Autowired
	private StaffRepository staffRepository;
	@Autowired
	private StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	private CoreSettingClassRepository coreSettingClassRepository;
	
	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;
	
	
	public ItemResponse dashBoardInfo() {

		ItemResponse itemResponse = new ItemResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		HomeDashboardResponse hd = new HomeDashboardResponse();

		List<Staff> staffList = staffRepository.findByInstituteAndStaffStatusOrderByStaffSerialAsc(institute, 1);
		List<CoreSettingClass> classes = coreSettingClassRepository.findByInstituteOrderBySerial(institute);
		List<ClassConfiguration> classConfigurations=classConfigurationRepository.findByInstituteOrderBySerial(institute);
		
		List<StudentIdentification> studentIdentificationList = studentIdentificationRepository.findByInstituteAndAcademicYearAndStudentStatusOrderByStudentRollAsc(institute, institute.getAcademicYear(),true);

		int totalHr = 0;
		int totalMaleHr = 0;
		int totalFemaleHr = 0;
		int totalOthersHr = 0;
		
		int totalStudent = 0;
		int totalMaleStudent = 0;
		int totalFemaleStudent = 0;
		int totalOthersStudent = 0;

		for (Staff staff : staffList) {
			
			totalHr++;

			if (staff.getGender().equalsIgnoreCase("Male")) {

				totalMaleHr++;

			} else if (staff.getGender().equalsIgnoreCase("Female")) {

				totalFemaleHr++;
			} else {
				totalOthersHr++;
			}
			
		}
		
		
		for(StudentIdentification si : studentIdentificationList) {
			
			totalStudent++;

			if (si.getStudentBasic().getStudentGender().equalsIgnoreCase("Male")) {

				totalMaleStudent++;

			} else if (si.getStudentBasic().getStudentGender().equalsIgnoreCase("Female")) {

				totalFemaleStudent++;
			} else {
				totalOthersStudent++;
			}
		}
		
		hd.setTotalHr(totalHr);
		hd.setTotalMaleHr(totalMaleHr);
		hd.setTotalFemalHr(totalFemaleHr);
		hd.setTotalOthersHr(totalOthersHr);
		hd.setTotalStudent(totalStudent);
		hd.setTotalMaleStudent(totalMaleStudent);
		hd.setTotalFemaleStudent(totalFemaleStudent);
		hd.setTotalOthersStudent(totalOthersStudent);
		hd.setTotalClass(classes.size());
		hd.setTotalSection(classConfigurations.size());
		
		itemResponse.setItem(hd);

//		long totalMaleStaff = staffList.stream().filter(s -> s.getGender().equalsIgnoreCase("Male")).count();

		return itemResponse;
	}

}
