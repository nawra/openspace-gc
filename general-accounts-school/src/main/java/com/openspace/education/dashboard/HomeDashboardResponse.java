package com.openspace.education.dashboard;

public class HomeDashboardResponse {

	private int totalStudent;
	private int totalMaleStudent;
	private int totalFemaleStudent;
	private int totalOthersStudent;
	private int totalHr;
	private int totalMaleHr;
	private int totalFemalHr;
	private int totalOthersHr;
	
	private int totalClass;
	private int totalSection;
	

	public int getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(int totalStudent) {
		this.totalStudent = totalStudent;
	}

	public int getTotalMaleStudent() {
		return totalMaleStudent;
	}

	public void setTotalMaleStudent(int totalMaleStudent) {
		this.totalMaleStudent = totalMaleStudent;
	}

	public int getTotalFemaleStudent() {
		return totalFemaleStudent;
	}

	public void setTotalFemaleStudent(int totalFemaleStudent) {
		this.totalFemaleStudent = totalFemaleStudent;
	}

	public int getTotalHr() {
		return totalHr;
	}

	public void setTotalHr(int totalHr) {
		this.totalHr = totalHr;
	}

	public int getTotalMaleHr() {
		return totalMaleHr;
	}

	public void setTotalMaleHr(int totalMaleHr) {
		this.totalMaleHr = totalMaleHr;
	}

	public int getTotalFemalHr() {
		return totalFemalHr;
	}

	public void setTotalFemalHr(int totalFemalHr) {
		this.totalFemalHr = totalFemalHr;
	}

	public int getTotalOthersStudent() {
		return totalOthersStudent;
	}

	public void setTotalOthersStudent(int totalOthersStudent) {
		this.totalOthersStudent = totalOthersStudent;
	}

	public int getTotalOthersHr() {
		return totalOthersHr;
	}

	public void setTotalOthersHr(int totalOthersHr) {
		this.totalOthersHr = totalOthersHr;
	}

	public int getTotalClass() {
		return totalClass;
	}

	public void setTotalClass(int totalClass) {
		this.totalClass = totalClass;
	}

	public int getTotalSection() {
		return totalSection;
	}

	public void setTotalSection(int totalSection) {
		this.totalSection = totalSection;
	}

	
	
}
