package com.openspace.education.dashboard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.openspace.education.common.ItemResponse;

@Controller
@RequestMapping(value = "/home/dashboard")
public class HomeDashboardController {
	
	@Autowired
	private HomeDashboardService homeDashboardService;
	
	@GetMapping(value = "/info")
	public ResponseEntity<ItemResponse> getHomeDashboardInfo(){
		return new ResponseEntity<>(homeDashboardService.dashBoardInfo(), HttpStatus.OK);
	}
	
	

}
