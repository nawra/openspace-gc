package com.openspace.education.filereader.model.response;

public class DeviceDataLogResponse {
	   
    private String userId;
    private String punchDate;
    private String punchTime;
    private String punchType;
    private int deviceSerial;

    public int getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(int deviceSerial) {
		this.deviceSerial = deviceSerial;
	}

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPunchDate() {
        return punchDate;
    }

    public void setPunchDate(String punchDate) {
        this.punchDate = punchDate;
    }

    public String getPunchTime() {
        return punchTime;
    }

    public void setPunchTime(String punchTime) {
        this.punchTime = punchTime;
    }

    public String getPunchType() {
        return punchType;
    }

    public void setPunchType(String punchType) {
        this.punchType = punchType;
    }

 
    
    
}
