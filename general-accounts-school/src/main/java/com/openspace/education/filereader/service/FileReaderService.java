package com.openspace.education.filereader.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.filereader.model.response.DeviceDataLogResponse;




@Service
public class FileReaderService {
	
	

	@SuppressWarnings("rawtypes")
	public ItemResponse readtxtFile(MultipartFile file, String date) {
		
		ItemResponse response = new ItemResponse();

		if (file.getContentType().equals("text/plain")) {
			
			DeviceDataLogResponse ddlr;

			List<DeviceDataLogResponse> ddlrs = new ArrayList<>();
			
			try {
				InputStream inputStream = file.getInputStream();
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
					String line;
				
					while ((line = reader.readLine()) != null) {
						ddlr = new DeviceDataLogResponse();
						line = line.replaceAll("\t", " ");
						StringTokenizer tokenizer = new StringTokenizer(line, " ");
						int col = 1;
						if (line.contains(date)) {
							while (tokenizer.hasMoreTokens()) {
								if (col > 5) {
									break;
								}
								String token = tokenizer.nextToken();
								switch (col) {
								case 1:
									ddlr.setUserId(token);
									break;
								case 2:								
									ddlr.setPunchDate(token);
									break;
								case 3:
									ddlr.setPunchTime(token);
									break;
								case 4:
									ddlr.setPunchType(token);
									break;
								case 5:
									ddlr.setDeviceSerial(Integer.parseInt(token));
									break;
								}
								col++;
							}
							ddlrs.add(ddlr);
						}
						
					}
					if (ddlrs.size() > 0) {
						response.setItem(ddlrs);
						response.setMessageType(1);
						response.setMessage("");
					} else {
						response.setMessageType(0);
						response.setMessage("No Data Found");
					}
				}

			} catch (IOException ex) {
				Logger.getLogger(FileReaderService.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			response.setMessageType(0);
			response.setMessage("Please select a .txt file");
		}
		return response;
	}

}
