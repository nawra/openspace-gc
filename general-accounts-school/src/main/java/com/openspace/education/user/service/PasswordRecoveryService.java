package com.openspace.education.user.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentBasic;
import com.openspace.education.student.model.entity.StudentProfileUpdateOtp;
import com.openspace.education.student.repository.StudentBasicRepository;
import com.openspace.education.student.repository.StudentProfileUpdateOtpRepository;
import com.openspace.education.user.model.entity.PasswordRecovery;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.repository.PasswordRecoveryRepository;
import com.openspace.education.user.repository.UsersRepository;

@Service
public class PasswordRecoveryService {
	
	@Autowired
	public UsersRepository usersRepository;
	
	@Autowired
	public PasswordRecoveryRepository passwordRecoveryRepository;
	
	@Autowired
	private StudentProfileUpdateOtpRepository studentProfileUpdateOtpRepository;
	

	
	@Autowired
	private StudentBasicRepository studentBasicRepository;
	
	
	
	
	
	
	
	@Transactional
	public BaseResponse resetPassword(String userName,String password,String token) {
		
		PasswordRecovery passwordRecovery=passwordRecoveryRepository.findByUserName(userName);
		BaseResponse baseResponse=new BaseResponse();
		if(passwordRecovery.getToken().equals(token)) {
			Users user=usersRepository.findByUsername(userName);
			String encryptedPassword=  UserInfoUtils.getHashPassword(password);
			user.setPassword(encryptedPassword);
		}else {
			baseResponse.setMessage("Token does not match");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		baseResponse.setMessage("Password Successfully Reset");
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
	
	
	
	//=========================================================
	
	
	
	
	@Transactional
	public BaseResponse useProfileUpdateToken(Long instituteId,String customStudentId,String token) {
		
		BaseResponse baseResponse=new BaseResponse();
	
		
		StudentProfileUpdateOtp studentProfileUpdateOtps = studentProfileUpdateOtpRepository.findByInstituteIdAndCustomStudentIdAndOtpGenerateDateAndTokenAndOtpStatus(instituteId,customStudentId,new Date(),token,0);
		
		if(studentProfileUpdateOtps==null) {
			
			baseResponse.setMessage("OTP already used.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}else {
			studentProfileUpdateOtps.setOtpStatus(1);
			studentProfileUpdateOtpRepository.save(studentProfileUpdateOtps);
			
		}
		
		baseResponse.setMessage("OTP used successfull");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	}


}
