package com.openspace.education.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.user.model.entity.UserRoles;

public interface UserRolesRepository extends JpaRepository<UserRoles, Long>{
	
	public List<UserRoles> findByUsername(String username);
	
	public List<UserRoles> findByUsernameAndInstitute(String username,Institute institute);
	
	public List<UserRoles> findByInstitute(Institute institute);

}
