package com.openspace.education.user.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.TestBaseResponse;
import com.openspace.education.generalaccount.model.request.ReceiptPaymentRequest;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.model.request.PasswordChangeRequest;
import com.openspace.education.user.model.request.UserCreateRequest;
import com.openspace.education.user.model.request.UserUpdateRequest;
import com.openspace.education.user.service.ClassInfoCreateRequest;
import com.openspace.education.user.service.DressInfoViewResponse;
import com.openspace.education.user.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private SessionRegistry sessionRegistry;
	
	@Autowired
	private UserService userService;
	
	
	
	   @PostMapping(value="/create") 
	   public ResponseEntity<BaseResponse> createUser(@RequestBody  @Valid UserCreateRequest userCreateRequest){
	        BaseResponse baseResponse=userService.createUser(userCreateRequest);
	      return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	   }
	   
	   
	   @PostMapping(value="/update") 
	   public ResponseEntity<BaseResponse> updateUser(@RequestBody  @Valid UserUpdateRequest userUpdateRequest){
	        BaseResponse baseResponse=userService.updateUser(userUpdateRequest);
	      return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	   }
	   
	   @DeleteMapping(value="/delete") 
	   public ResponseEntity<BaseResponse> deleteUser(@RequestParam String username){
	        BaseResponse baseResponse=userService.deleteUser(username);
	      return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	   }
	   
	   
	   @GetMapping(value="/list") 
	   public ResponseEntity<ItemResponse> userList(){
		   ItemResponse itemResponse=userService.userList();
	      return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	   }
	   
	   
	   @PostMapping(value="/change/password") 
	   public ResponseEntity<BaseResponse> changePassword(@RequestBody @Valid PasswordChangeRequest passwordChangeRequest){
		   BaseResponse baseResponse=userService.changeUserPasword(passwordChangeRequest);
	      return new ResponseEntity<>(baseResponse,HttpStatus.OK);
	   }
	   
	
	
	   
	   
	   
	   //--------------------------------Test Purpose----------------------------------------//  

	 
	  @GetMapping("/loggedUsers")
	  public ResponseEntity<ItemResponse> getLoggedUsers() {
	        
	   List<Object> principals = sessionRegistry.getAllPrincipals();

	   List<String> usersNamesList = new ArrayList<String>();

	    for (Object principal: principals) {
	    	
	    if (principal instanceof Users) {
	    	
	    usersNamesList.add(((Users) principal).getUsername());
	    
	    }
	    
	    }			
	    	
	   ItemResponse itemResponse=new ItemResponse(usersNamesList);
	    	
	   return new ResponseEntity<>(itemResponse,HttpStatus.OK);

	 }
	  
	  
	  
	  
	  

	@GetMapping("/testing")
	  public ResponseEntity<TestBaseResponse> testing() {
	        
		  TestBaseResponse itemResponse=new TestBaseResponse();
		  String uri="http://localhost:8080/dress-info/list?cmsId=146";
		  try {
		         HttpResponse<String> response;
		         response = Unirest.get(uri)
		                    .header("content-type", "application/json")
		                    .asString();

		         if(response.getStatus()==200){
		            ObjectMapper mapper = new ObjectMapper();
		            itemResponse=mapper.readValue(response.getBody(), TestBaseResponse.class);
		            List<DressInfoViewResponse> list=(List<DressInfoViewResponse>) itemResponse.getItem();
		            
		            System.out.println("List size"+list.size());
		         }else{

		         }            

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    	
		   return new ResponseEntity<>(itemResponse,HttpStatus.OK);

		 }
	
	
	
	 
	  @GetMapping("/testing2")
	  public ResponseEntity<TestBaseResponse> testing2() {
		  
		String uri ="http://localhost:8080/dress-info/list?cmsId=146";  
		TestBaseResponse itemResponse=new TestBaseResponse();
        HttpHeaders headers = new HttpHeaders();
        
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);
        try {
        
        ResponseEntity<String> response = new RestTemplate().
                exchange(uri, HttpMethod.GET,requestEntity, String.class);
        
        if (response.getStatusCodeValue() == 200) {
        	ObjectMapper mapper = new ObjectMapper();
        	itemResponse=mapper.readValue(response.getBody(), TestBaseResponse.class);
        	List<DressInfoViewResponse> list=(List<DressInfoViewResponse>) itemResponse.getItem();
            System.out.println("List size"+list.size());
        } 
        
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return new ResponseEntity<>(itemResponse,HttpStatus.OK);
     }
	 
	 
	 
	  @PostMapping("/testing/save")
	  public ResponseEntity<TestBaseResponse> saveCclassInfo(@RequestBody ClassInfoCreateRequest dto) {

		TestBaseResponse baseResponse=new TestBaseResponse<>();
	    HttpHeaders headers = new HttpHeaders();
		headers.add("content-type","application/json");


	        String uri ="http://localhost:8080/class-info/save";
	        
	        Integer responsecode=null;

	       try {

	        HttpEntity<ClassInfoCreateRequest> request = new HttpEntity<>(dto, headers);

	        ResponseEntity<String> response = new RestTemplate().exchange(uri, HttpMethod.POST, request, String.class);
	        responsecode=response.getStatusCodeValue();
	        if (responsecode == 201) {
	            ObjectMapper mapper = new ObjectMapper();
	            baseResponse=mapper.readValue(response.getBody(), TestBaseResponse.class);       
	        
	        }

	        }catch (Exception e) {
	        	baseResponse.setItem("Exception = "+e.getLocalizedMessage());
	        }       
	        
	 
	  
	  return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
			  
}
	 
	 

}
