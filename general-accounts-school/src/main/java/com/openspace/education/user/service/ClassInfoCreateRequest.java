package com.openspace.education.user.service;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class ClassInfoCreateRequest {

	
	@NotNull
	@NotBlank
	private String className;
	
	private int classSerial=0;
	
	@NotNull
	private Long cmsId;
	

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public int getClassSerial() {
		return classSerial;
	}

	public void setClassSerial(int classSerial) {
		this.classSerial = classSerial;
	}

	public Long getCmsId() {
		return cmsId;
	}

	public void setCmsId(Long cmsId) {
		this.cmsId = cmsId;
	}
	
	
	
}
