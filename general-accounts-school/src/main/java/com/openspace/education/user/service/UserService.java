package com.openspace.education.user.service;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.institute.repository.InstituteRepository;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.repository.StaffRepository;
import com.openspace.education.user.model.entity.UserRoles;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.model.request.PasswordChangeRequest;
import com.openspace.education.user.model.request.UserCreateRequest;
import com.openspace.education.user.model.request.UserUpdateRequest;
import com.openspace.education.user.model.response.PostLoginViewInfo;
import com.openspace.education.user.model.response.UserInfo;
import com.openspace.education.user.repository.UserRolesRepository;
import com.openspace.education.user.repository.UsersRepository;



@Service
public class UserService {
	
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private UserRolesRepository userRolesRepository;
	
	@Autowired
	private InstituteRepository instituteRepository;
	
	@Autowired
	private StaffRepository staffRepository;
	
	@Autowired
	private ImageStorageService imageStorageService;
	
	
	@Transactional
    public BaseResponse createUser(UserCreateRequest request){
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		Staff staff = staffRepository.findByStaffIdAndInstitute(request.getStaffId(), institute);
        
        BaseResponse baseResponse=new BaseResponse();
        Users checkUser=usersRepository.findByUsername(request.getUserName());
        if(checkUser!=null){
         baseResponse.setMessage("This username already exists.");
         baseResponse.setMessageType(0);
         return baseResponse;
        }
        Users users=new Users();
        
        String encryptedPassword=UserInfoUtils.getHashPassword(request.getPassword());
        users.setEnabled(true);
        users.setPassword(encryptedPassword);
        users.setUsername(request.getUserName());
        users.setNickName(request.getNickName());
        users.setMobileNo(request.getMobileNo());
        users.setInstitute(institute);
        users.setStaff(staff);
        List<UserRoles> userRoles=new ArrayList<>();
        UserRoles roles;
        
        for(String r:request.getRoleList()){
            roles=new UserRoles();
            roles.setUsername(request.getUserName());
            roles.setRoleName(r);
            roles.setInstitute(institute);
            userRoles.add(roles);
        }
        
        usersRepository.save(users);
        userRolesRepository.save(userRoles);
        
        baseResponse.setMessage("User has been created succesfully.");
        baseResponse.setMessageType(1);
        return baseResponse;
        
    }
	
	
	@Transactional
    public BaseResponse updateUser(UserUpdateRequest request){
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
        
        BaseResponse baseResponse=new BaseResponse();
        Users user=usersRepository.findByUsernameAndInstitute(request.getUserName(), institute);
        
        if(user==null) {
        	baseResponse.setMessage("User not found");
            baseResponse.setMessageType(0);
            return baseResponse;	
        }
        user.setNickName(request.getNickName());
        user.setEnabled(request.isEnabled());
        user.setMobileNo(request.getMobileNo());
       
        
        List<UserRoles> roleses=userRolesRepository.findByUsername(user.getUsername());
        
        userRolesRepository.delete(roleses);
        userRolesRepository.flush();
        
        List<UserRoles> userRoles=new ArrayList<>();
        for(String r:request.getRoleList()){
        	UserRoles roles=new UserRoles();
            roles.setUsername(request.getUserName());
            roles.setRoleName(r);
            roles.setInstitute(institute);
            userRoles.add(roles);
        }
        

        userRolesRepository.save(userRoles);
        
        baseResponse.setMessage("User has been updated succesfully.");
        baseResponse.setMessageType(1);
        return baseResponse;
        
    }
	
	
	@Transactional
    public BaseResponse deleteUser(String username){
        
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
        BaseResponse baseResponse=new BaseResponse();
        Users user=usersRepository.findByUsernameAndInstitute(username,institute);
        
        if(user==null) {
        	baseResponse.setMessage("User not found");
            baseResponse.setMessageType(0);
            return baseResponse;	
        }       
        
        List<UserRoles> roleses=userRolesRepository.findByUsernameAndInstitute(user.getUsername(),institute);        
       
        usersRepository.delete(user);

        userRolesRepository.delete(roleses);
        
        baseResponse.setMessage("User has been deleted succesfully.");
        baseResponse.setMessageType(1);
        return baseResponse;
        
    }
	
	
	public void findUserRoles(String username,List<UserRoles> roleses,List<String> roleList) {
		
		for(UserRoles r:roleses) {
			
			if(r.getUsername().equals(username)) {
				roleList.add(r.getRoleName());	
			}
		}
	}
	
	public ItemResponse userList() {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Users> users=usersRepository.findByInstitute(institute) ;
		List<UserRoles> roleses=userRolesRepository.findByInstitute(institute) ; 
		
		List<UserInfo> userInfos=new ArrayList<>();
		
		for(Users user:users) {
			UserInfo userInfo=new UserInfo();
			userInfo.setEnabled(user.isEnabled());
			userInfo.setNickName(user.getNickName());
			userInfo.setStatus(user.isEnabled()==true ? "Enabled" : "Disabled");
			userInfo.setUserid(user.getId());
			userInfo.setUsername(user.getUsername());
			userInfo.setMobileNo(user.getMobileNo());
			List<String> roleList=new ArrayList<>();
			findUserRoles(user.getUsername(), roleses, roleList);
			userInfo.setRoles(roleList);
			
			if(user.getStaff()!=null) {
				userInfo.setHrId(user.getStaff().getCustomStaffId());	
				
				if(user.getStaff().getDesignation()!= null) {
					userInfo.setDesignation(user.getStaff().getDesignation().getName());	
				}
			}
			
			userInfos.add(userInfo);
			
		}
		
		itemResponse.setItem(userInfos);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
	}
	
	
	@Transactional
    public BaseResponse changeUserPasword(PasswordChangeRequest request){
        
		String userName=UserInfoUtils.getLoggedInUserName();
		
        BaseResponse baseResponse=new BaseResponse();
        Users user=usersRepository.findByUsername(userName);
        if(user==null) {
        	baseResponse.setMessage("User not found");
            baseResponse.setMessageType(0);
            return baseResponse;	
        }       
        
       if(UserInfoUtils.isPreviousPasswordCorrect(request.getCurrentPassword(), user.getPassword())) {
    	String encryptedPassword=  UserInfoUtils.getHashPassword(request.getNewPassword());
    	user.setPassword(encryptedPassword);
    	   
       }else {
    	   baseResponse.setMessage("Current password is not correct.");
           baseResponse.setMessageType(0);
           return baseResponse; 
       }
       
        
        baseResponse.setMessage("User password successfully changed.");
        baseResponse.setMessageType(1);
        return baseResponse;
        
    }
	
	
	public PostLoginViewInfo postLoginViewInfo() {
		
		PostLoginViewInfo postLoginViewInfo=new PostLoginViewInfo();
		String userName=UserInfoUtils.getLoggedInUserName();
		Long instituteId=UserInfoUtils.getLoggedInInstituteId();
		Users user = usersRepository.findByUsername(userName);
		postLoginViewInfo.setUsername(user.getUsername());
		postLoginViewInfo.setNickname(user.getNickName());
		
		if(user.getStaff()!=null) {
			
			postLoginViewInfo.setStaffId(user.getStaff().getStaffId());
			postLoginViewInfo.setStaffName(user.getStaff().getStaffName());	
			postLoginViewInfo.setCustomStaffId(user.getStaff().getCustomStaffId());
			
			if(user.getStaff().getDesignation()!= null) {
				postLoginViewInfo.setDesignation(user.getStaff().getDesignation().getName());	
			}
			
			
			if(user.getStaff().getImageName()!= null) {
				try {
					String userphoto="data:image/png;base64, "+new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STAFF.name(), user.getStaff().getImageName()));
					postLoginViewInfo.setUserPhoto(userphoto);
				}catch(Exception e) {
					
				}	
			}
			
		}
		
		Institute institute=instituteRepository.getOne(instituteId);
		postLoginViewInfo.setInstituteid(institute.getInstituteId()+"");
		postLoginViewInfo.setInstitutename(institute.getInstituteName());
		postLoginViewInfo.setInstituteaddress(institute.getAddress());
		postLoginViewInfo.setCurrentAcYear(institute.getAcademicYear());
		postLoginViewInfo.setPackageId(institute.getPackageId());
		
		if(institute.getEiinNo()!=null) {
		postLoginViewInfo.setEiinno(institute.getEiinNo());
		}
		
		
		
		try {
			String logo="data:image/png;base64, "+new String(imageStorageService.fetchImageInBase64Encode(FileFolder.INSTITUTE.name(), institute.getImageName()));
			postLoginViewInfo.setInstitutelogo(logo);
		}catch(Exception e) {
			postLoginViewInfo.setInstitutelogo("");
		}
		
		return postLoginViewInfo;
	}
	
	
	 
}
