package com.openspace.education.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.user.model.entity.Users;

public interface UsersRepository extends JpaRepository<Users, Long>{
	
	public Users findByUsername(String username);
	
	public Users findByUsernameAndInstitute(String username,Institute institute);
	
	public List<Users> findByInstitute(Institute institute);

}
