package com.openspace.education.user.model.dto;

import java.util.List;

public class UserDetailsDto {
	
	private String username;
	private List<String> userroles;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<String> getUserroles() {
		return userroles;
	}
	public void setUserroles(List<String> userroles) {
		this.userroles = userroles;
	}

	
}
