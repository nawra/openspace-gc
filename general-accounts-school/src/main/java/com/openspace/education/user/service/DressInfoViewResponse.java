package com.openspace.education.user.service;

public class DressInfoViewResponse {

	private Long dressId;
	private String dressDetails;
	private Integer dressSerial;
	private String classRange;
	private String gender;	
	
	private String fileName;
	private byte[] fileContent;
	public Long getDressId() {
		return dressId;
	}
	public void setDressId(Long dressId) {
		this.dressId = dressId;
	}
	public String getDressDetails() {
		return dressDetails;
	}
	public void setDressDetails(String dressDetails) {
		this.dressDetails = dressDetails;
	}
	public Integer getDressSerial() {
		return dressSerial;
	}
	public void setDressSerial(Integer dressSerial) {
		this.dressSerial = dressSerial;
	}
	public String getClassRange() {
		return classRange;
	}
	public void setClassRange(String classRange) {
		this.classRange = classRange;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getFileContent() {
		return fileContent;
	}
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}
	
	
	
}
