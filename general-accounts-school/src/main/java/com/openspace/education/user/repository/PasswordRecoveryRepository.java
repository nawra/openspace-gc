package com.openspace.education.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.user.model.entity.PasswordRecovery;

public interface PasswordRecoveryRepository extends JpaRepository<PasswordRecovery, Long>{
	
	
	public PasswordRecovery findByUserName(String userName);

}
