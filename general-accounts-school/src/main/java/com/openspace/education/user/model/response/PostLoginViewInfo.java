package com.openspace.education.user.model.response;

public class PostLoginViewInfo {
	
	private String username;
	private String nickname;
	private String instituteid;
	private String eiinno="";
	private String institutename;
	private String instituteaddress;
	private String institutelogo;
	private Integer currentAcYear;
	private String userPhoto="";
	
	
	private Long staffId;
	private String customStaffId="";
	private String staffName="";
	private String designation="N/A";
	
	private Integer packageId;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getInstituteid() {
		return instituteid;
	}
	public void setInstituteid(String instituteid) {
		this.instituteid = instituteid;
	}
	public String getInstitutename() {
		return institutename;
	}
	public void setInstitutename(String institutename) {
		this.institutename = institutename;
	}
	public String getInstituteaddress() {
		return instituteaddress;
	}
	public void setInstituteaddress(String instituteaddress) {
		this.instituteaddress = instituteaddress;
	}
	public String getInstitutelogo() {
		return institutelogo;
	}
	public void setInstitutelogo(String institutelogo) {
		this.institutelogo = institutelogo;
	}
	public Integer getCurrentAcYear() {
		return currentAcYear;
	}
	public void setCurrentAcYear(Integer currentAcYear) {
		this.currentAcYear = currentAcYear;
	}
	public String getCustomStaffId() {
		return customStaffId;
	}
	public void setCustomStaffId(String customStaffId) {
		this.customStaffId = customStaffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Long getStaffId() {
		return staffId;
	}
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	public Integer getPackageId() {
		return packageId;
	}
	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}
	public String getEiinno() {
		return eiinno;
	}
	public void setEiinno(String eiinno) {
		this.eiinno = eiinno;
	}
	public String getUserPhoto() {
		return userPhoto;
	}
	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	
	
}
