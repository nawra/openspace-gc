package com.openspace.education.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.openspace.education.user.model.entity.UserRoles;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.repository.UserRolesRepository;
import com.openspace.education.user.repository.UsersRepository;


@Service
public class AuthorizationUserDetailsService implements UserDetailsService{
    
    @Autowired
    public UsersRepository usersRepository;
    
    @Autowired
    public UserRolesRepository userRolesRepository;

    @Override
    public UserDetails loadUserByUsername(String username){
        Users user = usersRepository.findByUsername(username);
       
         if(user != null && user.getInstitute().getInstituteStatus()==0) {
        	 throw new UsernameNotFoundException("Institute is disabled."); 
         }
        
       
        if(user != null) {
                List<String> roles = new ArrayList<>(); 
                List<UserRoles> roleList = userRolesRepository.findByUsername(user.getUsername());
                roleList.stream().forEach((r) -> {
                roles.add(r.getRoleName());
            });
                user.setRoles(roles);
                return user;

            } else {
                throw new UsernameNotFoundException("user not found");
            }
        } 
    }
