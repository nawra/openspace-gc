package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.openspace.education.institute.model.entity.Institute;


@Entity
@Table(name = "money_receipt_template")
public class MoneyReceiptTemplate implements Serializable{
	
	
	
	
	private static final long serialVersionUID = -728685398434535993L;


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "default_id", nullable = false)
	private Integer defaultId;
	
	@OneToOne
	@JoinColumn(name = "institute_id", nullable = false, unique = true)
	private Institute institute;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDefaultId() {
		return defaultId;
	}

	public void setDefaultId(Integer defaultId) {
		this.defaultId = defaultId;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}
	
	
	
	

}
