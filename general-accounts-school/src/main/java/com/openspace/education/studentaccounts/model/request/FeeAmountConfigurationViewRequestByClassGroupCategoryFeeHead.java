package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.NotNull;

public class FeeAmountConfigurationViewRequestByClassGroupCategoryFeeHead {
	
	@NotNull
	private Long classId;
	
	@NotNull
	private Long groupId;
	
	@NotNull
	private Long studentCategoryId;
	
	@NotNull
	private Long feeHeadId;

	
	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getStudentCategoryId() {
		return studentCategoryId;
	}

	public void setStudentCategoryId(Long studentCategoryId) {
		this.studentCategoryId = studentCategoryId;
	}

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	
	
	

}
