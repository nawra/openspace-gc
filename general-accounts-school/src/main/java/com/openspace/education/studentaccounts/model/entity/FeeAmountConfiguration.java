package com.openspace.education.studentaccounts.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name="fee_amount_configuration", uniqueConstraints = @UniqueConstraint(columnNames = { "class_id", "group_id", "student_category_id", "fee_head_id", "fee_subhead_id", "institute_id" }))
public class FeeAmountConfiguration {
	
	@Id
	@Column(name = "config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long configId;
	
	@Column(name = "fee_amount")
	private double feeAmount;
	
	@Column(name = "fine_amount")
	private double fineAmount;
	
	@ManyToOne
	@JoinColumn(name = "class_id", nullable = false)
	private CoreSettingClass classInfo;
	
	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private CoreSettingGroup groupInfo;
	
	@ManyToOne
	@JoinColumn(name = "student_category_id", nullable = false)
	private CoreSettingStudentCategory studentCategory;
	
	@ManyToOne
	@JoinColumn(name = "fee_head_id", nullable = false)
	private CoreSettingFeeHead feeHead;
	
	@ManyToOne
	@JoinColumn(name = "fee_subhead_id", nullable = false)
	private CoreSettingFeeSubHead feeSubHead;
	
	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;
	
	
	
	
	

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	public CoreSettingClass getClassInfo() {
		return classInfo;
	}

	public void setClassInfo(CoreSettingClass classInfo) {
		this.classInfo = classInfo;
	}

	public CoreSettingGroup getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(CoreSettingGroup groupInfo) {
		this.groupInfo = groupInfo;
	}

	public CoreSettingStudentCategory getStudentCategory() {
		return studentCategory;
	}

	public void setStudentCategory(CoreSettingStudentCategory studentCategory) {
		this.studentCategory = studentCategory;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public CoreSettingFeeSubHead getFeeSubHead() {
		return feeSubHead;
	}

	public void setFeeSubHead(CoreSettingFeeSubHead feeSubHead) {
		this.feeSubHead = feeSubHead;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}
	
	
	

}
