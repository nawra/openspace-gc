package com.openspace.education.studentaccounts.model.response;

import java.util.List;


public class MoneyReceiptResponse {
	
	private Long masterId;
	private String invoiceId;
	private String customStudentId;
    private Long studentId;
    private String studentName;
    private String studentRoll;
    private String sectionName;
    private String generatedDate;
    private String paymentDate;
    private String title;
    private String note;
    
    private String feeHeadName;
    private String feeSubHeadName;
    private String payableAmount;
    private String paidAmount;
    private String dueAmount;
    
    
    private List<MoneyReceiptResponse> detailsList;


	public Long getMasterId() {
		return masterId;
	}


	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}


	public String getInvoiceId() {
		return invoiceId;
	}


	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}



	public String getCustomStudentId() {
		return customStudentId;
	}


	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}


	public Long getStudentId() {
		return studentId;
	}


	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}


	public String getStudentName() {
		return studentName;
	}


	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}


	public String getStudentRoll() {
		return studentRoll;
	}


	public void setStudentRoll(String studentRoll) {
		this.studentRoll = studentRoll;
	}


	public String getSectionName() {
		return sectionName;
	}


	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}


	public String getGeneratedDate() {
		return generatedDate;
	}


	public void setGeneratedDate(String generatedDate) {
		this.generatedDate = generatedDate;
	}


	public String getPaymentDate() {
		return paymentDate;
	}


	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getFeeHeadName() {
		return feeHeadName;
	}


	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}


	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}


	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}


	public String getPayableAmount() {
		return payableAmount;
	}


	public void setPayableAmount(String payableAmount) {
		this.payableAmount = payableAmount;
	}


	public String getPaidAmount() {
		return paidAmount;
	}


	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}


	public String getDueAmount() {
		return dueAmount;
	}


	public void setDueAmount(String dueAmount) {
		this.dueAmount = dueAmount;
	}


	public List<MoneyReceiptResponse> getDetailsList() {
		return detailsList;
	}


	public void setDetailsList(List<MoneyReceiptResponse> detailsList) {
		this.detailsList = detailsList;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}
    
    
    
    
    

}
