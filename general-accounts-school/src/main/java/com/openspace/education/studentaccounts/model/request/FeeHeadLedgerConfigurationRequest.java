package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.NotNull;

public class FeeHeadLedgerConfigurationRequest {
	
	@NotNull
	public Long feeHeadId;
	
	@NotNull
	private Long ledgerId;
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public Long getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}
	
	

}
