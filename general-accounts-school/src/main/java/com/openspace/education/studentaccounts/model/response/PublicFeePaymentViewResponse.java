package com.openspace.education.studentaccounts.model.response;

import com.openspace.education.common.ItemResponse;

@SuppressWarnings("rawtypes")
public class PublicFeePaymentViewResponse extends ItemResponse{

	public double serviceCharge;

	public double getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(double serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	
	
	
}
