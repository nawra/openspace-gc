package com.openspace.education.studentaccounts.model.request;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonValueFormat;

public class FeeCollectionManulRequest {
	
	@NotNull
	private Long identificationId;
	
	@NotNull
	private Double totalPayable;
	
	@NotNull
	private Double totalPaid;
	
	private String note;
	
	@NotNull
	private Long paymentLedgerId;
	
	
	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date paymentDate;
	
	private int sendSms=1;
	
	
	@Valid
	private List<FeeCollectionManulRequestHelper1> feeHeadDetails = new ArrayList<>();
	
	@Valid
	private List<FeeCollectionManulRequestHelper2> feeDueDetails = new ArrayList<>();
	
	

	public Long getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}

	public Double getTotalPayable() {
		return totalPayable;
	}

	public void setTotalPayable(Double totalPayable) {
		this.totalPayable = totalPayable;
	}

	public Double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getPaymentLedgerId() {
		return paymentLedgerId;
	}

	public void setPaymentLedgerId(Long paymentLedgerId) {
		this.paymentLedgerId = paymentLedgerId;
	}

	public List<FeeCollectionManulRequestHelper1> getFeeHeadDetails() {
		return feeHeadDetails;
	}

	public void setFeeHeadDetails(List<FeeCollectionManulRequestHelper1> feeHeadDetails) {
		this.feeHeadDetails = feeHeadDetails;
	}

	public List<FeeCollectionManulRequestHelper2> getFeeDueDetails() {
		return feeDueDetails;
	}

	public void setFeeDueDetails(List<FeeCollectionManulRequestHelper2> feeDueDetails) {
		this.feeDueDetails = feeDueDetails;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public int getSendSms() {
		return sendSms;
	}

	public void setSendSms(int sendSms) {
		this.sendSms = sendSms;
	}
	
	
	
	
	
	

}
