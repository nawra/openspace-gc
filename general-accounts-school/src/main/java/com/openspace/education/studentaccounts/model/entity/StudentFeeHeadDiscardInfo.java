package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;





@Entity
@Table(name = "student_fee_head_discard_info", uniqueConstraints = @UniqueConstraint(columnNames = { "fee_head_id","identification_id" }))
public class StudentFeeHeadDiscardInfo implements Serializable{
	
	
	private static final long serialVersionUID = -684547601505503811L;


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@ManyToOne
	@JoinColumn(name = "identification_id", nullable = false)
	private StudentIdentification identification;
	
	@ManyToOne
	@JoinColumn(name = "fee_head_id")
	private CoreSettingFeeHead feeHead;
	
	@ManyToOne
	@JoinColumn(name = "institute_id")
	private Institute institute;
	
	@Column(name = "date_executed")
	private Date dateExecuted;
	
	@Column(name = "user_name")
	private String userName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StudentIdentification getIdentification() {
		return identification;
	}

	public void setIdentification(StudentIdentification identification) {
		this.identification = identification;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	

}
