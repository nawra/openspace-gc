package com.openspace.education.studentaccounts.model.response;



public class FeeHeadAndSubHeadViewHelper {

	 private Long feeSubHeadId;
		
	 private String feeSubHeadName;

	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}

	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}

	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}

	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}
	 
	 
	 
	
}
