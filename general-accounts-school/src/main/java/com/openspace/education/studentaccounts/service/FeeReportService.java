package com.openspace.education.studentaccounts.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdentificationRepository;
import com.openspace.education.studentaccounts.model.entity.FeeAmountConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeInvoice;
import com.openspace.education.studentaccounts.model.entity.FeeInvoiceDetails;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadTimeConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeWaiverConfiguration;
import com.openspace.education.studentaccounts.model.response.DateToDateFeesCollectionView;
import com.openspace.education.studentaccounts.model.response.MoneyReceiptResponse;
import com.openspace.education.studentaccounts.model.response.StudentFeeDueResponse;
import com.openspace.education.studentaccounts.model.response.StudentFeesAllPaymentView;
import com.openspace.education.studentaccounts.model.response.StudentFeesPaymentView;
import com.openspace.education.studentaccounts.repository.FeeAmountConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeInvoiceDetailsRepository;
import com.openspace.education.studentaccounts.repository.FeeInvoiceRepository;
import com.openspace.education.studentaccounts.repository.FeeSubHeadTimeConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeWaiverConfigurationRepository;

@Service
public class FeeReportService {
	
	
	@Autowired
	public StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	public FeeAmountConfigurationRepository feeAmountConfigurationRepository;
	
	@Autowired
	public FeeInvoiceDetailsRepository feeInvoiceDetailsRepository;
	
    @Autowired
	public FeeInvoiceRepository feeInvoiceRepository;
	
	@Autowired
	public FeeWaiverConfigurationRepository feeWaiverConfigurationRepository;
	
	@Autowired
	public FeeSubHeadTimeConfigurationRepository feeSubHeadTimeConfigurationRepository;
	
	@Autowired
	public ClassConfigurationRepository classConfigurationRepository;
	
	
	
	public void setFeeSubHeadTimeConfiguration(List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations,List<FeeAmountConfiguration> tempfeeAmountConfigurations, List<FeeAmountConfiguration> feeAmountConfigurations) {
		
		for(FeeAmountConfiguration cnf: tempfeeAmountConfigurations) {
		
		for(FeeSubHeadTimeConfiguration timeConf : feeSubHeadTimeConfigurations) {
			
				if(timeConf.getFeeHead().equals(cnf.getFeeHead()) && timeConf.getFeeSubHead().equals(cnf.getFeeSubHead())) {
					
					feeAmountConfigurations.add(cnf);
					
					break;
				}
			}
            
		}
	}
	
	public ItemResponse sectionStudentDueDatails(Long classConfigId, Integer academicYear) {
		
		ItemResponse itemResponse = new ItemResponse<>();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> identifications = studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, classConfigId, true);
		
		List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations = feeSubHeadTimeConfigurationRepository.findByInstituteAndFeeActiveDateLessThanEqualAndPayableYearOrderByFeeHead_Serial(institute, new Date(),academicYear);

		List<FeeAmountConfiguration> feeAmountConfigurations = new ArrayList<>();

		List<FeeAmountConfiguration> tempfeeAmountConfigurations = feeAmountConfigurationRepository.findByInstituteOrderByFeeSubHead_Serial(institute);
		
		setFeeSubHeadTimeConfiguration(feeSubHeadTimeConfigurations, tempfeeAmountConfigurations, feeAmountConfigurations);
		
		
		List<FeeInvoiceDetails> feeInvoiceDetails = feeInvoiceDetailsRepository.findByInstituteAndIdentification_ClassConfigurationInfo_IdAndIdentification_AcademicYear(institute, classConfigId, academicYear);
		 
		List<FeeWaiverConfiguration> waiverConfigurations = feeWaiverConfigurationRepository.findByInstituteAndStudentIdentification_ClassConfigurationInfo_IdAndStudentIdentification_AcademicYear(institute, classConfigId, academicYear); 
		
		List<StudentFeeDueResponse> studentFeeDueResponses = new ArrayList<>();
		
		
		
		for(StudentIdentification si : identifications) {
			
			StudentFeeDueResponse response = new StudentFeeDueResponse();
			response.setClassName(si.getClassConfigurationInfo().getClassInfo().getName());
			response.setCustomStudentId(si.getStudentBasic().getCustomStudentId());
			response.setMobileNumber(si.getStudentBasic().getGuardianMobile());
			response.setStudentId(si.getStudentBasic().getStudentId()+"");
			response.setStudentName(si.getStudentBasic().getStudentName());
			response.setStudentRoll(si.getStudentRoll());
			response.setSection(si.getClassConfigurationInfo().getClassInfo().getName()+"-"+si.getClassConfigurationInfo().getShiftInfo().getName()+"-"+si.getClassConfigurationInfo().getSectionInfo().getName());
			response.setStudentGroup(si.getGroupInfo().getName());
			response.setStudentCategory(si.getStudentCategoryInfo().getName());
			response.setIdentificationId(si.getIdentificationId());
			
			boolean flag = false;
			String details = "";
			double totalDue=0;
			
			List<FeeAmountConfiguration> studentFeeAmountConfigurations = singleStudentFeeAmountConfigurations(si, feeAmountConfigurations);
			List<CoreSettingFeeHead> singleFeeHeads=singleStudentFeeHeads(studentFeeAmountConfigurations);
			
			StringBuilder sb = new StringBuilder();
			
			for(CoreSettingFeeHead feeHead : singleFeeHeads) {
				
				double waiverAmount = findWaiverAmount(waiverConfigurations, si, feeHead);
				
				sb.append(" ").append(feeHead.getName()).append(" : ");
				
				List<FeeAmountConfiguration> singleFeeSubHeads = singleStudentFeeSubHeads(studentFeeAmountConfigurations, feeHead);
				
				for(FeeAmountConfiguration sfsh : singleFeeSubHeads) {
					
					if(isNotFeeSubHeadCollected(feeInvoiceDetails, si, feeHead, sfsh.getFeeSubHead())) {
						
						flag=true;
						
						double feeSubheadAmount = sfsh.getFeeAmount();
						
						if(waiverAmount >= feeSubheadAmount) {
							
						}else {
							feeSubheadAmount = feeSubheadAmount-waiverAmount;
						}
						
						sb.append(sfsh.getFeeSubHead().getName()).append("(").append(feeSubheadAmount).append("), ");	
						totalDue+=feeSubheadAmount;
						
					}
					
				}
				
			}
			
			if(flag) {
			  sb.setLength(sb.length() - 2);
              details = details + sb.toString();	
			}
			
			response.setDetails(details);
			response.setTotalDue(totalDue);
			
			studentFeeDueResponses.add(response);
			
		}
		
		
		itemResponse.setItem(studentFeeDueResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		
		return itemResponse;
		
	}
	
	
	public  boolean isBetween(LocalTime candidate, LocalTime start, LocalTime end) {
        return !candidate.isBefore(start) && !candidate.isAfter(end);  
    }
	
	
	public ItemResponse allStudentDueDatails(Integer academicYear) {
		
		ItemResponse itemResponse = new ItemResponse<>();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		
		Date currentDate = new Date();
        
        String time=new SimpleDateFormat("HH:mm").format(currentDate);
        LocalTime localTime = LocalTime.parse(time);
        
        if(isBetween(localTime, LocalTime.of(6, 0), LocalTime.of(11, 0))){
           itemResponse.setMessage("You can see this report after 11:00 am");
           itemResponse.setMessageType(0);
           return itemResponse;
        }
		
		
		
		List<StudentIdentification> identifications = studentIdentificationRepository.findByInstituteAndAcademicYearAndStudentStatusOrderByStudentRollAsc(institute, academicYear,  true);
		
		List<FeeAmountConfiguration> feeAmountConfigurations = feeAmountConfigurationRepository.findByInstituteOrderByFeeSubHead_Serial(institute);
		
		List<FeeInvoiceDetails> feeInvoiceDetails = feeInvoiceDetailsRepository.findByInstituteAndIdentification_AcademicYear(institute,  academicYear);
		 
		List<FeeWaiverConfiguration> waiverConfigurations = feeWaiverConfigurationRepository.findByInstituteAndStudentIdentification_AcademicYear(institute, academicYear); 
		
		List<StudentFeeDueResponse> studentFeeDueResponses = new ArrayList<>();
		
		
		
		for(StudentIdentification si : identifications) {
			
			StudentFeeDueResponse response = new StudentFeeDueResponse();
			response.setClassName(si.getClassConfigurationInfo().getClassInfo().getName());
			response.setCustomStudentId(si.getStudentBasic().getCustomStudentId());
			response.setMobileNumber(si.getStudentBasic().getGuardianMobile());
			response.setStudentId(si.getStudentBasic().getStudentId()+"");
			response.setStudentName(si.getStudentBasic().getStudentName());
			response.setStudentRoll(si.getStudentRoll());
			response.setSection(si.getClassConfigurationInfo().getClassInfo().getName()+"-"+si.getClassConfigurationInfo().getShiftInfo().getName()+"-"+si.getClassConfigurationInfo().getSectionInfo().getName());
			response.setStudentGroup(si.getGroupInfo().getName());
			response.setStudentCategory(si.getStudentCategoryInfo().getName());
			
			boolean flag = false;
			String details = "";
			double totalDue=0;
			
			List<FeeAmountConfiguration> studentFeeAmountConfigurations = singleStudentFeeAmountConfigurations(si, feeAmountConfigurations);
			List<CoreSettingFeeHead> singleFeeHeads=singleStudentFeeHeads(studentFeeAmountConfigurations);
			
			StringBuilder sb = new StringBuilder();
			
			for(CoreSettingFeeHead feeHead : singleFeeHeads) {
				
				double waiverAmount = findWaiverAmount(waiverConfigurations, si, feeHead);
				
				sb.append(" ").append(feeHead.getName()).append(" : ");
				
				List<FeeAmountConfiguration> singleFeeSubHeads = singleStudentFeeSubHeads(studentFeeAmountConfigurations, feeHead);
				
				for(FeeAmountConfiguration sfsh : singleFeeSubHeads) {
					
					if(isNotFeeSubHeadCollected(feeInvoiceDetails, si, feeHead, sfsh.getFeeSubHead())) {
						
						flag=true;
						
						double feeSubheadAmount = sfsh.getFeeAmount();
						
						if(waiverAmount >= feeSubheadAmount) {
							
						}else {
							feeSubheadAmount = feeSubheadAmount-waiverAmount;
						}
						
						sb.append(sfsh.getFeeSubHead().getName()).append("(").append(feeSubheadAmount).append("), ");	
						totalDue+=feeSubheadAmount;
						
					}
					
				}
				
			}
			
			if(flag) {
			  sb.setLength(sb.length() - 2);
              details = details + sb.toString();	
			}
			
			response.setDetails(details);
			response.setTotalDue(totalDue);
			
			studentFeeDueResponses.add(response);
			
		}
		
		
		itemResponse.setItem(studentFeeDueResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		
		return itemResponse;
		
	}


	
	
	public List<FeeAmountConfiguration> singleStudentFeeAmountConfigurations(StudentIdentification si, List<FeeAmountConfiguration> feeAmountConfigurations){
		
		List<FeeAmountConfiguration> amountConfigurations = new ArrayList<>();
		
		Long classId=si.getClassConfigurationInfo().getClassInfo().getId();
		Long groupId = si.getGroupInfo().getId();
		Long studentCategoryId= si.getStudentCategoryInfo().getId();
		
		for(FeeAmountConfiguration cnf : feeAmountConfigurations) {
			
		 if(cnf.getClassInfo().getId().equals(classId)  && cnf.getGroupInfo().getId().equals(groupId) && cnf.getStudentCategory().getId().equals(studentCategoryId)) {
			
			 amountConfigurations.add(cnf); 
		 }
		 
		 
		}
		
		return amountConfigurations;
		
	}
	
	
	public List<CoreSettingFeeHead> singleStudentFeeHeads(List<FeeAmountConfiguration> studentFeeAmountConfigurations){
		
		Map<Long, CoreSettingFeeHead> map = new LinkedHashMap();
		
		for(FeeAmountConfiguration cnf : studentFeeAmountConfigurations) {
			
			map.put(cnf.getFeeHead().getId(), cnf.getFeeHead());
		}
		
		List<CoreSettingFeeHead> feeHeads =new ArrayList<>();
		feeHeads.addAll(map.values());

		return feeHeads;
		
	}
	
	
   
	
	public List<FeeAmountConfiguration> singleStudentFeeSubHeads(List<FeeAmountConfiguration> studentFeeAmountConfigurations, CoreSettingFeeHead feeHead){
		
		List<FeeAmountConfiguration> feeSubHeadAmounts = new ArrayList<>();
		
		for(FeeAmountConfiguration fc : studentFeeAmountConfigurations) {
			
			if(fc.getFeeHead().equals(feeHead)) {
				
				feeSubHeadAmounts.add(fc);
				
			}
		}
		
		return feeSubHeadAmounts;

	}
	
	
	public double findWaiverAmount(List<FeeWaiverConfiguration> waiverConfigurations, StudentIdentification si, CoreSettingFeeHead feeHead) {
		
		double waiverAmount=0;
		
		for(FeeWaiverConfiguration wc : waiverConfigurations) {
			
			if(wc.getFeeHead().equals(feeHead) && wc.getStudentIdentification().equals(si)) {
				waiverAmount=wc.getWaiverAmount();	
				break;
			}
			
		}
		
		return waiverAmount;
	}
	
	
	public boolean isNotFeeSubHeadCollected(List<FeeInvoiceDetails> feeInvoiceDetails, StudentIdentification si, CoreSettingFeeHead feeHead, CoreSettingFeeSubHead feeSubHead) {
		
	    for(FeeInvoiceDetails det : feeInvoiceDetails) {
	    	
	    	if(si.equals(det.getIdentification()) && feeHead.equals(det.getFeeHead()) && feeSubHead.equals(det.getFeeSubHead())) {
	    		
	    	return false;
	    	
	    	}
	    }
		
		return true;
	}
    
	
	
	
	public ItemResponse sectionPaidInvoiceList(Long classConfigId, Date fromDate, Date toDate) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		String stringFromDate=df.format(fromDate);
		String stringToDate=df.format(toDate);
		
		ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(classConfigId, institute);
		
		if(classConfiguration==null) {

			itemResponse.setMessageType(0);
			itemResponse.setMessage("No Class Config Found.");
			
			return itemResponse;
		}
		
		List<Object[]> feeInvoices = feeInvoiceRepository.sectionStudentFeeInvoiceList(institute.getInstituteId(), classConfigId, stringFromDate, stringToDate);
		
		List<StudentFeesPaymentView> views = new ArrayList<>();
		
        for(Object[] obj : feeInvoices) {
			
    	   StudentFeesPaymentView view =new StudentFeesPaymentView();
			view.setMasterId(Long.parseLong(obj[0]+""));
			view.setFeeInvoiceId(obj[1]+"");
			view.setCustomStudentId(obj[2]+"");
			view.setStudentName(obj[3].toString());
			view.setStudentRoll(Integer.parseInt(obj[4]+""));
			view.setFeeHeads(obj[5]+"");
			view.setFeeSubHeads(obj[6]+"");
			view.setPaidAmount(Double.parseDouble(obj[7]+""));
			view.setPaymentStatus(obj[8]+"");
			view.setPaymentDate(obj[9]+"");
			view.setLedgerName(obj[10]+"");
			view.setClassName(classConfiguration.getClassInfo().getName());
			view.setSectionName(classConfiguration.getSectionInfo().getName());
			views.add(view);
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("OK");
		
		return itemResponse;
	}
	
	
	
		public ItemResponse allPaidInvoiceList(List<Long> ledgerIds, Date fromDate, Date toDate) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
		
		List<FeeInvoice> feeInvoices = feeInvoiceRepository.findByInstituteAndPaidStatusAndPaymentDateBetweenAndAccountLedger_LedgerIdInOrderByIdentification_ClassConfigurationInfo_SerialAscIdentification_StudentRoll(institute, 1, fromDate, toDate, ledgerIds);
		
		List<StudentFeesAllPaymentView> views = new ArrayList<>();
		
        for(FeeInvoice obj : feeInvoices) {
			
        	StudentFeesAllPaymentView view =new StudentFeesAllPaymentView();
			
        	view.setMasterId(obj.getMasterId());
			view.setFeeInvoiceId(obj.getInvoiceId());
			view.setCustomStudentId(obj.getIdentification().getStudentBasic().getCustomStudentId());
			view.setStudentName(obj.getIdentification().getStudentBasic().getStudentName());
			view.setStudentRoll(obj.getIdentification().getStudentRoll());

			view.setPaidAmount(obj.getPaidAmount());
			view.setPaymentStatus(obj.getPaidStatus());
			view.setPaymentDate(df.format(obj.getPaymentDate()));
			
			if(obj.getAccountLedger()!=null) {
				view.setLedgerName(obj.getAccountLedger().getLedgerName());	
			}
			
			String section=obj.getIdentification().getClassConfigurationInfo().getClassInfo().getName()+"-"+obj.getIdentification().getClassConfigurationInfo().getShiftInfo().getName()+"-"+obj.getIdentification().getClassConfigurationInfo().getSectionInfo().getName();
			view.setSection(section);
			view.setClassName(obj.getIdentification().getClassConfigurationInfo().getClassInfo().getName());
			view.setSectionName(obj.getIdentification().getClassConfigurationInfo().getSectionInfo().getName());
			
			views.add(view);
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("OK");
		
		return itemResponse;
	}
	
	
	
	
  public ItemResponse dateToDateFeesCollectionVlist(String fromDate, String toDate) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();

		List<Object[]> objects = feeInvoiceDetailsRepository.findDateToDateFeesCollection(fromDate, toDate, institute.getInstituteId());
		
		List<DateToDateFeesCollectionView> views = new ArrayList<>();
		
        for(Object[] obj : objects) {
			
        	DateToDateFeesCollectionView view =new DateToDateFeesCollectionView();
			view.setFeeHeadId(Long.parseLong(obj[0]+""));
			view.setFeeHeadName(obj[1]+"");
			view.setFeeCollection(Double.parseDouble(obj[2]+""));
			view.setFineCollection(Double.parseDouble(obj[3]+""));
			view.setTotalCollection(Double.parseDouble(obj[4]+""));

			views.add(view);
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("OK");
		
		return itemResponse;
	}
	
  
  
  
  
  public ItemResponse invoiceShowByInvoiceId(String invoiceId ){
      
	 ItemResponse response = new ItemResponse();
      
     Institute institute = UserInfoUtils.getLoggedInInstitute();
     
     FeeInvoice invoiceFee =feeInvoiceRepository.findByInstituteAndInvoiceIdAndPaidStatus(institute, invoiceId,1);
      
      if(invoiceFee != null){
          
          MoneyReceiptResponse moneyReceiptResponse = new MoneyReceiptResponse();
          MoneyReceiptResponse details;
          List<MoneyReceiptResponse> detailsList = new ArrayList<>();
          
          StudentIdentification identification=invoiceFee.getIdentification();

          moneyReceiptResponse.setStudentId(identification.getStudentBasic().getStudentId());
          moneyReceiptResponse.setCustomStudentId(identification.getStudentBasic().getCustomStudentId());
          moneyReceiptResponse.setStudentName(identification.getStudentBasic().getStudentName());
          moneyReceiptResponse.setStudentRoll(identification.getStudentRoll()+"");
          String sectionName=identification.getClassConfigurationInfo().getClassInfo().getName()+"-"+identification.getClassConfigurationInfo().getShiftInfo().getName()+"-"+identification.getClassConfigurationInfo().getSectionInfo().getName();
          moneyReceiptResponse.setSectionName(sectionName);
          moneyReceiptResponse.setGeneratedDate(invoiceFee.getCreateDate()+"");
          moneyReceiptResponse.setPaymentDate(invoiceFee.getPaymentDate()+"");
          moneyReceiptResponse.setInvoiceId(invoiceFee.getInvoiceId()+"");
          moneyReceiptResponse.setMasterId(invoiceFee.getMasterId());
          moneyReceiptResponse.setNote(invoiceFee.getNote());
          
          if(invoiceFee.getPaidStatus()==0){
           moneyReceiptResponse.setTitle("Pay-slip");   
          }else{
           moneyReceiptResponse.setTitle("Fee Invoice");  
          }
          
          
          moneyReceiptResponse.setPayableAmount(invoiceFee.getPayableAmount()+"");
          moneyReceiptResponse.setPaidAmount(invoiceFee.getPaidAmount()+"");
          moneyReceiptResponse.setDueAmount(invoiceFee.getDueAmount()+"");
          
          for (FeeInvoiceDetails obj : invoiceFee.getFeeInvoiceDetailes()) {
              
        	  details = new MoneyReceiptResponse();
              details.setFeeHeadName(obj.getFeeHead().getName());
              if(obj.getFeeSubHead()!=null){
                details.setFeeSubHeadName(obj.getFeeSubHead().getName());  
              }else{
                details.setFeeSubHeadName("");  
              }
              
              details.setPayableAmount(obj.getRequiredAmount()+"");
              details.setPaidAmount(obj.getPaidAmount()+"");
              details.setDueAmount(obj.getDueAmount()+"");
              detailsList.add(details);
          }
          
          moneyReceiptResponse.setDetailsList(detailsList);
          
         response.setItem(moneyReceiptResponse);
         response.setMessage("OK");
         response.setMessageType(1);
          
      }
      
      else{
          
         response.setMessage("No Invoice Found");
         response.setMessageType(0);
          
      }
      
      

      return response;
  }
	
	
	
	
}
