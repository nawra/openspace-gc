package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.NotNull;

public class FeeAmountConfigurationUpdateRequest {
	
	@NotNull
	private Long configId;
	private double feeAmount;
	private double fineAmount;
	
	
	public Long getConfigId() {
		return configId;
	}
	public void setConfigId(Long configId) {
		this.configId = configId;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getFineAmount() {
		return fineAmount;
	}
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}
	
	

}
