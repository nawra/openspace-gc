package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;


@Entity
@Table(name="fee_invoice_details", uniqueConstraints=@UniqueConstraint(columnNames = {"institute_id","fee_head_id","fee_sub_head_id","identification_id"}))
public class FeeInvoiceDetails implements Serializable {
	
	
	private static final long serialVersionUID = -37137816460686092L;

	@Id
    @Column(name="details_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long detailsId;
    
    @Column(name="required_amount")
    private double requiredAmount;
    
    @Column(name="due_amount")
    private double dueAmount;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute institute;
    
    @Column(name="paid_amount")
    private double paidAmount;
    
    @Column(name="payable_amount")
    private double payableAmount;
    
    @Column(name="fee_paid")
    private double feePaid;
    
    @Column(name="due_paid")
    private double duePaid;
    
    @Column(name="fine_paid")
    private double finePaid;
    
    @Column(name="fee_waiver_amount")
    private double feeWaiverAmount;
    
    @Column(name="due_waiver_amount")
    private double dueWaiverAmount=0;
    
    @ManyToOne
    @JoinColumn(name="waiver_id")
    private CoreSettingFeeWaiver waiver;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name="fee_head_id",nullable = false)
    private CoreSettingFeeHead feeHead;
    
    @ManyToOne
    @JoinColumn(name="fee_sub_head_id")
    private CoreSettingFeeSubHead feeSubHead;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="identification_id")
    private StudentIdentification identification;
    
    
    @ManyToOne
    @JoinColumn(name="master_id")
    private FeeInvoice feeInvoice;
    
    @Column(name="invoice_id")
    private String invoiceId;
    
    @Column(name="waiver_note")
    private String waiverNote;
    
    @Transient
    private Integer feeSubHeadViewSerial;
    
    @Transient
    private Integer feeHeadViewSerial;

	public Integer getFeeHeadViewSerial() {
		return feeHeadViewSerial;
	}

	public void setFeeHeadViewSerial(Integer feeHeadViewSerial) {
		this.feeHeadViewSerial = feeHeadViewSerial;
	}

	public Integer getFeeSubHeadViewSerial() {
		return feeSubHeadViewSerial;
	}

	public void setFeeSubHeadViewSerial(Integer feeSubHeadViewSerial) {
		this.feeSubHeadViewSerial = feeSubHeadViewSerial;
	}

	public Long getDetailsId() {
		return detailsId;
	}

	public void setDetailsId(Long detailsId) {
		this.detailsId = detailsId;
	}

	public double getRequiredAmount() {
		return requiredAmount;
	}

	public void setRequiredAmount(double requiredAmount) {
		this.requiredAmount = requiredAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public double getFeePaid() {
		return feePaid;
	}

	public void setFeePaid(double feePaid) {
		this.feePaid = feePaid;
	}

	public double getDuePaid() {
		return duePaid;
	}

	public void setDuePaid(double duePaid) {
		this.duePaid = duePaid;
	}

	public double getFinePaid() {
		return finePaid;
	}

	public void setFinePaid(double finePaid) {
		this.finePaid = finePaid;
	}

	public double getFeeWaiverAmount() {
		return feeWaiverAmount;
	}

	public void setFeeWaiverAmount(double feeWaiverAmount) {
		this.feeWaiverAmount = feeWaiverAmount;
	}

	public double getDueWaiverAmount() {
		return dueWaiverAmount;
	}

	public void setDueWaiverAmount(double dueWaiverAmount) {
		this.dueWaiverAmount = dueWaiverAmount;
	}

	public CoreSettingFeeWaiver getWaiver() {
		return waiver;
	}

	public void setWaiver(CoreSettingFeeWaiver waiver) {
		this.waiver = waiver;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public CoreSettingFeeSubHead getFeeSubHead() {
		return feeSubHead;
	}

	public void setFeeSubHead(CoreSettingFeeSubHead feeSubHead) {
		this.feeSubHead = feeSubHead;
	}

	public StudentIdentification getIdentification() {
		return identification;
	}

	public void setIdentification(StudentIdentification identification) {
		this.identification = identification;
	}

	public FeeInvoice getFeeInvoice() {
		return feeInvoice;
	}

	public void setFeeInvoice(FeeInvoice feeInvoice) {
		this.feeInvoice = feeInvoice;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getWaiverNote() {
		return waiverNote;
	}

	public void setWaiverNote(String waiverNote) {
		this.waiverNote = waiverNote;
	}

    
    
}
