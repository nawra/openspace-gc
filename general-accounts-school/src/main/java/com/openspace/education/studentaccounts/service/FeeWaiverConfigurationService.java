package com.openspace.education.studentaccounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeWaiverRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdentificationRepository;
import com.openspace.education.studentaccounts.model.entity.FeeWaiverConfiguration;
import com.openspace.education.studentaccounts.model.request.FeeWaiverConfigurationRequest;
import com.openspace.education.studentaccounts.model.request.FeeWaiverConfigurationUpdateRequest;
import com.openspace.education.studentaccounts.model.response.FeeWaiverConfigurationView;
import com.openspace.education.studentaccounts.repository.FeeWaiverConfigurationRepository;



@Service
public class FeeWaiverConfigurationService {
	
	@Autowired
	public CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	public CoreSettingFeeWaiverRepository coreSettingFeeWaiverRepository;
	
	@Autowired
	public FeeWaiverConfigurationRepository feeWaiverConfigurationRepository;
	
	@Autowired
	public StudentIdentificationRepository studentIdentificationRepository;
	
	
	@Transactional
    public BaseResponse saveFeeWaiverConfiguration(FeeWaiverConfigurationRequest request) {
		
		BaseResponse response = new BaseResponse();
		
		Date date=new Date();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
        CoreSettingFeeHead feeHead = coreSettingFeeHeadRepository.findByIdAndInstitute(request.getFeeHeadId(), institute);
        if(feeHead==null) {
            response.setMessageType(0);
            response.setMessage("Fee Head is not found.");
            return response;
        	
        }
         
        CoreSettingFeeWaiver feeWaiver = coreSettingFeeWaiverRepository.findByIdAndInstitute(request.getFeeWaiverId(), institute);
        if(feeWaiver==null) {
            response.setMessageType(0);
            response.setMessage("Fee Waiver is not found.");
            return response;
        }
        
        
        List<FeeWaiverConfiguration> feeWaiverConfigurations=feeWaiverConfigurationRepository.findByInstituteAndFeeHead_IdAndStudentIdentification_IdentificationIdIn(institute, request.getFeeHeadId(), request.getIdentificationIds());
        feeWaiverConfigurationRepository.delete(feeWaiverConfigurations);
        feeWaiverConfigurationRepository.flush();
        
        List<StudentIdentification> identifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, request.getIdentificationIds());
        
        
        List<FeeWaiverConfiguration> configurations=new ArrayList<>();
        
        for(StudentIdentification si:identifications) {
        	
        	FeeWaiverConfiguration configuration = new FeeWaiverConfiguration();	
        	configuration.setConfigDate(date);
        	configuration.setFeeHead(feeHead);
        	configuration.setFeeWaiver(feeWaiver);
        	configuration.setInstitute(institute);
        	configuration.setStudentIdentification(si);
        	configuration.setWaiverAmount(request.getWaiverAmount());
        	
        	configurations.add(configuration);
        	
        }
        
        
        feeWaiverConfigurationRepository.save(configurations);
        response.setMessageType(1);;
        response.setMessage("Fee Waiver Configuration Successfully Done.");
        return response;
    }
	
	
	
	    @Transactional
	    public BaseResponse updateFeeWaiverConfig(FeeWaiverConfigurationUpdateRequest request) {
	    	
	    	Institute institute=UserInfoUtils.getLoggedInInstitute();
	    	BaseResponse response = new BaseResponse();
	    	
	        CoreSettingFeeWaiver waiver = coreSettingFeeWaiverRepository.findByIdAndInstitute(request.getFeeWaiverId(), institute);
	       
	        if(waiver==null) {
	            response.setMessageType(0);
	            response.setMessage("Fee Waiver is Not Found.");
	            return response;
	        }
	        
	        FeeWaiverConfiguration feeWaiverConfiguration=feeWaiverConfigurationRepository.findByIdAndInstitute(request.getFeeWaiverConfigId(), institute);
	        
	        if(feeWaiverConfiguration==null) {
	        	response.setMessageType(0);
		        response.setMessage("Fee Waiver Configuration is Not Found.");
		        return response;	
	        }
	        
	        
	        feeWaiverConfiguration.setFeeWaiver(waiver);
	        feeWaiverConfiguration.setWaiverAmount(request.getWaiverAmount());
	        
	        response.setMessageType(1);
	        response.setMessage("Fee Waiver Configuration Successfully Updated.");
	        return response;
	    }
	    
	    
	    
	    
	    @Transactional
	    public BaseResponse deleteFeeWaiverConfiguration(Long feeWaiverConfigId) {
	    	
	    	Institute institute=UserInfoUtils.getLoggedInInstitute();
	    	BaseResponse response = new BaseResponse();
	    	
	        
	        FeeWaiverConfiguration feeWaiverConfiguration=feeWaiverConfigurationRepository.findByIdAndInstitute(feeWaiverConfigId, institute);
	        
	        if(feeWaiverConfiguration==null) {
	        	response.setMessageType(0);
		        response.setMessage("Fee Waiver Configuration is Not Found.");
		        return response;	
	        }
	        
	        feeWaiverConfigurationRepository.delete(feeWaiverConfiguration);
	        
	        response.setMessageType(1);
	        response.setMessage("Fee Waiver Configuration Successfully Deleted.");
	        return response;
	    }
	    
	    
	    
	    
	    @SuppressWarnings("rawtypes")
		public ItemResponse feeConfigurationViewList(Long classConfigurationId,Long studentCategoryId,Integer academicYear) {
	    	
	    	ItemResponse itemResponse=new ItemResponse();
	    	
	    	Institute institute=UserInfoUtils.getLoggedInInstitute();
	    	
	    	List<FeeWaiverConfigurationView> waiverConfigurationViews=new ArrayList<>();
	    	
	    	List<FeeWaiverConfiguration> waiverConfigurations = feeWaiverConfigurationRepository.findByInstituteAndStudentIdentification_ClassConfigurationInfo_IdAndStudentIdentification_StudentCategoryInfo_IdAndStudentIdentification_StudentStatusAndStudentIdentification_AcademicYear(institute, classConfigurationId, studentCategoryId, true, academicYear);
	    	
	    	for(FeeWaiverConfiguration fwc:waiverConfigurations) {
	    		
	    		FeeWaiverConfigurationView view = new FeeWaiverConfigurationView();
	    		
	    		view.setCustomStudentId(fwc.getStudentIdentification().getStudentBasic().getCustomStudentId());
	    		view.setFeeName(fwc.getFeeHead().getName());
	    		view.setStudentName(fwc.getStudentIdentification().getStudentBasic().getStudentName());
	    		view.setStudentRoll(fwc.getStudentIdentification().getStudentRoll());
	    		view.setWaiverAmount(fwc.getWaiverAmount());
	    		view.setWaiverConfigId(fwc.getId());
	    		view.setWaiverName(fwc.getFeeWaiver().getName());
	    		view.setWaiverId(fwc.getFeeWaiver().getId());
	    		
	    		waiverConfigurationViews.add(view);
	    	}
	    	
	    	
	    	itemResponse.setItem(waiverConfigurationViews);
	    	itemResponse.setMessage("OK");
	    	itemResponse.setMessageType(1);
	    	return itemResponse;
	    	
	    }

}
