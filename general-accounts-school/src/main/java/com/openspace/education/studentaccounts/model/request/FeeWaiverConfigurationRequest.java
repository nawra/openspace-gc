package com.openspace.education.studentaccounts.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;

public class FeeWaiverConfigurationRequest {
	
	@NotNull
	private Double waiverAmount;
	
	@NotNull
    private Long feeHeadId;
	
	@NotNull
    private Long feeWaiverId;
	
	@NotNull
    private List<Long> identificationIds;
    
    
	public Double getWaiverAmount() {
		return waiverAmount;
	}
	public void setWaiverAmount(Double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public Long getFeeWaiverId() {
		return feeWaiverId;
	}
	public void setFeeWaiverId(Long feeWaiverId) {
		this.feeWaiverId = feeWaiverId;
	}
	public List<Long> getIdentificationIds() {
		return identificationIds;
	}
	public void setIdentificationIds(List<Long> identificationIds) {
		this.identificationIds = identificationIds;
	}
    
    

}
