package com.openspace.education.studentaccounts.model.response;

public class StudentFeesAllPaymentView {
	
	private Long masterId;
	private String feeInvoiceId;
	private String customStudentId;
	private String studentName;
	private Integer studentRoll;
	private String section;
	private Double paidAmount;
	private Integer paymentStatus;
	private String paymentDate;
	private String ledgerName;
	private String className;
	private String sectionName;
	
	
	
	public Long getMasterId() {
		return masterId;
	}
	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}
	public String getFeeInvoiceId() {
		return feeInvoiceId;
	}
	public void setFeeInvoiceId(String feeInvoiceId) {
		this.feeInvoiceId = feeInvoiceId;
	}
	public String getCustomStudentId() {
		return customStudentId;
	}
	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Integer getStudentRoll() {
		return studentRoll;
	}
	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Integer getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(Integer paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getLedgerName() {
		return ledgerName;
	}
	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	
	
	
}
