package com.openspace.education.studentaccounts.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeSubHeadRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadConfiguration;
import com.openspace.education.studentaccounts.model.request.FeeSubHeadConfigurationRequest;
import com.openspace.education.studentaccounts.model.response.FeeSubheadConfigurationResponse;
import com.openspace.education.studentaccounts.repository.FeeSubHeadConfigurationRepository;

@Service
public class FeeSubheadConfigurationService {
	
	@Autowired
	public CoreSettingFeeSubHeadRepository coreSettingFeeSubHeadRepository;

	@Autowired
	public CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	public FeeSubHeadConfigurationRepository feeSubHeadConfigurationRepository;
	
	
	@Transactional
	public BaseResponse saveFeeSubheadConfiguration(FeeSubHeadConfigurationRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		CoreSettingFeeHead feeHead=coreSettingFeeHeadRepository.findByIdAndInstitute(request.getFeeHeadId(), institute);
		if(feeHead==null) {
			baseResponse.setMessage("No Fee Head Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		List<CoreSettingFeeSubHead> feeSubHeads=coreSettingFeeSubHeadRepository.findByInstituteAndIdInOrderBySerialAsc(institute, request.getFeeSubheadIds());
		
		if(feeSubHeads.size()<1) {
			baseResponse.setMessage("No Fee Sub Head Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		List<FeeSubHeadConfiguration> list=feeSubHeadConfigurationRepository.findByInstituteAndFeeHead_IdAndFeeSubhead_IdIn(institute,request.getFeeHeadId(), request.getFeeSubheadIds());
		feeSubHeadConfigurationRepository.delete(list);
		feeSubHeadConfigurationRepository.flush();
		
		List<FeeSubHeadConfiguration> feeSubHeadConfigurations=new ArrayList<>();
		for(CoreSettingFeeSubHead feeSubHead:feeSubHeads) {
			FeeSubHeadConfiguration configuration=new FeeSubHeadConfiguration();
			configuration.setFeeHead(feeHead);
			configuration.setFeeSubhead(feeSubHead);
			configuration.setInstitute(institute);
			feeSubHeadConfigurations.add(configuration);
			
		}
		
		feeSubHeadConfigurationRepository.save(feeSubHeadConfigurations);			
			
		baseResponse.setMessage("Fee SubHead Configuration Successfully saved.");	
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
	 @Transactional
	 public BaseResponse deleteFeeSubheadConfiguration(Long configId) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		FeeSubHeadConfiguration feeSubHeadConfiguration=feeSubHeadConfigurationRepository.findByConfigIdAndInstitute(configId, institute);
		feeSubHeadConfigurationRepository.delete(feeSubHeadConfiguration);
			
		baseResponse.setMessage("Fee SubHead Configuration Successfully deleted.");	
		baseResponse.setMessageType(1);
		return baseResponse;
	 }
	
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse feeSubheadConfigurationList() {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<FeeSubHeadConfiguration> list=feeSubHeadConfigurationRepository.findByInstituteOrderByFeeHead_SerialAscFeeSubhead_SerialAsc(institute);
		
		List<FeeSubheadConfigurationResponse> responses=new ArrayList<>();
		
		for(FeeSubHeadConfiguration conf : list) {
			
			FeeSubheadConfigurationResponse response=new FeeSubheadConfigurationResponse();
			response.setConfigId(conf.getConfigId());
			response.setFeeHeadId(conf.getFeeHead().getId());
			response.setFeeHeadName(conf.getFeeHead().getName());
			response.setFeeSubHeadId(conf.getFeeSubhead().getId());
			response.setFeeSubheadName(conf.getFeeSubhead().getName());
			
			responses.add(response);
		}
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");	
		itemResponse.setMessageType(1);
		return itemResponse;
	}
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse feeSubHeadConfigurationListByFeeHeadId(Long feeHeadId) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<FeeSubHeadConfiguration> list=feeSubHeadConfigurationRepository.findByInstituteAndFeeHead_IdOrderByFeeSubhead_SerialAsc(institute, feeHeadId);
		
		List<FeeSubheadConfigurationResponse> responses=new ArrayList<>();
		
		for(FeeSubHeadConfiguration conf : list) {
			
			FeeSubheadConfigurationResponse response=new FeeSubheadConfigurationResponse();
			response.setConfigId(conf.getConfigId());
			response.setFeeHeadId(conf.getFeeHead().getId());
			response.setFeeHeadName(conf.getFeeHead().getName());
			response.setFeeSubHeadId(conf.getFeeSubhead().getId());
			response.setFeeSubheadName(conf.getFeeSubhead().getName());
			
			responses.add(response);
		}
		
		itemResponse.setItem(responses);
		itemResponse.setMessage("OK");	
		itemResponse.setMessageType(1);
		return itemResponse;
	}

}
