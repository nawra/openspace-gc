package com.openspace.education.studentaccounts.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.generalaccount.repository.AccountTransactionRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeInvoice;
import com.openspace.education.studentaccounts.repository.FeeInvoiceRepository;


@Service
public class FeeInvoiceService {
	
	@Autowired
	public FeeInvoiceRepository feeInvoiceRepository;
	
	
	@Autowired
	public AccountTransactionRepository accountTransactionRepository;
	

	

	
	
	public long findMaxInvoiceId(Long instituteId) {
        Long invoiceId;
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
        String d = dateFormat.format(date);
        int year = Integer.parseInt(d);
        d = (year % 2000) + "";
        d += instituteId;
        Long setInvoice = Long.parseLong(d);
        invoiceId = feeInvoiceRepository.findMaxInvoiceId(year, instituteId);
        if (invoiceId == 0) {
            invoiceId = setInvoice * 10000000;
        }
        return invoiceId;
    }
	
	
	
	@Transactional
	public BaseResponse deleteFeeInvoice(Long masterId) {
		
		 BaseResponse response = new BaseResponse();
	      
	     Institute institute = UserInfoUtils.getLoggedInInstitute();
	     
	     FeeInvoice invoiceFee =feeInvoiceRepository.findByMasterIdAndInstituteAndPaidStatus(masterId, institute, 1);
		
	     if(invoiceFee==null) {
	    	 response.setMessage("No Invoice Found.");
	    	 response.setMessageType(0);
	    	 return response;
	     }
	     
	     if(invoiceFee.getDpsStatus()==1) {
	    	 response.setMessage("This is Online Financial Transaction. You can not delete it");
	    	 response.setMessageType(0);
	    	 return response;	 
	     }
	     
	     AccountTransaction accountTransaction = invoiceFee.getAccountTransaction();
	     
	     feeInvoiceRepository.delete(invoiceFee);
	     accountTransactionRepository.delete(accountTransaction);
	     
	     response.setMessage("Fee Invoice Successfully Deleted.");
    	 response.setMessageType(1);
    	 return response;
	}

}
