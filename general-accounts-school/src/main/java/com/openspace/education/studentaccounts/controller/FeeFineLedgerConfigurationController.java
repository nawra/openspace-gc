package com.openspace.education.studentaccounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.service.FeeFineLedgerConfigurationService;



@Controller
@RequestMapping(value = "/fee-fine/ledger-configuration")
public class FeeFineLedgerConfigurationController {
	
	
	@Autowired
	public  FeeFineLedgerConfigurationService feeFineLedgerConfigurationService;
	
	
	@GetMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeFineLedgerConfiguration(@RequestParam Long feeHeadId,@RequestParam Long ledgerId){
		BaseResponse baseResponse=feeFineLedgerConfigurationService.saveFeeFineLedgerConfiguration(feeHeadId, ledgerId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeFineLedgerConfiguration(@RequestParam Long configId){
		BaseResponse baseResponse=feeFineLedgerConfigurationService.deleteFeeFineLedgerConfiguration(configId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	

	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> feeFineLedgerConfigurationList(){
		ItemResponse itemResponse=feeFineLedgerConfigurationService.feeFineLedgerConfigurationList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	

}
