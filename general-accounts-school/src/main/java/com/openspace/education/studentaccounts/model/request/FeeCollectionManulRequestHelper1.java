package com.openspace.education.studentaccounts.model.request;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FeeCollectionManulRequestHelper1 {

	@NotNull
	private Long feeHeadId;
	
	private String feeHeadName;
	
	@Min(0)
	private double waiverAmount;
	
	private Long waiverId;
	
	@Min(0)
	private double fineAmount;
	
	@Min(1)
	private double feeAmount;
	
	@Min(1)
	private double payableAmount;
	
	@Min(1)
	private double paidAmount;
	
	@NotNull
	@Size(min = 1)
	private List<Long> feeSubHeadIds=new ArrayList<>();
	
	
	
	
	

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}

	public Long getWaiverId() {
		return waiverId;
	}

	public void setWaiverId(Long waiverId) {
		this.waiverId = waiverId;
	}

	public double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public List<Long> getFeeSubHeadIds() {
		return feeSubHeadIds;
	}

	public void setFeeSubHeadIds(List<Long> feeSubHeadIds) {
		this.feeSubHeadIds = feeSubHeadIds;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	
	
	
	
	
}
