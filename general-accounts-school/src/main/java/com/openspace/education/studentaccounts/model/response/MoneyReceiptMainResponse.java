package com.openspace.education.studentaccounts.model.response;

import java.util.List;

public class MoneyReceiptMainResponse {
	
	private String customStudentId;
    private String studentName;
    private String fatherName;
    private String motherName;
    private String gender;
    private String religion;
    private String section;
    private Integer roll;
    private String academicYear;
    private String paymentDate;
    private String createDate;
    private String category;
    private String guardianMobile;
    private String groupName;
    private String username;
    private String barcodeValue;
    private String note;
    private String paidInWord;
    private String invoiceId;
    
    private Double totalPayable;
    private Double totalPaid;
    private Double totalDue;
    
    private List<MoneyReceiptSubResponse> moneyReceiptSubList;

    public String getCustomStudentId() {
        return customStudentId;
    }

    public void setCustomStudentId(String customStudentId) {
        this.customStudentId = customStudentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Integer getRoll() {
        return roll;
    }

    public void setRoll(Integer roll) {
        this.roll = roll;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBarcodeValue() {
        return barcodeValue;
    }

    public void setBarcodeValue(String barcodeValue) {
        this.barcodeValue = barcodeValue;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPaidInWord() {
        return paidInWord;
    }

    public void setPaidInWord(String paidInWord) {
        this.paidInWord = paidInWord;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Double getTotalPayable() {
        return totalPayable;
    }

    public void setTotalPayable(Double totalPayable) {
        this.totalPayable = totalPayable;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public Double getTotalDue() {
        return totalDue;
    }

    public void setTotalDue(Double totalDue) {
        this.totalDue = totalDue;
    }

    public List<MoneyReceiptSubResponse> getMoneyReceiptSubList() {
        return moneyReceiptSubList;
    }

    public void setMoneyReceiptSubList(List<MoneyReceiptSubResponse> moneyReceiptSubList) {
        this.moneyReceiptSubList = moneyReceiptSubList;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGuardianMobile() {
        return guardianMobile;
    }

    public void setGuardianMobile(String guardianMobile) {
        this.guardianMobile = guardianMobile;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
    

}
