package com.openspace.education.studentaccounts.model.response;

import java.util.List;

public class FeeCollectionHeadView {
	
	private Long feeHeadId;
	private String feeHeadName;
	
	
	public FeeCollectionHeadView(Long feeHeadId, String feeHeadName) {
		this.feeHeadId = feeHeadId;
		this.feeHeadName = feeHeadName;
	}
	
	
	private List<FeeCollectionSubHeadView> feeSubHeadDetails;
	
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public String getFeeHeadName() {
		return feeHeadName;
	}
	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	public List<FeeCollectionSubHeadView> getFeeSubHeadDetails() {
		return feeSubHeadDetails;
	}
	public void setFeeSubHeadDetails(List<FeeCollectionSubHeadView> feeSubHeadDetails) {
		this.feeSubHeadDetails = feeSubHeadDetails;
	}
	
	

}
