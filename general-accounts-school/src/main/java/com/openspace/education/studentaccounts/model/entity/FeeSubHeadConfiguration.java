package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "fee_subhead_configuration", uniqueConstraints = @UniqueConstraint(columnNames = { "fee_head_id", "fee_subhead_id","institute_id" }))
public class FeeSubHeadConfiguration implements Serializable{
	
	private static final long serialVersionUID = 2430855515172551542L;

	@Id
	@Column(name = "config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long configId;
	
	@ManyToOne
	@JoinColumn(name = "fee_head_id", nullable = false)
	private CoreSettingFeeHead feeHead;
	
	@ManyToOne
	@JoinColumn(name = "fee_subhead_id", nullable = false)
	private CoreSettingFeeSubHead feeSubhead;
	
	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;
	

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public CoreSettingFeeSubHead getFeeSubhead() {
		return feeSubhead;
	}

	public void setFeeSubhead(CoreSettingFeeSubHead feeSubhead) {
		this.feeSubhead = feeSubhead;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}
	


}
