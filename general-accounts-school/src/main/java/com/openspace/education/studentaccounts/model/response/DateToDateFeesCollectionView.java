package com.openspace.education.studentaccounts.model.response;

public class DateToDateFeesCollectionView {
	
	private Long feeHeadId;
	private String feeHeadName;
	private Double feeCollection;
	private Double fineCollection;
	private Double totalCollection;
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public String getFeeHeadName() {
		return feeHeadName;
	}
	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	public Double getFeeCollection() {
		return feeCollection;
	}
	public void setFeeCollection(Double feeCollection) {
		this.feeCollection = feeCollection;
	}
	public Double getFineCollection() {
		return fineCollection;
	}
	public void setFineCollection(Double fineCollection) {
		this.fineCollection = fineCollection;
	}
	public Double getTotalCollection() {
		return totalCollection;
	}
	public void setTotalCollection(Double totalCollection) {
		this.totalCollection = totalCollection;
	}
	
	

}
