package com.openspace.education.studentaccounts.model.response;

public class FeesCollectionViewHelper2 {
	
	private Long feeSubHeadId;
	private String feeSubHeadName;
	private double feeAmount;
	private double waiverAmount;
	private double fineAmount;
	
	
	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}
	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}
	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}
	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getWaiverAmount() {
		return waiverAmount;
	}
	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	public double getFineAmount() {
		return fineAmount;
	}
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}
	
	

}
