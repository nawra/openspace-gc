package com.openspace.education.studentaccounts.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.studentaccounts.model.entity.FeeInvoiceDetails;

public interface FeeInvoiceDetailsRepository extends JpaRepository<FeeInvoiceDetails, Long>{
	
	public List<FeeInvoiceDetails> findByInstituteAndIdentification_IdentificationId(Institute institute,Long identificationId);
	
	public List<FeeInvoiceDetails> findByInstituteAndIdentificationIn(Institute institute,List<StudentIdentification> identifications);
	
	public List<FeeInvoiceDetails> findByInstituteAndIdentification_ClassConfigurationInfo_IdAndIdentification_AcademicYear(Institute institute, Long identificationId, Integer academicYear);

	public List<FeeInvoiceDetails> findByInstituteAndIdentification_AcademicYear(Institute institute, Integer academicYear);

	// native clause //
	
	@Query(value = "SELECT fh.id, fh.`name`, (sum(det.payable_amount)-(sum(det.due_waiver_amount)+sum(det.paid_amount))) previousduepayable "
		+ " from core_setting_feehead fh,fee_invoice_details det where fh.id=det.fee_head_id and fh.institute_id=det.institute_id "
		+ " and det.identification_id=?1 and det.institute_id=?2 group by fh.id having previousduepayable>0", nativeQuery = true)
	public List<Object[]> findStudentPreviousDueSummary(Long identificationId,Long instituteId);
	
	
	
	@Query(value = "select fh.id feeheadid, fh.`name` feeheadname, (sum(det.fee_paid)+sum(det.due_paid)) feecollection , sum(det.fine_paid) finecollection, "
			+ " (sum(det.fee_paid)+sum(det.due_paid)+sum(det.fine_paid))  totalcollection "
			+ " from fee_invoice inv, fee_invoice_details det, core_setting_feehead fh "
			+ " where inv.master_id=det.master_id and inv.institute_id=det.institute_id and det.fee_head_id=fh.id and det.institute_id=fh.institute_id "
			+ " and inv.paid_status=1 and inv.payment_date between ?1 and ?2 and inv.institute_id=?3 group by fh.id ", nativeQuery = true)
		public List<Object[]> findDateToDateFeesCollection(String fromDate,String toDate, Long instituteId);
	
	
}
