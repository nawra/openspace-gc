package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class FeeCollectionManulRequestHelper2 {
	
	
	@NotNull
	private Long feeHeadId;
	
	@Min(0)
	private double waiver;
	
	@NotNull
	private Long waiverId;
	
	@Min(1)
	private double paidAmount;
	
	@Min(1)
	private double previousDuePayable;
	
	
	private double dueAmount;

	
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public double getWaiver() {
		return waiver;
	}

	public void setWaiver(double waiver) {
		this.waiver = waiver;
	}

	public Long getWaiverId() {
		return waiverId;
	}

	public void setWaiverId(Long waiverId) {
		this.waiverId = waiverId;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public double getPreviousDuePayable() {
		return previousDuePayable;
	}

	public void setPreviousDuePayable(double previousDuePayable) {
		this.previousDuePayable = previousDuePayable;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}
	
	
	
	

}
