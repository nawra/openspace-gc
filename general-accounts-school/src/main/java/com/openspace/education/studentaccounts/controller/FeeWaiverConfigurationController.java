package com.openspace.education.studentaccounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.model.request.FeeWaiverConfigurationRequest;
import com.openspace.education.studentaccounts.model.request.FeeWaiverConfigurationUpdateRequest;
import com.openspace.education.studentaccounts.service.FeeWaiverConfigurationService;

@Controller
@RequestMapping(value = "/fee-waiver/configuration")
public class FeeWaiverConfigurationController {
	
	
	@Autowired
	public FeeWaiverConfigurationService feeWaiverConfigurationService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeWaiverConfiguration(@RequestBody FeeWaiverConfigurationRequest request){
		BaseResponse baseResponse = feeWaiverConfigurationService.saveFeeWaiverConfiguration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/update")
	public ResponseEntity<BaseResponse> updateFeeWaiverConfiguration(@RequestBody FeeWaiverConfigurationUpdateRequest request){
		BaseResponse baseResponse = feeWaiverConfigurationService.updateFeeWaiverConfig(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeWaiverConfiguration(@RequestParam Long feeWaiverConfigId){
		BaseResponse baseResponse = feeWaiverConfigurationService.deleteFeeWaiverConfiguration(feeWaiverConfigId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> feeWaiverConfigurationList(@RequestParam Long classConfigurationId,@RequestParam Long studentCategoryId,@RequestParam Integer academicYear){
		ItemResponse itemResponse = feeWaiverConfigurationService.feeConfigurationViewList(classConfigurationId, studentCategoryId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}

}
