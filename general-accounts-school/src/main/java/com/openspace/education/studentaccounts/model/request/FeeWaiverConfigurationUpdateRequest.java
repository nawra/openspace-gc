package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.NotNull;

public class FeeWaiverConfigurationUpdateRequest {
	
	@NotNull
	public Long feeWaiverConfigId;
	
	@NotNull
	private Long feeWaiverId;
	
	@NotNull
	private Double waiverAmount;
	

	public Long getFeeWaiverConfigId() {
		return feeWaiverConfigId;
	}

	public void setFeeWaiverConfigId(Long feeWaiverConfigId) {
		this.feeWaiverConfigId = feeWaiverConfigId;
	}

	public Long getFeeWaiverId() {
		return feeWaiverId;
	}

	public void setFeeWaiverId(Long feeWaiverId) {
		this.feeWaiverId = feeWaiverId;
	}

	public Double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(Double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	
	
	

}
