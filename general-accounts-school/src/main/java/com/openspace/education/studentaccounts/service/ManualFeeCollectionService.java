package com.openspace.education.studentaccounts.service;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.generalaccount.model.dto.AccountTransactionDetailsDto;
import com.openspace.education.generalaccount.model.dto.AccountTransactionDto;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.generalaccount.model.utils.AccountInfoUtils;
import com.openspace.education.generalaccount.repository.AccountLedgerRepository;
import com.openspace.education.generalaccount.repository.AccountTransactionRepository;
import com.openspace.education.generalaccount.service.GeneralAccountTransactionService;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeSubHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeWaiverRepository;
import com.openspace.education.institute.model.entity.Institute;

import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdentificationRepository;
import com.openspace.education.studentaccounts.model.entity.FeeFineLedgerConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeHeadLedgerConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeInvoice;
import com.openspace.education.studentaccounts.model.entity.FeeInvoiceDetails;
import com.openspace.education.studentaccounts.model.request.FeeCollectionManulRequest;
import com.openspace.education.studentaccounts.model.request.FeeCollectionManulRequestHelper1;
import com.openspace.education.studentaccounts.model.request.FeeCollectionManulRequestHelper2;
import com.openspace.education.studentaccounts.repository.FeeFineLedgerConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeHeadLedgerConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeInvoiceRepository;

@Service
public class ManualFeeCollectionService {
	
	
	@Autowired
	public StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	public CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	public CoreSettingFeeSubHeadRepository coreSettingFeeSubHeadRepository; 
	
	@Autowired
	public CoreSettingFeeWaiverRepository coreSettingFeeWaiverRepository;
	
	@Autowired
	public FeeInvoiceService feeInvoiceService;
	
	@Autowired
	public AccountLedgerRepository accountLedgerRepository;
	
	
	@Autowired
	public GeneralAccountTransactionService generalAccountTransactionService;
	
	
	@Autowired
	public FeeHeadLedgerConfigurationRepository feeHeadLedgerConfigurationRepository;
	
	
	@Autowired
	public FeeFineLedgerConfigurationRepository feeFineLedgerConfigurationRepository;
	
	@Autowired
	public AccountTransactionRepository accountTransactionRepository;
	
	@Autowired
	public FeeInvoiceRepository feeInvoiceRepository;
	
	
	
	@Transactional
	public ItemResponse collectFee(FeeCollectionManulRequest request) {
		
		ItemResponse itemResponse = new ItemResponse();
		itemResponse.setMessageType(1);
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		String userName = UserInfoUtils.getLoggedInUserName();
		
		Date today = new Date();
		
		Date paymentDate = request.getPaymentDate();
		
		StudentIdentification si = studentIdentificationRepository.findByIdentificationIdAndInstitute(request.getIdentificationId(), institute);
		
		if(si == null) {
			
			itemResponse.setMessageType(0);	
			itemResponse.setMessage("No Student Found.");
			return itemResponse;
		}
		
		quickCollectionRequestValidation(request, itemResponse);
		
		if(itemResponse.getMessageType()==0) {

			return itemResponse;	
		}
		
		// --------------------------Start--------------------------//
		 
		Set<Long> feeHeadIds = new LinkedHashSet<>();
		Set<Long> feeSubHeadIds = new LinkedHashSet<>();
		for(FeeCollectionManulRequestHelper1 helper : request.getFeeHeadDetails()) {
			feeHeadIds.add(helper.getFeeHeadId());
			for(Long l: helper.getFeeSubHeadIds()) {
				feeSubHeadIds.add(l);
			}
		}
		
		for(FeeCollectionManulRequestHelper2 helper : request.getFeeDueDetails()) {
			feeHeadIds.add(helper.getFeeHeadId());
		}
		
		// -----------------------------End-------------------------//
		
		double grandtotalPayable = 0;
		double grandtotalPaid = 0;
		
		List<CoreSettingFeeHead> feeHeads = coreSettingFeeHeadRepository.findByInstituteAndIdInOrderBySerialAsc(institute, feeHeadIds); 
		List<CoreSettingFeeSubHead> feeSubHeads = coreSettingFeeSubHeadRepository.findByInstituteAndIdInOrderBySerialAsc(institute, feeSubHeadIds); 
		List<CoreSettingFeeWaiver> feeWaivers = coreSettingFeeWaiverRepository.findByInstituteOrderBySerialAsc(institute);
		
		AccountLedger debitLedger = accountLedgerRepository.findByLedgerIdAndInstitute(request.getPaymentLedgerId(), institute);
        
		if(debitLedger == null) {
			itemResponse.setMessageType(0);	
			itemResponse.setMessage("Debit Ledger Not Found.");
			return itemResponse;
		}
		
		String feeInvoiceId=(feeInvoiceService.findMaxInvoiceId(institute.getInstituteId())+1)+""; 
	    	
		FeeInvoice feeInvoice = new FeeInvoice();
		feeInvoice.setAccountLedger(debitLedger);
		feeInvoice.setCreateDate(today);
		feeInvoice.setDpsStatus(0);
		feeInvoice.setExecutionDate(today);
		feeInvoice.setGenerateType("Manual");
		feeInvoice.setIdentification(si);
		feeInvoice.setInstitute(institute);
		feeInvoice.setInvoiceId(feeInvoiceId);
		feeInvoice.setNote(request.getNote());
		feeInvoice.setOnlineCreated(0);
		feeInvoice.setUsername(userName);
		feeInvoice.setPaymentDate(paymentDate);
	
		Set<FeeInvoiceDetails> feeInvoiceDetailes = new LinkedHashSet<>();
	
		for(CoreSettingFeeHead feeHead : feeHeads) {
			
			for(FeeCollectionManulRequestHelper1 helper1 : request.getFeeHeadDetails()) {
				
				if(feeHead.getId().equals(helper1.getFeeHeadId())) {
					
					Integer feeSubHeadSize = helper1.getFeeSubHeadIds().size();
					
					double payableamount=helper1.getPayableAmount()/feeSubHeadSize;
					double paidamount=helper1.getPaidAmount()/feeSubHeadSize;
					double fineamount=helper1.getFineAmount()/feeSubHeadSize;
					double feePaid = paidamount-fineamount;
					double waiveramount=helper1.getWaiverAmount()/feeSubHeadSize;
					double dueAmount=payableamount-paidamount;
					
					for(Long feeSubHeadId : helper1.getFeeSubHeadIds()) {
						
						CoreSettingFeeSubHead feeSubHead = findFeeSubHead(feeSubHeads, feeSubHeadId, itemResponse);
						if(feeSubHead==null) {
							return itemResponse;
						}
						
						FeeInvoiceDetails invoiceDetails =new FeeInvoiceDetails();
						invoiceDetails.setDueAmount(dueAmount);
						invoiceDetails.setFeeHead(feeHead);
						invoiceDetails.setFeeInvoice(feeInvoice);
						invoiceDetails.setFeePaid(feePaid);
						invoiceDetails.setFeeSubHead(feeSubHead);
						invoiceDetails.setFeeWaiverAmount(waiveramount);
						invoiceDetails.setFinePaid(fineamount);
						invoiceDetails.setIdentification(si);
						invoiceDetails.setInstitute(institute);
						invoiceDetails.setInvoiceId(feeInvoiceId);
						invoiceDetails.setPaidAmount(paidamount);
						invoiceDetails.setPayableAmount(payableamount);
						invoiceDetails.setRequiredAmount(payableamount);
						invoiceDetails.setDueAmount(payableamount-paidamount);
						
						CoreSettingFeeWaiver feeWaiver = findFeeWaiver(feeWaivers, helper1.getWaiverId(), itemResponse);
						
						invoiceDetails.setWaiver(feeWaiver);
						invoiceDetails.setWaiverNote("");
						
						feeInvoiceDetailes.add(invoiceDetails);
						
						grandtotalPayable+=payableamount;
						grandtotalPaid+=paidamount;
					}
					
					
					break;
				}
			}
		}
		 
		
		// ------------ Due Invoice Start ----------------- //
		
         
		for(CoreSettingFeeHead feeHead : feeHeads) {
			
			    for(FeeCollectionManulRequestHelper2 helper2 : request.getFeeDueDetails()) {
			    	
			    	
			    	if(feeHead.getId().equals(helper2.getFeeHeadId())) {
			    		
			    		FeeInvoiceDetails invoiceDetails =new FeeInvoiceDetails();
						invoiceDetails.setDueAmount(0);
						invoiceDetails.setFeeHead(feeHead);
						invoiceDetails.setFeeInvoice(feeInvoice);
						invoiceDetails.setFeePaid(0);
						//invoiceDetails.setFeeSubHead(null);
						//invoiceDetails.setFeeWaiverAmount(0);
						//invoiceDetails.setFinePaid(0);
						invoiceDetails.setIdentification(si);
						invoiceDetails.setInstitute(institute);
						invoiceDetails.setInvoiceId(feeInvoiceId);
						invoiceDetails.setPaidAmount(helper2.getPaidAmount());
						//invoiceDetails.setPayableAmount(0);
						invoiceDetails.setRequiredAmount(helper2.getPreviousDuePayable());
						invoiceDetails.setDuePaid(helper2.getPaidAmount());
						invoiceDetails.setDueWaiverAmount(helper2.getWaiver());
						invoiceDetails.setDueAmount(helper2.getPreviousDuePayable()-helper2.getPaidAmount());
						
						CoreSettingFeeWaiver feeWaiver = findFeeWaiver(feeWaivers, helper2.getWaiverId(), itemResponse);
						
						invoiceDetails.setWaiver(feeWaiver);
						invoiceDetails.setWaiverNote("");
						
						grandtotalPayable+=helper2.getPreviousDuePayable();
						grandtotalPaid+=helper2.getPaidAmount();
						
						feeInvoiceDetailes.add(invoiceDetails);
			    		
			    		
			    	}
				
			    }
			
          }
		
		// -------------Due Invoice End-------------------- //
		
		
		
		
		feeInvoice.setFeeInvoiceDetailes(feeInvoiceDetailes);
		feeInvoice.setPayableAmount(grandtotalPayable);
		feeInvoice.setPaidAmount(grandtotalPaid);
		feeInvoice.setPaidStatus(1);
		feeInvoice.setDueAmount(grandtotalPayable-grandtotalPaid);
		feeInvoice.setDpsStatus(0);
		
		List<FeeHeadLedgerConfiguration> feeHeadLedgerConfigurations = feeHeadLedgerConfigurationRepository.findByInstituteAndFeeHead_IdInOrderByFeeHead_SerialAsc(institute, feeHeadIds);
		List<FeeFineLedgerConfiguration> feeFineLedgerConfigurations = feeFineLedgerConfigurationRepository.findByInstituteAndFeeHead_IdInOrderByFeeHead_SerialAsc(institute, feeHeadIds);

		AccountTransaction accountTransaction= new AccountTransaction();
		
		dataFillInAccountTransactionDetails(accountTransaction, feeInvoiceDetailes, institute, userName, paymentDate, grandtotalPaid, debitLedger.getLedgerId(), feeHeadLedgerConfigurations, feeFineLedgerConfigurations, itemResponse);
		if(itemResponse.getMessageType()==0) {
			return itemResponse;
		}
		
		
		
		accountTransaction=accountTransactionRepository.save(accountTransaction);
		
		feeInvoice.setAccountTransaction(accountTransaction);
		feeInvoiceRepository.save(feeInvoice);
		
		itemResponse.setItem(feeInvoiceId);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("Fee Invoice Successfully Done.");
		
		return itemResponse;
	}
	
	
	
	
	
	
	
	
		public void dataFillInAccountTransactionDetails(AccountTransaction accountTransaction,Set<FeeInvoiceDetails> feeInvoiceDetailes, Institute institute, String username, Date trnDate, Double trnAmount, Long debitLedgerId, List<FeeHeadLedgerConfiguration> feeHeadLedgerConfigurations,List<FeeFineLedgerConfiguration> feeFineLedgerConfigurations, ItemResponse itemResponse) {
			
			AccountTransactionDto accountTransactionDto=new AccountTransactionDto();
	        Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos=new LinkedHashSet<>();
	        Long voucherId=generalAccountTransactionService.findMaxVoucherId(institute.getInstituteId())+1;
	        
	        accountTransactionDto.setModuleId(AccountInfoUtils.STUDENT_ACCOUNTS_MODULE_ID);
	        accountTransactionDto.setTranType(AccountInfoUtils.RECEIPT_TRNSACTION_TYPE);
	        accountTransactionDto.setTrnAmount(trnAmount);
	        accountTransactionDto.setTrnDate(trnDate);
	        accountTransactionDto.setUserName(username);
	        accountTransactionDto.setVoucherId(voucherId);
	        accountTransactionDto.setVoucherNo("");
	        accountTransactionDto.setVoucherNote("Manual Fees Collection");
	        
	        AccountTransactionDetailsDto dto1=new AccountTransactionDetailsDto();
	        dto1.setLedgerId(debitLedgerId);
	        dto1.setDebitAmount(trnAmount);
	        dto1.setCreditAmount(0);
	        accountTransactionDetailsDtos.add(dto1);
	        
	        for(FeeInvoiceDetails det : feeInvoiceDetailes){
	          	          
	          if(det.getFeePaid()>0) {
	        	  
	        	  AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
	        	  
	        	  AccountLedger creditLedger= findFeeHeadAccountLedger(feeHeadLedgerConfigurations, det.getFeeHead().getId());
	        	  if(creditLedger==null) {
	              	itemResponse.setMessageType(0);	
	        		itemResponse.setMessage(det.getFeeHead().getId()+" feeheadid ledger configuration not found");
	        		return ;  
	               }
	        	  dto2.setLedgerId(creditLedger.getLedgerId());
	              dto2.setCreditAmount(det.getFeePaid());
	              
	              accountTransactionDetailsDtos.add(dto2);
	          }
	          
	          if(det.getFinePaid()>0 ) {
	        	  
	        	  AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
	        	  
	        	  AccountLedger creditLedger=  findFeeFineAccountLedger(feeFineLedgerConfigurations, det.getFeeHead().getId());  
	              if(creditLedger==null) {
	            	itemResponse.setMessageType(0);	
	      			itemResponse.setMessage(det.getFeeHead().getId()+" feeheadid for fee fine, ledger configuration not found");
	      			return ;  
	              }
	              
	              dto2.setLedgerId(creditLedger.getLedgerId());
	              dto2.setCreditAmount(det.getFinePaid());
	              
	              accountTransactionDetailsDtos.add(dto2);
	          }
	          
	          if(det.getDuePaid()>0) {
	        	  
	        	  AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
	        	  AccountLedger creditLedger= findFeeHeadAccountLedger(feeHeadLedgerConfigurations, det.getFeeHead().getId());  
	        	  if(creditLedger==null) {
	                itemResponse.setMessageType(0);	
	          		itemResponse.setMessage(det.getFeeHead().getId()+" feeheadid ledger configuration not found");
	          		return ;  
	               }
	        	  
	        	  dto2.setLedgerId(creditLedger.getLedgerId());
	              dto2.setCreditAmount(det.getDuePaid());
	              
	              accountTransactionDetailsDtos.add(dto2);
	        	  
	          }
	          
	        }
	        
	        accountTransactionDto.setAccountTransactionDetailsDtos(accountTransactionDetailsDtos);
	        
	        generalAccountTransactionService.setAccountTransactionVoucher(accountTransactionDto, institute, accountTransaction, itemResponse);
			
		}
	
	
	
		public AccountLedger findFeeHeadAccountLedger(List<FeeHeadLedgerConfiguration> feeHeadLedgerConfigurations, Long feeHeadId) {
			
			for(FeeHeadLedgerConfiguration conf : feeHeadLedgerConfigurations) {
				
				if(conf.getFeeHead().getId().equals(feeHeadId)) {
					
					return conf.getAccountLedger();
				}
			}
			
			return null;
		}
	
	
	  public AccountLedger findFeeFineAccountLedger(List<FeeFineLedgerConfiguration> feeFineLedgerConfigurations, Long feeHeadId) {
			
			for(FeeFineLedgerConfiguration conf : feeFineLedgerConfigurations) {
				
				if(conf.getFeeHead().getId().equals(feeHeadId)) {
					
					return conf.getAccountLedger();
				}
			}
			
			return null;
		}
	
    
  
     public CoreSettingFeeWaiver findFeeWaiver(List<CoreSettingFeeWaiver> feeWaivers, Long feeWaiverId, ItemResponse itemResponse) {
		
		for(CoreSettingFeeWaiver obj : feeWaivers) {
			
			if(obj.getId().equals(feeWaiverId)) {
				
				return obj;
			}
		}
		
		return null;
	 }
	
	
	public CoreSettingFeeSubHead findFeeSubHead(List<CoreSettingFeeSubHead> feeSubHeads, Long feeSubHeadId, ItemResponse itemResponse) {
		
		for(CoreSettingFeeSubHead obj : feeSubHeads) {
			
			if(obj.getId().equals(feeSubHeadId)) {
				
				return obj;
			}
		}
		
		
		itemResponse.setMessageType(0);
		itemResponse.setMessage(" "+feeSubHeadId +" this feesubheadid not found.");
		return null;
	}
	
	
	public void quickCollectionRequestValidation(FeeCollectionManulRequest request,ItemResponse itemResponse){
	      
	      double totalPayable=0;
	      double totalPaid=0;
	  
	      
	      if(request.getPaymentDate().after(new Date())){
	          
	       itemResponse.setMessageType(0);
	       itemResponse.setMessage("Future date is not accepted.");
	       return ;
	          
	      }
	      
	      for(FeeCollectionManulRequestHelper1 helper1 : request.getFeeHeadDetails()){
	      
	       if(helper1.getPaidAmount()<helper1.getFineAmount()){
	                 
	       itemResponse.setMessageType(0);
	       itemResponse.setMessage(helper1.getFeeHeadName()+" Paid Amount is less than Fine Amount");
	       return ;
	      
	      }
	       	     	      
	      if(helper1.getWaiverId()==null){
	         
	         itemResponse.setMessageType(0);
	         itemResponse.setMessage(helper1.getFeeHeadName()+" Waiver Id is null");
	         return ; 
	      
	      }
	      
	      totalPayable+=helper1.getPayableAmount();
	      totalPaid+=helper1.getPaidAmount();

	     }
	      
	      for(FeeCollectionManulRequestHelper2 helper2 : request.getFeeDueDetails()){
	    	 
	    	  totalPayable+=helper2.getPreviousDuePayable();
		      totalPaid+=helper2.getPaidAmount();
	    	  
	      }
	      
	      if(request.getTotalPayable()!=totalPayable){
	          itemResponse.setMessageType(0);
	          itemResponse.setMessage(" Master Payable and Details Payable does not match");
	          return;  
	      }
	      
	      if(request.getTotalPaid()!=totalPaid){
	        
	          itemResponse.setMessageType(0);
	          itemResponse.setMessage(" Master Paid and Details Paid does not match");
	          return;  
	      }
	      
	      itemResponse.setMessageType(1);
	      itemResponse.setMessage(" OK");
	            
	   } 
	
	
	

}
