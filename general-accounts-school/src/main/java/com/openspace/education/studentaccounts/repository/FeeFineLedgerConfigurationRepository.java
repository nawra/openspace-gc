package com.openspace.education.studentaccounts.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeFineLedgerConfiguration;


public interface FeeFineLedgerConfigurationRepository extends JpaRepository<FeeFineLedgerConfiguration, Long>{
	
	public FeeFineLedgerConfiguration findByFeeHead_IdAndInstitute(Long feeHeadId,Institute institute);
	
	public FeeFineLedgerConfiguration findByConfigIdAndInstitute(Long configId,Institute institute);
	
    public List<FeeFineLedgerConfiguration> findByInstituteOrderByFeeHead_SerialAsc(Institute institute);
	
	public List<FeeFineLedgerConfiguration> findByInstituteAndFeeHead_IdInOrderByFeeHead_SerialAsc(Institute institute,Set<Long> feeHeadIds);

}
