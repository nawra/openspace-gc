package com.openspace.education.studentaccounts.model.response;

public class FeeHeadLedgerConfigurationView {
	
	private Long configId;
	
	private String feeHeadName;
	
	private String ledgerName;
	

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
	
	
	
	

}
