package com.openspace.education.studentaccounts.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.initialsetup.repository.CoreSettingClassRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingFeeSubHeadRepository;
import com.openspace.education.initialsetup.repository.CoreSettingGroupRepository;
import com.openspace.education.initialsetup.repository.CoreSettingStudentCategoryRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeAmountConfiguration;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationRequest;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationRequestHelper;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationUpdateRequest;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationViewRequestByClassGroupCategoryFeeHead;
import com.openspace.education.studentaccounts.model.response.FeeAmountConfigurationView;
import com.openspace.education.studentaccounts.model.response.FeeHeadAndSubHeadView;
import com.openspace.education.studentaccounts.model.response.FeeHeadAndSubHeadViewHelper;
import com.openspace.education.studentaccounts.repository.FeeAmountConfigurationRepository;

@Service
public class FeeAmountConfigurationService {
	
	@Autowired
	public CoreSettingClassRepository coreSettingClassRepository;
	
	@Autowired
	public CoreSettingGroupRepository coreSettingGroupRepository;
	
	@Autowired
	public CoreSettingStudentCategoryRepository coreSettingStudentCategoryRepository;
	
	@Autowired
	public CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	public FeeAmountConfigurationRepository feeAmountConfigurationRepository;
	
	
	@Autowired
	public CoreSettingFeeSubHeadRepository coreSettingFeeSubHeadRepository;
	
	
	@Transactional
	public BaseResponse saveFeeAmountConfiguration(FeeAmountConfigurationRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		CoreSettingClass classInfo=coreSettingClassRepository.findByIdAndInstitute(request.getClassId(), institute);
		
		CoreSettingGroup groupInfo=coreSettingGroupRepository.findByIdAndInstitute(request.getGroupId(), institute);
		
		CoreSettingStudentCategory studentCategory=coreSettingStudentCategoryRepository.findByIdAndInstitute(request.getStudentCategoryId(), institute);
		
		CoreSettingFeeHead feeHead=coreSettingFeeHeadRepository.findByIdAndInstitute(request.getFeeHeadId(), institute);
		
		Set<Long> feeSubHeadIds=new LinkedHashSet<>();
		
		for(FeeAmountConfigurationRequestHelper helper:request.getFeeSubHeadDetails()) {
			feeSubHeadIds.add(helper.getFeeSubHeadId());
		}
		
		List<CoreSettingFeeSubHead> subHeads=coreSettingFeeSubHeadRepository.findByInstituteAndIdInOrderBySerialAsc(institute, feeSubHeadIds);
		
		
		if(classInfo==null || groupInfo==null || feeHead==null || studentCategory==null || subHeads.size()<1) {
			baseResponse.setMessage("Core Setting Not Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		
		List<FeeAmountConfiguration> configurations=feeAmountConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_IdAndFeeSubHead_IdIn(institute, classInfo.getId(), groupInfo.getId(), studentCategory.getId(), feeHead.getId(), feeSubHeadIds);
		
		if(configurations.size()>0) {
			feeAmountConfigurationRepository.delete(configurations);	
			feeAmountConfigurationRepository.flush();
		}
		
		List<FeeAmountConfiguration> feeAmountConfigurations=new ArrayList<>();
		
		
		for(FeeAmountConfigurationRequestHelper helper:request.getFeeSubHeadDetails()) {
			
			for(CoreSettingFeeSubHead feeSubHead:subHeads) {
				
				if(helper.getFeeSubHeadId().equals(feeSubHead.getId())) {
					
				FeeAmountConfiguration feeAmountConfiguration=new FeeAmountConfiguration();
				feeAmountConfiguration.setClassInfo(classInfo);
				feeAmountConfiguration.setFeeAmount(helper.getFeeAmount());
				feeAmountConfiguration.setFeeHead(feeHead);
				feeAmountConfiguration.setFeeSubHead(feeSubHead);
				feeAmountConfiguration.setFineAmount(helper.getFineAmount());
				feeAmountConfiguration.setGroupInfo(groupInfo);
				feeAmountConfiguration.setInstitute(institute);
				feeAmountConfiguration.setStudentCategory(studentCategory);
				feeAmountConfigurations.add(feeAmountConfiguration);
				
				}
			 }
			
		   }
		
		feeAmountConfigurationRepository.save(feeAmountConfigurations);
		
		baseResponse.setMessage("Fee Amount Configuration Successfully Saved");	
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	

	@Transactional
	public BaseResponse updateFeeAmountConfiguration(FeeAmountConfigurationUpdateRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		FeeAmountConfiguration configuration=feeAmountConfigurationRepository.findByConfigIdAndInstitute(request.getConfigId(), institute);

		if(configuration==null) {
			baseResponse.setMessage("Configuration Not Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		configuration.setFeeAmount(request.getFeeAmount());
		configuration.setFineAmount(request.getFineAmount());
		
		baseResponse.setMessage("Fee Amount Configuration Successfully Updated");	
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
	@Transactional
	public BaseResponse deleteFeeAmountConfiguration(Long feeConfigId) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		FeeAmountConfiguration configuration=feeAmountConfigurationRepository.findByConfigIdAndInstitute(feeConfigId, institute);

		if(configuration==null) {
			baseResponse.setMessage("Configuration Not Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		feeAmountConfigurationRepository.delete(configuration);
		
		baseResponse.setMessage("Fee Amount Configuration Successfully Deleted.");	
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
	
	
	
	
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public ItemResponse feeAmountConfigurationListByClassGroupCategoryFeeHead(FeeAmountConfigurationViewRequestByClassGroupCategoryFeeHead request) {
		
	    ItemResponse itemResponse=new ItemResponse();
		
	    Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		
		List<FeeAmountConfiguration> configurations=feeAmountConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_Id(institute, request.getClassId(), request.getGroupId(), request.getStudentCategoryId(), request.getFeeHeadId());

		List<FeeAmountConfigurationView> views=new ArrayList<>();
		
		
		for(FeeAmountConfiguration cnf:configurations) {
			
			FeeAmountConfigurationView view =new FeeAmountConfigurationView();
			
			view.setClassId(cnf.getClassInfo().getId());
			view.setClassName(cnf.getClassInfo().getName());
			view.setConfigId(cnf.getConfigId());
			view.setFeeAmount(cnf.getFeeAmount());
			view.setFineAmount(cnf.getFineAmount());
			view.setFeeHeadId(cnf.getFeeHead().getId());
			view.setFeeHeadName(cnf.getFeeHead().getName());
			view.setFeeSubHeadId(cnf.getFeeSubHead().getId());
			view.setFeeSubHeadName(cnf.getFeeSubHead().getName());
			view.setGroupId(cnf.getGroupInfo().getId());
			view.setGroupName(cnf.getGroupInfo().getName());
			view.setStudentCategoryId(cnf.getStudentCategory().getId());
			view.setStudentCategoryName(cnf.getStudentCategory().getName());
			
			views.add(view);
			
		}
			
	    itemResponse.setItem(views);
		itemResponse.setMessage("Fee Amount Configuration Successfully Saved");	
		itemResponse.setMessageType(1);
		return itemResponse;
	}
  
  
    public ItemResponse findFeeHeadSubHeadViewList(Long classId) {
    	
    	ItemResponse itemResponse = new ItemResponse();
    	
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	
    	CoreSettingClass classInfo = coreSettingClassRepository.findByIdAndInstitute(classId, institute);
    	
    	if(classInfo==null) {
    		itemResponse.setMessage("No Class Info");	
    		itemResponse.setMessageType(0);
    		return itemResponse;	
    	}
    	
    	List<FeeAmountConfiguration> feeAmountConfigurations =feeAmountConfigurationRepository.findByInstituteAndClassInfoOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(institute, classInfo);	
    	
    	Set<CoreSettingFeeHead>  feeHeads = new LinkedHashSet<>();
    	
    	for(FeeAmountConfiguration cnf : feeAmountConfigurations) {
    		
    		feeHeads.add(cnf.getFeeHead());
    	}
    	
    	List<FeeHeadAndSubHeadView> views = new ArrayList<>();
    	
    	for(CoreSettingFeeHead head : feeHeads) {
    		
    		FeeHeadAndSubHeadView view = new FeeHeadAndSubHeadView();
    		view.setFeeHeadId(head.getId());
    		view.setFeeHeadName(head.getName());
    		
    		Map<Long,FeeHeadAndSubHeadViewHelper> map = new HashMap<>();
    		
    		for(FeeAmountConfiguration cnf : feeAmountConfigurations) {
        		
        		if(cnf.getFeeHead().equals(head)) {
        		
        			FeeHeadAndSubHeadViewHelper helper = new FeeHeadAndSubHeadViewHelper();
        			helper.setFeeSubHeadId(cnf.getFeeSubHead().getId());
        			helper.setFeeSubHeadName(cnf.getFeeSubHead().getName());
        			map.put(cnf.getFeeSubHead().getId(),helper);
        			
        		}
        	 }
    		
    		
    		  view.setFeeSubHeadList(map.values().stream().collect(Collectors.toList()));
    		  views.add(view);
    		
    	   }
    	
    	  itemResponse.setItem(views);
    	  itemResponse.setMessageType(1);
    	  itemResponse.setMessage("OK");
    	  return itemResponse;
    	
    }
	
}
