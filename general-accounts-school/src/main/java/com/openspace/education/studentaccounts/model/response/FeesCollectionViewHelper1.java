package com.openspace.education.studentaccounts.model.response;

import java.util.List;

import com.openspace.education.initialsetup.model.response.CoreSettingsResponse;

public class FeesCollectionViewHelper1 {
	
	private Long feeHeadId;
	private String feeHeadName;
	private double waiverAmount;
	private Long feeWaiverId;
	private List<CoreSettingsResponse> feeWaiverList;
	private double feeAmount;
	private double fineAmount;
	private double payableAmount;
	private double paidAmount;
	private double dueAmount;
	private List<FeesCollectionViewHelper2> feeSubHeadDetils;
	
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public String getFeeHeadName() {
		return feeHeadName;
	}
	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	public double getWaiverAmount() {
		return waiverAmount;
	}
	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	public Long getFeeWaiverId() {
		return feeWaiverId;
	}
	public void setFeeWaiverId(Long feeWaiverId) {
		this.feeWaiverId = feeWaiverId;
	}
	public List<CoreSettingsResponse> getFeeWaiverList() {
		return feeWaiverList;
	}
	public void setFeeWaiverList(List<CoreSettingsResponse> feeWaiverList) {
		this.feeWaiverList = feeWaiverList;
	}
	public double getPayableAmount() {
		return payableAmount;
	}
	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public List<FeesCollectionViewHelper2> getFeeSubHeadDetils() {
		return feeSubHeadDetils;
	}
	public void setFeeSubHeadDetils(List<FeesCollectionViewHelper2> feeSubHeadDetils) {
		this.feeSubHeadDetils = feeSubHeadDetils;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getFineAmount() {
		return fineAmount;
	}
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}
	
	
	
	
	

}
