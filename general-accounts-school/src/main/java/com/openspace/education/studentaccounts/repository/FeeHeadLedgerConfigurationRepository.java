package com.openspace.education.studentaccounts.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeHeadLedgerConfiguration;

public interface FeeHeadLedgerConfigurationRepository extends JpaRepository<FeeHeadLedgerConfiguration, Long>{
	
	public FeeHeadLedgerConfiguration findByFeeHead_IdAndInstitute(Long feeHeadId,Institute institute);
	
	public FeeHeadLedgerConfiguration findByConfigIdAndInstitute(Long configid,Institute institute);
	
	public List<FeeHeadLedgerConfiguration> findByInstituteOrderByFeeHead_SerialAsc(Institute institute);
	
	public List<FeeHeadLedgerConfiguration> findByInstituteAndFeeHead_IdInOrderByFeeHead_SerialAsc(Institute institute,Set<Long> feeHeadIds);

}
