package com.openspace.education.studentaccounts.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.service.FeeReportService;


@Controller
@RequestMapping(value = "/student-fee/report")
public class FeeReportController {
	
	
	@Autowired
	public FeeReportService feeReportService;
	
	

	
	
	@GetMapping(value = "/invoice/view/by/invoice-id")
	public ResponseEntity<ItemResponse> invoiceViewByInvoiceId(@RequestParam String invoiceId){
		ItemResponse itemResponse=feeReportService.invoiceShowByInvoiceId(invoiceId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@GetMapping(value = "/section/due-details")
	public ResponseEntity<ItemResponse> sectionStudentFeeDueDetails(@RequestParam Long classConfigId, @RequestParam Integer academicYear){
		ItemResponse itemResponse=feeReportService.sectionStudentDueDatails(classConfigId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	} 
	
	@GetMapping(value = "/whole/institute/due-details")
	public ResponseEntity<ItemResponse> allStudentFeeDueDetails( @RequestParam Integer academicYear){
		ItemResponse itemResponse=feeReportService.allStudentDueDatails(academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/section/paid/list")
	public ResponseEntity<ItemResponse> sectionStudentPaidInvoices(@RequestParam Long classConfigId, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate){
		ItemResponse itemResponse=feeReportService.sectionPaidInvoiceList(classConfigId, fromDate, toDate);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/all/paid/list")
	public ResponseEntity<ItemResponse> allStudentPaidInvoices(@RequestParam List<Long> ledgerIds, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate){
		ItemResponse itemResponse=feeReportService.allPaidInvoiceList(ledgerIds, fromDate, toDate);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	} 
	
	
	@GetMapping(value = "/date-to-date/fee-head/collection")
	public ResponseEntity<ItemResponse> dateToDateFeeHeadCollection(@RequestParam String fromDate, @RequestParam String toDate){
		ItemResponse itemResponse=feeReportService.dateToDateFeesCollectionVlist(fromDate, toDate);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	


}
