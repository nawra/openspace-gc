package com.openspace.education.studentaccounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.model.request.FeeCollectionManulRequest;
import com.openspace.education.studentaccounts.service.FeeCollectionViewService;
import com.openspace.education.studentaccounts.service.ManualFeeCollectionService;

@RestController
@RequestMapping(value = "/fee/collection")
public class FeeCollectionController {
	
	
	@Autowired
	public FeeCollectionViewService feeCollectionViewService;
	
	
	@Autowired
	public ManualFeeCollectionService manualFeeCollectionService;

	
	@GetMapping(value = "/view")
	public ResponseEntity<ItemResponse> feeCollectionView(@RequestParam String customStudentId, @RequestParam Integer academicYear){
		ItemResponse itemResponse=feeCollectionViewService.feeCollectionView(customStudentId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<ItemResponse> collectManualStudentFees(@RequestBody FeeCollectionManulRequest request){
		ItemResponse itemResponse=manualFeeCollectionService.collectFee(request);
		return new ResponseEntity<>(itemResponse,HttpStatus.CREATED);
	}
}
