package com.openspace.education.studentaccounts.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.studentaccounts.model.entity.StudentFeeHeadDiscardInfo;





public interface StudentFeeHeadDiscardInfoRepository extends JpaRepository<StudentFeeHeadDiscardInfo, Long>{
	
	public List<StudentFeeHeadDiscardInfo> findByInstituteAndIdentification(Institute institute, StudentIdentification studentIdentification);
	
	public List<StudentFeeHeadDiscardInfo> findByInstituteAndIdentificationInAndFeeHead(Institute institute, List<StudentIdentification> studentIdentifications, CoreSettingFeeHead feeHead);
	
	public List<StudentFeeHeadDiscardInfo> findByInstituteAndIdentificationIn(Institute institute, List<StudentIdentification> studentIdentifications);

	public List<StudentFeeHeadDiscardInfo> findByInstituteAndIdentification_ClassConfigurationInfoAndIdentification_AcademicYear(Institute institute, ClassConfiguration classConfig, Integer academicYear);

	public List<StudentFeeHeadDiscardInfo> findByInstituteAndIdentification_ClassConfigurationInfo_IdAndIdentification_AcademicYear(Institute institute, Long classConfigId, Integer academicYear);

	
	public StudentFeeHeadDiscardInfo findByInstituteAndId(Institute institute,  Long id);

}
