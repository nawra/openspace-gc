package com.openspace.education.studentaccounts.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.repository.AccountLedgerRepository;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.repository.CoreSettingFeeHeadRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeFineLedgerConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeHeadLedgerConfiguration;
import com.openspace.education.studentaccounts.model.response.FeeHeadLedgerConfigurationView;
import com.openspace.education.studentaccounts.repository.FeeFineLedgerConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeHeadLedgerConfigurationRepository;

@Service
public class FeeFineLedgerConfigurationService {
	
	@Autowired
	public AccountLedgerRepository accountLedgerRepository;
	
	@Autowired
	public CoreSettingFeeHeadRepository coreSettingFeeHeadRepository;
	
	@Autowired
	public FeeFineLedgerConfigurationRepository feeFineLedgerConfigurationRepository;
	

	
	@Transactional
	public BaseResponse saveFeeFineLedgerConfiguration(Long feeHeadId,Long ledgerId) {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		BaseResponse baseResponse=new BaseResponse();
		
		AccountLedger accountLedger=accountLedgerRepository.findByLedgerIdAndInstituteAndAccountCategory_CategoryType(ledgerId, institute, "Income");		
		if(accountLedger==null) {
			baseResponse.setMessage("No Ledger Found.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		CoreSettingFeeHead feeHead=coreSettingFeeHeadRepository.findByIdAndInstitute(feeHeadId, institute);
		
		if(feeHead==null) {
			baseResponse.setMessage("No Fee Head Found.");
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		
		FeeFineLedgerConfiguration configuration=feeFineLedgerConfigurationRepository.findByFeeHead_IdAndInstitute(feeHeadId, institute);
		
		if(configuration!=null) {
			configuration.setAccountLedger(accountLedger);
		}else {
			configuration=new FeeFineLedgerConfiguration();
			configuration.setAccountLedger(accountLedger);
			configuration.setFeeHead(feeHead);
			configuration.setInstitute(institute);
			feeFineLedgerConfigurationRepository.save(configuration);
		}
		
		
		
		baseResponse.setMessage("Fee Fine Ledger Configuration Successfully Saved.");
		baseResponse.setMessageType(1);
		return baseResponse;
	}
	
	
	
	  @SuppressWarnings({ "rawtypes", "unchecked" })
	  public ItemResponse feeFineLedgerConfigurationList() {
			
			Institute institute=UserInfoUtils.getLoggedInInstitute();
			
			ItemResponse itemResponse=new ItemResponse();
	
			List<FeeFineLedgerConfiguration> configurations=feeFineLedgerConfigurationRepository.findByInstituteOrderByFeeHead_SerialAsc(institute);
			
			List<FeeHeadLedgerConfigurationView> views=new ArrayList<>();
			
			for(FeeFineLedgerConfiguration conf : configurations) {
				FeeHeadLedgerConfigurationView view=new FeeHeadLedgerConfigurationView();
				view.setConfigId(conf.getConfigId());
				view.setFeeHeadName(conf.getFeeHead().getName());
				view.setLedgerName(conf.getAccountLedger().getLedgerName());
				views.add(view);
			}
			
			itemResponse.setItem(views);
			itemResponse.setMessage("OK");
			itemResponse.setMessageType(1);
			
			return itemResponse;
		}
	  
	  
	  
	    @Transactional
		public BaseResponse deleteFeeFineLedgerConfiguration(Long configId) {
			
			Institute institute=UserInfoUtils.getLoggedInInstitute();
			
			BaseResponse baseResponse= new BaseResponse();
			
			FeeFineLedgerConfiguration configuration=feeFineLedgerConfigurationRepository.findByConfigIdAndInstitute(configId, institute);
			
		    feeFineLedgerConfigurationRepository.delete(configuration);
						
			baseResponse.setMessage("Fee Fine Ledger Configuration Successfully Deleted.");
			baseResponse.setMessageType(1);
			return baseResponse;
		}
	

}
