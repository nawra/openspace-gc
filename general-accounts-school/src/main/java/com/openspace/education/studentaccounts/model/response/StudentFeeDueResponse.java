package com.openspace.education.studentaccounts.model.response;

public class StudentFeeDueResponse {
	
	    private String customStudentId;
	    private String studentId;
	    private String studentName;
	    private Integer studentRoll;
	    private String section;
	    private String mobileNumber;
	    private String details;
	    private String studentGroup;
	    private String studentCategory;
	    private Long identificationId;

	    private Double totalDue;
	    private String className;
	    
	    
		public String getCustomStudentId() {
			return customStudentId;
		}
		public void setCustomStudentId(String customStudentId) {
			this.customStudentId = customStudentId;
		}
		public String getStudentId() {
			return studentId;
		}
		public void setStudentId(String studentId) {
			this.studentId = studentId;
		}
		public String getStudentName() {
			return studentName;
		}
		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}
		public Integer getStudentRoll() {
			return studentRoll;
		}
		public void setStudentRoll(Integer studentRoll) {
			this.studentRoll = studentRoll;
		}
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public String getMobileNumber() {
			return mobileNumber;
		}
		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}
		public String getDetails() {
			return details;
		}
		public void setDetails(String details) {
			this.details = details;
		}
		
		public Double getTotalDue() {
			return totalDue;
		}
		public void setTotalDue(Double totalDue) {
			this.totalDue = totalDue;
		}
		public String getClassName() {
			return className;
		}
		public void setClassName(String className) {
			this.className = className;
		}
		public String getStudentGroup() {
			return studentGroup;
		}
		public void setStudentGroup(String studentGroup) {
			this.studentGroup = studentGroup;
		}
		public String getStudentCategory() {
			return studentCategory;
		}
		public void setStudentCategory(String studentCategory) {
			this.studentCategory = studentCategory;
		}
		public Long getIdentificationId() {
			return identificationId;
		}
		public void setIdentificationId(Long identificationId) {
			this.identificationId = identificationId;
		}
	    
	    
	    

}
