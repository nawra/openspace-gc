package com.openspace.education.studentaccounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.service.FeeHeadLedgerConfigurationService;

@Controller
@RequestMapping(value = "/fee-head/ledger-configuration")
public class FeeHeadLedgerConfigurationController {
	
	
	@Autowired
	public  FeeHeadLedgerConfigurationService feeHeadLedgerConfigurationService;
	
	
	@GetMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeHeadLedgerConfiguration(@RequestParam Long feeHeadId,@RequestParam Long ledgerId){
		BaseResponse baseResponse=feeHeadLedgerConfigurationService.saveFeeHeadLedgerConfiguration(feeHeadId, ledgerId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeHeadLedgerConfiguration(@RequestParam Long configId){
		BaseResponse baseResponse=feeHeadLedgerConfigurationService.deleteFeeHeadLedgerConfiguration(configId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}

	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> feeHeadLedgerConfigurationList(){
		ItemResponse itemResponse=feeHeadLedgerConfigurationService.feeHeadLedgerConfigurationList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}

}
