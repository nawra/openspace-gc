package com.openspace.education.studentaccounts.model.response;

import java.util.List;

import com.openspace.education.initialsetup.model.response.CoreSettingsResponse;

public class FeesCollectionViewHelper3 {
	
	private Long feeHeadId;
	private String feeHeadName;
	private double previousDuePayable;
	private double temporaryPreviousDuePayable;
	private double waiverAmount;
	private List<CoreSettingsResponse> feeWaiverList;
	private double paidAmount;
	private double dueAmount;
	
	
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public String getFeeHeadName() {
		return feeHeadName;
	}
	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	public double getPreviousDuePayable() {
		return previousDuePayable;
	}
	public void setPreviousDuePayable(double previousDuePayable) {
		this.previousDuePayable = previousDuePayable;
	}
	public double getWaiverAmount() {
		return waiverAmount;
	}
	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	public List<CoreSettingsResponse> getFeeWaiverList() {
		return feeWaiverList;
	}
	public void setFeeWaiverList(List<CoreSettingsResponse> feeWaiverList) {
		this.feeWaiverList = feeWaiverList;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public double getTemporaryPreviousDuePayable() {
		return temporaryPreviousDuePayable;
	}
	public void setTemporaryPreviousDuePayable(double temporaryPreviousDuePayable) {
		this.temporaryPreviousDuePayable = temporaryPreviousDuePayable;
	}
	public double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}
	
	
	
	
	

}
