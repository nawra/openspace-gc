package com.openspace.education.studentaccounts.model.response;

import java.util.ArrayList;
import java.util.List;

public class FeesCollectionView {
	
	private Long identificationId;
	private String customStudentId;
	private String studentName;
	private Integer studentRoll;
	private String section;
	private String studentPhoto;
	private List<FeesCollectionViewHelper1> feeHeadDetails = new ArrayList<>();
	private List<FeesCollectionViewHelper3> previousDueDetails = new ArrayList<>();
	
	
	
	public Long getIdentificationId() {
		return identificationId;
	}
	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}
	public String getCustomStudentId() {
		return customStudentId;
	}
	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Integer getStudentRoll() {
		return studentRoll;
	}
	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<FeesCollectionViewHelper1> getFeeHeadDetails() {
		return feeHeadDetails;
	}
	public void setFeeHeadDetails(List<FeesCollectionViewHelper1> feeHeadDetails) {
		this.feeHeadDetails = feeHeadDetails;
	}
	public List<FeesCollectionViewHelper3> getPreviousDueDetails() {
		return previousDueDetails;
	}
	public void setPreviousDueDetails(List<FeesCollectionViewHelper3> previousDueDetails) {
		this.previousDueDetails = previousDueDetails;
	}
	public String getStudentPhoto() {
		return studentPhoto;
	}
	public void setStudentPhoto(String studentPhoto) {
		this.studentPhoto = studentPhoto;
	}
	
	
	
	

}
