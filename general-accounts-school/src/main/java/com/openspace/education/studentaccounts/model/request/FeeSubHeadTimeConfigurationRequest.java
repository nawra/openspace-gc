package com.openspace.education.studentaccounts.model.request;

import javax.validation.constraints.NotNull;

public class FeeSubHeadTimeConfigurationRequest {
	
	@NotNull
	private Long  feeHeadId;

	@NotNull
	private Long feeSubHeadId;
	
	@NotNull
	private Integer payableYear;
	
	@NotNull
	private String payableMonth;
	
	

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}

	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}

	public Integer getPayableYear() {
		return payableYear;
	}

	public void setPayableYear(Integer payableYear) {
		this.payableYear = payableYear;
	}

	public String getPayableMonth() {
		return payableMonth;
	}

	public void setPayableMonth(String payableMonth) {
		this.payableMonth = payableMonth;
	}
	
	
	
	

	

}
