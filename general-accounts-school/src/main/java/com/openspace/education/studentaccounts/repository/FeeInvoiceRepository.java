package com.openspace.education.studentaccounts.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeInvoice;

public interface FeeInvoiceRepository extends JpaRepository<FeeInvoice, Long>{
	
	@Query(value="select (case when max(cast(invoice_id as unsigned)) is not null then max(cast(invoice_id as unsigned)) else 0 end) from fee_invoice  where  YEAR(create_date)=?1 and institute_id=?2",nativeQuery = true)
    public Long findMaxInvoiceId(int year, Long instituteId);
	
	public List<FeeInvoice> findByInstituteAndPaidStatusAndInvoiceIdInOrderByIdentification_StudentRoll(Institute institute, Integer paidStatus, List<String> invoiceIds);
	
	public List<FeeInvoice> findByIdentification_IdentificationIdAndPaidStatusAndDpsStatus(Long identificationId, Integer paidStatus, Integer dpsStatus);
	
	public List<FeeInvoice> findByInstituteAndIdentification_ClassConfigurationInfo_IdAndPaymentDateBetweenOrderByIdentification_StudentRoll(Institute institute, Long classConfigId, Date fromDate, Date toDate);

	public List<FeeInvoice> findByInstituteAndPaidStatusAndPaymentDateBetweenAndAccountLedger_LedgerIdInOrderByIdentification_ClassConfigurationInfo_SerialAscIdentification_StudentRoll(Institute institute, Integer paidStatus, Date fromDate, Date toDate, List<Long> ledgerIds);

	public FeeInvoice findByInstituteAndInvoiceIdAndPaidStatus(Institute institue,String invoiceId,int paidStatus);
	
	public FeeInvoice findByMasterIdAndInstituteAndPaidStatus(Long masterId,Institute institue,int paidStatus);
	
	@Query(value = "select fi.master_id,fi.invoice_id,sb.custom_student_id,sb.student_name,si.student_roll, group_concat(distinct ' ',fh.name) feeHeads," + 
			" ifnull(group_concat(distinct ' ',fsh.name),' ') feeSubHeads,fi.paid_amount," + 
			" (case when fi.dps_status=1 then 'Online Payment' else 'Manual Payment' end) payment_status,fi.payment_date" + 
			" from fee_invoice fi,core_setting_feehead fh, student_identification si,student_basic sb, fee_invoice_details sid " + 
			" left join core_setting_fee_subhead fsh on sid.fee_sub_head_id=fsh.id and sid.institute_id=fsh.institute_id" + 
			" where fi.master_id=sid.master_id and fi.institute_id=sid.institute_id " + 
			" and sid.fee_head_id=fh.id and sid.institute_id=fh.institute_id " + 
			" and fi.identification_id=si.identification_id and fi.institute_id=si.institute_id" + 
			" and si.student_id=sb.student_id and si.institute_id=sb.institute_id" + 
			" and fi.paid_status=?1 and si.identification_id=?2 group by fi.master_id",nativeQuery = true)
	public List<Object[]> singleStudentFeeInvoiceInfo(Integer paidStatus,Long identificationId);
	
	
	
	@Query(value = "select fi.master_id,fi.invoice_id,sb.custom_student_id,sb.student_name,si.student_roll, group_concat(distinct ' ',fh.name) feeHeads, " +
			 " ifnull(group_concat(distinct ' ',fsh.name),' ') feeSubHeads,fi.paid_amount, "+
			 " (case when fi.dps_status=1 then 'Online Payment' else 'Manual Payment' end) payment_status,fi.payment_date , al.ledger_name"+
			 " from fee_invoice fi,core_setting_feehead fh, student_identification si,student_basic sb,account_ledger al, fee_invoice_details sid "+
			 " left join core_setting_fee_subhead fsh on sid.fee_sub_head_id=fsh.id and sid.institute_id=fsh.institute_id "+
			 " where fi.master_id=sid.master_id and fi.institute_id=sid.institute_id  "+
			 " and sid.fee_head_id=fh.id and sid.institute_id=fh.institute_id  "+
			 " and fi.identification_id=si.identification_id and fi.institute_id=si.institute_id "+
			 " and si.student_id=sb.student_id and si.institute_id=sb.institute_id "+
			 " and fi.ledger_id=al.ledger_id and fi.institute_id = al.institute_id"+
			 " and fi.paid_status=1 and si.institute_id=?1 and si.class_config_id=?2 and fi.payment_date between ?3 and ?4 group by fi.master_id",nativeQuery = true) 
	public List<Object[]> sectionStudentFeeInvoiceList(Long instituteId, Long classConfigId, String fromDate, String toDate);
	
	
}
