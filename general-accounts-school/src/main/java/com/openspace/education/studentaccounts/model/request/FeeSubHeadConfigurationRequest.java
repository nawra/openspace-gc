package com.openspace.education.studentaccounts.model.request;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FeeSubHeadConfigurationRequest {

	@NotNull
	private Long feeHeadId;
	
	@NotNull
	@Size(min = 1)
	private Set<Long> feeSubheadIds;
	
	
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public Set<Long> getFeeSubheadIds() {
		return feeSubheadIds;
	}
	public void setFeeSubheadIds(Set<Long> feeSubheadIds) {
		this.feeSubheadIds = feeSubheadIds;
	}
	
	
	
	
	
	
}
