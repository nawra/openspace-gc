package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;


@Entity
@Table(name = "fee_invoice")
public class FeeInvoice implements Serializable{
	

	private static final long serialVersionUID = 2364761700203945708L;

	@Id
    @Column(name="master_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long masterId;
    
    @NotNull
    @Column(name="invoice_id", unique = true)
    private String invoiceId;
    
    @ManyToOne
    @JoinColumn(name = "institute_id")
    private Institute institute;
    
    @Column(name="payable_amount")
    private double payableAmount;
    
    @Column(name="paid_amount")
    private double paidAmount;
    
    @Column(name="due_amount")
    private double dueAmount;
    
    @Column(name="payment_date")
    @Temporal(TemporalType.DATE)
    private Date paymentDate;
    
    @Column(name="create_date")
    @Temporal(TemporalType.DATE)
    private Date createDate;
     
    @Column(name="paid_status")
    private int paidStatus;
    
    @Column(name="dps_status")
    private Integer dpsStatus = 0;
    
    @Column(name="generate_type")
    private String generateType;
    
    @OneToOne
    @JoinColumn(name = "trn_id", unique = true)
    private AccountTransaction accountTransaction;
    
    @ManyToOne
    @JoinColumn(name="identification_id")
    private StudentIdentification identification;
    
    @Column(name="user_name")
    private String username;
    
    @Column(name="note")
    private String note;
    
    @Column(name="execution_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date executionDate;
    
    @OneToMany(mappedBy ="feeInvoice", cascade = CascadeType.ALL)
    private Set<FeeInvoiceDetails> feeInvoiceDetailes;
    
    @ManyToOne
    @JoinColumn(name="ledger_id")
    private AccountLedger accountLedger;
    
    @Column(name="online_created")
    private Integer onlineCreated=0;
    

	public Long getMasterId() {
		return masterId;
	}

	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public int getPaidStatus() {
		return paidStatus;
	}

	public void setPaidStatus(int paidStatus) {
		this.paidStatus = paidStatus;
	}

	public Integer getDpsStatus() {
		return dpsStatus;
	}

	public void setDpsStatus(Integer dpsStatus) {
		this.dpsStatus = dpsStatus;
	}

	public String getGenerateType() {
		return generateType;
	}

	public void setGenerateType(String generateType) {
		this.generateType = generateType;
	}

	public AccountTransaction getAccountTransaction() {
		return accountTransaction;
	}

	public void setAccountTransaction(AccountTransaction accountTransaction) {
		this.accountTransaction = accountTransaction;
	}

	public StudentIdentification getIdentification() {
		return identification;
	}

	public void setIdentification(StudentIdentification identification) {
		this.identification = identification;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public Set<FeeInvoiceDetails> getFeeInvoiceDetailes() {
		return feeInvoiceDetailes;
	}

	public void setFeeInvoiceDetailes(Set<FeeInvoiceDetails> feeInvoiceDetailes) {
		this.feeInvoiceDetailes = feeInvoiceDetailes;
	}

	public AccountLedger getAccountLedger() {
		return accountLedger;
	}

	public void setAccountLedger(AccountLedger accountLedger) {
		this.accountLedger = accountLedger;
	}

	public Integer getOnlineCreated() {
		return onlineCreated;
	}

	public void setOnlineCreated(Integer onlineCreated) {
		this.onlineCreated = onlineCreated;
	}
    
    

}
