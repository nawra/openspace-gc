package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.institute.model.entity.Institute;


@Entity
@Table(name = "fee_subhead_time_configuration", uniqueConstraints=@UniqueConstraint(columnNames = {"institute_id","fee_head_id","fee_sub_head_id","payable_year"}))
public class FeeSubHeadTimeConfiguration implements Serializable{
	

	private static final long serialVersionUID = 4369697540242006657L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "fee_head_id", nullable = false)
	private CoreSettingFeeHead feeHead;
	
	@ManyToOne
	@JoinColumn(name = "fee_sub_head_id", nullable = false)
	private CoreSettingFeeSubHead feeSubHead;
	
	@Column(name = "payable_year", nullable = false)
	private Integer payableYear;
	
	@Column(name = "payable_month", nullable = false)
	private String payableMonth;
	
	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;
	
	@Column(name = "executed_date")
	private Date executedDate;
	
	
	@Column(name = "fee_active_date")
	@Temporal(TemporalType.DATE)
	private Date feeActiveDate;
	
	
	
	@Column(name = "fine_active_date")
	@Temporal(TemporalType.DATE)
	private Date fineActiveDate;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public CoreSettingFeeSubHead getFeeSubHead() {
		return feeSubHead;
	}

	public void setFeeSubHead(CoreSettingFeeSubHead feeSubHead) {
		this.feeSubHead = feeSubHead;
	}

	public Integer getPayableYear() {
		return payableYear;
	}

	public void setPayableYear(Integer payableYear) {
		this.payableYear = payableYear;
	}

	public String getPayableMonth() {
		return payableMonth;
	}

	public void setPayableMonth(String payableMonth) {
		this.payableMonth = payableMonth;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Date getExecutedDate() {
		return executedDate;
	}

	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}


	public Date getFeeActiveDate() {
		return feeActiveDate;
	}

	public void setFeeActiveDate(Date feeActiveDate) {
		this.feeActiveDate = feeActiveDate;
	}

	public Date getFineActiveDate() {
		return fineActiveDate;
	}

	public void setFineActiveDate(Date fineActiveDate) {
		this.fineActiveDate = fineActiveDate;
	}

	
	
	
	
	
}
