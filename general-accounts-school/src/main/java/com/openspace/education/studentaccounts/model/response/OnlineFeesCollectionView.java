package com.openspace.education.studentaccounts.model.response;

public class OnlineFeesCollectionView {
	
	private Long feeHeadId;
	private String feeHeadName;
	private Long feeSubHeadId;
	private String feeSubHeadName;
	
	
	private Long feeWaiverId;
	private String feeWaiverName;
	
	private double waiverAmount;
	
	private Double feeAmount;
	private Double fineAmount;
	private Double payableAmount;
	
	

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}

	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}

	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}

	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}

	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}

	public Long getFeeWaiverId() {
		return feeWaiverId;
	}

	public void setFeeWaiverId(Long feeWaiverId) {
		this.feeWaiverId = feeWaiverId;
	}

	public String getFeeWaiverName() {
		return feeWaiverName;
	}

	public void setFeeWaiverName(String feeWaiverName) {
		this.feeWaiverName = feeWaiverName;
	}

	public double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}

	public Double getPayableAmount() {
		return payableAmount;
	}

	public void setPayableAmount(Double payableAmount) {
		this.payableAmount = payableAmount;
	}

	public Double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(Double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(Double fineAmount) {
		this.fineAmount = fineAmount;
	}
	
	
	
	
	
	

}
