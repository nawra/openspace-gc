package com.openspace.education.studentaccounts.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.model.request.FeeSubHeadTimeConfigurationRequest;
import com.openspace.education.studentaccounts.service.FeeSubHeadTimeConfigurationService;

@Controller
@RequestMapping(value = "/fee-sub-head/time-configuration")
public class FeeSubHeadTimeConfigurationController {
	
	
	@Autowired
	public FeeSubHeadTimeConfigurationService feeSubHeadTimeConfigurationService;
	
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeSubheadTimeConfiguration(@RequestBody @Valid List<FeeSubHeadTimeConfigurationRequest> requests){
		BaseResponse baseResponse=feeSubHeadTimeConfigurationService.saveFeeSubHeadTimeConfiguration(requests);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeSubheadTimeConfiguration(@RequestParam Long id){
		BaseResponse baseResponse=feeSubHeadTimeConfigurationService.deleteFeeSubHeadTimeConfiguration(id);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> feeSubheadTimeConfigurationList(@RequestParam Integer year, @RequestParam Long feeHeadId){
		ItemResponse itemResponse=feeSubHeadTimeConfigurationService.feeSubHeadTimeConfigurationList(year, feeHeadId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	

}
