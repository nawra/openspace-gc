package com.openspace.education.studentaccounts.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationRequest;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationUpdateRequest;
import com.openspace.education.studentaccounts.model.request.FeeAmountConfigurationViewRequestByClassGroupCategoryFeeHead;
import com.openspace.education.studentaccounts.service.FeeAmountConfigurationService;

@RestController
@RequestMapping(value = "/fee-amount/configuration")
public class FeeAmountConfigurationController {
	
	@Autowired
	public FeeAmountConfigurationService feeAmountConfigurationService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeAmountConfiguration(@RequestBody @Valid FeeAmountConfigurationRequest request){
		BaseResponse baseResponse=feeAmountConfigurationService.saveFeeAmountConfiguration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/update")
	public ResponseEntity<BaseResponse> updateFeeAmountConfiguration(@RequestBody @Valid FeeAmountConfigurationUpdateRequest request){
		BaseResponse baseResponse=feeAmountConfigurationService.updateFeeAmountConfiguration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeAmountConfiguration(@RequestParam Long feeConfigId){
		BaseResponse baseResponse=feeAmountConfigurationService.deleteFeeAmountConfiguration(feeConfigId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PostMapping(value = "/list/by/class-group-category-feehead")
	public ResponseEntity<ItemResponse> feeAmountConfigurationListClassGroupCategoryFeeHead(@RequestBody @Valid FeeAmountConfigurationViewRequestByClassGroupCategoryFeeHead request){
		ItemResponse itemResponse=feeAmountConfigurationService.feeAmountConfigurationListByClassGroupCategoryFeeHead(request);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	
	

}
