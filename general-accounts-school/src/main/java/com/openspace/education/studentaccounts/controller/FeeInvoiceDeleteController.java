package com.openspace.education.studentaccounts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.studentaccounts.service.FeeInvoiceService;

@Controller
@RequestMapping(value = "/fee-invoice")
public class FeeInvoiceDeleteController {
	
	@Autowired
	public FeeInvoiceService feeInvoiceService;
	
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteFeeInvoice(@RequestParam Long masterId){
		BaseResponse baseResponse = feeInvoiceService.deleteFeeInvoice(masterId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	

}
