package com.openspace.education.studentaccounts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.MoneyReceiptTemplate;

public interface MoneyReceiptTemplateRepository extends JpaRepository<MoneyReceiptTemplate, Long>{

	public MoneyReceiptTemplate findByInstitute(Institute institute);
	
}
