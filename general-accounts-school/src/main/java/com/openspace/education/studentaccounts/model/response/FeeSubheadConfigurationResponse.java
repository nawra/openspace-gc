package com.openspace.education.studentaccounts.model.response;

public class FeeSubheadConfigurationResponse {
	
	private Long configId;
	private Long feeHeadId;
	private String feeHeadName;
	private Long feeSubHeadId;
	private String feeSubheadName;
	
	
	public Long getConfigId() {
		return configId;
	}
	public void setConfigId(Long configId) {
		this.configId = configId;
	}
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public String getFeeHeadName() {
		return feeHeadName;
	}
	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}
	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}
	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}
	public String getFeeSubheadName() {
		return feeSubheadName;
	}
	public void setFeeSubheadName(String feeSubheadName) {
		this.feeSubheadName = feeSubheadName;
	}
	
	
	

}
