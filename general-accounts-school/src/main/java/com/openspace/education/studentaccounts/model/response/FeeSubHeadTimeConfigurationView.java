package com.openspace.education.studentaccounts.model.response;



public class FeeSubHeadTimeConfigurationView {

	private Long id;

	private String feeHeadName;

	private String feeSubHeadName;
	
	private Integer payableYear;

	private String payableMonth;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}

	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}

	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}

	public Integer getPayableYear() {
		return payableYear;
	}

	public void setPayableYear(Integer payableYear) {
		this.payableYear = payableYear;
	}

	public String getPayableMonth() {
		return payableMonth;
	}

	public void setPayableMonth(String payableMonth) {
		this.payableMonth = payableMonth;
	}
	
	
	

	
}
