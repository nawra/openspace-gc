package com.openspace.education.studentaccounts.model.response;

public class FeeWaiverConfigurationView {
	
	private Long waiverConfigId;
    private String customStudentId;
    private String studentName;
    private Integer studentRoll;
    private String feeName;
    
    private String waiverName;
    private Long waiverId;

    private Double waiverAmount;

	public Long getWaiverConfigId() {
		return waiverConfigId;
	}

	public void setWaiverConfigId(Long waiverConfigId) {
		this.waiverConfigId = waiverConfigId;
	}

	public String getCustomStudentId() {
		return customStudentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getFeeName() {
		return feeName;
	}

	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}

	public String getWaiverName() {
		return waiverName;
	}

	public void setWaiverName(String waiverName) {
		this.waiverName = waiverName;
	}

	public Long getWaiverId() {
		return waiverId;
	}

	public void setWaiverId(Long waiverId) {
		this.waiverId = waiverId;
	}

	public Double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(Double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
    
    

}
