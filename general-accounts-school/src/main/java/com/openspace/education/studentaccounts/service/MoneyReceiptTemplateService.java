package com.openspace.education.studentaccounts.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.MoneyReceiptTemplate;
import com.openspace.education.studentaccounts.repository.MoneyReceiptTemplateRepository;

@Service
public class MoneyReceiptTemplateService {
	
	
	@Autowired
	public MoneyReceiptTemplateRepository moneyReceiptTemplateRepository;
	
	
	@Transactional
	public BaseResponse saveMoneyReceiptTemplate(Integer templateId) {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		BaseResponse baseResponse= new BaseResponse();
		
		MoneyReceiptTemplate moneyReceiptTemplate = moneyReceiptTemplateRepository.findByInstitute(institute);
		
		if(moneyReceiptTemplate==null) {
			moneyReceiptTemplate=new MoneyReceiptTemplate();
			moneyReceiptTemplate.setDefaultId(templateId);
			moneyReceiptTemplate.setInstitute(institute);
			
			baseResponse.setMessage("Money Receipt Template Successfully Saved.");
			baseResponse.setMessageType(1);
		}else {
			moneyReceiptTemplate.setDefaultId(templateId);	
			baseResponse.setMessage("Money Receipt Template Successfully Updated.");
			baseResponse.setMessageType(1);
		}
		
		return baseResponse;
	}
	
	
	

	@Transactional
	public ItemResponse showMoneyReceiptTemplateId() {
		
		ItemResponse itemResponse= new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		MoneyReceiptTemplate moneyReceiptTemplate = moneyReceiptTemplateRepository.findByInstitute(institute);
		
		if(moneyReceiptTemplate==null) {
			
			moneyReceiptTemplate=new MoneyReceiptTemplate();
			moneyReceiptTemplate.setDefaultId(102);
			moneyReceiptTemplate.setInstitute(institute);
			moneyReceiptTemplateRepository.save(moneyReceiptTemplate);
			
			itemResponse.setMessage("OK");
			itemResponse.setMessageType(1);
			itemResponse.setItem(102);
			
		} else {
			
			itemResponse.setMessage("OK");
			itemResponse.setMessageType(1);
			itemResponse.setItem(moneyReceiptTemplate.getDefaultId());
			
		}
		
		return itemResponse;
	}

}
