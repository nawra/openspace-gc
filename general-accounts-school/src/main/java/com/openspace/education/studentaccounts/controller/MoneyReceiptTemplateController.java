package com.openspace.education.studentaccounts.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.service.MoneyReceiptTemplateService;

@RestController
@RequestMapping(value = "/money-receipt/template")
public class MoneyReceiptTemplateController {
	
	
	@Autowired
	public MoneyReceiptTemplateService moneyReceiptTemplateService;
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveMoneyReceiptTemplate(@RequestParam Integer templateId){
		BaseResponse baseResponse=moneyReceiptTemplateService.saveMoneyReceiptTemplate(templateId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	} 
	
	
	@GetMapping(value = "/id/view")
	public ResponseEntity<ItemResponse> showMoneyReceiptTemplateId(){
		ItemResponse itemResponse=moneyReceiptTemplateService.showMoneyReceiptTemplateId();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}

}
