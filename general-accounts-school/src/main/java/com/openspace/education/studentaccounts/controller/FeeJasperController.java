package com.openspace.education.studentaccounts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.service.MoneyReceiptDownloadService;

@Controller
@RequestMapping(value = "/fee/jasper")
public class FeeJasperController {
	
	
	@Autowired
	public MoneyReceiptDownloadService moneyReceiptDownloadService;
		
	@RequestMapping(value = "/money-receipt/download", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> moneyReceiptDownload(@RequestParam List<String> invoiceIds) {
	  Institute institute=UserInfoUtils.getLoggedInInstitute();
	  return new ResponseEntity<>(moneyReceiptDownloadService.moneyReceiptDownload(invoiceIds,institute), HttpStatus.OK);
	}
	
	
	

}
