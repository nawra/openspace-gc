package com.openspace.education.studentaccounts.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadConfiguration;

public interface FeeSubHeadConfigurationRepository extends JpaRepository<FeeSubHeadConfiguration, Long>{
	
	public FeeSubHeadConfiguration findByConfigIdAndInstitute(Long configId,Institute institute);

	public List<FeeSubHeadConfiguration> findByInstituteAndFeeHead_IdAndFeeSubhead_IdIn(Institute institute,Long feeHeadId,Set<Long> feeSubHeadIds);
	
	public List<FeeSubHeadConfiguration> findByInstituteOrderByFeeHead_SerialAscFeeSubhead_SerialAsc(Institute institute);

	public List<FeeSubHeadConfiguration> findByInstituteAndFeeHead_IdOrderByFeeSubhead_SerialAsc(Institute institute,Long feeHeadId);

}
