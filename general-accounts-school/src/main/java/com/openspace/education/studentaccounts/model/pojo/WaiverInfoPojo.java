package com.openspace.education.studentaccounts.model.pojo;

public class WaiverInfoPojo {
	
	private Long waiverId=null;
	
	private String waiverName="";
	
	private double waiverAmount=0;

	public Long getWaiverId() {
		return waiverId;
	}

	public void setWaiverId(Long waiverId) {
		this.waiverId = waiverId;
	}

	public String getWaiverName() {
		return waiverName;
	}

	public void setWaiverName(String waiverName) {
		this.waiverName = waiverName;
	}

	public double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	
	
	

}
