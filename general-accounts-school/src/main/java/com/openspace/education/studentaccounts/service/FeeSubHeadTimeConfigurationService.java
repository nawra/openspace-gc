package com.openspace.education.studentaccounts.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeSubHead;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadTimeConfiguration;
import com.openspace.education.studentaccounts.model.request.FeeSubHeadTimeConfigurationRequest;
import com.openspace.education.studentaccounts.model.response.FeeSubHeadTimeConfigurationView;
import com.openspace.education.studentaccounts.repository.FeeSubHeadTimeConfigurationRepository;



@Service
public class FeeSubHeadTimeConfigurationService {
	
	
	@Autowired
	public FeeSubHeadTimeConfigurationRepository feeSubHeadTimeConfigurationRepository;
	
	
	@Transactional
	public BaseResponse saveFeeSubHeadTimeConfiguration(List<FeeSubHeadTimeConfigurationRequest> requests) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Date date = new Date();
		
		Set<Long> feeHeadIds=new HashSet<>();
		Set<Long> feeSubHeadIds = new HashSet<>();
		Set<Integer> years = new HashSet<>();
		
		for(FeeSubHeadTimeConfigurationRequest req : requests) {
			
			feeHeadIds.add(req.getFeeHeadId());
			feeSubHeadIds.add(req.getFeeSubHeadId());
			years.add(req.getPayableYear());
			
		}
		
		
		
		List<FeeSubHeadTimeConfiguration> subHeadTimeConfigurations = feeSubHeadTimeConfigurationRepository.findByInstituteAndFeeHead_IdInAndFeeSubHead_IdInAndPayableYearIn(institute, feeHeadIds, feeSubHeadIds, years);
		feeSubHeadTimeConfigurationRepository.delete(subHeadTimeConfigurations);
		feeSubHeadTimeConfigurationRepository.flush();
		
		List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations = new ArrayList<>();
		
		for(FeeSubHeadTimeConfigurationRequest req : requests) {
		
			FeeSubHeadTimeConfiguration configuration = new FeeSubHeadTimeConfiguration();
			configuration.setInstitute(institute);
			configuration.setPayableMonth(req.getPayableMonth());
			configuration.setPayableYear(req.getPayableYear());
			configuration.setExecutedDate(date);
			
			CoreSettingFeeHead feeHead=new CoreSettingFeeHead();
			feeHead.setId(req.getFeeHeadId());
			
			CoreSettingFeeSubHead feeSubHead=new CoreSettingFeeSubHead();
			feeSubHead.setId(req.getFeeSubHeadId());
			
			configuration.setFeeHead(feeHead);
			configuration.setFeeSubHead(feeSubHead);
			
			
			Date[] dateArray=customizeDate(req.getPayableMonth(), req.getPayableYear());
			configuration.setFeeActiveDate(dateArray[0]);
			configuration.setFineActiveDate(dateArray[1]);

			
			feeSubHeadTimeConfigurations.add(configuration);
			
		}
		
		feeSubHeadTimeConfigurationRepository.save(feeSubHeadTimeConfigurations);
		
		
		baseResponse.setMessage("FeeSubHead Time Configuration Successfully Done.");
		baseResponse.setMessageType(1);
		
		return baseResponse;
	}
	
	
	@Transactional
	public BaseResponse deleteFeeSubHeadTimeConfiguration(Long id) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
	
		FeeSubHeadTimeConfiguration feeSubHeadTimeConfiguration = feeSubHeadTimeConfigurationRepository.findByIdAndInstitute(id, institute);
		
		feeSubHeadTimeConfigurationRepository.delete(feeSubHeadTimeConfiguration);
		
		
		baseResponse.setMessage("FeeSubHead Time Configuration Successfully Deleted.");
		baseResponse.setMessageType(1);
		
		return baseResponse;
	}

	
	
	
	public ItemResponse feeSubHeadTimeConfigurationList(Integer year, Long feeHeadId) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
	
		List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations = feeSubHeadTimeConfigurationRepository.findByInstituteAndFeeHead_IdAndPayableYearOrderByFeeHead_Serial(institute, feeHeadId, year);

		List<FeeSubHeadTimeConfigurationView> views = new ArrayList<>();
		
		for(FeeSubHeadTimeConfiguration conf : feeSubHeadTimeConfigurations) {
			
			FeeSubHeadTimeConfigurationView view = new FeeSubHeadTimeConfigurationView();
			view.setFeeHeadName(conf.getFeeHead().getName());
			view.setFeeSubHeadName(conf.getFeeSubHead().getName());
			view.setId(conf.getId());
			view.setPayableMonth(conf.getPayableMonth());
			view.setPayableYear(conf.getPayableYear());
			
			views.add(view);
			
		}
		
		itemResponse.setItem(views);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		
		return itemResponse;
	}
	
	
	
	public static Date[] customizeDate(String month, int year) {

		Date[] dates= new Date[2];
        
		try {

            if (month.equalsIgnoreCase("JANUARY") || month.equalsIgnoreCase("MARCH") ||  
                month.equalsIgnoreCase("MAY") || month.equalsIgnoreCase("JULY") ||
                month.equalsIgnoreCase("AUGUST") || month.equalsIgnoreCase("OCTOBER") ||
                month.equalsIgnoreCase("DECEMBER")) {
                String s1 = "01";
                String s2 = "31";

                String sdate = s1 + "-" + month + "-" + year;
        
                SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
                Date ed = sd.parse(sdate);
                dates[0]=ed;
    
                String endDate = s2 + "-" + month + "-" + year;
    
                SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy");
                Date d = s.parse(endDate);
                dates[1]=d;


            } else if (month.equalsIgnoreCase("APRIL") || month.equalsIgnoreCase("JUNE") ||
                       month.equalsIgnoreCase("SEPTEMBER") || month.equalsIgnoreCase("NOVEMBER")) {
                String s3 = "01";
                String s4 = "30";
                String sdate = s3 + "-" + month + "-" + year;

                SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
                Date ed = sd.parse(sdate);
                dates[0]=ed;

                String endDate = s4 + "-" + month + "-" + year;

                SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy");
                Date d = s.parse(endDate);
                dates[1]=d;

            } else {
                if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {

                    if (month.equalsIgnoreCase("FEBRUARY")) {
                        String s5 = "01";
                        String s6 = "29";
                        String sdate = s5 + "-" + month + "-" + year;
       
                        SimpleDateFormat sd =
                            new SimpleDateFormat("dd-MMM-yyyy");
                        Date ed = sd.parse(sdate);
                        dates[0]=ed;
                        String endDate = s6 + "-" + month + "-" + year;

                        SimpleDateFormat s =
                            new SimpleDateFormat("dd-MMM-yyyy");
                        Date d = s.parse(endDate);
                        dates[1]=d;

                    }
                } else {
      
                    String s7 = "01";
                    String s8 = "28";
                    String sdate = s7 + "-" + month + "-" + year;
                    SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
                    Date ed = sd.parse(sdate);
                    dates[0]=ed;

                    String endDate = s8 + "-" + month + "-" + year;
                    SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy");
                    Date d = s.parse(endDate);
                    dates[1]=d;

                }
            }
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
		
        return dates;
    }

}
