package com.openspace.education.studentaccounts.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.openspace.education.common.ApplicationUtils;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.NumberConverter;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.jasper.utils.JasperUtils;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.studentaccounts.model.entity.FeeInvoice;
import com.openspace.education.studentaccounts.model.entity.FeeInvoiceDetails;
import com.openspace.education.studentaccounts.model.entity.MoneyReceiptTemplate;
import com.openspace.education.studentaccounts.model.response.MoneyReceiptMainResponse;
import com.openspace.education.studentaccounts.model.response.MoneyReceiptSubResponse;
import com.openspace.education.studentaccounts.repository.FeeInvoiceRepository;
import com.openspace.education.studentaccounts.repository.MoneyReceiptTemplateRepository;



@Service
public class MoneyReceiptDownloadService {
	
//	 public static final String ACCOUNTS = "jasper/student-accounts";
	
	@Autowired
	public FeeInvoiceRepository feeInvoiceRepository;
	
	@Autowired
	public JasperUtils jasperUtils;
	
	@Autowired
	public MoneyReceiptTemplateRepository moneyReceiptTemplateRepository;
	
	
	private static final Integer ATTENDANCE_FINE_DEFAULT_ID=100;
	
	
	public ItemResponse moneyReceiptDownload(List<String> invoiceIds, Institute institute) {
       
		List<MoneyReceiptMainResponse> moneyReceiptMainResponses = new ArrayList<>();
		
		 String DU_LOGO_PATH_AND_NAME = ApplicationUtils.getFilePath("DEFAULT")+"du.png";
        
        List<FeeInvoice> invoiceFees = feeInvoiceRepository.findByInstituteAndPaidStatusAndInvoiceIdInOrderByIdentification_StudentRoll(institute, 1, invoiceIds);
        
        MoneyReceiptMainResponse moneyReceiptMainResponse;
        
        for (FeeInvoice invoiceFee : invoiceFees) {
            moneyReceiptMainResponse = new MoneyReceiptMainResponse();
            dataFillInMoneyReceiptMainResponse(moneyReceiptMainResponse, invoiceFee);
            moneyReceiptMainResponses.add(moneyReceiptMainResponse);
        }

        // jasper download
        Map<String, Object> map = new HashMap<>();
        map.put("INSTITUTE_NAME", institute.getInstituteName());
        map.put("INSTITUTE_ADDRESS", institute.getAddress());
        map.put("INSTITUTE_LOGO", institute.getImageName());
        map.put("DU_LOGO", DU_LOGO_PATH_AND_NAME);
        map.put("LOGO_PATH", ApplicationUtils.getFilePath(FileFolder.INSTITUTE.name()));
        map.put("USER_ID", "");
        map.put("USER_NAME", "");
        String template = findTemplateName(institute);
        
        

        try {
        	jasperUtils.jasperPrintWithList(moneyReceiptMainResponses, map, template, "money_receipt");

        } catch (Exception e) {
            e.printStackTrace();
        }

        ItemResponse itemResponse = new ItemResponse();
        itemResponse.setMessage(""+moneyReceiptMainResponses.size()+ " money receipts are downloaded");
        return itemResponse;
    }
	
	
	
	public String findTemplateName(Institute institute) {
		
		String templateName="jasper/student-accounts/money_receipt_template_2.jasper";
		
//		MoneyReceiptTemplate moneyReceiptTemplate = moneyReceiptTemplateRepository.findByInstitute(institute);
//		
//		if(moneyReceiptTemplate==null) {
//			return templateName;
//		}
//		
//		if(moneyReceiptTemplate.getDefaultId().equals(101)) {
//			templateName="jasper/student-accounts/money_receipt_template_1.jasper";
//		}else if(moneyReceiptTemplate.getDefaultId().equals(102)) {
//			templateName="jasper/student-accounts/money_receipt_template_2.jasper";
//		}else if(moneyReceiptTemplate.getDefaultId().equals(103)) {
//			templateName="jasper/student-accounts/money_receipt_template_3.jasper";
//		}else if(moneyReceiptTemplate.getDefaultId().equals(104)) {
//			templateName="jasper/student-accounts/money_receipt_template_4.jasper";
//		}
		
		return templateName;
		
	}
	
	
	

	public void dataFillInMoneyReceiptMainResponseHelper1(MoneyReceiptMainResponse moneyReceiptMainResponse, FeeInvoice invoiceFee) {
        
        moneyReceiptMainResponse.setAcademicYear(invoiceFee.getIdentification().getAcademicYear()+"");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String barcodeValue = "Invoice ID - " + invoiceFee.getInvoiceId() + System.lineSeparator()
                + "Student Name - " + invoiceFee.getIdentification().getStudentBasic().getStudentName() + System.lineSeparator()
                + "Payment Date - " + dateFormat.format(invoiceFee.getPaymentDate()) + System.lineSeparator()
                + "Paid Amount - " + invoiceFee.getPaidAmount();
        String paidInWord = NumberConverter.numberToWord(invoiceFee.getPaidAmount());
        String paymentDate = dateFormat.format(invoiceFee.getPaymentDate());
        String createDate = dateFormat.format(invoiceFee.getCreateDate());
        StudentIdentification identification = invoiceFee.getIdentification();
        String section = identification.getClassConfigurationInfo().getClassInfo().getName() + "-" + identification.getClassConfigurationInfo().getShiftInfo().getName() + "-" + identification.getClassConfigurationInfo().getSectionInfo().getName();
        moneyReceiptMainResponse.setBarcodeValue(barcodeValue);
        moneyReceiptMainResponse.setCustomStudentId(invoiceFee.getIdentification().getStudentBasic().getCustomStudentId());
        moneyReceiptMainResponse.setInvoiceId(invoiceFee.getInvoiceId() + "");
        moneyReceiptMainResponse.setNote((invoiceFee.getNote() == null ? "" : invoiceFee.getNote()));
        moneyReceiptMainResponse.setPaidInWord(barcodeValue);
        moneyReceiptMainResponse.setPaidInWord(paidInWord);
        moneyReceiptMainResponse.setPaymentDate(paymentDate);
        moneyReceiptMainResponse.setRoll(identification.getStudentRoll());
        moneyReceiptMainResponse.setSection(section);

        moneyReceiptMainResponse.setCategory(identification.getStudentCategoryInfo().getName());
        moneyReceiptMainResponse.setCreateDate(createDate);
        moneyReceiptMainResponse.setGuardianMobile(identification.getStudentBasic().getGuardianMobile());
        moneyReceiptMainResponse.setGroupName(identification.getGroupInfo().getName());

        moneyReceiptMainResponse.setStudentName(identification.getStudentBasic().getStudentName());
        moneyReceiptMainResponse.setFatherName(identification.getStudentBasic().getFatherName());
        moneyReceiptMainResponse.setMotherName(identification.getStudentBasic().getMotherName());
        moneyReceiptMainResponse.setGender(identification.getStudentBasic().getStudentGender());
        moneyReceiptMainResponse.setReligion(identification.getStudentBasic().getStudentReligion());
        moneyReceiptMainResponse.setTotalDue(invoiceFee.getDueAmount());
        moneyReceiptMainResponse.setTotalPaid(invoiceFee.getPaidAmount());
        moneyReceiptMainResponse.setTotalPayable(invoiceFee.getPayableAmount());
        moneyReceiptMainResponse.setUsername(invoiceFee.getUsername());
        
    }
    
    public void dataFillInMoneyReceiptMainResponse(MoneyReceiptMainResponse moneyReceiptMainResponse, FeeInvoice invoiceFee) {
        

        dataFillInMoneyReceiptMainResponseHelper1(moneyReceiptMainResponse, invoiceFee);
        
        Set<FeeInvoiceDetails> invoiceFeeDetailsesSet = invoiceFee.getFeeInvoiceDetailes();
        List<FeeInvoiceDetails> invoiceFeeDetailses= new ArrayList<>(invoiceFeeDetailsesSet);
        
        
        setFeeHeadViewSerial(invoiceFeeDetailses);
        
        Set<Long> feeHeadIds=new LinkedHashSet<>();

        invoiceFeeDetailses.stream().forEach((det) -> {
            feeHeadIds.add(det.getFeeHead().getId());
        });
        
        setFeeSubHeadViewSerial(invoiceFeeDetailses);
        
        
        
        List<MoneyReceiptSubResponse> moneyReceiptSubList = new ArrayList<>();
        
        MoneyReceiptSubResponse moneyReceiptSubResponse;
        
        for(Long feeHeadId:feeHeadIds){
            
         String feeHeadName = "";
         String details = "";
         StringBuilder stringBuilder = new StringBuilder();
         double waiver = 0.0;
         double fine = 0.0;
         double payable = 0.0;
         double paid=0.0;
         
         moneyReceiptSubResponse = new MoneyReceiptSubResponse(); 
         
         for(FeeInvoiceDetails det2:invoiceFeeDetailses){
             
             if(feeHeadId.equals(det2.getFeeHead().getId())){
               
              feeHeadName=det2.getFeeHead().getName();
              fine += NumberConverter.round(det2.getFinePaid(), 2);
              payable += det2.getRequiredAmount();
              waiver += det2.getFeeWaiverAmount() + det2.getDueWaiverAmount();
              paid+=det2.getPaidAmount();
              
             if(det2.getFeeSubHead()!=null){
                 
                stringBuilder.append(det2.getFeeSubHead().getName());
                stringBuilder.append(", "); 
                
             } else {
               
               if (Objects.equals(det2.getFeeHead().getDefaultId(), ATTENDANCE_FINE_DEFAULT_ID)) {
                     
                   stringBuilder.append("Attendance Fine, ");
                   
               } else {
                    
                   stringBuilder.append("Previous Due, ");
                  
               }
                 
             }
                 
          }
             
         }
            
           stringBuilder.setLength(stringBuilder.length() - 2);
           details = stringBuilder.toString();
           moneyReceiptSubResponse.setDetails(details);
           moneyReceiptSubResponse.setFeeHeadName(feeHeadName);
           moneyReceiptSubResponse.setFine(NumberConverter.round(fine,2));
           moneyReceiptSubResponse.setPayable(NumberConverter.round(payable, 2));
           moneyReceiptSubResponse.setPaid(NumberConverter.round(paid, 2));
           moneyReceiptSubResponse.setWaiver(NumberConverter.round(waiver,2));
           
           moneyReceiptSubList.add(moneyReceiptSubResponse);
         
        }
        
       moneyReceiptMainResponse.setMoneyReceiptSubList(moneyReceiptSubList);

    }
    
    
    public void setFeeSubHeadViewSerial(List<FeeInvoiceDetails> invoiceFeeDetailses){
        
        for(FeeInvoiceDetails det:invoiceFeeDetailses){
            if(det.getFeeSubHead()==null){
                det.setFeeSubHeadViewSerial(200);
            }else{
                det.setFeeSubHeadViewSerial(det.getFeeSubHead().getSerial());
            }
        }
        
        Collections.sort(invoiceFeeDetailses, Comparator.comparing(FeeInvoiceDetails::getFeeSubHeadViewSerial));
        
        
     }
    
    
    public void setFeeHeadViewSerial(List<FeeInvoiceDetails> invoiceFeeDetailses){
        
        for(FeeInvoiceDetails det:invoiceFeeDetailses){
            
            if(det.getFeeHead()!=null){
            
             det.setFeeHeadViewSerial(det.getFeeHead().getSerial());
             
            }
            
        }
        
        Collections.sort(invoiceFeeDetailses, Comparator.comparing(FeeInvoiceDetails::getFeeHeadViewSerial));
        
        
     }

}
