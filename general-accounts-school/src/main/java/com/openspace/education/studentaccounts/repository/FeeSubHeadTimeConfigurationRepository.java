package com.openspace.education.studentaccounts.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadTimeConfiguration;

public interface FeeSubHeadTimeConfigurationRepository extends JpaRepository<FeeSubHeadTimeConfiguration, Long>{
	
public List<FeeSubHeadTimeConfiguration> findByInstituteAndFeeHead_IdInAndFeeSubHead_IdInAndPayableYearIn(Institute institute, Set<Long> feeHeadIds, Set<Long> feeSubHeadIds, Set<Integer> payableYears);

public List<FeeSubHeadTimeConfiguration> findByInstituteAndFeeHead_IdAndPayableYearOrderByFeeHead_Serial(Institute institute, Long feeHeadId, Integer payableYear);

public List<FeeSubHeadTimeConfiguration> findByInstituteAndPayableYearAndPayableMonthOrderByFeeHead_Serial(Institute institute, Integer payableYear, String month);

public List<FeeSubHeadTimeConfiguration> findByInstituteAndPayableYearAndPayableMonthInOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute, Integer payableYear, List<String> months);

public List<FeeSubHeadTimeConfiguration> findByInstituteAndPayableYearOrderByFeeHead_Serial(Institute institute,  Integer payableYear);

public List<FeeSubHeadTimeConfiguration> findByInstituteAndFeeActiveDateLessThanEqualAndPayableYearOrderByFeeHead_Serial(Institute institute, Date feeActiveDate, Integer payableYear);


public FeeSubHeadTimeConfiguration findByIdAndInstitute(Long id, Institute institute);

}
