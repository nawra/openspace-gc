package com.openspace.education.studentaccounts.model.response;

public class FeeCollectionSubHeadView {
	
	
	private Long feeSubHeadId;
	private String feeSubHeadName;
	private double feeAmount;
	private double fineAmount;
	private double waiverAmount;
	private Long waiverId;
	private String waiverName="";
	
	
	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}
	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}
	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}
	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}
	public double getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}
	public double getFineAmount() {
		return fineAmount;
	}
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}
	public double getWaiverAmount() {
		return waiverAmount;
	}
	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}
	public Long getWaiverId() {
		return waiverId;
	}
	public void setWaiverId(Long waiverId) {
		this.waiverId = waiverId;
	}
	public String getWaiverName() {
		return waiverName;
	}
	public void setWaiverName(String waiverName) {
		this.waiverName = waiverName;
	}
	
	

}
