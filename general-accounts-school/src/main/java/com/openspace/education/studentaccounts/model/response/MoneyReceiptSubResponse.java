package com.openspace.education.studentaccounts.model.response;

public class MoneyReceiptSubResponse {
	
	 private String feeHeadName;
	    private String details;
	    private Double waiver;
	    private Double fine;
	    private Double payable;
	    private Double paid;

	    

		public String getFeeHeadName() {
	        return feeHeadName;
	    }

	    public void setFeeHeadName(String feeHeadName) {
	        this.feeHeadName = feeHeadName;
	    }

	    public String getDetails() {
	        return details;
	    }

	    public void setDetails(String details) {
	        this.details = details;
	    }

	    public Double getWaiver() {
	        return waiver;
	    }

	    public void setWaiver(Double waiver) {
	        this.waiver = waiver;
	    }

	    public Double getFine() {
	        return fine;
	    }

	    public void setFine(Double fine) {
	        this.fine = fine;
	    }

	    public Double getPayable() {
	        return payable;
	    }

	    public void setPayable(Double payable) {
	        this.payable = payable;
	    }
	    
	    public Double getPaid() {
			return paid;
		}

		public void setPaid(Double paid) {
			this.paid = paid;
		}
	    

}
