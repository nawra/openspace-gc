package com.openspace.education.studentaccounts.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.studentaccounts.model.request.FeeSubHeadConfigurationRequest;
import com.openspace.education.studentaccounts.service.FeeSubheadConfigurationService;

@Controller
@RequestMapping(value ="/fee-subhead/configuration" )
public class FeeSubheadConfigurationController {
	
	@Autowired
	public FeeSubheadConfigurationService feeSubheadConfigurationService;
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveFeeSubheadConfiguration(@RequestBody @Valid FeeSubHeadConfigurationRequest request){
		BaseResponse baseResponse=feeSubheadConfigurationService.saveFeeSubheadConfiguration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> saveFeeSubheadConfiguration(@RequestParam Long configId){
		BaseResponse baseResponse=feeSubheadConfigurationService.deleteFeeSubheadConfiguration(configId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> feeSubheadConfigurationList(){
		ItemResponse itemResponse=feeSubheadConfigurationService.feeSubheadConfigurationList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/list/by/fee-head-id")
	public ResponseEntity<ItemResponse> feeSubheadConfigurationListByFeeHeadId(@RequestParam Long feeHeadId){
		ItemResponse itemResponse=feeSubheadConfigurationService.feeSubHeadConfigurationListByFeeHeadId(feeHeadId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}


}
