package com.openspace.education.studentaccounts.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeHead;
import com.openspace.education.initialsetup.model.entity.CoreSettingFeeWaiver;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;


@Entity
@Table(name="fee_waiver_configuration",uniqueConstraints = @UniqueConstraint(columnNames={"institute_id","fee_head_id","identification_id"}))
public class FeeWaiverConfiguration implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2104659613042891790L;


	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
        
    @Column(name="waiver_amount")
    private double waiverAmount;
    
    @ManyToOne
    @JoinColumn(name="fee_head_id")
    private CoreSettingFeeHead feeHead;
    
    @ManyToOne
    @JoinColumn(name="fee_waiver_id")
    private CoreSettingFeeWaiver feeWaiver;
    
    @ManyToOne
    @JoinColumn(name="identification_id")
    private StudentIdentification studentIdentification;
    
    @ManyToOne
    @JoinColumn(name="institute_id")
    private Institute institute;
    
    
    @Column(name="config_date")
    @Temporal(TemporalType.DATE)
    private Date configDate;
    
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(double waiverAmount) {
		this.waiverAmount = waiverAmount;
	}

	public CoreSettingFeeHead getFeeHead() {
		return feeHead;
	}

	public void setFeeHead(CoreSettingFeeHead feeHead) {
		this.feeHead = feeHead;
	}

	public CoreSettingFeeWaiver getFeeWaiver() {
		return feeWaiver;
	}

	public void setFeeWaiver(CoreSettingFeeWaiver feeWaiver) {
		this.feeWaiver = feeWaiver;
	}

	public StudentIdentification getStudentIdentification() {
		return studentIdentification;
	}

	public void setStudentIdentification(StudentIdentification studentIdentification) {
		this.studentIdentification = studentIdentification;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Date getConfigDate() {
		return configDate;
	}

	public void setConfigDate(Date configDate) {
		this.configDate = configDate;
	}
    
    
    

}
