package com.openspace.education.studentaccounts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.initialsetup.model.response.CoreSettingsResponse;
import com.openspace.education.initialsetup.service.CoreSettingsService;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdentificationRepository;
import com.openspace.education.studentaccounts.model.entity.FeeAmountConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeInvoiceDetails;
import com.openspace.education.studentaccounts.model.entity.FeeSubHeadTimeConfiguration;
import com.openspace.education.studentaccounts.model.entity.FeeWaiverConfiguration;
import com.openspace.education.studentaccounts.model.response.FeeCollectionHeadView;
import com.openspace.education.studentaccounts.model.response.FeesCollectionView;
import com.openspace.education.studentaccounts.model.response.FeesCollectionViewHelper1;
import com.openspace.education.studentaccounts.model.response.FeesCollectionViewHelper2;
import com.openspace.education.studentaccounts.model.response.FeesCollectionViewHelper3;
import com.openspace.education.studentaccounts.repository.FeeAmountConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeInvoiceDetailsRepository;
import com.openspace.education.studentaccounts.repository.FeeSubHeadTimeConfigurationRepository;
import com.openspace.education.studentaccounts.repository.FeeWaiverConfigurationRepository;

@Service
public class FeeCollectionViewService {
	
	
	@Autowired
	public StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	public FeeAmountConfigurationRepository feeAmountConfigurationRepository;
	
	@Autowired
	public FeeInvoiceDetailsRepository feeInvoiceDetailsRepository;
	
	@Autowired
	public FeeSubHeadTimeConfigurationRepository feeSubHeadTimeConfigurationRepository;
	
	@Autowired
	public FeeWaiverConfigurationRepository feeWaiverConfigurationRepository;
	
	@Autowired
	public CoreSettingsService coreSettingsService;
	
	@Autowired
	public ImageStorageService imageStorageService;
	
	
	public ItemResponse feeCollectionView(String customStudentId, Integer academicYear) {
		
		ItemResponse itemResponse = new ItemResponse<>();

		Institute institute  = UserInfoUtils.getLoggedInInstitute();
		
		StudentIdentification identification =  studentIdentificationRepository.findByInstituteAndAcademicYearAndStudentBasic_CustomStudentId(institute, academicYear, customStudentId);
		
		if(identification==null) {
			itemResponse.setMessage("No Student Found.");	
			itemResponse.setMessageType(0);
			return itemResponse;
		}
		
		FeesCollectionView feesCollectionView = new FeesCollectionView();
		feesCollectionView.setIdentificationId(identification.getIdentificationId());
		feesCollectionView.setCustomStudentId(identification.getStudentBasic().getCustomStudentId());
		feesCollectionView.setStudentName(identification.getStudentBasic().getStudentName());
		feesCollectionView.setStudentRoll(identification.getStudentRoll());
		feesCollectionView.setSection(identification.getClassConfigurationInfo().getClassInfo().getName()+"-"+identification.getClassConfigurationInfo().getShiftInfo().getName()+"-"+identification.getClassConfigurationInfo().getSectionInfo().getName());


		try {
			feesCollectionView.setStudentPhoto("data:image/png;base64, "+new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STUDENT.name(), identification.getStudentBasic().getImageName())));	
		}catch(Exception e) {
			feesCollectionView.setStudentPhoto("");
		}
		
		
		
		
		Long classId=identification.getClassConfigurationInfo().getClassInfo().getId();
		Long groupId=identification.getGroupInfo().getId();
		Long studentCategoryId=identification.getStudentCategoryInfo().getId();
		
		List<FeeAmountConfiguration> feeAmountConfigurations=feeAmountConfigurationRepository.findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(institute, classId, groupId, studentCategoryId);
		
		List<FeeInvoiceDetails> feeInvoiceDetails=feeInvoiceDetailsRepository.findByInstituteAndIdentification_IdentificationId(institute, identification.getIdentificationId());

		List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations = feeSubHeadTimeConfigurationRepository.findByInstituteAndPayableYearOrderByFeeHead_Serial(institute, identification.getAcademicYear());
		
		List<FeeWaiverConfiguration> feeWaiverConfigurations = feeWaiverConfigurationRepository.findByInstituteAndStudentIdentification_IdentificationId(institute, identification.getIdentificationId());
		
		List<CoreSettingsResponse> waiverList = coreSettingsService.findFeeWaiverList();
		
		Map<Long,FeeCollectionHeadView> feeHeadMap=new LinkedHashMap<>();
		
		List<FeeCollectionHeadView> feeHeadList=new ArrayList<>();
		
		feeAmountConfigurations.forEach(fc -> {
			
		  feeHeadMap.put(fc.getFeeHead().getId(), new FeeCollectionHeadView(fc.getFeeHead().getId(),fc.getFeeHead().getName()));	
			
		});
				
		feeHeadList.addAll(feeHeadMap.values());
		
		
		 List<FeesCollectionViewHelper1> feeHeadDetails = new ArrayList<>();
		
		 for(FeeCollectionHeadView fh : feeHeadList) {
			
			 List<FeesCollectionViewHelper2> feeSubHeadViews = new ArrayList<>();
			 findFeeSubHeadList(feeInvoiceDetails, feeAmountConfigurations, feeSubHeadViews, fh.getFeeHeadId(), feeSubHeadTimeConfigurations, feeWaiverConfigurations);
			
			 if(feeSubHeadViews.size()>0) {
				
				 FeesCollectionViewHelper1 feeHead = new FeesCollectionViewHelper1();
				 
				 Long feeWaiverid=findWaiverId(feeWaiverConfigurations, fh.getFeeHeadId());
				 
				 feeHead.setFeeHeadId(fh.getFeeHeadId());
				 feeHead.setFeeHeadName(fh.getFeeHeadName());
				 feeHead.setFeeWaiverId(feeWaiverid);
				 feeHead.setFeeWaiverList(waiverList);
				 feeHead.setFeeSubHeadDetils(feeSubHeadViews);
				 
				 feeHeadDetails.add(feeHead);
				 
			  }
			 
		   }
		 
		 
		feesCollectionView.setFeeHeadDetails(feeHeadDetails);
		
		
		// -----------------------------------------------//
		
		List<Object[]> prevDueDetails = feeInvoiceDetailsRepository.findStudentPreviousDueSummary(identification.getIdentificationId(), institute.getInstituteId());
		List<FeesCollectionViewHelper3> previousDueDetails = new ArrayList<>();
		
		for(Object[] prev : prevDueDetails) {
			
			FeesCollectionViewHelper3 dueDet=new FeesCollectionViewHelper3();
			dueDet.setFeeHeadId(Long.parseLong(prev[0].toString()));
			dueDet.setFeeHeadName(prev[1].toString());
			dueDet.setFeeWaiverList(waiverList);
			dueDet.setPreviousDuePayable(Double.parseDouble(prev[2].toString()));
			dueDet.setTemporaryPreviousDuePayable(Double.parseDouble(prev[2].toString()));
			previousDueDetails.add(dueDet);
			
		}
		
		feesCollectionView.setPreviousDueDetails(previousDueDetails);
		
		
		
		itemResponse.setItem(feesCollectionView);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		
		return itemResponse;
	}
	
	
	
    
	
	public void findFeeSubHeadList(List<FeeInvoiceDetails> feeInvoiceDetails,List<FeeAmountConfiguration> feeAmountConfigurations,List<FeesCollectionViewHelper2> feeSubHeadViews,Long feeHeadId, List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations, List<FeeWaiverConfiguration> feeWaiverConfigurations) {
		
		for(FeeAmountConfiguration feeAmt:feeAmountConfigurations) {
			
			if(feeAmt.getFeeHead().getId().equals(feeHeadId)) {
				
			String str="no";
				
			Long feeSubHeadId=feeAmt.getFeeSubHead().getId();
			
			for(FeeInvoiceDetails det:feeInvoiceDetails) {
				
			if(det.getFeeHead().getId().equals(feeHeadId) && det.getFeeSubHead()!=null && feeSubHeadId.equals(det.getFeeSubHead().getId())) {	
			
				str="yes";
				
				break;
			
			}
			
			}
			
			if(str.equals("no")) {
				
			FeesCollectionViewHelper2 view=new FeesCollectionViewHelper2();
			view.setFeeSubHeadName(feeAmt.getFeeSubHead().getName());
			view.setFeeSubHeadId(feeAmt.getFeeSubHead().getId());
			view.setFeeAmount(feeAmt.getFeeAmount());
			
			if(isFineApplicable(feeSubHeadTimeConfigurations, feeHeadId, feeSubHeadId)) {
			view.setFineAmount(feeAmt.getFineAmount());
			}
			
			double waiver = findWaiverAmount(feeWaiverConfigurations, feeHeadId);
			
			if(waiver >= feeAmt.getFeeAmount()) {
				waiver=0;	
			}
			
			  view.setWaiverAmount(waiver);
			
			  feeSubHeadViews.add(view);
				
			 }
			
			}
		
		}
		
	 }
	
	
	public boolean isFineApplicable(List<FeeSubHeadTimeConfiguration> feeSubHeadTimeConfigurations,Long feeHeadId, Long feeSubHeadId) {
		
		for(FeeSubHeadTimeConfiguration conf : feeSubHeadTimeConfigurations) {
			
			if(conf.getFeeHead().getId().equals(feeHeadId) && conf.getFeeSubHead().getId().equals(feeSubHeadId) && new Date().after(conf.getFineActiveDate())) {
				return true;
			}
		}
		
		return false;
	}
	
	
	public double findWaiverAmount(List<FeeWaiverConfiguration> feeWaiverConfigurations,Long feeHeadId) {
		
		for(FeeWaiverConfiguration conf : feeWaiverConfigurations) {
			
			if(conf.getFeeHead().getId().equals(feeHeadId)) {
				
				return conf.getWaiverAmount();
			}
		}
		
		return 0;
	}
	
	
   public Long findWaiverId(List<FeeWaiverConfiguration> feeWaiverConfigurations,Long feeHeadId) {
		
		for(FeeWaiverConfiguration conf : feeWaiverConfigurations) {
			
			if(conf.getFeeHead().getId().equals(feeHeadId)) {
				
				return conf.getFeeWaiver().getId();
			}
		}
		
		return null;
	}
	
	

}
