package com.openspace.education.studentaccounts.model.response;


public class FeeAmountConfigurationView {

    private Long configId;
	
	private double feeAmount;
	
	private double fineAmount;
	
	private Long classId;
	
	private String className;

    private Long groupId;
	
	private String groupName;
	
    private Long studentCategoryId;
	
	private String studentCategoryName;
	
    private Long feeHeadId;
	
	private String feeHeadName;
	
	private Long feeSubHeadId;
		
    private String feeSubHeadName;
    

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public double getFineAmount() {
		return fineAmount;
	}

	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getStudentCategoryId() {
		return studentCategoryId;
	}

	public void setStudentCategoryId(Long studentCategoryId) {
		this.studentCategoryId = studentCategoryId;
	}

	public String getStudentCategoryName() {
		return studentCategoryName;
	}

	public void setStudentCategoryName(String studentCategoryName) {
		this.studentCategoryName = studentCategoryName;
	}

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}

	public Long getFeeSubHeadId() {
		return feeSubHeadId;
	}

	public void setFeeSubHeadId(Long feeSubHeadId) {
		this.feeSubHeadId = feeSubHeadId;
	}

	public String getFeeSubHeadName() {
		return feeSubHeadName;
	}

	public void setFeeSubHeadName(String feeSubHeadName) {
		this.feeSubHeadName = feeSubHeadName;
	}
    
    
    
    
	
}
