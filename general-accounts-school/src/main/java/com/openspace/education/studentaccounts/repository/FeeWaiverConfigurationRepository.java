package com.openspace.education.studentaccounts.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.studentaccounts.model.entity.FeeWaiverConfiguration;

public interface FeeWaiverConfigurationRepository extends JpaRepository<FeeWaiverConfiguration, Long>{

	public List<FeeWaiverConfiguration> findByInstituteAndFeeHead_IdAndStudentIdentification_IdentificationIdIn(Institute institute,Long feeHeadId, List<Long> identificationIds);
	
	public FeeWaiverConfiguration findByIdAndInstitute(Long id,Institute institute);
	
	public List<FeeWaiverConfiguration> findByInstituteAndStudentIdentification_ClassConfigurationInfo_IdAndStudentIdentification_StudentCategoryInfo_IdAndStudentIdentification_StudentStatusAndStudentIdentification_AcademicYear(
			Institute institute,Long classConfigId,Long studentCategoryId,boolean studentStatus,Integer academicYear);
	
	
	public List<FeeWaiverConfiguration> findByInstituteAndStudentIdentification_IdentificationId(Institute institute,Long identificationId);

	public List<FeeWaiverConfiguration> findByInstituteAndStudentIdentificationIn(Institute institute,List<StudentIdentification> identifications);

	
	public List<FeeWaiverConfiguration> findByInstituteAndStudentIdentification_ClassConfigurationInfo_IdAndStudentIdentification_AcademicYear(Institute institute, Long classConfigId, Integer academicYear);

	public List<FeeWaiverConfiguration> findByInstituteAndStudentIdentification_AcademicYear(Institute institute, Integer academicYear);

}
