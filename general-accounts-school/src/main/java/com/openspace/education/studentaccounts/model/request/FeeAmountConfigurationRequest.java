package com.openspace.education.studentaccounts.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FeeAmountConfigurationRequest {
	
	@NotNull
	private Long classId;
	
	@NotNull
	private Long groupId;
	
	@NotNull
	private Long studentCategoryId;
	
	@NotNull
	private Long feeHeadId;
	
	@NotNull
	@Size(min = 1)
	private List<FeeAmountConfigurationRequestHelper> feeSubHeadDetails;
	
	
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public Long getStudentCategoryId() {
		return studentCategoryId;
	}
	public void setStudentCategoryId(Long studentCategoryId) {
		this.studentCategoryId = studentCategoryId;
	}
	public Long getFeeHeadId() {
		return feeHeadId;
	}
	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}
	public List<FeeAmountConfigurationRequestHelper> getFeeSubHeadDetails() {
		return feeSubHeadDetails;
	}
	public void setFeeSubHeadDetails(List<FeeAmountConfigurationRequestHelper> feeSubHeadDetails) {
		this.feeSubHeadDetails = feeSubHeadDetails;
	}
	
	
	
	
	

}
