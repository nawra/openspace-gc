package com.openspace.education.studentaccounts.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.studentaccounts.model.entity.FeeAmountConfiguration;

public interface FeeAmountConfigurationRepository extends JpaRepository<FeeAmountConfiguration, Long>{
	
	public List<FeeAmountConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_IdAndFeeSubHead_IdIn(
	Institute institute,Long classId,Long groupId,Long studentCategoryId,Long feeHeadId,Set<Long> feeSubHeadIds);
	
	public FeeAmountConfiguration findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_IdAndFeeSubHead_Id(Institute institute,Long classId,Long groupId,Long studentCategoryId,Long feeHeadId,Long feeSubHeadId);
	
	public FeeAmountConfiguration findByConfigIdAndInstitute(Long configId,Institute institute);
	
	public List<FeeAmountConfiguration> findByInstituteOrderByClassInfo_SerialAscGroupInfo_SerialAscStudentCategory_SerialAscFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute);
	
	public List<FeeAmountConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_Id(Institute institute,Long classId,Long groupId,Long studentCategoryId,Long feeHeadId);

	public List<FeeAmountConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute,Long classId,Long groupId,Long studentCategoryId);

	public List<FeeAmountConfiguration> findByInstituteAndClassInfo_IdAndGroupInfo_IdAndStudentCategory_IdAndFeeHead_IdInAndFeeSubHead_IdInOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute,Long classId,Long groupId,Long studentCategoryId, Set<Long> feeHeadIds, Set<Long> feeSubHeadIds);

	public List<FeeAmountConfiguration> findByInstituteAndClassInfoAndFeeHead_IdInAndFeeSubHead_IdInOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute,CoreSettingClass classInfo,Set<Long> feeHeadIds, Set<Long> feeSubHeadIds);

	public List<FeeAmountConfiguration> findByInstituteAndClassInfoOrderByFeeHead_SerialAscFeeSubHead_SerialAsc(Institute institute,CoreSettingClass classInfo);

	
	public List<FeeAmountConfiguration> findByInstituteOrderByFeeSubHead_Serial(Institute institute);
}
