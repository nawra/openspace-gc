package com.openspace.education.studentaccounts.model.response;

import java.util.List;

public class FeeHeadAndSubHeadView {
	
    private Long feeHeadId;
	
	private String feeHeadName;
	
	private List<FeeHeadAndSubHeadViewHelper> feeSubHeadList;

	public Long getFeeHeadId() {
		return feeHeadId;
	}

	public void setFeeHeadId(Long feeHeadId) {
		this.feeHeadId = feeHeadId;
	}

	public String getFeeHeadName() {
		return feeHeadName;
	}

	public void setFeeHeadName(String feeHeadName) {
		this.feeHeadName = feeHeadName;
	}

	public List<FeeHeadAndSubHeadViewHelper> getFeeSubHeadList() {
		return feeSubHeadList;
	}

	public void setFeeSubHeadList(List<FeeHeadAndSubHeadViewHelper> feeSubHeadList) {
		this.feeSubHeadList = feeSubHeadList;
	}
	
	
	
	

}
