/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.generalaccount.service.GeneralAccountReportService;

/**
 *
 * @author riad
 */

@Controller
@RequestMapping(value="/general/accounts/report")
public class GeneralAccountReportController {
    
    @Autowired
    public GeneralAccountReportService generalAccountReportService;
    
    @RequestMapping(value = "/category/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> categoryList()  {
        return new ResponseEntity<>(generalAccountReportService.accountCategoryList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/all/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> ledgerList()  {
        return new ResponseEntity<>(generalAccountReportService.allLedgerList(), HttpStatus.OK);
    }
        
    @RequestMapping(value = "/cash-equivalance/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashEquivalanceLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.cashEquiValanceLedgerList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/cash-equivalance/dfps/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashEquivalanceAndDfpsLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.cashEquiValanceAndDfpsLedgerList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/ledger/list/for/payment", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> ledgerListForPayment()  {
        return new ResponseEntity<>(generalAccountReportService.ledgerListForPayment(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/ledger/list/for/receipt", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> ledgerListForReceipt()  {
        return new ResponseEntity<>(generalAccountReportService.ledgerListForReceipt(), HttpStatus.OK);
    }
    
    @GetMapping(value = "/income/ledger/list")
    public ResponseEntity<ItemResponse> incomeLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.incomeLedgerList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/short/term/liability/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> shortTermLiabilityList()  {
        return new ResponseEntity<>(generalAccountReportService.shortTimeLiabilityList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/account/receivable/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> accountReceivableLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.accountReceivableList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/account/payable/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> accountPayableLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.accountPayableList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/account/regular-income/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> accountRegularIncomeLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.accountRegularIncomeList(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/account/ofps/ledger/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> ofpsLedgerList()  {
        return new ResponseEntity<>(generalAccountReportService.ofpsLedgerList(), HttpStatus.OK);
    }
    
    
    
    @RequestMapping(value = "/journal", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> transactionJournal(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate)  {
        return new ResponseEntity<>(generalAccountReportService.findTransactionJournal(fromDate, toDate), HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/trial-balance", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> trialBalance(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate)  {
        return new ResponseEntity<>(generalAccountReportService.findTrialBalance(fromDate, toDate), HttpStatus.OK);
    }
       
    @RequestMapping(value = "/balance-sheet", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> balanceSheet(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date trnDate)  {
        return new ResponseEntity<>(generalAccountReportService.findBalanceSheet(trnDate), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/income-statement", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> incomeStatement(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate)  {
        return new ResponseEntity<>(generalAccountReportService.incomeStatement(fromDate, toDate), HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/cash-summary", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashSummary(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate)  {
        return new ResponseEntity<>(generalAccountReportService.cashSummary(fromDate, toDate), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/funds-flow", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashSummary(@RequestParam int year)  {
        return new ResponseEntity<>(generalAccountReportService.fundsFlow(year), HttpStatus.OK);
    }
       
    @RequestMapping(value = "/show-voucher/by/voucher-id", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashSummary(@RequestParam Long voucherId)  {
        return new ResponseEntity<>(generalAccountReportService.showVoucherByVoucherId(voucherId), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/cashbook", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> cashBook(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate,@RequestParam List<Long> cashLedgerIds)  {
        return new ResponseEntity<>(generalAccountReportService.cashBook(fromDate, toDate, cashLedgerIds), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/ledgerbook", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> ledgerBook(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate,@RequestParam Long ledgerId)  {
        return new ResponseEntity<>(generalAccountReportService.ledgerBook(fromDate, toDate, ledgerId), HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/user-wise/collection", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> userWiseCollection(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate,@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate)  {
        return new ResponseEntity<>(generalAccountReportService.userWiseCollectionView(fromDate, toDate), HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/voucher/list", method = RequestMethod.GET)
    public ResponseEntity<ItemResponse> voucherList(@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date fromDate, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate, @RequestParam String trnType)  {
        return new ResponseEntity<>(generalAccountReportService.voucherListByTrnType(fromDate, toDate, trnType), HttpStatus.OK);
    }
    
}
