/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

/**
 *
 * @author riad
 */
public class AccountCategoryResponse {
    

    private Long categoryId;
    

    private String categoryName;
    

    private String nature;
    

    private String categroyType;
    

    private Integer categorySerial;
    

    private Integer categoryDefaultId;
    
   
    private String categoryNote;  

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getCategroyType() {
        return categroyType;
    }

    public void setCategroyType(String categroyType) {
        this.categroyType = categroyType;
    }

    public Integer getCategorySerial() {
        return categorySerial;
    }

    public void setCategorySerial(Integer categorySerial) {
        this.categorySerial = categorySerial;
    }

    public Integer getCategoryDefaultId() {
        return categoryDefaultId;
    }

    public void setCategoryDefaultId(Integer categoryDefaultId) {
        this.categoryDefaultId = categoryDefaultId;
    }

    public String getCategoryNote() {
        return categoryNote;
    }

    public void setCategoryNote(String categoryNote) {
        this.categoryNote = categoryNote;
    }
    
    
    
}
