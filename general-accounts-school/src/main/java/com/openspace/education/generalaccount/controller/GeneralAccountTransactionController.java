/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.controller;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.generalaccount.model.request.ContraRequest;
import com.openspace.education.generalaccount.model.request.JournalRequest;
import com.openspace.education.generalaccount.model.request.ReceiptPaymentRequest;
import com.openspace.education.generalaccount.service.GeneralAccountTransactionService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author riad
 */
@Controller
@RequestMapping("/general/accounts/transaction")
public class GeneralAccountTransactionController {
    
   @Autowired
   public GeneralAccountTransactionService generalAccountTransactionService;
    
   
   @RequestMapping(value="/receipt",method=RequestMethod.POST) 
   public ResponseEntity<ItemResponse> doReceiptVoucher(@RequestBody  @Valid ReceiptPaymentRequest receiptRequest){
       ItemResponse itemResponse=generalAccountTransactionService.doReceiptVoucher(receiptRequest);
      return new ResponseEntity<>(itemResponse,HttpStatus.CREATED);
   }
   
   
   @RequestMapping(value="/payment",method=RequestMethod.POST) 
   public ResponseEntity<ItemResponse> doPaymentVoucher(@RequestBody @Valid ReceiptPaymentRequest receiptRequest){
       ItemResponse itemResponse=generalAccountTransactionService.doPaymentVoucher(receiptRequest);
      return new ResponseEntity<>(itemResponse,HttpStatus.CREATED);
   }
   
   
   @RequestMapping(value="/contra",method=RequestMethod.POST) 
   public ResponseEntity<ItemResponse> doContraVoucher(@RequestBody @Valid ContraRequest contraRequest){
       ItemResponse itemResponse=generalAccountTransactionService.doContraVoucher(contraRequest);
      return new ResponseEntity<>(itemResponse,HttpStatus.CREATED);
   }
   
   
   @RequestMapping(value="/journal",method=RequestMethod.POST) 
   public ResponseEntity<ItemResponse> doJournalVoucher(@RequestBody @Valid JournalRequest journalRequest){
       ItemResponse itemResponse=generalAccountTransactionService.doJournalVoucher(journalRequest);
      return new ResponseEntity<>(itemResponse,HttpStatus.CREATED);
   }
   
   
   @RequestMapping(value="/delete",method=RequestMethod.DELETE) 
   public ResponseEntity<BaseResponse> deleteVoucher(@RequestParam Long trnId){
       BaseResponse baseResponse=generalAccountTransactionService.deleteVoucher(trnId);
      return new ResponseEntity<>(baseResponse,HttpStatus.OK);
   }
   
   
    
}
