/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

/**
 *
 * @author riad
 */
public class LedgerUpdateRequest {
    
    private Long ledgerId;
    private String ledgerName;
    private String note;
    private Long accountCategoryId;

    public Long getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getAccountCategoryId() {
        return accountCategoryId;
    }

    public void setAccountCategoryId(Long accountCategoryId) {
        this.accountCategoryId = accountCategoryId;
    }
    
    
}
