/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class IncomeStatementResponse {
    
    private double totalIncome;
    private double totalExpense;
    private double totalDebit;
    private double totalCredit;
    
    private List<IncomeStatementHelper> incomeList;
    private List<IncomeStatementHelper> expenseList;
    private List<IncomeStatementHelper> profitLosList;

    public double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public double getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(double totalExpense) {
        this.totalExpense = totalExpense;
    }

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public List<IncomeStatementHelper> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(List<IncomeStatementHelper> incomeList) {
        this.incomeList = incomeList;
    }

    public List<IncomeStatementHelper> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<IncomeStatementHelper> expenseList) {
        this.expenseList = expenseList;
    }

    public List<IncomeStatementHelper> getProfitLosList() {
        return profitLosList;
    }

    public void setProfitLosList(List<IncomeStatementHelper> profitLosList) {
        this.profitLosList = profitLosList;
    }
    
    
    
}
