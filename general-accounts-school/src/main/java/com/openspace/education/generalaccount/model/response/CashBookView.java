package com.openspace.education.generalaccount.model.response;

import java.util.List;

public class CashBookView {
	
	private double totalDebit;
	private double totalCredit;
	
	private List<CashBookViewHelper1> details;

	public double getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(double totalDebit) {
		this.totalDebit = totalDebit;
	}

	public double getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(double totalCredit) {
		this.totalCredit = totalCredit;
	}

	public List<CashBookViewHelper1> getDetails() {
		return details;
	}

	public void setDetails(List<CashBookViewHelper1> details) {
		this.details = details;
	}
	
	

}
