package com.openspace.education.generalaccount.model.response;

import java.util.Date;


public class VoucherView {

    private Long trnId;
    
    private Long voucherId;   

    private String voucherNo;
    
    private String voucherNote;
    
    private String trnType;
       
    private Double trnAmount;

    private String trnDate;

    private String userName;
    
    private String userNickName;
    
    private String userDesignation;
    
    private String inWord;
    
    private String ledgerName="";

	public Long getTrnId() {
		return trnId;
	}

	public void setTrnId(Long trnId) {
		this.trnId = trnId;
	}

	public Long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getVoucherNote() {
		return voucherNote;
	}

	public void setVoucherNote(String voucherNote) {
		this.voucherNote = voucherNote;
	}

	public String getTrnType() {
		return trnType;
	}

	public void setTrnType(String trnType) {
		this.trnType = trnType;
	}

	public Double getTrnAmount() {
		return trnAmount;
	}

	public void setTrnAmount(Double trnAmount) {
		this.trnAmount = trnAmount;
	}

	public String getTrnDate() {
		return trnDate;
	}

	public void setTrnDate(String trnDate) {
		this.trnDate = trnDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getInWord() {
		return inWord;
	}

	public void setInWord(String inWord) {
		this.inWord = inWord;
	}

	public String getUserDesignation() {
		return userDesignation;
	}

	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
    
    
    
    
}
