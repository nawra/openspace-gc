package com.openspace.education.generalaccount.model.response;

import java.util.Date;

public class LedgerBookView {
	
	    private Date trnDate;
	    private String type;
	    private String ledgerName;
	    private String voucherType;
	    private Long voucherId;
	    private String voucherNo;
	    private double crAmount;
	    private double drAmount;
	    private String userName;
	    
	    
	    
		public Date getTrnDate() {
			return trnDate;
		}
		public void setTrnDate(Date trnDate) {
			this.trnDate = trnDate;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getLedgerName() {
			return ledgerName;
		}
		public void setLedgerName(String ledgerName) {
			this.ledgerName = ledgerName;
		}
		public String getVoucherType() {
			return voucherType;
		}
		public void setVoucherType(String voucherType) {
			this.voucherType = voucherType;
		}
		public String getVoucherNo() {
			return voucherNo;
		}
		public void setVoucherNo(String voucherNo) {
			this.voucherNo = voucherNo;
		}
		public double getCrAmount() {
			return crAmount;
		}
		public void setCrAmount(double crAmount) {
			this.crAmount = crAmount;
		}
		public double getDrAmount() {
			return drAmount;
		}
		public void setDrAmount(double drAmount) {
			this.drAmount = drAmount;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public Long getVoucherId() {
			return voucherId;
		}
		public void setVoucherId(Long voucherId) {
			this.voucherId = voucherId;
		}
	    
	    
	    

}
