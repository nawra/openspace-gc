/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.utils;

/**
 *
 * @author riad
 */
public class AccountInfoUtils {
    
    public static final Long GENERAL_ACCOUNTS_MODULE_ID=1l;
    public static final Long STUDENT_ACCOUNTS_MODULE_ID=2l;

    
    public static final String RECEIPT_TRNSACTION_TYPE="Receipt";
    public static final String PAYNENT_TRNSACTION_TYPE="Payment";
    public static final String CONTRA_TRNSACTION_TYPE="Contra";
    public static final String JOURNAL_TRNSACTION_TYPE="Journal";
    
    
    public static double round(double number, int position) {
        double pow = 10;
        pow = Math.pow(pow, position);
        number = number * pow;
        number = Math.round(number);
        number = number / 100;
        return number;
    }
}
