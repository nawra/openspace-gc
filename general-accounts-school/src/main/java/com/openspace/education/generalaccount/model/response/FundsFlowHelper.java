/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

/**
 *
 * @author riad
 */
public class FundsFlowHelper {
    
    private String monthName;
    private double openingBalance;
    private double closingBalance;
    private double fundsflow;

    public FundsFlowHelper(String monthName, double openingBalance, double closingBalance, double fundsflow) {
        this.monthName = monthName;
        this.openingBalance = openingBalance;
        this.closingBalance = closingBalance;
        this.fundsflow = fundsflow;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public double getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(double openingBalance) {
        this.openingBalance = openingBalance;
    }

    public double getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(double closingBalance) {
        this.closingBalance = closingBalance;
    }

    public double getFundsflow() {
        return fundsflow;
    }

    public void setFundsflow(double fundsflow) {
        this.fundsflow = fundsflow;
    }
    
    
}
