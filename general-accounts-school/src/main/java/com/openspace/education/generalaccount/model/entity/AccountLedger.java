/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.entity;

import com.openspace.education.institute.model.entity.Institute;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author riad
 */

@Entity
@Table(name="account_ledger",uniqueConstraints = @UniqueConstraint(columnNames = {"ledger_name","institute_id"}))
public class AccountLedger implements Serializable{
    
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="ledger_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ledgerId;
    
    @Column(name="ledger_name")
    private String ledgerName;
    
    @Column(name="note")
    private String note;
    
    @ManyToOne
    @JoinColumn(name = "institute_id" ,nullable = false)
    private Institute institute; 
    
    @ManyToOne
    @JoinColumn(name = "category_id" ,nullable = false)
    private AccountCategory accountCategory;

    public Long getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    

    public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public AccountCategory getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(AccountCategory accountCategory) {
        this.accountCategory = accountCategory;
    }
    
    
    
}
