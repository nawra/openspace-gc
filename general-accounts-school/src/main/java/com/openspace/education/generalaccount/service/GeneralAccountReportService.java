/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.service;

import com.openspace.education.common.CommonInfoUtils;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.MonthInfo;
import com.openspace.education.common.NumberConverter;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.entity.AccountCategory;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.generalaccount.model.entity.AccountTransactionDetails;
import com.openspace.education.generalaccount.model.response.AccountCategoryResponse;
import com.openspace.education.generalaccount.model.response.BalanceSheetDetailsResponse;
import com.openspace.education.generalaccount.model.response.BalanceSheetManager;
import com.openspace.education.generalaccount.model.response.BalanceSheetResponse;
import com.openspace.education.generalaccount.model.response.CashBookView;
import com.openspace.education.generalaccount.model.response.CashBookViewHelper1;
import com.openspace.education.generalaccount.model.response.CashBookViewHelper2;
import com.openspace.education.generalaccount.model.response.CashSummaryHelper;
import com.openspace.education.generalaccount.model.response.CashSummaryResponse;
import com.openspace.education.generalaccount.model.response.FundsFlowHelper;
import com.openspace.education.generalaccount.model.response.FundsFlowResponse;
import com.openspace.education.generalaccount.model.response.IncomeStatementHelper;
import com.openspace.education.generalaccount.model.response.IncomeStatementResponse;
import com.openspace.education.generalaccount.model.response.LedgerBookView;
import com.openspace.education.generalaccount.model.response.LedgerListResponse;
import com.openspace.education.generalaccount.model.response.TrialBalanceDetailsResponse;
import com.openspace.education.generalaccount.model.response.TrialBalanceManager;
import com.openspace.education.generalaccount.model.response.TrialBalanceResponse;
import com.openspace.education.generalaccount.model.response.UserWiseCollectionView;
import com.openspace.education.generalaccount.model.response.VoucherDetailsResponse;
import com.openspace.education.generalaccount.model.response.VoucherResponse;
import com.openspace.education.generalaccount.model.response.VoucherView;
import com.openspace.education.generalaccount.model.utils.AccountInfoUtils;
import com.openspace.education.generalaccount.repository.AccountCategoryRepository;
import com.openspace.education.generalaccount.repository.AccountLedgerRepository;
import com.openspace.education.generalaccount.repository.AccountTransactionDetailsRepository;
import com.openspace.education.generalaccount.repository.AccountTransactionRepository;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author riad
 */

@Service
public class GeneralAccountReportService {
    
    @Autowired
    public AccountCategoryRepository accountCategoryRepository;
    
    @Autowired
    public AccountLedgerRepository accountLedgerRepository;
    
    @Autowired
    public AccountTransactionRepository accountTransactionRepository;
    
    @Autowired
    private AccountTransactionDetailsRepository accountTransactionDetailsRepository;
    
    public ItemResponse<List<AccountCategoryResponse>> accountCategoryList(){
      ItemResponse<List<AccountCategoryResponse>> itemResponse=new ItemResponse<List<AccountCategoryResponse>>();
      List<AccountCategory> accountCategorys=accountCategoryRepository.findAllOrderByCategoryDefaultId();
      List<AccountCategoryResponse> accountCategoryResponses=new ArrayList<>();
      for(AccountCategory ac:accountCategorys){
          AccountCategoryResponse categoryResponse=new AccountCategoryResponse();
          BeanUtils.copyProperties(ac, categoryResponse);
          accountCategoryResponses.add(categoryResponse);
      }
      
      itemResponse.setItem(accountCategoryResponses);
      itemResponse.setMessage("");
      itemResponse.setMessageType(1);
      return  itemResponse;
        
    }
    
    public void setAccountLedgerToLedgerListResponse(AccountLedger al,LedgerListResponse ledgerListResponse){
       ledgerListResponse.setAccountCategoryId(al.getAccountCategory().getCategoryId());
       ledgerListResponse.setAccountCategoryName(al.getAccountCategory().getCategoryName());
       ledgerListResponse.setCategroyType(al.getAccountCategory().getCategoryType());
       ledgerListResponse.setInstituteId(al.getInstitute().getInstituteId());
       ledgerListResponse.setLedgerId(al.getLedgerId());
       ledgerListResponse.setLedgerName(al.getLedgerName());
       ledgerListResponse.setNature(al.getAccountCategory().getNature());
       ledgerListResponse.setNote(al.getNote());  
    }
    
    
    public ItemResponse allLedgerList(){
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteOrderByAccountCategory_CategoryDefaultId(company);
      
      for(AccountLedger al:accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
    
    public ItemResponse cashEquiValanceLedgerList(){
      
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      Integer[] defaultIdsArray={102};
      
      Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
      
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
      
      for(AccountLedger al : accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
   
    public ItemResponse cashEquiValanceAndDfpsLedgerList(){
        
        ItemResponse itemResponse=new ItemResponse();
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
        Integer[] defaultIdsArray={102,103};
        
        Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
        
        List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
        
        for(AccountLedger al : accountLedgers){
         LedgerListResponse ledgerListResponse=new LedgerListResponse();
         setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
         ledgerListResponses.add(ledgerListResponse);
        }
        
        itemResponse.setItem(ledgerListResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return itemResponse;
          
      }
    
    
    public ItemResponse ledgerListForPayment(){
      
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      Integer[] defaultIdsArray={101, 201, 202, 203, 401, 402};
      
      Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
      
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
      
      for(AccountLedger al : accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
    public ItemResponse shortTimeLiabilityList(){
      
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      Integer[] defaultIdsArray={202};
      
      Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
      
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
      
      for(AccountLedger al : accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
    public ItemResponse accountReceivableList(){
      
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      Integer[] defaultIdsArray={103};
      
      Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
      
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
      
      for(AccountLedger al : accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
    public ItemResponse accountPayableList(){
        
        ItemResponse itemResponse=new ItemResponse();
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
        Integer[] defaultIdsArray={204};
        
        Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
        
        List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
        
        for(AccountLedger al : accountLedgers){
         LedgerListResponse ledgerListResponse=new LedgerListResponse();
         setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
         ledgerListResponses.add(ledgerListResponse);
        }
        
        itemResponse.setItem(ledgerListResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return itemResponse;
          
      }
    
    public ItemResponse accountRegularIncomeList(){
        
        ItemResponse itemResponse=new ItemResponse();
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
        Integer[] defaultIdsArray={301};
        
        Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
        
        List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
        
        for(AccountLedger al : accountLedgers){
         LedgerListResponse ledgerListResponse=new LedgerListResponse();
         setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
         ledgerListResponses.add(ledgerListResponse);
        }
        
        itemResponse.setItem(ledgerListResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return itemResponse;
          
      }
    
    
   public ItemResponse ofpsLedgerList(){
        
        ItemResponse itemResponse=new ItemResponse();
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
        Integer[] defaultIdsArray={103};
        
        Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
        
        List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
        
        for(AccountLedger al : accountLedgers){
         LedgerListResponse ledgerListResponse=new LedgerListResponse();
         setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
         ledgerListResponses.add(ledgerListResponse);
        }
        
        itemResponse.setItem(ledgerListResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return itemResponse;
          
      }
    
    
    public ItemResponse ledgerListForReceipt(){
      
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<LedgerListResponse> ledgerListResponses=new ArrayList<>();
      Integer[] defaultIdsArray={201, 202, 203, 301, 302};
      
      Set<Integer> categoryDefaultIds=new HashSet<>(Arrays.asList(defaultIdsArray));
      
      List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryDefaultIdIn(company,categoryDefaultIds);
      
      for(AccountLedger al : accountLedgers){
       LedgerListResponse ledgerListResponse=new LedgerListResponse();
       setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
       ledgerListResponses.add(ledgerListResponse);
      }
      
      itemResponse.setItem(ledgerListResponses);
      itemResponse.setMessage("OK");
      itemResponse.setMessageType(1);
      return itemResponse;
        
    }
    
    
    public ItemResponse incomeLedgerList(){
        
        ItemResponse itemResponse=new ItemResponse();
        Institute institute=UserInfoUtils.getLoggedInInstitute();
        
        List<LedgerListResponse> ledgerListResponses=new ArrayList<>();

        List<AccountLedger> accountLedgers=accountLedgerRepository.findByInstituteAndAccountCategory_CategoryTypeOrderByAccountCategory_CategoryDefaultId(institute, "Income");
        
        for(AccountLedger al : accountLedgers){
         LedgerListResponse ledgerListResponse=new LedgerListResponse();
         setAccountLedgerToLedgerListResponse(al, ledgerListResponse);
         ledgerListResponses.add(ledgerListResponse);
        }
        
  
        
        itemResponse.setItem(ledgerListResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return itemResponse;
          
      }
    
    
    
    public ItemResponse findTransactionJournal(Date fromDate,Date toDate){
    
      ItemResponse itemResponse=new ItemResponse();
      Institute company=UserInfoUtils.getLoggedInInstitute();
      
      List<AccountTransaction> accountTransactions=accountTransactionRepository.findByInstituteAndTrnDateBetweenOrderByTrnDateAscVoucherIdAsc(company, fromDate, toDate);
      List<VoucherResponse> voucherResponses=prepareVoucherResponse(accountTransactions);
    
      itemResponse.setItem(voucherResponses);
      itemResponse.setMessage("ok");
      itemResponse.setMessageType(1);
      
      return itemResponse;
    }
    
    
    public List<VoucherResponse> prepareVoucherResponse(List<AccountTransaction> accountTransactions){
        
        ItemResponse itemResponse=new ItemResponse();
        
        
        List<VoucherResponse> jasperResponses=new ArrayList<>();
        for(AccountTransaction accountTransaction:accountTransactions){
            VoucherResponse voucherResponse=prepareAccountTransactionToVoucherResponse(accountTransaction);
            jasperResponses.add(voucherResponse);
        }
          
        itemResponse.setItem(jasperResponses);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        return jasperResponses;
    }
    
    
    public ItemResponse showVoucherByVoucherId(Long voucherId) {
    	 ItemResponse itemResponse=new ItemResponse();
         Institute company=UserInfoUtils.getLoggedInInstitute();
         
         AccountTransaction accountTransaction=accountTransactionRepository.findByInstituteAndVoucherId(company, voucherId);
         if(accountTransaction==null) {
             itemResponse.setMessage("No Voucher Found");
             itemResponse.setMessageType(0);
             return itemResponse;
        	 
         }
         
         VoucherResponse voucherResponse=prepareAccountTransactionToVoucherResponse(accountTransaction);
         
         itemResponse.setItem(voucherResponse);
         itemResponse.setMessage("OK");
         itemResponse.setMessageType(1);
         return itemResponse;
         
    	
    }
    
    
    public VoucherResponse prepareAccountTransactionToVoucherResponse(AccountTransaction accountTransaction){
        
        VoucherResponse voucherResponse=new VoucherResponse();
        
        voucherResponse.setTrnId(accountTransaction.getTrnId());
        voucherResponse.setEntryBy(accountTransaction.getUserName());
        voucherResponse.setInWord(NumberConverter.numberToWord(accountTransaction.getTrnAmount()));
        voucherResponse.setNote(accountTransaction.getVoucherNote());
        voucherResponse.setTrnDate(new SimpleDateFormat("yyyy-MM-dd").format(accountTransaction.getTrnDate()));
        voucherResponse.setVoucherId(accountTransaction.getVoucherId());
        voucherResponse.setVoucherNo(accountTransaction.getVoucherNo());
        voucherResponse.setVoucherType(accountTransaction.getTrnType());
        voucherResponse.setTrnAmount(accountTransaction.getTrnAmount());
        double totalDebit=0,totalCredit=0;
        
        List<VoucherDetailsResponse>  list=new ArrayList<>();
        
        for(AccountTransactionDetails det : accountTransaction.getAccountTransactionDetailses()){
           VoucherDetailsResponse vdjr=new VoucherDetailsResponse();
           vdjr.setLedgerName(det.getAccountLedger().getLedgerName());
           vdjr.setDebit(det.getDebitAmount());
           vdjr.setCredit(det.getCreditAmount());
           
           list.add(vdjr);
           totalDebit+=det.getDebitAmount();
           totalCredit+=det.getCreditAmount();
           
        }
        
        voucherResponse.setList(list);
        voucherResponse.setTotalDebit(totalDebit);
        voucherResponse.setTotalCredit(totalCredit);
        
        
        return voucherResponse;
        
    }
    
    
    
    public ItemResponse findTrialBalance(Date fromDate,Date toDate){
        
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String fromDateString=df.format(fromDate);
        String toDateString=df.format(toDate);
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<Object[]> trialBalance=accountTransactionDetailsRepository.findTrialBalance(company.getInstituteId(), fromDateString, toDateString);
        Set<String> categoryNames=new LinkedHashSet<>();
        
        for(Object[] obj : trialBalance){
          categoryNames.add(obj[0].toString());
        }
        
        List<TrialBalanceResponse> trialBalanceResponses=new ArrayList<>();
        
        double grandTotalDebit=0,grandTotalCredit=0;
        
        for(String categoryName : categoryNames){
            
            TrialBalanceResponse trialBalanceResponse=new TrialBalanceResponse();
            
            double totalDebit=0,totalCredit=0;
            
            List<TrialBalanceDetailsResponse> trialBalanceDetails=new ArrayList<>();
            
            for(Object[] obj : trialBalance){
          
               String catName=obj[0].toString();
               
               if(catName.equals(categoryName)){
                   
                   TrialBalanceDetailsResponse trialBalanceDetailsResponse=new TrialBalanceDetailsResponse();
                   
                   String ledgerName=obj[1].toString();
                   double debit=Double.parseDouble(obj[2].toString());
                   double credit=Double.parseDouble(obj[3].toString());
                   trialBalanceDetailsResponse.setLedgerName(ledgerName);
                   trialBalanceDetailsResponse.setDebit(debit);
                   trialBalanceDetailsResponse.setCredit(credit);
                   trialBalanceDetails.add(trialBalanceDetailsResponse);
                   totalDebit+=debit;
                   totalCredit+=credit;
               }
  
            }
          
           grandTotalDebit+=totalDebit;
           grandTotalCredit+=totalCredit; 
           
           trialBalanceResponse.setAccountCategoryName(categoryName);
           trialBalanceResponse.setTotalDebit(totalDebit);
           trialBalanceResponse.setTotalCredit(totalCredit);
           trialBalanceResponse.setTrialBalanceDetails(trialBalanceDetails);
               
           trialBalanceResponses.add(trialBalanceResponse);
           
        }
        
        TrialBalanceManager trialBalanceManager=new TrialBalanceManager();
        trialBalanceManager.setInstituteName(company.getInstituteName());
        trialBalanceManager.setInstituteAddress(company.getAddress());
        trialBalanceManager.setFromDate(fromDateString);
        trialBalanceManager.setToDate(toDateString);
        trialBalanceManager.setGrandTotalDebit(grandTotalDebit);
        trialBalanceManager.setGrandTotalCredit(grandTotalCredit);
        trialBalanceManager.setTrialBalanceResponses(trialBalanceResponses);
        
        ItemResponse itemResponse=new ItemResponse(trialBalanceManager);
        itemResponse.setMessage("ok");
        itemResponse.setMessageType(1);
        
        
        return itemResponse;
    }
    
    
    public ItemResponse findBalanceSheet(Date trnDate){
        
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String trnDateString=df.format(trnDate);
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        List<Object[]> balanceSheet=accountTransactionDetailsRepository.findBalanceSheet(company.getInstituteId(), trnDateString);
        Set<String> categoryNames=new LinkedHashSet<>();
        
        for(Object[] obj : balanceSheet){
            
        if(obj[0].toString().equals("Asset") || obj[0].toString().equals("Liability"))
          categoryNames.add(obj[1].toString());
        
        }
        
        List<BalanceSheetResponse> balanceSheetResponses=new ArrayList<>();
        
        
        for(String categoryName : categoryNames){
            
            BalanceSheetResponse balanceSheetResponse=new BalanceSheetResponse();
            
            double totalBalance=0;
            
            List<BalanceSheetDetailsResponse> balanceSheetDetails=new ArrayList<>();
            
            for(Object[] obj : balanceSheet){
          
               String catName=obj[1].toString();
               
               if(catName.equals(categoryName)){
                   
                   BalanceSheetDetailsResponse balanceSheetDetailsResponse=new BalanceSheetDetailsResponse();
                   
                   String ledgerName=obj[2].toString();
                   double balance=Double.parseDouble(obj[3].toString());
                   balanceSheetDetailsResponse.setLedgerName(ledgerName);
                   balanceSheetDetailsResponse.setBalance(balance);
                   balanceSheetDetails.add(balanceSheetDetailsResponse);
                   
                   totalBalance+=balance;                  

               }
  
            }
          
           
           balanceSheetResponse.setCategoryName(categoryName);
           balanceSheetResponse.setTotalBalance(totalBalance);

           balanceSheetResponse.setBalanceSheetDetailsResponses(balanceSheetDetails);
               
           balanceSheetResponses.add(balanceSheetResponse);
           
        }
        
        double income=0,expense=0;
        
        for(Object[] obj : balanceSheet){
            
            String categoryType=obj[0].toString();
            
            if(categoryType.equalsIgnoreCase("Income")){
               income+= Double.parseDouble(obj[3].toString()); 
            }
            
            else if(categoryType.equalsIgnoreCase("Expense")){
               expense+= Double.parseDouble(obj[3].toString());
            }
        }
        
        double profit=income-expense;
        
        BalanceSheetManager balanceSheetManager=new BalanceSheetManager();
        
        if(profit>0){
           balanceSheetManager.setProfitLossLabel("Profit");
           balanceSheetManager.setProfitLossValue(profit);
        }
        
        else if(profit<0){
           balanceSheetManager.setProfitLossLabel("Loss");
           balanceSheetManager.setProfitLossValue(Math.abs(profit));   
        }
        
        
        balanceSheetManager.setInstituteName(company.getInstituteName());
        balanceSheetManager.setInstituteAddress(company.getAddress());
        balanceSheetManager.setTrnDate(trnDateString);
       
        balanceSheetManager.setBalanceSheetResponses(balanceSheetResponses);
        
        ItemResponse itemResponse=new ItemResponse(balanceSheetManager);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        
        
        return itemResponse;
    }
    
    
    public ItemResponse cashSummary(Date fromDate,Date toDate){
        
        
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        Long companyId=company.getInstituteId();
        
        ItemResponse itemResponse=new ItemResponse();
        
        CashSummaryResponse cashSummery=new CashSummaryResponse();
        
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String stringFromDate=df.format(fromDate);
        String stringToDate=df.format(toDate);
        
        List<CashSummaryHelper> incomeList = new ArrayList<>();
        List<CashSummaryHelper> expenseList = new ArrayList<>();
        List<CashSummaryHelper> assetLiabilitiesList = new ArrayList<>();
        List<CashSummaryHelper> openingBalanceList = new ArrayList<>();
        List<CashSummaryHelper> closingBalanceList = new ArrayList<>();
        
        List<Object[]> incomeListQuery ;
        List<Object[]> expenseListQuery;
        List<Object[]> assetLiabilitiesListQuery;
        List<Object[]> openingBalanceListQuery;
        List<Object[]> closingBalanceListQuery;
        
        double totalIncome = 0, totalExpense = 0, operatingSurplusDeficit = 0, totalNonOperatingMovement = 0, netCashMovement = 0, totalOpeningBalance = 0, 
                totalClosingBalance = 0;
        
        incomeListQuery = accountTransactionDetailsRepository.cashSummaryIncomeQuery(companyId, stringFromDate, stringToDate);
        expenseListQuery = accountTransactionDetailsRepository.cashSummaryExpenseQuery(companyId, stringFromDate, stringToDate);
        assetLiabilitiesListQuery = accountTransactionDetailsRepository.cashSummaryAssetLiabilityQuery(companyId, stringFromDate, stringToDate);
        openingBalanceListQuery = accountTransactionDetailsRepository.cashSummaryOpeningBalance(companyId, stringFromDate);
        closingBalanceListQuery = accountTransactionDetailsRepository.cashSummaryClosingBalance(companyId, stringToDate);
        
        for(Object[] ob : incomeListQuery){
            Long ledgerId = Long.parseLong(ob[0].toString());
            String ledgerName = ob[1].toString();
            double amount = Double.parseDouble(ob[2].toString());
            totalIncome += amount;
            incomeList.add(new CashSummaryHelper(ledgerId, ledgerName, amount));
        }
        
        
        for(Object[] ob : expenseListQuery){
            Long ledgerId = Long.parseLong(ob[0].toString());
            String ledgerName = ob[1].toString();
            double amount = Double.parseDouble(ob[2].toString());
            totalExpense += amount;
            expenseList.add(new CashSummaryHelper(ledgerId, ledgerName, amount));
        }
        
        for(Object[] ob : assetLiabilitiesListQuery){
            Long ledgerId = Long.parseLong(ob[0].toString());
            String ledgerName = ob[1].toString();
            double amount = Double.parseDouble(ob[2].toString());
            totalNonOperatingMovement += amount;
            assetLiabilitiesList.add(new CashSummaryHelper(ledgerId, ledgerName, amount));
        }
        
        for(Object[] ob : openingBalanceListQuery){
            Long ledgerId = Long.parseLong(ob[0].toString());
            String ledgerName = ob[1].toString();
            double amount = Double.parseDouble(ob[2].toString());
            totalOpeningBalance += amount;
            openingBalanceList.add(new CashSummaryHelper(ledgerId, ledgerName, amount));
        }
        
        for(Object[] ob : closingBalanceListQuery){
            Long ledgerId = Long.parseLong(ob[0].toString());
            String ledgerName = ob[1].toString();
            double amount = Double.parseDouble(ob[2].toString());
            totalClosingBalance += amount;
            closingBalanceList.add(new CashSummaryHelper(ledgerId, ledgerName, amount));
        }
        
        
        
        operatingSurplusDeficit = totalIncome - totalExpense;
        
        
        netCashMovement = operatingSurplusDeficit + totalNonOperatingMovement;
        
        cashSummery.setIncomeList(incomeList);
        cashSummery.setExpenseList(expenseList);
        cashSummery.setAssetLiabilitiesList(assetLiabilitiesList);
        cashSummery.setOpeningBalanceList(openingBalanceList);
        cashSummery.setClosingBalanceList(closingBalanceList);
        cashSummery.setTotalIncome(totalIncome);
        cashSummery.setTotalExpense(totalExpense);
        cashSummery.setOperatingSurplusDeficit(operatingSurplusDeficit);
        cashSummery.setTotalNonOperatingMovement(totalNonOperatingMovement);
        cashSummery.setNetCashMovement(netCashMovement);
        cashSummery.setTotalOpeningBalance(totalOpeningBalance);
        cashSummery.setTotalClosingBalance(totalClosingBalance);
        
        itemResponse.setItem(cashSummery);
        itemResponse.setMessage("OK");
        itemResponse.setMessageType(1);
        
        
        return itemResponse;
        
    }
    
    
    
    public boolean isfundFlowAddeble(int givenYear,String month){
        
         LocalDate today = LocalDate.now();
         
         int givenMonthValue=Month.valueOf(month.toUpperCase()).getValue();
         
         int currentmonthno = today.getMonthValue();
         
         int currentYear=Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
         
         if(givenYear<currentYear){
             return true;
         }
         
         if(givenMonthValue<=currentmonthno){
             return true;
         }
         
         return false;
     }
    
    public ItemResponse fundsFlow(int year) {
        
        Institute company=UserInfoUtils.getLoggedInInstitute();
         
         ItemResponse itemResponse = new ItemResponse();
     
         FundsFlowResponse fundFlow=new FundsFlowResponse();
         
         DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
         
         List<MonthInfo> monthInfos=CommonInfoUtils.getMonthNameWithFirstDateAndLastDate(year);
         
         Date toDate=new Date();
         for(MonthInfo mi:monthInfos){
             toDate=mi.getLastDate();
         }
         
         Date fromDate=new Date();
         for(MonthInfo mi:monthInfos){
             fromDate=mi.getFirstDate();
             break;
         }
         
        List<Object[]> accountTransactionVouchers= accountTransactionDetailsRepository.fundsFlowQuery(company.getInstituteId(), dateFormat.format(toDate));
       
        
       
        double openingBalance=0,closingBalance=0,fundFlowBalance=0,debit=0,credit=0;
        
        try{
        
        Date trnDate;
        for(Object[] v:accountTransactionVouchers){
             trnDate=dateFormat.parse(v[0].toString());
            
             if(trnDate.before(fromDate)){
               debit+=Double.parseDouble(v[1].toString());
               credit+=Double.parseDouble(v[2].toString());
            }
         }
        
        openingBalance=AccountInfoUtils.round(debit-credit, 2) ;
        fundFlow.setTotalOpeningBalance(openingBalance);
        
        
        List<FundsFlowHelper> fundFlows=new ArrayList<>();
        
        for(MonthInfo mi:monthInfos){
            
            debit=0;credit=0;
             
          for(Object[] v:accountTransactionVouchers){
              
              trnDate=dateFormat.parse(v[0].toString());
              
              if(trnDate.before(mi.getLastDate()) || trnDate.equals(mi.getLastDate())){
                  
                  if(trnDate.after(mi.getFirstDate()) || trnDate.equals(mi.getFirstDate())){
                    debit+=Double.parseDouble(v[1].toString());
                    credit+=Double.parseDouble(v[2].toString());  
                  }
              }
              

          }
          
          closingBalance=AccountInfoUtils.round(openingBalance+(debit-credit), 2) ;
          fundFlowBalance=closingBalance-openingBalance;
          fundFlowBalance=AccountInfoUtils.round(fundFlowBalance, 2) ;
          
          if(isfundFlowAddeble(year,mi.getMonthName())){
          fundFlows.add(new FundsFlowHelper(mi.getMonthName(), openingBalance, closingBalance, fundFlowBalance));
          }
          
          openingBalance = closingBalance; 
         
        }
        
        fundFlow.setTotalClosingBalance(closingBalance);
        double totalFundsFlow=fundFlow.getTotalClosingBalance()-fundFlow.getTotalOpeningBalance();
        fundFlow.setTotalFundsFlow(AccountInfoUtils.round(totalFundsFlow, 2));
        fundFlow.setYear(year);
        
        fundFlow.setFundFlows(fundFlows);
        
        
        itemResponse = new ItemResponse(fundFlow);
        itemResponse.setMessageType(1);
        itemResponse.setMessage("OK");
        
        }catch(Exception ex){
        itemResponse.setMessageType(0);
        itemResponse.setMessage("Failed");
            
        }
        
        return itemResponse;
    }
    
    
    public ItemResponse incomeStatement(Date fromDate, Date toDate) {
        
        double totalIncome=0,totalExpense=0,totalDebit=0,totalCredit=0,totalIncomeDebit=0,totalIncomeCredit=0,totalExpenseDebit=0,totalExpenseCredit=0;
        double profit=0,loss=0;
        String profitLossLedgerName="";
        Institute company=UserInfoUtils.getLoggedInInstitute();
        
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String fromDateString=df.format(fromDate);
        String toDateString=df.format(toDate);
        List<Object[]> incomeListObj =accountTransactionDetailsRepository.findIncomeExpenseListForIncomeStatement(company.getInstituteId(), fromDateString, toDateString, "Income");
        List<Object[]> expenseListObj =accountTransactionDetailsRepository.findIncomeExpenseListForIncomeStatement(company.getInstituteId(), fromDateString, toDateString, "Expense");
        
        List<IncomeStatementHelper> incomeList=new ArrayList<>();
        List<IncomeStatementHelper> expenseList=new ArrayList<>();
        List<IncomeStatementHelper> profitLossList=new ArrayList<>();
        
        for(Object[] obj:incomeListObj){
          
          Long ledgerId=Long.parseLong(obj[0].toString());
          String ledgerName=String.valueOf(obj[1]);
          double credit=AccountInfoUtils.round(Double.parseDouble(obj[2].toString()),2);
          double debit=AccountInfoUtils.round(Double.parseDouble(obj[3].toString()),2);
          
          if(credit>debit){
            credit=credit-debit;
            debit=0;
          }else{
            debit=debit-credit;
            credit=0;  
          }
          
          totalIncomeCredit+=credit;
          totalIncomeDebit+=debit;
          
          incomeList.add(new IncomeStatementHelper(ledgerId, ledgerName, debit, credit));
          
        }
        
        
        
        
        for(Object[] obj:expenseListObj){
          
          Long ledgerId=Long.parseLong(obj[0].toString());
          String ledgerName=String.valueOf(obj[1]);
          double credit=AccountInfoUtils.round(Double.parseDouble(obj[2].toString()),2);
          double debit=AccountInfoUtils.round(Double.parseDouble(obj[3].toString()),2);
          
          if(credit>debit){
            credit=credit-debit;
            debit=0;
          }else{
            debit=debit-credit;
            credit=0;  
          }
          
          totalExpenseCredit+=credit;
          totalExpenseDebit+=debit;
          
          expenseList.add(new IncomeStatementHelper(ledgerId, ledgerName, debit, credit));
          
        }
        
        
        totalDebit+=totalIncomeDebit+totalExpenseDebit;
        totalCredit+=totalIncomeCredit+totalExpenseCredit;
        
        totalIncome=totalIncomeCredit-totalIncomeDebit;       
        totalExpense=totalExpenseDebit-totalExpenseCredit;
        
        if(totalIncome>totalExpense){
            profit=totalIncome-totalExpense;
            profit=AccountInfoUtils.round(profit,2);
            profitLossLedgerName="Profit";
            totalDebit+=profit;
            
        }else{
            loss=totalExpense-totalIncome;
            loss=AccountInfoUtils.round(loss,2);
            profitLossLedgerName="Loss";
            totalCredit+=loss;
        }
        
        
        
        profitLossList.add(new IncomeStatementHelper(0l, profitLossLedgerName, profit, loss));
        
        
        IncomeStatementResponse incomeStatementResponse=new IncomeStatementResponse();
        incomeStatementResponse.setExpenseList(expenseList);
        incomeStatementResponse.setIncomeList(incomeList);
        incomeStatementResponse.setProfitLosList(profitLossList);
        incomeStatementResponse.setTotalCredit(AccountInfoUtils.round(totalCredit,2));
        incomeStatementResponse.setTotalDebit(AccountInfoUtils.round(totalDebit,2));
        incomeStatementResponse.setTotalExpense(totalExpense);
        incomeStatementResponse.setTotalIncome(totalIncome);
        
        
        ItemResponse itemResponse = new ItemResponse(incomeStatementResponse);
        itemResponse.setMessageType(1);
        itemResponse.setMessage("Ok");
        return itemResponse;
    }
    
    
    
    
    public ItemResponse cashBook(Date fromDate, Date toDate, List<Long> cashLedgerList) {
    	
    	ItemResponse itemResponse = new ItemResponse();
    	CashBookView cashBookView = new CashBookView();
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	
    	List<AccountTransactionDetails> accountTrnDetails = accountTransactionDetailsRepository.findByInstituteAndAccountTransaction_TrnDateBetween(institute, fromDate, toDate);
    	List<AccountLedger> accountLedgers = accountLedgerRepository.findByInstituteAndLedgerIdIn(institute, cashLedgerList); 
    	
    	List<CashBookViewHelper1> details1= new ArrayList<>();
    	double totalDebit = 0;
    	double totalCredit = 0;
    	
    	for(AccountLedger ledger : accountLedgers) {
    		
    		CashBookViewHelper1 detail1 = new CashBookViewHelper1();
    		
    		detail1.setLedgerId(ledger.getLedgerId());
    		detail1.setLedgerName(ledger.getLedgerName());
    		
    		Map<Long, CashBookViewHelper2> map = new HashMap<>();
    		
    		
    		Set<AccountTransaction> accountTransactions= new LinkedHashSet<>();
    		
    		for(AccountTransactionDetails det : accountTrnDetails) {
    			
    			if(det.getAccountLedger().getLedgerId().equals(ledger.getLedgerId())) {
    				accountTransactions.add(det.getAccountTransaction());	
    			}
    		}
    		
    		
    		
    		for(AccountTransaction at : accountTransactions) {
    			
    			Set<AccountTransactionDetails> detailTransactions = at.getAccountTransactionDetailses();
    			
    			for(AccountTransactionDetails det2 : detailTransactions) {
    				
    				if(!det2.getAccountLedger().getLedgerId().equals(ledger.getLedgerId())) {
    					
    					if(map.containsKey(det2.getAccountLedger().getLedgerId())) {
    						
    						CashBookViewHelper2 helper2= map.get(det2.getAccountLedger().getLedgerId());
    						
    						helper2.setCreditAmount(helper2.getCreditAmount()+det2.getCreditAmount());
    						helper2.setDebitAmount(helper2.getDebitAmount()+det2.getDebitAmount());
    						map.put(det2.getAccountLedger().getLedgerId(), helper2);
    						
    						totalDebit+=det2.getDebitAmount();
    						totalCredit+=det2.getCreditAmount();
    						
    					}else {
    						
    						CashBookViewHelper2 helper2=new CashBookViewHelper2();
    						helper2.setLedgerId(det2.getAccountLedger().getLedgerId());
    						helper2.setLedgerName(det2.getAccountLedger().getLedgerName());
    						helper2.setCreditAmount(det2.getCreditAmount());
    						helper2.setDebitAmount(det2.getDebitAmount());
    						map.put(det2.getAccountLedger().getLedgerId(), helper2);
    						
    						totalDebit+=det2.getDebitAmount();
    						totalCredit+=det2.getCreditAmount();
    						
    					}
    				}
    				
    			}
    		}
    		
    		detail1.setInsideDetails(new ArrayList<CashBookViewHelper2>(map.values()));
    		
    		details1.add(detail1);
    		
    		
    	}
    	
    	
    	cashBookView.setDetails(details1);
    	cashBookView.setTotalCredit(totalCredit);
    	cashBookView.setTotalDebit(totalDebit);
    	
    	itemResponse.setItem(cashBookView);
    	itemResponse.setMessage("OK");
    	itemResponse.setMessageType(1);
    	return itemResponse;
    }
    
    
    
 
    public ItemResponse ledgerBook(Date fromDate, Date toDate, Long ledgerId) {
    	
    	ItemResponse itemResponse = new ItemResponse();
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	
    	List<AccountTransactionDetails> accountTrnDetails = accountTransactionDetailsRepository.findByInstituteAndAccountTransaction_TrnDateBetweenAndAccountLedger_LedgerId(institute, fromDate, toDate, ledgerId);
 
    	List<LedgerBookView> views = new ArrayList<>();
    	
    	for(AccountTransactionDetails detail : accountTrnDetails) {
    		
    		LedgerBookView view = new LedgerBookView();
    		
    		view.setCrAmount(detail.getCreditAmount());
    		view.setDrAmount(detail.getDebitAmount());
    		view.setLedgerName(detail.getAccountLedger().getLedgerName());
    		view.setTrnDate(detail.getAccountTransaction().getTrnDate());
    		
    		if(detail.getCreditAmount()>0) {
    			view.setType("Credit");	
    		}else {
    			view.setType("Debit");	
    		}
    		
    		view.setUserName(detail.getAccountTransaction().getUserName());
    		view.setVoucherId(detail.getAccountTransaction().getTrnId());
    		view.setVoucherNo(detail.getAccountTransaction().getVoucherNo());
    		view.setVoucherType(detail.getAccountTransaction().getTrnType());
 
    		views.add(view);
    		
    	}
    	
    	itemResponse.setItem(views);
    	itemResponse.setMessage("OK");
    	itemResponse.setMessageType(1);
    	return itemResponse;
    }
    
    
    

    public ItemResponse userWiseCollectionView(Date fromDate, Date toDate) {
    	
    	ItemResponse itemResponse = new ItemResponse();
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        String fromDateString=df.format(fromDate);
        String toDateString=df.format(toDate);
    	
    	List<Object[]> accountTrnDetails = accountTransactionDetailsRepository.userWiseCollectionView(institute.getInstituteId(), fromDateString, toDateString);
 
    	List<UserWiseCollectionView> views = new ArrayList<>();
    	
    	for(Object[] obj : accountTrnDetails) {
    		
    		UserWiseCollectionView view = new UserWiseCollectionView();
    		
    		view.setUserName(obj[0]+"");
    		view.setLedgerName(obj[1]+"");
    		view.setCredit(Double.parseDouble(obj[2]+""));
    		view.setDebit(Double.parseDouble(obj[3]+""));
 
    		views.add(view);
    	}
    	
    	itemResponse.setItem(views);
    	itemResponse.setMessage("OK");
    	itemResponse.setMessageType(1);
    	return itemResponse;
    }
    
    

    
    
    public ItemResponse voucherListByTrnType(Date fromDate, Date toDate, String trnType) {
    	
    	ItemResponse itemResponse = new ItemResponse();
    	
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	
    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    	List<AccountTransaction> accountTransactions = accountTransactionRepository.findByInstituteAndTrnTypeAndTrnDateBetweenOrderByTrnDateAscVoucherIdAsc(institute, trnType, fromDate, toDate);

    	List<VoucherView> views = new ArrayList<>();
    	
    	for(AccountTransaction at : accountTransactions) {
    		
    		VoucherView view = new VoucherView();
    		
    		view.setTrnAmount(at.getTrnAmount());
    		view.setTrnDate(df.format(at.getTrnDate()));
    		view.setTrnId(at.getTrnId());
    		view.setTrnType(at.getTrnType());
    		view.setUserName(at.getUserName());
    		view.setVoucherId(at.getVoucherId());
    		view.setVoucherNo(at.getVoucherNo());
    		view.setVoucherNote(at.getVoucherNote());
    		
    		views.add(view);
    		
    	}
    	    	
    	itemResponse.setItem(views);
    	itemResponse.setMessage("OK");
    	itemResponse.setMessageType(1);
    	return itemResponse;
    }
}
