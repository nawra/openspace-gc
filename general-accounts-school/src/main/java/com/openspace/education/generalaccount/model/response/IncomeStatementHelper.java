/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

/**
 *
 * @author riad
 */
public class IncomeStatementHelper {
    
    private Long ledgerId;
    private String ledgerName;
    private double debitAmount;
    private double creditAmount;

    public IncomeStatementHelper(Long ledgerId, String ledgerName, double debitAmount, double creditAmount) {
        this.ledgerId = ledgerId;
        this.ledgerName = ledgerName;
        this.debitAmount = debitAmount;
        this.creditAmount = creditAmount;
    }

    public Long getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public double getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(double creditAmount) {
        this.creditAmount = creditAmount;
    }
    
    
    
}
