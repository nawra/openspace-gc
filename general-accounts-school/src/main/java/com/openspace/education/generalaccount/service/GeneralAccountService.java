/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.openspace.education.generalaccount.service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.entity.AccountCategory;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.model.request.LedgerCreateRequest;
import com.openspace.education.generalaccount.model.request.LedgerUpdateRequest;
import com.openspace.education.generalaccount.repository.AccountCategoryRepository;
import com.openspace.education.generalaccount.repository.AccountLedgerRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author riad
 */

@Service
public class GeneralAccountService {
    
	Logger logger = LoggerFactory.getLogger(GeneralAccountService.class);
    
    @Autowired
    public AccountCategoryRepository accountCategoryRepository;
    
    @Autowired
    public AccountLedgerRepository accountLedgerRepository;
    
    
    @Transactional
    public BaseResponse createLedger(LedgerCreateRequest request){
    	
       BaseResponse baseResponse=new BaseResponse(); 
       Long companyId=UserInfoUtils.getLoggedInInstituteId();
       Institute company=UserInfoUtils.getLoggedInInstitute();
       
       if(accountLedgerRepository.findByLedgerNameAndInstitute_InstituteId(request.getLedgerName(), companyId)!=null){
         baseResponse.setMessage(""+request.getLedgerName()+" Already exists.");
         baseResponse.setMessageType(0);
         return baseResponse;
       }
       
       AccountCategory accountCategory=accountCategoryRepository.getOne(request.getCategoryId());
       if(accountCategory==null){
         baseResponse.setMessage("This category not found.");
         baseResponse.setMessageType(0);
         return baseResponse;  
       }
       
       
       AccountLedger accountLedger=new AccountLedger();
       
       accountLedger.setLedgerName(request.getLedgerName());
       accountLedger.setNote(request.getLedgerNote());
       accountLedger.setAccountCategory(accountCategory);
       accountLedger.setInstitute(company);
       
       accountLedgerRepository.save(accountLedger);
        
       baseResponse.setMessage("Ledger Successfully Created");
       baseResponse.setMessageType(1);
       
       return baseResponse;
        
    }
    
    
    public boolean isLedgerNameExistsInLedgerList(List<AccountLedger> ledgers,String ledgername) {
    	
    	for(AccountLedger al:ledgers) {
    		if(al.getLedgerName().equals(ledgername)) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    @Transactional
    public BaseResponse updateLedger(LedgerUpdateRequest request){
       
       BaseResponse baseResponse=new BaseResponse(); 
       Institute company=UserInfoUtils.getLoggedInInstitute();
       
       AccountLedger accountLedger=accountLedgerRepository.findByLedgerIdAndInstitute(request.getLedgerId(), company);
       List<AccountLedger> ledgers=accountLedgerRepository.findByLedgerIdNotAndInstitute(request.getLedgerId(), company);
       AccountCategory accountCategory=accountCategoryRepository.getOne(request.getAccountCategoryId());
       
       if(accountLedger==null){
        baseResponse.setMessage("Ledger not found");
        baseResponse.setMessageType(0);
        return baseResponse;
       }
       
       if(accountLedger.getAccountCategory().getCategoryDefaultId().equals(103)){
           baseResponse.setMessage("Ledger can not be changed.");
           baseResponse.setMessageType(0);
           return baseResponse;
          }
       
       if(isLedgerNameExistsInLedgerList(ledgers, request.getLedgerName())) {
    	   baseResponse.setMessage("Ledger name already exists.");
           baseResponse.setMessageType(0);
           return baseResponse; 
       }
       
       if(accountCategory==null){
         baseResponse.setMessage("Account category not found");
         baseResponse.setMessageType(0); 
         return baseResponse;
       }
       
       if(!accountLedger.getAccountCategory().getNature().equals(accountCategory.getNature())){
         baseResponse.setMessage("Nature does not match.");
         baseResponse.setMessageType(0); 
         return baseResponse;  
       }
       
       accountLedger.setLedgerName(request.getLedgerName());
       accountLedger.setNote(request.getNote());
       accountLedger.setAccountCategory(accountCategory);
        
       baseResponse.setMessage("Ledger successfully updated");
       baseResponse.setMessageType(1);
       
       return baseResponse;
        
    }
    
    

    public BaseResponse deleteLedger(Long ledgerId){
       
       BaseResponse baseResponse=new BaseResponse(); 
       Institute company=UserInfoUtils.getLoggedInInstitute();
       
       AccountLedger accountLedger=accountLedgerRepository.findByLedgerIdAndInstitute(ledgerId, company);

       
       if(accountLedger==null){
        baseResponse.setMessage("Ledger not found");
        baseResponse.setMessageType(0);
        return baseResponse;
       }
       
       try{
           
       accountLedgerRepository.delete(accountLedger);
       
       }catch(DataIntegrityViolationException e){
          logger.error(e.getMessage());
          baseResponse.setMessage("This ledger is assigned in another configuration");
          baseResponse.setMessageType(0);
          return baseResponse;
       }
       catch(Exception e){
          logger.error(e.getMessage());
          baseResponse.setMessage("Operation Failed");
          baseResponse.setMessageType(0);
          return baseResponse;
       }
       
       
       baseResponse.setMessage("Ledger successfully deleted");
       baseResponse.setMessageType(1);
       
       return baseResponse;
        
    }
    
    
    
    
}
