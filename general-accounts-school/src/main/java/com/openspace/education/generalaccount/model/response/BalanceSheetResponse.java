/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class BalanceSheetResponse {
    
    private String categoryName;
    private double totalBalance;
    private List<BalanceSheetDetailsResponse> balanceSheetDetailsResponses;
    

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

   

    public List<BalanceSheetDetailsResponse> getBalanceSheetDetailsResponses() {
        return balanceSheetDetailsResponses;
    }

    public void setBalanceSheetDetailsResponses(List<BalanceSheetDetailsResponse> balanceSheetDetailsResponses) {
        this.balanceSheetDetailsResponses = balanceSheetDetailsResponses;
    }
    
    
}
