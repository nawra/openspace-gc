/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.CommonInfoUtils;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.NumberConverter;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.dto.AccountTransactionDetailsDto;
import com.openspace.education.generalaccount.model.dto.AccountTransactionDto;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.generalaccount.model.entity.AccountTransactionDetails;
import com.openspace.education.generalaccount.model.request.ContraRequest;
import com.openspace.education.generalaccount.model.request.JournalRequest;
import com.openspace.education.generalaccount.model.request.JournalRequestDetails;
import com.openspace.education.generalaccount.model.request.ReceiptPaymentRequest;
import com.openspace.education.generalaccount.model.request.ReceiptPaymentRequestDetails;
import com.openspace.education.generalaccount.model.utils.AccountInfoUtils;
import com.openspace.education.generalaccount.repository.AccountTransactionRepository;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author riad
 */

@Service
public class GeneralAccountTransactionService {
	
	
	private static final Logger logger = Logger.getLogger(GeneralAccountTransactionService.class);
    
    @Autowired
    public AccountTransactionRepository accountTransactionRepository;
    
    public Long findMaxVoucherId(Long instituteId){
        
        return accountTransactionRepository.searchCompanyMaxVoucherId(instituteId);
    }
    
    public void setAccountTransactionVoucher(AccountTransactionDto accountTransactionDto,Institute institute,AccountTransaction accountTransaction,ItemResponse itemResponse){
      
        double trnAmount=0,debit=0,credit=0;
        trnAmount=NumberConverter.round(accountTransactionDto.getTrnAmount(), 2) ;
        
   
        accountTransaction.setInstitute(institute);
        accountTransaction.setModuleId(accountTransactionDto.getModuleId());
        accountTransaction.setTrnAmount(trnAmount);
        accountTransaction.setTrnDate(accountTransactionDto.getTrnDate());
        accountTransaction.setTrnTime(new Date());
        accountTransaction.setTrnType(accountTransactionDto.getTranType());
        accountTransaction.setUserName(accountTransactionDto.getUserName());
        accountTransaction.setVoucherId(accountTransactionDto.getVoucherId());
        accountTransaction.setVoucherNo(accountTransactionDto.getVoucherNo());
        accountTransaction.setVoucherNote(accountTransactionDto.getVoucherNote());
        
        
        Set<AccountTransactionDetails> accountTransactionDetails=new LinkedHashSet<>();
        
        int add=1;
        
        for(AccountTransactionDetailsDto outerObj : accountTransactionDto.getAccountTransactionDetailsDtos()){
            
            add=1;
            
            for(AccountTransactionDetails innerObj:accountTransactionDetails){
                
                if(innerObj.getAccountLedger().getLedgerId().equals(outerObj.getLedgerId())){
                    innerObj.setCreditAmount(innerObj.getCreditAmount()+outerObj.getCreditAmount());
                    innerObj.setDebitAmount(innerObj.getDebitAmount()+outerObj.getDebitAmount());
                    add=0;
                    
                    break;
                }
            }
            
            if(add==1){
              
              AccountTransactionDetails details=new AccountTransactionDetails();
              AccountLedger accountLedger=new AccountLedger();
              accountLedger.setLedgerId(outerObj.getLedgerId());
              details.setAccountLedger(accountLedger);
              details.setAccountTransaction(accountTransaction);
              details.setInstitute(institute);
              details.setCreditAmount(outerObj.getCreditAmount());
              details.setDebitAmount(outerObj.getDebitAmount());
              
              accountTransactionDetails.add(details);
              
              
            }
            
            debit+=outerObj.getDebitAmount();
            credit+=outerObj.getCreditAmount();
        }
        
        accountTransaction.setAccountTransactionDetailses(accountTransactionDetails);
        
        
        debit  = NumberConverter.round(debit, 2);
        credit = NumberConverter.round(credit, 2);
        
        if(trnAmount==debit && debit==credit && trnAmount>0){
          itemResponse.setMessageType(CommonInfoUtils.SUCCESS_STATUS);  
          
        }else{
          itemResponse.setMessageType(CommonInfoUtils.ERROR_STATUS);
          logger.info("trnAmount= "+trnAmount+" debit= "+debit+" credit="+credit);
          itemResponse.setMessage("Transaction Amount Mismatch or Invalid");
        }
        
    }
    
    
    public ItemResponse doReceiptVoucher(ReceiptPaymentRequest receiptRequest){
              
        ItemResponse itemResponse=new ItemResponse();
        
        String userName=UserInfoUtils.getLoggedInUserName();
        Institute institute=UserInfoUtils.getLoggedInInstitute();
        AccountTransactionDto accountTransactionDto=new AccountTransactionDto();
        Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos=new LinkedHashSet<>();
        Long voucherId=findMaxVoucherId(institute.getInstituteId())+1;
        
        accountTransactionDto.setModuleId(AccountInfoUtils.GENERAL_ACCOUNTS_MODULE_ID);
        accountTransactionDto.setTranType(AccountInfoUtils.RECEIPT_TRNSACTION_TYPE);
        accountTransactionDto.setTrnAmount(receiptRequest.getTrnAmount());
        accountTransactionDto.setTrnDate(receiptRequest.getTranDate());
        accountTransactionDto.setUserName(userName);
        accountTransactionDto.setVoucherId(voucherId);
        accountTransactionDto.setVoucherNo(receiptRequest.getVoucherNo());
        accountTransactionDto.setVoucherNote(receiptRequest.getVoucherNote());
        
        AccountTransactionDetailsDto dto1=new AccountTransactionDetailsDto();
        dto1.setLedgerId(receiptRequest.getPaymetOrReceiptLedgerId());
        dto1.setDebitAmount(receiptRequest.getTrnAmount());
        dto1.setCreditAmount(0);
        accountTransactionDetailsDtos.add(dto1);
        
        for(ReceiptPaymentRequestDetails det : receiptRequest.getRequestDetailses()){
          AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
          dto2.setLedgerId(det.getLedgerId());
          dto2.setCreditAmount(det.getLedgerAmount());
          dto2.setDebitAmount(0);
          accountTransactionDetailsDtos.add(dto2);
        }
        
        accountTransactionDto.setAccountTransactionDetailsDtos(accountTransactionDetailsDtos);
        
        AccountTransaction accountTransaction=new AccountTransaction();
        setAccountTransactionVoucher(accountTransactionDto, institute, accountTransaction, itemResponse);
        
        if(itemResponse.getMessageType()==0){
            return itemResponse;
        }
        
        accountTransaction=accountTransactionRepository.save(accountTransaction);
        itemResponse.setItem(accountTransaction.getTrnId());
        itemResponse.setMessage("Receipt voucher successfully done.");
        return itemResponse;
    }
    
    
    public ItemResponse doPaymentVoucher(ReceiptPaymentRequest receiptRequest){
              
        ItemResponse itemResponse=new ItemResponse();
        
        String userName=UserInfoUtils.getLoggedInUserName();
        Institute campany=UserInfoUtils.getLoggedInInstitute();
        AccountTransactionDto accountTransactionDto=new AccountTransactionDto();
        Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos=new LinkedHashSet<>();
        Long voucherId=findMaxVoucherId(campany.getInstituteId())+1;
        
        accountTransactionDto.setModuleId(AccountInfoUtils.GENERAL_ACCOUNTS_MODULE_ID);
        accountTransactionDto.setTranType(AccountInfoUtils.PAYNENT_TRNSACTION_TYPE);
        accountTransactionDto.setTrnAmount(receiptRequest.getTrnAmount());
        accountTransactionDto.setTrnDate(receiptRequest.getTranDate());
        accountTransactionDto.setUserName(userName);
        accountTransactionDto.setVoucherId(voucherId);
        accountTransactionDto.setVoucherNo(receiptRequest.getVoucherNo());
        accountTransactionDto.setVoucherNote(receiptRequest.getVoucherNote());
        
        AccountTransactionDetailsDto dto1=new AccountTransactionDetailsDto();
        dto1.setLedgerId(receiptRequest.getPaymetOrReceiptLedgerId());
        dto1.setDebitAmount(0);
        dto1.setCreditAmount(receiptRequest.getTrnAmount());
        accountTransactionDetailsDtos.add(dto1);
        
        for(ReceiptPaymentRequestDetails det : receiptRequest.getRequestDetailses()){
          AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
          dto2.setLedgerId(det.getLedgerId());
          dto2.setDebitAmount(det.getLedgerAmount());
          dto2.setCreditAmount(0);
          accountTransactionDetailsDtos.add(dto2);
        }
        
        accountTransactionDto.setAccountTransactionDetailsDtos(accountTransactionDetailsDtos);
        
        AccountTransaction accountTransaction=new AccountTransaction();
        setAccountTransactionVoucher(accountTransactionDto, campany, accountTransaction, itemResponse);
        
        if(itemResponse.getMessageType()==0){
            return itemResponse;
        }
        
        accountTransaction=accountTransactionRepository.save(accountTransaction);
        itemResponse.setItem(accountTransaction.getTrnId());
        itemResponse.setMessage("Payment voucher successfully done.");
        return itemResponse;
    }
    
    
    
    public ItemResponse doContraVoucher(ContraRequest contraRequest){
              
        ItemResponse itemResponse=new ItemResponse();
        
        String userName=UserInfoUtils.getLoggedInUserName();
        Institute campany=UserInfoUtils.getLoggedInInstitute();
        AccountTransactionDto accountTransactionDto=new AccountTransactionDto();
        Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos=new LinkedHashSet<>();
        Long voucherId=findMaxVoucherId(campany.getInstituteId())+1;
        
        accountTransactionDto.setModuleId(AccountInfoUtils.GENERAL_ACCOUNTS_MODULE_ID);
        accountTransactionDto.setTranType(AccountInfoUtils.CONTRA_TRNSACTION_TYPE);
        accountTransactionDto.setTrnAmount(contraRequest.getTrnAmount());
        accountTransactionDto.setTrnDate(contraRequest.getTrnDate());
        accountTransactionDto.setUserName(userName);
        accountTransactionDto.setVoucherId(voucherId);
        accountTransactionDto.setVoucherNo(contraRequest.getVoucherNo());
        accountTransactionDto.setVoucherNote(contraRequest.getVoucherNote());
        
        AccountTransactionDetailsDto dto1=new AccountTransactionDetailsDto();
        dto1.setLedgerId(contraRequest.getFromLedgerId());
        dto1.setDebitAmount(0);
        dto1.setCreditAmount(contraRequest.getTrnAmount());
        accountTransactionDetailsDtos.add(dto1);
        
        
        AccountTransactionDetailsDto dto2=new AccountTransactionDetailsDto();
        dto2.setLedgerId(contraRequest.getToLedgerId());
        dto2.setDebitAmount(contraRequest.getTrnAmount());
        dto2.setCreditAmount(0);
        accountTransactionDetailsDtos.add(dto2);     
        
        accountTransactionDto.setAccountTransactionDetailsDtos(accountTransactionDetailsDtos);
        
        AccountTransaction accountTransaction=new AccountTransaction();
        setAccountTransactionVoucher(accountTransactionDto, campany, accountTransaction, itemResponse);
        
        if(itemResponse.getMessageType()==0){
            return itemResponse;
        }
        
        accountTransaction=accountTransactionRepository.save(accountTransaction);
        itemResponse.setItem(accountTransaction.getTrnId());
        itemResponse.setMessage("Contra voucher successfully done.");
        return itemResponse;
    }
    
    
    
    public ItemResponse doJournalVoucher(JournalRequest journalRequest){
              
        ItemResponse itemResponse=new ItemResponse();
        
        String userName=UserInfoUtils.getLoggedInUserName();
        Institute campany=UserInfoUtils.getLoggedInInstitute();
        AccountTransactionDto accountTransactionDto=new AccountTransactionDto();
        Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos=new LinkedHashSet<>();
        Long voucherId=findMaxVoucherId(campany.getInstituteId())+1;
        
        accountTransactionDto.setModuleId(AccountInfoUtils.GENERAL_ACCOUNTS_MODULE_ID);
        accountTransactionDto.setTranType(AccountInfoUtils.JOURNAL_TRNSACTION_TYPE);
        accountTransactionDto.setTrnAmount(journalRequest.getTrnAmount());
        accountTransactionDto.setTrnDate(journalRequest.getTranDate());
        accountTransactionDto.setUserName(userName);
        accountTransactionDto.setVoucherId(voucherId);
        accountTransactionDto.setVoucherNo(journalRequest.getVoucherNo());
        accountTransactionDto.setVoucherNote(journalRequest.getVoucherNote());
        
        for(JournalRequestDetails det:journalRequest.getDetailList()){
          AccountTransactionDetailsDto dto=new AccountTransactionDetailsDto();
          dto.setLedgerId(det.getLedgerId());
          dto.setCreditAmount(det.getCreditAmount());
          dto.setDebitAmount(det.getDebitAmount());
          accountTransactionDetailsDtos.add(dto);
        }
       
        accountTransactionDto.setAccountTransactionDetailsDtos(accountTransactionDetailsDtos);
        
        AccountTransaction accountTransaction=new AccountTransaction();
        setAccountTransactionVoucher(accountTransactionDto, campany, accountTransaction, itemResponse);
        
        if(itemResponse.getMessageType()==0){
            return itemResponse;
        }
        
        accountTransaction=accountTransactionRepository.save(accountTransaction);
        itemResponse.setItem(accountTransaction.getTrnId());
        itemResponse.setMessage("Journal voucher successfully done.");
        return itemResponse;
    }
    
    
//    public BaseResponse deleteVoucher(Long voucherId){
//              
//        BaseResponse baseResponse=new BaseResponse();
//      
//        Company campany=UserInfoUtils.getLoggedInCompany();
//
//        AccountTransaction accountTransaction = accountTransactionRepository.findByCompanyAndVoucherId(campany, voucherId);
//      
//        if(accountTransaction == null){
//          baseResponse.setMessage("Voucher not found"); 
//          baseResponse.setMessageType(0);
//        }else{     
//         accountTransactionRepository.delete(accountTransaction);
//         baseResponse.setMessage("voucher successfully deleted.");
//         baseResponse.setMessageType(1);
//        }
//        
//       return baseResponse;
//    }
    
    public BaseResponse deleteVoucher(Long trnId){
        
        BaseResponse baseResponse=new BaseResponse();
      
        Institute campany=UserInfoUtils.getLoggedInInstitute();

        AccountTransaction accountTransaction = accountTransactionRepository.findByInstituteAndTrnId(campany, trnId);
      
        if(accountTransaction == null){
          baseResponse.setMessage("Voucher not found"); 
          baseResponse.setMessageType(0);
        }else{     
         accountTransactionRepository.delete(accountTransaction);
         baseResponse.setMessage("voucher successfully deleted.");
         baseResponse.setMessageType(1);
        }
        
       return baseResponse;
    }
}
