/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class LedgerCreateRequest {
    
    @NotNull
    private String ledgerName;
    
    private String ledgerNote;
    
    @NotNull
    private Long categoryId;

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getLedgerNote() {
        return ledgerNote;
    }

    public void setLedgerNote(String ledgerNote) {
        this.ledgerNote = ledgerNote;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
    
    
    
}
