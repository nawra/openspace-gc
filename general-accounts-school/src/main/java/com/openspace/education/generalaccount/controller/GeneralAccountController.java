/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.controller;

import com.openspace.education.common.BaseResponse;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.generalaccount.model.request.LedgerCreateRequest;
import com.openspace.education.generalaccount.model.request.LedgerUpdateRequest;
import com.openspace.education.generalaccount.service.GeneralAccountService;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author riad
 */
@Controller
@RequestMapping("/general/accounts")
public class GeneralAccountController {
    
    @Autowired
    public GeneralAccountService generalAccountService;
    
    
    
    
    @PostMapping(value = "/ledger/create")
    public ResponseEntity<BaseResponse> createLedger(@Valid @ModelAttribute("ledgerCreateRequest")  LedgerCreateRequest ledgerCreateRequest)  {
        return new ResponseEntity<>(this.generalAccountService.createLedger(ledgerCreateRequest), HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/ledger/update", method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> updateLedger(@Valid @ModelAttribute("ledgerUpdateRequest") LedgerUpdateRequest ledgerUpdateRequest)  {
        return new ResponseEntity<>(this.generalAccountService.updateLedger(ledgerUpdateRequest), HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/ledger/delete", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteLedger(@RequestParam Long ledgerId)  {
        return new ResponseEntity<>(this.generalAccountService.deleteLedger(ledgerId), HttpStatus.OK);
    }
    
    
    
}
