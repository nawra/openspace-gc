/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class JournalRequest {
    
    
    @NotNull(message = "Trn Amount can not be null")
    private Double trnAmount;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date tranDate;
     
    private String voucherNo;
    
    private String voucherNote;
    
    private List<JournalRequestDetails> detailList;

    public Double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(Double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getVoucherNote() {
        return voucherNote;
    }

    public void setVoucherNote(String voucherNote) {
        this.voucherNote = voucherNote;
    }

    public List<JournalRequestDetails> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<JournalRequestDetails> detailList) {
        this.detailList = detailList;
    }
    
    
    
}
