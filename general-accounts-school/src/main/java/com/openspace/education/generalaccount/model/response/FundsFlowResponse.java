/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class FundsFlowResponse {
    
    private int year;
    private double totalOpeningBalance;
    private double totalClosingBalance;
    private double totalFundsFlow;
    
    private List<FundsFlowHelper> fundFlows;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getTotalOpeningBalance() {
        return totalOpeningBalance;
    }

    public void setTotalOpeningBalance(double totalOpeningBalance) {
        this.totalOpeningBalance = totalOpeningBalance;
    }

    public double getTotalClosingBalance() {
        return totalClosingBalance;
    }

    public void setTotalClosingBalance(double totalClosingBalance) {
        this.totalClosingBalance = totalClosingBalance;
    }

    public double getTotalFundsFlow() {
        return totalFundsFlow;
    }

    public void setTotalFundsFlow(double totalFundsFlow) {
        this.totalFundsFlow = totalFundsFlow;
    }

    public List<FundsFlowHelper> getFundFlows() {
        return fundFlows;
    }

    public void setFundFlows(List<FundsFlowHelper> fundFlows) {
        this.fundFlows = fundFlows;
    }
    
    
}
