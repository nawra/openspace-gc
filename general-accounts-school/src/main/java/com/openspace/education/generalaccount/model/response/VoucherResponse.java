/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class VoucherResponse {
    
    private String voucherType;
    
    private Long voucherId;
    private String voucherNo;
    private Long trnId;
    private String entryBy;
    private String trnDate;
    private String note;
    private String inWord;
    private double trnAmount;
    private double totalDebit;
    private double totalCredit;
    
    private List<VoucherDetailsResponse>  list;

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    
    
    public Long getTrnId() {
		return trnId;
	}

	public void setTrnId(Long trnId) {
		this.trnId = trnId;
	}

	public String getEntryBy() {
        return entryBy;
    }

    public void setEntryBy(String entryBy) {
        this.entryBy = entryBy;
    }

    public String getTrnDate() {
        return trnDate;
    }

    public void setTrnDate(String trnDate) {
        this.trnDate = trnDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getInWord() {
        return inWord;
    }

    public void setInWord(String inWord) {
        this.inWord = inWord;
    }

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }

    public List<VoucherDetailsResponse> getList() {
        return list;
    }

    public void setList(List<VoucherDetailsResponse> list) {
        this.list = list;
    }

	public double getTrnAmount() {
		return trnAmount;
	}

	public void setTrnAmount(double trnAmount) {
		this.trnAmount = trnAmount;
	}
    
    
    
    
}
