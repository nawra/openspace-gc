/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.controller;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.generalaccount.service.GeneralAccountJasperService;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author riad
 */
@Controller
@RequestMapping(value="/general/accounts/jasper")
public class GeneralAccountJasperController {
    
    @Autowired
    public GeneralAccountJasperService generalAccountJasperService;
    
    @RequestMapping(value="/download/voucher",method=RequestMethod.GET) 
    public ResponseEntity<BaseResponse> downloadVoucher(@RequestParam Set<Long> trnIds){
       BaseResponse baseResponse=generalAccountJasperService.voucherDownload(trnIds);
      return new ResponseEntity<>(baseResponse,HttpStatus.OK);
   }
    
    @RequestMapping(value="/download/payment/voucher",method=RequestMethod.GET) 
    public ResponseEntity<BaseResponse> downloadPaymentVoucher(@RequestParam Set<Long> trnIds){
       BaseResponse baseResponse=generalAccountJasperService.downloadPaymentVoucher(trnIds);
      return new ResponseEntity<>(baseResponse,HttpStatus.OK);
   }
    
}
