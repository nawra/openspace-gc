/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.NumberConverter;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import com.openspace.education.generalaccount.model.entity.AccountTransactionDetails;
import com.openspace.education.generalaccount.model.response.VoucherResponse;
import com.openspace.education.generalaccount.model.response.VoucherView;
import com.openspace.education.generalaccount.repository.AccountTransactionRepository;
import com.openspace.education.jasper.utils.JasperUtils;
import com.openspace.education.user.model.entity.Users;
import com.openspace.education.user.repository.UsersRepository;

/**
 *
 * @author riad
 */
@Service
public class GeneralAccountJasperService {
    
    @Autowired
    private AccountTransactionRepository accountTransactionRepository;
    
    @Autowired
    private JasperUtils jasperUtils;
    
    @Autowired
    private GeneralAccountReportService generalAccountReportService;
    
    @Autowired
    private UsersRepository usersRepository;
    
    
    public static final String ACCOUNTS="jasper/general-accounts/";
    
    
    
    public BaseResponse voucherDownload(Set<Long> trnIds) {
    	
    	BaseResponse baseResponse=new BaseResponse();
       
        Institute company = UserInfoUtils.getLoggedInInstitute();
        
        

        Map<String, Object> map = new HashMap<>();
        map.put("companyName", company.getInstituteName());
        map.put("companyAddress", company.getAddress());
        
        List<AccountTransaction> accountTransactions=accountTransactionRepository.findByInstituteAndTrnIdInOrderByTrnDateAscVoucherIdAsc(company, trnIds);
        
        List<VoucherResponse> atvs = generalAccountReportService.prepareVoucherResponse(accountTransactions);

        String voucherlocation = ACCOUNTS + "voucher.jasper";
        
        try {
            jasperUtils.jasperPrintWithList(atvs, map, voucherlocation,"voucher");
            baseResponse.setMessageType(1);
            baseResponse.setMessage("download ok");
        } catch (Exception ex) {
        	baseResponse.setMessage("download not ok");
        }

        return baseResponse;
    }
    
    
    
    
  
    public BaseResponse downloadPaymentVoucher(Set<Long> trnIds) {
    	
    	BaseResponse baseResponse=new BaseResponse();
       
        Institute institute = UserInfoUtils.getLoggedInInstitute();
        
        List<Users> users = usersRepository.findByInstitute(institute);
        
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Object> map = new HashMap<>();
        map.put("companyName", institute.getInstituteName());
        map.put("companyAddress", institute.getAddress());
        
        List<AccountTransaction> accountTransactions = accountTransactionRepository.findByInstituteAndTrnIdInOrderByTrnDateAscVoucherIdAsc(institute, trnIds);
        
        List<VoucherView> views = new ArrayList<>();
    	
    	for(AccountTransaction at : accountTransactions) {
    		
    		VoucherView view = new VoucherView();
    		
    		view.setTrnAmount(at.getTrnAmount());
    		view.setTrnDate(df.format(at.getTrnDate()));
    		view.setTrnId(at.getTrnId());
    		view.setTrnType(at.getTrnType());
    		view.setUserName(at.getUserName());
    		view.setVoucherId(at.getVoucherId());
    		view.setVoucherNo(at.getVoucherNo());
    		view.setVoucherNote(at.getVoucherNote());
    		view.setInWord(NumberConverter.numberToWord(at.getTrnAmount()));
    		
    		view.setUserNickName(userNickName(users, at.getUserName()));
    		view.setUserDesignation(userdesignation(users, at.getUserName()));
    		
    		Set<AccountTransactionDetails> sets = at.getAccountTransactionDetailses();
    		
    		for(AccountTransactionDetails det : sets) {
    			
    			if(det.getCreditAmount()>0) {
    				
    				view.setLedgerName(det.getAccountLedger().getLedgerName());	
    				
    				break;
    			}
    		}
    		    		
    		views.add(view);
    		
    	}
        

        String voucherlocation = ACCOUNTS + "payment_voucher.jasper";
        
        try {
            jasperUtils.jasperPrintWithList(views, map, voucherlocation,"payment_voucher");
            baseResponse.setMessageType(1);
            baseResponse.setMessage("download ok");
        } catch (Exception ex) {
        	baseResponse.setMessage("download not ok");
        }

        return baseResponse;
    }
     
    
    public String userNickName(List<Users> users,String userName) {
    	
    	for(Users user : users) {
    		
    		if(user.getUsername().equals(userName)) {
    			
    			return user.getNickName();
    		}
    	}
    	
    	return "Mr. X";
    }
    
    
  public String userdesignation(List<Users> users,String userName) {
    	
    	for(Users user : users) {
    		
    		if(user.getUsername().equals(userName)) {
    			
    			if(user.getStaff()!=null) {
    				
    				if(user.getStaff().getDesignation()!=null) {
    				
    				 return	user.getStaff().getDesignation().getName();
    					
    				}
    				
    			}
    			
    			break;
    		}
    	}
    	
    	return "Only User";
    }
    
    
}
