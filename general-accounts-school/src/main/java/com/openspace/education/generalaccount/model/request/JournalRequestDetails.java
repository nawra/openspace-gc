/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class JournalRequestDetails {
    
    @NotNull(message = "Ledger ID can not be null")
    private Long ledgerId;
    
    
    private String ledgerName;
    
    
    @NotNull(message = "Debit Amount can not be null")
    private Double debitAmount;
    
    
    @NotNull(message = "Credit Amount can not be null")
    private Double creditAmount;

    public Long getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public Double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public Double getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Double creditAmount) {
        this.creditAmount = creditAmount;
    }

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}
    
    
    
}
