/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class BalanceSheetManager {
    
    private String instituteName;
    private String instituteAddress;
    private String profitLossLabel;
    private double profitLossValue;
    private String trnDate;
    
    private List<BalanceSheetResponse> balanceSheetResponses;

    

    public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	

    public String getInstituteAddress() {
		return instituteAddress;
	}

	public void setInstituteAddress(String instituteAddress) {
		this.instituteAddress = instituteAddress;
	}

	public List<BalanceSheetResponse> getBalanceSheetResponses() {
        return balanceSheetResponses;
    }

    public void setBalanceSheetResponses(List<BalanceSheetResponse> balanceSheetResponses) {
        this.balanceSheetResponses = balanceSheetResponses;
    }

    public String getProfitLossLabel() {
        return profitLossLabel;
    }

    public void setProfitLossLabel(String profitLossLabel) {
        this.profitLossLabel = profitLossLabel;
    }

    public double getProfitLossValue() {
        return profitLossValue;
    }

    public void setProfitLossValue(double profitLossValue) {
        this.profitLossValue = profitLossValue;
    }

    public String getTrnDate() {
        return trnDate;
    }

    public void setTrnDate(String trnDate) {
        this.trnDate = trnDate;
    }
    
    
    
}
