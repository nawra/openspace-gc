/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

/**
 *
 * @author riad
 */
public class LedgerListResponse {
    

    private Long ledgerId;
    
    private String ledgerName;
    
    private String note;

    private Long instituteId; 
    
    private Long  accountCategoryId;
    
    private String accountCategoryName;
    
    private String categroyType;
    
    private String nature;
    
    
    

    public Long getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

   
    public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public Long getAccountCategoryId() {
        return accountCategoryId;
    }

    public void setAccountCategoryId(Long accountCategoryId) {
        this.accountCategoryId = accountCategoryId;
    }

    public String getAccountCategoryName() {
        return accountCategoryName;
    }

    public void setAccountCategoryName(String accountCategoryName) {
        this.accountCategoryName = accountCategoryName;
    }

    public String getCategroyType() {
        return categroyType;
    }

    public void setCategroyType(String categroyType) {
        this.categroyType = categroyType;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }
    
    
    
    
    
}
