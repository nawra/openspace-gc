/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class TrialBalanceResponse {
    
    
    private String accountCategoryName;
    
    private double totalDebit;
    
    private double totalCredit;
    
    private List<TrialBalanceDetailsResponse> trialBalanceDetails;

    public String getAccountCategoryName() {
        return accountCategoryName;
    }

    public void setAccountCategoryName(String accountCategoryName) {
        this.accountCategoryName = accountCategoryName;
    }


    public List<TrialBalanceDetailsResponse> getTrialBalanceDetails() {
        return trialBalanceDetails;
    }

    public void setTrialBalanceDetails(List<TrialBalanceDetailsResponse> trialBalanceDetails) {
        this.trialBalanceDetails = trialBalanceDetails;
    }

    public double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public double getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(double totalCredit) {
        this.totalCredit = totalCredit;
    }
    
    
}
