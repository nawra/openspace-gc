/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class ReceiptPaymentRequest implements Serializable{
   
    @NotNull(message = "paymetOrReceiptLedgerId can not be null")
    private Long paymetOrReceiptLedgerId;
    
    @NotNull(message = "trnAmount can not be null")
    private Double trnAmount;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date tranDate;
     
    private String voucherNo;
    
    private String voucherNote;
    
    private Set<ReceiptPaymentRequestDetails> requestDetailses;

    public Long getPaymetOrReceiptLedgerId() {
        return paymetOrReceiptLedgerId;
    }

    public void setPaymetOrReceiptLedgerId(Long paymetOrReceiptLedgerId) {
        this.paymetOrReceiptLedgerId = paymetOrReceiptLedgerId;
    }

    public Double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(Double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getVoucherNote() {
        return voucherNote;
    }

    public void setVoucherNote(String voucherNote) {
        this.voucherNote = voucherNote;
    }

   

    /**
	 * @return the requestDetailses
	 */
	public Set<ReceiptPaymentRequestDetails> getRequestDetailses() {
		return requestDetailses;
	}

	/**
	 * @param requestDetailses the requestDetailses to set
	 */
	public void setRequestDetailses(Set<ReceiptPaymentRequestDetails> requestDetailses) {
		this.requestDetailses = requestDetailses;
	}

	public Date getTranDate() {
        return tranDate;
    }

    public void setTranDate(Date tranDate) {
        this.tranDate = tranDate;
    }

    
    
    
}
