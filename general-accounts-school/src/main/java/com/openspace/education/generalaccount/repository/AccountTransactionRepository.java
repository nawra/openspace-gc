/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.repository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.entity.AccountTransaction;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author riad
 */
public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, Long>{
    
    @Query(value="select  ifnull(max(voucher_id),0) from account_transaction where institute_id=?1 ",nativeQuery = true)
    public Long searchCompanyMaxVoucherId(Long instituteId);
    
    public AccountTransaction findByInstituteAndVoucherId(Institute institute,Long voucherId);
    
    public AccountTransaction findByInstituteAndTrnId(Institute institute,Long trnId);
    
    public List<AccountTransaction> findByInstituteAndTrnIdInOrderByTrnDateAscVoucherIdAsc(Institute institute,Set<Long> trnIds);
    
    public List<AccountTransaction> findByInstituteAndTrnDateBetweenOrderByTrnDateAscVoucherIdAsc(Institute institute,Date fromDate,Date toDate);
    
    public List<AccountTransaction> findByInstituteAndTrnTypeAndTrnDateBetweenOrderByTrnDateAscVoucherIdAsc(Institute institute, String trnType, Date fromDate,Date toDate);

}
