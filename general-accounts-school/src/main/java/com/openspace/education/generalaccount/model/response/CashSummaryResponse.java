/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class CashSummaryResponse {
    
    private double totalIncome;
    private double totalExpense;
    private double operatingSurplusDeficit;
    private double totalNonOperatingMovement;
    
    private double netCashMovement;
    
    private double totalOpeningBalance;
    private double totalClosingBalance;
    
    private List<CashSummaryHelper> incomeList;
    private List<CashSummaryHelper> expenseList;
    private List<CashSummaryHelper> assetLiabilitiesList;
    private List<CashSummaryHelper> openingBalanceList;
    private List<CashSummaryHelper> closingBalanceList;

    public double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public double getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(double totalExpense) {
        this.totalExpense = totalExpense;
    }

    public double getOperatingSurplusDeficit() {
        return operatingSurplusDeficit;
    }

    public void setOperatingSurplusDeficit(double operatingSurplusDeficit) {
        this.operatingSurplusDeficit = operatingSurplusDeficit;
    }

    public double getTotalNonOperatingMovement() {
        return totalNonOperatingMovement;
    }

    public void setTotalNonOperatingMovement(double totalNonOperatingMovement) {
        this.totalNonOperatingMovement = totalNonOperatingMovement;
    }

    public double getNetCashMovement() {
        return netCashMovement;
    }

    public void setNetCashMovement(double netCashMovement) {
        this.netCashMovement = netCashMovement;
    }

    public double getTotalOpeningBalance() {
        return totalOpeningBalance;
    }

    public void setTotalOpeningBalance(double totalOpeningBalance) {
        this.totalOpeningBalance = totalOpeningBalance;
    }

    public double getTotalClosingBalance() {
        return totalClosingBalance;
    }

    public void setTotalClosingBalance(double totalClosingBalance) {
        this.totalClosingBalance = totalClosingBalance;
    }

    public List<CashSummaryHelper> getIncomeList() {
        return incomeList;
    }

    public void setIncomeList(List<CashSummaryHelper> incomeList) {
        this.incomeList = incomeList;
    }

    public List<CashSummaryHelper> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<CashSummaryHelper> expenseList) {
        this.expenseList = expenseList;
    }

    public List<CashSummaryHelper> getAssetLiabilitiesList() {
        return assetLiabilitiesList;
    }

    public void setAssetLiabilitiesList(List<CashSummaryHelper> assetLiabilitiesList) {
        this.assetLiabilitiesList = assetLiabilitiesList;
    }

    public List<CashSummaryHelper> getOpeningBalanceList() {
        return openingBalanceList;
    }

    public void setOpeningBalanceList(List<CashSummaryHelper> openingBalanceList) {
        this.openingBalanceList = openingBalanceList;
    }

    public List<CashSummaryHelper> getClosingBalanceList() {
        return closingBalanceList;
    }

    public void setClosingBalanceList(List<CashSummaryHelper> closingBalanceList) {
        this.closingBalanceList = closingBalanceList;
    }
    
}
