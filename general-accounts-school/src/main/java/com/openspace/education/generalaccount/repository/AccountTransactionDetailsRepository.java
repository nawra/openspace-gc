/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.repository;

import com.openspace.education.generalaccount.model.entity.AccountTransactionDetails;
import com.openspace.education.institute.model.entity.Institute;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author riad
 */
public interface AccountTransactionDetailsRepository extends JpaRepository<AccountTransactionDetails, Long>{
    
    
	public List<AccountTransactionDetails> findByInstituteAndAccountTransaction_TrnDateBetween(Institute institute,Date fromDate, Date toDate);
	
	public List<AccountTransactionDetails> findByInstituteAndAccountTransaction_TrnDateBetweenAndAccountLedger_LedgerId(Institute institute,Date fromDate, Date toDate,Long ledgerId);

	
    @Query(value="SELECT ac.category_name, al.ledger_name, sum(det.debit_amount) debit, sum(det.credit_amount) credit " +
    " FROM account_category ac,account_ledger al, account_transaction atn , account_transaction_details det " +
    " where ac.category_id=al.category_id and al.ledger_id=det.ledger_id and al.institute_id=det.institute_id " +
    " and atn.trn_id=det.trn_id  and atn.institute_id=det.institute_id and atn.institute_id=?1 and atn.trn_date between ?2 and ?3 " +
    " group by ac.category_id,al.ledger_id order by ac.category_default_id", nativeQuery = true)
    public List<Object[]> findTrialBalance(Long institute_id,String fromDate,String toDate);
    
    
    @Query(value="SELECT ac.category_type, ac.category_name, al.ledger_name," +
    " (case when ac.nature='Debit' then sum(det.debit_amount)-sum(det.credit_amount) " +
    " when ac.nature='Credit' then sum(det.credit_amount)-sum(det.debit_amount) else 0 end ) balance" +
    " FROM account_category ac,account_ledger al, account_transaction atn , account_transaction_details det " +
    " where ac.category_id=al.category_id and al.ledger_id=det.ledger_id and al.institute_id=det.institute_id " +
    " and atn.trn_id=det.trn_id and atn.institute_id=det.institute_id and atn.institute_id=?1 and atn.trn_date <= ?2 " +
    " group by ac.category_id,al.ledger_id order by ac.category_default_id", nativeQuery = true)
    public List<Object[]> findBalanceSheet(Long instituteId, String trnDate);
    
    
    
    @Query(value = "SELECT l.ledger_id, l.ledger_name, round((sum(td.credit_amount)-sum(td.debit_amount))) amount from account_transaction tm, account_transaction_details td,account_ledger l,account_category c " +
    " where  tm.trn_id=td.trn_id and tm.institute_id=td.institute_id" +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and c.category_type='Income' " +
    " and tm.institute_id=?1 and tm.trn_date between ?2 and ?3 " +
    " group by l.ledger_id order by c.category_default_id", nativeQuery = true)
    public List<Object[]> cashSummaryIncomeQuery(Long instituteId,String fromDate,String toDate);
    
    
    @Query(value = "SELECT l.ledger_id, l.ledger_name, round((sum(td.debit_amount)-sum(td.credit_amount))) amount from account_transaction tm, account_transaction_details td,account_ledger l,account_category c " +
    " where  tm.trn_id=td.trn_id and tm.institute_id=td.institute_id" +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and c.category_type='Expense' " +
    " and tm.institute_id=?1 and tm.trn_date between ?2 and ?3 " +
    " group by l.ledger_id order by c.category_default_id", nativeQuery = true)
    public List<Object[]> cashSummaryExpenseQuery(Long instituteId,String fromDate,String toDate);
    
    
    @Query(value = "SELECT l.ledger_id, l.ledger_name, round((sum(td.credit_amount)-sum(td.debit_amount))) amount " +
    " from account_transaction tm, account_transaction_details td,account_ledger l,account_category c " +
    " where  tm.trn_id=td.trn_id and tm.institute_id=td.institute_id " +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and c.category_type in ('Asset','Liability') " +
    " and tm.institute_id=?1 and tm.trn_date between ?2 and ?3 and c.category_default_id not in (102,103)" +
    " group by l.ledger_id order by c.category_default_id", nativeQuery = true)
    public List<Object[]> cashSummaryAssetLiabilityQuery(Long instituteId,String fromDate,String toDate);
    
    
    @Query(value = "SELECT l.ledger_id, l.ledger_name, round((sum(td.debit_amount)-sum(td.credit_amount))) amount " +
    " from account_transaction tm, account_transaction_details td,account_ledger l,account_category c " +
    " where  tm.trn_id=td.trn_id and tm.institute_id=td.institute_id " +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and tm.institute_id=?1 and tm.trn_date < ?2 " +
    " and c.category_default_id  in (102,103) " +
    " group by l.ledger_id order by c.category_default_id ", nativeQuery = true)
    public List<Object[]> cashSummaryOpeningBalance(Long instituteId,String date);
    
    
    @Query(value = " SELECT l.ledger_id, l.ledger_name, round((sum(td.debit_amount)-sum(td.credit_amount))) amount " +
    " from account_transaction tm, account_transaction_details td,account_ledger l,account_category c " +
    " where  tm.trn_id=td.trn_id and tm.institute_id=td.institute_id " +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and tm.institute_id=?1 and tm.trn_date <= ?2 " +
    " and c.category_default_id  in (102,103) " +
    " group by l.ledger_id order by c.category_default_id ", nativeQuery = true)
    public List<Object[]> cashSummaryClosingBalance(Long instituteId,String date);
    
    
    
    
    @Query(value = "SELECT  tm.trn_date,sum(td.debit_amount) debit,sum(td.credit_amount) credit " +
    " from account_transaction tm,account_transaction_details td,account_ledger l,account_category c " +
    " where tm.trn_id=td.trn_id and tm.institute_id=td.institute_id " +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id and c.category_default_id in (102,103) " +
    " and tm.institute_id=?1 and tm.trn_date <= ?2 " +
    " group by tm.trn_date order by tm.trn_date;", nativeQuery = true)
    public List<Object[]> fundsFlowQuery(Long instituteId, String toDate);
    
    
    
   @Query(value = "select l.ledger_id,l.ledger_name,sum(td.credit_amount),sum(td.debit_amount) " +
    " from account_transaction tm,account_transaction_details td,account_ledger l,account_category c " +
    " where tm.trn_id = td.trn_id and tm.institute_id=td.institute_id " +
    " and td.ledger_id=l.ledger_id and td.institute_id=l.institute_id " +
    " and l.category_id=c.category_id " +
    " and tm.institute_id=?1 and tm.trn_date between ?2 and ?3 and c.category_type=?4 " +
    " group by l.ledger_id order by c.category_default_id", nativeQuery = true)
    public List<Object[]> findIncomeExpenseListForIncomeStatement(Long instituteId, String fromDate, String toDate,String categoryType);
    
    
    
    
    @Query(value = "select trn.user_name,led.ledger_name,sum(det.credit_amount) credit,sum(det.debit_amount) debit " + 
    " from account_transaction trn,account_transaction_details det,account_ledger led " + 
    " where trn.trn_id=det.trn_id and trn.institute_id=det.institute_id " + 
    " and det.ledger_id=led.ledger_id and det.institute_id=led.institute_id " + 
    " and trn.institute_id=?1 and trn.trn_date between ?2 and ?3" + 
    " group by trn.user_name,led.ledger_id", nativeQuery = true)
   public List<Object[]> userWiseCollectionView(Long instituteId, String fromDate, String toDate);
    
    
}
 