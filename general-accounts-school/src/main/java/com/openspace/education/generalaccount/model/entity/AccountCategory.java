/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



/**
 *
 * @author riad
 */


@Entity
@Table(name="account_category")
public class AccountCategory implements Serializable{
      
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="category_id")
    private Long categoryId;
    
    @Column(name="category_name")
    private String categoryName;
    
    @Column(name="nature")
    private String nature;
    
    @Column(name="category_type")
    private String categoryType;
    
    @Column(name="category_serial")
    private Integer categorySerial;
    
    @Column(name="category_default_id")
    private Integer categoryDefaultId;
    
    @Column(name="category_note")
    private String categoryNote;   
    

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    

    public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public Integer getCategorySerial() {
        return categorySerial;
    }

    public void setCategorySerial(Integer categorySerial) {
        this.categorySerial = categorySerial;
    }

    public Integer getCategoryDefaultId() {
        return categoryDefaultId;
    }

    public void setCategoryDefaultId(Integer categoryDefaultId) {
        this.categoryDefaultId = categoryDefaultId;
    }

    public String getCategoryNote() {
        return categoryNote;
    }

    public void setCategoryNote(String categoryNote) {
        this.categoryNote = categoryNote;
    }
    
    
    
}
