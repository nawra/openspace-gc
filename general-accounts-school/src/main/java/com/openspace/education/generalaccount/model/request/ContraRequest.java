/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class ContraRequest {
    
    
    @NotNull(message = "From LedgerId can not be null")
    private Long fromLedgerId;
    
    @NotNull(message = "To LedgerId can not be null")
    private Long toLedgerId;
    
    @NotNull(message = "Transaction can not be null")
    private Double trnAmount;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date trnDate;
    
    private String voucherNo;
    
    private String voucherNote;
    

    public Long getFromLedgerId() {
        return fromLedgerId;
    }

    public void setFromLedgerId(Long fromLedgerId) {
        this.fromLedgerId = fromLedgerId;
    }

    public Long getToLedgerId() {
        return toLedgerId;
    }

    public void setToLedgerId(Long toLedgerId) {
        this.toLedgerId = toLedgerId;
    }

    public Double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(Double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public Date getTrnDate() {
        return trnDate;
    }

    public void setTrnDate(Date trnDate) {
        this.trnDate = trnDate;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getVoucherNote() {
        return voucherNote;
    }

    public void setVoucherNote(String voucherNote) {
        this.voucherNote = voucherNote;
    }
    
    
    
}
