package com.openspace.education.generalaccount.model.response;

import java.util.List;

public class CashBookViewHelper1 {
	
	private Long ledgerId;
	private String ledgerName;
	
	private List<CashBookViewHelper2> insideDetails;

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public List<CashBookViewHelper2> getInsideDetails() {
		return insideDetails;
	}

	public void setInsideDetails(List<CashBookViewHelper2> insideDetails) {
		this.insideDetails = insideDetails;
	}

	
	
	

}
