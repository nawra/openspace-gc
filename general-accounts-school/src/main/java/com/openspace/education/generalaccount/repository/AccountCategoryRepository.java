/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.repository;

import com.openspace.education.generalaccount.model.entity.AccountCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author riad
 */
public interface AccountCategoryRepository extends JpaRepository<AccountCategory, Long>{
  
    @Query(value="select t from AccountCategory t order by t.categoryDefaultId")
    public List<AccountCategory>   findAllOrderByCategoryDefaultId();
}
