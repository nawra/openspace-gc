/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.repository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.generalaccount.model.entity.AccountLedger;
import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author riad
 */
public interface AccountLedgerRepository extends JpaRepository<AccountLedger, Long>{
    
    public AccountLedger findByLedgerNameAndInstitute_InstituteId(String ledgerName,Long instituteId);
    
    public AccountLedger findByLedgerIdAndInstitute(Long ledgerId,Institute institute);
    
    public AccountLedger findByLedgerIdAndInstituteAndAccountCategory_CategoryType(Long ledgerId,Institute institute,String categoryName);
    
    public List<AccountLedger> findByInstituteAndAccountCategory_CategoryTypeOrderByAccountCategory_CategoryDefaultId(Institute institute,String categoryName);

    public List<AccountLedger> findByLedgerIdNotAndInstitute(Long ledgerId,Institute institute);
    
    public List<AccountLedger> findByInstituteAndLedgerIdIn(Institute institute, List<Long> ledgerIds);
    
    public List<AccountLedger> findByInstituteOrderByAccountCategory_CategoryDefaultId(Institute institute);
    
    public List<AccountLedger> findByInstituteAndAccountCategory_CategoryDefaultIdIn(Institute institute, Set<Integer> defaultIds);
    
}
