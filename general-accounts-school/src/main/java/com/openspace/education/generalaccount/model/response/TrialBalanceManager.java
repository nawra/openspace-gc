/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

import java.util.List;

/**
 *
 * @author riad
 */
public class TrialBalanceManager {
    
    private String instituteName;
    private String instituteAddress;
    private String fromDate;
    private String toDate;
    private double grandTotalDebit;
    private double grandTotalCredit;
    
    private List<TrialBalanceResponse> trialBalanceResponses;

   

    public String getInstituteName() {
		return instituteName;
	}

	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}

	public String getInstituteAddress() {
		return instituteAddress;
	}

	public void setInstituteAddress(String instituteAddress) {
		this.instituteAddress = instituteAddress;
	}

	public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<TrialBalanceResponse> getTrialBalanceResponses() {
        return trialBalanceResponses;
    }

    public void setTrialBalanceResponses(List<TrialBalanceResponse> trialBalanceResponses) {
        this.trialBalanceResponses = trialBalanceResponses;
    }

    public double getGrandTotalDebit() {
        return grandTotalDebit;
    }

    public void setGrandTotalDebit(double grandTotalDebit) {
        this.grandTotalDebit = grandTotalDebit;
    }

    public double getGrandTotalCredit() {
        return grandTotalCredit;
    }

    public void setGrandTotalCredit(double grandTotalCredit) {
        this.grandTotalCredit = grandTotalCredit;
    }
    
    
}
