/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.entity;

import com.openspace.education.institute.model.entity.Institute;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author riad
 */

@Entity
@Table(name="account_transaction_details")
public class AccountTransactionDetails implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="trn_details_id")
    private Long trnDetailsId;
    
    @ManyToOne
    @JoinColumn(name="trn_id")
    private AccountTransaction accountTransaction;
    
    @ManyToOne
    @JoinColumn(name="ledger_id")
    private AccountLedger accountLedger;
    
    @Column(name="debit_amount")
    private Double debitAmount;
    
    @Column(name="credit_amount")
    private Double creditAmount;
    
    @ManyToOne
    @JoinColumn(name="institute_id", nullable = false)
    private Institute institute;
    

    public Long getTrnDetailsId() {
        return trnDetailsId;
    }

    public void setTrnDetailsId(Long trnDetailsId) {
        this.trnDetailsId = trnDetailsId;
    }

    public AccountTransaction getAccountTransaction() {
        return accountTransaction;
    }

    public void setAccountTransaction(AccountTransaction accountTransaction) {
        this.accountTransaction = accountTransaction;
    }

    public AccountLedger getAccountLedger() {
        return accountLedger;
    }

    public void setAccountLedger(AccountLedger accountLedger) {
        this.accountLedger = accountLedger;
    }

    public Double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public Double getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Double creditAmount) {
        this.creditAmount = creditAmount;
    }

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

    

   
    
    
    
    
}
