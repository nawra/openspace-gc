/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author riad
 */
public class ReceiptPaymentRequestDetails {
    
   
    private Long ledgerId;
    
    private String ledgerName;
    
    private double ledgerAmount;

	public Long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(Long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public double getLedgerAmount() {
		return ledgerAmount;
	}

	public void setLedgerAmount(double ledgerAmount) {
		this.ledgerAmount = ledgerAmount;
	}

	
    
    
}
