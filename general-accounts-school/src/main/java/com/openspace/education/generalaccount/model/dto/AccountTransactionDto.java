/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.dto;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author riad
 */
public class AccountTransactionDto {
    
    private double trnAmount;
    private Long voucherId;
    private String tranType;
    private String voucherNo;
    private String voucherNote;
    private Date trnDate;
    private Long moduleId;
    private String userName;
    
    private Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos;

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getVoucherNote() {
        return voucherNote;
    }

    public void setVoucherNote(String voucherNote) {
        this.voucherNote = voucherNote;
    }

    public Date getTrnDate() {
        return trnDate;
    }

    public void setTrnDate(Date trnDate) {
        this.trnDate = trnDate;
    }

    public Set<AccountTransactionDetailsDto> getAccountTransactionDetailsDtos() {
        return accountTransactionDetailsDtos;
    }

    public void setAccountTransactionDetailsDtos(Set<AccountTransactionDetailsDto> accountTransactionDetailsDtos) {
        this.accountTransactionDetailsDtos = accountTransactionDetailsDtos;
    }

    public double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    
    
    
    
}
