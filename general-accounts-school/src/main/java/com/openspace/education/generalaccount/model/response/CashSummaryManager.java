/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.response;

/**
 *
 * @author riad
 */
public class CashSummaryManager {
    
    private String companyName;
    private String companyAddress;
    private String fromDate;
    private String toDate;
    private CashSummaryResponse CashSummary;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public CashSummaryResponse getCashSummary() {
        return CashSummary;
    }

    public void setCashSummary(CashSummaryResponse CashSummary) {
        this.CashSummary = CashSummary;
    }
    
}
