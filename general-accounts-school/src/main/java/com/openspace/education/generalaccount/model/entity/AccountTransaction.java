/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.generalaccount.model.entity;

import com.openspace.education.institute.model.entity.Institute;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author riad
 */
@Entity
@Table(name="account_transaction",uniqueConstraints = @UniqueConstraint(columnNames = {"voucher_id","institute_id"}))
public class AccountTransaction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="trn_id")
    private Long trnId;
    
    @Column(name="voucher_id",nullable = false)
    private Long voucherId;
    
    @Column(name="voucher_no")
    private String voucherNo;
    
    @Column(name="voucher_note")
    private String voucherNote;
    
    @Column(name="trn_type")
    private String trnType;
       
    @Column(name="trn_amount")
    private Double trnAmount;
    
    @Column(name="trn_date")
    @Temporal(TemporalType.DATE)
    private Date trnDate;
    
    @Column(name="trn_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date trnTime;
    
    @Column(name="module_id")
    private Long moduleId;
    
    @ManyToOne
    @JoinColumn(name="institute_id")
    private Institute institute;
    
    @Column(name="user_name")
    private String userName;
    
    @OneToMany(mappedBy = "accountTransaction", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<AccountTransactionDetails> accountTransactionDetailses;

    public Long getTrnId() {
        return trnId;
    }

    public void setTrnId(Long trnId) {
        this.trnId = trnId;
    }

    

    public String getTrnType() {
        return trnType;
    }

    public void setTrnType(String trnType) {
        this.trnType = trnType;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public Double getTrnAmount() {
        return trnAmount;
    }

    public void setTrnAmount(Double trnAmount) {
        this.trnAmount = trnAmount;
    }

    public Date getTrnDate() {
        return trnDate;
    }

    public void setTrnDate(Date trnDate) {
        this.trnDate = trnDate;
    }

    public Date getTrnTime() {
        return trnTime;
    }

    public void setTrnTime(Date trnTime) {
        this.trnTime = trnTime;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

   

    public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherNote() {
        return voucherNote;
    }

    public void setVoucherNote(String voucherNote) {
        this.voucherNote = voucherNote;
    }

    public Set<AccountTransactionDetails> getAccountTransactionDetailses() {
        return accountTransactionDetailses;
    }

    public void setAccountTransactionDetailses(Set<AccountTransactionDetails> accountTransactionDetailses) {
        this.accountTransactionDetailses = accountTransactionDetailses;
    }
    
    
    
}
