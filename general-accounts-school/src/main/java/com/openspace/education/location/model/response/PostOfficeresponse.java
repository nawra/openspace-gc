package com.openspace.education.location.model.response;



public class PostOfficeresponse {
	
    private Integer postOfficeId;
	
	private String postOfficeName;

	private String postalCode;

	private Integer thanaId;
	
	private String thanaName;

	public Integer getPostOfficeId() {
		return postOfficeId;
	}

	public void setPostOfficeId(Integer postOfficeId) {
		this.postOfficeId = postOfficeId;
	}

	public String getPostOfficeName() {
		return postOfficeName;
	}

	public void setPostOfficeName(String postOfficeName) {
		this.postOfficeName = postOfficeName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Integer getThanaId() {
		return thanaId;
	}

	public void setThanaId(Integer thanaId) {
		this.thanaId = thanaId;
	}

	public String getThanaName() {
		return thanaName;
	}

	public void setThanaName(String thanaName) {
		this.thanaName = thanaName;
	}
	
	
	
	

}
