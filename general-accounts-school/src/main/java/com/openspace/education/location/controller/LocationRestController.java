package com.openspace.education.location.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.location.service.LocationService;

@RestController
@RequestMapping(value = "/test")
public class LocationRestController {
	
	@Autowired
	public LocationService locationService;
	
	@GetMapping(value = "/district-list/by/division-id")
	public ResponseEntity<ItemResponse> findDistrictByDivisionId(@RequestParam Integer divisionId){
		ItemResponse itemResponse=locationService.districtListByDivisionId(divisionId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	

}
