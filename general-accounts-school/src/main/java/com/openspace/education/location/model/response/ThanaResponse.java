package com.openspace.education.location.model.response;



import com.openspace.education.location.model.entity.District;

public class ThanaResponse {
	

	private Integer thanaId;

	private String thanaName;

	private Integer districtId;
	
	private String districtName;

	public Integer getThanaId() {
		return thanaId;
	}

	public void setThanaId(Integer thanaId) {
		this.thanaId = thanaId;
	}

	public String getThanaName() {
		return thanaName;
	}

	public void setThanaName(String thanaName) {
		this.thanaName = thanaName;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}
	
	
	
	

}
