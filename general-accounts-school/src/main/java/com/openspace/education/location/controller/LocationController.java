package com.openspace.education.location.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.location.service.LocationService;

@Controller
@RequestMapping(value = "/location/info")
public class LocationController {
	
	@Autowired
	public LocationService locationService;
	
	
	@GetMapping(value = "/district-list")
	public ResponseEntity<ItemResponse> findDistrictList(){
		ItemResponse itemResponse=locationService.districtList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@GetMapping(value = "/district-list/by/division-id")
	public ResponseEntity<ItemResponse> findDistrictByDivisionId(@RequestParam Integer divisionId){
		ItemResponse itemResponse=locationService.districtListByDivisionId(divisionId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@GetMapping(value = "/thana-list/by/district-id")
	public ResponseEntity<ItemResponse> findThanaByDistrictId(@RequestParam Integer districtId){
		ItemResponse itemResponse=locationService.thanaListByDistrictId(districtId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/post-office-list/by/thana-id")
	public ResponseEntity<ItemResponse> findPostOfficeByDivisionId(@RequestParam Integer thanaId){
		ItemResponse itemResponse=locationService.postOfficeListByThanaId(thanaId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	

}
