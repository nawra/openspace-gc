package com.openspace.education.location.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="location_post_office")
public class PostOffice implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="post_office_id")
	private Integer postOfficeId;
	
	@Column(name="post_office_name")
	private String postOfficeName;
	
	@Column(name="postal_code")
	private String postalCode;
	
	@ManyToOne
	@JoinColumn(name="thana_id")
	private Thana thana;

	public Integer getPostOfficeId() {
		return postOfficeId;
	}

	public void setPostOfficeId(Integer postOfficeId) {
		this.postOfficeId = postOfficeId;
	}

	public String getPostOfficeName() {
		return postOfficeName;
	}

	public void setPostOfficeName(String postOfficeName) {
		this.postOfficeName = postOfficeName;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Thana getThana() {
		return thana;
	}

	public void setThana(Thana thana) {
		this.thana = thana;
	}
	
	
	

}
