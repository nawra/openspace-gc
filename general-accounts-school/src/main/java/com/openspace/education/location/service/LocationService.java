package com.openspace.education.location.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.location.model.entity.District;
import com.openspace.education.location.model.entity.Division;
import com.openspace.education.location.model.entity.PostOffice;
import com.openspace.education.location.model.entity.Thana;
import com.openspace.education.location.model.response.DistrictResponse;
import com.openspace.education.location.model.response.DivisionResponse;
import com.openspace.education.location.model.response.PostOfficeresponse;
import com.openspace.education.location.model.response.ThanaResponse;
import com.openspace.education.location.repository.DistrictRepository;
import com.openspace.education.location.repository.DivisionRepository;
import com.openspace.education.location.repository.PostOfficeRepository;
import com.openspace.education.location.repository.ThanaRepository;

@Service
public class LocationService {
	
	@Autowired
	public DivisionRepository divisionRepository;
	
	@Autowired
	public DistrictRepository districtRepository;
	
	@Autowired
	public ThanaRepository thanaRepository;
	
	@Autowired
	public PostOfficeRepository postOfficeRepository;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse divisionList() {
		ItemResponse itemResponse=new ItemResponse();
		List<Division> divisions=divisionRepository.findAll();
		List<DivisionResponse> divisionResponses=new ArrayList<>();
		for(Division d:divisions) {
			DivisionResponse divisionResponse=new DivisionResponse();
			divisionResponse.setDivisionId(d.getDivisionId());
			divisionResponse.setDivisionName(d.getDivisionName());
			divisionResponses.add(divisionResponse);
		}
		itemResponse.setItem(divisionResponses);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("ok");
		return itemResponse;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse districtListByDivisionId(Integer divisionId) {
		ItemResponse itemResponse=new ItemResponse();
		List<District> districts=districtRepository.findByDivision_DivisionId(divisionId);
		List<DistrictResponse> districtResponses=new ArrayList<>();
		for(District d:districts) {
			DistrictResponse districtResponse=new DistrictResponse();
			districtResponse.setDistrictId(d.getDistrictId());
			districtResponse.setDistrictName(d.getDistrictName());
			districtResponse.setDivisionId(d.getDivision().getDivisionId());
			districtResponse.setDivisionName(d.getDivision().getDivisionName());
			districtResponses.add(districtResponse);
			
		}
		itemResponse.setItem(districtResponses);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("ok");
		return itemResponse;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse districtList() {
		ItemResponse itemResponse=new ItemResponse();
		List<District> districts=districtRepository.findAll();
		List<DistrictResponse> districtResponses=new ArrayList<>();
		for(District d:districts) {
			DistrictResponse districtResponse=new DistrictResponse();
			districtResponse.setDistrictId(d.getDistrictId());
			districtResponse.setDistrictName(d.getDistrictName());
			districtResponse.setDivisionId(d.getDivision().getDivisionId());
			districtResponse.setDivisionName(d.getDivision().getDivisionName());
			districtResponses.add(districtResponse);
			
		}
		itemResponse.setItem(districtResponses);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("ok");
		return itemResponse;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse thanaListByDistrictId(Integer districtId) {
	
		ItemResponse itemResponse=new ItemResponse();
		List<Thana> thanas=thanaRepository.findByDistrict_districtId(districtId);
		List<ThanaResponse> thanaResponses=new ArrayList<>();
		for(Thana d:thanas) {
			ThanaResponse thanaResponse=new ThanaResponse();
			thanaResponse.setThanaId(d.getThanaId());
			thanaResponse.setThanaName(d.getThanaName());
			thanaResponse.setDistrictId(d.getDistrict().getDistrictId());
			thanaResponse.setDistrictName(d.getDistrict().getDistrictName());
			thanaResponses.add(thanaResponse);
			
		}
		itemResponse.setItem(thanaResponses);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("ok");
		return itemResponse;
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ItemResponse postOfficeListByThanaId(Integer thanaId) {
	
		ItemResponse itemResponse=new ItemResponse();
		List<PostOffice> postoffices=postOfficeRepository.findByThana_thanaId(thanaId);
		List<PostOfficeresponse> postOfficeresponses=new ArrayList<>();
		for(PostOffice d:postoffices) {
			PostOfficeresponse postOfficeresponse=new PostOfficeresponse();
			postOfficeresponse.setPostOfficeId(d.getPostOfficeId());
			postOfficeresponse.setPostOfficeName(d.getPostOfficeName());
			postOfficeresponse.setPostalCode(d.getPostalCode());
			postOfficeresponse.setThanaId(d.getThana().getThanaId());
			postOfficeresponse.setThanaName(d.getThana().getThanaName());
			postOfficeresponses.add(postOfficeresponse);
			
		}
		itemResponse.setItem(postOfficeresponses);
		itemResponse.setMessageType(1);
		itemResponse.setMessage("ok");
		return itemResponse;
	}

}
