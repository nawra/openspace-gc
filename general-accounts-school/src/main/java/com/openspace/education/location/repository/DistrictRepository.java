package com.openspace.education.location.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.location.model.entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer>{
	
	public List<District> findByDivision_DivisionId(Integer divisionId);

}
