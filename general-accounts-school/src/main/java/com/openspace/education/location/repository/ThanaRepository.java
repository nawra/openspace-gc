package com.openspace.education.location.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.location.model.entity.Thana;

public interface ThanaRepository extends JpaRepository<Thana, Integer>{

	
	public List<Thana> findByDistrict_districtId(Integer districtId);
}
