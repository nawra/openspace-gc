package com.openspace.education.location.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.location.model.entity.Division;

public interface DivisionRepository extends JpaRepository<Division, Integer>{

}
