package com.openspace.education.location.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.location.model.entity.PostOffice;

public interface PostOfficeRepository extends JpaRepository<PostOffice, Integer>{
	
	public List<PostOffice> findByThana_thanaId(Integer thanaId);

}
