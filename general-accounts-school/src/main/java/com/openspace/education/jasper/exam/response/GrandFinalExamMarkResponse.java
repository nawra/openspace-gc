package com.openspace.education.jasper.exam.response;

public class GrandFinalExamMarkResponse {

	private Long subjectId;
	private String subjectName;
	private String subjectDefaultId;

	// 1st Exam
	private double firstSc1;
	private double firstSc2;
	private double firstSc3;
	private double firstSc4;
	private double firstSc5;
	private double firstSc6;

	private double firstSc1PassMark;
	private double firstSc2PassMark;
	private double firstSc3PassMark;
	private double firstSc4PassMark;
	private double firstSc5PassMark;
	private double firstSc6PassMark;

	private double firstSbjTotalMark; // examTaken Subject Full Mark * acceptance
	private double firstSbjFullMark;
	private double firstSbjObtainedMark;
	private String firstSbjLetterGrade;
	private double firstSbjGradePoint;

	// 2nd Exam
	private double secondSc1;
	private double secondSc2;
	private double secondSc3;
	private double secondSc4;
	private double secondSc5;
	private double secondSc6;

	private double secondSc1PassMark;
	private double secondSc2PassMark;
	private double secondSc3PassMark;
	private double secondSc4PassMark;
	private double secondSc5PassMark;
	private double secondSc6PassMark;
	private double secondSbjTotalMark;
	private double secondSbjObtainedMark;
	private String secondSbjLetterGrade;
	private double secondSbjGradePoint;

	// 3rd Exam
	private double thirdSc1;
	private double thirdSc2;
	private double thirdSc3;
	private double thirdSc4;
	private double thirdSc5;
	private double thirdSc6;

	private double thirdSc1PassMark;
	private double thirdSc2PassMark;
	private double thirdSc3PassMark;
	private double thirdSc4PassMark;
	private double thirdSc5PassMark;
	private double thirdSc6PassMark;

	private double thirdSbjTotalMark;
	private double thirdSbjObtainedMark;
	private String thirdSbjLetterGrade;
	private double thirdSbjGradePoint;

	// 4th Exam
	private double fourthSc1;
	private double fourthSc2;
	private double fourthSc3;
	private double fourthSc4;
	private double fourthSc5;
	private double fourthSc6;

	private double fourthSc1PassMark;
	private double fourthSc2PassMark;
	private double fourthSc3PassMark;
	private double fourthSc4PassMark;
	private double fourthSc5PassMark;
	private double fourthSc6PassMark;

	private double fourthSbjTotalMark;
	private double fourthSbjObtainedMark;
	private String fourthSbjLetterGrade;
	private double fourthSbjGradePoint;

	// Gf Exam
	private double gfSc1;
	private double gfSc2;
	private double gfSc3;
	private double gfSc4;
	private double gfSc5;
	private double gfSc6;
	private double gfSbjTotalMark;
	private double gfSbjObtainedMark;
	private String gfSbjLetterGrade;
	private double gfSbjGradePoint;

	public Long getSubjectId() {
		return subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public String getSubjectDefaultId() {
		return subjectDefaultId;
	}

	public double getFirstSc1() {
		return firstSc1;
	}

	public double getFirstSc2() {
		return firstSc2;
	}

	public double getFirstSc3() {
		return firstSc3;
	}

	public double getFirstSc4() {
		return firstSc4;
	}

	public double getFirstSc5() {
		return firstSc5;
	}

	public double getFirstSc6() {
		return firstSc6;
	}

	public double getFirstSc1PassMark() {
		return firstSc1PassMark;
	}

	public double getFirstSc2PassMark() {
		return firstSc2PassMark;
	}

	public double getFirstSc3PassMark() {
		return firstSc3PassMark;
	}

	public double getFirstSc4PassMark() {
		return firstSc4PassMark;
	}

	public double getFirstSc5PassMark() {
		return firstSc5PassMark;
	}

	public double getFirstSc6PassMark() {
		return firstSc6PassMark;
	}

	public double getFirstSbjTotalMark() {
		return firstSbjTotalMark;
	}

	public double getFirstSbjFullMark() {
		return firstSbjFullMark;
	}

	public double getFirstSbjObtainedMark() {
		return firstSbjObtainedMark;
	}

	public String getFirstSbjLetterGrade() {
		return firstSbjLetterGrade;
	}

	public double getFirstSbjGradePoint() {
		return firstSbjGradePoint;
	}

	public double getSecondSc1() {
		return secondSc1;
	}

	public double getSecondSc2() {
		return secondSc2;
	}

	public double getSecondSc3() {
		return secondSc3;
	}

	public double getSecondSc4() {
		return secondSc4;
	}

	public double getSecondSc5() {
		return secondSc5;
	}

	public double getSecondSc6() {
		return secondSc6;
	}

	public double getSecondSc1PassMark() {
		return secondSc1PassMark;
	}

	public double getSecondSc2PassMark() {
		return secondSc2PassMark;
	}

	public double getSecondSc3PassMark() {
		return secondSc3PassMark;
	}

	public double getSecondSc4PassMark() {
		return secondSc4PassMark;
	}

	public double getSecondSc5PassMark() {
		return secondSc5PassMark;
	}

	public double getSecondSc6PassMark() {
		return secondSc6PassMark;
	}

	public double getSecondSbjTotalMark() {
		return secondSbjTotalMark;
	}

	public double getSecondSbjObtainedMark() {
		return secondSbjObtainedMark;
	}

	public String getSecondSbjLetterGrade() {
		return secondSbjLetterGrade;
	}

	public double getSecondSbjGradePoint() {
		return secondSbjGradePoint;
	}

	public double getThirdSc1() {
		return thirdSc1;
	}

	public double getThirdSc2() {
		return thirdSc2;
	}

	public double getThirdSc3() {
		return thirdSc3;
	}

	public double getThirdSc4() {
		return thirdSc4;
	}

	public double getThirdSc5() {
		return thirdSc5;
	}

	public double getThirdSc6() {
		return thirdSc6;
	}

	public double getThirdSc1PassMark() {
		return thirdSc1PassMark;
	}

	public double getThirdSc2PassMark() {
		return thirdSc2PassMark;
	}

	public double getThirdSc3PassMark() {
		return thirdSc3PassMark;
	}

	public double getThirdSc4PassMark() {
		return thirdSc4PassMark;
	}

	public double getThirdSc5PassMark() {
		return thirdSc5PassMark;
	}

	public double getThirdSc6PassMark() {
		return thirdSc6PassMark;
	}

	public double getThirdSbjTotalMark() {
		return thirdSbjTotalMark;
	}

	public double getThirdSbjObtainedMark() {
		return thirdSbjObtainedMark;
	}

	public String getThirdSbjLetterGrade() {
		return thirdSbjLetterGrade;
	}

	public double getThirdSbjGradePoint() {
		return thirdSbjGradePoint;
	}

	public double getFourthSc1() {
		return fourthSc1;
	}

	public double getFourthSc2() {
		return fourthSc2;
	}

	public double getFourthSc3() {
		return fourthSc3;
	}

	public double getFourthSc4() {
		return fourthSc4;
	}

	public double getFourthSc5() {
		return fourthSc5;
	}

	public double getFourthSc6() {
		return fourthSc6;
	}

	public double getFourthSc1PassMark() {
		return fourthSc1PassMark;
	}

	public double getFourthSc2PassMark() {
		return fourthSc2PassMark;
	}

	public double getFourthSc3PassMark() {
		return fourthSc3PassMark;
	}

	public double getFourthSc4PassMark() {
		return fourthSc4PassMark;
	}

	public double getFourthSc5PassMark() {
		return fourthSc5PassMark;
	}

	public double getFourthSc6PassMark() {
		return fourthSc6PassMark;
	}

	public double getFourthSbjTotalMark() {
		return fourthSbjTotalMark;
	}

	public double getFourthSbjObtainedMark() {
		return fourthSbjObtainedMark;
	}

	public String getFourthSbjLetterGrade() {
		return fourthSbjLetterGrade;
	}

	public double getFourthSbjGradePoint() {
		return fourthSbjGradePoint;
	}

	public double getGfSc1() {
		return gfSc1;
	}

	public double getGfSc2() {
		return gfSc2;
	}

	public double getGfSc3() {
		return gfSc3;
	}

	public double getGfSc4() {
		return gfSc4;
	}

	public double getGfSc5() {
		return gfSc5;
	}

	public double getGfSc6() {
		return gfSc6;
	}

	public double getGfSbjTotalMark() {
		return gfSbjTotalMark;
	}

	public double getGfSbjObtainedMark() {
		return gfSbjObtainedMark;
	}

	public String getGfSbjLetterGrade() {
		return gfSbjLetterGrade;
	}

	public double getGfSbjGradePoint() {
		return gfSbjGradePoint;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public void setSubjectDefaultId(String subjectDefaultId) {
		this.subjectDefaultId = subjectDefaultId;
	}

	public void setFirstSc1(double firstSc1) {
		this.firstSc1 = firstSc1;
	}

	public void setFirstSc2(double firstSc2) {
		this.firstSc2 = firstSc2;
	}

	public void setFirstSc3(double firstSc3) {
		this.firstSc3 = firstSc3;
	}

	public void setFirstSc4(double firstSc4) {
		this.firstSc4 = firstSc4;
	}

	public void setFirstSc5(double firstSc5) {
		this.firstSc5 = firstSc5;
	}

	public void setFirstSc6(double firstSc6) {
		this.firstSc6 = firstSc6;
	}

	public void setFirstSc1PassMark(double firstSc1PassMark) {
		this.firstSc1PassMark = firstSc1PassMark;
	}

	public void setFirstSc2PassMark(double firstSc2PassMark) {
		this.firstSc2PassMark = firstSc2PassMark;
	}

	public void setFirstSc3PassMark(double firstSc3PassMark) {
		this.firstSc3PassMark = firstSc3PassMark;
	}

	public void setFirstSc4PassMark(double firstSc4PassMark) {
		this.firstSc4PassMark = firstSc4PassMark;
	}

	public void setFirstSc5PassMark(double firstSc5PassMark) {
		this.firstSc5PassMark = firstSc5PassMark;
	}

	public void setFirstSc6PassMark(double firstSc6PassMark) {
		this.firstSc6PassMark = firstSc6PassMark;
	}

	public void setFirstSbjTotalMark(double firstSbjTotalMark) {
		this.firstSbjTotalMark = firstSbjTotalMark;
	}

	public void setFirstSbjFullMark(double firstSbjFullMark) {
		this.firstSbjFullMark = firstSbjFullMark;
	}

	public void setFirstSbjObtainedMark(double firstSbjObtainedMark) {
		this.firstSbjObtainedMark = firstSbjObtainedMark;
	}

	public void setFirstSbjLetterGrade(String firstSbjLetterGrade) {
		this.firstSbjLetterGrade = firstSbjLetterGrade;
	}

	public void setFirstSbjGradePoint(double firstSbjGradePoint) {
		this.firstSbjGradePoint = firstSbjGradePoint;
	}

	public void setSecondSc1(double secondSc1) {
		this.secondSc1 = secondSc1;
	}

	public void setSecondSc2(double secondSc2) {
		this.secondSc2 = secondSc2;
	}

	public void setSecondSc3(double secondSc3) {
		this.secondSc3 = secondSc3;
	}

	public void setSecondSc4(double secondSc4) {
		this.secondSc4 = secondSc4;
	}

	public void setSecondSc5(double secondSc5) {
		this.secondSc5 = secondSc5;
	}

	public void setSecondSc6(double secondSc6) {
		this.secondSc6 = secondSc6;
	}

	public void setSecondSc1PassMark(double secondSc1PassMark) {
		this.secondSc1PassMark = secondSc1PassMark;
	}

	public void setSecondSc2PassMark(double secondSc2PassMark) {
		this.secondSc2PassMark = secondSc2PassMark;
	}

	public void setSecondSc3PassMark(double secondSc3PassMark) {
		this.secondSc3PassMark = secondSc3PassMark;
	}

	public void setSecondSc4PassMark(double secondSc4PassMark) {
		this.secondSc4PassMark = secondSc4PassMark;
	}

	public void setSecondSc5PassMark(double secondSc5PassMark) {
		this.secondSc5PassMark = secondSc5PassMark;
	}

	public void setSecondSc6PassMark(double secondSc6PassMark) {
		this.secondSc6PassMark = secondSc6PassMark;
	}

	public void setSecondSbjTotalMark(double secondSbjTotalMark) {
		this.secondSbjTotalMark = secondSbjTotalMark;
	}

	public void setSecondSbjObtainedMark(double secondSbjObtainedMark) {
		this.secondSbjObtainedMark = secondSbjObtainedMark;
	}

	public void setSecondSbjLetterGrade(String secondSbjLetterGrade) {
		this.secondSbjLetterGrade = secondSbjLetterGrade;
	}

	public void setSecondSbjGradePoint(double secondSbjGradePoint) {
		this.secondSbjGradePoint = secondSbjGradePoint;
	}

	public void setThirdSc1(double thirdSc1) {
		this.thirdSc1 = thirdSc1;
	}

	public void setThirdSc2(double thirdSc2) {
		this.thirdSc2 = thirdSc2;
	}

	public void setThirdSc3(double thirdSc3) {
		this.thirdSc3 = thirdSc3;
	}

	public void setThirdSc4(double thirdSc4) {
		this.thirdSc4 = thirdSc4;
	}

	public void setThirdSc5(double thirdSc5) {
		this.thirdSc5 = thirdSc5;
	}

	public void setThirdSc6(double thirdSc6) {
		this.thirdSc6 = thirdSc6;
	}

	public void setThirdSc1PassMark(double thirdSc1PassMark) {
		this.thirdSc1PassMark = thirdSc1PassMark;
	}

	public void setThirdSc2PassMark(double thirdSc2PassMark) {
		this.thirdSc2PassMark = thirdSc2PassMark;
	}

	public void setThirdSc3PassMark(double thirdSc3PassMark) {
		this.thirdSc3PassMark = thirdSc3PassMark;
	}

	public void setThirdSc4PassMark(double thirdSc4PassMark) {
		this.thirdSc4PassMark = thirdSc4PassMark;
	}

	public void setThirdSc5PassMark(double thirdSc5PassMark) {
		this.thirdSc5PassMark = thirdSc5PassMark;
	}

	public void setThirdSc6PassMark(double thirdSc6PassMark) {
		this.thirdSc6PassMark = thirdSc6PassMark;
	}

	public void setThirdSbjTotalMark(double thirdSbjTotalMark) {
		this.thirdSbjTotalMark = thirdSbjTotalMark;
	}

	public void setThirdSbjObtainedMark(double thirdSbjObtainedMark) {
		this.thirdSbjObtainedMark = thirdSbjObtainedMark;
	}

	public void setThirdSbjLetterGrade(String thirdSbjLetterGrade) {
		this.thirdSbjLetterGrade = thirdSbjLetterGrade;
	}

	public void setThirdSbjGradePoint(double thirdSbjGradePoint) {
		this.thirdSbjGradePoint = thirdSbjGradePoint;
	}

	public void setFourthSc1(double fourthSc1) {
		this.fourthSc1 = fourthSc1;
	}

	public void setFourthSc2(double fourthSc2) {
		this.fourthSc2 = fourthSc2;
	}

	public void setFourthSc3(double fourthSc3) {
		this.fourthSc3 = fourthSc3;
	}

	public void setFourthSc4(double fourthSc4) {
		this.fourthSc4 = fourthSc4;
	}

	public void setFourthSc5(double fourthSc5) {
		this.fourthSc5 = fourthSc5;
	}

	public void setFourthSc6(double fourthSc6) {
		this.fourthSc6 = fourthSc6;
	}

	public void setFourthSc1PassMark(double fourthSc1PassMark) {
		this.fourthSc1PassMark = fourthSc1PassMark;
	}

	public void setFourthSc2PassMark(double fourthSc2PassMark) {
		this.fourthSc2PassMark = fourthSc2PassMark;
	}

	public void setFourthSc3PassMark(double fourthSc3PassMark) {
		this.fourthSc3PassMark = fourthSc3PassMark;
	}

	public void setFourthSc4PassMark(double fourthSc4PassMark) {
		this.fourthSc4PassMark = fourthSc4PassMark;
	}

	public void setFourthSc5PassMark(double fourthSc5PassMark) {
		this.fourthSc5PassMark = fourthSc5PassMark;
	}

	public void setFourthSc6PassMark(double fourthSc6PassMark) {
		this.fourthSc6PassMark = fourthSc6PassMark;
	}

	public void setFourthSbjTotalMark(double fourthSbjTotalMark) {
		this.fourthSbjTotalMark = fourthSbjTotalMark;
	}

	public void setFourthSbjObtainedMark(double fourthSbjObtainedMark) {
		this.fourthSbjObtainedMark = fourthSbjObtainedMark;
	}

	public void setFourthSbjLetterGrade(String fourthSbjLetterGrade) {
		this.fourthSbjLetterGrade = fourthSbjLetterGrade;
	}

	public void setFourthSbjGradePoint(double fourthSbjGradePoint) {
		this.fourthSbjGradePoint = fourthSbjGradePoint;
	}

	public void setGfSc1(double gfSc1) {
		this.gfSc1 = gfSc1;
	}

	public void setGfSc2(double gfSc2) {
		this.gfSc2 = gfSc2;
	}

	public void setGfSc3(double gfSc3) {
		this.gfSc3 = gfSc3;
	}

	public void setGfSc4(double gfSc4) {
		this.gfSc4 = gfSc4;
	}

	public void setGfSc5(double gfSc5) {
		this.gfSc5 = gfSc5;
	}

	public void setGfSc6(double gfSc6) {
		this.gfSc6 = gfSc6;
	}

	public void setGfSbjTotalMark(double gfSbjTotalMark) {
		this.gfSbjTotalMark = gfSbjTotalMark;
	}

	public void setGfSbjObtainedMark(double gfSbjObtainedMark) {
		this.gfSbjObtainedMark = gfSbjObtainedMark;
	}

	public void setGfSbjLetterGrade(String gfSbjLetterGrade) {
		this.gfSbjLetterGrade = gfSbjLetterGrade;
	}

	public void setGfSbjGradePoint(double gfSbjGradePoint) {
		this.gfSbjGradePoint = gfSbjGradePoint;
	}

}
