package com.openspace.education.jasper.exam.response;

public class ExamGradeResponse {

	private String gradeRange;

	private String grade;

	private double gp;

	private double pointHigh;

	private double pointLow;

	private String gradeName;

	private String gradeNote;

	public String getGradeRange() {
		return gradeRange;
	}

	public String getGrade() {
		return grade;
	}

	public double getGp() {
		return gp;
	}

	public double getPointHigh() {
		return pointHigh;
	}

	public double getPointLow() {
		return pointLow;
	}

	public String getGradeName() {
		return gradeName;
	}

	public String getGradeNote() {
		return gradeNote;
	}

	public void setGradeRange(String gradeRange) {
		this.gradeRange = gradeRange;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public void setGp(double gp) {
		this.gp = gp;
	}

	public void setPointHigh(double pointHigh) {
		this.pointHigh = pointHigh;
	}

	public void setPointLow(double pointLow) {
		this.pointLow = pointLow;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public void setGradeNote(String gradeNote) {
		this.gradeNote = gradeNote;
	}

}
