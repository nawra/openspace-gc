package com.openspace.education.jasper.idcard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.ApplicationUtils;
import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.CommonInfoUtils;
import com.openspace.education.common.ImageFactory;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.dto.DefaultSignatureDto;
import com.openspace.education.initialsetup.model.response.StudentIdCardResponse;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.jasper.utils.JasperUtils;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.model.entity.StaffIdCardTemplate;
import com.openspace.education.staff.model.response.StaffBasicView;
import com.openspace.education.staff.repository.StaffIdCardTemplateRepository;
import com.openspace.education.staff.repository.StaffRepository;
import com.openspace.education.student.model.entity.StudentIdCardTemplate;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.repository.StudentIdCardTemplateRepository;
import com.openspace.education.student.repository.StudentIdentificationRepository;
@Service
public class IdCardService {

  private Logger logger = LoggerFactory.getLogger(IdCardService.class);

  @Autowired
  private StudentIdentificationRepository studentIdentificationRepository;
  @Autowired
  private StudentIdCardTemplateRepository studentIdCardTemplateRepository;
  @Autowired
  private StaffIdCardTemplateRepository staffIdCardTemplateRepository;
  @Autowired
  private ImageFactory imageFactory;
  @Autowired
  private CommonInfoUtils commonInfoUtils;
  @Autowired
  private JasperUtils jasperUtils;
  @Autowired
  private StaffRepository staffRepository;
  
	public static final String STUDENT_IMAGE_PATH = ApplicationUtils.getFilePath("STUDENT");
	public static final String DEFAULT_IMAGE_PATH = ApplicationUtils.getFilePath("DEFAULT");
	public static final String INSTITUTE_IMAGE_PATH = ApplicationUtils.getFilePath("INSTITUTE");
	public static final String STAFF_IMAGE_PATH = ApplicationUtils.getFilePath("STAFF");
//	public static final String UNDERLINE = "...............................................................";
//	public static final int REGISTRATION_WITH_CUSTOMID = 1;
//	public static final int REGISTRATION_WITH_AUTO_ID = 0;
//	public static final String STUDENT_ID_CARD_TEMP_1_IMAGE_NAME = "std_id_card_temp1_heading.png";
//	public static final String STUDENT_ID_CARD_TEMP_2_IMAGE_NAME = "std_id_card_temp2_heading.png";
	public static final String STUDENT_ID_CARD_IMAGE_NAME = "id_barcode.png";
	public static final String STD_ID_CARD_TEMP_4_F_IMG = "std_idcard_temp_4_f.png";
	public static final String STD_ID_CARD_TEMP_4_B_IMG = "std_idcard_temp_4_b.png";
	
	public static final String STD_ID_CARD_TEMP_5_F_IMG = "std_idcard_temp_5_f.png";
	public static final String STD_ID_CARD_TEMP_5_B_IMG = "std_idcard_temp_5_b.png";
	
	public static final String STD_ID_CARD_TEMP_6_F_IMG = "std_idcard_temp_6_f.png";
	public static final String STD_ID_CARD_TEMP_6_B_IMG = "std_idcard_temp_6_b.png";
	
	
	
	public static final String STAFF_ID_CARD_TEMP_1_F_IMG = "staff_idcard_temp_1_f.png";
	public static final String STAFF_ID_CARD_TEMP_1_B_IMG = "staff_idcard_temp_1_b.png";
	
	public static final String STAFF_ID_CARD_TEMP_2_F_IMG = "staff_idcard_temp_2_f.png";
	public static final String STAFF_ID_CARD_TEMP_2_B_IMG = "staff_idcard_temp_2_b.png";
	
	public static final String STUDENT_ID_CARD_PATH = "jasper/studentidcard/";
//	public static final String ADMIT_CARD_PATH = "jasper/AdmitCard/";

//	public static final String STAFF_ID_CARD_TEMP_3_IMAGE_NAME = "staff_id_card_temp_3_heading_image.png";
//	public static final int HR_ID_CARD_TEMP_2_EMAIL_CONTAINER_HEIGHT = 12;
//	public static final int HR_ID_CARD_TEMP_2_EMAIL_CONTAINER_WIDTH = 112;
//	public static final String STAFF_ID_CARD_TEMP_7_IMAGE_NAME = "staff_id_card_temp_7_heading_image.png";
	public static final String HR_ID_CARD_BASE_PATH = "jasper/staffIdCard/";
  
public BaseResponse downloadStudentIdCard(Integer startRoll, Integer endRoll,Long classConfigId) {
              
        String card = "";

        Institute institute = UserInfoUtils.getLoggedInInstitute();
        BaseResponse br = new BaseResponse();
       
        
        List<StudentIdentification> studentIdentifications = provideStudentIdentifications(institute,startRoll,endRoll,classConfigId);

        if (studentIdentifications.isEmpty()) {
          
          br.setMessage("Student Not found");
            return br;
        }
        
        StudentIdCardTemplate studentIdcardTemplate = studentIdCardTemplateRepository.findByInstitute(institute);
        
        List<StudentIdCardResponse> studentIdCardResponse = studentIdentifications.stream().map(si -> getStudentIdCardResponse(si,studentIdcardTemplate)).collect(Collectors.toList());
        
        Map<String, Object> map = getParamForStudentIdCard(institute,classConfigId,studentIdcardTemplate);
        
        map.put("duLogo", DEFAULT_IMAGE_PATH+"du.png");
                          
        
                switch (studentIdcardTemplate.getTemplateId()) {
                    case 101:
                        card = STUDENT_ID_CARD_PATH + "Student_ID_Card_101.jasper";
                        break;
                    case 102:
                      card = STUDENT_ID_CARD_PATH + "Student_ID_Card_102.jasper";
                        break;
                    case 103:
                      card = STUDENT_ID_CARD_PATH + "Student_ID_Card_103.jasper";
                        break;
                        
                    case 104:
                    	 map.put("front_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_4_F_IMG);
                        card = STUDENT_ID_CARD_PATH + "Student_ID_Card_104.jasper";
                        break;
                                               
                    case 105:
                    	 map.put("front_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_5_F_IMG);
                        card = STUDENT_ID_CARD_PATH + "Student_ID_Card_105.jasper";
                        break;
                        
                    case 106:
                   	 map.put("front_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_6_F_IMG);
                       card = STUDENT_ID_CARD_PATH + "Student_ID_Card_106.jasper";
                       break;
                       
                    case 109:
//                      	 map.put("front_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_6_F_IMG);
                          card = STUDENT_ID_CARD_PATH + "Student_ID_Card_109.jasper";
                          break;
                    
                    default:
                      card = STUDENT_ID_CARD_PATH + "Student_ID_Card_104.jasper";
                        break;
                }
                  
                try {
          jasperUtils.jasperPrintWithList(studentIdCardResponse, map, card, "STUDENT_ID_CARD");
        } catch (Exception e) {
        	
        	e.printStackTrace();
        	br.setMessageType(0);
        	br.setMessage("Somthing went wrong");
          logger.error(e.getMessage());
        }
         
                return br;
            }




public BaseResponse downloadStudentIdCardBackPart(Integer startRoll, Integer endRoll,Long classConfigId) {
    
    String card = "";

    Institute institute = UserInfoUtils.getLoggedInInstitute();
    BaseResponse br = new BaseResponse();

    StudentIdCardTemplate studentIdcardTemplate = studentIdCardTemplateRepository.findByInstitute(institute);
    
    List<StudentIdCardResponse> studentIdCardResponse = new ArrayList<StudentIdCardResponse>();
    
    StudentIdCardResponse sr = new StudentIdCardResponse();
    
    studentIdCardResponse.add(sr);
    
    Map<String, Object> map = getParamForStudentIdCard(institute,classConfigId,studentIdcardTemplate);

      
            switch (studentIdcardTemplate.getTemplateId()) {
                case 101:
                    card = STUDENT_ID_CARD_PATH + "Student_ID_Card_101.jasper";
                    break;
                case 102:
                  card = STUDENT_ID_CARD_PATH + "Student_ID_Card_102.jasper";
                    break;
                case 103:
                  card = STUDENT_ID_CARD_PATH + "Student_ID_Card_103.jasper";
                    break;
                    
                case 104:
                	map.put("back_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_4_B_IMG);
                    card = STUDENT_ID_CARD_PATH + "Student_ID_Card_104_back.jasper";
                    break;
                    
                case 105:
                	map.put("back_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_5_B_IMG);
                    card = STUDENT_ID_CARD_PATH + "Student_ID_Card_105_back.jasper";
                    break;
                    
                case 106:
                	map.put("back_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_6_B_IMG);
                    card = STUDENT_ID_CARD_PATH + "Student_ID_Card_106_back.jasper";
                    break;
                    
                case 109:
                    card = STUDENT_ID_CARD_PATH + "Student_ID_Card_109_back.jasper";
                    break;
                
                default:
                  card = STUDENT_ID_CARD_PATH + "Student_ID_Card_104.jasper";
                    break;
            }
              
            try {
      jasperUtils.jasperPrintWithList(studentIdCardResponse, map, card, "STUDENT_ID_CARD");
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
     
            return br;
        }



public List<StudentIdentification> provideStudentIdentifications(Institute institute, int startRoll,int endRoll,Long calssConfigId) {
  
    if (startRoll == 0 || endRoll == 0) {
      return studentIdentificationRepository
          .findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusOrderByStudentRollAsc(
              institute, institute.getAcademicYear(), calssConfigId, true);
    } else {
      return studentIdentificationRepository
          .findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusAndStudentRollBetweenOrderByStudentRollAsc(
              institute, institute.getAcademicYear(), calssConfigId, true, startRoll, endRoll);
    }
  }



public StudentIdCardResponse getStudentIdCardResponse(StudentIdentification studentIdentification, StudentIdCardTemplate ct) {
    
  StudentIdCardResponse studentIdCardResponse = new StudentIdCardResponse();
  
  studentIdCardResponse.setCustomStudentId(studentIdentification.getStudentBasic().getCustomStudentId());
  studentIdCardResponse.setStudentName(studentIdentification.getStudentBasic().getStudentName());
  studentIdCardResponse.setFatherName(studentIdentification.getStudentBasic().getFatherName());
  studentIdCardResponse.setMotherName(studentIdentification.getStudentBasic().getMotherName());
  studentIdCardResponse.setStudentRoll(studentIdentification.getStudentRoll());
  studentIdCardResponse.setAcademicYear(studentIdentification.getAcademicYear());
  
  studentIdCardResponse.setClassName(studentIdentification.getClassConfigurationInfo().getClassInfo().getName());
  
  studentIdCardResponse.setShiftName(studentIdentification.getClassConfigurationInfo().getShiftInfo().getName());
  studentIdCardResponse.setSectionName(studentIdentification.getClassConfigurationInfo().getSectionInfo().getName());
  studentIdCardResponse.setGroupName(studentIdentification.getGroupInfo().getName());
  studentIdCardResponse.setStudentImageName(studentIdentification.getStudentBasic().getImageName());
  
  studentIdCardResponse.setFatherImageName(studentIdentification.getStudentBasic().getFatherPhoto());
  studentIdCardResponse.setMotherImageName(studentIdentification.getStudentBasic().getMotherPhoto());
  
  
  
  if (studentIdentification.getStudentBasic().getStudentDOB() != null) {
      studentIdCardResponse.setDateofbirth(studentIdentification.getStudentBasic().getStudentDOB()+"");
    } else {
      studentIdCardResponse.setDateofbirth(null);
    }
  
    if (studentIdentification.getStudentBasic().getBloodGroup() != null) {
      studentIdCardResponse.setBloodGroup(studentIdentification.getStudentBasic().getBloodGroup());
    } else {
      studentIdCardResponse.setBloodGroup("N/A");
    }

    
    studentIdCardResponse.setGuardianMobile(studentIdentification.getStudentBasic().getGuardianMobile());
    
    if (studentIdentification.getStudentBasic().getAcademicSession() != null) {
      studentIdCardResponse.setStudentSession(studentIdentification.getStudentBasic().getAcademicSession());
    } else {
      studentIdCardResponse.setStudentSession(studentIdentification.getAcademicYear().toString());
    }   
    
    if(studentIdCardResponse.getStudentImageName() != null && ! studentIdCardResponse.getStudentImageName().equals("")) {
     
     int cornerRadius = imageFactory.provideCornerRadiusForStudent(ct.getTemplateId()); 
     
          try {
            studentIdCardResponse.setImage(imageFactory.cropImage(cornerRadius, STUDENT_IMAGE_PATH + (studentIdCardResponse.getStudentImageName()))); 
          }catch(Exception e) {
            logger.error("Image could not be added for cropping issue" + e.getMessage());
          }         
        }
        
    
    return studentIdCardResponse;
}



public Map<String, Object> getParamForStudentIdCard(Institute institute,Long classConfigId, StudentIdCardTemplate cardTemplate) {

	
	DefaultSignatureDto defaultSignature = commonInfoUtils.provideDefaultSignDto(institute);
	
    Map<String, Object> map = new HashMap<>();
    map.put("instituteID", institute.getInstituteId());
    map.put("academicYear", institute.getAcademicYear());
    map.put("classConfigID", classConfigId);
    map.put("stdImagePath", STUDENT_IMAGE_PATH);
  
    
    map = instituteInfo(institute, map,defaultSignature.getStudentIdCardSignStatus(),defaultSignature.getStudentIdCardSignName(),defaultSignature.getStudentIdCardSignTitle());

    map.put("id_barcode", DEFAULT_IMAGE_PATH + STUDENT_ID_CARD_IMAGE_NAME);

    return map;
  }

	public BaseResponse downloadStaffIdCard() {

		BaseResponse br = new BaseResponse();
		
		String jasperFile = "";
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Staff> staffList=staffRepository.findByInstituteAndStaffStatusOrderByStaffSerialAsc(institute,1);
		
		if(staffList==null) {
			br.setMessage("Staff Info Not Found");
			logger.error("=============Staff Info not found+++++");
			return br;
		}

		StaffIdCardTemplate staffIdCardTemplate = staffIdCardTemplateRepository.findByInstitute(institute);

		List<StaffBasicView> staffBasicViews = staffList.stream().map(staff -> staffToStaffResponse(staff, staffIdCardTemplate)).collect(Collectors.toList());

		try {

			Map<String, Object> map = getParamForStaffIdCard(institute);


			switch (staffIdCardTemplate.getTemplateId()) {
			
			
			
			case 201:
				 map.put("front_img", DEFAULT_IMAGE_PATH + STAFF_ID_CARD_TEMP_1_F_IMG);
				 jasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_201.jasper";
				 break;
			case 202:
				 map.put("front_img", DEFAULT_IMAGE_PATH + STAFF_ID_CARD_TEMP_2_F_IMG);
				 jasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_202.jasper";
				
				break;

			default:
				break;
			}

			jasperUtils.jasperPrintWithList(staffBasicViews, map, jasperFile, "STAFF_ID_CARD");

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return br;
	}
	
	

//	private String getStaffJasperFile(StaffIdCardTemplate ct) {
//
//		String staffIdCardJasperFile = "";
//
//		if (ct.getTemplateId() == null && ct.getTemplateId() == null) {
//
//			 map.put("front_img", DEFAULT_IMAGE_PATH + STD_ID_CARD_TEMP_4_F_IMG);
//			staffIdCardJasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_201.jasper";
//
//		} else if (ct.getTemplateId() == 201) {
//			staffIdCardJasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_201.jasper";
//
//		} else if (ct.getTemplateId() == 202) {
//
//			staffIdCardJasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_202.jasper";
//		} else if (ct.getTemplateId() == 203) {
//
//			staffIdCardJasperFile = HR_ID_CARD_BASE_PATH + "Staff_ID_Card_203.jasper";
//		} 
//		return staffIdCardJasperFile;
//	}

	
	public StaffBasicView staffToStaffResponse(Staff staff, StaffIdCardTemplate st) {

		StaffBasicView staffBasicView = new StaffBasicView();

		staffBasicView.setCustomStaffId(staff.getCustomStaffId());
		staffBasicView.setStaffName(staff.getStaffName());
		staffBasicView.setImageName(staff.getImageName());
		staffBasicView.setMobileNumber(staff.getMobileNumber());
		staffBasicView.setEmail(staff.getEmail());
		staffBasicView.setImageName(staff.getImageName());
	

		if (staff.getDesignation() != null) {
			staffBasicView.setDesignationName(staff.getDesignation().getName());
		}

		if (staff.getBloodGroup() != null) {
			staffBasicView.setBloodGroup(staff.getBloodGroup());
		} else {
			staffBasicView.setBloodGroup("N/A");
		}

//		if (staffBasicView.getImageName() != null && !staffBasicView.getImageName().equals("")) {
//
//			int cornerRadius = imageFactory.provideCornerRadiusForStaff(st.getTemplateId());
//
//			try {
//				staffBasicView.setImage(
//						imageFactory.cropImage(cornerRadius, STAFF_IMAGE_PATH + (staffBasicView.getImageName())));
//			} catch (Exception e) {
//				logger.error("Image could not be added for cropping issue" + e.getMessage());
//			}
//		}

		return staffBasicView;
	}


	public Map<String, Object> getParamForStaffIdCard(Institute institute) {
		
		DefaultSignatureDto defaultSignature = commonInfoUtils.provideDefaultSignDto(institute);

		Map<String, Object> map = new HashMap<>();

		map = instituteInfo(institute, map, defaultSignature.getHrIdCardSignStatus(), defaultSignature.getHrIdCardSignName(),defaultSignature.getHrIdCardSignTitle());

		map.put("staffImgPath", STAFF_IMAGE_PATH);

		return map;
	}
	
	
	private Map<String, Object> instituteInfo(Institute institute, Map<String, Object> map, int signatureStatus,
			String signName, String signTitle) {

		map.put("instituteLogoPath", INSTITUTE_IMAGE_PATH);
		map.put("instituteLogoName", institute.getImageName());
		map.put("instituteName", institute.getInstituteName());
		map.put("instituteAddress", institute.getAddress());

		if (signatureStatus == 1) {
			map.put("signatureImagePath", DEFAULT_IMAGE_PATH);
			map.put("signatureImgName", signName);
			map.put("signatureTitle", signTitle);
		}

		return map;
	}


}
