package com.openspace.education.jasper.exam.response;

import java.util.List;

public class GrandFinalMarksheetResponse {

	private String studentId;
	private String customStudentId;
	private Long identificationId;
	private String studentName;
	private String fatherName;
	private String motherName;
	private String gender;
	private String imageName;
	private int studentRoll;
	private String section;
	private String examName;
	private String groupName;
	private String sessionOrAcademicYear;

	private List<ExamGradeResponse> grades;
	private List<String> shortCodeTitles;

	private List<GrandFinalExamMarkResponse> examMarkSbjs1;
	private List<GrandFinalExamMarkResponse> examMarkSbjs2;
	private List<GrandFinalExamMarkResponse> examMarkSbjs3;
	private List<GrandFinalExamMarkResponse> examMarkSbjs4;
	private List<GrandFinalExamMarkResponse> examMarkSbjs5;
	private List<GrandFinalExamMarkResponse> examMarkSbjs6;
	private List<GrandFinalExamMarkResponse> examMarkSbjs7;
	private List<GrandFinalExamMarkResponse> examMarkSbjs8;

	private List<GrandFinalExamMarkResponse> examMarkNonMergeSbjs;
	private List<GrandFinalExamMarkResponse> uncountableExamMarks;

	private String firstExam;
	private double firstExamObtainedMark;
	private double firstExamGpaWithFourth;
	private double firstExamGpaWithoutFourth;
	private String firstExamLetterGrade;
	private Integer firstExamTotalFailedSubject;
	private Integer firstExamTotalAbsent;
	private Integer firstExamTotalWorkingDay;
	private String firstExamResultStatus;
	private int firstExamMeritPosition;

	private String secondExam;
	private double secondExamObtainedMark;
	private double secondExamGpaWithFourth;
	private double secondExamGpaWithoutFourth;
	private String secondExamLetterGrade;
	private Integer secondExamTotalFailedSubject;
	private Integer secondExamTotalAbsent;
	private Integer secondExamTotalWorkingDay;
	private String secondExamResultStatus;
	private int secondExamMeritPosition;
	private Long secondExamConfigId;

	private String thirdExam;
	private double thirdExamObtainedMark;
	private double thirdExamGpaWithFourth;
	private double thirdExamGpaWithoutFourth;
	private String thirdExamLetterGrade;
	private Integer thirdExamTotalFailedSubject;
	private Integer thirdExamTotalAbsent;
	private Integer thirdExamTotalWorkingDay;
	private String thirdExamResultStatus;
	private int thirdExamMeritPosition;

	private String fourthExam;
	private double fourthExamObtainedMark;
	private double fourthExamGpaWithFourth;
	private double fourthExamGpaWithoutFourth;
	private String fourthExamLetterGrade;
	private Integer fourthExamTotalFailedSubject;
	private Integer fourthExamTotalAbsent;
	private Integer fourthExamTotalWorkingDay;
	private String fourthExamResultStatus;
	private int fourthExamMeritPosition;

	private String gfExam;
	private double gfExamObtainedMark;
	private double gfExamGpaWithFourth;
	private double gfExamGpaWithoutFourth;
	private String gfExamLetterGrade;
	private String gfExamLetterGradeForBarcode;
	private Integer gfExamTotalFailedSubject;
	private Integer gfExamTotalAbsent;
	private Integer gfExamTotalWorkingDay;
	private String gfExamResultStatus;
	private int gfExamMeritPosition;
	private Long gfExamConfigId;

	private int choosenMeritPosition;
	private int classPosition;
	private int shiftPosition;
	private int sectionPosition;
	private int groupPosition;
	private int numOfFailedSbjs;
	private int totalWorkingDays;
	private int totalPresentDays;
	private int totalAbsentDays;

	private String gfExamRemarks;

	private double firstSbj1MergeGradePoint;
	private String firstSbj1MergeGradeName;
	private double firstSbj2MergeGradePoint;
	private String firstSbj2MergeGradeName;
	private double firstSbj3MergeGradePoint;
	private String firstSbj3MergeGradeName;
	private double firstSbj4MergeGradePoint;
	private String firstSbj4MergeGradeName;
	private double firstSbj5MergeGradePoint;
	private String firstSbj5MergeGradeName;
	private double firstSbj6MergeGradePoint;
	private String firstSbj6MergeGradeName;
	private double firstSbj7MergeGradePoint;
	private String firstSbj7MergeGradeName;

	private double secondSbj1MergeGradePoint;
	private String secondSbj1MergeGradeName;
	private double secondSbj2MergeGradePoint;
	private String secondSbj2MergeGradeName;
	private double secondSbj3MergeGradePoint;
	private String secondSbj3MergeGradeName;
	private double secondSbj4MergeGradePoint;
	private String secondSbj4MergeGradeName;
	private double secondSbj5MergeGradePoint;
	private String secondSbj5MergeGradeName;
	private double secondSbj6MergeGradePoint;
	private String secondSbj6MergeGradeName;
	private double secondSbj7MergeGradePoint;
	private String secondSbj7MergeGradeName;

	private double thirdSbj1MergeGradePoint;
	private String thirdSbj1MergeGradeName;
	private double thirdSbj2MergeGradePoint;
	private String thirdSbj2MergeGradeName;
	private double thirdSbj3MergeGradePoint;
	private String thirdSbj3MergeGradeName;
	private double thirdSbj4MergeGradePoint;
	private String thirdSbj4MergeGradeName;
	private double thirdSbj5MergeGradePoint;
	private String thirdSbj5MergeGradeName;
	private double thirdSbj6MergeGradePoint;
	private String thirdSbj6MergeGradeName;
	private double thirdSbj7MergeGradePoint;
	private String thirdSbj7MergeGradeName;

	private double fourthSbj1MergeGradePoint;
	private String fourthSbj1MergeGradeName;
	private double fourthSbj2MergeGradePoint;
	private String fourthSbj2MergeGradeName;
	private double fourthSbj3MergeGradePoint;
	private String fourthSbj3MergeGradeName;
	private double fourthSbj4MergeGradePoint;
	private String fourthSbj4MergeGradeName;
	private double fourthSbj5MergeGradePoint;
	private String fourthSbj5MergeGradeName;
	private double fourthSbj6MergeGradePoint;
	private String fourthSbj6MergeGradeName;
	private double fourthSbj7MergeGradePoint;
	private String fourthSbj7MergeGradeName;

	private double gfSbj1MergeGradePoint;
	private String gfSbj1MergeGradeName;
	private double gfSbj2MergeGradePoint;
	private String gfSbj2MergeGradeName;
	private double gfSbj3MergeGradePoint;
	private String gfSbj3MergeGradeName;
	private double gfSbj4MergeGradePoint;
	private String gfSbj4MergeGradeName;
	private double gfSbj5MergeGradePoint;
	private String gfSbj5MergeGradeName;
	private double gfSbj6MergeGradePoint;
	private String gfSbj6MergeGradeName;
	private double gfSbj7MergeGradePoint;
	private String gfSbj7MergeGradeName;

	public String getStudentId() {
		return studentId;
	}

	public String getCustomStudentId() {
		return customStudentId;
	}

	public Long getIdentificationId() {
		return identificationId;
	}

	public String getStudentName() {
		return studentName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getGender() {
		return gender;
	}

	public String getImageName() {
		return imageName;
	}

	public int getStudentRoll() {
		return studentRoll;
	}

	public String getSection() {
		return section;
	}

	public String getExamName() {
		return examName;
	}

	public String getGroupName() {
		return groupName;
	}

	public String getSessionOrAcademicYear() {
		return sessionOrAcademicYear;
	}

	public List<ExamGradeResponse> getGrades() {
		return grades;
	}

	public List<String> getShortCodeTitles() {
		return shortCodeTitles;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs1() {
		return examMarkSbjs1;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs2() {
		return examMarkSbjs2;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs3() {
		return examMarkSbjs3;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs4() {
		return examMarkSbjs4;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs5() {
		return examMarkSbjs5;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs6() {
		return examMarkSbjs6;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs7() {
		return examMarkSbjs7;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkSbjs8() {
		return examMarkSbjs8;
	}

	public List<GrandFinalExamMarkResponse> getExamMarkNonMergeSbjs() {
		return examMarkNonMergeSbjs;
	}

	public List<GrandFinalExamMarkResponse> getUncountableExamMarks() {
		return uncountableExamMarks;
	}

	public String getFirstExam() {
		return firstExam;
	}

	public double getFirstExamObtainedMark() {
		return firstExamObtainedMark;
	}

	public double getFirstExamGpaWithFourth() {
		return firstExamGpaWithFourth;
	}

	public double getFirstExamGpaWithoutFourth() {
		return firstExamGpaWithoutFourth;
	}

	public String getFirstExamLetterGrade() {
		return firstExamLetterGrade;
	}

	public Integer getFirstExamTotalFailedSubject() {
		return firstExamTotalFailedSubject;
	}

	public Integer getFirstExamTotalAbsent() {
		return firstExamTotalAbsent;
	}

	public Integer getFirstExamTotalWorkingDay() {
		return firstExamTotalWorkingDay;
	}

	public String getFirstExamResultStatus() {
		return firstExamResultStatus;
	}

	public int getFirstExamMeritPosition() {
		return firstExamMeritPosition;
	}

	public String getSecondExam() {
		return secondExam;
	}

	public double getSecondExamObtainedMark() {
		return secondExamObtainedMark;
	}

	public double getSecondExamGpaWithFourth() {
		return secondExamGpaWithFourth;
	}

	public double getSecondExamGpaWithoutFourth() {
		return secondExamGpaWithoutFourth;
	}

	public String getSecondExamLetterGrade() {
		return secondExamLetterGrade;
	}

	public Integer getSecondExamTotalFailedSubject() {
		return secondExamTotalFailedSubject;
	}

	public Integer getSecondExamTotalAbsent() {
		return secondExamTotalAbsent;
	}

	public Integer getSecondExamTotalWorkingDay() {
		return secondExamTotalWorkingDay;
	}

	public String getSecondExamResultStatus() {
		return secondExamResultStatus;
	}

	public int getSecondExamMeritPosition() {
		return secondExamMeritPosition;
	}

	public Long getSecondExamConfigId() {
		return secondExamConfigId;
	}

	public String getThirdExam() {
		return thirdExam;
	}

	public double getThirdExamObtainedMark() {
		return thirdExamObtainedMark;
	}

	public double getThirdExamGpaWithFourth() {
		return thirdExamGpaWithFourth;
	}

	public double getThirdExamGpaWithoutFourth() {
		return thirdExamGpaWithoutFourth;
	}

	public String getThirdExamLetterGrade() {
		return thirdExamLetterGrade;
	}

	public Integer getThirdExamTotalFailedSubject() {
		return thirdExamTotalFailedSubject;
	}

	public Integer getThirdExamTotalAbsent() {
		return thirdExamTotalAbsent;
	}

	public Integer getThirdExamTotalWorkingDay() {
		return thirdExamTotalWorkingDay;
	}

	public String getThirdExamResultStatus() {
		return thirdExamResultStatus;
	}

	public int getThirdExamMeritPosition() {
		return thirdExamMeritPosition;
	}

	public String getFourthExam() {
		return fourthExam;
	}

	public double getFourthExamObtainedMark() {
		return fourthExamObtainedMark;
	}

	public double getFourthExamGpaWithFourth() {
		return fourthExamGpaWithFourth;
	}

	public double getFourthExamGpaWithoutFourth() {
		return fourthExamGpaWithoutFourth;
	}

	public String getFourthExamLetterGrade() {
		return fourthExamLetterGrade;
	}

	public Integer getFourthExamTotalFailedSubject() {
		return fourthExamTotalFailedSubject;
	}

	public Integer getFourthExamTotalAbsent() {
		return fourthExamTotalAbsent;
	}

	public Integer getFourthExamTotalWorkingDay() {
		return fourthExamTotalWorkingDay;
	}

	public String getFourthExamResultStatus() {
		return fourthExamResultStatus;
	}

	public int getFourthExamMeritPosition() {
		return fourthExamMeritPosition;
	}

	public String getGfExam() {
		return gfExam;
	}

	public double getGfExamObtainedMark() {
		return gfExamObtainedMark;
	}

	public double getGfExamGpaWithFourth() {
		return gfExamGpaWithFourth;
	}

	public double getGfExamGpaWithoutFourth() {
		return gfExamGpaWithoutFourth;
	}

	public String getGfExamLetterGrade() {
		return gfExamLetterGrade;
	}

	public String getGfExamLetterGradeForBarcode() {
		return gfExamLetterGradeForBarcode;
	}

	public Integer getGfExamTotalFailedSubject() {
		return gfExamTotalFailedSubject;
	}

	public Integer getGfExamTotalAbsent() {
		return gfExamTotalAbsent;
	}

	public Integer getGfExamTotalWorkingDay() {
		return gfExamTotalWorkingDay;
	}

	public String getGfExamResultStatus() {
		return gfExamResultStatus;
	}

	public int getGfExamMeritPosition() {
		return gfExamMeritPosition;
	}

	public Long getGfExamConfigId() {
		return gfExamConfigId;
	}

	public int getChoosenMeritPosition() {
		return choosenMeritPosition;
	}

	public int getClassPosition() {
		return classPosition;
	}

	public int getShiftPosition() {
		return shiftPosition;
	}

	public int getSectionPosition() {
		return sectionPosition;
	}

	public int getGroupPosition() {
		return groupPosition;
	}

	public int getNumOfFailedSbjs() {
		return numOfFailedSbjs;
	}

	public int getTotalWorkingDays() {
		return totalWorkingDays;
	}

	public int getTotalPresentDays() {
		return totalPresentDays;
	}

	public int getTotalAbsentDays() {
		return totalAbsentDays;
	}

	public String getGfExamRemarks() {
		return gfExamRemarks;
	}

	public double getFirstSbj1MergeGradePoint() {
		return firstSbj1MergeGradePoint;
	}

	public String getFirstSbj1MergeGradeName() {
		return firstSbj1MergeGradeName;
	}

	public double getFirstSbj2MergeGradePoint() {
		return firstSbj2MergeGradePoint;
	}

	public String getFirstSbj2MergeGradeName() {
		return firstSbj2MergeGradeName;
	}

	public double getFirstSbj3MergeGradePoint() {
		return firstSbj3MergeGradePoint;
	}

	public String getFirstSbj3MergeGradeName() {
		return firstSbj3MergeGradeName;
	}

	public double getFirstSbj4MergeGradePoint() {
		return firstSbj4MergeGradePoint;
	}

	public String getFirstSbj4MergeGradeName() {
		return firstSbj4MergeGradeName;
	}

	public double getFirstSbj5MergeGradePoint() {
		return firstSbj5MergeGradePoint;
	}

	public String getFirstSbj5MergeGradeName() {
		return firstSbj5MergeGradeName;
	}

	public double getFirstSbj6MergeGradePoint() {
		return firstSbj6MergeGradePoint;
	}

	public String getFirstSbj6MergeGradeName() {
		return firstSbj6MergeGradeName;
	}

	public double getFirstSbj7MergeGradePoint() {
		return firstSbj7MergeGradePoint;
	}

	public String getFirstSbj7MergeGradeName() {
		return firstSbj7MergeGradeName;
	}

	public double getSecondSbj1MergeGradePoint() {
		return secondSbj1MergeGradePoint;
	}

	public String getSecondSbj1MergeGradeName() {
		return secondSbj1MergeGradeName;
	}

	public double getSecondSbj2MergeGradePoint() {
		return secondSbj2MergeGradePoint;
	}

	public String getSecondSbj2MergeGradeName() {
		return secondSbj2MergeGradeName;
	}

	public double getSecondSbj3MergeGradePoint() {
		return secondSbj3MergeGradePoint;
	}

	public String getSecondSbj3MergeGradeName() {
		return secondSbj3MergeGradeName;
	}

	public double getSecondSbj4MergeGradePoint() {
		return secondSbj4MergeGradePoint;
	}

	public String getSecondSbj4MergeGradeName() {
		return secondSbj4MergeGradeName;
	}

	public double getSecondSbj5MergeGradePoint() {
		return secondSbj5MergeGradePoint;
	}

	public String getSecondSbj5MergeGradeName() {
		return secondSbj5MergeGradeName;
	}

	public double getSecondSbj6MergeGradePoint() {
		return secondSbj6MergeGradePoint;
	}

	public String getSecondSbj6MergeGradeName() {
		return secondSbj6MergeGradeName;
	}

	public double getSecondSbj7MergeGradePoint() {
		return secondSbj7MergeGradePoint;
	}

	public String getSecondSbj7MergeGradeName() {
		return secondSbj7MergeGradeName;
	}

	public double getThirdSbj1MergeGradePoint() {
		return thirdSbj1MergeGradePoint;
	}

	public String getThirdSbj1MergeGradeName() {
		return thirdSbj1MergeGradeName;
	}

	public double getThirdSbj2MergeGradePoint() {
		return thirdSbj2MergeGradePoint;
	}

	public String getThirdSbj2MergeGradeName() {
		return thirdSbj2MergeGradeName;
	}

	public double getThirdSbj3MergeGradePoint() {
		return thirdSbj3MergeGradePoint;
	}

	public String getThirdSbj3MergeGradeName() {
		return thirdSbj3MergeGradeName;
	}

	public double getThirdSbj4MergeGradePoint() {
		return thirdSbj4MergeGradePoint;
	}

	public String getThirdSbj4MergeGradeName() {
		return thirdSbj4MergeGradeName;
	}

	public double getThirdSbj5MergeGradePoint() {
		return thirdSbj5MergeGradePoint;
	}

	public String getThirdSbj5MergeGradeName() {
		return thirdSbj5MergeGradeName;
	}

	public double getThirdSbj6MergeGradePoint() {
		return thirdSbj6MergeGradePoint;
	}

	public String getThirdSbj6MergeGradeName() {
		return thirdSbj6MergeGradeName;
	}

	public double getThirdSbj7MergeGradePoint() {
		return thirdSbj7MergeGradePoint;
	}

	public String getThirdSbj7MergeGradeName() {
		return thirdSbj7MergeGradeName;
	}

	public double getFourthSbj1MergeGradePoint() {
		return fourthSbj1MergeGradePoint;
	}

	public String getFourthSbj1MergeGradeName() {
		return fourthSbj1MergeGradeName;
	}

	public double getFourthSbj2MergeGradePoint() {
		return fourthSbj2MergeGradePoint;
	}

	public String getFourthSbj2MergeGradeName() {
		return fourthSbj2MergeGradeName;
	}

	public double getFourthSbj3MergeGradePoint() {
		return fourthSbj3MergeGradePoint;
	}

	public String getFourthSbj3MergeGradeName() {
		return fourthSbj3MergeGradeName;
	}

	public double getFourthSbj4MergeGradePoint() {
		return fourthSbj4MergeGradePoint;
	}

	public String getFourthSbj4MergeGradeName() {
		return fourthSbj4MergeGradeName;
	}

	public double getFourthSbj5MergeGradePoint() {
		return fourthSbj5MergeGradePoint;
	}

	public String getFourthSbj5MergeGradeName() {
		return fourthSbj5MergeGradeName;
	}

	public double getFourthSbj6MergeGradePoint() {
		return fourthSbj6MergeGradePoint;
	}

	public String getFourthSbj6MergeGradeName() {
		return fourthSbj6MergeGradeName;
	}

	public double getFourthSbj7MergeGradePoint() {
		return fourthSbj7MergeGradePoint;
	}

	public String getFourthSbj7MergeGradeName() {
		return fourthSbj7MergeGradeName;
	}

	public double getGfSbj1MergeGradePoint() {
		return gfSbj1MergeGradePoint;
	}

	public String getGfSbj1MergeGradeName() {
		return gfSbj1MergeGradeName;
	}

	public double getGfSbj2MergeGradePoint() {
		return gfSbj2MergeGradePoint;
	}

	public String getGfSbj2MergeGradeName() {
		return gfSbj2MergeGradeName;
	}

	public double getGfSbj3MergeGradePoint() {
		return gfSbj3MergeGradePoint;
	}

	public String getGfSbj3MergeGradeName() {
		return gfSbj3MergeGradeName;
	}

	public double getGfSbj4MergeGradePoint() {
		return gfSbj4MergeGradePoint;
	}

	public String getGfSbj4MergeGradeName() {
		return gfSbj4MergeGradeName;
	}

	public double getGfSbj5MergeGradePoint() {
		return gfSbj5MergeGradePoint;
	}

	public String getGfSbj5MergeGradeName() {
		return gfSbj5MergeGradeName;
	}

	public double getGfSbj6MergeGradePoint() {
		return gfSbj6MergeGradePoint;
	}

	public String getGfSbj6MergeGradeName() {
		return gfSbj6MergeGradeName;
	}

	public double getGfSbj7MergeGradePoint() {
		return gfSbj7MergeGradePoint;
	}

	public String getGfSbj7MergeGradeName() {
		return gfSbj7MergeGradeName;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setStudentRoll(int studentRoll) {
		this.studentRoll = studentRoll;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setSessionOrAcademicYear(String sessionOrAcademicYear) {
		this.sessionOrAcademicYear = sessionOrAcademicYear;
	}

	public void setGrades(List<ExamGradeResponse> grades) {
		this.grades = grades;
	}

	public void setShortCodeTitles(List<String> shortCodeTitles) {
		this.shortCodeTitles = shortCodeTitles;
	}

	public void setExamMarkSbjs1(List<GrandFinalExamMarkResponse> examMarkSbjs1) {
		this.examMarkSbjs1 = examMarkSbjs1;
	}

	public void setExamMarkSbjs2(List<GrandFinalExamMarkResponse> examMarkSbjs2) {
		this.examMarkSbjs2 = examMarkSbjs2;
	}

	public void setExamMarkSbjs3(List<GrandFinalExamMarkResponse> examMarkSbjs3) {
		this.examMarkSbjs3 = examMarkSbjs3;
	}

	public void setExamMarkSbjs4(List<GrandFinalExamMarkResponse> examMarkSbjs4) {
		this.examMarkSbjs4 = examMarkSbjs4;
	}

	public void setExamMarkSbjs5(List<GrandFinalExamMarkResponse> examMarkSbjs5) {
		this.examMarkSbjs5 = examMarkSbjs5;
	}

	public void setExamMarkSbjs6(List<GrandFinalExamMarkResponse> examMarkSbjs6) {
		this.examMarkSbjs6 = examMarkSbjs6;
	}

	public void setExamMarkSbjs7(List<GrandFinalExamMarkResponse> examMarkSbjs7) {
		this.examMarkSbjs7 = examMarkSbjs7;
	}

	public void setExamMarkSbjs8(List<GrandFinalExamMarkResponse> examMarkSbjs8) {
		this.examMarkSbjs8 = examMarkSbjs8;
	}

	public void setExamMarkNonMergeSbjs(List<GrandFinalExamMarkResponse> examMarkNonMergeSbjs) {
		this.examMarkNonMergeSbjs = examMarkNonMergeSbjs;
	}

	public void setUncountableExamMarks(List<GrandFinalExamMarkResponse> uncountableExamMarks) {
		this.uncountableExamMarks = uncountableExamMarks;
	}

	public void setFirstExam(String firstExam) {
		this.firstExam = firstExam;
	}

	public void setFirstExamObtainedMark(double firstExamObtainedMark) {
		this.firstExamObtainedMark = firstExamObtainedMark;
	}

	public void setFirstExamGpaWithFourth(double firstExamGpaWithFourth) {
		this.firstExamGpaWithFourth = firstExamGpaWithFourth;
	}

	public void setFirstExamGpaWithoutFourth(double firstExamGpaWithoutFourth) {
		this.firstExamGpaWithoutFourth = firstExamGpaWithoutFourth;
	}

	public void setFirstExamLetterGrade(String firstExamLetterGrade) {
		this.firstExamLetterGrade = firstExamLetterGrade;
	}

	public void setFirstExamTotalFailedSubject(Integer firstExamTotalFailedSubject) {
		this.firstExamTotalFailedSubject = firstExamTotalFailedSubject;
	}

	public void setFirstExamTotalAbsent(Integer firstExamTotalAbsent) {
		this.firstExamTotalAbsent = firstExamTotalAbsent;
	}

	public void setFirstExamTotalWorkingDay(Integer firstExamTotalWorkingDay) {
		this.firstExamTotalWorkingDay = firstExamTotalWorkingDay;
	}

	public void setFirstExamResultStatus(String firstExamResultStatus) {
		this.firstExamResultStatus = firstExamResultStatus;
	}

	public void setFirstExamMeritPosition(int firstExamMeritPosition) {
		this.firstExamMeritPosition = firstExamMeritPosition;
	}

	public void setSecondExam(String secondExam) {
		this.secondExam = secondExam;
	}

	public void setSecondExamObtainedMark(double secondExamObtainedMark) {
		this.secondExamObtainedMark = secondExamObtainedMark;
	}

	public void setSecondExamGpaWithFourth(double secondExamGpaWithFourth) {
		this.secondExamGpaWithFourth = secondExamGpaWithFourth;
	}

	public void setSecondExamGpaWithoutFourth(double secondExamGpaWithoutFourth) {
		this.secondExamGpaWithoutFourth = secondExamGpaWithoutFourth;
	}

	public void setSecondExamLetterGrade(String secondExamLetterGrade) {
		this.secondExamLetterGrade = secondExamLetterGrade;
	}

	public void setSecondExamTotalFailedSubject(Integer secondExamTotalFailedSubject) {
		this.secondExamTotalFailedSubject = secondExamTotalFailedSubject;
	}

	public void setSecondExamTotalAbsent(Integer secondExamTotalAbsent) {
		this.secondExamTotalAbsent = secondExamTotalAbsent;
	}

	public void setSecondExamTotalWorkingDay(Integer secondExamTotalWorkingDay) {
		this.secondExamTotalWorkingDay = secondExamTotalWorkingDay;
	}

	public void setSecondExamResultStatus(String secondExamResultStatus) {
		this.secondExamResultStatus = secondExamResultStatus;
	}

	public void setSecondExamMeritPosition(int secondExamMeritPosition) {
		this.secondExamMeritPosition = secondExamMeritPosition;
	}

	public void setSecondExamConfigId(Long secondExamConfigId) {
		this.secondExamConfigId = secondExamConfigId;
	}

	public void setThirdExam(String thirdExam) {
		this.thirdExam = thirdExam;
	}

	public void setThirdExamObtainedMark(double thirdExamObtainedMark) {
		this.thirdExamObtainedMark = thirdExamObtainedMark;
	}

	public void setThirdExamGpaWithFourth(double thirdExamGpaWithFourth) {
		this.thirdExamGpaWithFourth = thirdExamGpaWithFourth;
	}

	public void setThirdExamGpaWithoutFourth(double thirdExamGpaWithoutFourth) {
		this.thirdExamGpaWithoutFourth = thirdExamGpaWithoutFourth;
	}

	public void setThirdExamLetterGrade(String thirdExamLetterGrade) {
		this.thirdExamLetterGrade = thirdExamLetterGrade;
	}

	public void setThirdExamTotalFailedSubject(Integer thirdExamTotalFailedSubject) {
		this.thirdExamTotalFailedSubject = thirdExamTotalFailedSubject;
	}

	public void setThirdExamTotalAbsent(Integer thirdExamTotalAbsent) {
		this.thirdExamTotalAbsent = thirdExamTotalAbsent;
	}

	public void setThirdExamTotalWorkingDay(Integer thirdExamTotalWorkingDay) {
		this.thirdExamTotalWorkingDay = thirdExamTotalWorkingDay;
	}

	public void setThirdExamResultStatus(String thirdExamResultStatus) {
		this.thirdExamResultStatus = thirdExamResultStatus;
	}

	public void setThirdExamMeritPosition(int thirdExamMeritPosition) {
		this.thirdExamMeritPosition = thirdExamMeritPosition;
	}

	public void setFourthExam(String fourthExam) {
		this.fourthExam = fourthExam;
	}

	public void setFourthExamObtainedMark(double fourthExamObtainedMark) {
		this.fourthExamObtainedMark = fourthExamObtainedMark;
	}

	public void setFourthExamGpaWithFourth(double fourthExamGpaWithFourth) {
		this.fourthExamGpaWithFourth = fourthExamGpaWithFourth;
	}

	public void setFourthExamGpaWithoutFourth(double fourthExamGpaWithoutFourth) {
		this.fourthExamGpaWithoutFourth = fourthExamGpaWithoutFourth;
	}

	public void setFourthExamLetterGrade(String fourthExamLetterGrade) {
		this.fourthExamLetterGrade = fourthExamLetterGrade;
	}

	public void setFourthExamTotalFailedSubject(Integer fourthExamTotalFailedSubject) {
		this.fourthExamTotalFailedSubject = fourthExamTotalFailedSubject;
	}

	public void setFourthExamTotalAbsent(Integer fourthExamTotalAbsent) {
		this.fourthExamTotalAbsent = fourthExamTotalAbsent;
	}

	public void setFourthExamTotalWorkingDay(Integer fourthExamTotalWorkingDay) {
		this.fourthExamTotalWorkingDay = fourthExamTotalWorkingDay;
	}

	public void setFourthExamResultStatus(String fourthExamResultStatus) {
		this.fourthExamResultStatus = fourthExamResultStatus;
	}

	public void setFourthExamMeritPosition(int fourthExamMeritPosition) {
		this.fourthExamMeritPosition = fourthExamMeritPosition;
	}

	public void setGfExam(String gfExam) {
		this.gfExam = gfExam;
	}

	public void setGfExamObtainedMark(double gfExamObtainedMark) {
		this.gfExamObtainedMark = gfExamObtainedMark;
	}

	public void setGfExamGpaWithFourth(double gfExamGpaWithFourth) {
		this.gfExamGpaWithFourth = gfExamGpaWithFourth;
	}

	public void setGfExamGpaWithoutFourth(double gfExamGpaWithoutFourth) {
		this.gfExamGpaWithoutFourth = gfExamGpaWithoutFourth;
	}

	public void setGfExamLetterGrade(String gfExamLetterGrade) {
		this.gfExamLetterGrade = gfExamLetterGrade;
	}

	public void setGfExamLetterGradeForBarcode(String gfExamLetterGradeForBarcode) {
		this.gfExamLetterGradeForBarcode = gfExamLetterGradeForBarcode;
	}

	public void setGfExamTotalFailedSubject(Integer gfExamTotalFailedSubject) {
		this.gfExamTotalFailedSubject = gfExamTotalFailedSubject;
	}

	public void setGfExamTotalAbsent(Integer gfExamTotalAbsent) {
		this.gfExamTotalAbsent = gfExamTotalAbsent;
	}

	public void setGfExamTotalWorkingDay(Integer gfExamTotalWorkingDay) {
		this.gfExamTotalWorkingDay = gfExamTotalWorkingDay;
	}

	public void setGfExamResultStatus(String gfExamResultStatus) {
		this.gfExamResultStatus = gfExamResultStatus;
	}

	public void setGfExamMeritPosition(int gfExamMeritPosition) {
		this.gfExamMeritPosition = gfExamMeritPosition;
	}

	public void setGfExamConfigId(Long gfExamConfigId) {
		this.gfExamConfigId = gfExamConfigId;
	}

	public void setChoosenMeritPosition(int choosenMeritPosition) {
		this.choosenMeritPosition = choosenMeritPosition;
	}

	public void setClassPosition(int classPosition) {
		this.classPosition = classPosition;
	}

	public void setShiftPosition(int shiftPosition) {
		this.shiftPosition = shiftPosition;
	}

	public void setSectionPosition(int sectionPosition) {
		this.sectionPosition = sectionPosition;
	}

	public void setGroupPosition(int groupPosition) {
		this.groupPosition = groupPosition;
	}

	public void setNumOfFailedSbjs(int numOfFailedSbjs) {
		this.numOfFailedSbjs = numOfFailedSbjs;
	}

	public void setTotalWorkingDays(int totalWorkingDays) {
		this.totalWorkingDays = totalWorkingDays;
	}

	public void setTotalPresentDays(int totalPresentDays) {
		this.totalPresentDays = totalPresentDays;
	}

	public void setTotalAbsentDays(int totalAbsentDays) {
		this.totalAbsentDays = totalAbsentDays;
	}

	public void setGfExamRemarks(String gfExamRemarks) {
		this.gfExamRemarks = gfExamRemarks;
	}

	public void setFirstSbj1MergeGradePoint(double firstSbj1MergeGradePoint) {
		this.firstSbj1MergeGradePoint = firstSbj1MergeGradePoint;
	}

	public void setFirstSbj1MergeGradeName(String firstSbj1MergeGradeName) {
		this.firstSbj1MergeGradeName = firstSbj1MergeGradeName;
	}

	public void setFirstSbj2MergeGradePoint(double firstSbj2MergeGradePoint) {
		this.firstSbj2MergeGradePoint = firstSbj2MergeGradePoint;
	}

	public void setFirstSbj2MergeGradeName(String firstSbj2MergeGradeName) {
		this.firstSbj2MergeGradeName = firstSbj2MergeGradeName;
	}

	public void setFirstSbj3MergeGradePoint(double firstSbj3MergeGradePoint) {
		this.firstSbj3MergeGradePoint = firstSbj3MergeGradePoint;
	}

	public void setFirstSbj3MergeGradeName(String firstSbj3MergeGradeName) {
		this.firstSbj3MergeGradeName = firstSbj3MergeGradeName;
	}

	public void setFirstSbj4MergeGradePoint(double firstSbj4MergeGradePoint) {
		this.firstSbj4MergeGradePoint = firstSbj4MergeGradePoint;
	}

	public void setFirstSbj4MergeGradeName(String firstSbj4MergeGradeName) {
		this.firstSbj4MergeGradeName = firstSbj4MergeGradeName;
	}

	public void setFirstSbj5MergeGradePoint(double firstSbj5MergeGradePoint) {
		this.firstSbj5MergeGradePoint = firstSbj5MergeGradePoint;
	}

	public void setFirstSbj5MergeGradeName(String firstSbj5MergeGradeName) {
		this.firstSbj5MergeGradeName = firstSbj5MergeGradeName;
	}

	public void setFirstSbj6MergeGradePoint(double firstSbj6MergeGradePoint) {
		this.firstSbj6MergeGradePoint = firstSbj6MergeGradePoint;
	}

	public void setFirstSbj6MergeGradeName(String firstSbj6MergeGradeName) {
		this.firstSbj6MergeGradeName = firstSbj6MergeGradeName;
	}

	public void setFirstSbj7MergeGradePoint(double firstSbj7MergeGradePoint) {
		this.firstSbj7MergeGradePoint = firstSbj7MergeGradePoint;
	}

	public void setFirstSbj7MergeGradeName(String firstSbj7MergeGradeName) {
		this.firstSbj7MergeGradeName = firstSbj7MergeGradeName;
	}

	public void setSecondSbj1MergeGradePoint(double secondSbj1MergeGradePoint) {
		this.secondSbj1MergeGradePoint = secondSbj1MergeGradePoint;
	}

	public void setSecondSbj1MergeGradeName(String secondSbj1MergeGradeName) {
		this.secondSbj1MergeGradeName = secondSbj1MergeGradeName;
	}

	public void setSecondSbj2MergeGradePoint(double secondSbj2MergeGradePoint) {
		this.secondSbj2MergeGradePoint = secondSbj2MergeGradePoint;
	}

	public void setSecondSbj2MergeGradeName(String secondSbj2MergeGradeName) {
		this.secondSbj2MergeGradeName = secondSbj2MergeGradeName;
	}

	public void setSecondSbj3MergeGradePoint(double secondSbj3MergeGradePoint) {
		this.secondSbj3MergeGradePoint = secondSbj3MergeGradePoint;
	}

	public void setSecondSbj3MergeGradeName(String secondSbj3MergeGradeName) {
		this.secondSbj3MergeGradeName = secondSbj3MergeGradeName;
	}

	public void setSecondSbj4MergeGradePoint(double secondSbj4MergeGradePoint) {
		this.secondSbj4MergeGradePoint = secondSbj4MergeGradePoint;
	}

	public void setSecondSbj4MergeGradeName(String secondSbj4MergeGradeName) {
		this.secondSbj4MergeGradeName = secondSbj4MergeGradeName;
	}

	public void setSecondSbj5MergeGradePoint(double secondSbj5MergeGradePoint) {
		this.secondSbj5MergeGradePoint = secondSbj5MergeGradePoint;
	}

	public void setSecondSbj5MergeGradeName(String secondSbj5MergeGradeName) {
		this.secondSbj5MergeGradeName = secondSbj5MergeGradeName;
	}

	public void setSecondSbj6MergeGradePoint(double secondSbj6MergeGradePoint) {
		this.secondSbj6MergeGradePoint = secondSbj6MergeGradePoint;
	}

	public void setSecondSbj6MergeGradeName(String secondSbj6MergeGradeName) {
		this.secondSbj6MergeGradeName = secondSbj6MergeGradeName;
	}

	public void setSecondSbj7MergeGradePoint(double secondSbj7MergeGradePoint) {
		this.secondSbj7MergeGradePoint = secondSbj7MergeGradePoint;
	}

	public void setSecondSbj7MergeGradeName(String secondSbj7MergeGradeName) {
		this.secondSbj7MergeGradeName = secondSbj7MergeGradeName;
	}

	public void setThirdSbj1MergeGradePoint(double thirdSbj1MergeGradePoint) {
		this.thirdSbj1MergeGradePoint = thirdSbj1MergeGradePoint;
	}

	public void setThirdSbj1MergeGradeName(String thirdSbj1MergeGradeName) {
		this.thirdSbj1MergeGradeName = thirdSbj1MergeGradeName;
	}

	public void setThirdSbj2MergeGradePoint(double thirdSbj2MergeGradePoint) {
		this.thirdSbj2MergeGradePoint = thirdSbj2MergeGradePoint;
	}

	public void setThirdSbj2MergeGradeName(String thirdSbj2MergeGradeName) {
		this.thirdSbj2MergeGradeName = thirdSbj2MergeGradeName;
	}

	public void setThirdSbj3MergeGradePoint(double thirdSbj3MergeGradePoint) {
		this.thirdSbj3MergeGradePoint = thirdSbj3MergeGradePoint;
	}

	public void setThirdSbj3MergeGradeName(String thirdSbj3MergeGradeName) {
		this.thirdSbj3MergeGradeName = thirdSbj3MergeGradeName;
	}

	public void setThirdSbj4MergeGradePoint(double thirdSbj4MergeGradePoint) {
		this.thirdSbj4MergeGradePoint = thirdSbj4MergeGradePoint;
	}

	public void setThirdSbj4MergeGradeName(String thirdSbj4MergeGradeName) {
		this.thirdSbj4MergeGradeName = thirdSbj4MergeGradeName;
	}

	public void setThirdSbj5MergeGradePoint(double thirdSbj5MergeGradePoint) {
		this.thirdSbj5MergeGradePoint = thirdSbj5MergeGradePoint;
	}

	public void setThirdSbj5MergeGradeName(String thirdSbj5MergeGradeName) {
		this.thirdSbj5MergeGradeName = thirdSbj5MergeGradeName;
	}

	public void setThirdSbj6MergeGradePoint(double thirdSbj6MergeGradePoint) {
		this.thirdSbj6MergeGradePoint = thirdSbj6MergeGradePoint;
	}

	public void setThirdSbj6MergeGradeName(String thirdSbj6MergeGradeName) {
		this.thirdSbj6MergeGradeName = thirdSbj6MergeGradeName;
	}

	public void setThirdSbj7MergeGradePoint(double thirdSbj7MergeGradePoint) {
		this.thirdSbj7MergeGradePoint = thirdSbj7MergeGradePoint;
	}

	public void setThirdSbj7MergeGradeName(String thirdSbj7MergeGradeName) {
		this.thirdSbj7MergeGradeName = thirdSbj7MergeGradeName;
	}

	public void setFourthSbj1MergeGradePoint(double fourthSbj1MergeGradePoint) {
		this.fourthSbj1MergeGradePoint = fourthSbj1MergeGradePoint;
	}

	public void setFourthSbj1MergeGradeName(String fourthSbj1MergeGradeName) {
		this.fourthSbj1MergeGradeName = fourthSbj1MergeGradeName;
	}

	public void setFourthSbj2MergeGradePoint(double fourthSbj2MergeGradePoint) {
		this.fourthSbj2MergeGradePoint = fourthSbj2MergeGradePoint;
	}

	public void setFourthSbj2MergeGradeName(String fourthSbj2MergeGradeName) {
		this.fourthSbj2MergeGradeName = fourthSbj2MergeGradeName;
	}

	public void setFourthSbj3MergeGradePoint(double fourthSbj3MergeGradePoint) {
		this.fourthSbj3MergeGradePoint = fourthSbj3MergeGradePoint;
	}

	public void setFourthSbj3MergeGradeName(String fourthSbj3MergeGradeName) {
		this.fourthSbj3MergeGradeName = fourthSbj3MergeGradeName;
	}

	public void setFourthSbj4MergeGradePoint(double fourthSbj4MergeGradePoint) {
		this.fourthSbj4MergeGradePoint = fourthSbj4MergeGradePoint;
	}

	public void setFourthSbj4MergeGradeName(String fourthSbj4MergeGradeName) {
		this.fourthSbj4MergeGradeName = fourthSbj4MergeGradeName;
	}

	public void setFourthSbj5MergeGradePoint(double fourthSbj5MergeGradePoint) {
		this.fourthSbj5MergeGradePoint = fourthSbj5MergeGradePoint;
	}

	public void setFourthSbj5MergeGradeName(String fourthSbj5MergeGradeName) {
		this.fourthSbj5MergeGradeName = fourthSbj5MergeGradeName;
	}

	public void setFourthSbj6MergeGradePoint(double fourthSbj6MergeGradePoint) {
		this.fourthSbj6MergeGradePoint = fourthSbj6MergeGradePoint;
	}

	public void setFourthSbj6MergeGradeName(String fourthSbj6MergeGradeName) {
		this.fourthSbj6MergeGradeName = fourthSbj6MergeGradeName;
	}

	public void setFourthSbj7MergeGradePoint(double fourthSbj7MergeGradePoint) {
		this.fourthSbj7MergeGradePoint = fourthSbj7MergeGradePoint;
	}

	public void setFourthSbj7MergeGradeName(String fourthSbj7MergeGradeName) {
		this.fourthSbj7MergeGradeName = fourthSbj7MergeGradeName;
	}

	public void setGfSbj1MergeGradePoint(double gfSbj1MergeGradePoint) {
		this.gfSbj1MergeGradePoint = gfSbj1MergeGradePoint;
	}

	public void setGfSbj1MergeGradeName(String gfSbj1MergeGradeName) {
		this.gfSbj1MergeGradeName = gfSbj1MergeGradeName;
	}

	public void setGfSbj2MergeGradePoint(double gfSbj2MergeGradePoint) {
		this.gfSbj2MergeGradePoint = gfSbj2MergeGradePoint;
	}

	public void setGfSbj2MergeGradeName(String gfSbj2MergeGradeName) {
		this.gfSbj2MergeGradeName = gfSbj2MergeGradeName;
	}

	public void setGfSbj3MergeGradePoint(double gfSbj3MergeGradePoint) {
		this.gfSbj3MergeGradePoint = gfSbj3MergeGradePoint;
	}

	public void setGfSbj3MergeGradeName(String gfSbj3MergeGradeName) {
		this.gfSbj3MergeGradeName = gfSbj3MergeGradeName;
	}

	public void setGfSbj4MergeGradePoint(double gfSbj4MergeGradePoint) {
		this.gfSbj4MergeGradePoint = gfSbj4MergeGradePoint;
	}

	public void setGfSbj4MergeGradeName(String gfSbj4MergeGradeName) {
		this.gfSbj4MergeGradeName = gfSbj4MergeGradeName;
	}

	public void setGfSbj5MergeGradePoint(double gfSbj5MergeGradePoint) {
		this.gfSbj5MergeGradePoint = gfSbj5MergeGradePoint;
	}

	public void setGfSbj5MergeGradeName(String gfSbj5MergeGradeName) {
		this.gfSbj5MergeGradeName = gfSbj5MergeGradeName;
	}

	public void setGfSbj6MergeGradePoint(double gfSbj6MergeGradePoint) {
		this.gfSbj6MergeGradePoint = gfSbj6MergeGradePoint;
	}

	public void setGfSbj6MergeGradeName(String gfSbj6MergeGradeName) {
		this.gfSbj6MergeGradeName = gfSbj6MergeGradeName;
	}

	public void setGfSbj7MergeGradePoint(double gfSbj7MergeGradePoint) {
		this.gfSbj7MergeGradePoint = gfSbj7MergeGradePoint;
	}

	public void setGfSbj7MergeGradeName(String gfSbj7MergeGradeName) {
		this.gfSbj7MergeGradeName = gfSbj7MergeGradeName;
	}

}
