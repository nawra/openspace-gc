package com.openspace.education.jasper.idcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;

@Controller
@RequestMapping(value = "/jasper")
public class IdCardController {
	
	@Autowired
	private IdCardService idCardService;
	
	@RequestMapping(value = "/student/id-card/download", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> downloadStudentIdCard(@RequestParam int startRoll,@RequestParam int endRoll,@RequestParam Long classConfigId) {
		BaseResponse baseResponse = idCardService.downloadStudentIdCard(startRoll, endRoll,classConfigId);
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/student/id-card/backpart/download", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> downloadStudentIdCardBackPart(@RequestParam int startRoll,@RequestParam int endRoll,@RequestParam Long classConfigId) {
		BaseResponse baseResponse = idCardService.downloadStudentIdCardBackPart(startRoll, endRoll,classConfigId);
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}
	
//	
//	@RequestMapping(value = "/student/id-card/back-part/download", method = RequestMethod.GET)
//	public ResponseEntity<BaseResponse> downloadIdCardBackPart(@RequestParam int startRoll,@RequestParam int endRoll,@RequestParam Long classConfigId) {
//		BaseResponse baseResponse = idCardService.downloadStudentIdCardBackPart(startRoll, endRoll,classConfigId);
//		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
//	}
	
	
	@RequestMapping(value = "/staff/id-card/download", method = RequestMethod.GET)
	public ResponseEntity<BaseResponse> downloadStaffIdCard() {
		BaseResponse baseResponse = idCardService.downloadStaffIdCard();
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}

}
