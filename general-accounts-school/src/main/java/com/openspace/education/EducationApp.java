package com.openspace.education;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducationApp {

	public static void main(String[] args) {
		SpringApplication.run(EducationApp.class, args);
	}
}
