package com.openspace.education.student.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.student.model.request.StudentIdCardTemplateRequest;
import com.openspace.education.student.service.StudentIdCardTemplateService;

@Controller
@RequestMapping(value = "/student/id-card/template")
public class StudentIdCardTemplateController {
	
	@Autowired
	private StudentIdCardTemplateService studentIdCardTemplateService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveStudentIdTemplate(@RequestBody StudentIdCardTemplateRequest request){
		BaseResponse baseResponse = studentIdCardTemplateService.saveStudentIdTemplate(request);
		return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/view")
	public ResponseEntity<ItemResponse> singleIdCardTemplateView() {
		ItemResponse itemResponse = studentIdCardTemplateService.getSingleStudentIdCardTEmplate();
		return new ResponseEntity<>(itemResponse, HttpStatus.OK);
	}

}
