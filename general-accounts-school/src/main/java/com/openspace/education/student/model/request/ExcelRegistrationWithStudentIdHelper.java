package com.openspace.education.student.model.request;

public class ExcelRegistrationWithStudentIdHelper {
	
	private String customStudentId;
	
	private String studentName;

	private String studentGender;

	private String studentReligion;

	private String guardianMobile;

    private String fatherName;

    private String motherName;
    
    private Integer studentRoll;
    
    private String registrationId;

	public String getCustomStudentId() {
		return customStudentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

    
    
}
