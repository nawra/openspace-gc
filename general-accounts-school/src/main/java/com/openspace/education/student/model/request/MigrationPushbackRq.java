package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class MigrationPushbackRq {

	@NotNull
	@Size(min = 1)
    private List<Long>  currentIndentificationIds;
    
	@NotNull
    private Integer acYearToPushback;
    
    

	public List<Long> getCurrentIndentificationIds() {
		return currentIndentificationIds;
	}

	public void setCurrentIndentificationIds(List<Long> currentIndentificationIds) {
		this.currentIndentificationIds = currentIndentificationIds;
	}

	public Integer getAcYearToPushback() {
		return acYearToPushback;
	}

	public void setAcYearToPushback(Integer acYearToPushback) {
		this.acYearToPushback = acYearToPushback;
	}


    
    
    
    
    
    
    
}
