package com.openspace.education.student.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.CommonInfoUtils;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingGroupRepository;
import com.openspace.education.initialsetup.repository.CoreSettingStudentCategoryRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.location.model.entity.Thana;
import com.openspace.education.location.repository.ThanaRepository;

import com.openspace.education.student.model.entity.StudentBasic;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.model.request.ExcelRegistrationWithStudentId;
import com.openspace.education.student.model.request.ExcelRegistrationWithStudentIdHelper;
import com.openspace.education.student.model.request.MultipleStudentRegistrationRequest;
import com.openspace.education.student.model.request.MultipleStudentRegistrationRequestHelper;
import com.openspace.education.student.model.request.RegistrationDeleteRequest;
import com.openspace.education.student.model.request.StudentAddressUpdateRequest;
import com.openspace.education.student.model.request.StudentBasicViewUpdateRequest;
import com.openspace.education.student.model.request.StudentCategoryInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentEnableDisableRequest;
import com.openspace.education.student.model.request.StudentGroupInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentIdUpdateRequest;
import com.openspace.education.student.model.request.StudentSectionInfoUpdateRequest;
import com.openspace.education.student.model.response.StudentGeneralViewResponse;
import com.openspace.education.student.repository.StudentBasicRepository;
import com.openspace.education.student.repository.StudentIdentificationRepository;


@Service
public class StudentRegistrationService {
	
	public Logger logger = LoggerFactory.getLogger(StudentRegistrationService.class);
	
	@Autowired
	private StudentBasicRepository studentBasicRepository;
	
	@Autowired
	private CoreSettingGroupRepository coreSettingGroupRepository;

	@Autowired
	private CoreSettingStudentCategoryRepository coreSettingStudentCategoryRepository;
	
	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;
	
	@Autowired
	private StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	private ThanaRepository thanaRepository;
	
	@Autowired
	private ImageStorageService imageStorageService;
	
	
	@Autowired
	private CommonInfoUtils commonInfoUtils;
	
	
    
	@Transactional
	public BaseResponse doStudentRegistration(MultipleStudentRegistrationRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		String userName=UserInfoUtils.getLoggedInUserName();

		Long customStudentId = studentBasicRepository.findMaxCustomStudentId(institute.getInstituteId()) + 1;
		
		

		ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(request.getClassConfigurationId(), institute);
		
		if(classConfiguration==null) {
			baseResponse.setMessage("No Class Configuration Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}

		CoreSettingGroup coreSettingGroup = coreSettingGroupRepository.findByIdAndInstitute(request.getGroupId(), institute);
		
		if(coreSettingGroup==null) {
			baseResponse.setMessage("No Group Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		
		CoreSettingStudentCategory coreSettingStudentCategory = coreSettingStudentCategoryRepository.findByIdAndInstitute(request.getCategoryId(), institute);
	       
		if(coreSettingStudentCategory==null) {
			baseResponse.setMessage("No Category Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		
	      
		   List<StudentBasic> studentBasicList = new LinkedList<>();
	       
	       classConfiguration.setId(request.getClassConfigurationId());
	       
	       coreSettingGroup.setId(request.getGroupId());

	        
	       
	       for (MultipleStudentRegistrationRequestHelper multipleStudentInfoRequest : request.getMultipleStudentList()) {
	        	
	        	Set<StudentIdentification> studentIdentifications = new LinkedHashSet<>();
	        	
	            StudentBasic studentBasic =new StudentBasic();
	            
	            copyMultipleStudentInfoRequestToStudentBasic(multipleStudentInfoRequest, studentBasic, customStudentId, institute);
	           
	            
	            StudentIdentification studentIdentification = new StudentIdentification();
	            
	            studentIdentification.setAcademicYear(request.getAcademicYear());
	            
	            studentIdentification.setStudentRoll(multipleStudentInfoRequest.getStudentRoll());
	            
	            studentIdentification.setInstitute(institute);
	            
	            studentIdentification.setClassConfigurationInfo(classConfiguration);
	            
	            studentIdentification.setGroupInfo(coreSettingGroup);
	            
	            studentIdentification.setStudentCategoryInfo(coreSettingStudentCategory);
	            
	            studentIdentification.setStudentBasic(studentBasic);
	            
	            studentIdentifications.add(studentIdentification);
	            
	            studentBasic.setStudentIdentifications(studentIdentifications);

	            studentBasicList.add(studentBasic); 
	            
	            customStudentId++;
	        }

	     
	       studentBasicList=studentBasicRepository.save(studentBasicList);
	     
	        

	     	baseResponse.setMessage("Student Registration Successfully Done.");
			baseResponse.setMessageType(1);
	        return baseResponse;
	    }
	
	
	
	  
	
	
	
	
	   @Transactional
	   public BaseResponse doExcelRegistrationWithStudentId(ExcelRegistrationWithStudentId request) {
		
			BaseResponse baseResponse=new BaseResponse();
	
			Institute institute = UserInfoUtils.getLoggedInInstitute();
			
			String userName=UserInfoUtils.getLoggedInUserName();
	
			ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(request.getClassConfigurationId(), institute);
			
			if(classConfiguration==null) {
				baseResponse.setMessage("No Class Configuration Found.");	
				baseResponse.setMessageType(0);
				return baseResponse;
			}
	
			CoreSettingGroup coreSettingGroup = coreSettingGroupRepository.findByIdAndInstitute(request.getGroupId(), institute);
			
			if(coreSettingGroup==null) {
				baseResponse.setMessage("No Group Found.");	
				baseResponse.setMessageType(0);
				return baseResponse;
			}
			
			
			CoreSettingStudentCategory coreSettingStudentCategory = coreSettingStudentCategoryRepository.findByIdAndInstitute(request.getCategoryId(), institute);
		       
			if(coreSettingStudentCategory==null) {
				baseResponse.setMessage("No Category Found.");	
				baseResponse.setMessageType(0);
				return baseResponse;
		    }
		
	      
		   List<StudentBasic> studentBasicList = new LinkedList<>();
	       

	        for (ExcelRegistrationWithStudentIdHelper excelRequest : request.getMultipleStudentList()) {
	        	
	        	Set<StudentIdentification> studentIdentifications = new LinkedHashSet<>();
	        	
	            StudentBasic studentBasic =new StudentBasic();
	            
	            copyExcelStudentInfoRequestToStudentBasic(excelRequest, studentBasic, excelRequest.getCustomStudentId(), institute);
	           
	            StudentIdentification studentIdentification = new StudentIdentification();
	            
	            studentIdentification.setAcademicYear(request.getAcademicYear());
	            
	            studentIdentification.setStudentRoll(excelRequest.getStudentRoll());
	            
	            studentIdentification.setInstitute(institute);
	            
	            studentIdentification.setClassConfigurationInfo(classConfiguration);
	            
	            studentIdentification.setGroupInfo(coreSettingGroup);
	            
	            studentIdentification.setStudentCategoryInfo(coreSettingStudentCategory);
	            
	            studentIdentification.setStudentBasic(studentBasic);
	            
	            studentIdentifications.add(studentIdentification);
	            
	            studentBasic.setStudentIdentifications(studentIdentifications);

	            studentBasicList.add(studentBasic); 
	            
	            
	        }

	     
	        studentBasicRepository.save(studentBasicList);

	        
	     
	     	baseResponse.setMessage("Student Registration Successfully Done.");
			baseResponse.setMessageType(1);
	        return baseResponse;
	    }
	   
	   
	   
	 
	   public BaseResponse deleteStudentRegistration(RegistrationDeleteRequest request) {
		
			BaseResponse baseResponse=new BaseResponse();
	
			Institute institute = UserInfoUtils.getLoggedInInstitute();
			
			List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, request.getIdentificationIds());
	     
			int deleteCount=0;
			
			for(StudentIdentification si:studentIdentifications) {
				
				StudentBasic studentBasic=si.getStudentBasic();
				
			try {	
				studentIdentificationRepository.delete(si);
				studentBasicRepository.delete(studentBasic);
				deleteCount++;
			}catch(Exception e) {
				
			}
				
				
			}
			
			if(deleteCount>0) {
				baseResponse.setMessage(""+deleteCount+" students are deleted.");
				baseResponse.setMessageType(1);
			}else {
				
				baseResponse.setMessage("No student is deleted.");
				baseResponse.setMessageType(0);
			}
			
	     	
			
	        return baseResponse;
	    }
	
	
	 
	   
	   public void copyMultipleStudentInfoRequestToStudentBasic(MultipleStudentRegistrationRequestHelper helperInfo,StudentBasic studentBasic,Long customStudentId,Institute institute) {

		studentBasic.setStudentName(helperInfo.getStudentName());
		studentBasic.setCustomStudentId(String.valueOf(customStudentId));
		studentBasic.setFatherName(helperInfo.getFatherName());
		studentBasic.setMotherName(helperInfo.getMotherName());
		studentBasic.setGuardianMobile(helperInfo.getGuardianMobile());
		studentBasic.setStudentGender(helperInfo.getStudentGender());
		studentBasic.setStudentReligion(helperInfo.getStudentReligion());
		studentBasic.setInstitute(institute);
		studentBasic.setRegistrationDate(new Date());
		studentBasic.setRegistrationNo(helperInfo.getRegistrationId());
        
    }
	
	
	public void copyExcelStudentInfoRequestToStudentBasic(ExcelRegistrationWithStudentIdHelper helperInfo,StudentBasic studentBasic,String customStudentId,Institute institute) {

		studentBasic.setStudentName(helperInfo.getStudentName());
		studentBasic.setCustomStudentId(customStudentId);
		studentBasic.setFatherName(helperInfo.getFatherName());
		studentBasic.setMotherName(helperInfo.getMotherName());
		studentBasic.setGuardianMobile(helperInfo.getGuardianMobile());
		studentBasic.setStudentGender(helperInfo.getStudentGender());
		studentBasic.setStudentReligion(helperInfo.getStudentReligion());
		studentBasic.setInstitute(institute);
		studentBasic.setRegistrationDate(new Date());
		studentBasic.setRegistrationNo(helperInfo.getRegistrationId());
        
    }
	
	
	public ItemResponse sectionWiseStudentRegistrationList(Long classConfigurationId,Integer academicYear) {
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, classConfigurationId, true);
		
		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}
	
	

	public ItemResponse sectionWiseStudentEnableDisableList(Long classConfigurationId, Integer academicYear, Institute institute) {
		
		ItemResponse itemResponse=new ItemResponse();
		
		List<StudentIdentification> studentIdentifications = studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdOrderByStudentRollAsc(institute, academicYear, classConfigurationId);
		
		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si : studentIdentifications) {
			
			StudentGeneralViewResponse view = new StudentGeneralViewResponse();
			
			copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}
	
	
public ItemResponse sectionAndGroupWiseStudentRegistrationList(Long classConfigurationId,Long groupId,Integer academicYear) {
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, classConfigurationId,groupId, true);
		
		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}


public ItemResponse sectionAndGroupWiseStudentListForMigration(Long classConfigurationId,Long groupId,Integer academicYear) {
	
	ItemResponse itemResponse=new ItemResponse();
	Institute institute = UserInfoUtils.getLoggedInInstitute();
	
	List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndMigrationStatusAndStudentStatusOrderByStudentRollAsc(institute, academicYear, classConfigurationId,groupId,true, true);
	
	List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
	
	for(StudentIdentification si:studentIdentifications) {
		
		StudentGeneralViewResponse view=new StudentGeneralViewResponse();
		
		copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
		
		viewResponses.add(view);
	
	}
	
	itemResponse.setItem(viewResponses);
	itemResponse.setMessage("OK");
	itemResponse.setMessageType(1);
	return itemResponse;
	
	
}
	
	
	
	public void copyStudentIdentificationIntoStudentGeneralViewResponse(StudentIdentification si,StudentGeneralViewResponse studentGeneralView) {
	
		studentGeneralView.setBloodGroup(si.getStudentBasic().getBloodGroup());
		studentGeneralView.setCustomStudentId(si.getStudentBasic().getCustomStudentId());
		studentGeneralView.setDisableDate(si.getDisableDate());
		studentGeneralView.setDisableReason(si.getDisableReason());
		studentGeneralView.setFatherName(si.getStudentBasic().getFatherName());
		studentGeneralView.setGuardianMobile(si.getStudentBasic().getGuardianMobile());
		studentGeneralView.setIdentificationId(si.getIdentificationId());
		studentGeneralView.setMailingAddress(si.getStudentBasic().getMailingAddress());
		studentGeneralView.setMigrationStatus(si.isMigrationStatus());
		studentGeneralView.setMotherName(si.getStudentBasic().getMotherName());
		studentGeneralView.setRegistrationNo(si.getStudentBasic().getRegistrationNo());
		studentGeneralView.setStudentDOB(si.getStudentBasic().getStudentDOB());
		studentGeneralView.setStudentGender(si.getStudentBasic().getStudentGender());
		studentGeneralView.setStudentId(si.getStudentBasic().getStudentId());
		studentGeneralView.setStudentName(si.getStudentBasic().getStudentName());
		studentGeneralView.setStudentReligion(si.getStudentBasic().getStudentReligion());
		studentGeneralView.setStudentRoll(si.getStudentRoll());
		studentGeneralView.setStudentStatus(si.isStudentStatus());
		studentGeneralView.setStudentSession(si.getStudentBasic().getAcademicSession());	
		studentGeneralView.setBirthCertificateNo(si.getStudentBasic().getBirthCertificateNo());
		studentGeneralView.setFatherNid(si.getStudentBasic().getFatherNid());
		studentGeneralView.setMotherNid(si.getStudentBasic().getMotherNid());
		studentGeneralView.setAcademicYear(si.getAcademicYear());
		studentGeneralView.setStudentCategoryId(si.getStudentCategoryInfo().getId());
		studentGeneralView.setStudentCategoryName(si.getStudentCategoryInfo().getName());	
		studentGeneralView.setGroupId(si.getGroupInfo().getId()); 
		studentGeneralView.setGroupName(si.getGroupInfo().getName());
		
		
		if(si.isStudentStatus()==true) {
		studentGeneralView.setStatus("Enabled");	
		} else {
		studentGeneralView.setStatus("Disabled");
		}
		
		ClassConfiguration classConfiguration=si.getClassConfigurationInfo();
		
		studentGeneralView.setClassId(classConfiguration.getClassInfo().getId());
		studentGeneralView.setClassName(classConfiguration.getClassInfo().getName());
		
		studentGeneralView.setShiftId(classConfiguration.getShiftInfo().getId());
		studentGeneralView.setShiftName(classConfiguration.getShiftInfo().getName());
		
		studentGeneralView.setSectionId(classConfiguration.getSectionInfo().getId());
		studentGeneralView.setSectionName(classConfiguration.getSectionInfo().getName());
		
		studentGeneralView.setClassConfigId(classConfiguration.getId());
		String classConfigName=classConfiguration.getClassInfo().getName()+"-"+classConfiguration.getShiftInfo().getName()+"-"+classConfiguration.getSectionInfo().getName();
		studentGeneralView.setClassConfigName(classConfigName);
		studentGeneralView.setPresentPostOffice(si.getStudentBasic().getPresentPostOffice());
		studentGeneralView.setPresentVillege(si.getStudentBasic().getPresentVillege());
		
		studentGeneralView.setPermanentPostOffice(si.getStudentBasic().getPermanentPostOffice());
		studentGeneralView.setPermanentVillege(si.getStudentBasic().getPermanentVillege());
		
		if(si.getStudentBasic().getPresentThana()!=null) {
			studentGeneralView.setPresentThanaId(si.getStudentBasic().getPresentThana().getThanaId());
			studentGeneralView.setPresentThanaName(si.getStudentBasic().getPresentThana().getThanaName());
			studentGeneralView.setPresentDistrictId(si.getStudentBasic().getPresentThana().getDistrict().getDistrictId());
			studentGeneralView.setPresentDistrictName(si.getStudentBasic().getPresentThana().getDistrict().getDistrictName());
		}
		
		
		if(si.getStudentBasic().getPermanentThana()!=null) {
			studentGeneralView.setPermanentThanaId(si.getStudentBasic().getPermanentThana().getThanaId());
			studentGeneralView.setPermanentThanaName(si.getStudentBasic().getPermanentThana().getThanaName());
			studentGeneralView.setPermanentDistrictId(si.getStudentBasic().getPermanentThana().getDistrict().getDistrictId());
			studentGeneralView.setPermanentDistrictName(si.getStudentBasic().getPermanentThana().getDistrict().getDistrictName());
		}
		
		
		
		try {
			studentGeneralView.setImageName(new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STUDENT.name(), si.getStudentBasic().getImageName())));	
		}catch(Exception e) {
			studentGeneralView.setImageName("");
		}
	}
	
	
	
	@Transactional
	public BaseResponse updateStudentBasicInfo(List<StudentBasicViewUpdateRequest> requests) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Set<Long> identificationIds=new HashSet<>();
		
		for(StudentBasicViewUpdateRequest req:requests) {
			identificationIds.add(req.getIdentificationId());
		}
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);
		
//		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			for(StudentBasicViewUpdateRequest req:requests) {
				
					if(si.getIdentificationId().equals(req.getIdentificationId())) {
						
						si.setStudentRoll(req.getStudentRoll());
						
						StudentBasic studentBasic=si.getStudentBasic();
						
						studentBasic.setCustomStudentId(req.getCustomStudentId());
						studentBasic.setBloodGroup(req.getBloodGroup());
						studentBasic.setFatherName(req.getFatherName());
						studentBasic.setGuardianMobile(req.getGuardianMobile());
						studentBasic.setMotherName(req.getMotherName());
						studentBasic.setStudentDOB(req.getStudentDOB());
						studentBasic.setStudentGender(req.getStudentGender());
						studentBasic.setStudentName(req.getStudentName());
						studentBasic.setStudentReligion(req.getStudentReligion());
						studentBasic.setRegistrationNo(req.getRegistrationNo());
						studentBasic.setAcademicSession(req.getStudentSession());
						
						break;
						
					}
			}
				
		
		}
		
		baseResponse.setMessage("Student Information Successfully Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
		
		
	}
	
	
	
	@Transactional
	public BaseResponse updateStudentCustomIds(List<StudentIdUpdateRequest> requests) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<Long> studentIds=new ArrayList<>();
		
		for(StudentIdUpdateRequest req: requests) {
			
			studentIds.add(req.getStudentId());
			
		}
		
		List<StudentBasic> studentBasics = studentBasicRepository.findByInstituteAndStudentIdIn(institute, studentIds);
		
		for(StudentBasic si : studentBasics) {
			
			for(StudentIdUpdateRequest req : requests) {
				
					if(si.getStudentId().equals(req.getStudentId())) {
						
						si.setCustomStudentId(req.getCustomStudentId());
						
						break;
						
					}
			}
		
		 }
		
		baseResponse.setMessage("Student ID Successfully Updated.");
		
		baseResponse.setMessageType(1);
		
		return baseResponse;
		
		
	}
	
	
	@Transactional
	public BaseResponse updateStudentSectionInfo(StudentSectionInfoUpdateRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Set<Long> identificationIds=new HashSet<>();
		
		ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(request.getClassConfigId(), institute);
		
		if(classConfiguration==null) {
			baseResponse.setMessage("No Section Found.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		for(Long identificationId:request.getIdentificationIds()) {
			identificationIds.add(identificationId);
		}
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);
				
		for(StudentIdentification si:studentIdentifications) {
			
			for(Long identificationId:request.getIdentificationIds()) {
				
					if(si.getIdentificationId().equals(identificationId)){
						
						si.setClassConfigurationInfo(classConfiguration);
						
						break;
						
				 }
			 
			 }
				
		  }
		
		baseResponse.setMessage("Student Section Successfully Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	}
	
	
	@Transactional
	public BaseResponse updateStudentGroupInfo(StudentSectionInfoUpdateRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Set<Long> identificationIds=new HashSet<>();
		
		ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(request.getClassConfigId(), institute);
		
		if(classConfiguration==null) {
			baseResponse.setMessage("No Section Found.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		for(Long identificationId:request.getIdentificationIds()) {
			identificationIds.add(identificationId);
		}
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);
				
		for(StudentIdentification si:studentIdentifications) {
			
			for(Long identificationId:request.getIdentificationIds()) {
				
					if(si.getIdentificationId().equals(identificationId)){
						
						si.setClassConfigurationInfo(classConfiguration);
						
						break;
						
				 }
			 
			 }
				
		  }
		
		baseResponse.setMessage("Student Section Successfully Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	}
	
	
	@Transactional
	public BaseResponse updateStudentGroupInfo(StudentGroupInfoUpdateRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Set<Long> identificationIds=new HashSet<>();
		
		CoreSettingGroup group = coreSettingGroupRepository.findByIdAndInstitute(request.getGroupId(), institute);
		
		if(group==null) {
			baseResponse.setMessage("No Group Found.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		for(Long identificationId:request.getIdentificationIds()) {
			identificationIds.add(identificationId);
		}
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);
				
		for(StudentIdentification si:studentIdentifications) {
			
			for(Long identificationId:request.getIdentificationIds()) {
				
					if(si.getIdentificationId().equals(identificationId)){
						
						si.setGroupInfo(group);
						
						break;
						
				 }
			 
			 }
				
		  }
		
		baseResponse.setMessage("Student Group Successfully Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	}
	
	
	@Transactional
	public BaseResponse updateStudentCategoryInfo(StudentCategoryInfoUpdateRequest request) {
		
		BaseResponse baseResponse=new BaseResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		Set<Long> identificationIds=new HashSet<>();
		
		CoreSettingStudentCategory category = coreSettingStudentCategoryRepository.findByIdAndInstitute(request.getCategoryId(), institute);
		
		if(category==null) {
			baseResponse.setMessage("No Category Found.");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		for(Long identificationId:request.getIdentificationIds()) {
			identificationIds.add(identificationId);
		}
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);
				
		for(StudentIdentification si:studentIdentifications) {
			
			for(Long identificationId:request.getIdentificationIds()) {
				
					if(si.getIdentificationId().equals(identificationId)){
						
						si.setStudentCategoryInfo(category);
						
						break;
						
				 }
			 
			 }
				
		  }
		
		baseResponse.setMessage("Student Category Successfully Updated.");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	}
	
	
	
	public BaseResponse updateSingleStudentPhoto(Long studentId, MultipartFile file) {
		
		   
		   BaseResponse baseResponse = new BaseResponse();
		   Institute institute=UserInfoUtils.getLoggedInInstitute();
		   StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstitute(studentId, institute);
		   if(studentBasic==null) {
			   baseResponse.setMessage("No Student Found");
			   baseResponse.setMessageType(0);
			   return baseResponse;
		   }
		   
		   String imageName=institute.getInstituteId()+"_"+studentBasic.getCustomStudentId()+".jpg";
		   imageStorageService.uploadImgHeightWidth(FileFolder.STUDENT.name(), imageName, file, 200, 200);
		   studentBasic.setImageName(imageName);
		   studentBasicRepository.save(studentBasic);
			
		   baseResponse.setMessage("Student Image Successfully Updated.");
		   baseResponse.setMessageType(1);
		   return baseResponse;
		}
	
	
     public BaseResponse deletePhoto(Long studentId) {
		   
		   BaseResponse baseResponse = new BaseResponse();
		   Institute institute=UserInfoUtils.getLoggedInInstitute();
		   StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstitute(studentId, institute);
		   if(studentBasic==null) {
			   baseResponse.setMessage("No Student Found");
			   baseResponse.setMessageType(0);
			   return baseResponse;
		   }
		   
		   imageStorageService.deleteFile(FileFolder.STUDENT.name(), studentBasic.getImageName());

			
		   baseResponse.setMessage("Student Image Successfully Deleted.");
		   baseResponse.setMessageType(1);
		   return baseResponse;
		}
     
     

     @Transactional
 	public BaseResponse updateStudentAddressInfo(StudentAddressUpdateRequest request) {
    	 
    	 Institute institute = UserInfoUtils.getLoggedInInstitute();
    	 
    	 BaseResponse baseResponse = new BaseResponse();
    	 
    	 StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstitute(request.getStudentId(), institute);
    	 
    	 if(studentBasic == null) {
    		 baseResponse.setMessage("No Student Found."); 
    		 baseResponse.setMessageType(0);
    		 return baseResponse;
    	 }
    	 
    	 Thana thana = thanaRepository.findOne(request.getPresentThanaId());
    	  
    	 if(thana == null) {
    		 baseResponse.setMessage("No Thana Found."); 
    		 baseResponse.setMessageType(0);
    		 return baseResponse;
    	 }
    	 
    	 studentBasic.setPresentPostOffice(request.getPresentPostOffice());
    	 studentBasic.setMailingAddress(request.getMailingAddress());
    	 studentBasic.setPresentThana(thana);
    	 studentBasic.setPresentVillege(request.getPresentVillege());
    	 
    	 baseResponse.setMessage("Student Address Successfully Updated."); 
		 baseResponse.setMessageType(1);
    	 
    	 return baseResponse;
    	 
     }
     
     
     
     
    @Transactional
  	public BaseResponse updateStudentStatusInfo(StudentEnableDisableRequest request) {
    	 
    	BaseResponse baseResponse = new BaseResponse();
    	
    	Institute institute = UserInfoUtils.getLoggedInInstitute();
    	
    	Date date = new Date();
    	
 		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, request.getIdentificationIds());
 		
 		for(StudentIdentification si : studentIdentifications) {
 			
 			si.setStudentStatus(request.getStudentStatus());
 			si.setDisableReason(request.getDisableNote());
 			si.setDisableDate(date);
 		}
     
 		baseResponse.setMessage("Student Status Successfully Updated.");
 		baseResponse.setMessageType(1);
    	 
    	 return baseResponse;
     }
     
     

     
     
     
     //============================Student Update for University Student===========================================
     
     
 	


     
     //-------------------- University Student Registration End ---------------------------------- 

}
