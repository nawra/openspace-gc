package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentCategoryInfoUpdateRequest {

	
	@NotNull
	private Long categoryId;
	
	@NotNull
	@Size(min = 1)
	private List<Long> identificationIds;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public List<Long> getIdentificationIds() {
		return identificationIds;
	}

	public void setIdentificationIds(List<Long> identificationIds) {
		this.identificationIds = identificationIds;
	}
	
}
