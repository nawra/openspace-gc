package com.openspace.education.student.model.request;

import javax.validation.constraints.NotNull;

public class SectionGroupCategoryStudentViewRequest {
	
	@NotNull
	private Integer academicYear;
	
	@NotNull
	private Long classConfigId;
	
	@NotNull
	private Long groupId;
	
	@NotNull
	private Long categoryId;

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public Long getClassConfigId() {
		return classConfigId;
	}

	public void setClassConfigId(Long classConfigId) {
		this.classConfigId = classConfigId;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	
	
	

}
