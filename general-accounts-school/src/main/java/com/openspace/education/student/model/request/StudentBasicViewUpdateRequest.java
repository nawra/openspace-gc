package com.openspace.education.student.model.request;

import java.util.Date;

public class StudentBasicViewUpdateRequest {


	private String customStudentId;

	private String studentName;

	private String studentGender;

	private Date studentDOB;

	private String studentReligion;

	private String fatherName;

	private String motherName;

	private String guardianMobile;

	private String bloodGroup;

	private String mailingAddress;
	
	private Long identificationId;

	private Integer studentRoll;

	private String registrationNo;
	
	private String studentSession;

	public String getCustomStudentId() {
		return customStudentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public Date getStudentDOB() {
		return studentDOB;
	}

	public void setStudentDOB(Date studentDOB) {
		this.studentDOB = studentDOB;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public Long getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getStudentSession() {
		return studentSession;
	}

	public void setStudentSession(String studentSession) {
		this.studentSession = studentSession;
	}
	
	
	
	

	
}
