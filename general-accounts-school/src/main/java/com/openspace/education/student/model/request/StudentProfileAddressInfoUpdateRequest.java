package com.openspace.education.student.model.request;

public class StudentProfileAddressInfoUpdateRequest {

	private String mailingAddress;

	private String presentPostOffice;

	private String presentVillege;

	private Integer presentThanaId;

	private String permanentPostOffice;

	private String permanentVillege;

	private Integer permanentThanaId;

	private Long studentId;

	private Long instituteId;

	public String getMailingAddress() {
		return mailingAddress;
	}

	public String getPresentPostOffice() {
		return presentPostOffice;
	}

	public String getPresentVillege() {
		return presentVillege;
	}

	public Integer getPresentThanaId() {
		return presentThanaId;
	}

	public String getPermanentPostOffice() {
		return permanentPostOffice;
	}

	public String getPermanentVillege() {
		return permanentVillege;
	}

	public Integer getPermanentThanaId() {
		return permanentThanaId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public void setPresentPostOffice(String presentPostOffice) {
		this.presentPostOffice = presentPostOffice;
	}

	public void setPresentVillege(String presentVillege) {
		this.presentVillege = presentVillege;
	}

	public void setPresentThanaId(Integer presentThanaId) {
		this.presentThanaId = presentThanaId;
	}

	public void setPermanentPostOffice(String permanentPostOffice) {
		this.permanentPostOffice = permanentPostOffice;
	}

	public void setPermanentVillege(String permanentVillege) {
		this.permanentVillege = permanentVillege;
	}

	public void setPermanentThanaId(Integer permanentThanaId) {
		this.permanentThanaId = permanentThanaId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

}
