package com.openspace.education.student.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "student_profile_update_otp")
public class StudentProfileUpdateOtp implements Serializable {

	private static final long serialVersionUID = -8645365141843090850L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "custom_student_id")
	private String customStudentId;

	@Column(name = "token")
	private String token;

	@Column(name = "date_executed")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecuted;

	@Column(name = "otp_generate_date")
	@Temporal(TemporalType.DATE)
	private Date otpGenerateDate;

	@Column(name = "institute_id")
	private Long instituteId;

	@Column(name = "otp_status")
	private Integer otpStatus;

	public Long getId() {
		return id;
	}

	public String getCustomStudentId() {
		return customStudentId;
	}

	public String getToken() {
		return token;
	}

	public Date getDateExecuted() {
		return dateExecuted;
	}

	public Date getOtpGenerateDate() {
		return otpGenerateDate;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public Integer getOtpStatus() {
		return otpStatus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setDateExecuted(Date dateExecuted) {
		this.dateExecuted = dateExecuted;
	}

	public void setOtpGenerateDate(Date otpGenerateDate) {
		this.otpGenerateDate = otpGenerateDate;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	public void setOtpStatus(Integer otpStatus) {
		this.otpStatus = otpStatus;
	}

}
