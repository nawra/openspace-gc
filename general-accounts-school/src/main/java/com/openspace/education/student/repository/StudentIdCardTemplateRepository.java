package com.openspace.education.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdCardTemplate;

public interface StudentIdCardTemplateRepository extends JpaRepository<StudentIdCardTemplate, Long> {
	
	public StudentIdCardTemplate findByInstituteAndTemplateId(Institute isInstitute,Integer templateId);
	public StudentIdCardTemplate findByInstitute(Institute institute);

}
