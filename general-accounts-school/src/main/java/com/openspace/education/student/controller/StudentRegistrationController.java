package com.openspace.education.student.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.request.ExcelRegistrationWithStudentId;
import com.openspace.education.student.model.request.MultipleStudentRegistrationRequest;
import com.openspace.education.student.model.request.RegistrationDeleteRequest;
import com.openspace.education.student.model.request.StudentAddressUpdateRequest;
import com.openspace.education.student.model.request.StudentBasicViewUpdateRequest;
import com.openspace.education.student.model.request.StudentCategoryInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentEnableDisableRequest;
import com.openspace.education.student.model.request.StudentGroupInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentIdUpdateRequest;
import com.openspace.education.student.model.request.StudentSectionInfoUpdateRequest;
import com.openspace.education.student.service.StudentRegistrationService;
import com.openspace.education.user.service.PasswordRecoveryService;

@Controller
@RequestMapping(value = "/student/registration")
public class StudentRegistrationController {

	@Autowired
	private StudentRegistrationService studentRegistrationService;
	


	
	@PostMapping(value = "/multiple/save")
	public ResponseEntity<BaseResponse> doMultipleStudentRegistration(@RequestBody @Valid MultipleStudentRegistrationRequest request){
		BaseResponse baseResponse=studentRegistrationService.doStudentRegistration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PostMapping(value = "/excel/with/student-id")
	public ResponseEntity<BaseResponse> doExcelRegistrationWithStudentId(@RequestBody @Valid ExcelRegistrationWithStudentId request){
		BaseResponse baseResponse=studentRegistrationService.doExcelRegistrationWithStudentId(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PostMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteStudentRegistration(@RequestBody @Valid RegistrationDeleteRequest request){
		BaseResponse baseResponse=studentRegistrationService.deleteStudentRegistration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/by/class-configuration-id")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfiguration(@RequestParam Long classConfigurationId){
		Integer academicYear=UserInfoUtils.getInstituteCurrentAcademicYear();
		ItemResponse itemResponse=studentRegistrationService.sectionWiseStudentRegistrationList(classConfigurationId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/enable/disable/list")
	public ResponseEntity<ItemResponse> studentEnableDisableList(@RequestParam Long classConfigurationId,@RequestParam Integer academicYear ){
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		ItemResponse itemResponse = studentRegistrationService.sectionWiseStudentEnableDisableList(classConfigurationId, academicYear, institute);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/by/class-configuration-id/group-id")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfiguration(@RequestParam Long classConfigurationId,@RequestParam Long groupId){
		Integer academicYear=UserInfoUtils.getInstituteCurrentAcademicYear();
		ItemResponse itemResponse=studentRegistrationService.sectionAndGroupWiseStudentRegistrationList(classConfigurationId, groupId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/for-migration/by/class-configuration-id/group-id")
	public ResponseEntity<ItemResponse> studentListForMigrationByClassConfigurationIdAndGroupId(@RequestParam Long classConfigurationId,@RequestParam Long groupId){
		Integer academicYear=UserInfoUtils.getInstituteCurrentAcademicYear();
		ItemResponse itemResponse=studentRegistrationService.sectionAndGroupWiseStudentListForMigration(classConfigurationId, groupId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/list/by/class-configuration-id/group-id/academic-year")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfigurationAndGroupInfoAndAcademicYear(@RequestParam Long classConfigurationId,@RequestParam Long groupId,@RequestParam Integer academicYear){
		ItemResponse itemResponse=studentRegistrationService.sectionAndGroupWiseStudentRegistrationList(classConfigurationId, groupId, academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@PutMapping(value = "/basic/update")
	public ResponseEntity<BaseResponse> updateStudentBasic(@RequestBody List<StudentBasicViewUpdateRequest> studentBasicViewUpdateRequests){
		BaseResponse baseResponse=studentRegistrationService.updateStudentBasicInfo(studentBasicViewUpdateRequests);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/custom-id/update")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<BaseResponse> updateStudentCustomId(@RequestBody List<StudentIdUpdateRequest> requests){
		BaseResponse baseResponse=studentRegistrationService.updateStudentCustomIds(requests);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/section/update")
	public ResponseEntity<BaseResponse> updateStudentSection(@RequestBody @Valid StudentSectionInfoUpdateRequest request){
		BaseResponse baseResponse=studentRegistrationService.updateStudentSectionInfo(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/group/update")
	public ResponseEntity<BaseResponse> updateStudentGroup(@RequestBody @Valid StudentGroupInfoUpdateRequest request){
		BaseResponse baseResponse=studentRegistrationService.updateStudentGroupInfo(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PutMapping(value = "/category/update")
	public ResponseEntity<BaseResponse> updateStudentCategory(@RequestBody @Valid StudentCategoryInfoUpdateRequest request){
		BaseResponse baseResponse=studentRegistrationService.updateStudentCategoryInfo(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@PostMapping(value = "/photo/update")
	public ResponseEntity<BaseResponse> updateSingleStudentPhoto(@RequestParam Long studentId,@RequestParam MultipartFile file){
		BaseResponse baseResponse=studentRegistrationService.updateSingleStudentPhoto(studentId, file);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	
	@PostMapping(value = "/photo/delete")
	public ResponseEntity<BaseResponse> deleteSingleStudentPhoto(@RequestParam Long studentId){
		BaseResponse baseResponse=studentRegistrationService.deletePhoto(studentId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	@PostMapping(value = "/address/update")
	public ResponseEntity<BaseResponse> updateSingleStudentAddress(@RequestBody @Valid StudentAddressUpdateRequest request){
		BaseResponse baseResponse=studentRegistrationService.updateStudentAddressInfo(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	@PostMapping(value = "/enable/disable")
	public ResponseEntity<BaseResponse> updateStudentStatus(@RequestBody @Valid StudentEnableDisableRequest request){
		BaseResponse baseResponse=studentRegistrationService.updateStudentStatusInfo(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	  
	   
	   
	   

}
