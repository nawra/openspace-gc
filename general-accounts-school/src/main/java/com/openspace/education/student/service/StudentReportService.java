package com.openspace.education.student.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingClassRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.model.request.SectionGroupCategoryStudentViewRequest;
import com.openspace.education.student.model.response.ClassWiseStudentView;
import com.openspace.education.student.model.response.SectionWiseStudentSummaryResponse;
import com.openspace.education.student.model.response.SingleStudentPublicView;
import com.openspace.education.student.model.response.StudentGeneralViewResponse;
import com.openspace.education.student.repository.StudentIdentificationRepository;

@Service
public class StudentReportService {
	
	@Autowired
	public StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	public StudentRegistrationService studentRegistrationService;
	
	@Autowired
	public ClassConfigurationRepository classConfigurationRepository;
	
	@Autowired
	public CoreSettingClassRepository coreSettingClassRepository;
	
	
	String[] colorArray= {"#8DA2DD", "#4A2AC0", "#F0D193", "#ECA0BA", "#DD3B3B", "#6BD344", "#D86500", "#6E90FF", "#0D3F9B", "#AAD7EB", "#94F686", "#FC296D", "#99DF72", "#DCB927", "#005985"};
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemResponse studentGeneralList(Long id,int typeOfReport,Integer academicYear) {
		
		//typeOfReport = 1 (class wise report)
		//typeOfReport = 2 (section wise report)
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> studentIdentifications = null;
		
		if(typeOfReport == 1) {
			studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfoClassInfoIdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, id,true);
		}else {
			studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, id,true);

		}
		
		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemResponse studentGeneralListBySectionGroupAndCategory(SectionGroupCategoryStudentViewRequest request) {

		ItemResponse itemResponse=new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> studentIdentifications = null;
		
		studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndStudentCategoryInfo_IdAndStudentStatusOrderByStudentRollAsc(institute, request.getAcademicYear(),request.getClassConfigId(),request.getGroupId(),request.getCategoryId(),true);
		
		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}
	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemResponse studentGeneralListGroupWise(Long classId,Long groupId, Integer academicYear) {
		
		//typeOfReport = 1 (class wise report)
		//typeOfReport = 2 (section wise report)
		//typeOfReport = 3 (group wise report)
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> studentIdentifications=studentIdentificationRepository.findByInstituteAndAcademicYearAndClassConfigurationInfoClassInfoIdAndGroupInfoIdAndStudentStatusOrderByStudentRollAsc(institute, academicYear, classId, groupId, true);

		List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si:studentIdentifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		
		}
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
		
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemResponse generalSingleStudentView(String customStudentId) {
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute=UserInfoUtils.getLoggedInInstitute();

		Integer academicYear=institute.getAcademicYear();
		
		StudentIdentification studentIdentification=studentIdentificationRepository.findByInstituteAndAcademicYearAndStudentBasic_CustomStudentId(institute, academicYear, customStudentId);
		
		if(studentIdentification==null) {
			itemResponse.setMessage("No Student Found.");	
			itemResponse.setMessageType(0);
			return itemResponse;	
		}
		
		SingleStudentPublicView view=new SingleStudentPublicView();
		view.setInstituteName(institute.getInstituteName());
		view.setInstituteAddress(institute.getAddress());
		view.setAcademicYear(academicYear);
		view.setInstituteId(institute.getInstituteId());
		
		studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(studentIdentification, view);
		
		itemResponse.setItem(view);
		itemResponse.setMessage("OK");	
		itemResponse.setMessageType(1);
		return itemResponse;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemResponse generalSingleStudentView(String customStudentId, Integer academicYear) {
		
		ItemResponse itemResponse=new ItemResponse();
		Institute institute=UserInfoUtils.getLoggedInInstitute();

		
		StudentIdentification studentIdentification=studentIdentificationRepository.findByInstituteAndAcademicYearAndStudentBasic_CustomStudentId(institute, academicYear, customStudentId);
		
		if(studentIdentification==null) {
			itemResponse.setMessage("No Student Found.");	
			itemResponse.setMessageType(0);
			return itemResponse;	
		}
		
		SingleStudentPublicView view=new SingleStudentPublicView();
		view.setInstituteName(institute.getInstituteName());
		view.setInstituteAddress(institute.getAddress());
		view.setAcademicYear(academicYear);
		view.setInstituteId(institute.getInstituteId());
		
		studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(studentIdentification, view);
		
		itemResponse.setItem(view);
		itemResponse.setMessage("OK");	
		itemResponse.setMessageType(1);
		return itemResponse;
	}
	
	
	
	
public ItemResponse<List<SectionWiseStudentSummaryResponse>> getSectionWiseStudentSummary() {
	
	Institute institute = UserInfoUtils.getLoggedInInstitute();
        
		ItemResponse<List<SectionWiseStudentSummaryResponse>> itemResponse = new ItemResponse<>();
        
        List<StudentIdentification> identifications = studentIdentificationRepository
                .findByInstituteAndAcademicYearAndStudentStatusOrderByStudentRollAsc(institute,institute.getAcademicYear(),true);
        
        List<ClassConfiguration> classConfigurations = classConfigurationRepository.findByInstituteOrderBySerial(institute);
               
        List<SectionWiseStudentSummaryResponse> sectionWiseStudentSummaryResponse = classConfigurations.stream().map((classConfiguration) -> {
        	
        	SectionWiseStudentSummaryResponse studentSummaryResponse = new SectionWiseStudentSummaryResponse();
        	
                    studentSummaryResponse.setClassShiftSectionName(classConfiguration.getClassInfo().getName() + "-"
                            + classConfiguration.getShiftInfo().getName() + "-"
                            + classConfiguration.getSectionInfo().getName());
                    
                    studentSummaryResponse.setClassName(classConfiguration.getClassInfo().getName());
                    studentSummaryResponse.setShiftName(classConfiguration.getShiftInfo().getName());
                    studentSummaryResponse.setSectionName(classConfiguration.getSectionInfo().getName());
                    studentSummaryResponse.setTotalStudent(sectionWiseStudentList(classConfiguration.getId(), identifications).size());
                    
                    return studentSummaryResponse;
                    
                }).collect(Collectors.toList());
        
        
        if(sectionWiseStudentSummaryResponse.size()>0){
          
        	itemResponse.setItem(sectionWiseStudentSummaryResponse);
        	itemResponse.setMessageType(1);
        	itemResponse.setMessage("OK");
        
        } else {
          
          itemResponse.setItem(sectionWiseStudentSummaryResponse);
          itemResponse.setMessageType(0);
          itemResponse.setMessage("No Data Found");
            
        }
        
        
        return itemResponse;
    }


	public List<StudentIdentification> sectionWiseStudentList(Long classConfigId, List<StudentIdentification> studentIdentifications) {
		return studentIdentifications.stream()
				.filter(studentIdentification -> Objects.equals(studentIdentification.getClassConfigurationInfo().getId(), classConfigId))
				.collect(Collectors.toList());
	}

	
	
	public ItemResponse classWiseStudentCount() {
		
		ItemResponse itemResponse = new ItemResponse();
		
		Institute institute= UserInfoUtils.getLoggedInInstitute();
		
		List<CoreSettingClass> classes = coreSettingClassRepository.findByInstituteOrderBySerial(institute);
		
		List<Object[]> objects=studentIdentificationRepository.classWiseStudentSummary(institute.getAcademicYear(), institute.getInstituteId());
		
		List<ClassWiseStudentView> views = new ArrayList<>();
		
		for(CoreSettingClass cl : classes) {
			
			ClassWiseStudentView view =new ClassWiseStudentView();
			
			view.setClassId(cl.getId());
			view.setClassName(cl.getName());
			view.setColor(getRandomColor());
			
			for(Object[] obj : objects) {
				
				Long classId=Long.parseLong(obj[0]+"");
				
				if(cl.getId().equals(classId)) {
					
					view.setTotalStudent(Integer.parseInt(obj[2]+""));
					
					break;
				}
			}
			
			views.add(view);
			
		  }
		
		itemResponse.setItem(views);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
	}
	
	
	public String getRandomColor() {
		
	    String rnd = colorArray[new Random().nextInt(colorArray.length)];
	   
	    return rnd;
	}
	
	
	public ItemResponse viewStudentByCustomStudentId(String customStudentId) {
		
		ItemResponse itemResponse = new ItemResponse();
		
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		
		List<StudentIdentification> identifications = studentIdentificationRepository.findByInstituteAndStudentBasic_CustomStudentIdOrderByAcademicYearAsc(institute, customStudentId);
		
        List<StudentGeneralViewResponse> viewResponses=new ArrayList<>();
		
		for(StudentIdentification si : identifications) {
			
			StudentGeneralViewResponse view=new StudentGeneralViewResponse();
			
			studentRegistrationService.copyStudentIdentificationIntoStudentGeneralViewResponse(si, view);
			
			viewResponses.add(view);
		}
		
		if(viewResponses.size()<1) {
			
			itemResponse.setItem(viewResponses);
			itemResponse.setMessage("No Student Found.");
			itemResponse.setMessageType(0);
			
		}
		
		else {
		
		itemResponse.setItem(viewResponses);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		}
		
		return itemResponse;
	}
}
