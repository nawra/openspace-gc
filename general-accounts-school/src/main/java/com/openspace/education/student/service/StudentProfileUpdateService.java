package com.openspace.education.student.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.FileFolder;
import com.openspace.education.image.FileStorageService;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.location.model.entity.Thana;
import com.openspace.education.location.repository.ThanaRepository;
import com.openspace.education.student.model.entity.StudentBasic;
import com.openspace.education.student.model.request.StudentProfileAddressInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentProfileBasicInfoUpdateRequest;
import com.openspace.education.student.model.request.StudentProfileGuardianInfoUpdateRequest;
import com.openspace.education.student.repository.StudentBasicRepository;

@Service
public class StudentProfileUpdateService {
	
	public Logger logger = LoggerFactory.getLogger(StudentProfileUpdateService.class);
	
	@Autowired
	private StudentBasicRepository studentBasicRepository;
	@Autowired
	private ImageStorageService imageStorageService;
	
	@Autowired
	private ThanaRepository thanaRepository;
	@Autowired
	private FileStorageService fileStorageService;

	 	public BaseResponse updateStudentProfileBasicInfo(StudentProfileBasicInfoUpdateRequest request) {
	    	
	    	 BaseResponse baseResponse = new BaseResponse();
	    	 
	    	 StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstituteInstituteId(request.getStudentId(),request.getInstituteId());
	    	 
	    	 if(studentBasic == null) {
	    		 baseResponse.setMessage("No Student Found."); 
	    		 baseResponse.setMessageType(0);
	    		 return baseResponse;
	    	 }
	    	 
	    	 studentBasic.setStudentName(request.getStudentName());
	    	 studentBasic.setStudentDOB(request.getStudentDOB());
	    	 studentBasic.setStudentReligion(request.getStudentReligion());
	    	 studentBasic.setStudentGender(request.getStudentGender());
	    	 studentBasic.setBloodGroup(request.getBloodGroup());
	    	 studentBasic.setBirthCertificateNo(request.getBirthCertificateNo());
	    	 	    		    	 
	    	 try {
	    		 
	    		String fileExtension = fileStorageService.provideFileExtension(request.getStudentImageFileName()).get();
		 			
	 	 		String studentImageName = request.getStudentId()+"_photo." + fileExtension;
	 	 		
	 	 		imageStorageService.uploadImgHeightWidth(FileFolder.STUDENT.name(), studentImageName, fileExtension, request.getStudentImageFileContent(), 200, 180);
	 	 		
	 	 		studentBasic.setImageName(studentImageName);
	 	    	 
			} catch (Exception e) {
				logger.error("Student Photo Update Problem in student profile update");
			}
	    	 
	    	 studentBasicRepository.save(studentBasic);
	 		
	    	 baseResponse.setMessage("Student Basic Info Successfully Updated."); 
			 baseResponse.setMessageType(1);
	    	 
	    	 return baseResponse;
	    	 
	     }
	 	
	 	
	 	
	 	
	 	public BaseResponse updateStudentGuardianInfo(StudentProfileGuardianInfoUpdateRequest request) {
	    	
	    	 BaseResponse baseResponse = new BaseResponse();
	    	 
	    	 StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstituteInstituteId(request.getStudentId(),request.getInstituteId());
	    	 
	    	 if(studentBasic == null) {
	    		 baseResponse.setMessage("No Student Found."); 
	    		 baseResponse.setMessageType(0);
	    		 return baseResponse;
	    	 }
	    	 
	    	 studentBasic.setFatherName(request.getFatherName());	    	 
	    	 studentBasic.setFatherOccupation(request.getFatherOccupation());
	    	 studentBasic.setFatherNid(request.getFatherNid());
	    	 
	    	 studentBasic.setMotherName(request.getMotherName());
	    	 studentBasic.setMotherOccupation(request.getMotherOccupation());
	    	 studentBasic.setMotherNid(request.getMotherNid());
	    	 studentBasic.setGuardianMobile(request.getGuardianMobile());
	    	 studentBasic.setFatherDesignation(request.getFatherDesignation());
	    	 studentBasic.setFatherWorkingPlace(request.getFatherWorkingPlace());
	    	 studentBasic.setMotherDesignation(request.getMotherDesignation());
	    	 studentBasic.setMotherWorkingPlace(request.getMotherWorkingPlace());
	    	 	    	 
	    	 try {
	    		 
	    		 	String fatherFileExtension = fileStorageService.provideFileExtension(request.getFatherPhotoName()).get();
		 			
			 		String fatherPhotoName = request.getInstituteId()+"_"+request.getStudentId()+"_father." + fatherFileExtension;
			 		
			 		imageStorageService.uploadImgHeightWidth(FileFolder.STUDENT.name(), fatherPhotoName, fatherFileExtension, request.getFatherPhotoFileContent(), 200, 180);
			 		
			 		studentBasic.setFatherPhoto(fatherPhotoName);
			 				
			 		
			 		String motherFileExtension = fileStorageService.provideFileExtension(request.getMotherPhotoName()).get();
			 			
				 	String motherPhotoName = request.getInstituteId()+"_"+request.getStudentId()+"_mother_photo." + motherFileExtension;
				 	
				 	imageStorageService.uploadImgHeightWidth(FileFolder.STUDENT.name(), motherPhotoName, motherFileExtension, request.getMotherPhotoFileContent(), 200, 180);
				 	
				 	studentBasic.setMotherPhoto(motherPhotoName);
			 		
			} catch (Exception e) {
				logger.error("Father of Mother Photo Update Problem in student profile update");
			}
	    	 
	    	 studentBasicRepository.save(studentBasic);
	    	 
	    	 baseResponse.setMessage("Student Guardian Info Successfully Updated."); 
			 baseResponse.setMessageType(1);
	    	 
	    	 return baseResponse;
	    	 
	     }
	 	
	 		 		 	
	    @Transactional
	 	public BaseResponse updateStudentAddressInfo(StudentProfileAddressInfoUpdateRequest request) {
	    	 	    	 
	    	 BaseResponse baseResponse = new BaseResponse();
	    	 
	    	 StudentBasic studentBasic = studentBasicRepository.findByStudentIdAndInstituteInstituteId(request.getStudentId(),request.getInstituteId());
	    	 
	    	 List<Thana> thanas = thanaRepository.findAll();
	    	 
	    	 if(thanas == null) {
	    		 baseResponse.setMessage("No Thana Found."); 
	    		 baseResponse.setMessageType(0);
	    		 return baseResponse;
	    	 }
	    	 
	    	 Thana PresentThana = getThanaByThanaId(request.getPresentThanaId());
	    	 
	    	 Thana permanentThana = getThanaByThanaId(request.getPermanentThanaId());
	    	 	    	 
	    	 if(studentBasic == null) {
	    		 baseResponse.setMessage("No Student Found."); 
	    		 baseResponse.setMessageType(0);
	    		 return baseResponse;
	    	 }
	    	 	    	  	    	 
	    	 studentBasic.setPresentPostOffice(request.getPresentPostOffice());
	    	 studentBasic.setMailingAddress(request.getMailingAddress());
	    	 studentBasic.setPresentThana(PresentThana);
	    	 studentBasic.setPresentVillege(request.getPresentVillege());
	    	 	    	 	    	 
	    	 studentBasic.setPermanentPostOffice(request.getPermanentPostOffice());
	    	 studentBasic.setMailingAddress(request.getMailingAddress());
	    	 studentBasic.setPermanentThana(permanentThana);
	    	 studentBasic.setPermanentVillege(request.getPermanentVillege());
	    	 
	    	 studentBasicRepository.save(studentBasic);
	    	 
	    	 baseResponse.setMessage("Student Address Successfully Updated."); 
			 baseResponse.setMessageType(1);
	    	 
	    	 return baseResponse;
	    	 
	     }
	    
	    
	    public Thana getThanaByThanaId(Integer thanaId) {
	    	
	    	List<Thana> thanas = thanaRepository.findAll();	    	
	    	Thana thana = thanas.stream().filter(t->thanaId==t.getThanaId()).findAny().orElse(null);
	    	return thana;
	    }
	     
}
