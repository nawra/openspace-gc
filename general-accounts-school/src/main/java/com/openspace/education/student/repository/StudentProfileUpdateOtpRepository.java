package com.openspace.education.student.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.student.model.entity.StudentProfileUpdateOtp;

public interface StudentProfileUpdateOtpRepository extends JpaRepository<StudentProfileUpdateOtp, Long>{

	public List<StudentProfileUpdateOtp> findByInstituteIdAndCustomStudentIdAndOtpGenerateDate(Long instituteId,String customStudentId, Date currentDate);
	
	public StudentProfileUpdateOtp findByInstituteIdAndCustomStudentIdAndOtpGenerateDateAndTokenAndOtpStatus(Long instituteId,String customStudentId, Date currentDate,String token,Integer otpStatus);
	
}
