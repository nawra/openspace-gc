package com.openspace.education.student.model.request;

import javax.validation.constraints.NotNull;

public class StudentAddressUpdateRequest {

	@NotNull
	private Long studentId;

	@NotNull
	private Integer presentThanaId;

	private String presentPostOffice;

	private String mailingAddress;

	private String presentVillege;

	public Long getStudentId() {
		return studentId;
	}

	public Integer getPresentThanaId() {
		return presentThanaId;
	}

	public String getPresentPostOffice() {
		return presentPostOffice;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public String getPresentVillege() {
		return presentVillege;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setPresentThanaId(Integer presentThanaId) {
		this.presentThanaId = presentThanaId;
	}

	public void setPresentPostOffice(String presentPostOffice) {
		this.presentPostOffice = presentPostOffice;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public void setPresentVillege(String presentVillege) {
		this.presentVillege = presentVillege;
	}

	

}
