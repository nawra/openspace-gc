package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentSectionInfoUpdateRequest {

	@NotNull
	private Long classConfigId;
	
	@NotNull
	@Size(min = 1)
	private List<Long> identificationIds;
	
	

	public Long getClassConfigId() {
		return classConfigId;
	}

	public void setClassConfigId(Long classConfigId) {
		this.classConfigId = classConfigId;
	}

	public List<Long> getIdentificationIds() {
		return identificationIds;
	}

	public void setIdentificationIds(List<Long> identificationIds) {
		this.identificationIds = identificationIds;
	}
	
	
	
}
