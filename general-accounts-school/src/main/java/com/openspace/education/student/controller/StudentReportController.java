package com.openspace.education.student.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.student.model.request.SectionGroupCategoryStudentViewRequest;
import com.openspace.education.student.service.StudentReportService;

@Controller
@RequestMapping(value = "/student/report")
public class StudentReportController {
	
	@Autowired
	public StudentReportService studentReportService;
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/basic/info/list/by/class-id")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassId(@RequestParam Long classId,@RequestParam Integer academicYear){
		ItemResponse itemResponse=studentReportService.studentGeneralList(classId,1,academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/basic/info/list/by/class-configuration-id")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfiguration(@RequestParam Long classConfigurationId, @RequestParam Integer academicYear){
		ItemResponse itemResponse=studentReportService.studentGeneralList(classConfigurationId,2,academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/basic/info/list/by/class-id/group-id")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfiguration(@RequestParam Long classId,@RequestParam Long groupId, @RequestParam Integer academicYear){
		ItemResponse itemResponse=studentReportService.studentGeneralListGroupWise(classId,groupId,academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/basic/info/list/by/academic-year/section/group/category")
	public ResponseEntity<ItemResponse> studentRegistrationListByClassConfiguration(@RequestBody SectionGroupCategoryStudentViewRequest request){
		ItemResponse itemResponse=studentReportService.studentGeneralListBySectionGroupAndCategory(request);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/view/by/custom-student-id")
	public ResponseEntity<ItemResponse> studentViewByCustomStudentId(@RequestParam String customStudentId){
		ItemResponse itemResponse=studentReportService.generalSingleStudentView(customStudentId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/view/by/custom-student-id/academic-year")
	public ResponseEntity<ItemResponse> studentViewByCustomStudentIdAndAcademicYear(@RequestParam String customStudentId, @RequestParam Integer academicYear){
		ItemResponse itemResponse=studentReportService.generalSingleStudentView(customStudentId,academicYear);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/section-wise/summary")
	public ResponseEntity<ItemResponse> getSectionWiseStudentSummary(){
		
		ItemResponse itemResponse=studentReportService.getSectionWiseStudentSummary();
		
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/class-wise/summary")
	public ResponseEntity<ItemResponse> getClassWiseStudentSummary(){
		
		ItemResponse itemResponse=studentReportService.classWiseStudentCount();
		
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/single/general/view")
	public ResponseEntity<ItemResponse> findStudentGeneralView(@RequestParam String customStudentId){
		
		ItemResponse itemResponse=studentReportService.viewStudentByCustomStudentId(customStudentId);
		
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}

}
