package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentEnableDisableRequest {

	@NotNull
	@Size(min = 1)
	private List<Long> identificationIds;
	
	private String disableNote;
	
	@NotNull
	private Boolean studentStatus;

	public List<Long> getIdentificationIds() {
		return identificationIds;
	}

	public void setIdentificationIds(List<Long> identificationIds) {
		this.identificationIds = identificationIds;
	}

	public String getDisableNote() {
		return disableNote;
	}

	public void setDisableNote(String disableNote) {
		this.disableNote = disableNote;
	}

	public Boolean getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(Boolean studentStatus) {
		this.studentStatus = studentStatus;
	}

	
	
	
	
	
}
