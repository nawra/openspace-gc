package com.openspace.education.student.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.student.model.request.MigrationPushbackRq;
import com.openspace.education.student.model.request.StudentMigrationRequest;
import com.openspace.education.student.service.StudentMigrationService;

@Controller
@RequestMapping(value = "/student/migration")
public class StudentMigrationController {
	
	@Autowired
	private StudentMigrationService studentMigrationService;
	
	
	@PostMapping(value = "/done")
	public ResponseEntity<BaseResponse> doMultipleStudentMigration(@RequestBody @Valid StudentMigrationRequest request){
		BaseResponse baseResponse=studentMigrationService.migrateStudent(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PostMapping(value = "/push-back")
	public ResponseEntity<BaseResponse> doMultipleStudentMigrationPushBack(@RequestBody @Valid MigrationPushbackRq request){
		BaseResponse baseResponse=studentMigrationService.pushbackMigration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}

	
	@GetMapping(value = "/change")
	public ResponseEntity<BaseResponse> doMultipleStudentMigrationChange(@RequestParam Long identificationId, boolean migrationStatus){
		BaseResponse baseResponse=studentMigrationService.changeMigration(identificationId, migrationStatus);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
}
