package com.openspace.education.student.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingAcademicYear;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.model.entity.CoreSettingStudentCategory;
import com.openspace.education.institute.model.entity.Institute;

@Entity
@Table(name = "student_identification", uniqueConstraints = @UniqueConstraint(columnNames = { "academic_year","institute_id", "student_id" }))
public class StudentIdentification implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "identification_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long identificationId;

	@Column(name = "student_roll")
	private Integer studentRoll;

	@Column(name = "student_status")
	private boolean studentStatus = true;

	@Column(name = "migration_status")
	private boolean migrationStatus = true; // true is for migrated on that academicYear
	
	@Column(name = "migrated_status")
	private boolean migratedStatus = false;

	@Column(name = "disable_Date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date disableDate;

	@Column(name = "disable_reason")
	private String disableReason;

	

	@ManyToOne
	@JoinColumn(name = "student_id", nullable = false)
	private StudentBasic studentBasic;

	
	@Column(name = "academic_year", nullable = false)
	private Integer academicYear;

	@ManyToOne
	@JoinColumn(name = "student_category_id", nullable = false)
	private CoreSettingStudentCategory studentCategoryInfo;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private CoreSettingGroup groupInfo;

	@ManyToOne
	@JoinColumn(name = "class_config_id", nullable = false)
	private ClassConfiguration classConfigurationInfo;

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;



	public Long getIdentificationId() {
		return identificationId;
	}

	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public boolean isStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(boolean studentStatus) {
		this.studentStatus = studentStatus;
	}

	public boolean isMigrationStatus() {
		return migrationStatus;
	}

	public void setMigrationStatus(boolean migrationStatus) {
		this.migrationStatus = migrationStatus;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public String getDisableReason() {
		return disableReason;
	}

	public void setDisableReason(String disableReason) {
		this.disableReason = disableReason;
	}


	public StudentBasic getStudentBasic() {
		return studentBasic;
	}

	public void setStudentBasic(StudentBasic studentBasic) {
		this.studentBasic = studentBasic;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public CoreSettingStudentCategory getStudentCategoryInfo() {
		return studentCategoryInfo;
	}

	public void setStudentCategoryInfo(CoreSettingStudentCategory studentCategoryInfo) {
		this.studentCategoryInfo = studentCategoryInfo;
	}

	public CoreSettingGroup getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(CoreSettingGroup groupInfo) {
		this.groupInfo = groupInfo;
	}

	

	public ClassConfiguration getClassConfigurationInfo() {
		return classConfigurationInfo;
	}

	public void setClassConfigurationInfo(ClassConfiguration classConfigurationInfo) {
		this.classConfigurationInfo = classConfigurationInfo;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public boolean isMigratedStatus() {
		return migratedStatus;
	}

	public void setMigratedStatus(boolean migratedStatus) {
		this.migratedStatus = migratedStatus;
	}

	
}
