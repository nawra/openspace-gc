package com.openspace.education.student.model.request;

public class StudentMigrationHelperRequest {

	private Long identificationId;
	private Integer newRoll;
	
	public Long getIdentificationId() {
		return identificationId;
	}
	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}
	public Integer getNewRoll() {
		return newRoll;
	}
	public void setNewRoll(Integer newRoll) {
		this.newRoll = newRoll;
	}
	
	
	
}
