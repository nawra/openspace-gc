package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentGroupInfoUpdateRequest {

	@NotNull
	private Long groupId;
	
	@NotNull
	@Size(min = 1)
	private List<Long> identificationIds;
	
	
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public List<Long> getIdentificationIds() {
		return identificationIds;
	}

	public void setIdentificationIds(List<Long> identificationIds) {
		this.identificationIds = identificationIds;
	}
	
}
