package com.openspace.education.student.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.location.model.entity.Thana;

@Entity
@Table(name = "student_basic", uniqueConstraints = @UniqueConstraint(columnNames = { "custom_student_id",
		"institute_id" }))
public class StudentBasic implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "student_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long studentId;

	@Column(name = "custom_student_id")
	private String customStudentId;

	@Column(name = "student_name", nullable = false)
	private String studentName;

	@Column(name = "student_gender", nullable = false)
	private String studentGender;

	@Column(name = "student_dob")
	@Temporal(TemporalType.DATE)
	private Date studentDOB;

	@Column(name = "student_religion", nullable = false)
	private String studentReligion;

	@Column(name = "birth_certificate_no")
	private String birthCertificateNo;

	@Column(name = "father_name", nullable = false)
	private String fatherName;

	@Column(name = "father_photo")
	private String fatherPhoto;

	@Column(name = "father_occupation")
	private String fatherOccupation;

	@Column(name = "father_working_place")
	private String fatherWorkingPlace;

	@Column(name = "father_designation")
	private String fatherDesignation;

	@Column(name = "father_nid")
	private String fatherNid;

	@Column(name = "mother_name", nullable = false)
	private String motherName;

	@Column(name = "mother_photo")
	private String motherPhoto;

	@Column(name = "mother_occupation")
	private String motherOccupation;

	@Column(name = "mother_working_place")
	private String motherWorkingPlace;

	@Column(name = "mother_designation")
	private String motherDesignation;

	@Column(name = "mother_nid")
	private String motherNid;

	@Column(name = "guardian_mobile", nullable = false)
	private String guardianMobile;

	@Column(name = "reg_date")
	@Temporal(TemporalType.DATE)
	private Date registrationDate;

	@Column(name = "guardian_email")
	private String guardianEmail;

	@Column(name = "std_session")
	private String academicSession;

	@Column(name = "blood_group")
	private String bloodGroup;

	@Column(name = "image_name")
	private String imageName;

	@Column(name = "mailing_address")
	private String mailingAddress;

	@Column(name = "present_post_office")
	private String presentPostOffice;

	@Column(name = "present_villege")
	private String presentVillege;

	@ManyToOne
	@JoinColumn(name = "present_thana_id")
	Thana presentThana;

	@Column(name = "permanent_post_office")
	private String permanentPostOffice;

	@Column(name = "permanent_villege")
	private String permanentVillege;

	@ManyToOne
	@JoinColumn(name = "permanent_thana_id")
	Thana permanentThana;

	@Column(name = "registration_no")
	private String registrationNo;

	@ManyToOne
	@JoinColumn(name = "institute_id", nullable = false)
	private Institute institute;

	@OneToMany(mappedBy = "studentBasic", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<StudentIdentification> studentIdentifications;

	public Long getStudentId() {
		return studentId;
	}

	public String getCustomStudentId() {
		return customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public Date getStudentDOB() {
		return studentDOB;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public String getBirthCertificateNo() {
		return birthCertificateNo;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getFatherPhoto() {
		return fatherPhoto;
	}

	public String getFatherOccupation() {
		return fatherOccupation;
	}

	public String getFatherWorkingPlace() {
		return fatherWorkingPlace;
	}

	public String getFatherDesignation() {
		return fatherDesignation;
	}

	public String getFatherNid() {
		return fatherNid;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getMotherPhoto() {
		return motherPhoto;
	}

	public String getMotherOccupation() {
		return motherOccupation;
	}

	public String getMotherWorkingPlace() {
		return motherWorkingPlace;
	}

	public String getMotherDesignation() {
		return motherDesignation;
	}

	public String getMotherNid() {
		return motherNid;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public String getGuardianEmail() {
		return guardianEmail;
	}

	public String getAcademicSession() {
		return academicSession;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public String getImageName() {
		return imageName;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public String getPresentPostOffice() {
		return presentPostOffice;
	}

	public String getPresentVillege() {
		return presentVillege;
	}

	public Thana getPresentThana() {
		return presentThana;
	}

	public String getPermanentPostOffice() {
		return permanentPostOffice;
	}

	public String getPermanentVillege() {
		return permanentVillege;
	}

	public Thana getPermanentThana() {
		return permanentThana;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public Institute getInstitute() {
		return institute;
	}

	public Set<StudentIdentification> getStudentIdentifications() {
		return studentIdentifications;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public void setStudentDOB(Date studentDOB) {
		this.studentDOB = studentDOB;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public void setBirthCertificateNo(String birthCertificateNo) {
		this.birthCertificateNo = birthCertificateNo;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setFatherPhoto(String fatherPhoto) {
		this.fatherPhoto = fatherPhoto;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public void setFatherWorkingPlace(String fatherWorkingPlace) {
		this.fatherWorkingPlace = fatherWorkingPlace;
	}

	public void setFatherDesignation(String fatherDesignation) {
		this.fatherDesignation = fatherDesignation;
	}

	public void setFatherNid(String fatherNid) {
		this.fatherNid = fatherNid;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setMotherPhoto(String motherPhoto) {
		this.motherPhoto = motherPhoto;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public void setMotherWorkingPlace(String motherWorkingPlace) {
		this.motherWorkingPlace = motherWorkingPlace;
	}

	public void setMotherDesignation(String motherDesignation) {
		this.motherDesignation = motherDesignation;
	}

	public void setMotherNid(String motherNid) {
		this.motherNid = motherNid;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public void setGuardianEmail(String guardianEmail) {
		this.guardianEmail = guardianEmail;
	}

	public void setAcademicSession(String academicSession) {
		this.academicSession = academicSession;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public void setPresentPostOffice(String presentPostOffice) {
		this.presentPostOffice = presentPostOffice;
	}

	public void setPresentVillege(String presentVillege) {
		this.presentVillege = presentVillege;
	}

	public void setPresentThana(Thana presentThana) {
		this.presentThana = presentThana;
	}

	public void setPermanentPostOffice(String permanentPostOffice) {
		this.permanentPostOffice = permanentPostOffice;
	}

	public void setPermanentVillege(String permanentVillege) {
		this.permanentVillege = permanentVillege;
	}

	public void setPermanentThana(Thana permanentThana) {
		this.permanentThana = permanentThana;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public void setStudentIdentifications(Set<StudentIdentification> studentIdentifications) {
		this.studentIdentifications = studentIdentifications;
	}

}
