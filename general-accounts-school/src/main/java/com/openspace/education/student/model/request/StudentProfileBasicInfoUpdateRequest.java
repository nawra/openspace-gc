package com.openspace.education.student.model.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StudentProfileBasicInfoUpdateRequest {

	private Long studentId;

	private String studentName;

	private String studentGender;

	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date studentDOB;

	private String studentReligion;

	private String bloodGroup;

	private String birthCertificateNo;	
	
	private String studentImageFileName;
	
	private byte[] studentImageFileContent;
	
	private Long instituteId;

	public Long getStudentId() {
		return studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public Date getStudentDOB() {
		return studentDOB;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public String getBirthCertificateNo() {
		return birthCertificateNo;
	}

	public String getStudentImageFileName() {
		return studentImageFileName;
	}

	public byte[] getStudentImageFileContent() {
		return studentImageFileContent;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public void setStudentDOB(Date studentDOB) {
		this.studentDOB = studentDOB;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public void setBirthCertificateNo(String birthCertificateNo) {
		this.birthCertificateNo = birthCertificateNo;
	}

	public void setStudentImageFileName(String studentImageFileName) {
		this.studentImageFileName = studentImageFileName;
	}

	public void setStudentImageFileContent(byte[] studentImageFileContent) {
		this.studentImageFileContent = studentImageFileContent;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}



}
