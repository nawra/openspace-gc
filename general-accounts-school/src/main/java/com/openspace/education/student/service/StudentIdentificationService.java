package com.openspace.education.student.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.student.repository.StudentIdentificationRepository;

@Service
public class StudentIdentificationService {
	
	public Logger logger = LoggerFactory.getLogger(StudentIdentificationService.class);
	
	@Autowired
	private StudentIdentificationRepository studentIdentificationRepository;

}
