package com.openspace.education.student.model.request;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentMigrationRequest {

	@NotNull
    private Long groupId;
    
	@NotNull
    private Long classConfigId;
    
	@NotNull
    private Integer migrationAcademicYear;
	
	@NotNull
	@Size(min = 1)
	private Set<StudentMigrationHelperRequest> studentInfos;
	

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Long getClassConfigId() {
		return classConfigId;
	}

	public void setClassConfigId(Long classConfigId) {
		this.classConfigId = classConfigId;
	}

	public Integer getMigrationAcademicYear() {
		return migrationAcademicYear;
	}

	public void setMigrationAcademicYear(Integer migrationAcademicYear) {
		this.migrationAcademicYear = migrationAcademicYear;
	}

	public Set<StudentMigrationHelperRequest> getStudentInfos() {
		return studentInfos;
	}

	public void setStudentInfos(Set<StudentMigrationHelperRequest> studentInfos) {
		this.studentInfos = studentInfos;
	}

	
	
	
	
}
