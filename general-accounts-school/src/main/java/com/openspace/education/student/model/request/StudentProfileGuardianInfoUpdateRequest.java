package com.openspace.education.student.model.request;


public class StudentProfileGuardianInfoUpdateRequest {

	private String fatherName;

	private String fatherWorkingPlace;

	private String fatherDesignation;

	private byte[] fatherPhotoFileContent;

	private String fatherPhotoName;

	private String fatherOccupation;

	private String fatherNid;

	private String motherName;

	private String motherPhotoName;

	private String motherWorkingPlace;

	private String motherDesignation;

	private byte[] motherPhotoFileContent;

	private String motherOccupation;

	private String motherNid;

	private String guardianMobile;

	private Long studentId;

	private Long instituteId;

	public String getFatherName() {
		return fatherName;
	}

	public String getFatherWorkingPlace() {
		return fatherWorkingPlace;
	}

	public String getFatherDesignation() {
		return fatherDesignation;
	}

	public byte[] getFatherPhotoFileContent() {
		return fatherPhotoFileContent;
	}

	public String getFatherPhotoName() {
		return fatherPhotoName;
	}

	public String getFatherOccupation() {
		return fatherOccupation;
	}

	public String getFatherNid() {
		return fatherNid;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getMotherPhotoName() {
		return motherPhotoName;
	}

	public String getMotherWorkingPlace() {
		return motherWorkingPlace;
	}

	public String getMotherDesignation() {
		return motherDesignation;
	}

	public byte[] getMotherPhotoFileContent() {
		return motherPhotoFileContent;
	}

	public String getMotherOccupation() {
		return motherOccupation;
	}

	public String getMotherNid() {
		return motherNid;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public Long getStudentId() {
		return studentId;
	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setFatherWorkingPlace(String fatherWorkingPlace) {
		this.fatherWorkingPlace = fatherWorkingPlace;
	}

	public void setFatherDesignation(String fatherDesignation) {
		this.fatherDesignation = fatherDesignation;
	}

	public void setFatherPhotoFileContent(byte[] fatherPhotoFileContent) {
		this.fatherPhotoFileContent = fatherPhotoFileContent;
	}

	public void setFatherPhotoName(String fatherPhotoName) {
		this.fatherPhotoName = fatherPhotoName;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public void setFatherNid(String fatherNid) {
		this.fatherNid = fatherNid;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setMotherPhotoName(String motherPhotoName) {
		this.motherPhotoName = motherPhotoName;
	}

	public void setMotherWorkingPlace(String motherWorkingPlace) {
		this.motherWorkingPlace = motherWorkingPlace;
	}

	public void setMotherDesignation(String motherDesignation) {
		this.motherDesignation = motherDesignation;
	}

	public void setMotherPhotoFileContent(byte[] motherPhotoFileContent) {
		this.motherPhotoFileContent = motherPhotoFileContent;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public void setMotherNid(String motherNid) {
		this.motherNid = motherNid;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

}
