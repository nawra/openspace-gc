package com.openspace.education.student.service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.model.entity.CoreSettingGroup;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.initialsetup.repository.CoreSettingGroupRepository;
import com.openspace.education.initialsetup.service.ClassConfigurationService;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.model.request.MigrationPushbackRq;
import com.openspace.education.student.model.request.StudentMigrationHelperRequest;
import com.openspace.education.student.model.request.StudentMigrationRequest;
import com.openspace.education.student.repository.StudentIdentificationRepository;



@Service
public class StudentMigrationService {
	
	public Logger logger = LoggerFactory.getLogger(StudentMigrationService.class);
	
	@Autowired
	private StudentIdentificationRepository studentIdentificationRepository;
	
	@Autowired
	private ClassConfigurationRepository classConfigurationRepository;
	
	@Autowired
	private CoreSettingGroupRepository coreSettingGroupRepository;
	
	
	
	
   public BaseResponse migrateStudent(StudentMigrationRequest migrationRq) {
        
        Integer migratedStudent = 0;
        Institute institute = UserInfoUtils.getLoggedInInstitute();
        Integer currentAcademicYear = institute.getAcademicYear();
        Integer migrationAcademicYear = migrationRq.getMigrationAcademicYear();
        
        BaseResponse response = new BaseResponse();
        
        if (migrationAcademicYear <= currentAcademicYear) {
            response.setMessageType(0);
            response.setMessage("Sorry! You must migrate to the next academic year");
            return response;
        }
        
        ClassConfiguration classConfiguration=classConfigurationRepository.findByIdAndInstitute(migrationRq.getClassConfigId(), institute);
        
        if (classConfiguration==null) {
            response.setMessageType(0);
            response.setMessage("No Section Found.");
            return response;
        }
        
        CoreSettingGroup groupInfo=coreSettingGroupRepository.findByIdAndInstitute(migrationRq.getGroupId(), institute);
        
        if (groupInfo==null) {
            response.setMessageType(0);
            response.setMessage("No Group Found.");
            return response;
        }
        
        Set<Long> identificationIds = new HashSet<>();
        for (StudentMigrationHelperRequest mr : migrationRq.getStudentInfos()) {
            identificationIds.add(mr.getIdentificationId());
        }
        
        List<StudentIdentification> currentStudentIdentifications = studentIdentificationRepository.findByInstituteAndIdentificationIdIn(institute, identificationIds);

        for (StudentIdentification si : currentStudentIdentifications) {

            for (StudentMigrationHelperRequest mr : migrationRq.getStudentInfos()) {

                if (Objects.equals(si.getIdentificationId(), mr.getIdentificationId())) {
                    StudentIdentification studentIdentification = new StudentIdentification();
                    studentIdentification.setClassConfigurationInfo(classConfiguration);
                    studentIdentification.setGroupInfo(groupInfo);
                    studentIdentification.setMigrationStatus(true);
                    studentIdentification.setAcademicYear(migrationAcademicYear);
                    studentIdentification.setStudentRoll(mr.getNewRoll());
                    studentIdentification.setStudentStatus(true);
                    studentIdentification.setInstitute(institute);
                    studentIdentification.setStudentBasic(si.getStudentBasic());
                    studentIdentification.setStudentCategoryInfo(si.getStudentCategoryInfo());
                    studentIdentification.setMigratedStatus(true);

                    try {
                        studentIdentificationRepository.saveAndFlush(studentIdentification);
                        si.setMigrationStatus(false);
                        studentIdentificationRepository.saveAndFlush(si);
                        migratedStudent++;
                        break;
                    } catch (Exception e) {
                       logger.error(""+e.getLocalizedMessage());
                        break;
                    }
                }
            }

        }

        if (migratedStudent == 0) {
            response.setMessageType(0);
            response.setMessage("No student has been migrated");
        } else {        	
            response.setMessageType(1);            
            response.setMessage(migratedStudent + " Students have been migrated successfully!");
       }
                
               
        return response;
    }
   
   
   
   
   
   public BaseResponse pushbackMigration(MigrationPushbackRq migrationPushbackRq) {

       BaseResponse response = new BaseResponse();

       Long instituteId = UserInfoUtils.getLoggedInInstituteId();
       Integer currentAcademicYear = null;
       Integer previousAcademicYear = migrationPushbackRq.getAcYearToPushback();
       Integer migrationPushBacked = 0;
       Set<Long> studentIds = new HashSet<>();

       List<StudentIdentification> currentStudentIdentifications = studentIdentificationRepository.findByInstitute_InstituteIdAndIdentificationIdIn(instituteId, migrationPushbackRq.getCurrentIndentificationIds());

       //-------------- find just current academic Year ---------//
       for (StudentIdentification si : currentStudentIdentifications) {
           currentAcademicYear = si.getAcademicYear();
           break;
       }

       if (previousAcademicYear >= currentAcademicYear) {
           response.setMessageType(0);
           response.setMessage("Push Back Academic Year Must be Less Than Current Academic Year");
           return response;
       }
       // -------------------------------------------------------//

       for (StudentIdentification si : currentStudentIdentifications) {
           studentIds.add(si.getStudentBasic().getStudentId());
       }

       List<StudentIdentification> previousStudentIdentifications = studentIdentificationRepository.findByInstitute_InstituteIdAndAcademicYearAndStudentBasic_StudentIdIn(instituteId, previousAcademicYear, studentIds); //studentIdentificationRepository.findByInstituteIdAndIdentificationIdIn(instituteId,identificationIds);

       for (StudentIdentification currentIdentification : currentStudentIdentifications) {

           for (StudentIdentification previousIdentification : previousStudentIdentifications) {

               if (currentIdentification.getStudentBasic().getStudentId().equals(previousIdentification.getStudentBasic().getStudentId())) {

                   try {
                       studentIdentificationRepository.delete(currentIdentification);
                       studentIdentificationRepository.flush();

                       previousIdentification.setMigrationStatus(true);
                       studentIdentificationRepository.saveAndFlush(previousIdentification);
                       migrationPushBacked++;
                       break;
                   } catch (Exception e) {
                       logger.info("pushbackMigration : " + e.getMessage());
                       break;
                   }
               }
           }
       }

       if (migrationPushBacked == 0) {
           response.setMessageType(0);
           response.setMessage("No Student Migrated");
       } else {
           response.setMessageType(1);
           response.setMessage(migrationPushBacked + " Students Migration has been Push Backed Successfully!");
       }

       return response;
   }
   
   
   
   @Transactional
   public BaseResponse changeMigration(Long identificationId, boolean migrationStatus) {
	   
	   BaseResponse response = new BaseResponse();
	   
	   Institute institute = UserInfoUtils.getLoggedInInstitute();
	   
	   StudentIdentification studentIdentification = studentIdentificationRepository.findByIdentificationIdAndInstitute(identificationId, institute);
	   
	   if (studentIdentification == null) {
           response.setMessageType(0);
           response.setMessage("No Student Found.");
           return response;
       } 
        
	   studentIdentification.setMigrationStatus(migrationStatus);
	   
	   response.setMessageType(1);
       response.setMessage("Migration Status successfully Changed.");
	   
	   return response;
	   
   }


}
