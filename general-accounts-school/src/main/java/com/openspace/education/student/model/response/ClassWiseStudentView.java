package com.openspace.education.student.model.response;

public class ClassWiseStudentView {
	
	private Long classId;
	
	private String className;
	
	private Integer totalStudent=0;
	
	private String color;
	

	public Long getClassId() {
		return classId;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(Integer totalStudent) {
		this.totalStudent = totalStudent;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	

}
