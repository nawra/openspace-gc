package com.openspace.education.student.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.openspace.education.initialsetup.model.entity.CoreSettingClass;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdentification;
import com.openspace.education.student.model.request.MigrationPushbackRq;

public interface StudentIdentificationRepository extends JpaRepository<StudentIdentification, Long>{
	

	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId,boolean studentStatus);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId);
		
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdInAndStudentStatusOrderByStudentRollAsc(Institute institute, Integer academicYear, List<Long> classConfigIds, boolean studentStatus);

	public List<StudentIdentification> findByInstituteAndStudentBasic_CustomStudentIdOrderByAcademicYearAsc(Institute institute,String customStudentId);

	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfoClassInfoIdAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classId,boolean studentStatus);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfoClassInfoIdAndGroupInfoIdAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classId,Long groupId,boolean studentStatus);
	
	public List<StudentIdentification> findByInstituteAndClassConfigurationInfo_ClassInfoAndIdentificationIdIn(Institute institute,CoreSettingClass coreSettingClass, List<Long> identificationIds);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndStudentStatusAndStudentRollBetweenOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId,boolean studentStatus,int startRoll,int endRoll);
	
	public StudentIdentification findByInstituteAndAcademicYearAndStudentBasic_CustomStudentId(Institute institute,Integer academicYear,String customStudentId);
	
	public StudentIdentification findByInstituteAndAcademicYearAndStudentStatusAndStudentBasic_CustomStudentId(Institute institute,Integer academicYear,boolean studentStatus,String customStudentId);

	public List<StudentIdentification> findByInstitute_InstituteIdAndIdentificationIdIn(Long instituteId, List<Long> identificationIds);

	public List<StudentIdentification> findByInstitute_InstituteIdAndAcademicYearAndStudentBasic_StudentIdIn(
			Long instituteId, Integer previousAcademicYear, Set<Long> studentIds);
	
	public StudentIdentification findByIdentificationIdAndInstitute_InstituteId(Long identificationId, Long instituteId);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId,Long groupId,boolean studentStatus);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndStudentCategoryInfo_IdAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId,Long groupId,Long categoryId, boolean studentStatus);

	public List<StudentIdentification> findByInstituteAndAcademicYearAndStudentBasic_StudentIdInAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,List<Long> studentIds,boolean studentStatus);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndClassConfigurationInfo_IdAndGroupInfo_IdAndMigrationStatusAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,Long classConfigId,Long groupId,boolean migrationStatus, boolean studentStatus);

	public List<StudentIdentification> findByInstituteAndIdentificationIdIn(Institute institute,Set<Long> identificationIds);
	
	public List<StudentIdentification> findByInstituteAndIdentificationIdIn(Institute institute,List<Long> identificationIds);
	
	public StudentIdentification findByIdentificationIdAndInstitute(Long identificationId,Institute institute);
	
	public StudentIdentification findByIdentificationIdAndInstituteInstituteId(Long identificationId,Long instituteId);
	
	public List<StudentIdentification> findByInstituteAndAcademicYearAndStudentStatusOrderByStudentRollAsc(Institute institute,Integer academicYear,boolean studentStatus);
	 
	
	@Query(value="select max(si.academicYear) from StudentIdentification si where si.studentBasic.customStudentId=?1 and si.studentBasic.institute.instituteId=?2 and si.institute.instituteId=?2")
	public Integer findMaxAcdemicYearOfStudent(String customStudentId,Long instituteId);
	    
	    
	    //------------------------------- Native Clause -----------------------------------//
	    
	 @Query(value="select c.id, c.`name`, count(si.identification_id) totalStudent "
	    + " from core_setting_class c, core_setting_class_configuration cnf, student_identification si "
	    + " where si.class_config_id=cnf.id and si.institute_id=cnf.institute_id "
	    + " and cnf.class_id=c.id and cnf.institute_id=c.institute_id "
	    + " and si.student_status=true and si.academic_year=?1 and si.institute_id=?2 group by c.id", nativeQuery=true)
	 public List<Object[]> classWiseStudentSummary(Integer academicYear, Long instituteId);
	    
	    //----------------------University Student Registration Start-----------------------
	    
	    public List<StudentIdentification> findByStudentRollAndInstituteEiinNo(Integer studentRoll,String collegeCode);
	    
	    public List<StudentIdentification> findByStudentBasicCustomStudentIdAndInstituteEiinNo(String customStudentId,String collegeCode);
	    
	    public List<StudentIdentification> findByStudentBasicRegistrationNoAndInstituteEiinNo(String registrationNo,String collegeCode);
	    
	    
	    public List<StudentIdentification> findByStudentBasicCustomStudentIdAndInstituteInstituteIdAndAcademicYear(String customStudentId,Long instituteId,Integer academicYear);
	    
	    
	    
		


		

	    
	    //----------------------University Student Registration End-----------------------
	    
}
