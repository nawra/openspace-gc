package com.openspace.education.student.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentBasic;

public interface StudentBasicRepository extends JpaRepository<StudentBasic, Long> {
	
	    @Query(value = "select (case when max(cast(custom_student_id as unsigned)) is not null then max(cast(custom_student_id as unsigned)) else 100000 end) from student_basic where institute_id=?1 and custom_student_id REGEXP '^[0-9]' ",nativeQuery=true)
	    public Long findMaxCustomStudentId(Long instituteId);
	  
	    public StudentBasic findByStudentIdAndInstitute(Long studentId,Institute institute);
	    
	    public StudentBasic findByStudentIdAndInstituteInstituteId(Long studentId,Long instituteId);
	    
	    public List<StudentBasic> findByInstituteAndStudentIdIn(Institute institute,List<Long> studentId);
	  
	  

}
