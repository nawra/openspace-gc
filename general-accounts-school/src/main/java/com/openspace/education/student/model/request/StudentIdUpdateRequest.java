package com.openspace.education.student.model.request;

public class StudentIdUpdateRequest {

	private String customStudentId;

	private Long studentId;

	public String getCustomStudentId() {
		return customStudentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	
	
	
}
