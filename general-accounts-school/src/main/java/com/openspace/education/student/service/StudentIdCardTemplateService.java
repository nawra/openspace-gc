package com.openspace.education.student.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.student.model.entity.StudentIdCardTemplate;
import com.openspace.education.student.model.request.StudentIdCardTemplateRequest;
import com.openspace.education.student.repository.StudentIdCardTemplateRepository;

@Service
public class StudentIdCardTemplateService {

	public Logger logger = LoggerFactory.getLogger(StudentIdCardTemplateService.class);

	@Autowired
	private StudentIdCardTemplateRepository cardTemplateRepository;

	@Transactional
	public BaseResponse saveStudentIdTemplate(StudentIdCardTemplateRequest request) {

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		BaseResponse response = new BaseResponse();
		StudentIdCardTemplate cardTemplate = new StudentIdCardTemplate();
		
		StudentIdCardTemplate template = cardTemplateRepository.findByInstitute(institute);
		
		if(template!=null) {
			
			template.setBarcodeValue(request.getBarcodeValue());
			template.setTemplateId(request.getTemplateId());
			
			response.setMessage("Student ID Card Template Update Successfull");
			response.setMessageType(1);
		}else {

		cardTemplate.setTemplateId(request.getTemplateId());
		cardTemplate.setBarcodeValue(request.getBarcodeValue());
		cardTemplate.setInstitute(institute);

		cardTemplateRepository.save(cardTemplate);

		response.setMessage("Student ID Card Template Save Successfull");
		response.setMessageType(1);
		}

		return response;
	}
	
	
	public ItemResponse getSingleStudentIdCardTEmplate() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		StudentIdCardTemplate template = cardTemplateRepository.findByInstitute(institute);
		StudentIdCardTemplateRequest response = new StudentIdCardTemplateRequest();
		response.setTemplateId(template.getTemplateId());		
		itemResponse.setItem(response);	
		return itemResponse;
		
	}

}
