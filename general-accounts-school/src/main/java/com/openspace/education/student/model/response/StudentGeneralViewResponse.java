package com.openspace.education.student.model.response;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StudentGeneralViewResponse {

	private Long studentId;

	private String customStudentId;

	private String studentName;

	private String studentGender;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date studentDOB;

	private String studentReligion;

	private String fatherName;

	private String fatherPhoto;

	private String fatherOccupation;

	private String motherName;

	private String motherPhoto;

	private String motherOccupation;

	private String guardianMobile;

	private String bloodGroup;

	private String imageName;

	private String mailingAddress;

	private Long identificationId;

	private Integer studentRoll;

	private boolean studentStatus;

	private String status;

	private boolean migrationStatus;

	private Date disableDate;

	private String disableReason;

	private String registrationNo;

	private String studentSession;

	private Long studentCategoryId;

	private String studentCategoryName;

	private Long groupId;

	private String groupName;

	private Long classId;

	private String className;

	private Long shiftId;

	private String shiftName;

	private Long sectionId;

	private String sectionName;

	private Long classConfigId;

	private String classConfigName;

	private String presentPostOffice;

	private Integer presentThanaId;

	private String presentThanaName;

	private Integer presentDistrictId;

	private String presentDistrictName;

	private String presentVillege;

	private String permanentPostOffice;

	private Integer permanentThanaId;

	private String permanentThanaName;

	private Integer permanentDistrictId;

	private String permanentDistrictName;

	private String permanentVillege;

	private String birthCertificateNo;

	private String fatherNid;

	private String motherNid;

	private Integer academicYear;

	private String fatherWorkingPlace;

	private String fatherDesignation;

	private String motherWorkingPlace;

	private String motherDesignation;

	public Long getStudentId() {
		return studentId;
	}

	public String getCustomStudentId() {
		return customStudentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public Date getStudentDOB() {
		return studentDOB;
	}

	public String getStudentReligion() {
		return studentReligion;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getFatherPhoto() {
		return fatherPhoto;
	}

	public String getFatherOccupation() {
		return fatherOccupation;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getMotherPhoto() {
		return motherPhoto;
	}

	public String getMotherOccupation() {
		return motherOccupation;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public String getImageName() {
		return imageName;
	}

	public String getMailingAddress() {
		return mailingAddress;
	}

	public Long getIdentificationId() {
		return identificationId;
	}

	public Integer getStudentRoll() {
		return studentRoll;
	}

	public boolean isStudentStatus() {
		return studentStatus;
	}

	public String getStatus() {
		return status;
	}

	public boolean isMigrationStatus() {
		return migrationStatus;
	}

	public Date getDisableDate() {
		return disableDate;
	}

	public String getDisableReason() {
		return disableReason;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public String getStudentSession() {
		return studentSession;
	}

	public Long getStudentCategoryId() {
		return studentCategoryId;
	}

	public String getStudentCategoryName() {
		return studentCategoryName;
	}

	public Long getGroupId() {
		return groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public Long getClassId() {
		return classId;
	}

	public String getClassName() {
		return className;
	}

	public Long getShiftId() {
		return shiftId;
	}

	public String getShiftName() {
		return shiftName;
	}

	public Long getSectionId() {
		return sectionId;
	}

	public String getSectionName() {
		return sectionName;
	}

	public Long getClassConfigId() {
		return classConfigId;
	}

	public String getClassConfigName() {
		return classConfigName;
	}

	public String getPresentPostOffice() {
		return presentPostOffice;
	}

	public Integer getPresentThanaId() {
		return presentThanaId;
	}

	public String getPresentThanaName() {
		return presentThanaName;
	}

	public Integer getPresentDistrictId() {
		return presentDistrictId;
	}

	public String getPresentDistrictName() {
		return presentDistrictName;
	}

	public String getPresentVillege() {
		return presentVillege;
	}

	public String getPermanentPostOffice() {
		return permanentPostOffice;
	}

	public Integer getPermanentThanaId() {
		return permanentThanaId;
	}

	public String getPermanentThanaName() {
		return permanentThanaName;
	}

	public Integer getPermanentDistrictId() {
		return permanentDistrictId;
	}

	public String getPermanentDistrictName() {
		return permanentDistrictName;
	}

	public String getPermanentVillege() {
		return permanentVillege;
	}

	public String getBirthCertificateNo() {
		return birthCertificateNo;
	}

	public String getFatherNid() {
		return fatherNid;
	}

	public String getMotherNid() {
		return motherNid;
	}

	public Integer getAcademicYear() {
		return academicYear;
	}

	public String getFatherWorkingPlace() {
		return fatherWorkingPlace;
	}

	public String getFatherDesignation() {
		return fatherDesignation;
	}

	public String getMotherWorkingPlace() {
		return motherWorkingPlace;
	}

	public String getMotherDesignation() {
		return motherDesignation;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public void setCustomStudentId(String customStudentId) {
		this.customStudentId = customStudentId;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public void setStudentDOB(Date studentDOB) {
		this.studentDOB = studentDOB;
	}

	public void setStudentReligion(String studentReligion) {
		this.studentReligion = studentReligion;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setFatherPhoto(String fatherPhoto) {
		this.fatherPhoto = fatherPhoto;
	}

	public void setFatherOccupation(String fatherOccupation) {
		this.fatherOccupation = fatherOccupation;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setMotherPhoto(String motherPhoto) {
		this.motherPhoto = motherPhoto;
	}

	public void setMotherOccupation(String motherOccupation) {
		this.motherOccupation = motherOccupation;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public void setIdentificationId(Long identificationId) {
		this.identificationId = identificationId;
	}

	public void setStudentRoll(Integer studentRoll) {
		this.studentRoll = studentRoll;
	}

	public void setStudentStatus(boolean studentStatus) {
		this.studentStatus = studentStatus;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setMigrationStatus(boolean migrationStatus) {
		this.migrationStatus = migrationStatus;
	}

	public void setDisableDate(Date disableDate) {
		this.disableDate = disableDate;
	}

	public void setDisableReason(String disableReason) {
		this.disableReason = disableReason;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public void setStudentSession(String studentSession) {
		this.studentSession = studentSession;
	}

	public void setStudentCategoryId(Long studentCategoryId) {
		this.studentCategoryId = studentCategoryId;
	}

	public void setStudentCategoryName(String studentCategoryName) {
		this.studentCategoryName = studentCategoryName;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setClassId(Long classId) {
		this.classId = classId;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setShiftId(Long shiftId) {
		this.shiftId = shiftId;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public void setSectionId(Long sectionId) {
		this.sectionId = sectionId;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public void setClassConfigId(Long classConfigId) {
		this.classConfigId = classConfigId;
	}

	public void setClassConfigName(String classConfigName) {
		this.classConfigName = classConfigName;
	}

	public void setPresentPostOffice(String presentPostOffice) {
		this.presentPostOffice = presentPostOffice;
	}

	public void setPresentThanaId(Integer presentThanaId) {
		this.presentThanaId = presentThanaId;
	}

	public void setPresentThanaName(String presentThanaName) {
		this.presentThanaName = presentThanaName;
	}

	public void setPresentDistrictId(Integer presentDistrictId) {
		this.presentDistrictId = presentDistrictId;
	}

	public void setPresentDistrictName(String presentDistrictName) {
		this.presentDistrictName = presentDistrictName;
	}

	public void setPresentVillege(String presentVillege) {
		this.presentVillege = presentVillege;
	}

	public void setPermanentPostOffice(String permanentPostOffice) {
		this.permanentPostOffice = permanentPostOffice;
	}

	public void setPermanentThanaId(Integer permanentThanaId) {
		this.permanentThanaId = permanentThanaId;
	}

	public void setPermanentThanaName(String permanentThanaName) {
		this.permanentThanaName = permanentThanaName;
	}

	public void setPermanentDistrictId(Integer permanentDistrictId) {
		this.permanentDistrictId = permanentDistrictId;
	}

	public void setPermanentDistrictName(String permanentDistrictName) {
		this.permanentDistrictName = permanentDistrictName;
	}

	public void setPermanentVillege(String permanentVillege) {
		this.permanentVillege = permanentVillege;
	}

	public void setBirthCertificateNo(String birthCertificateNo) {
		this.birthCertificateNo = birthCertificateNo;
	}

	public void setFatherNid(String fatherNid) {
		this.fatherNid = fatherNid;
	}

	public void setMotherNid(String motherNid) {
		this.motherNid = motherNid;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public void setFatherWorkingPlace(String fatherWorkingPlace) {
		this.fatherWorkingPlace = fatherWorkingPlace;
	}

	public void setFatherDesignation(String fatherDesignation) {
		this.fatherDesignation = fatherDesignation;
	}

	public void setMotherWorkingPlace(String motherWorkingPlace) {
		this.motherWorkingPlace = motherWorkingPlace;
	}

	public void setMotherDesignation(String motherDesignation) {
		this.motherDesignation = motherDesignation;
	}

}
