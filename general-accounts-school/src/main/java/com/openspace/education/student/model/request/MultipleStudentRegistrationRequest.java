package com.openspace.education.student.model.request;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class MultipleStudentRegistrationRequest {

	@NotNull
	@Size(min = 1)
	List<MultipleStudentRegistrationRequestHelper> multipleStudentList;

	@NotNull
	private Long classConfigurationId;

	@NotNull
	private Long categoryId;

	@NotNull
	private Integer academicYear;

	@NotNull
	private Long groupId;
	
	private boolean smsSend;

	

	public List<MultipleStudentRegistrationRequestHelper> getMultipleStudentList() {
		return multipleStudentList;
	}

	public void setMultipleStudentList(List<MultipleStudentRegistrationRequestHelper> multipleStudentList) {
		this.multipleStudentList = multipleStudentList;
	}

	public Long getClassConfigurationId() {
		return classConfigurationId;
	}

	public void setClassConfigurationId(Long classConfigurationId) {
		this.classConfigurationId = classConfigurationId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	

	public Integer getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(Integer academicYear) {
		this.academicYear = academicYear;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public boolean isSmsSend() {
		return smsSend;
	}

	public void setSmsSend(boolean smsSend) {
		this.smsSend = smsSend;
	}

	
	
}
