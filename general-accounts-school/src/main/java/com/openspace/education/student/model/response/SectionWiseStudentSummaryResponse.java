package com.openspace.education.student.model.response;

public class SectionWiseStudentSummaryResponse {

	private String classShiftSectionName;
	private String className;
	private String shiftName;
	private String sectionName;
	private Integer totalStudent;

	public String getClassShiftSectionName() {
		return classShiftSectionName;
	}

	public void setClassShiftSectionName(String classShiftSectionName) {
		this.classShiftSectionName = classShiftSectionName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public Integer getTotalStudent() {
		return totalStudent;
	}

	public void setTotalStudent(Integer totalStudent) {
		this.totalStudent = totalStudent;
	}

}
