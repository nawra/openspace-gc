package com.openspace.education.staff.model.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StaffBasicUpdateRequest {
	
	@NotNull
	private Long staffId;
	
	@NotBlank
    private String staffName;

	@NotNull
    private Integer staffSerial;

	@NotBlank
    private String customStaffId;
    
	@NotBlank
    private String gender;
    
	@NotBlank
    private String religion;
  
	@NotNull
    private Integer staffStatus;

    private String staffAddress;

    @NotBlank
    private String fatherName;
   
    @NotBlank
    private String motherName;
 
    @NotBlank
    private String mobileNumber;
    
    private String email;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date birthDate;
   
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date employmentDate;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date resignDate;
   
    private Long designationId;
    
    private String bloodGroup;
    
    

	public final Long getStaffId() {
		return staffId;
	}

	public final void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public final String getStaffName() {
		return staffName;
	}

	public final void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public final Integer getStaffSerial() {
		return staffSerial;
	}

	public final void setStaffSerial(Integer staffSerial) {
		this.staffSerial = staffSerial;
	}

	public final String getCustomStaffId() {
		return customStaffId;
	}

	public final void setCustomStaffId(String customStaffId) {
		this.customStaffId = customStaffId;
	}

	public final String getGender() {
		return gender;
	}

	public final void setGender(String gender) {
		this.gender = gender;
	}

	public final String getReligion() {
		return religion;
	}

	public final void setReligion(String religion) {
		this.religion = religion;
	}

	public final Integer getStaffStatus() {
		return staffStatus;
	}

	public final void setStaffStatus(Integer staffStatus) {
		this.staffStatus = staffStatus;
	}

	public final String getStaffAddress() {
		return staffAddress;
	}

	public final void setStaffAddress(String staffAddress) {
		this.staffAddress = staffAddress;
	}

	public final String getFatherName() {
		return fatherName;
	}

	public final void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public final String getMotherName() {
		return motherName;
	}

	public final void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public final String getMobileNumber() {
		return mobileNumber;
	}

	public final void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public final String getEmail() {
		return email;
	}

	public final void setEmail(String email) {
		this.email = email;
	}

	public final Date getBirthDate() {
		return birthDate;
	}

	public final void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public final Date getEmploymentDate() {
		return employmentDate;
	}

	public final void setEmploymentDate(Date employmentDate) {
		this.employmentDate = employmentDate;
	}

	public final Date getResignDate() {
		return resignDate;
	}

	public final void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}

	public final Long getDesignationId() {
		return designationId;
	}

	public final void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public final String getBloodGroup() {
		return bloodGroup;
	}

	public final void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
    


}
