package com.openspace.education.staff.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.initialsetup.model.entity.CoreSettingDesignation;
import com.openspace.education.initialsetup.repository.CoreSettingDesignationRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.model.request.StaffBasicRegistrationRequest;
import com.openspace.education.staff.model.request.StaffBasicUpdateRequest;
import com.openspace.education.staff.model.request.StaffBatchRegistrationRequest;
import com.openspace.education.staff.model.response.StaffBasicView;
import com.openspace.education.staff.repository.StaffRepository;

@Service
public class StaffRegistrationService {
   
	@Autowired
	public StaffRepository staffRepository;
	
	@Autowired
	private CoreSettingDesignationRepository coreSettingDesignationRepository;
	
	@Autowired
	private ImageStorageService imageStorageService;
	
	public BaseResponse registerSingleStaff(StaffBasicRegistrationRequest req) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		String customStaffId=(staffRepository.findMaxCustomStaffId(institute.getInstituteId())+1)+"";
		Integer staffSerial=staffRepository.findMaxStaffSerial(institute.getInstituteId())+1;
		
		Staff staff=new Staff();
		staff.setCustomStaffId(customStaffId);
		staff.setFatherName(req.getFatherName());
		staff.setGender(req.getGender());
		staff.setInstitute(institute);
		staff.setMobileNumber(req.getMobileNumber());
		staff.setMotherName(req.getMotherName());
		staff.setReligion(req.getReligion());
		staff.setStaffAddress(req.getStaffAddress());
		staff.setStaffName(req.getStaffName());
		staff.setStaffStatus(1);
		staff.setStaffSerial(staffSerial);
		staff.setBloodGroup(req.getBloodGroup());
		
		staffRepository.save(staff);
		
		baseResponse.setMessage("Staff Registration Successfully Done.");
		baseResponse.setMessageType(1);
		
		return baseResponse;
	}
	
	
	
   @Transactional	
   public BaseResponse updateStaffBasicInfo(StaffBasicUpdateRequest req) {
		
		BaseResponse baseResponse=new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		Staff staff=staffRepository.findByStaffIdAndInstitute(req.getStaffId(), institute);
		CoreSettingDesignation designation=coreSettingDesignationRepository.findByIdAndInstitute(req.getDesignationId(), institute);
		
		if(staff==null) {
			baseResponse.setMessage("No Staff Found");
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		staff.setCustomStaffId(req.getCustomStaffId());
		staff.setFatherName(req.getFatherName());
		staff.setGender(req.getGender());
		staff.setInstitute(institute);
		staff.setMobileNumber(req.getMobileNumber());
		staff.setMotherName(req.getMotherName());
		staff.setReligion(req.getReligion());
		staff.setStaffAddress(req.getStaffAddress());
		staff.setStaffName(req.getStaffName());
		staff.setStaffStatus(req.getStaffStatus());
		staff.setStaffSerial(req.getStaffSerial());
		staff.setBloodGroup(req.getBloodGroup());
		staff.setBirthDate(req.getBirthDate());
		staff.setEmploymentDate(req.getEmploymentDate());
		staff.setResignDate(req.getResignDate());
		staff.setDesignation(designation);
		staff.setEmail(req.getEmail());
			
		baseResponse.setMessage("Staff Basic Successfully Updated.");
		baseResponse.setMessageType(1);
		
		return baseResponse;
	}
   
   
   public BaseResponse updateSingleStaffPhoto(Long staffId, MultipartFile file) {
	   
	   BaseResponse baseResponse = new BaseResponse();
	   Institute institute=UserInfoUtils.getLoggedInInstitute();
	   Staff staff = staffRepository.findByStaffIdAndInstitute(staffId, institute);
	   if(staff==null) {
		   baseResponse.setMessage("No Staff Found");
		   baseResponse.setMessageType(0);
		   return baseResponse;
	   }
	   
	   String imageName=institute.getInstituteId()+"_"+staff.getCustomStaffId()+".jpg";
	   imageStorageService.uploadImgHeightWidth(FileFolder.STAFF.name(), imageName, file, 200, 200);
	   staff.setImageName(imageName);
	   staffRepository.save(staff);
		
	   baseResponse.setMessage("Staff Image Successfully Updated.");
	   baseResponse.setMessageType(1);
	   return baseResponse;
	}
   
   
  

	
	public ItemResponse staffBasicViewListForUpdate() {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Staff> staffList=staffRepository.findByInstituteOrderByStaffStatusDescStaffSerialAsc(institute);
		
		List<StaffBasicView> staffBasicViews=new ArrayList<>();
		
		for(Staff staff:staffList) {
			StaffBasicView staffBasicView=new StaffBasicView();
			staffBasicView.setCustomStaffId(staff.getCustomStaffId());
			
			if(staff.getDesignation()!=null) {
				staffBasicView.setDesignationId(staff.getDesignation().getId());
				staffBasicView.setDesignationName(staff.getDesignation().getName());
			}
			
			staffBasicView.setEmail(staff.getEmail());
			staffBasicView.setEmploymentDate(staff.getEmploymentDate());
			staffBasicView.setFatherName(staff.getFatherName());
			staffBasicView.setMotherName(staff.getMotherName());
			staffBasicView.setGender(staff.getGender());
			staffBasicView.setMobileNumber(staff.getMobileNumber());
			staffBasicView.setReligion(staff.getReligion());
			staffBasicView.setBirthDate(staff.getBirthDate());
			staffBasicView.setResignDate(staff.getResignDate());
			staffBasicView.setStaffAddress(staff.getStaffAddress());
			staffBasicView.setStaffId(staff.getStaffId());
			staffBasicView.setStaffName(staff.getStaffName());
			staffBasicView.setStaffSerial(staff.getStaffSerial());
			staffBasicView.setStaffStatus(staff.getStaffStatus());
			staffBasicView.setBloodGroup(staff.getBloodGroup());
			
		
			try {
				staffBasicView.setImageName(new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STAFF.name(), staff.getImageName())));	
			}catch(Exception e) {
				staffBasicView.setImageName("");
			}
			
			staffBasicViews.add(staffBasicView);
		}
		
		ItemResponse itemResponse=new ItemResponse(staffBasicViews);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
	}
	
	
	
	
	public BaseResponse staffBatchRegistration(List<StaffBatchRegistrationRequest> requests) {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		Long customStaffId=(staffRepository.findMaxCustomStaffId(institute.getInstituteId())+1);
		
		Integer staffSerial=staffRepository.findMaxStaffSerial(institute.getInstituteId())+1;
		
		List<Staff> staffList = new ArrayList<Staff>();		
		
		for(StaffBatchRegistrationRequest rq : requests) {
			
			Staff staff = new Staff();
			
			staff.setStaffName(rq.getStaffName());
			
			staff.setGender(rq.getGender());
			
			staff.setReligion(rq.getReligion());
			
			staff.setMobileNumber(rq.getMobileNumber());
			
			staff.setFatherName(rq.getFatherName());
			
			staff.setMotherName(rq.getMotherName());
			
			staff.setStaffSerial(staffSerial);
			
			staff.setCustomStaffId(customStaffId+"");
			
			staff.setInstitute(institute);
			
			staff.setStaffStatus(1);
											
			staffList.add(staff);
			
			customStaffId++;
			
			staffRepository.save(staffList);
		
		}
		
		BaseResponse baseResponse=new BaseResponse();
		baseResponse.setMessage(staffList.size()+" Staff Registration Successfully Done");
		baseResponse.setMessageType(1);
		return baseResponse;
		
	} 
}
