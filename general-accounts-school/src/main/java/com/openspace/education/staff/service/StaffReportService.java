package com.openspace.education.staff.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.FileFolder;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.image.ImageStorageService;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.model.response.StaffBasicView;
import com.openspace.education.staff.repository.StaffRepository;

@Service
public class StaffReportService {
	
	@Autowired
	public StaffRepository staffRepository;
	
	@Autowired
	public ImageStorageService imageStorageService;
	
	
	
    public ItemResponse staffBasicViewList() {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Staff> staffList=staffRepository.findByInstituteAndStaffStatusOrderByStaffSerialAsc(institute,1);
		
		List<StaffBasicView> staffBasicViews=new ArrayList<>();
		
		for(Staff staff:staffList) {
			StaffBasicView staffBasicView=new StaffBasicView();
			staffBasicView.setCustomStaffId(staff.getCustomStaffId());
			
			if(staff.getDesignation()!=null) {
				staffBasicView.setDesignationId(staff.getDesignation().getId());
				staffBasicView.setDesignationName(staff.getDesignation().getName());
			}
			
			staffBasicView.setEmail(staff.getEmail());
			staffBasicView.setEmploymentDate(staff.getEmploymentDate());
			staffBasicView.setFatherName(staff.getFatherName());
			staffBasicView.setMotherName(staff.getMotherName());
			staffBasicView.setGender(staff.getGender());
			staffBasicView.setMobileNumber(staff.getMobileNumber());
			staffBasicView.setReligion(staff.getReligion());
			staffBasicView.setBirthDate(staff.getBirthDate());
			staffBasicView.setResignDate(staff.getResignDate());
			staffBasicView.setStaffAddress(staff.getStaffAddress());
			staffBasicView.setStaffId(staff.getStaffId());
			staffBasicView.setStaffName(staff.getStaffName());
			staffBasicView.setStaffSerial(staff.getStaffSerial());
			staffBasicView.setStaffStatus(staff.getStaffStatus());
			staffBasicView.setBloodGroup(staff.getBloodGroup());
			
		
			try {
				staffBasicView.setImageName(new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STAFF.name(), staff.getImageName())));	
			}catch(Exception e) {
				staffBasicView.setImageName("");
			}
			
			staffBasicViews.add(staffBasicView);
		}
		
		ItemResponse itemResponse=new ItemResponse(staffBasicViews);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
	}
    
    
    	public ItemResponse staffBasicViewLightList() {
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<Staff> staffList=staffRepository.findByInstituteAndStaffStatusOrderByStaffSerialAsc(institute,1);
		
		List<StaffBasicView> staffBasicViews=new ArrayList<>();
		
		for(Staff staff:staffList) {
			StaffBasicView staffBasicView=new StaffBasicView();
			staffBasicView.setCustomStaffId(staff.getCustomStaffId());
			
			if(staff.getDesignation()!=null) {
				staffBasicView.setDesignationId(staff.getDesignation().getId());
				staffBasicView.setDesignationName(staff.getDesignation().getName());
			}
			
			staffBasicView.setEmail(staff.getEmail());
			staffBasicView.setEmploymentDate(staff.getEmploymentDate());
			staffBasicView.setFatherName(staff.getFatherName());
			staffBasicView.setMotherName(staff.getMotherName());
			staffBasicView.setGender(staff.getGender());
			staffBasicView.setMobileNumber(staff.getMobileNumber());
			staffBasicView.setReligion(staff.getReligion());
			staffBasicView.setBirthDate(staff.getBirthDate());
			staffBasicView.setResignDate(staff.getResignDate());
			staffBasicView.setStaffAddress(staff.getStaffAddress());
			staffBasicView.setStaffId(staff.getStaffId());
			staffBasicView.setStaffName(staff.getStaffName());
			staffBasicView.setStaffSerial(staff.getStaffSerial());
			staffBasicView.setStaffStatus(staff.getStaffStatus());
			staffBasicView.setBloodGroup(staff.getBloodGroup());

			
			staffBasicViews.add(staffBasicView);
		}
		
		ItemResponse itemResponse=new ItemResponse(staffBasicViews);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;
		
	}
    
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	 public ItemResponse singleStaffBasicView(String customStaffId) {
		
   	    ItemResponse itemResponse=new ItemResponse();
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		Staff staff=staffRepository.findByCustomStaffIdAndInstitute(customStaffId, institute);
		
		    if(staff==null) {
			   itemResponse.setMessage("No Teacher Found.");	
			   itemResponse.setMessageType(0);
			   return itemResponse;
		    }
		
		
		    StaffBasicView staffBasicView=new StaffBasicView();

		    staffBasicView.setCustomStaffId(staff.getCustomStaffId());
			
		    if(staff.getDesignation()!=null) {
			 staffBasicView.setDesignationId(staff.getDesignation().getId());
			 staffBasicView.setDesignationName(staff.getDesignation().getName());
		    }
			
			staffBasicView.setEmail(staff.getEmail());
			staffBasicView.setEmploymentDate(staff.getEmploymentDate());
			staffBasicView.setFatherName(staff.getFatherName());
			staffBasicView.setMotherName(staff.getMotherName());
			staffBasicView.setGender(staff.getGender());
			staffBasicView.setMobileNumber(staff.getMobileNumber());
			staffBasicView.setReligion(staff.getReligion());
			staffBasicView.setBirthDate(staff.getBirthDate());
			staffBasicView.setResignDate(staff.getResignDate());
			staffBasicView.setStaffAddress(staff.getStaffAddress());
			staffBasicView.setStaffId(staff.getStaffId());
			staffBasicView.setStaffName(staff.getStaffName());
			staffBasicView.setStaffSerial(staff.getStaffSerial());
			staffBasicView.setStaffStatus(staff.getStaffStatus());
			staffBasicView.setBloodGroup(staff.getBloodGroup());
			staffBasicView.setInstituteId(institute.getInstituteId());
		
			try {
				staffBasicView.setImageName(new String(imageStorageService.fetchImageInBase64Encode(FileFolder.STAFF.name(), staff.getImageName())));	
			}catch(Exception e) {
				staffBasicView.setImageName("");
			}
			
		
		  itemResponse.setItem(staffBasicView);
		  itemResponse.setMessage("OK");
		  itemResponse.setMessageType(1);
		  return itemResponse;
		
	  }
	
}
