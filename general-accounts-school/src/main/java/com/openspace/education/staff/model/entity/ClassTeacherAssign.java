package com.openspace.education.staff.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.institute.model.entity.Institute;


@Entity
@Table(name="class_teacher_assign" , uniqueConstraints= @UniqueConstraint(columnNames={"staff_id","class_config_id","institute_id"}))
public class ClassTeacherAssign  implements Serializable {

	
	private static final long serialVersionUID = 1402780339525530565L;

	@Id
	@Column(name="class_teacher_assign_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classTeacherAssignId;
		
	
	@ManyToOne
	@JoinColumn(name="institute_id")
	private Institute institute;	
	
	@ManyToOne
	@JoinColumn(name = "staff_id")
	private Staff staff;
	
	@ManyToOne
	@JoinColumn(name = "class_config_id")
	private ClassConfiguration classConfiguration;

	public Long getClassTeacherAssignId() {
		return classTeacherAssignId;
	}

	public void setClassTeacherAssignId(Long classTeacherAssignId) {
		this.classTeacherAssignId = classTeacherAssignId;
	}

	

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public ClassConfiguration getClassConfiguration() {
		return classConfiguration;
	}

	public void setClassConfiguration(ClassConfiguration classConfiguration) {
		this.classConfiguration = classConfiguration;
	}

	
	
}

