/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.staff.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.Staff;

/**
 *
 * @author riad
 */
public interface StaffRepository extends JpaRepository<Staff, Long>{
  
    @Query(value = "select (case when max(custom_staff_id) is not null then max(custom_staff_id) else 900001 end) from staff_basic_info where institute_id=?1 and custom_staff_id REGEXP '^[0-9]' ",nativeQuery=true)
    public Long findMaxCustomStaffId(Long instituteId);
    
    @Query(value = "select (case when max(custom_staff_id) is not null then max(staff_serial) else 0 end) from staff_basic_info where institute_id=?1",nativeQuery=true)
    public Integer findMaxStaffSerial(Long instituteId);
    
    public List<Staff> findByInstituteAndStaffStatusOrderByStaffSerialAsc(Institute institute,Integer staffStatus);
    
    public List<Staff> findByInstituteAndStaffIdInOrderByStaffSerial(Institute institute,List<Long> staffIds);
    
    public List<Staff> findByInstituteOrderByStaffStatusDescStaffSerialAsc(Institute institute);
    
    public Staff findByStaffIdAndInstitute(Long staffId,Institute institute);
    
    public Staff findByCustomStaffIdAndInstitute(String customStaffId,Institute institute);
}
