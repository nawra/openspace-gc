package com.openspace.education.staff.model.response;

public class ClassTeacherAssignView {

	private Long classTeacherAssignId;
		
	private Long  staffId;
	
	private String staffCustomId;
	
	private String staffName;
	
	private Long classConfigurationId;
	
	private String classConfigName;
	
	private String className;
	
	private String shiftName;
	
	private String sectionName;
	
	private String designation="";
	

	public Long getClassTeacherAssignId() {
		return classTeacherAssignId;
	}

	public void setClassTeacherAssignId(Long classTeacherAssignId) {
		this.classTeacherAssignId = classTeacherAssignId;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffCustomId() {
		return staffCustomId;
	}

	public void setStaffCustomId(String staffCustomId) {
		this.staffCustomId = staffCustomId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Long getClassConfigurationId() {
		return classConfigurationId;
	}

	public void setClassConfigurationId(Long classConfigurationId) {
		this.classConfigurationId = classConfigurationId;
	}

	public String getClassConfigName() {
		return classConfigName;
	}

	public void setClassConfigName(String classConfigName) {
		this.classConfigName = classConfigName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getShiftName() {
		return shiftName;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	
	
	
	
	
	

}
