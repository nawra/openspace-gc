package com.openspace.education.staff.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.openspace.education.common.ItemResponse;
import com.openspace.education.staff.service.StaffReportService;

@Controller
@RequestMapping(value = "/staff/report")
public class StaffReportController {
	
	@Autowired
	private StaffReportService staffReportService;
	
	
	@GetMapping(value = "/basic/info/list")
	public ResponseEntity<ItemResponse> staffBasicInfoList(){
		ItemResponse itemResponse=staffReportService.staffBasicViewList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/basic/info/light/list")
	public ResponseEntity<ItemResponse> staffBasicInfoLightList(){
		ItemResponse itemResponse=staffReportService.staffBasicViewLightList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
		
	}
	
	
	@GetMapping(value = "/single/view")
	public ResponseEntity<ItemResponse> staffBasicSingleView(String customStaffId){
		ItemResponse itemResponse=staffReportService.singleStaffBasicView(customStaffId);
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
		
	}

}
