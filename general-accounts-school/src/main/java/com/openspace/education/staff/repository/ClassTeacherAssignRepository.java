package com.openspace.education.staff.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.ClassTeacherAssign;
import com.openspace.education.staff.model.entity.Staff;

public interface ClassTeacherAssignRepository extends JpaRepository<ClassTeacherAssign, Long>{
	
	public List<ClassTeacherAssign> findByInstitute(Institute institute);
	
	public List<ClassTeacherAssign> findByInstituteAndStaff(Institute institute, Staff staff);
	
	public ClassTeacherAssign findByInstituteAndStaffAndClassConfiguration(Institute institute, Staff staff,ClassConfiguration classConfiguration);
	
	public ClassTeacherAssign findByClassTeacherAssignIdAndInstitute(Long classTeacherAssignId,Institute institute);

}
