package com.openspace.education.staff.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.staff.model.request.StaffBasicRegistrationRequest;
import com.openspace.education.staff.model.request.StaffBasicUpdateRequest;
import com.openspace.education.staff.model.request.StaffBatchRegistrationRequest;
import com.openspace.education.staff.service.StaffRegistrationService;
import com.openspace.education.student.model.request.MultipleStudentRegistrationRequest;

@Controller
@RequestMapping(value = "/staff")
public class StaffBasicController {
	
	@Autowired
	public StaffRegistrationService staffRegistrationService;
	
	@PostMapping(value = "/single/registration")
	public ResponseEntity<BaseResponse> saveSingleStaffRegistration(@RequestBody @Valid StaffBasicRegistrationRequest registrationRequest){
		BaseResponse baseResponse=staffRegistrationService.registerSingleStaff(registrationRequest);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	
	@PostMapping(value = "/multiple/registration")
	public ResponseEntity<BaseResponse> doMultipleStaffRegistration(@RequestBody @Valid List<StaffBatchRegistrationRequest> request){
		BaseResponse baseResponse=staffRegistrationService.staffBatchRegistration(request);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@PostMapping(value = "/basic/update")
	public ResponseEntity<BaseResponse> updateSingleStaffBasicInfo(@RequestBody @Valid StaffBasicUpdateRequest staffBasicUpdateRequest){
		BaseResponse baseResponse=staffRegistrationService.updateStaffBasicInfo(staffBasicUpdateRequest);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	@PostMapping(value = "/photo/update")
	public ResponseEntity<BaseResponse> updateSingleStaffPhoto(@RequestParam Long staffId,@RequestParam MultipartFile file){
		BaseResponse baseResponse=staffRegistrationService.updateSingleStaffPhoto(staffId, file);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
		
	}
	
	
	@GetMapping(value = "/basic/info/list/for/update")
	public ResponseEntity<ItemResponse> staffBasicInfoList(){
		ItemResponse itemResponse=staffRegistrationService.staffBasicViewListForUpdate();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
		
	}

}
