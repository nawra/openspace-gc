package com.openspace.education.staff.model.response;

//import java.awt.image.BufferedImage;
import java.util.Date;

public class StaffBasicView {

	private Long staffId;

	private String staffName;

	private Integer staffSerial;

	private String customStaffId;

	private String gender;

	private String religion;

	private Integer staffStatus;

	private String staffAddress;

	private String fatherName;

	private String motherName;

	private String mobileNumber;

	private String email;

	private Date birthDate;

	private Date employmentDate;

	private Date resignDate;

	private Long designationId;

	private String designationName="";

	private String bloodGroup;

	private String imageName;

//	private BufferedImage image;
	
	private Long instituteId;

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public final String getBloodGroup() {
		return bloodGroup;
	}

	public final void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getStaffSerial() {
		return staffSerial;
	}

	public void setStaffSerial(Integer staffSerial) {
		this.staffSerial = staffSerial;
	}

	public String getCustomStaffId() {
		return customStaffId;
	}

	public void setCustomStaffId(String customStaffId) {
		this.customStaffId = customStaffId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public Integer getStaffStatus() {
		return staffStatus;
	}

	public void setStaffStatus(Integer staffStatus) {
		this.staffStatus = staffStatus;
	}

	public String getStaffAddress() {
		return staffAddress;
	}

	public void setStaffAddress(String staffAddress) {
		this.staffAddress = staffAddress;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getEmploymentDate() {
		return employmentDate;
	}

	public void setEmploymentDate(Date employmentDate) {
		this.employmentDate = employmentDate;
	}

	public Date getResignDate() {
		return resignDate;
	}

	public void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}

	public Long getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Long designationId) {
		this.designationId = designationId;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public final Date getBirthDate() {
		return birthDate;
	}

	public final void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

//	public BufferedImage getImage() {
//		return image;
//	}
//
//	public void setImage(BufferedImage image) {
//		this.image = image;
//	}

	public Long getInstituteId() {
		return instituteId;
	}

	public void setInstituteId(Long instituteId) {
		this.instituteId = instituteId;
	}

	
}
