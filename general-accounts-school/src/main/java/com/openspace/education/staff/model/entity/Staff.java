/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openspace.education.staff.model.entity;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.openspace.education.initialsetup.model.entity.CoreSettingDesignation;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.location.model.entity.PostOffice;

/**
 *
 * @author riad
 */

@Entity
@Table(name="staff_basic_info",uniqueConstraints = @UniqueConstraint(columnNames = {"custom_staff_id","institute_id"}))
public class Staff implements Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 7056808380008369344L;

	@Id
    @Column(name="staff_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long staffId;
    
    @Column(name="staff_name")
    private String staffName;
    
    @Column(name="staff_serial")
    private Integer staffSerial;
       
    @Column(name="custom_staff_id")
    private String customStaffId;
    
    @Column(name="gender")
    private String gender;
    
    @Column(name="religion")
    private String religion;
    
    @Column(name="staff_status")
    private Integer staffStatus;
    
    @Column(name="staff_address")
    private String staffAddress;
    
    @Column(name="father_name")
    private String fatherName;
    
    @Column(name="mother_name")
    private String motherName;
    
    @Column(name="mobile_number")
    private String mobileNumber;
    
    @Column(name="email")
    private String email;
    
    @Column(name="blood_group")
    private String bloodGroup;
    
    @Column(name="image_name")
    private String imageName;
    
    @Column(name="birth_date")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    
    @Column(name="employment_date")
    @Temporal(TemporalType.DATE)
    private Date employmentDate;
    
    @Column(name="resign_date")
    @Temporal(TemporalType.DATE)
    private Date resignDate;
   
    @ManyToOne
    @JoinColumn(name="designation_id")
    private CoreSettingDesignation designation;
    
    @ManyToOne
    @JoinColumn(name = "institute_id" ,nullable = false)
    private Institute institute;

    


	public Long getStaffId() {
		return staffId;
	}

	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public Integer getStaffSerial() {
		return staffSerial;
	}

	public void setStaffSerial(Integer staffSerial) {
		this.staffSerial = staffSerial;
	}

	public String getCustomStaffId() {
		return customStaffId;
	}

	public void setCustomStaffId(String customStaffId) {
		this.customStaffId = customStaffId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public Integer getStaffStatus() {
		return staffStatus;
	}

	public void setStaffStatus(Integer staffStatus) {
		this.staffStatus = staffStatus;
	}

	public String getStaffAddress() {
		return staffAddress;
	}

	public void setStaffAddress(String staffAddress) {
		this.staffAddress = staffAddress;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public CoreSettingDesignation getDesignation() {
		return designation;
	}

	public void setDesignation(CoreSettingDesignation designation) {
		this.designation = designation;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Date getEmploymentDate() {
		return employmentDate;
	}

	public void setEmploymentDate(Date employmentDate) {
		this.employmentDate = employmentDate;
	}

	public Date getResignDate() {
		return resignDate;
	}

	public void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}

	public final String getBloodGroup() {
		return bloodGroup;
	}

	public final void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public final Date getBirthDate() {
		return birthDate;
	}

	public final void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

    
    
}
