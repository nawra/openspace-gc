package com.openspace.education.staff.model.request;

import org.hibernate.validator.constraints.NotBlank;

public class StaffBatchRegistrationRequest {

	@NotBlank
	private String staffName;

	@NotBlank
	private String gender;

	@NotBlank
	private String religion;

	@NotBlank
	private String fatherName;

	@NotBlank
	private String motherName;

	@NotBlank
	private String mobileNumber;

	public String getStaffName() {
		return staffName;
	}

	public String getGender() {
		return gender;
	}

	public String getReligion() {
		return religion;
	}

	public String getFatherName() {
		return fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
