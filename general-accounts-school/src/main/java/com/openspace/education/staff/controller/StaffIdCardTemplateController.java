package com.openspace.education.staff.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.staff.model.request.StaffIdCardTemplateRequest;
import com.openspace.education.staff.service.StaffIdCardTemplateService;

@Controller
@RequestMapping(value = "/staff/id-card/template")
public class StaffIdCardTemplateController {
	
	@Autowired
	private StaffIdCardTemplateService staffIdCardTemplateService;
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveStaffIdTemplate(@RequestBody StaffIdCardTemplateRequest request){
		BaseResponse baseResponse = staffIdCardTemplateService.saveStaffIdTemplate(request);
		return new ResponseEntity<>(baseResponse, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/view")
	public ResponseEntity<ItemResponse> singleIdCardTemplateView() {
		ItemResponse itemResponse = staffIdCardTemplateService.getSingleStaffIdCardTEmplate();
		return new ResponseEntity<>(itemResponse, HttpStatus.OK);
	}

}
