package com.openspace.education.staff.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.StaffIdCardTemplate;
import com.openspace.education.staff.model.request.StaffIdCardTemplateRequest;
import com.openspace.education.staff.repository.StaffIdCardTemplateRepository;

@Service
public class StaffIdCardTemplateService {

	public Logger logger = LoggerFactory.getLogger(StaffIdCardTemplateService.class);

	@Autowired
	private StaffIdCardTemplateRepository cardTemplateRepository;

	@Transactional
	public BaseResponse saveStaffIdTemplate(StaffIdCardTemplateRequest request) {

		Institute institute = UserInfoUtils.getLoggedInInstitute();
		BaseResponse response = new BaseResponse();
		StaffIdCardTemplate cardTemplate = new StaffIdCardTemplate();
		
		StaffIdCardTemplate template = cardTemplateRepository.findByInstitute(institute);
		
		if(template!=null) {
			
			template.setBarcodeValue(request.getBarcodeValue());
			template.setTemplateId(request.getTemplateId());
			
			response.setMessage("Staff ID Card Template Update Successfull");
			response.setMessageType(1);
		}else {

		cardTemplate.setTemplateId(request.getTemplateId());
		cardTemplate.setBarcodeValue(request.getBarcodeValue());
		cardTemplate.setInstitute(institute);

		cardTemplateRepository.save(cardTemplate);

		response.setMessage("Staff ID Card Template Save Successfull");
		response.setMessageType(1);
		}

		return response;
	}
	
	
	public ItemResponse getSingleStaffIdCardTEmplate() {
		
		ItemResponse itemResponse = new ItemResponse();
		Institute institute = UserInfoUtils.getLoggedInInstitute();
		StaffIdCardTemplate template = cardTemplateRepository.findByInstitute(institute);
		StaffIdCardTemplateRequest response = new StaffIdCardTemplateRequest();
		response.setTemplateId(template.getTemplateId());		
		itemResponse.setItem(response);	
		return itemResponse;
		
	}

}
