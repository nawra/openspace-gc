package com.openspace.education.staff.model.request;

import org.hibernate.validator.constraints.NotBlank;

public class StaffBasicRegistrationRequest {
	

	@NotBlank
    private String staffName;

    private String staffAddress;

    @NotBlank
    private String fatherName;
    
    @NotBlank
    private String motherName;
    
    @NotBlank
    private String mobileNumber;
    
    @NotBlank
    private String gender;
    
    @NotBlank
    private String religion;
    
    private String bloodGroup;
    
    
    
    

	public final String getBloodGroup() {
		return bloodGroup;
	}

	public final void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public String getStaffAddress() {
		return staffAddress;
	}

	public void setStaffAddress(String staffAddress) {
		this.staffAddress = staffAddress;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}
    
    
    

    


}
