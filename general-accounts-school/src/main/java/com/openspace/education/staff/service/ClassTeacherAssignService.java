package com.openspace.education.staff.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.common.UserInfoUtils;
import com.openspace.education.initialsetup.model.entity.ClassConfiguration;
import com.openspace.education.initialsetup.repository.ClassConfigurationRepository;
import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.ClassTeacherAssign;
import com.openspace.education.staff.model.entity.Staff;
import com.openspace.education.staff.model.response.ClassTeacherAssignView;
import com.openspace.education.staff.repository.ClassTeacherAssignRepository;
import com.openspace.education.staff.repository.StaffRepository;

@Service
public class ClassTeacherAssignService {

	@Autowired
	public ClassConfigurationRepository classConfigurationRepository;
	
	@Autowired
	public StaffRepository staffRepository;
	
	@Autowired
	public ClassTeacherAssignRepository classTeacherAssignRepository;
	
	
	
	@Transactional
	public BaseResponse saveClassTeacher(Long staffId, Long classConfigId) {
		
		BaseResponse baseResponse = new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		Staff staff = staffRepository.findByStaffIdAndInstitute(staffId, institute);
		
		if(staff==null) {
			baseResponse.setMessage("No Teacher Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;
		}
		
		ClassConfiguration classConfiguration = classConfigurationRepository.findByIdAndInstitute(classConfigId, institute);
		
		if(classConfiguration==null) {
			baseResponse.setMessage("No Section Found.");	
			baseResponse.setMessageType(0);
			return baseResponse;	
		}
		
		
		ClassTeacherAssign classTeacherAssign = classTeacherAssignRepository.findByInstituteAndStaffAndClassConfiguration(institute, staff,classConfiguration);
		
		if(classTeacherAssign==null) {
		
			classTeacherAssign = new ClassTeacherAssign();
			
			classTeacherAssign.setClassConfiguration(classConfiguration);
			classTeacherAssign.setInstitute(institute);
			classTeacherAssign.setStaff(staff);
			
			classTeacherAssignRepository.save(classTeacherAssign);
			
		}
		
		
		
		baseResponse.setMessage("Class Teacher Successfully Assigned.");	
		baseResponse.setMessageType(1);
		return baseResponse;	
		
	}
	
	
	@Transactional
	public BaseResponse deleteClassTeacher(Long assignId) {
		
		BaseResponse baseResponse = new BaseResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		ClassTeacherAssign classTeacherAssign = classTeacherAssignRepository.findByClassTeacherAssignIdAndInstitute(assignId, institute);
		
		if(classTeacherAssign==null) {
			baseResponse.setMessage("No Assigned Found..");	
			baseResponse.setMessageType(0);
			return baseResponse;
			
		}
		
		classTeacherAssignRepository.delete(classTeacherAssign);
		
		baseResponse.setMessage("Class Teacher Successfully Deleted.");	
		baseResponse.setMessageType(1);
		return baseResponse;	
		
	}
	
	
	

	public ItemResponse classTeacherList() {
		
		ItemResponse itemResponse = new ItemResponse();
		
		Institute institute=UserInfoUtils.getLoggedInInstitute();
		
		List<ClassTeacherAssign> classTeacherAssigns = classTeacherAssignRepository.findByInstitute(institute);
		
		List<ClassTeacherAssignView> views= new ArrayList<>();
		
		for(ClassTeacherAssign cta : classTeacherAssigns) {
			
			ClassTeacherAssignView view = new ClassTeacherAssignView();
			
			view.setClassTeacherAssignId(cta.getClassTeacherAssignId());
			view.setClassConfigurationId(cta.getClassConfiguration().getId());
			view.setClassName(cta.getClassConfiguration().getClassInfo().getName());
			view.setClassConfigName(cta.getClassConfiguration().getClassInfo().getName()+"-"+cta.getClassConfiguration().getShiftInfo().getName()+"-"+cta.getClassConfiguration().getSectionInfo().getName());
			view.setSectionName(cta.getClassConfiguration().getSectionInfo().getName());
			view.setShiftName(cta.getClassConfiguration().getShiftInfo().getName());
			view.setStaffCustomId(cta.getStaff().getCustomStaffId());
			view.setStaffId(cta.getStaff().getStaffId());
			view.setStaffName(cta.getStaff().getStaffName());
			
			if(cta.getStaff().getDesignation()!=null) {
				view.setDesignation(cta.getStaff().getDesignation().getName());	
			}
			
			views.add(view);
			
		}
		
		
		itemResponse.setItem(views);
		itemResponse.setMessage("OK");
		itemResponse.setMessageType(1);
		return itemResponse;	
		
	}
}
