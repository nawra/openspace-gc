package com.openspace.education.staff.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.openspace.education.common.BaseResponse;
import com.openspace.education.common.ItemResponse;
import com.openspace.education.staff.service.ClassTeacherAssignService;

@Controller
@RequestMapping(value = "/class-teacher/assign")
public class ClassTeacherAssignController {
	
	@Autowired
	public ClassTeacherAssignService classTeacherAssignService;
	
	
	@PostMapping(value = "/save")
	public ResponseEntity<BaseResponse> saveClassTeacher(@RequestParam Long staffId,@RequestParam Long classConfigId){
		BaseResponse baseResponse=classTeacherAssignService.saveClassTeacher(staffId, classConfigId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/delete")
	public ResponseEntity<BaseResponse> deleteClassTeacher(@RequestParam Long assignId){
		BaseResponse baseResponse=classTeacherAssignService.deleteClassTeacher(assignId);
		return new ResponseEntity<>(baseResponse,HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/list")
	public ResponseEntity<ItemResponse> classTeacherList(){
		ItemResponse itemResponse=classTeacherAssignService.classTeacherList();
		return new ResponseEntity<>(itemResponse,HttpStatus.OK);
	}
	

}
