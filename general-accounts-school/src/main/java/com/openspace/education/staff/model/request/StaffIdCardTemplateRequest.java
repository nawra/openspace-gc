package com.openspace.education.staff.model.request;

public class StaffIdCardTemplateRequest {

	private Integer templateId;

	private String barcodeValue;

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getBarcodeValue() {
		return barcodeValue;
	}

	public void setBarcodeValue(String barcodeValue) {
		this.barcodeValue = barcodeValue;
	}

}
