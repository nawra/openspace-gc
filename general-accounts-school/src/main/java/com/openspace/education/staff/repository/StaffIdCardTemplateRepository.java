package com.openspace.education.staff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.openspace.education.institute.model.entity.Institute;
import com.openspace.education.staff.model.entity.StaffIdCardTemplate;

public interface StaffIdCardTemplateRepository extends JpaRepository<StaffIdCardTemplate, Long> {
	
	public StaffIdCardTemplate findByInstituteAndTemplateId(Institute isInstitute,Integer templateId);
	public StaffIdCardTemplate findByInstitute(Institute institute);

}
