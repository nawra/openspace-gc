let successAlert = function (val) {
    return new Noty({
        text: val,
        timeout: 2000,
        type: 'success',
        dismissQueue: false,
    }).show();
}

let errorAlert = function (val) {
    return new Noty({
        text: val,
        timeout: 2000,
        type: 'error',
        dismissQueue: false,
    }).show();
}

let requireAlert = function () {
    return new Noty({
        text: "Please Select Required Field(s)",
        timeout: 2000,
        type: 'error',
        dismissQueue: false,
    }).show();
};


