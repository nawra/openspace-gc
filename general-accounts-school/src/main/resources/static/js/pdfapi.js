(function (api, $) {
    'use strict';
    api.writeText = function (x, y, text, options) {
        options = options || {};
        var defaults = {
            align: 'left',
            // width: this.internal.pageSize.getWidth()
            width: this.internal.pageSize.width
        }
        var settings = $.extend({}, defaults, options);
        var fontSize = this.internal.getFontSize();
        var txtWidth = this.getStringUnitWidth(text) * fontSize / this.internal.scaleFactor;
        if (settings.align === 'center')
            x += (settings.width - txtWidth) / 2;
        else if (settings.align === 'right')
            x += (settings.width - txtWidth);
        this.text(text, x, y);
    }
})(jsPDF.API, jQuery);


let insLogo = function () {
    return localStorage.getItem("logo")
}
let poweredBy = function (doc) {
    return doc.setFontSize(8), doc.text("Powered by Openspace24 Limited", 15, doc.internal.pageSize.height - 10);
}

let pdfGenerate = function (doc, col, rows, pageContent) {

    return doc.autoTable(col, rows, {
        headerStyles: {
            lineWidth: .01,
            lineColor: [224, 224, 224]
        },
        theme: "grid",
        startY: 45,
        addPageContent: pageContent
    })


};

let pdfDataL = function (doc, details) {

    var instituteName = localStorage.getItem("name")
    var instituteAddress = localStorage.getItem("address")
    var imgData = localStorage.getItem("logo")

    return doc.addImage(imgData, "JPEG", 15, 12, 20, 20),
        doc.setFontSize(15), doc.writeText(37, 21, instituteName, { align: "left" }),
        doc.setFontSize(9), doc.writeText(37, 26, instituteAddress, { align: "left" }),
        doc.setDrawColor(219, 219, 219), doc.line(14, 33, 283, 33),
        doc.setFontType("bold"), doc.setFontSize(11), doc.writeText(0, 40, details, { align: "center" });

}

var pdfday = new Date();
var dd = pdfday.getDate();
var mm = pdfday.getMonth() + 1;
var yyyy = pdfday.getFullYear();
if (dd < 10) {
    dd = '0' + dd
}

if (mm < 10) {
    mm = '0' + mm
}

pdfday = dd + '/' + mm + '/' + yyyy;
var lpowerdbypdf = "Powered by Openspace24 Limited                                                                         Page  ";
var ppowerdbypdf = "Powered by Openspace24 Limited                                Page  ";
var ldatepdf = `                                                                                                                                Date: ${pdfday}`;
var pdatepdf = `                                                                 Date: ${pdfday}`;

